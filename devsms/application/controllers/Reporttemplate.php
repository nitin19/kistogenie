<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reporttemplate extends CI_Controller {
	
	var $logmode;
	function __construct(){
       
      parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','Reporttemplate_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }

	

	public function index(){
		
		$data = array();
		$user_id						= $this->session->userdata('user_id');
		$status_search					= isset($_GET['status'])?$_GET['status']:NULL; 
		$word_search				    = isset($_GET['seachword'])?trim($_GET['seachword']):NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$search_condition['is_deleted']	= '0';
		
		if($status_search!=''){
            $search_condition['is_active']	= $status_search;
		  }
		 
		$data							= array();
		
		$school_id						= $this->session->userdata('user_school_id');
		
		$data['title']	      			= "Report Template";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage					= $PerPage;
		} else {
			$perpage					= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'reporttemplate/index';
	    $config['total_rows'] 			= $this->Reporttemplate_model->getRows($search_condition,$school_id,$word_search);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] 		= "/?status=$status_search&seachword=$word_search&perpage=$perpage"; 
		
		$this->pagination->initialize($config);

		$odr 							= "template_id";
		$dirc							= "desc";
		
		$data_rows						= $this->Reporttemplate_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$word_search);

		$templateid 					= $this->uri->segment(4);
		
		if($templateid!='') {
		  $template_data_rows			= $this->Reporttemplate_model->get_single_template($templateid);
		  $data['info']				    = $template_data_rows[0];
		 }

		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['status_search']			= $status_search;
		$data['word_search']		    = $word_search;
		$data['PerPage']				= $PerPage;

		$data['PerPage']				= $PerPage;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		$this->load->view('header');
		$this->load->view('progressreport/report_template',$data);
		$this->load->view('footer');
	}

	 public function addtemplate(){
		 
		    $reportdata 		= array();

			
			$school_id = $this->session->userdata('user_school_id');
			
		    $reportdata['school_id'] = $this->session->userdata('user_school_id');
			$reportdata['created_by'] = $this->session->userdata('user_name');
			$reportdata['updated_by'] = $this->session->userdata('user_name');
			$reportdata['deleted_by'] = $this->session->userdata('user_name');
			$reportdata['is_active'] = '1';
			$reportdata['is_deleted'] = '0';
			$reportdata['created_date'] = date('Y-m-d H:i:s');
			$reportdata['updated_date'] = date('Y-m-d H:i:s');
			$reportdata['deleted_date'] = date('Y-m-d H:i:s');
			
			if (trim($this->input->post('reporttitle')) != '') {
                $reportdata['title'] = trim($this->input->post('reporttitle'));
				
            }

            if (trim($this->input->post('subject_name')) != '') {
                $reportdata['subject_name'] = trim($this->input->post('subject_name'));
				
            }

            if (trim($this->input->post('cat_name')) != '') {
                $reportdata['category_name'] = trim($this->input->post('cat_name'));
				
            }

            if (trim($this->input->post('subcat_name')) != '') {
                $reportdata['subcategory_id'] = trim($this->input->post('subcat_name'));
				
            }
			
			if (trim($this->input->post('reportdesc')) != '') {
                $reportdata['description'] = trim($this->input->post('reportdesc'));
            }

		   if($this->Reporttemplate_model->insertTemplate($reportdata)) {
			   $this->session->set_flashdata('success', 'Template has been added successfully.');
			   redirect(base_url()."reporttemplate/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Template has not been added.');
				redirect(base_url()."reporttemplate/");exit;
		      }
		
			
        }

		public function updatetemplate(){
	
			
			  $start						    = $this->uri->segment(3);
		      $editedTemplateid					= $this->uri->segment(4);
			  $Action							= $this->uri->segment(5);  
	
		  $postURL	= $this->createPostURL($_GET);
		if($editedTemplateid==NULL){
			$this->session->set_flashdata('error', 'Select the Template first.');
			redirect(base_url()."reporttemplate");exit;
		   }
		   
		 $reportdata 					= array();
         $school_id 					= $this->session->userdata('user_school_id');
		 $where							= array('template_id'=>$editedTemplateid);
		 $data['title']	      			= "Edit Template";
		 $data['mode']					= "Edit";
			
			if (trim($this->input->post('reporttitle')) != '') {
                $reportdata['title'] = trim($this->input->post('reporttitle'));
				
            }
            
            if (trim($this->input->post('subject_name')) != '') {
                $reportdata['subject_name'] = trim($this->input->post('subject_name'));
                	if($reportdata['subject_name']=='islamic' || $reportdata['subject_name']=='arabic'){
                		$reportdata['subcategory_id']='0';

                	}else{

                		if (trim($this->input->post('subcat_name')) != '') {
                $reportdata['subcategory_id'] = trim($this->input->post('subcat_name'));
				
        		    }
          	}

				
            }

            if (trim($this->input->post('cat_name')) != '') {
                $reportdata['category_name'] = trim($this->input->post('cat_name'));
				
            }

            
			if (trim($this->input->post('reportdesc')) != '') {
                $reportdata['description'] = trim($this->input->post('reportdesc'));
            }
	     
			$reportdata['updated_by'] 	= $this->session->userdata('user_name');
			$reportdata['updated_date'] = date('Y-m-d H:i:s');
			
		   if($this->Reporttemplate_model->updateTemplates($reportdata,$where)) {
			   $this->session->set_flashdata('success', 'Template has been updated successfully.');
			   redirect(base_url()."reporttemplate/index/$start/$editedTemplateid/edit/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Template has not been updated.');
				redirect(base_url()."reporttemplate/index/$start/$editedTemplateid/edit/");exit;
		      }
	   }
	 
	    
	   	function deactivatetemplate(){
		
		  $start						    = $this->uri->segment(3);
		  $templateid						= $this->uri->segment(4); 
		  $where							= array('template_id'=>$templateid);
		  $data								= array(
		  										'is_active'=>'0',
												'updated_by'=>$this->session->userdata('user_name'),
												'updated_date'=>date('Y-m-d H:i:s'));
												
		if($this->Reporttemplate_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Template has been updated successfully.');
			redirect(base_url()."reporttemplate/index/$start/$templateid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Template has not been updated.');
			redirect(base_url()."reporttemplate/index/$start/$templateid");exit;
		}
		
	}
	function activatetemplate(){
		 
		  $start						    = $this->uri->segment(3);
		  $templateid						= $this->uri->segment(4); 
			  
		  $where							= array('template_id'=>$templateid);
		  $data								= array('is_active'=>'1',
											        'updated_by'=>$this->session->userdata('user_name'),
													'updated_date'=>date('Y-m-d H:i:s'));
													
		if($this->Reporttemplate_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Template has been updated successfully.');
			redirect(base_url()."reporttemplate/index/$start/$templateid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Template has not been updated.');
			redirect(base_url()."reporttemplate/index/$start/$templateid");exit;
		}
		
	}
	
	
		public function deletetemplate($templateid){
			
			 $school_id = $this->session->userdata('user_school_id');

			
		 		 if($this->Reporttemplate_model->delete($templateid)){
					$this->session->set_flashdata('success', 'Template has been deleted successfully.');
					redirect(base_url()."reporttemplate/index");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."reporttemplate/index");exit;
		          }

	    }

	public function get_subcategory(){
		$subject_name  = $this->input->post('subject_name');
		
		$cat_name  = $this->input->post('cat_name');
		
		$subcategorys = $this->Reporttemplate_model->get_subcategory($subject_name,$cat_name);
	
		foreach($subcategorys as $subcategory) {
		echo '<option value="'.$subcategory->dropdown_id.'">'.$subcategory->dropdown_name.'</option>';
			}		   		  
		  
		}

	
		function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}
}

