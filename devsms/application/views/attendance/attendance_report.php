<div class="editprofile-content">

        <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

       <ul>

          <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

           <?php  } elseif($user_type == 'student'){?>

              <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>
    <?php }else {?>
          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
    <?php } ?>

        <li><a href="<?php echo base_url(); ?>modifyattendance">Attendance</a></li>

        <li class="edit">Generate Report</li>        

        </ul>

        </div>

       <!-- <div class="col-sm-3 col-xs-4 actionbtn nopadding">



        <input type="button" value="Quick actions" class="activequickaction">



        </div>-->



        </div>

        

        

 <div style="clear:both"></div>

 <?php if($this->session->flashdata('error')): ?>

   <div class="alert alert-danger alert-dismissable" >

			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

			<b>Alert!</b> 

			  <?php echo $this->session->flashdata('error'); ?>

		</div>

<?php endif; ?>



<?php if($this->session->flashdata('success')): ?>

     <div class="alert alert-success alert-dismissable" >

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		   <?php echo $this->session->flashdata('success'); ?>

	   </div>

<?php endif; ?>

 <div style="clear:both"></div>

        <div class="attendancesec">	

        <div class="col-sm-12 profile-bg filterbox">

        <div class="filterdiv">


       <form action="<?php echo base_url();?>attendancereport/index" name="attendancereportForm" id="attendancereportForm">



    <div class="col-sm-12 col-xs-12 nopadding applycodediv">
          <div class="col-sm-4 nopadding tabfilterhalf">
                <div class="col-sm-2 col-xs-2 selectfilter datetext nopadding">
                <span>Date:</span>
                </div>
                <div class="col-sm-10 col-xs-10 nopadding datepickdiv">
                <div class="col-sm-5 col-xs-5 nopadding dateblk">
        		<div class="form-group">

    <div class="inputbox datepickerdiv nopadding">
      <input type="text" name="sdate" id="sdate" value="<?php echo @$sdate_search;?>" class="datepickerattendancereport1">
    </div>
  </div> 
  				</div>


<div class="col-sm-1 col-xs-1 dividerline nopadding"><span><img src="<?php echo base_url();?>assets/images/between.png"></span> </div>

                <div class="col-sm-5 col-xs-5 nopadding dateblk">
  				<div class="form-group">
    <div class="inputbox datepickerdiv nopadding">
      <input type="text" name="edate" id="edate" value="<?php echo @$edate_search;?>" class="datepickerattendancereport2">
    </div>
  </div>

  </div>
    <div class="col-sm-1 nopadding errspan">
    <span class="fa fa-question-circle"  tool-tip-toggle="tooltip-demo" data-original-title="Select starting and ending dates for generate report of attendance."></span>
  </div>
</div>

</div>
	<div class="col-sm-8 nopadding selectoption tabfilterfull mobfullfilter">



   <div class="col-sm-4 nopadding resselectfil">
    <div class="form-group fullwidthinput">
    <label class="col-sm-3 control-label">Branch:</label>
    <div class="col-sm-8 inputbox nopadding">
<?php /*?>      <select class="form-control" name="branch" id="branch">
      <option value="">Select Branch</option>   
    <?php foreach($branches as $branch) { ?>
     <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>

    <?php  } ?>

      </select><?php */?>
      
 <select class="form-control" name="branch" id="branch">
  
  <option value="">Select Branch</option>    
  
  <?php
  
   $staff_branch_id = $this->session->userdata('staff_branch_id');
   $user_type = $this->session->userdata('user_type');
   
     if($user_type=='teacher' && $staff_branch_id!=''){
      $staff_branch = explode(',',$staff_branch_id); 
	  foreach($staff_branch as $staff_branch_ids){
		   $this->load->model(array('attendancereport_model'));
	 $branchdta =$this->attendancereport_model->getstaff_branches($staff_branch_ids);
	  ?>
	 <option <?php if($branch_search == $branchdta->branch_id){ echo 'selected'; } ?> value="<?php echo $branchdta->branch_id; ?>"><?php echo $branchdta->branch_name;?></option>
	 <?php  }  } 
	 else if($user_type=='admin'){
	 
	 	foreach($branches as $branch){
	 ?>
     	  <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
		<?php  
		   
   }}
   else{
   	 ?>
     <option value=""></option>
     <?php } ?>
</select>

    </div>

  </div>

</div>

   <div class="col-sm-4 nopadding selectoption resselectfil">
<div class="form-group fullwidthinput">

    <label class="col-sm-3 control-label">Class:</label>
    <div class="col-sm-8 inputbox nopadding">
      <select class="form-control" name="class" id="class">
         <option value="">Select Class</option>
         <?php 
  if( @$_GET['branch']!='') {

		  $school_id		= $this->session->userdata('user_school_id');

		  $schoolbranch = @$_GET['branch'];

		  $this->load->model(array('attendancereport_model'));

		  $sclasses =$this->attendancereport_model->getclasses($school_id,$schoolbranch);

     foreach($sclasses as $sclass) { ?>

          <option <?php if($class_search == $sclass->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>

        <?php  } 

          }

	    ?>   

        </select>
    </div>

  </div>

</div>

   <div class="col-sm-4 rightspace selectoption resselectfil">
<div class="form-group generatereport">
    <input type="submit" class="btn btn-danger" value="Generate Report">
     <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>
 </div>

</div>

		</div>
        
        

     </div>

        </form>
        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">



    <?php if(@$_GET['sdate']!='' && @$_GET['edate']!='' ) { ?>

    

    <h1>Generate Report</h1>



    <div class="tablewrapper">

    

    <div id="printTable">



<table class="table-bordered table-striped">



			  <thead>



				  <tr class="headings">



					  <th class="column1">No.</th>



					  <th class="column4">First Name</th>



					  <th class="column4">Last Name</th>



					  <th class="column3">Present</th>



					  <th class="column2"> Late</th>



                      <th class="column3"> Absent</th>



                    <th class="column3">Action</th> 



                     



				  </tr>



			  </thead>



				<tbody>

                

                  <?php 

					  if(count(@$data_rows) > 0){

					      $sr=$last_page;

					  foreach($data_rows as $student) { 

					     $sr++;

		        $studentid = $student->student_id; 

		       $this->load->model(array('attendancereport_model'));

		       $studentinfo =$this->attendancereport_model->getstudentinfo($studentid);

			   

			       $startdate	= date('Y-m-d', strtotime($sdate_search));

			       $enddate	= date('Y-m-d', strtotime($edate_search));

			   

			    $totalAttendancestatus = $this->attendancereport_model->get_total_Attendancestatus($studentid,$startdate,$enddate);

			

				$presentAttendancestatus = $this->attendancereport_model->get_present_Attendancestatus($studentid,$startdate,$enddate);

				

				$absentAttendancestatus = $this->attendancereport_model->get_absent_Attendancestatus($studentid,$startdate,$enddate);

				

				$lateAttendancestatus = $this->attendancereport_model->get_late_Attendancestatus($studentid,$startdate,$enddate);

			  

			   $present_percentage =  ($presentAttendancestatus * 100) / $totalAttendancestatus;

			   $absent_percentage =  ($absentAttendancestatus * 100) / $totalAttendancestatus;

			   $late_percentage =  ($lateAttendancestatus * 100) / $totalAttendancestatus;

			   

					   ?>



					<tr class="familydata report">



						<td class="column1"><?php echo $sr; ?></td>



						<td class="column4"><?php if($studentinfo->student_fname!='') { echo $studentinfo->student_fname; } else { echo '<span style="color:transparent;">-</span>'; }?></td>



						<td class="column4"><?php if($studentinfo->student_lname!='') { echo $studentinfo->student_lname; } else { echo '<span style="color:transparent;">-</span>'; }?></td>



                        <td class="column3">(<?php echo  ceil($present_percentage);?>%)</td>



                        <td class="column2">(<?php echo  ceil($late_percentage);?>%)</td>



                        <td class="column3">(<?php echo  ceil($absent_percentage);?>%)</td>


<td class="column3"><a href="<?php echo base_url();?>viewattendance/index/?id=<?php echo $studentinfo->student_id;?>&sdate=<?php echo @$sdate_search;?>&edate=<?php echo @$edate_search;?>" style="cursor: pointer;" class="change">  <i class="fa fa-eye" data-toggle="tooltip" title="View Report" data-placement="top"></i></a></td>



                        </tr>



			    <?php	}		

					} else { ?>

                    <tr><th colspan="7" style="text-align: center; width:1056px;font-size:25px;height:100px; background:#FFF; color: #6a7a91; ">No record to show.</th></tr>	

				   <?php } ?>  



				</tbody>



		  </table>

          

     </div>     

          

     <?php if(count(@$data_rows) > 0) { ?>



       <div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-12 nopadding">

        <div class="confirmlink">
         
    <input type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" value="Save Archive" style="margin-right:10px;">
        
       <a href="#null" onclick="printContent('printTable')"> <input type="button" value="Print Report" class="btn btn-default"> </a>
        </div>



<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close closebtn" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Attendance Report Archive</h3>
        </div>
        <!---------error message ------------------>
        <div style="clear:both"></div>
       <div class="alert alert-danger alert-dismissable" id="AttendancereportArchiveError" style="display:none" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			Some problem exists. Attendance report archive has not been saved. 
		</div>
        
         <div class="alert alert-danger alert-dismissable" id="AttendancereportArchiveAlreadyError" style="display:none" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			Attendance report archive name already exists. Attendance report archive has not been saved.
		</div>
    
<!---------error message end------------------>
<!---------success message ------------------>
         
      <div class="alert alert-success alert-dismissable" id="AttendancereportArchiveSuccess" style="display:none">
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		    Attendance report archive has been saved successfully. Please click here to view <a href="<?php echo base_url();?>attendancereportarchive"> report archive </a>
	   </div>

 <div style="clear:both"></div>         
<!---------success message end------------------>
        <div class="modal-body">
        <form action="<?php echo base_url();?>attendancereport/insert_archive_data" name="AttendancereportarchiveForm" id="AttendancereportarchiveForm">
        
            <input type="hidden" name="att_sdate" id="att_sdate" value="<?php echo @$_GET['sdate'];?>"> 
            <input type="hidden" name="att_edate" id="att_edate" value="<?php echo @$_GET['edate'];?>">
            <input type="hidden" name="att_branch" id="att_branch" value="<?php echo @$_GET['branch'];?>" >
            <input type="hidden" name="att_class" id="att_class" value="<?php echo  @$_GET['class'];?>">
            
          <div class="col-sm-12 popup_section_form">
              <div class="col-sm-10 nopadding text_box_sec">
                 <input type="text" name="filename" id="filename" placeholder="Enter Attendance Report Archive Name" maxlength="41" />
             </div>
             <div class="col-sm-2 nopadding text_box_btn">
                <input value="Save" class="popup_save_btn" id="save" name="save" type="submit">
             </div>
          </div>
          </form>
        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>
      
    </div>
  </div>




        </div>



        </div>        



        <?php } ?>



</div>



    <?php } else { ?>

    

    <div class="beforeresult_section"><h1></h1></div>



     <?php } ?>







	</div>



    



	</div>



    </div>



    </div>

    

    

    <script type="text/javascript">

<!--

function printContent(id){

str=document.getElementById(id).innerHTML

newwin=window.open('','printwin','left=100,top=100,width=400,height=400')

newwin.document.write('<HTML>\n<HEAD>\n')

newwin.document.write('<TITLE>Print Page</TITLE>\n')

newwin.document.write('<script>\n')

newwin.document.write('function chkstate(){\n')

newwin.document.write('if(document.readyState=="complete"){\n')

newwin.document.write('window.close()\n')

newwin.document.write('}\n')

newwin.document.write('else{\n')

newwin.document.write('setTimeout("chkstate()",2000)\n')

newwin.document.write('}\n')

newwin.document.write('}\n')

newwin.document.write('function print_win(){\n')

newwin.document.write('window.print();\n')

newwin.document.write('chkstate();\n')

newwin.document.write('}\n')

newwin.document.write('<\/script>\n')

newwin.document.write('</HEAD>\n')

newwin.document.write('<BODY onload="print_win()">\n')

newwin.document.write(str)

newwin.document.write('</BODY>\n')

newwin.document.write('</HTML>\n')

newwin.document.close()

}

//-->

</script>

    

 <script>

  jQuery(document).ready(function(){

	  

	  	   jQuery(".cleanSearchFilter").click(function() {

	          jQuery("#attendancereportForm")[0].reset();

			   window.location.href='<?php echo base_url()?>attendancereport/index';

			}); 

			

	  jQuery("#branch").on('change', function(e){ 

   		  var schoolbranch = jQuery(this).val();

		  if(schoolbranch!='') {

			// var BranchID =  jQuery('input[name=student_school_branch]:checked').val();

			jQuery("#class").find('option[value!=""]').remove();

			

			jQuery.ajax({

            type : 'POST',

            url  : '<?php echo base_url(); ?>attendancereport/check_classes',

            data:{ 'schoolbranch': schoolbranch },

            success :  function(resp) {

					    jQuery('#class').append(resp);

			          }

				   });

				 } else {

			     jQuery("#class").find('option[value!=""]').remove();

				 return false;

				 }

			 });

			 

			 

	    jQuery("#attendancereportForm").validate({

        rules: {

			

			  sdate: {

    					required: true

   					},

			   edate: {

    					required: true

   					}

					

            /*  branch: {

                 required: true

                 },

			  class: {

    					required: true

   					}, 

			   

			  type: {

                required: true

                 }*/

            },

        messages: {

			

			  sdate: {

    					required: "Please select start date."

   				 },

			  edate: {

    					required: "Please select end date."

   				 }

				 

              /*branch: {

                  required: "Please select branch."

                 },

			  class: {

    					required: "Please select class."

   					},

			  

			  type: {

                   required: "Please select type."

                 }*/

             },

       

        submitHandler: function(form) {

            form.submit();

          }

        });



		
		
	jQuery.validator.addMethod("alphaLnumber", function(value, element) {
      return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);
    });		
		
   jQuery("#AttendancereportarchiveForm").validate({
        rules: {
			  filename: {
                required: true,
				alphaLnumber: true,
				maxlength: 40,
                 }
            },
        messages: {
			  filename: {
                   required: "Please enter attendance report archive name.",
				   alphaLnumber: "Letter and number are allowed.",
				   maxlength: "Maximum 40 character are allowed.",
                 }
             },
		 submitHandler: submitForm
        });
		
	  	function submitForm() {
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>attendancereport/insert_archive_data',
            data : jQuery("#AttendancereportarchiveForm").serialize(),
			dataType : "html",
           beforeSend: function()
            {
            },
                 success :  function(data) {
				    data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
					  jQuery('#AttendancereportArchiveError').hide();
					  jQuery('#AttendancereportArchiveAlreadyError').hide();
					  jQuery("#AttendancereportarchiveForm")[0].reset();
					   jQuery('#AttendancereportArchiveSuccess').show();
					   
			            setTimeout(profilesteps, 6000);
    			         function profilesteps() {
							 jQuery('#AttendancereportArchiveSuccess').hide();
							 jQuery('#myModal').modal('hide');
					       }
					   
					} else if(data.Status == 'false') {
						jQuery('#AttendancereportArchiveSuccess').hide();
						jQuery('#AttendancereportArchiveAlreadyError').hide();
						jQuery('#AttendancereportArchiveError').show();
					} else if(data.Status == 'alreadyexist') {
						jQuery('#AttendancereportArchiveSuccess').hide();
						jQuery('#AttendancereportArchiveError').hide();
						jQuery('#AttendancereportArchiveAlreadyError').show();
					} 
             }
        });
        return false;
      }

	 });
	 
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>

<script type="text/javascript">
 
$(document).ready(function(){
 
    $('[tool-tip-toggle="tooltip-demo"]').tooltip({
 
        placement : 'top'
 
    });
 
});
 
</script>
<style>
 .beforeresult_section > h1 {

    min-height: 300px;

	}
	
	#myModal { margin-top:140px; } 

.save_btn > input {
    background: #37b248 none repeat scroll 0 0;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #fff;
    font-size: 14px;
    padding: 7px 20px;
}
.save_btn {
    padding-top: 20px;
}
.popup_save_btn {
    background: #37b248 none repeat scroll 0 0;
    border: medium none;
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    color: #fff;
    padding: 12px 25px;
}
.modal-body {
    padding: 35px 0 70px;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-radius: 3px;
    min-height: 45px;
    width: 100%;
	padding: 10px;
}
.btn.btn-default {
    background: #37b248 none repeat scroll 0 0;
    color: #fff;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
    min-height: 44px;
    padding: 10px;
    width: 100%;
}
.save_btn {
    text-align: right;
}
.text_box_btn {
    text-align: left;
}
.modal-header {
    text-align: center;
}

.errspan {
    padding: 6px 0 0 10px;
}
.rightspace {
     padding: 0 0 0 0px !important;
}

.change .fa-eye:hover {
  color: #57cdf7;
}

.fa-question-circle + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.fa-question-circle{color:#090;}

.fa-eye + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-eye + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.modal-header{background: #36b047;}
.modal-header> h3 {
   color: #fff;
}
.closebtn{
    background: #fff none repeat scroll 0 0 !important;
    border-radius: 40px;
    float: right;
    height: 40px;
    opacity: 1;
    width: 40px;
	color:#36b047;
}
</style>