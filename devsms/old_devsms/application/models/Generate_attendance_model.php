<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Generate_attendance_model extends CI_Model {

	var $tbl	= "student";
	var $users = "users";
	var $nationality = "nationality";
	var $country = "country";
	var $religion = "religion";
	var $ethnic_origin = "ethnic_origin";
	var $branch = "branch";
	var $fee_band = "fee_band";
	var $classes = "classes";
	var $attendance = "attendance";
	var $attendance_register = "attendance_register";
	var $staff = "staff";
	var $generate_salary = "generate_salary";
	
	

    function __construct() {
        parent::__construct();
    }
	
	 function getRows($where=NULL,$school_id){
	      $this->db->select('*');
          $this->db->from($this->staff);
		  $this->db->join('users', 'users.id = '.$this->staff.'.user_id','INNER'); 
		  $this->db->where(array($this->staff.'.school_id'=>$school_id));
		  if($where != NULL){
		  $this->db->where($where);
		  }
          $query = $this->db->get();
		  $this->db->last_query();  
		  return $query->num_rows();
	    }
	  
	   function getPagedData($where=NULL,$start,$limit,$dirc=NULL,$school_id){
	      $this->db->select('*');
          $this->db->from($this->staff);
		  $this->db->join('users', 'users.id = '.$this->staff.'.user_id','INNER'); 
		  $this->db->where(array($this->staff.'.school_id'=>$school_id));
		  if($where != NULL){
		  $this->db->where($where);
		    }
	 	  $this->db->limit($limit, $start);
          $query = $this->db->get();
          $this->db->last_query();
          return  $query->result();
	     }
	
	function getschoolclasses($school_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        $this->db->last_query();
        return  $query->result();
      }
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        $this->db->last_query();
        return  $query->result();
      }
	
	function getbranchName($staff_school_branch) {
        $this->db->select('*');
        $this->db->from($this->branch);
	    $this->db->where(array("branch_id="=>$staff_school_branch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        $this->db->last_query();
        return  $query->row();
       }
	
	function getclassName($staff_class_name) {
        $this->db->select('*');
        $this->db->from($this->classes);
	    $this->db->where(array("class_id="=>$staff_class_name,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        $this->db->last_query();
        return  $query->row();
       }
	
      function getstaff_branches($staff_branch_ids) {
        $this->db->select('*');
        $this->db->from($this->branch);
	    $this->db->where(array("branch_id"=>$staff_branch_ids,"is_active"=>'1',"is_deleted"=>'0'));
        $query = $this->db->get();
        $this->db->last_query();
        return  $query->row();
       }
	
     function getclasses($school_id,$schoolbranch) {
        $this->db->select('*');
        $this->db->from($this->classes);
	    $this->db->where(array("school_id="=>$school_id,"branch_id"=>$schoolbranch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        $this->db->last_query();
        return  $query->result();
    }
	
	function user_attendence($userid) {
        $this->db->select('*');
        $this->db->from($this->attendance_register);
	    $this->db->where(array("user_id"=>$userid,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        $this->db->last_query();
        return  $query->result();
    }

	function get_staff($school_id,$branch_search, $limit=NULL, $offset=NULL) {
		$this->db->limit($limit, $offset);
	 	$this->db->select('*');
        $this->db->from($this->staff);
        $this->db->join($this->users, $this->users.'.id = '.$this->staff.'.user_id','INNER' );
		$this->db->where(array($this->staff.'.school_id'=>$school_id,$this->users.".user_type="=>'teacher',$this->users.".is_deleted="=>'0',$this->users.".is_active="=>'1'));
		if($branch_search!=''){
		$this->db->where($this->staff.'.existing_branch',$branch_search );	
		}
		$this->db->order_by($this->staff.".staff_id", "desc"); 
		 $query = $this->db->get();
		$this->db->last_query(); 
		return $query->result();
      }

     function record_count($school_id,$branch_search) {
	 	$this->db->select('*');
        $this->db->from($this->staff);
        $this->db->join($this->users, $this->users.'.id = '.$this->staff.'.user_id','INNER' );
		$this->db->where(array($this->staff.'.school_id'=>$school_id,$this->users.".user_type="=>'teacher',$this->users.".is_deleted="=>'0',$this->users.".is_active="=>'1'));
		if($branch_search!=''){
		$this->db->where($this->staff.'.existing_branch',$branch_search );	
		}
		$this->db->order_by($this->staff.".staff_id", "desc"); 
		 $query = $this->db->get();
		$this->db->last_query();  
        return $query->num_rows();
    }

    function staff_attendance($userid, $sdate){
		$attmonth =  date('Y-m-d', strtotime($sdate));
		$attcalender = explode('-', $attmonth);
		$calenderyear = $attcalender[0];
		$calendermonth =  $attcalender[1];

		$sql = "SELECT * FROM `attendance_register` WHERE `user_id` = '$userid' AND `is_active` = '1' AND `is_deleted` = '0' AND MONTH(Date_Time) = '$calendermonth' AND YEAR(Date_Time) = '$calenderyear' GROUP  BY DATE(Date_Time)";
		$query = $this->db->query($sql);
        $this->db->last_query();
        return  $query->result();
    }
	
	function staff_day_hrs($presentdate){
		$datepresent =  date('Y-m-d', strtotime($presentdate));
		$sql = "SELECT * FROM `attendance_register` WHERE Date_Time LIKE '$datepresent%'" ;
		$query = $this->db->query($sql);
         $this->db->last_query();
        return  $query->result();
	}
	
	function generated_salary($month_year, $sal_branch) {
        $this->db->select('*');
        $this->db->from($this->generate_salary);
	    $this->db->where(array("branch_id"=>$sal_branch,"month_year"=>$month_year,"is_active="=>'1',"is_deleted="=>'0',));
        $query = $this->db->get();
        $this->db->last_query(); 
        return  $query->result();
    }
	
	function getuserinfo($user_id) {
        $this->db->select('*');
        $this->db->from($this->staff);
	    $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $this->db->last_query(); 
        return  $query->row();
    }
	
	function generatesalary($inputdata){
	$userid  = 	$inputdata['user_id_arr'];
	//$user_tot_wrk  = 	$inputdata['user_tot_wrk'];
	//$staff_present_day  = 	$inputdata['staff_present_day'];
	$branchid  = 	$inputdata['branch_id'];
	$total_working_hours  = 	$inputdata['total_working_hours'];
	$total_working_days  = 	$inputdata['total_working_days'];
	$working_hrs_in_days  = 	$inputdata['working_hrs_in_days'];
	$present_working_days  = 	$inputdata['present_working_days'];
	$absent_working_days  = 	$inputdata['absent_working_days'];
	$present_working_hours  = 	$inputdata['present_working_hours'];
	$absent_working_hours  = 	$inputdata['absent_working_hours'];
	$late_working_hours	  = 	$inputdata['late_working_hours'];
	$actual_salary         = 	$inputdata['actual_salary'];
	$received_salary       = 	$inputdata['received_salary'];
	$adjusted             = 	$inputdata['adjusted'];
	$grand                = 	$inputdata['grand'];
	
	$isactive = '1';
	$date = date('Y-m-d H:i:s');
	$schoolid = $this->session->userdata('user_school_id');
	$month_year = $this->input->post('month_year');
	
	$data =array();
	for ($i = 0; $i < count($userid); $i++) {
		 $sv = $userid[$i];
		 $data[$i] = array(
           'user_id' => $sv,
		   'branch_id' => $branchid[$sv],
		   'adjust_amount' => $adjusted[$sv],
		   'grand_amount' => $grand[$sv],
		    'is_active' => $isactive,
			'created_date' => $date,
			'updated_date' => $date,
			'school_id' => $schoolid,
			'month_year' => $month_year,
			'total_working_days' => $total_working_days[$sv],
			'total_working_hours' => $total_working_hours[$sv],
			'present_working_days' => $present_working_days[$sv],
			'absent_working_days' => $absent_working_days[$sv],
			'present_working_hours' => $present_working_hours[$sv],
			'late_working_hours' => $late_working_hours[$sv],
			'absent_working_hours' => $absent_working_hours[$sv],
			'actual_salary' => $actual_salary[$sv],
			'calculated_salary' => $received_salary[$sv],
			'adjust_amount' => $adjusted[$sv],
			'grand_amount' => $grand[$sv],
          );
	}
			
		 if($this->db->insert_batch($this->generate_salary, $data))
			return $this->db->insert_id();
		   else
			return false;
    }
	
	 function salary_record_count($branch_search,$sdate,$search_condition,$school_id) {
	 	$this->db->select('*');
        $this->db->from($this->generate_salary);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
		//if($branch_search!='0'){
			$this->db->where('branch_id', $branch_search);
	// }
	 if($sdate!=''){
			$this->db->where('month_year', $sdate);
	 }
		$this->db->order_by('gen_sal_id', "desc"); 
		 $query = $this->db->get();
		$this->db->last_query();
        return $query->num_rows();
    }
	
	function get_salary_record($branch_search,$sdate,$search_condition,$school_id, $limit=NULL, $offset=NULL) {
		 $this->db->limit($limit, $offset);
	 	$this->db->select('*');
        $this->db->from($this->generate_salary);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
		//if($branch_search!='0'){
			$this->db->where('branch_id', $branch_search);
	// }
	 if($sdate!=''){
			$this->db->where('month_year', $sdate);
	 }
		$this->db->order_by('gen_sal_id', "desc"); 
		 $query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();
    }


}