<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shortlistedcandidate_model extends CI_Model {
	var $tbl	= "staff";
	var $users = "users";
	var $branch = "branch";
	var $classes = "classes";
	var $subjects = "subjects";
	var $teacher_role = "teacher_role";
	var $teacher_level = "teacher_level";
	var $teacher_type = "teacher_type";
	var $school	= "school";
	var $currency="currency";
	var $staff_access	= "staff_access";
	var $application_bank = "application_bank";
	var $short_listed_candidates = "short_listed_candidates";
	//var $password	= "password";

    function __construct() {
        parent::__construct();
    }
	
/*---------------dhrup-----------------*/		
	function move_to_application_bank($application_data){
	//print_r($application_data);
	if($this->db->insert("application_bank", $application_data)){
		return $this->db->insert_id();
	  }else{
		return false;
	  }
	}
	
	function deleteenroll($sender_id){
		$where		= array('user_id' => $sender_id);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update("staff_enrolement", $data)){
			return true;
		 }else{
			return false;
		}
	}
/*---------------dhrup-----------------*/

	function get_staffenrollment($user_id){
		$this->db->select('*');
		$this->db->from("staff_enrolement");
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();
        return  $query->row();
	}

	function get_branch($branch_id){
		$this->db->select('*');
		$this->db->from("branch");
		$this->db->where('branch_id', $branch_id);
		$query = $this->db->get();
        return  $query->row();
	}
	
	function checkstaffname($Uname) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where('username', $Uname);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function checkstaffemail($email, $school_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('email'=>$email,'school_id'=>$school_id, 'is_deleted'=>'0'));
        $query = $this->db->get();
		return $query->num_rows();
    }
	
	function checkeditstaffemail($email, $id, $school_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('email'=>$email,'id !='=>$id,'school_id'=>$school_id , 'is_deleted'=>'0'));
        $query = $this->db->get();
		return $query->num_rows();
    }
	


function get_records($where, $school_id, $limit=NULL, $offset=NULL,$gg,$user_srch) 
{
		$this->db->limit($limit, $offset);
	 	$this->db->select('slc.id as slc_id, slc.user_id as user_id, se.staff_fname as staff_fname, se.staff_lname as staff_lname, s.school_name as school_name, se.staff_telephone as staff_telephone, u.email as email');
        $this->db->from("short_listed_candidates slc, users u, staff_enrolement se, school s");
        $this->db->where("slc.school_id = '$school_id' AND slc.is_deleted = '0' AND se.user_id = slc.user_id AND u.id = slc.user_id AND s.school_id = se.school_id $gg");
		$this->db->order_by("slc.id", "desc"); 
		 $query = $this->db->get();
		$rr= $this->db->last_query(); 
		//echo $rr; exit();
        $result = $query->result();
    return $result;	
}

public function record_count($where, $school_id, $gg,$user_srch) {
         $this->db->select('*');
        $this->db->select('slc.id as slc_id, slc.user_id as user_id, se.staff_fname as staff_fname, se.staff_lname as staff_lname, s.school_name as school_name, se.staff_telephone as staff_telephone, u.email as email');
        $this->db->from("short_listed_candidates slc, users u, staff_enrolement se, school s");
        $this->db->where("slc.school_id = '$school_id' AND slc.is_deleted = '0' AND se.user_id = slc.user_id AND u.id = slc.user_id AND s.school_id = se.school_id $gg");
		$this->db->order_by("slc.id", "desc"); 
        $query = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $query->num_rows();

}

function get_single($id) {
$this->db->select('*');
$this->db->from($this->tbl);
$this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
$this->db->where('users.id', $id);
$query = $this->db->get();
$result = $query->result();
return $result;
}

function get_user($id) {
$this->db->select('*');
$this->db->from($this->users);
$this->db->where('id', $id);
$query = $this->db->get();
$result = $query->row();
return $result;
}

function get_singlerecord($id) {
	$this->db->select('*');
$this->db->from($this->tbl);
 $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
$this->db->where('users.id', $id);
$query = $this->db->get();
return $query->row(); 
}	

function get_single_staff_access($id) {
        $this->db->select('*');
        $this->db->from($this->staff_access);
		$this->db->where(array("user_id="=>$id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	


	
function get_branches($school_id) {
	$this->db->select('*');
	$this->db->from($this->branch);
	$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
	$query = $this->db->get();
	$this->db->last_query(); 
	return $query->result_array();
	}
	
function get_classes($school_id, $branch_id) {
	$this->db->select('*');
    $this->db->from($this->classes);
	$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
	$this->db->where_in('branch_id', $branch_id); 
	$query = $this->db->get();
	$this->db->last_query();
    return $query->result();
}
	
function getsubjects($school_id, $branch_id, $class_id) {
	$this->db->select('*');
    $this->db->from($this->subjects);
	$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
	$this->db->where_in('branch_id', $branch_id); 
	$this->db->where_in('class_id', $class_id); 
	$query = $this->db->get();
	$this->db->last_query(); 
    return $query->result();
}
	


function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getsingleClassName($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where('class_id', $class_id);
        $query = $this->db->get();
        return  $query->row();
    }

function getclassName($class) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where_in('class_id', $class);
        $query = $this->db->get();
       return $query->result();
    }

function getsubjectName($subject) {
        $this->db->select('*');
        $this->db->from($this->subjects);
        $this->db->where_in('subject_id', $subject);
        $query = $this->db->get();
       return $query->result();
    }

function getteacherrole($role) {
        $this->db->select('*');
        $this->db->from($this->teacher_role);
        $this->db->where('id', $role);
        $query = $this->db->get();
        return  $query->row();
    }
	
function getteacherlevel($level) {
        $this->db->select('*');
        $this->db->from($this->teacher_level);
        $this->db->where('id', $level);
        $query = $this->db->get();
        return  $query->row();
    }

function getteachertype($type) {
        $this->db->select('*');
        $this->db->from($this->teacher_type);
        $this->db->where('id', $type);
        $query = $this->db->get();
        return  $query->row();
    }
	



	/******************** chnages ********************************/
	function teacher_role($school_id) {
		$this->db->select('*');
        $this->db->from($this->teacher_role);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0',"is_active="=>'1'));
		$query = $this->db->get();
	//	echo $this->db->last_query(); exit();
        return $query->result();
	}
	
	function teacher_type($school_id) {
	 $this->db->select('*');
        $this->db->from($this->teacher_type);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0',"is_active="=>'1'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
         return $query->result();
	}
	
	function teacher_level($school_id) {
	 $this->db->select('*');
        $this->db->from($this->teacher_level);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0',"is_active="=>'1'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
       return $query->result();
	}
		function getdatastaff( $staff_id) {
        $this->db->select('*');
        $this->db->from($this->users);
  		$this->db->where('id', $staff_id);
        $query = $this->db->get();
        return  $query->row();
	}
	
	function get_school_name($school_id) {
          $this->db->select('*');
          $this->db->from($this->school);
		  $this->db->where('school_id', $school_id);
          $query = $this->db->get();
          return  $query->row();
    }
	
	function getcurrencies() {
        $this->db->select('*');
        $this->db->from($this->currency);
		$query = $this->db->get();
        return  $query->result();
    }
	
/***************************************** EXCEL FUNCTION **************************************************/

	
	
	function getexcel_className($class_name) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where('class_id', $class_name);
        $query = $this->db->get();
       return $query->row();
    }
	
	function getexceldata($where, $school_id,$gg,$user_srch) {
		
	 	$this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
		$this->db->join('staff_access', 'staff_access.user_id = '.$this->tbl.'.user_id','LEFT' );
		$this->db->where(array($this->tbl.'.school_id'=>$school_id,"users.user_type="=>'teacher',"users.is_deleted="=>'0'));
		if($where!=''){
		$this->db->where($gg, null, false);
		$this->db->or_where("CONCAT(staff_fname, ' ', staff_lname) LIKE '%".$user_srch."%'", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".staff_id", "desc"); 
		 $query = $this->db->get();
		$rr= $this->db->last_query(); 
//	echo $rr; exit();
        $result = $query->result();
    	return $result;
	
}
		
/*************************** changes for user access system ************************************************/

 function getStaff_record_count($where, $staff_branch_id, $gg,$user_srch)
  {
         $this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
		$this->db->where(array("users.user_type="=>'teacher',"users.is_deleted="=>'0') );
		
		if($staff_branch_id !=''){
		$this->db->where_in($this->tbl.'.branch_id',explode(',',$staff_branch_id));
		}
		if($where!=''){
		//$this->db->where($gg, null, false );
		$this->db->where("CONCAT(staff_fname, ' ', staff_lname) LIKE '%".$user_srch."%'", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".staff_id", "desc");
        $query = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $query->num_rows();
    }
	


function getStaff_records($where, $staff_branch_id , $limit=NULL, $offset=NULL,$gg,$user_srch) {
	 	$this->db->limit($limit, $offset);
	 	$this->db->select('*');
        $this->db->from($this->application_bank);

		$this->db->order_by($this->application_bank.".id", "desc"); 
		 $query = $this->db->get();
		$rr= $this->db->last_query(); 
		//echo $rr; exit();
        $result = $query->result();
    return $result;
	
}


function delete($id){
		$where		= array('id' => $id);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->short_listed_candidates, $data)){
			return true;
		 }else{
			return false;
			}
	}	
}
/*------- Dhrup --------*/


function get_shortlisted($id) {
	$this->db->select('*');
	$this->db->from('staff_enrolement');
	$this->db->where('user_id', $id);
	$query = $this->db->get();
	$result = $query->row();
	return $result;
}
  
function get_School_ID($user_id){
	$this->db->select('*');
	$this->db->from('users');
	$this->db->where('id', $user_id);
	$query = $this->db->get();
	$result = $query->row();
	return $result;
}
function getUser($user_id){
	$this->db->select('*');
	 $this->db->from($this->users);
	$this->db->where('id', $user_id);
	$query = $this->db->get();
	return $query->row();
}


/*------- Dhrup --------*/