<?php
 if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 class Teachertypemodel extends CI_Model {
	
	     var $tbl = "teacher_type";
	     var $branch = "branch";
	     var $staff = 'staff';
		 var $school = 'school';

         function __construct() {
         parent::__construct();
    }
	
	     function get_branch3($school_id){            //this function selects branch for teacher level page
	     $this->db->select('*');
         $this->db->from($this->branch);
		 $this->db->where(array("school_id"=>$school_id,"is_active"=>'1',"is_deleted"=>'0'));
		 $query = $this->db->get();
		 $last_qry = $this->db->last_query(); 
         $branch = $query->result_array();
         return $branch;
	
    }
	
	    function insert_teach_type($typedata){          //this function inserts data into select level  for teacher level page
		if($this->db->insert($this->tbl, $typedata))
		return $this->db->insert_id();
	    else
		return false;		
	}
	
/*	    function get_teach_type($where, $school_id, $limit=NULL, $offset=NULL, $type_search){  	
		$this->db->limit($limit, $offset);  		                     
	    $this->db->select('*');                                                      //this function is used for listing or to fetch data from database for teacher level page
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', $this->branch.".is_deleted"=>'0', $this->branch.".is_active"=>'1'));
	    if($where != NULL){
			$this->db->where($where);
		}
		if($type_search != NULL){
			$this->db->where("($this->tbl.teacher_type LIKE '%$type_search%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc");
	    $query = $this->db->get();
	   $this->db->last_query(); 
       return $query->result_array();
}
	*/
/*		function check_teach_type($branch, $type, $school_id){         //this function checks duplicate select level for teacher level page
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"teacher_type"=>$type));
		$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();   
	}*/
    
	    function edit_type_record($id) {
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where('id', $id);
		$query = $this->db->get();
		$this->db->last_query(); 
		return $query->result();
	}

	   public function get_contents() {
              $this->db->select('*');
              $this->db->from('teacher_type');
              $query = $this->db->get();
              return $result = $query->result();
          }
	
	    function get_single_record($id) {
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where('id', $id);
		$query = $this->db->get();
	    $this->db->last_query(); 
		return $query->result_array();
	}
/*	    function check_update_teach_type($branch, $type, $school_id, $id){
	        $this->db->select('*');
            $this->db->from($this->tbl);
		    $this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"teacher_type"=>$type));
		    $this->db->where('id !=', $id);
		    $query = $this->db->get();
            $this->db->last_query();
            return $query->result();   
	  }*/

	    function update_teacher_type($id,$data){
            $this->db->where('id', $id);
            $this->db->update($this->tbl, $data);
      }
	  
	    function teacher_action($id,$data){
            $this->db->where('id', $id);
            $this->db->update($this->tbl, $data); 
      }
/*	function totaltypes($where=NULL, $school_id, $type_search){
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', $this->branch.".is_deleted"=>'0', $this->branch.".is_active"=>'1'));
	    if($where != NULL){
		$this->db->where($where);
		}
		if($type_search != NULL){
			$this->db->where("($this->tbl.teacher_type LIKE '%$type_search%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		$query = $this->db->get(); 
	    $this->db->last_query();
        return $query->result_array();
	}*/
/*	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get(); 
        return  $query->row();
    }*/
	function get_deltype($school_id, $id) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where(array("school_id"=>$school_id, "id"=>$id));
         $query = $this->db->get(); 
		  $this->db->last_query(); 
		  return  $query->row();
      }
	
/*	function check_staffassigntype($teachertype_id, $school_id, $branch_id){
	 $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("school_id"=>$school_id,"staff_teacher_type"=>$teachertype_id,"branch_id"=>$branch_id));
		  $query = $this->db->get();
		 $this->db->last_query(); 
		 return $query->num_rows(); 
	}
	*/
	function getteachertype( $techertype_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
  		$this->db->where('id', $techertype_id);
        $query = $this->db->get();
        return  $query->row();
	}
	
	
	/* ****************** changes in teacher_type ***************************** */
	function getschoolName($school_id) {
        $this->db->select('*');
        $this->db->from($this->school);
        $this->db->where('school_id', $school_id);
        $query = $this->db->get(); 
        return  $query->row();
    }
	
	function check_teach_type($type, $school_id){         //this function checks duplicate select level for teacher level page
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"teacher_type"=>$type,"is_deleted"=>'0'));
		$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();   
	}
	
   function get_teach_type($where, $school_id, $limit=NULL, $offset=NULL, $type_search){  	
		$this->db->limit($limit, $offset);  		                     
	    $this->db->select('*');                                                      //this function is used for listing or to fetch data from database for teacher level page
        $this->db->from($this->tbl);
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0'));
	    if($where != NULL){
			$this->db->where($where);
		}
		if($type_search != NULL){
			$this->db->where("($this->tbl.teacher_type LIKE '%$type_search%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc");
	    $query = $this->db->get();
	   $this->db->last_query(); 
       return $query->result_array();
}
	function check_staffassigntype($teachertype_id, $school_id){
		 $this->db->select('*');
         $this->db->from($this->staff);
		 $this->db->where(array("school_id"=>$school_id,"staff_teacher_type"=>$teachertype_id));
		 $query = $this->db->get();
		 $this->db->last_query(); 
		 return $query->num_rows(); 
	}
	function totaltypes($where=NULL, $school_id, $type_search){
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0'));
	    if($where != NULL){
		$this->db->where($where);
		}
		if($type_search != NULL){
			$this->db->where("($this->tbl.teacher_type LIKE '%$type_search%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		$query = $this->db->get(); 
	    $this->db->last_query();
        return $query->result_array();
	}
	
    function check_update_teach_type($type, $school_id, $id){
	        $this->db->select('*');
            $this->db->from($this->tbl);
		    $this->db->where(array("school_id"=>$school_id,"teacher_type"=>$type));
		    $this->db->where('id !=', $id);
		    $query = $this->db->get();
            $this->db->last_query();
            return $query->result();   
	  }
 }