<?php
 if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 class Master_model_type extends CI_Model {
	
	     var $tbl = "teacher_type";
	     var $branch = "branch";
	     var $staff = 'staff';

         function __construct() {
         parent::__construct();
    }
	
	     function get_branch3($school_id){            //this function selects branch for teacher level page
	     $this->db->select('*');
         $this->db->from($this->branch);
		 $this->db->where(array("school_id"=>$school_id,"is_active"=>'1',"is_deleted"=>'0'));
		 $query = $this->db->get();
		 $last_qry = $this->db->last_query(); 
         $branch = $query->result_array();
         return $branch;
	
    }
	function get_types($where=NULL, $school_id, $limit=NULL, $offset=NULL){
	    $this->db->limit($limit, $offset);
	    $this->db->select('*');
        $this->db->from($this->tbl);
/*		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );*/
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', ));
		if($where != NULL){
		$this->db->where($where);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		 $query = $this->db->get(); 
		$this->db->last_query(); 
        return $query->result_array();
	}
	
	    function insert_teach_type($typedata){          //this function inserts data into select level  for teacher level page
		if($this->db->insert($this->tbl, $typedata))
		return $this->db->insert_id();
	    else
		return false;		
	}
	
	    function get_teach_type($where, $school_id, $type_search, $limit=NULL, $offset=NULL){  	
		$this->db->limit($limit, $offset);  		                     
	    $this->db->select('*');                                                      //this function is used for listing or to fetch data from database for teacher level page
        $this->db->from($this->tbl);
		/*$this->db->join($this->branch , 'branch.branch_id = teacher_type.branch_id','INNER' );*/
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', ));
		
		if($where != NULL){
		$this->db->where($where);
		}
		
		$this->db->order_by($this->tbl.".id", "desc");
		
	    $query = $this->db->get();
		$last_qry = $this->db->last_query(); 
        $teachtype = $query->result_array();
        return $teachtype;
    }
	
		function check_teach_type($branch, $type, $school_id){         //this function checks duplicate select level for teacher level page
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"teacher_type"=>$type));
		$query = $this->db->get();
		$last_qry = $this->db->last_query(); 
        return $query->result();   
	}
    
	    function total_teach_type($type_search, $school_id){            // this function shows all teacher level records i.e used for pagination
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', ));
		
		if($type_search!=''){
		$this->db->where($this->tbl.'.teacher_type',$type_search);
		}
		
		$this->db->order_by($this->tbl.".id", "desc"); 
		$query = $this->db->get(); 
		$rr= $this->db->last_query(); 
        return $query->result_array();
	}
	
	    function edit_type_record($id) {
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where('id', $id);
		$query = $this->db->get();
		$this->db->last_query(); 
		return $query->result();
	}

	   public function get_contents() {
              $this->db->select('*');
              $this->db->from('teacher_type');
              $query = $this->db->get();
              return $result = $query->result();
          }
		
      //  public function entry_update( $id ) {
//
//            $this->db->select('*');
//            $this->db->from('teacher_type');
//            $this->db->where('id',$id );
//            $query = $this->db->get();
//            return  $query->row_array();
//
//        }
//		
//        public function entry_update1($id) {   
//            $this->db->where('id', $id);
//            $this->db->update('teacher_type', $data);
//		}
			
	    function get_single_record($id) {
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where('id', $id);
		$query = $this->db->get();
	    $this->db->last_query(); 
		return $query->result_array();
	}
	    function check_update_teach_type($branch, $type, $school_id, $id){
	        $this->db->select('*');
            $this->db->from($this->tbl);
		    $this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"teacher_type"=>$type));
		    $this->db->where('id !=', $id);
		    $query = $this->db->get();
            $this->db->last_query(); 
            return $query->result();   
	  }

	    function update_teacher_type($id,$data){
            $this->db->where('id', $id);
            $this->db->update($this->tbl, $data);
      }
	  
	    function teacher_action($id,$data){
            $this->db->where('id', $id);
            $this->db->update($this->tbl, $data); 
      }
	function totaltypes($where=NULL, $school_id){
	    $this->db->select('*');
        $this->db->from($this->tbl);
	/*	$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );*/
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', ));
	    if($where != NULL){
		$this->db->where($where);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		$query = $this->db->get(); 
		$this->db->last_query(); 
        return $query->result_array();
	}
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get(); 
        return  $query->row();
    }
	function get_deltype($school_id,$id) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where(array("school_id"=>$school_id,"id"=>$id));
          $query = $this->db->get();
          return  $query->row();
      }
	
	function check_staffassigntype($teacher_type, $school_id, $branch_id){
	 $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("school_id"=>$school_id,"staff_teacher_type"=>$teacher_type,"branch_id"=>$branch_id));
		  $query = $this->db->get();
		echo $this->db->last_query(); 
		 return $query->num_rows(); 
       /* return $query->result();*/   
	}
	
 }