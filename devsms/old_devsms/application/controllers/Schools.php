<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schools extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','main_schools_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
    }

	public function index()

	{
		$status_search					= isset($_GET['status'])?$_GET['status']:NULL; 
		$shortbyname_search				= isset($_GET['shortbyname'])?$_GET['shortbyname']:NULL;
		$word_search				    = isset($_GET['seachword'])?$_GET['seachword']:NULL;
		$PerPage				   		= isset($_POST['perpage'])?$_POST['perpage']:NULL;
		
		$search_condition['is_deleted']	= '0';
		if($status_search!=''){
            $search_condition['is_active']	= $status_search;
		  }
		 
		$data							= array();
		$school_id		= $this->session->userdata('user_school_id');
		
		$data['title']	      			= "Schools";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 2;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'schools/index';
	    $config['total_rows'] 			= $this->main_schools_model->getRows($search_condition,$word_search);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = "/?&status=$status_search&shortbyname=$shortbyname_search&seachword=$word_search"; 
		
		$this->pagination->initialize($config);
		
		if($shortbyname_search!='') {
			 $odr =   "school_name";
			 if($shortbyname_search=='0') {
				  $dirc	= "asc";
			  } elseif($shortbyname_search=='1') {
				  $dirc	= "desc";
			     }
		 } else {
			 $odr =   "school_id";
			 $dirc	= "desc";
		   }
		
		$data_rows= $this->main_schools_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$word_search);
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['status_search']			= $status_search;
		$data['shortbyname_search']		= $shortbyname_search;
		$data['word_search']		    = $word_search;
		$data['PerPage']				= $PerPage;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		 
		$this->load->view('header');

		$this->load->view('school/schools', $data);

		$this->load->view('footer');

	}
	
	function deactivate(){
		  
		  $start						    = $this->uri->segment(3);
		  $schoolid						    = $this->uri->segment(4);
		  
		$where		= array('school_id'=>$schoolid);
		$data		= array('is_active'=>'0');
		if($this->main_schools_model->update($data,$where)){
			$this->session->set_flashdata('success', 'School has been updated successfuly.');
			redirect(base_url()."schools/index/$start/$schoolid");exit;
		 } else {
			$this->session->set_flashdata('error', 'Some problem exists. School has not been updated.');
			redirect(base_url()."schools/index/$start/$schoolid");exit;
		}
		
	}
	function activate(){
		
		 $start						    = $this->uri->segment(3);
		 $schoolid					    = $this->uri->segment(4);
		  
		$where		= array('school_id'=>$schoolid);
		$data		= array('is_active'=>'1');
		if($this->main_schools_model->update($data,$where)){
			$this->session->set_flashdata('success', 'School has been updated successfuly.');
			redirect(base_url()."schools/index/$start/$schoolid");exit;
		 } else {
			$this->session->set_flashdata('error', 'Some problem exists. School has not been updated.');
			redirect(base_url()."schools/index/$start/$schoolid");exit;
		}
		
	}
	
	public function deleteschool($schoolid){
		if($this->main_schools_model->delete($schoolid)){
			$this->session->set_flashdata('success', 'School has been deleted successfuly.');
			redirect(base_url()."schools/index");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Try again.');
			redirect(base_url()."schools/index");exit;
		}
	}
	
	
	 public function view_school($schoolid) {
		
		if($schoolid==NULL){
			$this->session->set_flashdata('error', 'Select school first.');
			redirect(base_url()."schools/index");exit;
		   }
		   
		$data = array();
		$data_rows						= $this->main_schools_model->get_single_school($schoolid);
		$data['info']					= $data_rows[0];
		$data['title']	      			= "School detail";
		$data['error']					= $this->session->flashdata('error');
		$data['logmode']				= $this->logmode;
		$data['success']				= $this->session->flashdata('success');
	     
		$this->load->view('header');
        $this->load->view('school/school_view',$data);
	    $this->load->view('footer');
    }
	
		
		public function editschool($schoolid){
		
			if($schoolid==NULL){
				$this->session->set_flashdata('error', 'Select school first.');
				redirect(base_url()."schools/index");exit;
			   }
		   
		 $data = array();
		 $school_id = $this->session->userdata('user_school_id');
		
		$data_rows						= $this->main_schools_model->get_single_school($schoolid);
		$data['info']					= $data_rows[0];
		$data['title']	      			= "Edit School";
		$data['error']					= $this->session->flashdata('error');
		$data['logmode']				= $this->logmode;
		$data['success']				= $this->session->flashdata('success');
		
		$this->load->view('header');
        $this->load->view('school/school_edit',$data);
	    $this->load->view('footer');
		
	}
	
	public function updateschool($schoolid){
		
		$postURL	= $this->createPostURL($_GET);
		 if($schoolid==NULL){
			$this->session->set_flashdata('error', 'Select school first.');
			redirect(base_url()."schools/index");exit;
		   }
		  
		  $userdata 						= array();
		  $schooldata 					    = array();						
		  $data							    = array();
	  
	 
	    $whereuser						= array('school_id'=>$schoolid);
		$whereschool					= array('school_id'=>$schoolid);
			
			if ($this->input->post('schoolname') != '') {
                $schooldata['school_name'] = trim($this->input->post('schoolname'));
				$schoolname = trim($this->input->post('schoolname'));
            }
			
			if ($this->input->post('telephone') != '') {
                $schooldata['school_phone'] = trim($this->input->post('telephone'));
            }
			
			if ($this->input->post('address') != '') {
                $schooldata['school_address'] = trim($this->input->post('address'));
            }
			
			if ($this->input->post('contact_person') != '') {
                $schooldata['school_contact_person'] = trim($this->input->post('contact_person'));
            }
			
			if ($this->input->post('email') != '') {
                $schooldata['school_email'] = trim($this->input->post('email'));
            }
			
			if ($this->input->post('password') != '') {
                $userdata['password'] = trim($this->input->post('password'));
            }
			
			if ($this->input->post('email') != '') {
                $userdata['email'] = trim($this->input->post('email'));
            }
	 
	      $checkSchoolname = $this->main_schools_model->check_edited_schoolname($schoolid,$schoolname);
		  $countSchoolname = count($checkSchoolname);
	 
	 		if( $countSchoolname > 0 ) {
				  $this->session->set_flashdata('error', 'School name already exists. School has not been updated.');
				  redirect(base_url()."schools/editschool/$schoolid");exit;
			 } else {
	     if($this->main_schools_model->udateSchool($schooldata,$whereschool)){
			$this->main_schools_model->updateUser($userdata,$whereuser);
			$this->session->set_flashdata('success', 'School has been updated successfuly.');
			redirect(base_url()."schools/index/");exit;
		  } else {
			$this->session->set_flashdata('error', 'Some problem exists. School has not been updated.');
			redirect(base_url()."schools/editschool/$schoolid");exit;
		    }
		 }
		   
	  } 
	
       function get_school_info() {
             $school_id =  $_POST['schoolId'];
		     $schoolInfo = $this->main_schools_model->get_school_name($school_id);
			 echo $schoolInfo->school_name;
    }
	
	function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}

}

