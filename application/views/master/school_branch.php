<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>
    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li class="edit">Branch</li>        

        </ul>

        </div>

        </div>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    	
                <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>

        <div class="attendancesec">	
        
        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">
             
             <?php 
		$editedBranchid = $this->uri->segment(4); 
		$Action = $this->uri->segment(5); 
		if($Action == 'edit') {
		?>

<?php 
$branch_time = $info->branch_timing;
if($branch_time!=''){
list($start_time,$end_time) = explode("-", $branch_time);
 $start_time."<br>";
 $end_time."<br>";
}else{
    $start_time=' ';
	$end_time=' ';
}
?>
<form action="<?php echo base_url();?>schoolbranch/updatebranch/<?php echo $last_page;?>/<?php echo $editedBranchid;?>" name="schoolbarchForm" id="schoolbarchForm" method="post">
<div class="col-sm-12 col-xs-12 applycodediv nopadding">

    <div class="form-group fullwidthinput">
    
    	<div class="col-sm-4 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Branch:</label>
        </div>
  
        <div class="col-sm-9 inputbox termselect">
        <input type="text" name="branchname" id="branchname" value="<?php echo $info->branch_name;?>"  placeholder="Enter branch name" class="form-control" maxlength="61"   autocomplete="off" >
        </div>
        </div>
        
        
        <div class="col-sm-6 nopadding">
        <div class="col-sm-2 nopadding" style="margin:0 5px 0 20px;">
        <label class="control-label nopadding filterlabel">Branch Timing</label>
        </div>
         
    	<div class="col-sm-9 inputbox termselect">
        <div id="datetimepicker7" class="col-sm-5 start_time input-append">
        <input type="text" data-format="hh:mm" name="start_timing" id="start_timing" value="<?php echo $start_time;?>" placeholder="Start Time" class="form-control timepicker" maxlength="41" autocomplete="off" onKeyUp="this.value=this.value.replace(/[^0-9]/g,'')"  >
   		</div>
        <div class="col-sm-1 arrow_dash">
             <span class="arrow">-</span>
        </div>
        <div id="datetimepicker8" class="col-sm-5 end_time input-append">
        <input type="text" data-format="hh:mm" name="end_timing" id="end_timing" value="<?php echo $end_time;?>" placeholder="Start Time" class="form-control timepicker2" maxlength="41" autocomplete="off" onKeyUp="this.value=this.value.replace(/[^0-9]/g,'')" >
   		</div>
        </div>
        </div>
        <div class="col-sm-2 nopadding">
        <div class="col-sm-12 confirmlink">
        <input type="submit" class="open1" value="Update">
        </div>
        </div>
	</div>
    
</div>
<div class="col-sm-12 col-xs-12 applycodediv nopadding">

    <div class="form-group fullwidthinput">
    
    	<div class="col-sm-4 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Address</label>
        </div>
  
        <div class="col-sm-9 inputbox termselect">
        <textarea  class="form-control" name="branchaddress" id="branchaddress" placeholder="Mention Address"  style="resize:none; max-height:35px;" cols="num" rows="num"         autocomplete="off" maxlenght="251" ><?php echo $info->branch_address;?> </textarea>
        </div>
        </div>
        
        
        <div class="col-sm-7 nopadding">
        <div class="col-sm-2 nopadding" style="margin:0 5px 0 20px;">
        <label class="control-label nopadding filterlabel">Branch Days</label>
        </div>
         <?php $weekdaysArr = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat"); 
         $branch_days_Arr  = explode(',', $info->branch_days);
		 ?>
    	<div class="col-sm-9 inputbox termselect">
   		<?php
		 foreach($weekdaysArr as $weekday) { 
		 ?>
        <div class="checkboxdiv profilecheckbox checkbox_cls days_check">
   		<input type="checkbox" name="branch_days[]" id="week_day_<?php echo $weekday;?>" value="<?php echo $weekday;?>" <?php if(in_array($weekday, $branch_days_Arr)){ echo 'checked'; }?>>
       <label for="week_day_<?php echo $weekday;?>"><span class="checkbox cls_day days_check_box"><?php echo $weekday;?></span></label>
       </div>
       <?php }?>
   		</div>
        </div>
	</div>
    
</div>


    </form>      
    
  <?php } else { ?>

   <form action="<?php echo base_url();?>schoolbranch/addbranch" name="schoolbarchForm" id="schoolbarchForm" method="post" autocomplete="off">


<div class="col-sm-12 col-xs-12 applycodediv nopadding">

    <div class="form-group fullwidthinput">
    
    	<div class="col-sm-4 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Branch:</label>
        </div>
  
        <div class="col-sm-9 inputbox termselect">
        <input type="text" name="branchname" id="branchname" value=""  placeholder="Enter branch name" class="form-control" maxlength="61" autocomplete="off" >
        </div>
        </div>
        
        
        <div class="col-sm-6 nopadding">
        <div class="col-sm-2 nopadding" style="margin:0 5px 0 20px;">
        <label class="control-label nopadding filterlabel">Branch Timing:</label>
        </div>
         
    	<div class="col-sm-9 inputbox termselect">
        <div id="datetimepicker7" class="col-sm-5 start_time input-append">
   		<input type="text" data-format="hh:mm" name="start_timing" id="start_timing" value="" placeholder="Start Time" class="form-control timepicker" maxlength="41" autocomplete="off" onKeyUp="this.value=this.value.replace(/[^0-9]/g,'')"  >
        
   		</div>
        <div class="col-sm-1 arrow_dash">
             <span class="arrow">-</span>
        </div>
        <div id="datetimepicker8" class="col-sm-5 end_time input-append">
   		<input type="text" data-format="hh:mm" name="end_timing" id="end_timing" value="" placeholder="End Time" class="form-control timepicker2" maxlength="41" autocomplete="off" onKeyUp="this.value=this.value.replace(/[^0-9]/g,'')" >
   		</div>
        </div>
        </div>
        
        <div class="col-sm-2 nopadding">
        <div class="col-sm-12 confirmlink">
        <input type="submit" class="open1" value="Add">
        </div>
        </div>
	</div>
    
</div>
<div class="col-sm-12 col-xs-12 applycodediv nopadding">

    <div class="form-group fullwidthinput">
    
    	<div class="col-sm-4 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Address:</label>
        </div>
  
        <div class="col-sm-9 inputbox termselect">
       <textarea  class="form-control" name="branchaddress" id="branchaddress" placeholder="Mention Address"  style="resize:none; max-height:35px;" cols="num" rows="num"         autocomplete="off" maxlenght="251" > </textarea>
        </div>
        </div>
        
        
        <div class="col-sm-7 nopadding">
        <div class="col-sm-2 nopadding" style="margin:0 5px 0 20px;">
        <label class="control-label nopadding filterlabel">Branch Days:</label>
        </div>
     <?php $weekdaysArr = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat"); ?>
    	<div class="col-sm-9 inputbox termselect">
        <?php foreach($weekdaysArr as $weekday) { ?>
        <div class="checkboxdiv profilecheckbox checkbox_cls days_check">
   		<input type="checkbox" name="branch_days[]" id="week_day_<?php echo $weekday;?>" value="<?php echo $weekday;?>" >
       <label for="week_day_<?php echo $weekday;?>"><span class="checkbox cls_day days_check_box"><?php echo $weekday;?></span></label>
       </div>
       <?php }?>
   		</div>
        </div>
        
	</div>
    
</div>

    </form>
        
    <?php } ?>
  

        </div>

    </div>
        

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

  <form action="<?php echo base_url();?>schoolbranch/index" name="branchfilterform" id="branchfilterform">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding">

				<div class="col-sm-7 nopadding selectoption">

<div class="form-group fullwidthinput">

    <label class="col-sm-1 col-xs-1 control-label nopadding filterlabel">Show:</label>

    <div class="col-sm-11 col-xs-11 nopadding selectfilter">

    <div class="col-sm-3 col-xs-3 inputbox termselect">

<select class="form-control" name="status" id="status">
  <option value="">Select Status </option>    
   <option <?php if($status_search == "1"){ echo 'selected'; } ?> value="1">Active</option>
   <option <?php if($status_search == "0"){ echo 'selected'; } ?> value="0">Inactive</option>
</select>

    </div>

    </div>

  </div>

</div>		

<div class="col-sm-3 col-xs-5 searching termsearch nopadding">

        <div class="form-group">

      <input type="text" class="form-control searchbox" placeholder="Search Here" name="seachword" id="seachword" value="<?php echo $word_search;?>">

   <!--  <input type="submit" value="" class="searchbtn">-->

     </div>

        </div>

        <div class="col-sm-2 col-xs-7 rightspace viewreport">

<div class="form-group">

    <input type="submit" class="btn btn-danger srchbtn" value="Find Branch">

   </div>

</div>

        </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <h1>Branches<span><?php echo $Totalrec = $total_rows;?></span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2">S.No.</th>

					  <th class="column5"> Branch </th>
                        
					  <th class="column5">Address</th>

                     <th class="column4">Status</th>
                     
                      <th class="column4">Action</th>

				  </tr>

			  </thead>

				<tbody>

					<?php 
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $branches) { 
					     $sr++;
						 
					?>
					<tr class="familydata students">
						<td class="column2"><?php echo $sr; ?></td>
                        <td class="column5">
						<?php 
							if($branches->branch_name!=''){						
								echo $branches->branch_name;
							}else{
								echo '<span style="color:transparent;">-</span>'; 
								}
						?> </td>
                        <td class="column5"><?php 
							if($branches->branch_address!=''){						
								echo $branches->branch_address;
								}else{
								echo '<span style="color:transparent;">-</span>';
								 }
						?></td>
                        <td class="column4"><span class="acceptbtn statusbtn <?php if($branches->is_active==1) { echo 'Active'; } else { echo 'Inactive'; } ?>"><?php if($branches->is_active==1) { echo 'Active'; } else { echo 'Inactive'; } ?></span></td>
                        
                        
                        <td class="column4">
											<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
                                                
 <!-- <li> <a href="<?php echo base_url();?>students/view_student/<?php echo $branches->id;?>"> View  </a> </li>-->                                    
	      <li><a href="<?=base_url();?>schoolbranch/index/<?php echo $last_page;?>/<?php echo $branches->branch_id;?>/edit<?php echo $post_url;?>" target="_blank"> Edit </a></li>
          
				<li>
				<?php if($branches->is_active == '1'){ ?>
	<a href="<?php echo base_url();?>schoolbranch/deactivatebranch/<?php echo $last_page;?>/<?php echo $branches->branch_id;?><?php echo $post_url;?>" title="click to deactivate branch"> Active </a>
					<?php
					 } else {
					?>
	<a href="<?=base_url();?>schoolbranch/activatebranch/<?php echo $last_page;?>/<?php echo $branches->branch_id;?><?php echo $post_url;?>" title="click to activate branch">De-Activated </a>
				<?php } ?>
				</li>
                
			<li class="divider"></li>
				 <li><a onclick="delConfirm('<?php echo $branches->branch_id;?>')" style="cursor: pointer;"> Delete </a> </li>
									</ul>
								</div>
											
							</td>
                                        
                                        
                        </tr>
				<?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px; height: 100px;font-size:25px ; background:#FFF;  color: #6a7a91;">No record to show.</th></tr>	
				   <?php } ?>

				</tbody>

		  </table>

     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv">

    <div class="col-sm-9 col-xs-6 paginationblk">
         <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			<p class="showp">Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries</p>
			<?php  } ?>
	<ul class="pagination">
		
		<?php echo $pagination;?>
        

	</ul>

    </div>

    <div class="col-sm-3 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total:&nbsp;<?php echo $Totalrec ;?></h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>schoolbranch/index" method="get">
       
          <input type="hidden" name="status" id="status" value="<?php echo $status_search; ?>"  />
       
          <input type="hidden" name="seachword" id="seachword" value="<?php echo $word_search; ?>"  />  

          <select class="form-control" name="perpage" id="perpage">
        
          <option value="">Select</option>
        
          <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>
        
          <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>
        
          <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>
        
          <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>
        
          <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>
        
         </select>

</form>



        </div>

    </div>

	</div>

    </div>     

</div>

	</div>
   

	</div>

    </div>

    </div>
    
        
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
	
 
 <script>
  jQuery(document).ready(function(){
	  
	  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
					
	
  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });
/*jQuery.validator.addMethod("time", function(value, element) {  
return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);  
}, "Please enter a valid time.");
*/	
    jQuery("#schoolbarchForm").validate({
        rules: {
			   branchname: {
                   required: true,
                   alphanumeric: true,
				   maxlength: 61
				  // lettersonly: true,
				 
               },

			   branchaddress: {
                  required: true
               }
			  
        },
        messages: {
			  branchname: {
                  required: "Please Enter Branch Name.",
				  alphanumeric: "Letters and numbers only please.",
				  maxlength: "Maximum 60 characters allowed."
				  },
			  branchaddress: {
                  required: "Please Enter Description"
                 }

			},
        submitHandler: function(form) {
            form.submit();
          }
        });
	
jQuery( "#start_timing" ).timepicker({
	interval: 60,
	'minTime': '6:00am',
    'maxTime': '7:00pm',
	 change: function(time) {
               $('#end_timing').timepicker('option', 'minTime', $(this).val() + (1 * 60 * 60 * 1000)); 
              }
});

jQuery( "#end_timing" ).timepicker({
	interval: 60,
    'maxTime': '11:30pm',
    'showDuration': true
});
  $('#start_timing').attr('readonly', true);

$('#end_timing').attr('readonly', true);
});
</script>
 
  
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>schoolbranch/deletebranch/"+id;
		}else{
			return false;
		}
	}
</script>	

<!--<script type="text/javascript">
$('.timepicker').timepicker({
    timeFormat: 'hh:mm',
    interval: 30,
    minTime: '01',
    maxTime: '11:00am',
    defaultTime: ' ',
    startTime: '01:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});
$('.timepicker2').timepicker({
    timeFormat: 'hh:mm',
    interval: 30,
    minTime: '12',
    maxTime: '11:00pm',
    defaultTime: ' ',
    startTime: '01:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});
</script>-->	
<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.fullwidthinput select {
    background-position: 95% center;
	padding:0;
}
.Inactive {
    background: #ff0000 none repeat scroll 0 0;
}
<!--changes-->
.showp {
    padding-top: 20px;
}
.pagination{
	margin:0px;
	padding-left:180px;}
	
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px;
}
.srchbtn{
   padding: 6px 30px;
}
.open1{
	margin:0px !important;
	margin-bottom:20px !important;
}
@media screen and (min-width:320px) and (max-width:480px){
.cleanSearchFilter {
    padding: 8px 5px !important;
}
.termsearch {
    padding-bottom: 10px !important;
}
.searching {
    width: 100% !important;
}
.paginationblk {
    text-align: center !important;
}
}
@media screen and (min-width:481px) and (max-width:767px){ 
.selectoption {
    width: 100%;
}
.fullwidthinput label {
    width: auto;
}
.inputbox.termselect {
    width: 100%;
}
#status {
    margin-bottom: 10px;
}
.inputbox.termselect {
    width: 100%;
}
}
@media screen and (min-width:768px) and (max-width:991px){
.fullwidthinput select {
    font-size: 14px;
    margin-bottom: 10px;
}
.inputbox.termselect {
    width: 100%;
}
.fullwidthinput label {
    width: auto;
}
}
@media screen and (min-width:992px) and (max-width:1280px){
.statusbtn {
    font-size: 10px;
    padding: 7px 10px;
}
#branchfilterform .inputbox.termselect {
    padding: 0 10px;
    width: 50%;
}
.cleanSearchFilter {
    padding: 10px 2px 10px 1px;
}
.searching {
    width: 24%;
}
.btn {
    padding: 7px 8px;
}
#branchfilterform .selectoption {
    width: 50%;
}
.totalstudent {
    padding: 0 15px 0 0;
    text-align: left;
}
}
.arrow_dash {
    text-align: center;
}
.arrow_dash span {
    font-size: 30px;
    line-height: 30px;
}
.profile-bg.filterbox {
    padding: 20px 15px 15px 15px !important;
}
.checkboxdiv{ padding:0px 16px 0px 22px !important; }
.checkboxdiv.checkbox_cls days_check{padding-top:2px;}
.checkboxdiv span.checkbox::before{
	left:-18px !important;
}
.checkboxdiv span.checkbox::after{
	left:-21px !important;
}
.checkboxdiv.profilecheckbox{ margin:0px !important}
</style>	
