<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

          <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li class="edit">Report Template</li>        

        </ul>

        </div>

        </div>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    	
                <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>

        

        <div class="attendancesec">	
        
        
        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">


             <?php 
		$editedtemplateid = $this->uri->segment(4); 
		$Action = $this->uri->segment(5); 
		if($Action == 'edit') {
		?>

<form action="<?php echo base_url();?>reporttemplate/updatetemplate/<?php echo $last_page;?>/<?php echo $editedtemplateid;?>" name="reporttemplate" id="reporttemplate" method="post">
<div class="col-sm-9 col-xs-9">
<div class="col-sm-12 col-xs-12 applycodediv reportdiv nopadding">

    <div class="form-group fullwidthinput">
      <div class="col-sm-6 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Subject:</label>
        </div>
  
      <div class="col-sm-9 inputbox termselect">
       <select name="subject_name" id="subject_name">
        <option value="">Select Subject </option>  
        <option <?php if ($info->subject_name == 'Memorisation') { echo 'selected';}?> value='Memorisation'>Memorisation</option>
        <option <?php if ($info->subject_name == 'Reading') { echo 'selected';}?> value='Reading'>Reading</option>
        <option <?php if ($info->subject_name == 'Islamic Studies') { echo 'selected';}?> value='Islamic Studies'>Islamic Studies</option>
        <option <?php if ($info->subject_name == 'Arabic Language') { echo 'selected';}?> value='Arabic Language'>Arabic Language</option>
       </select>
        </div>
        </div>

      <div class="col-sm-6 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Category:</label>
        </div>
  
      <div class="col-sm-9 inputbox termselect">
       <select name="cat_name" id="cat_name1">
        <option value="">Select Category </option>  
        <option <?php if ($info->category_name == 'AoA') { echo 'selected';}?> value='AoA'>Area of Achievement</option>
        <option <?php if ($info->category_name == 'AoD') { echo 'selected';}?> value='AoD'>Area of Devepolment</option>
<?php if ($info->subject_name == 'Islamic Studies') {?>
        <option <?php if ($info->category_name == 'ToC') { echo 'selected';}?> value='ToC'>Topic Covered</option>
  <?php } ?>
      </select>
        </div>
        </div>
      </div>
    </div>


<div class="col-sm-12 col-xs-12 applycodediv reportdiv nopadding">

    <div class="form-group fullwidthinput">

      <div class="col-sm-6 nopadding">
        <div class="col-sm-4">
        <label class=" control-label nopadding filterlabel">SubCategory:</label>
        </div>
  
        <div class="col-sm-8">
            <?php 
                if($info->subcategory_id !='' && $info->subcategory_id !='0'){
                $tempsubject_name=$info->subject_name;
                $tempcat_name=$info->category_name;
                $this->load->model(array('Reporttemplate_model'));
                $subcategorys_name =$this->Reporttemplate_model->get_subcategory($tempsubject_name, $tempcat_name);

            ?>
            <select class="form-control" name="subcat_name" id="subcat_name">
              <?php foreach($subcategorys_name as $subcategory_name){ ?>
             <option <?php if ($info->subcategory_id == $subcategory_name->dropdown_id){ echo 'selected';}?> value= '<?php echo $subcategory_name->dropdown_id ?>' ><?php echo $subcategory_name->dropdown_name; ?></option>
              <?php } ?>
          </select>
            <?php }else{ ?>

        <select class="form-control" name="subcat_name" id="subcat_name" disabled="true"> </select>

           <?php } ?>
           
           </div>
        </div>  
    
    	<div class="col-sm-6 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Title:</label>
        </div>
  
        <div class="col-sm-9 inputbox termselect">
        <input type="text" name="reporttitle" id="reporttitle" value="<?php echo $info->title;?>"  placeholder="Enter title" class="form-control" autocomplete="off" >
        </div>
      </div>

  
	</div>
    
</div>
<div class="col-sm-12 col-xs-12 applycodediv reportdiv nopadding">

    <div class="form-group fullwidthinput">
    
    	<div class="col-sm-12 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Description</label>
        </div>
  
        <div class="col-sm-9 inputbox termselect">
        <textarea  class="form-control" name="reportdesc" id="reportdesc" placeholder="Enter Description" autocomplete="off" maxlenght="251" ><?php echo $info->description;?> </textarea>
        </div>
        </div>
         
	</div>
    
</div>
<div class="col-sm-12 col-xs-12 applycodediv reportdiv nopadding">

    <div class="form-group fullwidthinput">
    <div class="col-sm-12 nopadding">
          <div class="col-sm-6 nopadding"></div>
        <div class="col-sm-6 nopadding">
        <div class="col-sm-4 nopadding"></div>
        <div class="col-sm-8 confirmlink nopadding">
          <input type="submit" class="open1 addtmplt" value="Update My Template">
        </div>
        </div>
       </div> 
        
	</div>
    
</div>
</div>
   
   <div class="col-sm-3 col-xs-3 applycodediv reportdiv variablediv nopadding">
   <h4>Standard Variables </h4>
<p>{first_name} , {last_name} , {school_name} , {branch_name} , {gender} , {father_name} , {mother_name} , {email_id} , {date_of_birth} , {main_telephone} , {father_mobile} , {mother_mobile} , {father_email} , {mother_email} , {father_address} , {mother_address} , {child_class} , {enrolment_date} , {his/her} , {him/her} , {start_level} , {start_sub_level} , {current_level} , {current_sub_level} , {target_level} , {target_sub_level}</p>


</div>
    </form>      
    
  <?php } else { ?>

   <form action="<?php echo base_url();?>reporttemplate/addtemplate" name="reporttemplate" id="reporttemplate" method="post" autocomplete="off">

<div class="col-sm-9 col-xs-9">
<div class="col-sm-12 col-xs-12 applycodediv reportdiv nopadding">

    <div class="form-group fullwidthinput">
      <div class="col-sm-6 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Subject:</label>
        </div>
  
      <div class="col-sm-9 inputbox termselect">
       <select name="subject_name" id="subject_name">
        <option value="">Select Subject </option>  
        <option value='Memorisation'>Memorisation</option>
        <option value='Reading'>Reading</option>
        <option value='Islamic Studies'>Islamic Studies</option>
        <option value='Arabic Language'>Arabic Language</option>
       </select>
        </div>
        </div>

      <div class="col-sm-6 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Category:</label>
        </div>
  
      <div class="col-sm-9 inputbox termselect">
       <select name="cat_name" id="cat_name">
       </select>
        </div>
        </div>
      </div>
    </div>

<div class="col-sm-12 col-xs-12 applycodediv reportdiv nopadding">
<div class="form-group fullwidthinput">
     <div class="col-sm-6 nopadding">
        <div class="col-sm-4">
        <label class=" control-label nopadding filterlabel">SubCategory:</label>
        </div>
  
        <div class="col-sm-8">
        <select class="form-control" name="subcat_name" id="subcat_name">
        </select> 
        </div>
        </div>  
    	<div class="col-sm-6 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Title:</label>
        </div>
  
        <div class="col-sm-9 inputbox termselect">
        <input type="text" name="reporttitle" id="reporttitle" placeholder="Enter title" class="form-control" autocomplete="off" >
        </div>
        </div>


	</div>
    
</div>
<div class="col-sm-12 col-xs-12 applycodediv reportdiv nopadding">

    <div class="form-group fullwidthinput">
    
    	<div class="col-sm-12 nopadding">
        <div class="col-sm-3">
        <label class=" control-label nopadding filterlabel">Description:</label>
        </div>
  
        <div class="col-sm-9 inputbox termselect">
       <textarea  class="form-control" name="reportdesc" id="reportdesc" placeholder="Enter description" cols="num" rows="num" autocomplete="off" maxlenght="251" > </textarea>
        </div>
        </div>

	</div>
    
</div>

<div class="col-sm-12 col-xs-12 applycodediv reportdiv tmplt_btndiv nopadding">

    <div class="form-group fullwidthinput">
    <div class="col-sm-12 nopadding">
          <div class="col-sm-6 nopadding"></div>
        <div class="col-sm-6 nopadding">
        <div class="col-sm-4 nopadding"></div>
        <div class="col-sm-8 confirmlink nopadding">
        <input type="submit" class="open1 addtmplt" id="addtemplate" value="Save My Template">
        </div>
        </div>
       </div> 
        
	</div>
    
</div>
</div>
   <div class="col-sm-3 col-xs-3 applycodediv reportdiv variablediv nopadding">
   <h4>Standard Variables </h4>
<p>{first_name} , {last_name} , {school_name} , {branch_name} , {gender} , {father_name} , {mother_name} , {email_id} , {date_of_birth} , {main_telephone} , {father_mobile} , {mother_mobile} , {father_email} , {mother_email} , {father_address} , {mother_address} , {child_class} , {enrolment_date} , {his/her}, {him/her} , {start_sub_level} , {start_level} , {current_sub_level} , {current_level} , {target_sub_level} , {target_level}</p>

</div> 

 </form>     
    <?php } ?>

        </div>

    </div>
        

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

  <form action="<?php echo base_url();?>reporttemplate/index" name="templatefilterform" id="templatefilterform">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding">

				<div class="col-sm-7 nopadding selectoption">

<div class="form-group fullwidthinput">

    <label class="col-sm-1 col-xs-1 control-label nopadding filterlabel">Show:</label>

    <div class="col-sm-11 col-xs-11 nopadding selectfilter">

    <div class="col-sm-3 col-xs-3 inputbox termselect">

<select class="form-control" name="status" id="status">
  <option value="">Select Status </option>    
   <option value="1">Active</option>
   <option  value="0">Inactive</option>
</select>

    </div>

    </div>

  </div>

</div>		

<div class="col-sm-3 col-xs-5 searching termsearch nopadding">

        <div class="form-group">

      <input type="text" class="form-control searchbox" placeholder="Search Here" name="seachword" id="seachword" value="<?php echo $word_search;?>">


     </div>

        </div>

        <div class="col-sm-2 col-xs-7 rightspace viewreport">

<div class="form-group">

    <input type="submit" class="btn btn-danger srchbtn" value="Find Template">

   </div>

</div>

        </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <h1>Report Templates<span><?php echo $Totalrec = $total_rows;?></span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

            <th class="column3">S.No.</th>

            <th class="column4">Subject / Category  </th>
                        
            <th class="column4">Subcategory / Title</th>

            <th class="column6">Description</th>
                      
            <th class="column3">Action</th>

				  </tr>

			  </thead>

				<tbody>
                 <?php 
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $templates) { 
					     $sr++;
            $dropdownid=  $templates->subcategory_id; 
            $this->load->model(array('Reporttemplate_model'));
            $subcategoryname =$this->Reporttemplate_model->only_subcategory($dropdownid);
						
					?>

					<tr class="familydata students">
						<td class="column3"><?php echo $sr; ?></td>
   <td class="column4"><?php if($templates->category_name == 'AoA'){echo $templates->subject_name.'/Account of Achievement';}
                                                   else {echo $templates->subject_name.'/Area of Devepolment';} ?></td>
                      <td class="column4"><?php echo $subcategoryname->dropdown_name .'/'. $templates->title; ?></td>
                      <td class="column6"><?php echo $templates->description; ?></td>
                   
                      
                        <td class="column3">
											<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
 	      <li><a href="<?php echo base_url();?>reporttemplate/index/<?php echo $last_page;?>/<?php echo $templates->template_id;?>/edit<?php echo $post_url;?>" target="_blank"> Edit </a></li>
          
           
			<li class="divider"></li>
				 <li><a onclick="delConfirm('<?php echo $templates->template_id;?>')" style="cursor: pointer;"> Delete </a> </li>
									</ul>
								</div>
											
							</td>
      
                        </tr>
			         <?php	}		
					} else { ?>               
                                  
                    <tr><th colspan="7" style="text-align: center; width:1215px; height: 100px;font-size:25px ; background:#FFF;  color: #6a7a91;">No record to show.</th></tr>	
				   <?php } ?>

				</tbody>

		  </table>

     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv">

    <div class="col-sm-8 col-xs-6 paginationblk">

			<p class="showp">Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?>  entries</p>
			
	<ul class="pagination">

		<?php echo $pagination;?>

	</ul>

    </div>

    <div class="col-sm-4 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total:&nbsp;<?php echo $Totalrec ;?></h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>reporttemplate/index" method="get">
       
          <input type="hidden" name="status" id="status" value="<?php echo $status_search; ?>"  />
       
          <input type="hidden" name="seachword" id="seachword" value="<?php echo $word_search; ?>"  />  

          <select class="form-control" name="perpage" id="perpage">
        
          <option value="">Select</option>
        
          <option   <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>
        
          <option   <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>
        
          <option   <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>
        
          <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>
        
          <option <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>
        
         </select>

</form>


        </div>

    </div>

	</div>

    </div>     

</div>


	</div>

    

	</div>

    </div>

    </div>
  <script>
  jQuery(document).ready(function(){
	  
	  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
					
	 jQuery.validator.addMethod("lettersonly", function(value, element) {
		return this.optional(element) || /^[a-z\s]+$/i.test(value);
		}, "Only alphabetical characters");

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });
	    jQuery("#reporttemplate").validate({
        rules: {
		      subject_name:{
            required:true
          },
          subcat_name:{
            required:true
          },
          cat_name:{
            required:true
          },
         reporttitle: {
                   required: true
                   //maxlength: 61
          },

         reportdesc: {
                  required: true
          }
			  
        },
        messages: {
        subject_name: {
                  required: "Select Subject Name."
          },
           cat_name: {
                  required: "Select Your Category."
          },
           subcat_name: {
                  required: "Select Your SubCategory."
          },
        reporttitle: {
                  required: "Please Enter title."
                 // maxlength: "Maximum 60 characters allowed."
          },
        reportdesc: {
                  required: "Please Enter Description"
          }

			},
        submitHandler: function(form) {
            form.submit();
          }
        });	

/******************* option values of category depending on subject ************************************/

    $('#cat_name').empty();
    $('#cat_name').prop('disabled','disabled');

var options="";

$("#subject_name").on('change',function(){ 
 $('#cat_name').prop('disabled', false );
    var value=$(this).val(); 
    $('#subcat_name').empty();
    $('#subcat_name').prop('disabled','disabled');
    if(value=="Islamic Studies") 
    {
        options = "<option value=''>Select Category</option>"+"<option value='AoA'>Area of Achievement</option>"
                    +"<option value='AoD'>Area of Devepolment</option>"
                    +"<option value='ToC'>Topic Covered</option>";
           
        $("#cat_name").html(options);
    }
    else 
    {
        options ="<option value=''>Select Category</option>"+"<option value='AoA'>Area of Achievement</option>"
                    +"<option value='AoD'>Area of Devepolment</option>";
        $("#cat_name").html(options);
    }
   
}); 


    jQuery("#cat_name").on('change', function(e){ 
        var cat_name = jQuery(this).val();
       var subject_name = jQuery('#subject_name').val();
       
      if(cat_name!='' && subject_name!='') {
      jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>Reporttemplate/get_subcategory',
            data:{ 'subject_name': subject_name, 'cat_name':cat_name},
            success :  function(resp) {
              if(resp !=''){
              jQuery('#subcat_name').prop('disabled', false);
              jQuery('#subcat_name').empty();
              jQuery('#subcat_name').html(resp);
                }else{
                   jQuery("#subcat_name")[0].selectedIndex = 0;
                   jQuery('#subcat_name').empty();
                   jQuery('#subcat_name').prop('disabled','disabled');
              }
            }
           });
         } else {
         return false;
         }
       });

    /* jQuery("#subject_name").on('change', function(e){ 
        var subject_name = jQuery(this).val();
       var cat_name = jQuery('#cat_name').val();
       
      if(cat_name!='' && subject_name!='') {
      jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>Reporttemplate/get_subcategory',
            data:{ 'subject_name': subject_name, 'cat_name':cat_name},
            success :  function(resp) {
              if(resp !=''){
              jQuery('#subcat_name').prop('disabled', false);
              jQuery('#subcat_name').empty();
              jQuery('#subcat_name').html(resp);
              }else{
                  jQuery('#subcat_name').empty();
                  jQuery('#subcat_name').prop('disabled', 'disabled');
              }
                }
           });
         } else {
         return false;
         }
       }); */

					
 });
</script>					   
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>reporttemplate/deletetemplate/"+id;
		}else{
			return false;
		}
	}
</script>	
	
<style>
.variablediv {
    border: 1px solid #ccc;
    height: 310px;
    border-radius: 4px;
    box-shadow: 0 0 5px #ccc;

}
.variablediv > p, h4 {
   padding: 0 0 0 10px;
 
}

.open1.addtmplt {
    font-size: 18px !important;
	margin:0px !important
}
.tmplt_btndiv{
	margin:0px;
}
#reportdesc {
    min-height: 200px;
}
.reportdiv {
    margin-bottom: 20px;
}
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.fullwidthinput select {
    background-position: 95% center;
	padding:0;
}
.Inactive {
    background: #ff0000 none repeat scroll 0 0;
}
<!--changes-->
.showp {
    padding-top: 20px;
}
.pagination{
	margin:0px;
	padding-left:180px;}
	
.paginationblk {
    padding: 25px;
}
.srchbtn{
   padding: 6px 30px;
}
.open1{
	margin:0px !important;
	margin-bottom:20px !important;
}
@media screen and (min-width:320px) and (max-width:480px){
.cleanSearchFilter {
    padding: 8px 5px !important;
}
.termsearch {
    padding-bottom: 10px !important;
}
.searching {
    width: 100% !important;
}
.paginationblk {
    text-align: center !important;
}
}
@media screen and (min-width:481px) and (max-width:767px){ 
.selectoption {
    width: 100%;
}
.fullwidthinput label {
    width: auto;
}
.inputbox.termselect {
    width: 100%;
}
#status {
    margin-bottom: 10px;
}
.inputbox.termselect {
    width: 100%;
}
}
@media screen and (min-width:768px) and (max-width:991px){
.fullwidthinput select {
    font-size: 14px;
    margin-bottom: 10px;
}
.inputbox.termselect {
    width: 100%;
}
.fullwidthinput label {
    width: auto;
}
}
@media screen and (min-width:992px) and (max-width:1280px){
.statusbtn {
    font-size: 10px;
    padding: 7px 10px;
}
#branchfilterform .inputbox.termselect {
    padding: 0 10px;
    width: 50%;
}
.cleanSearchFilter {
    padding: 10px 2px 10px 1px;
}
.searching {
    width: 24%;
}
.btn {
    padding: 7px 8px;
}
#branchfilterform .selectoption {
    width: 50%;
}
.totalstudent {
    padding: 0 15px 0 0;
    text-align: left;
}
}
.arrow_dash {
    text-align: center;
}
.arrow_dash span {
    font-size: 30px;
    line-height: 30px;
}
.profile-bg.filterbox {
    padding: 20px 15px 15px 15px !important;
}
.checkboxdiv{ padding:0px 16px 0px 22px !important; }
.checkboxdiv.checkbox_cls days_check{padding-top:2px;}
.checkboxdiv span.checkbox::before{
	left:-18px !important;
}
.checkboxdiv span.checkbox::after{
	left:-21px !important;
}
.checkboxdiv.profilecheckbox{ margin:0px !important}
</style>	
