<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		        <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li><a href="<?php echo base_url(); ?>progressreports">Progress Reports</a></li>

        <li><a href="<?php echo base_url(); ?>progressreports"><?php echo $termInfo->term_name;?></a></li>

        <li class="edit">View Report <?php echo $termInfo->term_name;?>: <?php echo $classInfo->class_name;?> - <?php echo $studentInfo->student_fname.' '.$studentInfo->student_lname;?></li>        

        </ul>

        </div>

        </div>
      

        <div class="col-sm-12 leftspace fullwidsec fulldiv">

        <div class="editprofileform editdetails accdetailinfo">

        <div class="col-sm-12 nopadding accdetailheading">

        <h1>Account Details</h1>

        </div>

        <form>
        
        <?php if( count($termReport) > 0 ) { 
		
		foreach($termReport as $treport) { 
		
		      $subjectid   =     $treport->subject_id;
			  $subjectname =  $this->viewprogressreport_model->get_subject_name($subjectid,$school_id,$branch_id,$class_id);
	$teacherName = $this->viewprogressreport_model->get_teacher_name($school_id,$branch_id,$class_id,$subjectid);

    $report_completion_image = ''; 
		if($treport->effort!='' && $treport->behaviour!='' && $treport->home_work!='' && $treport->proof_reading!='') {
	            $report_completion_image = 'green.png'; 
            } else {
				 $report_completion_image = 'darkblue.png'; 
			} 
			
			$proof_reading_status_image = ''; 
		if($treport->proof_reading == 'Incomplete') {
	            $proof_reading_status_image = 'darkblue.png'; 
            } elseif($treport->proof_reading == 'Amendments Required') {
				 $proof_reading_status_image = 'yellow.png'; 
			} elseif($treport->proof_reading == 'Approved by Teacher') {
				 $proof_reading_status_image = 'blue.png'; 
			} elseif($treport->proof_reading == 'Complete') {
				 $proof_reading_status_image = 'green.png'; 
			} 
		?>

        <div class="col-sm-6 rptdiv profile-bg">
		<div class=" col-sm-12 prfltitlediv" style="padding: 0px 0px 0px 15px;">
        <div class="col-sm-8 col-xs-8 nopadding subjheading">

        <h2> <?php echo $subjectname->subject_name;?></h2>

        </div>

        <div class="col-sm-4 col-sm-8 nopadding reporticons">

   <!--     <a href="javascript:void(0)" title="Report completion"><img src="<?php echo base_url();?>assets/images/<?php echo $report_completion_image;?>"></a>

        <a href="javascript:void(0)" title="Proof reading"><img src="<?php echo base_url();?>assets/images/<?php echo $proof_reading_status_image;?>"></a> -->

		<!-- <a href="javascript:void(0)"><i class="fa fa-cog"></i></a>-->

        </div>
        </div>
        
	<div class="contentdiv">
  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Student:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $studentInfo->student_fname.' '.$studentInfo->student_lname;?></span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Year:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $treport->progress_report_year;?></span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-4 col-xs-4 control-label nopadding">Teacher:</label>

    	<div class="col-sm-8 col-xs-8 inputinfo">

      	<span><?php echo @$teacherName->staff_fname.' '.@$teacherName->staff_lname;?></span>

    	</div>

  		</div>
        
        
        <div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Class:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $classInfo->class_name;?></span>

    </div>

  </div>
  

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Subject:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span> <?php echo $subjectname->subject_name;  ?></span>

    </div>

  </div>



        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Effort:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php if($treport->effort!=""){echo $treport->effort;}?></span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Behaviour</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $treport->behaviour;?></span>

    </div>

  </div>

  		

  <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Homework:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span><?php echo $treport->home_work;?></span>

    </div>

  </div>
  
  

  <?php if ($treport->start_level != '') { ?>

    <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Start Level:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span><?php echo $treport->start_level;?></span>

    </div>

  </div>
  <?php } ?>

    <?php if ($treport->current_level != '') { 

      $current_lvl = $this->viewprogressreport_model->get_current_level($treport->current_level);
  

      ?> 


  <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Current Level:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span><?php echo $current_lvl->sublevel_name;?></span>

    </div>

  </div>
<?php } ?>

<?php if ($treport->target_level != '') { ?> 

  <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Target Level:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span><?php echo $treport->target_level;?></span>

    </div>

  </div>
<?php } ?>

  <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding ">Account of Achievement:</label>

    <div class="col-sm-8 col-xs-8 inputbox AoAcls">

    <span><?php echo $treport->account_of_achievement;?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding ">Area Development:</label>

    <div class="col-sm-8 col-xs-8 inputbox AoAcls">

    <span><?php echo $treport->area_development;?></span>

    </div>

  </div>
  <?php if ($subjectname->subject_name == 'Islamic Studies' || $subjectname->subject_name == 'Islamic studies'){?>

  <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Topic Covered:</label>

    <div class="col-sm-8 col-xs-8 inputbox AoAcls">

    <span><?php echo $treport->topic_covered;?></span>

    </div>

  </div>
<?php } ?>


  		</div>
        </div>
        
        <?php }
		} ?>
        


        </form>

        </div>

        </div>

  <!--    <div class="col-sm-6 rightspace mobfullwidth fullwidsec fulldiv">

        <div class="col-sm-12 nopadding accdetailheading">

        <h1>Legend</h1>

        </div>

        <div class="profilecompletion">

        <div class="col-sm-6 leftspace reportblock">

        <div class="col-sm-12 prfltitlediv prfltitle reporttitle subjheading">

        <h4>Proof Reading</h4>

        </div>

        <div class="col-sm-12 nopadding reportbox">

        <div class="reportbg">

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/darkblue.png">

        <span>Incomplete</span>

        </div>

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/yellow.png">

        <span>Amendments Required</span>

        </div>

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/blue.png">

        <span>Approved by Teacher</span>

        </div>

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/green.png">

        <span>Complete</span>

        </div>

        </div>

        </div>

        </div>

        <div class="col-sm-6 rightspace reportblock mobfullwidth">

        <div class="col-sm-12 prfltitlediv prfltitle reporttitle subjheading">

        <h4>Report Completion</h4>

        </div>

        <div class="col-sm-12 nopadding reportbox">

        <div class="reportbg">

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/darkblue.png">

        <span>No Entry Exists</span>

        </div>

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/green.png">

        <span>Entry Exists</span>

        </div>

        </div>

        </div>

        </div>

        <div class="col-sm-12 nopadding linktext">

        <a href="#">Archived reports</a>

        </div>

        </div>

        </div> -->

        </div>
        
        <style>
		.reporttitle{padding-left:15px}
		.profile-bg{padding:0px !important;}
		.contentdiv{padding:0 15px 0 15px;}
		.subjheading>h2{margin-top:0px !important;  padding-top: 12px;}
    .rptdiv.profile-bg { margin-right: 10px; width: 48%; }
    .AoAcls{ overflow-wrap: break-word; text-align: justify; }
		</style>