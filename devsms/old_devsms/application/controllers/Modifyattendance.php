<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Modifyattendance extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','modifyattendance_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{	
		
		$data							= array();
		$school_id		= $this->session->userdata('user_school_id');
		$data['branches'] = $this->modifyattendance_model->getbranches($school_id);
		
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;
		$attendancedate_search			= isset($_GET['date'])?$_GET['date']:NULL;
		$attendance_session_search		= isset($_GET['session'])?$_GET['session']:NULL;
		
		$search_condition['school_id']	= $this->session->userdata('user_school_id');
		 
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		if($class_search!=''){
			$search_condition['class_id']	= $class_search;
		}
		
		if($attendancedate_search!=''){
			$search_condition['attendance_date']	= date('Y-m-d', strtotime($attendancedate_search));
		}
		if($attendance_session_search!=''){
			$search_condition['attendance_session']	= $attendance_session_search;
		}
		
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		
		$data['title']	      			= "Modify Attendance";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		$perpage						= 50;
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'modify_attendance/index';
		
if($branch_search!=='' && $class_search!='' && $attendancedate_search!='' && $attendance_session_search!='') {
	    $config['total_rows'] 			= $this->modifyattendance_model->getRows($search_condition);
	   }
		$config['per_page'] 			= $perpage;
 		$config['postfix_string'] = "/?branch=$branch_search&class=$class_search&attendancedate=$attendancedate_search&attendance_session=$attendance_session_search"; 
		$this->pagination->initialize($config);
		$odr =   "attendance_id";
		$dirc	= "asc";
		
	if($branch_search!=='' && $class_search!='' && $attendancedate_search!='' && $attendance_session_search!='') {
		if( $config['total_rows'] > 0 ) {
		    $data_rows  = $this->modifyattendance_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc);
			$data['Attendencerecords'] =  "Attendencerecords";
		} else {
			 $data_rows  = $this->modifyattendance_model->getstudents($school_id,$branch_search,$class_search);
			 $config['total_rows'] = count($data_rows);
		    }
	   }
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
if($branch_search!=='' && $class_search!='' && $attendancedate_search!='' && $attendance_session_search!='') {
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
     }
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['class_search']			= $class_search;
		$data['attendancedate_search']	= $attendancedate_search;
		$data['attendance_session_search']	= $attendance_session_search;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		$this->load->view('header');

		$this->load->view('attendance/modify_attendance', $data);

		$this->load->view('footer');

	}
	
	
	 public function check_classes()

	{     
		    $school_id = $this->session->userdata('user_school_id');
			$schoolbranch = trim($_POST['schoolbranch']); 
	        $getclasses = $this->modifyattendance_model->getclasses($school_id,$schoolbranch);
			
			foreach($getclasses as $getclass) {
 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
			}
	  }
	  
	  
	  function check_student_Attentance_status() {
	
		 $studentCount =  count(array_filter($this->input->post('studentid')));
		 $attendanceCount = count(array_filter($this->input->post('attendance_status')));
		 if($studentCount == $attendanceCount){
		           echo json_encode(array('Status'=>"true"));
			      } else {
			        echo json_encode(array('Status'=>"false"));
				  }
	  }
	  
	  	function update_attendance() {
		
		   $school_id = $this->session->userdata('user_school_id');
		   
			if ($this->input->post('branchid') != '') {
                 $branch_id = trim($this->input->post('branchid'));
            }
			if ($this->input->post('classid') != '') {
                $class_id = trim($this->input->post('classid'));
            }
			if ($this->input->post('attendancedate') != '') {
                  $attendancedate = trim($this->input->post('attendancedate'));
				  $attendance_date = date('Y-m-d', strtotime($attendancedate));
            }
			if ($this->input->post('attendance_session') != '') {
                 $attendance_session = trim($this->input->post('attendance_session'));
            }
			
			
/*	if ($this->input->post('studentid') != '') {
		 $studentCount =  count(array_filter($this->input->post('studentid')));
		 $attendanceCount = count(array_filter($this->input->post('attendance_status')));
		
		if($studentCount!=$attendanceCount){
			 $this->session->set_flashdata('error', 'Please fill all students attendance status field first.');
	redirect(base_url()."modifyattendance/index?branch=$branch_id&class=$class_id&date=$attendancedate&session=$attendance_session"); exit;
			   }
	   }*/
			
			$chkAttendance = $this->modifyattendance_model->check_attendance($school_id,$branch_id,$class_id,$attendance_date,$attendance_session);
			$countAttendance = count($chkAttendance);
			
			if( $countAttendance > 0 ) {
	
			           $input	=  $this->input->post();
		            if($this->modifyattendance_model->updateAttendance($input)) {
		                  $this->session->set_flashdata('success', 'Attendance has been updated successfuly.');
	   redirect(base_url()."modifyattendance/index?branch=$branch_id&class=$class_id&date=$attendancedate&session=$attendance_session");exit;
				       } else {
					      $this->session->set_flashdata('error', 'Some problem exists. Attendance has not been updated.');
       redirect(base_url()."modifyattendance/index?branch=$branch_id&class=$class_id&date=$attendancedate&session=$attendance_session");exit;
				        }
						
			       }  else  {
					   
					    $input		=  $this->input->post();
		            if($this->modifyattendance_model->insertAttendance($input)) {
		                  $this->session->set_flashdata('success', 'Attendance has been added successfuly.');
	  redirect(base_url()."modifyattendance/index?branch=$branch_id&class=$class_id&date=$attendancedate&session=$attendance_session");exit;
				       } else {
					      $this->session->set_flashdata('error', 'Some problem exists. Attendance has not been added.');
	  redirect(base_url()."modifyattendance/index?branch=$branch_id&class=$class_id&date=$attendancedate&session=$attendance_session");exit;
				           }
				       
					   }

	      }
	   
	   
	   
	   public function loadstudents()

	{     
/*		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;
		$attendancedate_search			= isset($_GET['attendancedate'])?$_GET['attendancedate']:NULL;
		$attendance_session_search		= isset($_GET['attendance_session'])?$_GET['attendance_session']:NULL;
		
		$search_condition['school_id']	= $this->session->userdata('user_school_id');
		 
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		if($class_search!=''){
			$search_condition['class_id']	= $class_search;
		}
		
		if($attendancedate_search!=''){
			$search_condition['attendance_date']	= date('Y-m-d', strtotime($attendancedate_search));
		}
		if($attendance_session_search!=''){
			$search_condition['attendance_session']	= $attendance_session_search;
		}
		
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		$data							= array();
		$school_id		= $this->session->userdata('user_school_id');
		$data['branches'] = $this->modifyattendance_model->getbranches($school_id);
		
		
		$data['title']	      			= "Modify Attendance";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		$perpage						= 50;
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'modify_attendance/index';
	    $config['total_rows'] 			= $this->modifyattendance_model->getRows($search_condition);
		$config['per_page'] 			= $perpage;
 		$config['postfix_string'] = "/?branch=$branch_search&class=$class_search&attendancedate=$attendancedate_search&attendance_session=$attendance_session_search"; 
		$this->pagination->initialize($config);
		$odr =   "attendance_id";
		$dirc	= "asc";
		$data_rows= $this->modifyattendance_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc);
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];

		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['class_search']			= $class_search;
		$data['attendancedate_search']	= $attendancedate_search;
		$data['attendance_session_search']	= $attendance_session_search;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
					
			$this->load->view('header');

			$this->load->view('attendance/modify_attendance', $data);

			$this->load->view('footer');*/
		  
		   /* if ($this->input->post('branch') != '') {
                 $branch_id = trim($this->input->post('branch'));
				 $data['branch_search']			= $branch_id;
            }
			
			if ($this->input->post('class') != '') {
                $class_id = trim($this->input->post('class'));
				$data['class_search']			= $class_id;
            }
			
			if ($this->input->post('attendancedate') != '') {
                  $attendancedate = trim($this->input->post('attendancedate'));
				  $attendance_date = date('Y-m-d', strtotime($attendancedate));
				  $data['attendancedate_search']			= $attendancedate;
            }
			
			if ($this->input->post('attendance_session') != '') {
                 $attendance_session = trim($this->input->post('attendance_session'));
				 $data['attendance_session_search']			= $attendance_session;
            }
			 
			$data_rows= $this->modifyattendance_model->getstudents($school_id,$branch_id,$class_id,$attendance_date,$attendance_session);
			$data['data_rows']				= $data_rows;
			*/
	  }

}

