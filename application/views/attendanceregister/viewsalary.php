<?php 

?>

<div class="editprofile-content">
    	<div class="profilemenus">
        <ul>
		          <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

           <?php  } elseif($user_type == 'student'){?>

              <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>
    <?php }else {?>
          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
    <?php } ?>
        <li><a href="<?php echo base_url();?>generateattendance">Generate Salary</a>
        <li class="edit">Staff Salary</li>        
        </ul>
        </div>
        
         <!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>

        <div class="col-sm-12 profile-bg generatesalary-filter">
        <form name="attfilter" id="attfilter" method="get" action="<?php echo base_url(); ?>generateattendance/view_salary/0/">
        <div class="col-sm-12 tablenavdiv">
    <div class="col-sm-5 tablenevright">
        <div class="col-sm-12 col-xs-12 applycodediv nopadding datepickerspacing">
        <div class="col-sm-2 branchlabel">
                <label>Branch:</label>
        </div>
        <div class="col-sm-6 col-xs-5 nopadding dateblk">
        <div class="form-group">
           <div class="inputbox datepickerdiv nopadding">
          <select class="form-control" name="branch" id="branch">
          <option value="">Select Branch</option>  
            <?php
			$staff_branch_id = $this->session->userdata('staff_branch_id');
      $user_type = $this->session->userdata('user_type');

        if($user_type=='teacher' && $staff_branch_id!=''){

 			$staff_branch = explode(',',$staff_branch_id); 
	  		foreach($staff_branch as $staff_branch_ids){
	  		$this->load->model(array('generate_attendance_model'));
	 		$branchdta = $this->generate_attendance_model->get_staff_branches($staff_branch_ids);

    ?>

     <option <?php if($branch_search == $branchdta->branch_id){ echo 'selected'; } ?> value="<?php echo $branchdta->branch_id; ?>"><?php echo $branchdta->branch_name;?></option>

<?php }
         } else if(count($branches) > 0 && $user_type=='admin'){

	   foreach($branches as $branch) { 

	 ?>
	  <option  <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
		<?php  
	   }
   }
	 ?>
     </select>
            </div>
        </div> 
  	    </div>
        </div>
        </div>
    <div class="col-sm-5 tablenevright">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding datepickerspacing">
                <div class="col-sm-2 datelabel">
                <label>Date:</label>
                </div>
                <div class="col-sm-6 col-xs-5 nopadding dateblk">
        		<div class="form-group">
    <div class="inputbox datepickerdiv nopadding">
   
      <input type="text" name="sdate" class="date-picker23" id="sdate" value="<?php if($sdate!='') { echo $sdate; }  ?>">
     
    </div>
  </div> 
  	</div>
</div>

        </div>
   <div class="col-sm-2 tablenevright">
   <div class="confirmlink">
       <input type="submit" class="open1" value="View Staff Salary">
   </div>
   </div>

    </div>
    </form>
    </div>
        <div class="col-sm-12 attendancesec nopadding">	
    <div class="col-sm-12 tablediv nopadding">
    <div class="col-sm-12 nopadding">
    <div class="tabletitlediv generatetitlediv">
    <h1 class="viewstaffhed">View Staff Salary<span></span></h1>
<form method="post" name="export_form" id="export_form" action="<?php echo base_url(); ?>/generateattendance/excel_action">
    
   		<input type="hidden" name="srch_branch" id="srch_branch" value="<?php echo $branch_search; ?>"  />
                        
      <input type="hidden" name="search_date" id="search_date" value="<?php echo $sdate; ?>"  />
    
      <input name="export" class="btn btn-success excelbtn" value="Export to Excel" type="submit">
    
</form>
    </div>
    </div>
   
    <div class="tablewrapper">
<table class="table-bordered table-striped">
			  <thead>
				  <tr class="headings enrolment-headings">
					  <th class="column1">No.</th>
					  <th class="column3">Name</th>
                      <th class="column2">Total days</th>
                      <th class="column2">Present</th>
                      <th class="column2">Absent</th>
                      <th class="column2">Late</th>
                      <th class="column2">Actual Salary</th>
                      <th class="column2">Calculated</th>
                      <th class="column2">Adjustment</th>
                      <th class="column2">Grand </th>
                      
				  </tr>
			  </thead>
              <tbody>
              <?php 
			  if (count($data_rows) > 0){ 
			  $sr=$last_page;
			  foreach($data_rows as $record){
			  ?>
              <tr class="familydata generate-salary enrolment-table">
						<td class="column1"><?php echo ++$sr; ?></td>
                        <td class="column3"><?php 
						$this->load->model(array('generate_attendance_model'));
						 $userdetails = $this->generate_attendance_model->getuserinfo($record->user_id);
						echo $userdetails->staff_fname.' '.$userdetails->staff_lname;
						?></td>
                        <td class="column2"><?php echo $record->total_working_days;?></td>
                        <td class="column2"><?php echo $record->present_working_days;?></td>
                        <td class="column2"><?php echo $record->absent_working_days;?></td>
                        <td class="column2"><?php echo $record->late_working_hours;?></td>
                        <td class="column2"><?php echo $record->actual_salary;?></td>
                        <td class="column2"><?php echo $userdetails->salary_currency.$record->calculated_salary;?></td>
                        <td class="column2"><?php echo $userdetails->salary_currency.$record->adjust_amount;?></td>
                        <td class="column2"><?php echo $userdetails->salary_currency.$record->grand_amount;?></td>
                        </tr>
                        <?php }  } else { ?>
                     <tr><th colspan="7" style="text-align: center; width:1215px;font-size:25px;height:100px; background:#FFF; color: #6a7a91;">No record to show.</th></tr>
                   <?php } ?>
              </tbody>
		  </table>
     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-8 col-xs-6 paginationblk">
			<?php
							if (count($data_rows) > 0){
								$last_page1=$last_page;
								?>
								Showing <?=++$last_page;?> to <?=$sr++;?> of <?=$total_rows++;?> entries
								<?
							}
							?>
	<ul class="pagination">
	<?php echo $pagination;?>
		
	</ul>

    </div>

    <div class="col-sm-4 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total Staffs: <?php echo count($data_rows); ?></h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>generateattendance/view_salary" method="post">

  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>

  <option <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>

    </div>

	</div>
    
    <!--<div class="col-sm-12 exceldiv">
                     <form method="post" name="export_form" action="<?php echo base_url(); ?>/generateattendance/excel_action">
    
   						<input type="hidden" name="srch_branch" id="srch_branch" value="<?php echo $branch_search; ?>"  />
                        
                        <input type="hidden" name="search_date" id="search_date" value="<?php echo $sdate; ?>"  />
    
          				<input name="export" class="btn btn-success excelbtn" value="Export to Excel" type="submit">
    
   					 </form>
                    </div>-->

    </div>     
          
</div>



	</div>
    
	</div>
    </div>
    </div>
    </div>
    </div>
<script type="text/javascript"> 
      $(document).ready( function() {
        $('#message').delay(5000).fadeOut();
      });
	  
	  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	});
	  </script>
  
 
  
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>students/delete/"+id;
		}else{
			return false;
		}
	}
	
	<?php  if(count($data_rows) > 0){
					     
					  foreach($data_rows as $staff) { ?>
	function sum<?php echo $staff->user_id; ?>() {
            var txtFirstNumberValue = document.getElementById('actual<?php echo $staff->user_id; ?>').value;
            var txtSecondNumberValue = document.getElementById('adjusted<?php echo $staff->user_id; ?>').value;
            var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue);
			///var result = txtFirstNumberValue + txtSecondNumberValue;
            if (!isNaN(result)) {
                document.getElementById('grand<?php echo $staff->user_id;?>').value = result;
				document.getElementById('granddemo<?php echo $staff->user_id;?>').value = result;
			} }
				<?php 
				} }
				?>
         
</script>	 
<script>
           $(function() {
    $( ".datepicker1" ).datepicker({ dateFormat: "dd MM yy" });

});



$(function() {
    $('.date-picker23').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
        onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });
});


$(document).ready(function () {

    $('#attfilter').validate({ // initialize the plugin
        rules: {
            branch: {
                required: true,
            },
			sdate: {
                required: true,
            }
        },
		messages :{
        branch : {
            required : 'Please select one Branch Name'
        },
		sdate: {
                required: 'Please select Date',
            }
    }
    });

});
      </script>
<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.branchlabel > label {
    padding-top: 5px;
}
.confirmlink input {
    margin: 10px 0;
}
.confirmlink {
    margin: 0 -15px 0 0px;
}
.paginationblk {
    padding-top: 25px;
}
#perpage{
	margin-right:35px;
}
.ui-datepicker-calendar {
    display: none;
    }
.viewstaffhed {
    float: left;
}
#export_form {
    float: right;
    margin: 20px;
}
</style>
  </body>
</html>