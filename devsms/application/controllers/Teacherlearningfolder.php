<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teacherlearningfolder extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','teacherlearning_model','teacherlearningfolder_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('image_lib');
    }

	public function index()

	{
		$data							= array();
		
		
		$Folder_id      = $this->uri->segment(3);
		$school_id		= $this->session->userdata('user_school_id');
		$data['Folders'] = $this->teacherlearning_model->getallFolders($school_id);
		
		$data['Folder_id'] = $Folder_id;
		
		$data['Foldersdata'] = $this->teacherlearningfolder_model->getFolderdata($school_id,$Folder_id);
		
		$data['Folderfiles'] = $this->teacherlearningfolder_model->getFolderfiles($school_id,$Folder_id);
		
		$this->load->view('header');
		$this->load->view('teacherlearning/folders', $data);
		$this->load->view('footer');
	}

	function addFolder() {
			 
			     $folderdata = array();
				 $school_id =  $this->session->userdata('user_school_id');
				 
				 

			if (trim($this->input->post('pra_branch')) != '') {
                 $folderdata['parent_teacher_learning'] = trim($this->input->post('pra_branch'));
				 $parent = trim($this->input->post('pra_branch'));
                }
				
			if ($this->input->post('foldername') != '') {
                 $folderdata['teacher_learning_name'] = trim($this->input->post('foldername'));
				 $foldername = trim($this->input->post('foldername'));
                }
			
			$folderdata['school_id'] = $this->session->userdata('user_school_id');
			$folderdata['created_by'] = $this->session->userdata('user_name');
			$folderdata['is_active'] = '1';
			$folderdata['created_date'] = date('Y-m-d H:i:s');;
			$folderdata['updated_date'] = date('Y-m-d H:i:s');
			
			$chk_foldername = $this->teacherlearningfolder_model->chk_foldername($foldername, $parent, $school_id);
			   if( count($chk_foldername) > 0 ){
				   echo json_encode(array('Status'=>"alreadyexist"));
			    } else {
              if($this->teacherlearningfolder_model->insertfolder($folderdata)) {
				 echo json_encode(array('Status'=>"true"));
			    } else {
				 echo json_encode(array('Status'=>"false"));
			       }
			     }
	        }


		function uploaddocuments() {
			 
			  $attachmentdata = array();
			  
				
			  $files = $_FILES;
              $count = count($_FILES['school_certificates']['name']);
              for($i=0; $i<$count; $i++)
                {
                $_FILES['school_certificates']['name']= time().$files['school_certificates']['name'][$i];
                $_FILES['school_certificates']['type']= $files['school_certificates']['type'][$i];
                $_FILES['school_certificates']['tmp_name']= $files['school_certificates']['tmp_name'][$i];
                $_FILES['school_certificates']['error']= $files['school_certificates']['error'][$i];
                $_FILES['school_certificates']['size']= $files['school_certificates']['size'][$i];
                $config['upload_path'] = './uploads/teacher_learning_certificates/';
                $config['allowed_types'] = '*';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload("school_certificates");
                $fileName = $_FILES['school_certificates']['name'];
                $images[] = $fileName;
                }
                 /* $fileName = implode(',',$images);*/
				  
				  foreach($images as $singleimage){
				  $attachmentdata['teacher_learning_attachment_name']		= $singleimage;
				  if ($this->input->post('folder_id') != '') {
                 $attachmentdata['teacher_learning_id'] = trim($this->input->post('folder_id'));
                }
				
			$attachmentdata['school_id'] = $this->session->userdata('user_school_id');
			$attachmentdata['created_by'] = $this->session->userdata('user_name');
			$attachmentdata['is_active'] = '1';
			$attachmentdata['created_date'] = date('Y-m-d H:i:s');;
			$attachmentdata['updated_date'] = date('Y-m-d H:i:s');
			$result = $this->teacherlearningfolder_model->insertDocuments($attachmentdata);
				  }
				  
			if($result)	  
			 {
				 echo json_encode(array('Status'=>"true"));
			        } else {
					 echo json_encode(array('Status'=>"false"));
			        }
			
			/*$chk_foldername = $this->documents_model->chk_folder_name($foldername);
			   if( count($chk_foldername) > 0 ){
				   echo json_encode(array('Status'=>"alreadyexist"));
			    } else {
              if($this->documents_model->insertfolder($folderdata)) {
				 echo json_encode(array('Status'=>"true"));
			    } else {
				 echo json_encode(array('Status'=>"false"));
			       }
			     }*/
	     }
		 
		 function searchitem(){
	    $data							= array();
		$filesearch			= isset($_GET['filesearch'])?$_GET['filesearch']:NULL;
		$school_id		= $this->session->userdata('user_school_id');
		
		$data['searchFolders'] = $this->teacherlearning_model->getallFolders($school_id, $filesearch);
		
		$data['Foldersdata'] = $this->teacherlearningfolder_model->searchFolder($school_id, $filesearch);
		
		$data['Folderfiles'] = $this->teacherlearningfolder_model->searchfiles($school_id, $filesearch);
		
		$this->load->view('header');
		$this->load->view('teacherlearning/search', $data);
		$this->load->view('footer');
			 
			}
		
		 function deletefile(){
	    $id = $this->uri->segment(3); 
		 $folderid = $this->uri->segment(4);
		$school_id		= $this->session->userdata('user_school_id'); 
		$data = array(
				'is_deleted' => '1'
				);
		$deletefile = $this->teacherlearningfolder_model->deletefile($id, $data);
		if($deletefile){
			$this->session->set_flashdata('success', 'File has been Deleted successfuly.');
				redirect(base_url()."teacherlearningfolder/index/".$folderid);exit;}
				else {
					$this->session->set_flashdata('error', 'File has not been Deleted ');
				redirect(base_url()."teacherlearningfolder/index/".$folderid);exit;}
			}
			
			 function deletesbfile(){
	    $id = $this->uri->segment(3); 
		 $folderid = $this->uri->segment(4);
		$school_id		= $this->session->userdata('user_school_id'); 
		$data = array(
				'is_deleted' => '1'
				);
		$deletefile = $this->teacherlearningfolder_model->deletefile($id, $data);
		if($deletefile){
			$this->session->set_flashdata('success', 'File has been Deleted successfuly.');
				redirect(base_url()."teacherlearningfolder/subfolder/".$folderid);exit;}
				else {
					$this->session->set_flashdata('error', 'File has not been Deleted ');
				redirect(base_url()."teacherlearningfolder/subfolder/".$folderid);exit;}
			}
			
				 function deletefolder(){
	    $id = $this->uri->segment(3); 
		 $folderid = $this->uri->segment(4);
		$school_id		= $this->session->userdata('user_school_id'); 
		$data = array(
				'is_deleted' => '1'
				);
		$deletefolder = $this->teacherlearning_model->deletefolder($id, $data);
		if($deletefolder){
			$this->session->set_flashdata('success', 'Folder has been Deleted successfuly.');
				redirect(base_url()."teacherlearningfolder/index/".$folderid);exit;}
				else {
					$this->session->set_flashdata('error', 'Folder has not been Deleted ');
				redirect(base_url()."teacherlearningfolder/index/".$folderid);exit;}
			}
	  
	   public function downloadfile($fileName = NULL) { 
	  $folderid = $this->uri->segment(4);
/*	    $postURL	= $this->createPostURL($_GET);
*/       if ($fileName) {
        $file = realpath ( FCPATH.'uploads/teacher_learning_certificates/'.$fileName );
        if (file_exists ( $file )) {
         $data = file_get_contents ( $file );
         $this->load->helper('download');
         force_download ( $fileName, $data );
        } else {
		 $this->session->set_flashdata('error', 'Requested file does not exists.');
       redirect(base_url()."teacherlearningfolder/index/".$folderid);exit;
        }
       }
      }
	  
	  public function subfolder(){
	  $data							= array();
		$Folder_id      = $this->uri->segment(3);
		$school_id		= $this->session->userdata('user_school_id');
		$data['Folders'] = $this->teacherlearning_model->getallFolders($school_id);
		
		$data['Folder_id'] = $Folder_id;
		
		$data['Foldersdata'] = $this->teacherlearningfolder_model->getFolderdata($school_id,$Folder_id);
		
		$data['Folderfiles'] = $this->teacherlearningfolder_model->getFolderfiles($school_id,$Folder_id);
		
		$this->load->view('header');
		$this->load->view('teacherlearning/subfolder', $data);
		$this->load->view('footer');
			 
			}
	

}

