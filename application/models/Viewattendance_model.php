<?php

if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Viewattendance_model extends CI_Model {

	

	var $tbl	= "attendance";

	var $users = "users";

	var $student = "student";

	var $branch = "branch";

	var $classes = "classes";

	var $staff = "staff";
    
	var $attendance_archive = 'attendance_archive';
	

	

    function __construct() {

        parent::__construct();

    }

	 function  getstudentinfo($studentid) {
        $this->db->select('*');
        $this->db->from($this->student);
		$this->db->where('student_id', $studentid);
        $query = $this->db->get();
        return  $query->row();
	   }


	function getbranchName($student_school_branch) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id="=>$student_school_branch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getclassName($student_class_group) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("class_id="=>$student_class_group,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	
	function getRows($where=NULL,$startdate,$enddate){
		$this->db->select('*');
		if($where != NULL){
			$this->db->where($where);
		}
	 if($startdate != NULL && $enddate!= NULL){
			$this->db->where('attendance_date >=', $startdate);
			$this->db->where('attendance_date <=', $enddate);
		  }
		$query = $this->db->get($this->tbl);
		 $this->db->last_query();
		return $query->num_rows();
	}

	  

	  function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$startdate,$enddate){
		$this->db->select('*');
		if($where != NULL){
			$this->db->where($where);
		}
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where('attendance_date >=', $startdate);
			$this->db->where('attendance_date <=', $enddate);
		  }

		$this->db->order_by("attendance_id", "asc"); 	
		$this->db->limit($limit, $start);
		$query = $this->db->get($this->tbl);
		$this->db->last_query();
		return $query->result();
	  }



}







 

