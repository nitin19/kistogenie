<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li><a href="#">Progress Reports</a></li>

        <li><a href="#"><?php echo $termInfo->term_name;?></a></li>

        <li class="edit"><a href="#"> View Report <?php echo $termInfo->term_name;?>: <?php echo $classInfo->class_name;?> - <?php echo $studentInfo->student_fname.' '.$studentInfo->student_lname;?></a></li>        

        </ul>

        </div>

 <!--       <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>-->

        </div>
        
        
       

        <div class="col-sm-6 leftspace fullwidsec fulldiv">

        <div class="editprofileform editdetails accdetailinfo">

        <div class="col-sm-12 nopadding accdetailheading">

        <h1>Account Details</h1>

        </div>

        <form>
        
        <?php if( count($termReport) > 0 ) { 
		
		foreach($termReport as $treport) { 
		
		      $subjectid   =     $treport->subject_id;
			  $subjectname =  $this->viewprogressreport_model->get_subject_name($subjectid,$school_id,$branch_id,$class_id);
	$teacherName = $this->viewprogressreport_model->get_teacher_name($school_id,$branch_id,$class_id,$subjectid);
	
	       $report_completion_image = ''; 
		if($treport->effort!='' && $treport->behaviour!='' && $treport->home_work!='' && $treport->proof_reading!='') {
	            $report_completion_image = 'green.png'; 
            } else {
				 $report_completion_image = 'darkblue.png'; 
			} 
			
			$proof_reading_status_image = ''; 
		if($treport->proof_reading == 'Incomplete') {
	            $proof_reading_status_image = 'darkblue.png'; 
            } elseif($treport->proof_reading == 'Amendments Required') {
				 $proof_reading_status_image = 'yellow.png'; 
			} elseif($treport->proof_reading == 'Approved by Teacher') {
				 $proof_reading_status_image = 'blue.png'; 
			} elseif($treport->proof_reading == 'Complete') {
				 $proof_reading_status_image = 'green.png'; 
			} 
		?>

        <div class="profile-bg">
		<div class=" col-sm-12 prfltitlediv" style="padding: 0px 0px 0px 15px;">
        <div class="col-sm-8 col-xs-8 nopadding subjheading">

        <h2> <?php echo $subjectname->subject_name;?></h2>

        </div>

        <div class="col-sm-4 col-sm-8 nopadding reporticons">

        <a href="javascript:void(0)" title="Report completion"><img src="<?php echo base_url();?>assets/images/<?php echo $report_completion_image;?>"></a>

        <a href="javascript:void(0)" title="Proof reading"><img src="<?php echo base_url();?>assets/images/<?php echo $proof_reading_status_image;?>"></a>

		<!-- <a href="javascript:void(0)"><i class="fa fa-cog"></i></a>-->

        </div>
        </div>
        
	<div class="contentdiv">
  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Student:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $studentInfo->student_fname.' '.$studentInfo->student_lname;?></span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Year:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $treport->progress_report_year;?></span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-4 col-xs-4 control-label nopadding">Teacher:</label>

    	<div class="col-sm-8 col-xs-8 inputinfo">

      	<span><?php echo @$teacherName->staff_fname.' '.@$teacherName->staff_lname;?></span>

    	</div>

  		</div>
        
        
        <div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Class:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $classInfo->class_name;?></span>

    </div>

  </div>
  

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Subject:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span> <?php echo $subjectname->subject_name;  ?></span>

    </div>

  </div>



        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Effort:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php if($treport->effort!=""){echo $treport->effort;}?></span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Behaviour</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $treport->behaviour;?></span>

    </div>

  </div>

  		

  <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Homework:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span><?php echo $treport->home_work;?></span>

    </div>

  </div>
  
  
  <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Proof Reading:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span><?php echo $treport->proof_reading;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Comments:</label>

    <div class="col-sm-8 col-xs-8 inputbox">
     <span><?php echo $treport->comment;?></span>

    </div>

  </div>

  		</div>
        </div>
        
        <?php }
		} ?>
        
        
<!--
        <div class="profile-bg">

        <div class="col-sm-8 col-xs-8 nopadding">

        <h2>Islamic Study Report</h2>

        </div>

        <div class="col-sm-4 col-sm-8 nopadding reporticons">

        <a href="#"><img src="<?php echo base_url();?>assets/images/blue.png"></a>

        <a href="#"><img src="<?php echo base_url();?>assets/images/darkblue.png"></a>

        <a href="#"><i class="fa fa-cog"></i></a>

        </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Student:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>GCS - Aaliyah Ali</span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Year:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>GCS - SAT - Class 1</span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-4 col-xs-4 control-label nopadding">Teacher:</label>

    	<div class="col-sm-8 col-xs-8 inputinfo">

      	<span>Reha Ullah</span>

    	</div>

  		</div>

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Subject:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Arabic Language</span>

    </div>

  </div>



        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Effort:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Excellent</span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Behaviour</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Very Good</span>

    </div>

  </div>

  		

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Homework:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span>Satisfactory</span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Comments:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

     <span>-</span>

    </div>

  </div>

  		</div>

        <div class="profile-bg">

        <div class="col-sm-8 col-xs-8 nopadding">

        <h2>Memorisation Report</h2>

        </div>

        <div class="col-sm-4 col-sm-8 nopadding reporticons">

        <a href="#"><img src="<?php echo base_url();?>assets/images/green.png"></a>

        <a href="#"><img src="<?php echo base_url();?>assets/images/yellow.png"></a>

        <a href="#"><i class="fa fa-cog"></i></a>

        </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Student:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>GCS - Aaliyah Ali</span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Year:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>GCS - SAT - Class 1</span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-4 col-xs-4 control-label nopadding">Teacher:</label>

    	<div class="col-sm-8 col-xs-8 inputinfo">

      	<span>Reha Ullah</span>

    	</div>

  		</div>

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Subject:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Arabic Language</span>

    </div>

  </div>



        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Effort:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Excellent</span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Behaviour</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Very Good</span>

    </div>

  </div>

  		

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Homework:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span>Satisfactory</span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Comments:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

     <span>-</span>

    </div>

  </div>

  		</div>

        <div class="profile-bg">

        <div class="col-sm-8 col-xs-8 nopadding">

        <h2>Recitation Report</h2>

        </div>

        <div class="col-sm-4 col-sm-8 nopadding reporticons">

        <a href="#"><img src="<?php echo base_url();?>assets/images/blue.png"></a>

        <a href="#"><img src="<?php echo base_url();?>assets/images/darkblue.png"></a>

        <a href="#"><i class="fa fa-cog"></i></a>

        </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Student:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>GCS - Aaliyah Ali</span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Year:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>GCS - SAT - Class 1</span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-4 col-xs-4 control-label nopadding">Teacher:</label>

    	<div class="col-sm-8 col-xs-8 inputinfo">

      	<span>Reha Ullah</span>

    	</div>

  		</div>

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Subject:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Arabic Language</span>

    </div>

  </div>



        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Effort:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Excellent</span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Behaviour</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Very Good</span>

    </div>

  </div>

  		

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Homework:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span>Satisfactory</span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Comments:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

     <span>-</span>

    </div>

  </div>

  		</div>

        <div class="profile-bg">

        <div class="col-sm-8 col-xs-8 nopadding">

        <h2>Tutor Group: GCS - SAT - Class 1 Report</h2>

        </div>

        <div class="col-sm-4 col-sm-8 nopadding reporticons">

        <a href="#"><img src="<?php echo base_url();?>assets/images/blue.png"></a>

        <a href="#"><img src="<?php echo base_url();?>assets/images/darkblue.png"></a>

        <a href="#"><i class="fa fa-cog"></i></a>

        </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Student:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>GCS - Aaliyah Ali</span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Year:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>GCS - SAT - Class 1</span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-4 col-xs-4 control-label nopadding">Teacher:</label>

    	<div class="col-sm-8 col-xs-8 inputinfo">

      	<span>Reha Ullah</span>

    	</div>

  		</div>

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Subject:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Arabic Language</span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Additional Comments:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

     <span>-</span>

    </div>

  </div>

  		</div>

-->

        </form>

        </div>

        </div>

        <div class="col-sm-6 rightspace mobfullwidth fullwidsec fulldiv">

        <div class="col-sm-12 nopadding accdetailheading">

        <h1>Legend</h1>

        </div>

        <div class="profilecompletion">

        <div class="col-sm-6 leftspace reportblock">

        <div class="col-sm-12 prfltitlediv prfltitle reporttitle subjheading">
 
         <h4>Proof Reading</h4>

        </div>

        <div class="col-sm-12 nopadding reportbox">

        <div class="reportbg">

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/darkblue.png">

        <span>Incomplete</span>

        </div>

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/yellow.png">

        <span>Amendments Required</span>

        </div>

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/blue.png">

        <span>Approved by Teacher</span>

        </div>

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/green.png">

        <span>Complete</span>

        </div>

        </div>

        </div>

        </div>

        <div class="col-sm-6 rightspace reportblock mobfullwidth">

        <div class="col-sm-12 prfltitlediv prfltitle reporttitle subjheading">

        <h4>Report Completion</h4>

        </div>

        <div class="col-sm-12 nopadding reportbox">

        <div class="reportbg">

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/darkblue.png">

        <span>No Entry Exists</span>

        </div>

        <div class="col-sm-12 proofdiv">

        <img src="<?php echo base_url();?>assets/images/green.png">

        <span>Entry Exists</span>

        </div>

        </div>

        </div>

        </div>

<!--        <div class="col-sm-12 nopadding linktext">

        <a href="#">Archived reports</a>

        </div>-->

        </div>

        </div>

        </div>
        
        <style>
		.reporttitle{padding-left:15px}
		.profile-bg{padding:0px !important;}
		.contentdiv{padding:0 15px 0 15px;}
		.subjheading>h2{margin-top:0px !important;  padding-top: 12px;}
		</style>