<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	   function __construct() {
		   
        parent::__construct();
		
	     $this->load->model(array('login_model','authorization_model'));
        $this->load->helper('cookie'); 
			}
			
			
	public function index()	{		
	if(isset($_COOKIE["username"]) && isset($_COOKIE["userpass"])) {
		
		$user_info	= $this->login_model->getUserInfo($_COOKIE["username"],$_COOKIE["userpass"]);
		//$this->authorize->do_user_login($user_info[0]);
	
		/*if($user_info[0]->user_type=='superadmin'){
					
					redirect(base_url().'dashboard');
					
				} elseif($user_info[0]->user_type=='admin'){

					redirect(base_url().'admindashboard');

				} elseif($user_info[0]->user_type=='teacher'){

					redirect(base_url().'teacherdashboard');

				} elseif($user_info[0]->user_type=='student'){

					redirect(base_url().'studentdashboard');

				 }*/
	        }
			/*if($this->authorize->is_user_logged_in()==true)
				{
					$usertype=$this->session->userdata('user_type');
					if($usertype=='superadmin'){
					redirect(base_url().'dashboard');
				  } elseif($usertype=='admin'){
					redirect(base_url().'admindashboard');
				  } elseif($usertype=='teacher'){
					redirect(base_url().'teacherdashboard');
				  } elseif($usertype=='student'){
					redirect(base_url().'studentdashboard');
				   }
				}*/
				
		$data = array();
		

		$this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');

		$this->form_validation->set_rules('userpass', 'password', 'trim|required');				

        if ($this->form_validation->run() == TRUE){
			
			 $user				= $this->input->post('username');
			 $pass				= $this->input->post('userpass'); 
			 //$org_pass			= $this->input->post('password');
			 $remember_me	    =  $this->input->post('group2'); 

	if( $this->authorize->validate_user_credentials($user, $pass) == true ){

				$user_info	= $this->login_model->getUserInfo($user,$pass);
				$userid = $user_info[0]->id;
				$user_type = $user_info[0]->user_type;
				
				$user_access	= $this->login_model->getUseraccess($userid);
				
				$staff_details	= $this->login_model->getstaffdetails($userid);
				
				
				$this->load->helper('cookie');
					if($remember_me == 'on' ) {
/*							   $cookiedata = array(
							        username' => $user,
							        userpass' => $pass
							    );
							  $this->input->set_cookie($cookiedata);*/
							  $username=$user;
							  $userpass=$pass;
							  $this->input->set_cookie($username);
							  $this->input->set_cookie($userpass);

					  } else {

			/*		  	 $cookiedata = array(
							        'username' => '',
							        'userpass' => ''
							    );
							$this->input->set_cookie($cookiedata);*/
							
							  $username='';
 							  $userpass='';
							  $this->input->set_cookie($username);
  							  $this->input->set_cookie($userpass);	
							 
					    }
				
		if($user_info[0]->school_id=='0' && $user_info[0]->user_type=='superadmin') {
					
					$logedin_user_id = $user_info[0]->id;
				
				    $total_login = $user_info[0]->total_login;
				
				    $total_login_count = $total_login+1;
					
						
					$this->login_model->savelogin_time($logedin_user_id,$total_login_count);

				    $this->authorize->do_user_login($user_info[0]);
					 redirect(base_url().'dashboard'); exit;
					
				  } else {
			    
				$user_school_id = $user_info[0]->school_id;
				$user_school_info	= $this->login_model->getUserSchoolInfo($user_school_id);
					  
		 if($user_school_info->is_active==1 && $user_school_info->is_deleted==0) {
			    
				 $is_active = $user_info[0]->is_active;
				 $is_deleted = $user_info[0]->is_deleted;
				
			if( $is_active==1 && $is_deleted==0 ) {
					
				$logedin_user_id = $user_info[0]->id;
				
				$total_login = $user_info[0]->total_login;
				
				$total_login_count = $total_login+1;
				
				if($remember_me == 'on' ) {

				       setcookie ("username",$user,time()+ (1555200));
				       setcookie ("userpass",$pass,time()+ (1555200));
					  } else {
						  /*if(isset($_COOKIE["username"])) {
					             echo setcookie("username", "", time() - 3600);
								 echo  $_COOKIE["username"];
				            }
				           if(isset($_COOKIE["userpass"])) {
					           setcookie("userpass", "", time() - 3600);
							   echo $_COOKIE["userpass"];
				          }*/
						   setcookie("username", "", time() - 3600);
						   setcookie("userpass", "", time() - 3600);
					    }
				
			  
				
				$this->login_model->savelogin_time($logedin_user_id,$total_login_count);

				//$this->authorize->do_user_login($user_info[0]);
				
				$this->authorize->do_user_login($user_info[0], $user_access, $staff_details);
				
				//$this->authorize->do_user_login($user_info[0]);
				
			
		
				if($user_info[0]->user_type=='superadmin'){
					redirect(base_url().'dashboard');
				  } elseif($user_info[0]->user_type=='admin'){
					redirect(base_url().'admindashboard');
				  } elseif($user_info[0]->user_type=='teacher'){
					redirect(base_url().'teacherdashboard');
				  } elseif($user_info[0]->user_type=='student'){
					redirect(base_url().'studentdashboard');
				   } elseif($user_info[0]->user_type=='enrolementstudent'){
					redirect(base_url().'studentenrolementdashboard');
				   }elseif($user_info[0]->user_type=='enrolementstaff'){
					redirect(base_url().'staffenrolementdashboard');
				   }  else {
					$this->session->set_flashdata('error', 'Invalid login. Please enter correct Username or password.');
					 redirect(base_url());exit;
				   }
				
			  } elseif( $is_active==0 && $is_deleted==0 ) {
				  $this->session->set_flashdata('error', 'Your account is not active yet.');
				   redirect(base_url());exit;
			      }  elseif( $is_active==0 || $is_active==1 && $is_deleted==1 ) {
				  $this->session->set_flashdata('error', 'Your account has been deleted.');
				   redirect(base_url());exit;
			       }
				  
		   }  else {
				  $this->session->set_flashdata('error', 'Invalid login. The access for this school hase been denied by Superadmin');
				   redirect(base_url());exit;
			      }
					  
			   }

			} else {

				$this->session->set_flashdata('error', 'Invalid login. Please enter correct Username or password');

				redirect(base_url());exit;

			}
	}
		
		$data['error']		= $this->session->flashdata('error');

		$data['success']	= $this->session->flashdata('success');

		$this->load->view('login',$data);
				
		
	}
	
	public function lockscreen(){
		$data = array();
		$data['username'] = $this->session->userdata('user_name');
		$this->session->set_userdata('user_loggedin', false);	
		$this->load->view('lockscreen', $data);
	}
	
	

}