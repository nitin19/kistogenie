<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class School_branch_model extends CI_Model {
	
	var $tbl = "branch";
	var $classes	= "classes";
	var $users = "users";
	var $fee_band = "fee_band";
	var $student = "student";
	var $school = "school";
	var $staff = "staff";
	
    function __construct() {
        parent::__construct();
    }
	
	 function getRows($where=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  if($word_search != NULL){
			 //$this->db->where("(branch_name LIKE '%$word_search%')", NULL, FALSE);
			  $this->db->where("(branch_name LIKE '%$word_search%' OR branch_address LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	   function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  if($word_search != NULL){
			// $this->db->where("(branch_name LIKE '%$word_search%')", NULL, FALSE);
			 $this->db->where("(branch_name LIKE '%$word_search%' OR branch_address LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("branch_id", "desc"); 	
		   }
		
		     $this->db->limit($limit, $start);
             $query = $this->db->get();
		     $this->db->last_query(); 
             return  $query->result();

	}
	
	function check_branch_name($branchname, $school_id) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_name"=>$branchname));
		  $query = $this->db->get();
		  $this->db->last_query(); 
          return $query->result();   
	}
	
	 function insertBranch($branchdata){
		if($this->db->insert($this->tbl, $branchdata))
			return $this->db->insert_id();
		  else
			return false;
		echo $this->db->last_query(); exit();
	   }
	
	function check_editedbranch_name($branchname, $school_id, $editedBranchid) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_name"=>$branchname));
		$this->db->where('branch_id !=', $editedBranchid);
		  $query = $this->db->get();
		  $this->db->last_query(); 
          return $query->result();   
	}
	
	
	 function updateBranches($branchdata,$where) {
		$this->db->where($where);
		if($this->db->update($this->tbl, $branchdata))
			return true;
		else
			return false;
		echo $this->db->last_query();exit();
	 }
	
	
	   function get_single_branch($branchid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('branch_id', $branchid);
          $query = $this->db->get();
          return  $query->result();
           echo $this->db->last_query(); exit();
     }
	
	
	 function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	
	
	 function check_student_branch($school_id,$branchid) {
          $this->db->select('*');
          $this->db->from($this->student);
		   $this->db->join('users', 'users.id = '.$this->student.'.user_id','INNER'); 
		 // $this->db->where('class_id', $classid);
		  $this->db->where(array($this->student.".school_id"=>$school_id,$this->student.".student_school_branch"=>$branchid,"users.is_active"=>'1',"users.is_deleted"=>'0'));
           $query = $this->db->get();
		  $this->db->last_query();
          return  $query->result();
         }
		 
		function check_staff_branch($school_id,$branchid) {
          $this->db->select('*');
		  $this->db->from($this->staff);
		   $this->db->join('users', 'users.id = '.$this->staff.'.user_id','INNER'); 
		  $this->db->where(array($this->staff.".school_id"=>$school_id,$this->staff.".branch_id"=>$branchid,"users.is_active"=>'1',"users.is_deleted"=>'0'));
           $query = $this->db->get();
		   $this->db->last_query();
          return  $query->result();
         }
		 
		 
	function delete($branchid){
		$where		= array('branch_id' => $branchid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		 else
			return false;
	   }
	
	/*function check_deletedbranch($school_id,$branchid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		 // $this->db->where('class_id', $classid);
		  $this->db->where(array("school_id"=>$school_id,"branch_id"=>$branchid));
          $query = $this->db->get();
          return  $query->row();
      }*/
	
	
	/*function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		//$this->db->where('school_id', $school_id);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getschoolclasses($school_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		//$this->db->where('school_id', $school_id);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	   function insertClasses($classdata){
		if($this->db->insert($this->tbl, $classdata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	   }*/
	   
	

	   
	   
	   
	 
	
}



 
