<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>
    
        <li class="edit">Classes</li>        

        </ul>

        </div>

        </div>

    	
   <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
  <div style="clear:both"></div>

        

        <div class="attendancesec">	
        
        
        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">
             
             
             <?php 
		$editedClassid = $this->uri->segment(4); 
		$Action = $this->uri->segment(5); 
		if($Action == 'edit') {
		?>
<form action="<?php echo base_url();?>schoolclasses/updateclass/<?php echo $last_page;?>/<?php echo $editedClassid;?>" name="schoolclassForm" id="schoolclassForm" method="post">

<div class="col-sm-12 col-xs-12 applycodediv nopadding">

 <div class="col-sm-7 nopadding selectoption">

    <div class="form-group fullwidthinput">

    <label class="col-sm-2 control-label nopadding filterlabel">Branch:</label>

    <div class="col-sm-10 nopadding selectfilter" style="margin-bottom:35px">

    <div class="col-sm-5 inputbox termselect">

      <select class="form-control" name="schoolbranch" id="schoolbranch">
      <option value="">Select Branch</option>   
      <?php  foreach($branches as $branch) { ?> 
       <option <?php if($branch->branch_id == $info->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>  
         <?php } ?>
     </select>

    </div>
    <label class="col-sm-2 control-label nopadding filterlabel">Class:</label>
    <div class="col-sm-5 inputbox termselect">
    <input type="text" name="classname" id="classname" value="<?php echo $info->class_name;?>"  placeholder="Enter class name" class="form-control" >
        </div>
     </div>
     
    </div>
    
    
    <div class="form-group fullwidthinput">

    <label class="col-sm-2 control-label nopadding filterlabel">Description:</label>

    <div class="col-sm-10 nopadding selectfilter" id="selectfilter">
    <div class="col-sm-12 inputbox termselect">

       <textarea  class="form-control" name="classdescription" id="classdescription" placeholder="Write Description" style="resize:none" cols="num" rows="num"> <?php echo $info->class_description;?> </textarea>
    </div>
   

      <div class="confirmlink">
        <input type="submit" class="open1" value="Update">
        </div>
        
    
      </div>
    </div>
    
      </div>
    </div>

    </form>
    
     <?php } else { ?>
     

     <form action="<?php echo base_url();?>schoolclasses/addclass" name="schoolclassForm" id="schoolclassForm" method="post" autocomplete="off">


<div class="col-sm-12 col-xs-12 applycodediv nopadding">

 <div class="col-sm-7 nopadding selectoption">

    <div class="form-group fullwidthinput">

    <label class="col-sm-2 control-label nopadding filterlabel">Branch:</label>

    <div class="col-sm-10 nopadding selectfilter" style="margin-bottom:35px">

    <div class="col-sm-5 inputbox termselect">

      <select class="form-control" name="schoolbranch" id="schoolbranch">
      <option value="">Select Branch</option>   
      <?php  foreach($branches as $branch) { ?> 
       <option value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>  
         <?php } ?>
     </select>

    </div>
    <label class="col-sm-2 control-label nopadding filterlabel">Class:</label>
    <div class="col-sm-5 inputbox termselect">
    <input type="text" name="classname" id="classname" value=""  placeholder="Enter class name" class="form-control" maxlength="61" >
        </div>
     </div>
     
    </div>
    
    
    <div class="form-group fullwidthinput">

    <label class="col-sm-2 control-label nopadding filterlabel">Description:</label>

    <div class="col-sm-10 nopadding selectfilter" id="selectfilter">
    <div class="col-sm-12 inputbox termselect">

    <textarea  class="form-control" name="classdescription" id="classdescription" placeholder="Write Description" maxlength="251" style="resize:none" cols="num" rows="num"> </textarea>
    </div>
   

      <div class="confirmlink">
        <input type="submit" class="open1" value="Add">
        </div>
        
    
      </div>
    </div>
    
      </div>
    </div>

    </form>
        
    <?php } ?>
  

        </div>

    </div>
        

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

  <form action="<?php echo base_url();?>schoolclasses/index" name="classfilterform" id="classfilterform">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding">

				<div class="col-sm-7 nopadding selectoption">

<div class="form-group fullwidthinput">

    <label class="col-sm-1 col-xs-1 control-label nopadding filterlabel">Show</label>

    <div class="col-sm-11 col-xs-11 nopadding selectfilter">

    <div class="col-sm-4 col-xs-4 inputbox termselect">

      <select class="form-control" name="branch" id="branch">
  <option value="">Select Branch</option>    
  <?php foreach($branches as $branch) { ?>
     <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
    <?php  } ?>
</select>

    </div>

    <div class="col-sm-4 col-xs-4 inputbox termselect">

<select class="form-control" name="status" id="status">
  <option value="">Select Status</option>    
   <option <?php if($status_search == "1"){ echo 'selected'; } ?> value="1">Active</option>
   <option <?php if($status_search == "0"){ echo 'selected'; } ?> value="0">Inactive</option>
</select>

    </div>

    </div>

  </div>

</div>		

<div class="col-sm-3 col-xs-5 searching termsearch nopadding">

        <div class="form-group">

      <input type="text" class="form-control searchbox" placeholder="Search By Class" name="seachword" id="seachword" value="<?php echo $word_search;?>">

   <!--  <input type="submit" value="" class="searchbtn">-->

     </div>

        </div>

        <div class="col-sm-2 col-xs-7 rightspace viewreport">

<div class="form-group">

    <input type="submit" class="btn btn-danger srchbtn" value="Find Class">

  </div>

</div>

        </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <h1>Classes<span><?php echo $Totalrec = $total_rows;?></span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2">S.No.</th>
                      
                       <th class="column5">Class</th>
                       
					  <th class="column5"> Branch </th>

                     <th class="column4">Status</th>
                     
                      <th class="column4">Action</th>

				  </tr>

			  </thead>

				<tbody>

					<?php 
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $classes) { 
					     $sr++;
						  $branch_id = $classes->branch_id;
						  $this->load->model(array('school_classes_model'));
						  $branchName = $this->school_classes_model->getbranchName($branch_id);
					?>
					<tr class="familydata students">
						<td class="column2"><?php echo $sr; ?></td>
                        <td class="column5"><?php 
							if($classes->class_name!=''){
								echo $classes->class_name;
								}else{
								echo '<span style="color:transparent;">-</span>'; }
							?></td>
                            
                        <td class="column5"><?php 
								if($branchName->branch_name!=''){
								echo @$branchName->branch_name;
								}else{
								echo '<span style="color:transparent;">-</span>'; }
								?> </td>   
                                               
                        <td class="column4"><span class="acceptbtn statusbtn <?php if($classes->is_active==1) { echo ' Active';}else{echo ' Inactive'; } ?>"><?php if($classes->is_active==1) { echo 'Active'; } else { echo 'Inactive'; } ?></span></td>
                        
                        
                        <td class="column4">
											<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
                                                
          <!-- <li> <a href="<?php echo base_url();?>students/view_student/<?php echo $classes->id;?>"> View  </a> </li>-->                                    
	      <li><a href="<?=base_url();?>schoolclasses/index/<?php echo $last_page;?>/<?php echo $classes->class_id;?>/edit<?php echo $post_url;?>"> Edit </a></li>
          
				<li>
				<?php if($classes->is_active == '1'){ ?>
	<a href="<?php echo base_url();?>schoolclasses/deactivateclass/<?php echo $last_page;?>/<?php echo $classes->class_id;?><?php echo $post_url;?>" title="click to deactivate class"> Active </a>
					<?php
					 } else {
					?>
	<a href="<?=base_url();?>schoolclasses/activateclass/<?php echo $last_page;?>/<?php echo $classes->class_id;?><?php echo $post_url;?>" title="click to activate class">De-Activated </a>
				<?php } ?>
				</li>
                
			<li class="divider"></li>
				 <li><a onclick="delConfirm('<?php echo $classes->class_id;?>')" style="cursor: pointer;"> Delete </a> </li>
									</ul>
								</div>
											
							</td>
                                        
                                        
                        </tr>
				<?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px;height: 100px;font-size:25px ; background:#FFF;  color: #6a7a91;">No record to show.</th></tr>	
				   <?php } ?>

				</tbody>

		  </table>

     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-9 col-xs-6 paginationblk">
         <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
			<?php  } ?>
	<ul class="pagination">
		
		<?php echo $pagination;?>
        

	</ul>

    </div>

    <div class="col-sm-3 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total:&nbsp;<?php echo $Totalrec ;?> </h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>schoolclasses/index" method="get">
       
           <input type="hidden" name="status" id="status" value="<?php echo $status_search; ?>"  />
       
          <input type="hidden" name="seachword" id="seachword" value="<?php echo $word_search; ?>"  />  
        
          <input type="hidden" name="branch" id="branch" value="<?php echo $branch_search; ?>"  />

          <select class="form-control" name="perpage" id="perpage">
        
          <option value="">Select</option>
        
          <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>
        
          <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>
        
          <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>
        
          <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>
        
          <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>

    </div>

	</div>

    </div>     

</div>





	</div>

    

	</div>

    </div>

    </div>
    
        
	
 
 <script>
  jQuery(document).ready(function(){
	  
	  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
					
  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });
	jQuery.validator.addMethod("alphaUname", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/);
    });
	
	jQuery.validator.addMethod("alphaFLname", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaUpwd", function(value, element) {
    return this.optional(element) || value == value.match(/^(?=.*[a-zA-Z0-9])[a-zA-Z0-9!@#$%&*.]{7,}$/);
    });
	
	jQuery.validator.addMethod("pwcheck", function(value) {
   return /[!,%,&,@,#,$,^,*,?,_,~]/.test(value) // consists of only these
       && /[a-z]/.test(value) // has a lowercase letter
       && /\d/.test(value) // has a digit
	  
});
	
	
    jQuery("#schoolclassForm").validate({
        rules: {
                schoolbranch: {
                   required: true,
               },
			   classname: {
                   required: true,
                   lettersonly_comp: true,
     			   maxlength: 100
               },

			  
        },
        messages: {
              schoolbranch: {
                  required: "Please select branch."
                 },
			  classname: {
                  required: "Please enter class.",
				  maxlength: "Maximum 60 characters allowed."
                 },

            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		  jQuery.validator.addMethod("lettersonly_comp", function(value, element) {
  return this.optional(element) || /^[0-9+a-z' ]+$/i.test(value) || /^[A-Z]+$/i.test(value);
}, "Letters and Number only please"); 
	});
</script>
 
  
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>schoolclasses/deleteclass/"+id;
		}else{
			return false;
		}
	}
</script>	

<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.fullwidthinput select{  background-position: 90% center;}
.Inactive {background: #ff0000 none repeat scroll 0 0;}
/*<!--.cleanSearchFilter {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533 !important;
    border-radius: 3px;
    color: #fff !important;
    padding: 9px;
    text-decoration: none !important;
}-->*/
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px 0 0;
}

.srchbtn{
   padding: 6px 30px;
}
@media screen and (min-width:320px) and (max-width:480px){
.cleanSearchFilter {
    padding: 8px 5px !important;
}
.termsearch {
    padding-bottom: 10px !important;
}
.searching {
    width: 100% !important;
}
.paginationblk {
    text-align: center !important;
}
.paginationselbox {
    justify-content: end;
}
}
@media screen and (min-width:481px) and (max-width:767px){ 
#classfilterform .inputbox.termselect {
    padding: 0 10px;
    width: 50%;
}
.selectoption {
    width: 100% !important;
	padding-bottom: 10px;
}
}
@media screen and (min-width:768px) and (max-width:991px){
.selectoption .inputbox.termselect {
    padding: 0 10px;
}
#schoolclassForm .selectfilter {
    padding-left: 35px;
}
.selectoption {
    width: auto;
}
#classfilterform .filterlabel {
    width: auto;
}
#classfilterform .selectoption {
    width: 100%;
}
#classfilterform .inputbox.termselect {
    padding-bottom: 10px;
    width: 100%;
}
#classfilterform .nopadding.selectfilter {
    width: 100%;
}
#classfilterform .termsearch {
    padding-bottom: 10px;
    width: 100%;
}
.viewreport {
    padding-left: 7px;
}
.confirmlink {
    padding-right: 10px;
}
}
@media screen and (min-width:992px) and (max-width:1047px){
.cleanSearchFilter {
    padding: 9px 5px;
}
.btn {
    padding: 6px 8px;
}
#seachword {
    width: 100%;
}
.statusbtn {
    padding: 6px 10px;
}
.totaldiv h3 {
    text-align: left;
}
}
@media screen and (min-width:1048px) and (max-width:1280px){
.statusbtn {
    font-size: 9px;
    padding: 6px 10px;
}
}
</style>	