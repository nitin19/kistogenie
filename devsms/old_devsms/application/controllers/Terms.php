<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','term_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){
		
		$id = $this->uri->segment(4);	
		$data=array();
		$search_condition=array();
		$school_id		= $this->session->userdata('user_school_id');
		
		$term_srch			= isset($_GET['term_srch'])?$_GET['term_srch']:NULL;
		$branch_search		= isset($_GET['branch'])?$_GET['branch']:NULL;
		if($branch_search!=''){
			$search_condition['branch.branch_id']	= $branch_search;
		}
		$totrows = $this->term_model->total_terms($search_condition,$school_id,$term_srch);
	
		$data["total_rows"] = count($totrows);
		$data["base_url"] = base_url() . "terms/index";
		
			 $data["per_page"]						= 10;
		
		 $data["uri_segment"] = 3;
		 $this->pagination->initialize($data);
		  $page =  $this->uri->segment(3,0) ;
		  $data['last_page']				= $page;
          $data["links"] = $this->pagination->create_links();
		
		  
		//$data['branch'] = $this->term_model->get_branch($school_id);
		
		$data['detail'] = $this->term_model->get_terms($search_condition,$school_id,$data["per_page"], $page,$term_srch);
					
		$roledata = $this->term_model->get_single_record($id);
		
		$data['info']		= $roledata;	
				
		$this->load->view('header');

		$this->load->view('master/term', $data);
	
		$this->load->view('footer');
		
				}
				
				
		public function add_new_term(){
		
		$termdata = array();
		
		$school_id		= $this->session->userdata('user_school_id');					
		$termdata['school_id'] = $school_id;
		/*********************** chnages in term *****************/
		
/*		 if (trim($this->input->post('branch')) != '') {
		 $termdata['branch_id'] = trim($this->input->post('branch'));
		 }*/
		 
		 if(trim($this->input->post('termname'))!= ''){
    	 $termdata['term_name'] = trim($this->input->post('termname'));
		 }
		  if(trim($this->input->post('description'))!= ''){
		 $termdata['term_description'] = trim($this->input->post('description'));
		  }
		     $termdata['is_active'] = 1 ;
		    $termdata['is_deleted'] = 0 ;
		    $termdata['created_by'] = $this->session->userdata('user_name');
			$termdata['created_date'] = date('Y-m-d H:i:s');
			$termdata['updated_date'] = date('Y-m-d H:i:s');
						
			$term = trim($this->input->post('termname'));
			/********************************** changes in term **************************************************/
/*			$branch = trim($this->input->post('branch'));
						
		$checkterm_name = $this->term_model->check_term_name($branch, $term, $school_id);*/
		
		$checkterm_name = $this->term_model->check_term_name($term, $school_id);
		           $countterm = count($checkterm_name);
				   
				   if($countterm > 0 ) {
					   
					   $this->session->set_flashdata('error', 'Term already exists in this Branch. Term can not be added.');
				       redirect(base_url()."terms/");exit;
				
				     } else {
					     if($this->term_model->insert_term_name($termdata)) {
							$this->session->set_flashdata('success', 'Term has been added successfully.');
							redirect(base_url()."terms/");exit; 
						 } else {
							$this->session->set_flashdata('error', 'Some problem exists. Term has not been added.');
							redirect(base_url()."terms/");exit; 
						 }
				   }
	     }
		 
		 
		 		 
		 public function update_term(){
			 $id 		= 		$this->uri->segment(4);
			 $page 		= 		$this->uri->segment(3);
			 $school_id	= 		$this->session->userdata('user_school_id');
			 $data		= 		array();
			 $id 		=		trim($this->input->post('id'));
			 
			 $data = array(
			//'branch_id' => trim($this->input->post('branch')),
			'term_name' 		=>		 trim($this->input->post('termname')),
			'term_description'  => 		 trim($this->input->post('description'))
			); 
			$termname 			= 		trim($this->input->post('termname'));
/*			$branch 			= 		trim($this->input->post('branch'));
			$checkterm_name 	= 		$this->term_model->check_update_term_name($branch, $termname, $school_id, $id);*/
			
			$checkterm_name 	= 		$this->term_model->check_update_term_name($termname, $school_id, $id);
		     $counttermname 	= 		count($checkterm_name);
				   
				   if($counttermname > 0 ) {
					    $this->session->set_flashdata('error', 'Term already exists in this Branch.Term has not been Updated.');
				       redirect(base_url()."terms/index/".$page."/".$id);exit;
				
				     } else {
					$updatedata = $this->term_model->update_term($id,$data);
					 if($updatedata = 1){
					 $this->session->set_flashdata('success', 'Term has been updated successfully.');
				redirect(base_url()."terms/index/".$page."/".$id);exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Term has not been updated.');
				redirect(base_url()."terms/index/".$page."/".$id);exit;
			   }
			 }
		 }
		 
	
	
		public function role_action(){
			$id         = $this->uri->segment(3);  
			$activity   = $this->uri->segment(4);
			$data		= array();
			if($activity == 'active'){ 
			 $data = array(
				'is_active' => '1'
				);
		  } 
		  if($activity == 'inactive'){ 
			 $data = array(
				'is_active' => '0'
				);
		  } 
		  if($activity == 'delete'){ 
			 $data = array(
				'is_deleted' => '1'
				);
		  } 
		  $actiondata = $this->term_model->term_action($id, $data);
		  if($actiondata == 1){	
				$this->session->set_flashdata('success', 'Term has been updated successfully.');
				redirect(base_url()."terms/");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Term has not been updated.');
				redirect(base_url()."terms/");exit;
			   }
			  
		  }
		  
		  public function del_term($term_id){
			  
			  $id = $this->uri->segment(3); 
			  $data = array(
				'is_deleted' => '1'
				);
				
			 $school_id = $this->session->userdata('user_school_id');
			 
	 
			 $chk_studentprogess_term = $this->term_model->check_studentprogress_term($school_id,$id);
			
			$countStudentprogressterm = count($chk_studentprogess_term); 
			 			
			   if( $countStudentprogressterm > 0 ) {
				  $this->session->set_flashdata('error', 'Term has not been deleted. As this term is assigned to students progress report');
				  redirect(base_url()."terms/index/");exit;
				 } else {
			
			  $deldata = $this->term_model->update_term($id,$data);
			  if($deldata = 1){	
				$this->session->set_flashdata('success', 'Term has been deleted successfully.');
				redirect(base_url()."terms/");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Term has not been deleted.');
				redirect(base_url()."terms/");exit;
			   }
			  }
}}

