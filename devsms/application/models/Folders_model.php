<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Folders_model extends CI_Model {
	var $tbl	= "document_folder";
	var $document_folder_attachments = "document_folder_attachments";
	var $users = "users";
	
    function __construct() {
        parent::__construct();
    }
	
	
	function getFolderdata($school_id,$Folder_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"parent_folder="=>$Folder_id,"is_active="=>'1',"is_deleted="=>'0'));
		$this->db->order_by("folder_id", "desc");
        $query = $this->db->get();
        return  $query->result();
    }
	
	
	function getFolderfiles($school_id,$Folder_id) {
        $this->db->select('*');
        $this->db->from($this->document_folder_attachments);
		$this->db->where(array("school_id="=>$school_id,"folder_id="=>$Folder_id,"is_active="=>'1',"is_deleted="=>'0'));
		$this->db->order_by("folder_attachment_id", "desc");
        $query = $this->db->get();
        return  $query->result();
    }
	
	
	 function chk_folder_name($foldername) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where('folder_name', $foldername);
        $query = $this->db->get();
        return  $query->result();
     }
	 
	 	function insertfolder($folderdata){
		if($this->db->insert($this->tbl, $folderdata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 function insertDocuments($attachmentdata){
		if($this->db->insert($this->document_folder_attachments, $attachmentdata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
	 function chk_foldername($foldername, $parent, $school_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"parent_folder="=>$parent,"is_active="=>'1',"is_deleted="=>'0', "folder_name="=>$foldername));
        $query = $this->db->get();
        return  $query->result();
     }
	 
	 function searchFolder($school_id, $filesearch) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
		if($filesearch != NULL){
			$this->db->where("(folder_name LIKE '%$filesearch%')", NULL, FALSE);
		}
		$this->db->order_by("folder_id", "desc");
        $query = $this->db->get();
        return  $query->result();
    }
	
	
	function searchfiles($school_id, $filesearch) {
        $this->db->select('*');
        $this->db->from($this->document_folder_attachments);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
		if($filesearch != NULL){
			$this->db->where("(attachment_name LIKE '%$filesearch%')", NULL, FALSE);
		}
		$this->db->order_by("folder_attachment_id", "desc");
        $query = $this->db->get();
        return  $query->result();
    }
	
		  function deletefile($id, $data){
$this->db->where('folder_attachment_id', $id);
$result = $this->db->update($this->document_folder_attachments, $data);
return $result;
}

  function getParentFolderName($school_id, $Parentfolderid) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"folder_id="=>$Parentfolderid,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
	}
	
	function getMainParentFolderName($school_id, $mainParentfolderId) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"folder_id="=>$mainParentfolderId,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
	}

}
