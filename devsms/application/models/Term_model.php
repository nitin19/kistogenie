<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Term_model extends CI_Model {
	
	 var $tbl = 'term';
	
	 var $branch = 'branch';
	
	 var $staff = 'staff';
	 var $school='school';
	//var $username	= "username";
	//var $password	= "password";

    function __construct() {
        parent::__construct();
    }
	
	function get_branch($school_id){
	 $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
         return $query->result();
	}
	
	
/*	function check_term_name($branch, $term, $school_id){
		$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"term_name"=>$term ));
		$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result(); 
		
		}*/
 	function insert_term_name($termdata){
		if($this->db->insert($this->tbl, $termdata))
			return $this->db->insert_id();
		  else
			return false;
	}
 
/*  function get_terms($where=NULL,$school_id,$limit=NULL, $offset=NULL,$term_srch){
	   $this->db->limit($limit, $offset);
	 	$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', $this->branch.".is_deleted"=>'0', $this->branch.".is_active"=>'1'));
		
		if($where != NULL){
		$this->db->where($where);
		}
		if($term_srch != NULL){
			$this->db->where("($this->tbl.term_name LIKE '%$term_srch%')", NULL, FALSE);
		}
		
		$this->db->order_by($this->tbl.".term_id", "desc");
		$query = $this->db->get(); 
		$this->db->last_query(); 
        return $query->result();
	}*/
	
/*	function total_terms($where=NULL, $school_id, $term_srch){
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', $this->branch.".is_deleted"=>'0', $this->branch.".is_active"=>'1'));
	
		if($where != NULL){
		$this->db->where($where);
		}
		if($term_srch != NULL){
			$this->db->where("($this->tbl.term_name LIKE '%$term_srch%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".term_id", "desc");
		$query = $this->db->get(); 
		 $this->db->last_query(); 

        return $query->result();
	}
	*/
	
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
      	 $query = $this->db->get(); 
        return  $query->row();
    }
	
	function term_action($id,$data){
		 $this->db->where('term_id', $id);
		 if($this->db->update($this->tbl, $data))
		 	return true;
		else
			return false;
	}

	function getterm($term_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
  		$this->db->where('term_id', $term_id);
        $query = $this->db->get();
        return  $query->row();
	}
	
	function get_single_record($id) {
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where('term_id', $id);
		$query = $this->db->get();
		$this->db->last_query(); 
		return $query->row();
		}
	
		
/*	function check_update_term_name($branch, $termname, $school_id, $id){
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"term_name"=>$termname));
		$this->db->where('term_id !=', $id);
		  $query = $this->db->get();
		 $this->db->last_query(); 
        return $query->result();   
	}*/

	 function update_term($id,$data){
		$this->db->where('term_id', $id);
		$this->db->update($this->tbl, $data);
}
/* *********************************** changes in term *******************************/

	function check_term_name($term, $school_id){
		$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"term_name"=>$term ));
		$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result(); 
		
		}
		
	function check_update_term_name($termname, $school_id, $id){
	 	$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"term_name"=>$termname));
		$this->db->where('term_id !=', $id);
    	$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();   
	}
	function getschoolName($school_id) {
        $this->db->select('*');
        $this->db->from($this->school);
  		$this->db->where('school_id', $school_id);
        $query = $this->db->get();
        return  $query->row();
    }
	function total_terms($where=NULL, $school_id, $term_srch){
	 	$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0'));
	
		if($where != NULL){
		$this->db->where($where);
		}
		if($term_srch != NULL){
			$this->db->where("($this->tbl.term_name LIKE '%$term_srch%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".term_id", "desc");
		$query = $this->db->get(); 
		 $this->db->last_query(); 

        return $query->result();
	}
	
	  function get_terms($where=NULL,$school_id,$limit=NULL, $offset=NULL,$term_srch){
	   $this->db->limit($limit, $offset);
	 	$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0'));
		
		if($where != NULL){
		$this->db->where($where);
		}
		if($term_srch != NULL){
			$this->db->where("($this->tbl.term_name LIKE '%$term_srch%')", NULL, FALSE);
		}
		
		$this->db->order_by($this->tbl.".term_id", "desc");
		$query = $this->db->get(); 
		$this->db->last_query(); 
        return $query->result();
	}
	
}
