
<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">
   

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="#">Account Details </a></li>        

        </ul>

        </div>

        <!--<div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>-->

        </div>

    	

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-12 col-xs-7 nopadding profile-bg accsection">

        <h1>Account Details</h1>

        </div>

        </div>

        <div class="col-sm-6 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

      

        <div class="profile-bg prfltitle profile_titlebg">

        <h1>General details</h1>
<div class=" col-sm-12 nopadding contentdiv">
  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Username</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->username; ?></span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Password</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->password; ?></span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Avtar<br/><span class="tagline">JPG, max. 500KB</span></label>

    <div class="col-sm-9 col-xs-8 profile-picture inputbox">
    
	<?php if($info->profile_image!='') { ?> <img src="<?php echo base_url();?>uploads/staff/<?php echo $info->profile_image; ?>"> 
     <?php } else { ?> 
     <img src="<?php echo base_url();?>assets/images/avtar.png">
     <?php } ?>

     <!-- <div class="profilepicbtns"><i class="fa fa-picture-o"></i><br/><hr class="borderline"><i class="fa fa-trash-o"></i></div>-->

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-3 col-xs-4 control-label nopadding">Full Name</label>

    	<div class="col-sm-9 col-xs-8 inputinfo">

      	<span><?php echo $info->staff_fname. ' ' . $info->staff_lname; ?></span>

    	</div>

  		</div>

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Date of Birth</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo date('d M Y', strtotime($info->staff_dob));?></span>

    </div>

  </div>

        <div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Title</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->staff_title;?></span>

    </div>

  </div>

       

  	

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Telephone</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->staff_telephone;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Full Address</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->staff_address;?></span>

    </div>

  </div>
  
  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Address(optional)</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->staff_address_1;?></span>

    </div>

  </div>
  
<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Qualification</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->staff_education_qualification;?></span>

    </div>

  </div>
  
    		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Personal Summary</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->staff_personal_summery?></span>

    </div>

  </div>

</div>
</div>
        

        </div>

        </div>

        <div class="col-sm-6 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

      

        <div class="profile-bg prfltitle profile_titlebg">

        <h1>Education details</h1>
    <div class="col-sm-12 tablediv nopadding">
	    <div class="tablewrapper">
        <table class="table-bordered table-striped" id="edurecord">
        
        <?php if(count($edu_info)>'0'){
			?>
			
			<thead>
				  <tr class="headings">
					  <th class="column5">Degree</th>
                      <th class="column5">University</th>
					  <th class="column4">Start Year</th>
					  <th class="column4">End Year</th>
					 <th class="column2">Grade</th>
					
					  
				  </tr>
			  </thead>
              <?php foreach($edu_info as $educationdata){?>
              <tbody > <tr class="familydata educationlisting">
						<td class="column5 numeric"><?php echo $educationdata->degree; ?></td>
                        <td class="column5"><?php echo $educationdata->university; ?></td>
						<td class="column4"><?php echo $educationdata->start_year; ?></td>
						<td class="column4"><?php echo $educationdata->end_year; ?></td>
						<td class="column2"><?php echo $educationdata->percentage;?></td>

                        </tr> </tbody>
              
           <?php }} else { ?>   
               <tbody>
            <tr><th colspan="7" style="text-align: center; width:999px;height:100px; background:#FFF; color: #6a7a91;;font-size:25px;">No record to show.</th></tr>
            </tbody>
              <?php } ?>
              
			</table>
        
          </div>
        
		  </div>
</div>
        <div class="profile-bg prfltitle profile_titlebg">

        <h1>Experience details</h1>
   <div class="col-sm-12 tablediv nopadding">
	    <div class="tablewrapper">
        
        <table class="table-bordered table-striped" id="experncdetail">
        
         <?php if(count($exp_info)>0){?>
			  <thead>
				  <tr class="headings">
					  <th class="column7">Company Name</th>
                      <th class="column5">Position</th>	
					  <th class="column4">Start Year</th>
					  <th class="column4">End Year</th>
					  				  
				  </tr>
			  </thead>
              
                <?php foreach($exp_info as $experiencedata){?>
				<tbody>
					<tr class="familydata experiencelist">
						<td class="column7 numeric"><?php echo $experiencedata->company_name; ?></td>
                        <td class="column5"><?php echo $experiencedata->position; ?></td>
						<td class="column4"><?php echo $experiencedata->exp_startyear; ?></td>
						<td class="column4"><?php echo $experiencedata->exp_endyear; ?></td>
   
                        </tr>

					</tbody>
                    
           <?php }} else { ?>   
               <tbody>
            <tr><th colspan="7" style="text-align: center; width:999px;height:100px; background:#FFF; color: #6a7a91;;font-size:25px;">No record to show.</th></tr>
            </tbody>
              <?php } ?>
		  </table>
     
          </div>
		  </div>
</div>

        </div>

        </div>

        </div>
        
       

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script> 


<style>
.dashboard-attendance .statusdiv {
    width: 100%;
}
.icon_section {
    float: right;
    padding: 0 15px 0 0;
}
.cetificate_section {
    padding-left:0px;
	list-style:none;
	 font-size: 15px;
}
.cetificate_section > li {
    border: 1px solid #ccc;
    padding: 5px 10px 5px 15px;
}
.prfltitle {
    padding: 0 0px 15px;
}
.fa.fa-download + .tooltip > .tooltip-inner {background-color:#57B94A !important; color:#fff;}
.fa.fa-download + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}
.fa.fa-eye + .tooltip > .tooltip-inner {background-color: #57B94A !important; color:#fff;}
.fa.fa-eye + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}
.cetificate_section li h4 {
    border-bottom: 1px solid #ccc;
    padding-bottom: 10px;
}
.cetificate_section > li {
    border: medium none;

}
.profile-bg {
    border: medium none;
	padding: 0px;
}
.cetificate_section li h4 {
    color: #354052 !important;
    font-size: 14px;
}
.prfltitlediv {
    margin-bottom: 15px;
}
.profile_titlebg h1{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1);  padding: 10px 15px; }
.profiletitltbox{padding-top:0px;padding-bottom:0px;}
.profiletitltbox .profile_titlebg {
    padding: 0;
}
.contentdiv{
	padding:15px 15px 0px 15px;

}
.accsection h1 {
    padding: 0 0 10px 15px;
}

.docsec{
	margin-bottom:0px;}
.grphdiv{
	margin-left:15px;
}
.prfilleft {
    padding: 0;
}

.tablediv {

    padding-bottom:0px !important;

}
</style> 