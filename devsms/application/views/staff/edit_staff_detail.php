<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

     <li><a href="<?php echo base_url(); ?>staff">Staff</a></li>

          <li><a href="<?php echo base_url(); ?>staff">Edit</a></li> 

        <li class="edit"><a href="#">Details <?php echo $staffdata->staff_fname. ' ' . $staffdata->staff_lname; ?></a></li>

             

        </ul>

        </div>

        </div>	
        <!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>

<div class="col-sm-12 nopadding profile-bg accsection">
 <h1>Edit Details</h1>
 </div>
<div class="col-sm-12 nopadding edit_profile">

 <form action="<?php echo base_url(); ?>staff/update" method="post" name="editstaffprofileForm" id="editstaffprofileForm" enctype="multipart/form-data">
 
  <div class="col-sm-7 formleft">
<div class="editprofileform editprofile_left">
      <div class="profile-bg prfltitle profile_titlebg">

       <h2>Personal details</h2>
           
       <div class="col-sm-9 inputbox">
      <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $staffdata->user_id; ?> ">
        </div>
        <div class="col-sm-12 nopadding contentdiv">
        
  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">First Name<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="text" class="form-control" name="firstname" id="firstname" placeholder="" value="<?php echo $staffdata->staff_fname ; ?>" maxlength="61">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Last Name<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="text" class="form-control" name="lastname" id="lastname" placeholder="" value="<?php echo $staffdata->staff_lname ; ?>" maxlength="61">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Username<span>*</span></label>

    <div class="col-sm-9 inputbox">
 <input type="text" class="form-control" name="username" id="username" placeholder="" value="<?php echo $staffdata->username; ?>" maxlength="61">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Password<span>*</span></label>

    <div class="col-sm-9 inputbox">

       <input type="text" class="form-control" name="password" id="password" placeholder="" value="<?php echo $staffdata->password; ?>" maxlength="16">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Avtar<br/><span class="tagline">JPG, max. 500KB</span></label>

    <div class="col-sm-9 profile-picture inputbox">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"/>
<?php 
if($staffdata->profile_image == ''){
?>
<img src="<?php echo base_url();?>assets/images/avtar.png" id="ppp">
<?php } else { ?>
      <img src="<?php echo base_url();?>uploads/staff/<?php echo $staffdata->profile_image ; ?>" id="ppp">
      <?php } ?>
<img src="" id="ppp1" >

      <div class="profilepicbtns"><i class="fa fa-picture-o"></i><span class="name" style="display:none">No file selected</span><br />
      <input type="file" name="profile_image" id="profile_image" style="display:none;"/>
      <hr class="borderline"><a href="<?php echo base_url() . "staff/delete_img/". $staffdata->user_id."/del" ;?>"><i class="fa fa-trash-o" ></i></a></div>
    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Title<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <select class="form-control" name="title" id="title">
  <option value="">Select Title  </option>

  <option <? if($staffdata->staff_title == "ms."){ echo 'selected'; } ?> value="ms.">Ms.</option>

  <option <? if($staffdata->staff_title == "mr."){ echo 'selected'; } ?> value="mr.">Mr.</option>

  <option <? if($staffdata->staff_title == "mrs."){ echo 'selected'; } ?> value="mrs.">Mrs.</option>

</select>

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Date of Birth<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="text" name="MyDate1" id="MyDate1" class="datepicker" placeholder="DD-MM-YYYY" value=<?php if( $staffdata->staff_dob=='0000-00-00' || $staffdata->staff_dob=='1970-01-01' ) { echo ''; } else { echo date('d-m-Y', strtotime($staffdata->staff_dob)) ; } ?>>

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Email<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="email" class="form-control" name="email" id="email" placeholder="" value="<?php echo $staffdata->email ; ?>" maxlength="121">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Telephone<span>*</span></label>

    <div class="col-sm-9 inputbox">

     <input type="text" id="phone" name="phone" class="form-control" value="<?php echo $staffdata->staff_telephone ; ?>" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" >

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Address<span>*</span></label>

    <div class="col-sm-9 inputbox">

     <input type="text" id="address" name="address" class="form-control" value="<?php echo $staffdata->staff_address ; ?>">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Address(Optional)</label>

    <div class="col-sm-9 inputbox">

     <input type="text" id="optionaladdress" name="optionaladdress" class="form-control" value="<?php echo $staffdata-> staff_address_1 ; ?>">

    </div>

  </div>
    
  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Qualification</label>

    <div class="col-sm-9 inputbox">
    
    <input type="text" name="quali" id="quali" class="form-control" value="<?php echo $staffdata->staff_education_qualification; ?>">

    </div>

  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Account number</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_acc_no" id="staff_acc_no" class="form-control" value="<?php echo $staffdata->account_no; ?>" onKeyUp="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Sort code</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_sort_code" id="staff_sort_code" class="form-control" value="<?php echo $staffdata->sort_code; ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">National insurance</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_insurance" id="staff_insurance" class="form-control" value="<?php echo $staffdata->staff_insurance; ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Hourly rate</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_hourly_rate" id="staff_hourly_rate" class="form-control" onkeypress="return isNumberKey(event)" value="<?php echo $staffdata->hourly_rate; ?>">
    </div>
  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Personal Summary</label>

    <div class="col-sm-9 inputbox">

     <textarea class="form-control" rows="3" id="persummary" name="persummary"><?php echo $staffdata->staff_personal_summery ; ?></textarea><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="Enter your previous experience/s or achievements,if any." ></span>

    </div>

  </div>


 </div>
 </div>
       <div class="col-sm-12 nopadding">
        <div class="col-sm-6 confirmlink link nopadding">
  
 <input type="submit" class="btn btn-default" id="updateBtn" name="update" value="Update Staff Detail">
        </div>
  
  </div>
 </div>
 </div>
 
        
        <!--<div class="col-sm-12 cancelconfirm updatestaffbtn">

        <div class="col-sm-12 nopadding">

        <div class="confirmlink">
 <input type="submit" class="btn btn-default" id="updateBtn" name="update" value="Update Staff Detail">
        </div>

        </div>

        </div>-->

		</form>


    </div>    
    </div>

        
        <script>
  jQuery(document).ready(function(){
	  
	  $("#MyDate1").keydown(false);

	jQuery.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });  	  
   var v = jQuery("#editstaffprofileForm").validate({
		ignore: [],
        rules: {
              username: {
                required: true,
				maxlength: 60
               },
			  password: {
                required: true,
				minlength: 5,
         		maxlength: 15
               },
			  email: {
                required: true,
		  		email: true,
		  		maxlength: 120,
				remote:'<?php echo base_url(); ?>staff/edit_check_email/<?php echo $staffdata->user_id; ?>'
               },
			   
			  firstname: {
                  required: true,
				  alphaLetter: true,
				  maxlength: 60
                 },
			  lastname: {
                   required: true,
				   alphaLetter: true,
				   maxlength: 60
                 },
			  title: {
                required: true
               },
			  MyDate1: {
                  required: true
                 },
			 phone: {
				 required: true,
				 number: true,
                 minlength: 11
               },
			    address: {
                required: true
               }
			  
			  
          },
        messages: {
                username: {
                  required: "Please enter username",
				  maxlength: "Maximum 60 characters allowed."
                 },
				password: {
                  required: "Please enter password",
				  minlength: "Minimum 5 characters required.",
                  maxlength: "Maximum 15 characters allowed."
                 },
				email: {
                  required: "Please enter email address.",
				  email: "Please enter a valid email address.",
				  maxlength: "Maximum 120 characters allowed.",
				  remote: "Email already in use."
                 },
				 
				 firstname: {
                   required: "Please enter firstname",
				   alphaLetter: "Letters only please",
				   maxlength: "Maximum 60 characters allowed."

                 },
			   lastname: {
                   required: "Please enter lastname",
				   alphaLetter: "Letters only please",
				   maxlength: "Maximum 60 characters allowed."
                 },
			  title: {
                  required: "Please select title",
                 },
			   MyDate1: {
                  required: "Please enter date of birth",
                 },
			  phone: {
					 required: "Please enter a phone number",
              		 minlength: "Enter 11 digit number ",
					 Numericonly: "enter only numeric"
                 },
				 address: {
                  required: "Please enter your address",
                  }
			  
            },
       
        /*submitHandler: function(form) {
            form.submit();
          }*/
        });
	  
	  
	jQuery("#updateBtn").click(function() {
      if (v.form()) {
		  jQuery( "#editstaffprofileForm" ).submit();
          return false;
       }
     });
	  
	  
	  jQuery("#branch").on('change', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/getclass',
            data:{ 'branch_id': branch_id },
            success :  function(resp) {
						jQuery('.profilecheckbox').empty();
						jQuery('#class_name').empty();
					    jQuery('#class_name').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
			 jQuery("#class_name").on('click', function(e){ 
   		  var class_id = jQuery(this).val();
		   var branch_id = jQuery('#branch').val();
		  if(branch_id!='' && class_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/get_subject',
            data:{ 'branch_id': branch_id, 'class_id':class_id},
            success :  function(resp) {
						jQuery('.profilecheckbox').empty();
						jQuery('#subject_name').empty();
					    jQuery('#subject_name').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
			   jQuery("#branch").on('change', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/get_teacher_role',
            data:{ 'branch_id': branch_id },
            success :  function(resp) {
						jQuery('#teachrole').empty();
					    jQuery('#teachrole').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
			 jQuery("#branch").on('change', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/get_teacher_level',
            data:{ 'branch_id': branch_id },
            success :  function(resp) {
						jQuery('#teachlevel').empty();
					    jQuery('#teachlevel').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
			 jQuery("#branch").on('change', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/get_teacher_type',
            data:{ 'branch_id': branch_id },
            success :  function(resp) {
						jQuery('#teachtype').empty();
					    jQuery('#teachtype').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });

	});
</script>
         <script>
	$(".fa-picture-o").click(function () {
  $("input[type='file']").trigger('click');
  $("#ppp").hide();
  $("#ppp1").show();
});

 $("#ppp1").hide();
 
$('input[type="file"]').on('change', function() {
  var val = $(this).val();
  $(this).siblings('span').text(val);
})

$('input[type="file"]').on('change', function() {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#ppp1").css("background-image", "url("+this.result+")");
            }
        }
        });
		</script> 
        <script type="text/javascript">
 
$(document).ready(function(){
 
    $('[tool-tip-toggle="tooltip-demo"]').tooltip({
 
        placement : 'top'
 
    });
 
});
 
</script> 
  <style>
#ppp,#ppp1 {
   
    background-position: center center;
    background-size: cover;
    -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
    display: inline-block;
}
#cancel{
	background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
	border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
    float: left;
    font-size: 14px;
    margin: 20px 0;
    padding: 6px 20px;

}
.editprofileform {
    border-radius: 4px;
    float: left;
    margin-right: 2%;
    width: 48%;
}
.fa-question-circle + .tooltip > .tooltip-inner {background-color:#090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.errspan {
    float: right;
    margin-right: -14px;
    margin-top: -90px;
    position: relative;
}


.padd {
    max-height: 722px;
    min-height: 722px;
}
.updatestaffbtn {
    width: 100%;
}
.updatestaffbtn .confirmlink{padding-right:10px;}
.editprofile_left{width:100%;}
.editprofile_right{width:100%;}
.formleft{padding-left:0px;}
.formright{padding-right:0px;}
.fa.fa-question-circle.errspan{color:#090;}

.profile_titlebg h2{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1); 
	padding: 10px 15px;
	margin:0px; }
.profiletitltbox{padding-top:0px;}
.profiletitltbox .profile_titlebg {
    padding: 0;
}
.contentdiv{
	padding-left:15px;
	padding-top:15px;

}
.profile-bg {
	padding: 0px;
}
.accsection h1 {
    padding: 0 0 10px 15px;
	color:#393C3E;
}
.link input{
    font-size: 19px;
    margin: 0;
	width: 100%;
}
</style>  
   
       
