<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller {

	 var $logmode;
	function __construct(){
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','notifications_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
      }
	
  public function index() {
	   
	     $data  				= array();
		 $user_id				= $this->session->userdata('user_id');
		 $school_id				= $this->session->userdata('user_school_id');
		 
		 $search_condition['school_id']	 = $school_id;
		 $search_condition['to']		 = $user_id;
		 $search_condition['is_active']	 = '1';
		 $search_condition['is_deleted'] = '0';
		 /*******************************************************/
		        $whereupnotification = array('to'=>$user_id,'school_id'=>$school_id,'is_active'=>'1','read_by'=>'');
			$dataupdatenotification = array("read_by" => $user_id);
			$this->notifications_model->updateNotificationReadby($dataupdatenotification,$whereupnotification);
		 /*******************************************************/
		$data['title']	      			= "Notifications";
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		$perpage						= 12;
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'notifications/index';
	    $config['total_rows'] 			= $this->notifications_model->getRows($search_condition);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = "/"; 
		
		$this->pagination->initialize($config);
		
		$odr =   "notification_id";
		$dirc	= "desc";
		
		$data_rows= $this->notifications_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc);
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		 
		 $this->load->view('header');
		 $this->load->view('notifications/notifications_list_view', $data);
		 $this->load->view('footer');
	 }
	
  public function count_notifications() {
			$user_id =  $_POST['userId'];
			$school_id =  $_POST['schoolId'];
			$userInfo = $this->notifications_model->getuserInfo($user_id,$school_id);
			$last_notification_checke_at = $userInfo->notification_checked_at;
			$notificationsCount = $this->notifications_model->count_notifications($school_id,$user_id,$last_notification_checke_at);
			echo $notificationsCount;
		   }
    public function update_notification_checking_status() {
			$user_id =  $_POST['userId'];
			$school_id =  $_POST['schoolId'];
			$userInfo = $this->notifications_model->getuserInfo($user_id,$school_id);
			$last_notification_checke_at = $userInfo->notification_checked_at;
			$notificationsCount = $this->notifications_model->count_notifications($school_id,$user_id,$last_notification_checke_at);
			if( $notificationsCount > 0 ) {
			$now = date('Y-m-d H:i:s');
			$where		= array('id'=>$user_id,'school_id'=>$school_id);
			$data = array("notification_checked_at" => $now);
			if($this->notifications_model->update($data,$where)) {
				    echo 'true';
			       } else {
					echo 'false';
				  }
			  }
		  }
		 
  public function get_notifications() {
			$user_id =  $_POST['userId'];
			$school_id =  $_POST['schoolId'];
			$notificationsInfo = $this->notifications_model->get_notifications($school_id,$user_id);
			    $notificationsData = '';
			  if(count(array_filter($notificationsInfo)) > 0) {
				foreach($notificationsInfo as $notification) {
					$userInfo = $this->notifications_model->getuserInfo($notification->from,$notification->school_id);
					
					     if($userInfo->user_type=='teacher') {
						       $path = 'staff';
							   $Info = $this->notifications_model->getStaffInfo($notification->from,$notification->school_id);
							   $Name = $Info->staff_fname.' '.$Info->staff_lname;
					         } elseif($userInfo->user_type=='student') {
								$path = 'student';
							   $Info = $this->notifications_model->getStudentInfo($notification->from,$notification->school_id);
							   $Name = $Info->student_fname.' '.$Info->student_lname;
							  } else {
								$path = 'admin';
							    $Info = $this->notifications_model->getAdminInfo($notification->from,$notification->school_id);
								$Name = $Info->school_contact_person;
							    }
					if($notification->read_by!='') {
						 $readStatus = 'readed_notification';
					   } else {
						 $readStatus = 'unreaded_notification';
					    }
						
           $notificationsData.= '<li class="'.$readStatus.'"> <a href="'.base_url().'notifications/index/">';
		        if($userInfo->profile_image!='') {
		   $notificationsData.=  '<img src="'.base_url().'uploads/'.$path.'/'.$userInfo->profile_image.'" width="30px" height="30px">';
		           } else {
		   $notificationsData.= '<img src="'.base_url().'assets/images/avtar.png" width="30px" height="30px">';
		              }
		   $notificationsData.=	$Name.'<br>';
$notificationsData.=	date('d M Y', strtotime($notification->created_date)).' at '.date('H:i A', strtotime($notification->created_date)).'<br>';
		     if($notification->subject!='') {
		         $notificationsData.= substr($notification->subject, 0,25);
				 if(strlen($notification->subject) > 25 ) { 
				 $notificationsData.= '...'; 
				   }
		        } else {
				 $notificationsData.= substr($notification->message, 0,25);
				 if(strlen($notification->message) > 25 ) {
				  $notificationsData.= '...'; 
					  }
		           }
		   $notificationsData.= ' </a> </li>';
		   
		   $notificationsData.= '<li role="separator" class="divider"></li>';
                      }
			      } else {
					$notificationsData.= '<li><a href="javascript:void(0)">No notifications</a></li>';
					$notificationsData.= '<li role="separator" class="divider"></li>';
			         }
			    $notificationsData.= '<li><a href="'.base_url().'notifications/index">See All</a></li>';
			    echo $notificationsData;
			  
		  }	
		  
	 public function download_attachments($fileName = NULL) { 
	
       if ($fileName) {
        $file		=	realpath ( FCPATH.'uploads/notification/'.$fileName );
        if (file_exists ( $file )) {
         $data		=	file_get_contents ( $file );
         $this->load->helper('download');
         force_download ( $fileName, $data );
		 echo"doen";
        } else {
		 $this->session->set_flashdata('error', 'Requested file does not exists.');
         redirect(base_url()."notifications/");exit;
          }
        }
      }

	
}
	