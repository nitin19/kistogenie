      <style>
.caret-up {
    width: 0; 
    height: 0; 
    border-left: 4px solid rgba(0, 0, 0, 0);
    border-right: 4px solid rgba(0, 0, 0, 0);
    border-bottom: 4px solid;
    
    display: inline-block;
    margin-left: 2px;
    vertical-align: middle;
}
</style>



        <div class="col-sm-2 nopadding menulist">
        <div class="documents-list">
        <h2>Menu</h2>
        <ul class="schooldoc">
        <h1> <a href="<?php echo base_url();?>teacherlearning"> School Documents </a></h1>

		 <?php 
		foreach($searchFolders as $folder) { 
		$parent_folder_id = $folder->teacher_learning_id;
		$school_id		= $this->session->userdata('user_school_id');
		$this->load->model(array('teacherlearning_model'));
		$sbfolder = $this->teacherlearning_model->getSubfolders($school_id,$parent_folder_id);
		?>
        <li class="dropdown docmenu">
          <a href="<?php echo base_url();?>teacherlearningfolder/index/<?php echo $folder->teacher_learning_id;?>"><b class="caret"></b><i class="fa fa-folder"></i><?php echo $folder->teacher_learning_name;?></a>
          <ul>
          
          <?php foreach($sbfolder as $Subfolders) { ?>
             <li class="submenu-dropdown">
 <a href="<?php echo base_url();?>teacherlearningfolder/subfolders/<?php echo $Subfolders->teacher_learning_id;?>"><b class="caret"></b><i class="fa fa-folder"></i><?php  echo $Subfolders->teacher_learning_name;?> </a>
           <?php } ?>
          </li>
          </ul>
        </li>
        
        <?php } ?>

        </ul>
        <div class="sidebar-search">
        <form action="<?php echo base_url();?>teacherlearningfolder/searchitem" class="navbar-form sidebarsearchbox searchbar"  name="search" id="search">
     <div class="form-group">
     <input type="text" class="form-control srch" placeholder="Search" name="filesearch">
     </div>
     <input type="submit" value="" class="submiticon submitbtn">
     </form>
        </div>
        </div>
        </div>
        
        <div class="col-sm-10 documentsec nopadding">
    <div class="editprofile-content">
    <div class="profilemenus col-sm-12">
        <div class="col-sm-6  menubaritems nopadding">
        <ul>
    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li class="edit">Teacher Learning</li>        
        </ul>
        </div>
 <!--       
        <div class="col-sm-3"> <input class="btn btn-success" data-toggle="modal" data-target="#myModal1" value="Add Folder" type="button"></div>
        
     <div class="col-sm-3"> <input type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" value="Add Files"></div>-->
        
        </div>
        
<div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>
 
        
        <div class="col-sm-12 nopadding accdetailheading">
        <div class="col-sm-12 col-xs-12 nopadding">
        <h1>Documents</h1>
        </div>
        </div>
		<div class="col-sm-12 nopadding">
        <div class="folderssec">
        
        
        <?php 
		if( count($Foldersdata) > 0 ) {
		$school_id		= $this->session->userdata('user_school_id');
		foreach($Foldersdata as $folder) { 
		
		$Parentfolderid = $folder->parent_teacher_learning;
        if($Parentfolderid!=0){
		$this->load->model(array('teacherlearningfolder_model'));
		$ParentFoldername = $this->teacherlearningfolder_model->getParentFolderName($school_id, $Parentfolderid);	
		
		$mainParentfolderId = $ParentFoldername->parent_teacher_learning;
		$MainParentFoldername = $this->teacherlearningfolder_model->getMainParentFolderName($school_id, $mainParentfolderId);
		
		?>
        <div class="col-sm-3">
   <a href="<?php echo base_url();?>teacherlearningfolder/index/<?php echo $folder->teacher_learning_id;?>" class="mycustomtoltp" data-toggle="tooltip" data-placement="top" title="<?php echo $ParentFoldername->teacher_learning_name.'->'.$folder->teacher_learning_name;?>"><div class="ancr-content"> <img src="<?php echo base_url();?>assets/images/folder_icon.png" class="img-responsive" ><p><?php echo $folder->teacher_learning_name;?></p></div></a>
        <a class="delbtna" href="#" onclick="delConfirm2('<?php echo $folder->teacher_learning_id;?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle fa-lg"></i>
</a>
</div>
          <?php }  } 
		} 
		  
		   ?>
           
         <?php 
		 if(count($Folderfiles) > 0){
		$school_id		= $this->session->userdata('user_school_id');
		foreach($Folderfiles as $folderfile) { 
		
		$Parentfolderid = $folderfile->teacher_learning_id;
		if($Parentfolderid!=0){
		$this->load->model(array('teacherlearningfolder_model'));
		$ParentFoldername = $this->teacherlearningfolder_model->getParentFolderName($school_id, $Parentfolderid);
		$mainParentfolderId = $ParentFoldername->parent_teacher_learning;
		if($mainParentfolderId!=0) {
		 $MainParentFoldername = $this->teacherlearningfolder_model->getMainParentFolderName($school_id, $mainParentfolderId);
		}
		
		$imagetype =  array('gif','png','jpg','jpeg');
		$videotype =  array('mp4','3gp','avi','flv','wmv');
		$audiotype =  array('mp3','wav','wma','au');
		$filetype =  array('txt','docx','docs','xml','sql');
		$exceltype =  array('xls','xlsx');
		$ext = pathinfo($folderfile->teacher_learning_attachment_name, PATHINFO_EXTENSION);
		if(in_array($ext,$imagetype) ) {
			 ?>
         <div class="col-sm-3">
        <a class="delbtna" href="#" onclick="delConfirm('<?php echo $folderfile->teacher_learning_attachment_id; ?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle fa-lg"></i></a>
       <a href="" class="mycustomtoltp" data-toggle="tooltip" data-placement="top" title="<?php if($mainParentfolderId!=0) { echo $MainParentFoldername->teacher_learning_name.'->'; } echo $ParentFoldername->teacher_learning_name.'->'.substr($folderfile->teacher_learning_attachment_name,10);?>"> <img src="<?php echo base_url();?>uploads/teacher_learning_certificates/<?php echo $folderfile->teacher_learning_attachment_name; ?>" class="img-responsive" ><span><?php echo  substr($folderfile->teacher_learning_attachment_name,10); ?></span></a></div>
			<?php
		}
		elseif(in_array($ext,$videotype) ) {
			 ?>
         <div class="col-sm-3">
          <a class="delbtna" href="#" onclick="delConfirm('<?php echo $folderfile->teacher_learning_attachment_id; ?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle fa-lg"></i></a>
         <a href="" class="mycustomtoltp" data-toggle="tooltip" data-placement="top" title="<?php if($mainParentfolderId!=0) { echo $MainParentFoldername->teacher_learning_name.'->'; } echo $ParentFoldername->teacher_learning_name.'->'.substr($folderfile->teacher_learning_attachment_name,10);?>"><img src="<?php echo base_url();?>assets/images/videofile.png" class="img-responsive" ><span><?php echo substr($folderfile->teacher_learning_attachment_name,10); ?></span></a></div>
			<?php
		}
		elseif(in_array($ext,$audiotype) ) {
			 ?>
         <div class="col-sm-3">
          <a class="delbtna" href="#" onclick="delConfirm('<?php echo $folderfile->teacher_learning_attachment_id; ?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle fa-lg"></i></a>
         <a href="" class="mycustomtoltp" data-toggle="tooltip" data-placement="top" title="<?php if($mainParentfolderId!=0) { echo $MainParentFoldername->teacher_learning_name.'->'; } echo $ParentFoldername->teacher_learning_name.'->'.substr($folderfile->teacher_learning_attachment_name,10);?>"><img src="<?php echo base_url();?>assets/images/audiofile.png" class="img-responsive" ><span><?php echo substr($folderfile->teacher_learning_attachment_name,10); ?></span></a></div>
			<?php
		}
		elseif(in_array($ext,$filetype) ) {
			 ?>
         <div class="col-sm-3">
          <a class="delbtna" href="#" onclick="delConfirm('<?php echo $folderfile->teacher_learning_attachment_id; ?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle fa-lg"></i></a>
         <a href="" class="mycustomtoltp" data-toggle="tooltip" data-placement="top" title="<?php if($mainParentfolderId!=0) { echo $MainParentFoldername->teacher_learning_name.'->'; } echo $ParentFoldername->teacher_learning_name.'->'.substr($folderfile->teacher_learning_attachment_name,10);?>"><img src="<?php echo base_url();?>assets/images/document.png" class="img-responsive" ><span><?php echo substr($folderfile->teacher_learning_attachment_name,10); ?></span></a></div>
			<?php
		}
		elseif(in_array($ext,$exceltype) )  {
			 ?>
        <div class="col-sm-3">
         <a class="delbtna" href="#" onclick="delConfirm('<?php echo $folderfile->teacher_learning_attachment_id; ?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle fa-lg"></i></a>
        <a href="" class="mycustomtoltp" data-toggle="tooltip" data-placement="top" title="<?php if($mainParentfolderId!=0) { echo $MainParentFoldername->teacher_learning_name.'->'; } echo $ParentFoldername->teacher_learning_name.'->'.substr($folderfile->teacher_learning_attachment_name,10);?>"><img src="<?php echo base_url();?>assets/images/excel.png" class="img-responsive" ><span><?php echo substr($folderfile->teacher_learning_attachment_name,10); ?></span></a></div>
			<?php
		}
		elseif($ext == 'pdf') {
			 ?>
         <div class="col-sm-3">
          <a class="delbtna" href="#" onclick="delConfirm('<?php echo $folderfile->teacher_learning_attachment_id; ?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle fa-lg"></i></a>
         <a href="" class="mycustomtoltp" data-toggle="tooltip" data-placement="top" title="<?php if($mainParentfolderId!=0) { echo $MainParentFoldername->teacher_learning_name.'->'; } echo $ParentFoldername->teacher_learning_name.'->'.substr($folderfile->teacher_learning_attachment_name,10);?>"><img src="<?php echo base_url();?>assets/images/pdffile.png" class="img-responsive" ><span><?php echo substr($folderfile->teacher_learning_attachment_name,10); ?></span></a></div>
			<?php
		}
		elseif($ext == 'zip') {
			 ?>
        <div class="col-sm-3">
         <a class="delbtna" href="#" onclick="delConfirm('<?php echo $folderfile->teacher_learning_attachment_id; ?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle fa-lg"></i></a>
        <a href="" class="mycustomtoltp" data-toggle="tooltip" data-placement="top" title="<?php if($mainParentfolderId!=0) { echo $MainParentFoldername->teacher_learning_name.'->'; } echo $ParentFoldername->teacher_learning_name.'->'.substr($folderfile->teacher_learning_attachment_name,10);?>"><img src="<?php echo base_url();?>assets/images/zipfile.png" class="img-responsive" ><span><?php echo substr($folderfile->teacher_learning_attachment_name,10); ?></span></a></div>
			<?php
		}
		else {
			 ?>
         <div class="col-sm-3">
          <a class="delbtna" href="#" onclick="delConfirm('<?php echo $folderfile->teacher_learning_attachment_id; ?>')" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle fa-lg"></i></a>
         <a href="" class="mycustomtoltp" data-toggle="tooltip" data-placement="top" title="<?php if($mainParentfolderId!=0) { echo $MainParentFoldername->teacher_learning_name.'->'; } echo $ParentFoldername->folder_name.'->'.substr($folderfile->teacher_learning_attachment_name,10);?>"><img src="<?php echo base_url();?>assets/images/attachment.png" class="img-responsive" ><span><?php echo substr($folderfile->teacher_learning_attachment_name,10); ?></span></a></div>
			<?php
		}
		}
		 }}
		?>
        
    <!-- <div class="col-sm-3"><img src="<?php echo base_url();?>uploads/school_certificates/" class="img-responsive"><?php echo $folderfile->teacher_learning_attachment_name;?></div>-->
        
      <!--  <img src="<?php //echo base_url();?>assets/images/foldersimg.jpg" class="img-responsive">-->
      
      
      
        </div>
        </div>
       <div class="col-sm-12 nopadding items" style="margin-top: 5%;">
        <div class="col-sm-12 col-xs-12 nopadding">
        <span><?php 
		$items = 0;
		if(count($Foldersdata)>0){ $items = $items + count($Foldersdata); }
         $items = $items + count($Folderfiles);
		echo $items.' Items';
		 
		?>
        </span>
        </div>
        </div>
        </div>
        </div>
        
        
        
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Document Files</h4>
        </div>
        <!---------error message ------------------>
        <div style="clear:both"></div>
       <div class="alert alert-danger alert-dismissable" id="attachmentError" style="display:none" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			Some problem exists. Files has not been saved. 
		</div>
        
         <div class="alert alert-danger alert-dismissable" id="attachmentAlreadyError" style="display:none" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			Some Problem exists. Files has not been saved.
		</div>
    
<!---------error message end------------------>
<!---------success message ------------------>
         
      <div class="alert alert-success alert-dismissable" id="attachmentSuccess" style="display:none">
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		    Files been saved successfuly.
	   </div>

 <div style="clear:both"></div>         
<!---------success message end------------------>
        <div class="modal-body">
        <div id="loadingimage" style="padding-bottom: 20px; text-align: center;"><img src="<?php echo base_url();?>assets/images/image.gif" /></div>
<form action="" name="uploaddocumentForm" id="uploaddocumentForm" enctype="multipart/form-data">

 <input type="hidden" name="folder_id" id="folder_id" value="<?php echo $Folder_id;?>" />
 
          <div class="col-sm-12 popup_section_form">
          
              <div class="col-sm-10 nopadding">
            <input type="file" name="school_certificates[]" id="file" multiple style="padding: 0 12px;" />
             </div>
             
             <div class="col-sm-2 nopadding text_box_btn">
                <input value="Save" class="popup_save_btn" id="save" name="save" type="submit">
             </div>
          </div>
          </form>
        </div>
        <div class="modal-footer">
        
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>
      
    </div>
  </div>
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Document Folders</h4>
        </div>
        <!---------error message ------------------>
        <div style="clear:both"></div>
       <div class="alert alert-danger alert-dismissable" id="folderError" style="display:none" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			Some problem exists. Folder has not been saved. 
		</div>
        
         <div class="alert alert-danger alert-dismissable" id="folderAlreadyError" style="display:none" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			Folder name already exists. Folder has not been saved.
		</div>
    
<!---------error message end------------------>
<!---------success message ------------------>
         
      <div class="alert alert-success alert-dismissable" id="folderSuccess" style="display:none">
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		    Folder been saved successfuly.
	   </div>

 <div style="clear:both"></div>         
<!---------success message end------------------>
        <div class="modal-body">
        <form action="" name="createfolderForm" id="createfolderForm" >
            <input type="hidden" name="pra_branch" id="pra_branch" value="<?php echo $Folder_id;?>" >
          <div class="col-sm-12 popup_section_form">
          
          <?php /*?><div class="col-sm-6 col-xs-3 inputbox termselect">
      <select class="form-control" name="parent" id="parent">
  <option value="">Select Folder</option>    
  <?php foreach($Folders as $folder) { ?>
     <option value="<?php echo $folder->folder_id; ?>"><?php echo $folder->folder_name;?></option>
    <?php  } ?>
</select>
    </div><?php */?>
          
              <div class="col-sm-10 nopadding text_box_sec">
                 <input type="text" name="foldername" id="foldername" placeholder="Folder Name" maxlength="41"  value=""/>
             </div>
             
             <div class="col-sm-2 nopadding text_box_btn">
                <input value="Save" class="popup_save_btn" id="save" name="save" type="submit">
             </div>
          </div>
          </form>
        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>
      
    </div>
  </div>  
  
  
 <script>
  jQuery(document).ready(function(){  
  
  jQuery('#loadingimage').hide();		
	
	jQuery.validator.addMethod("alphaLnumber", function(value, element) {
      return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);
    });		
		
   jQuery("#uploaddocumentForm").validate({
	   ignore: [],
        rules: {
			  'school_certificates[]': {
				required: true
                 }
            },
        messages: {
			  'school_certificates[]': {
				  required: "Required field"
                 }
             },
		 submitHandler: submitForm
        });
		
	  	function submitForm() {
			 jQuery('#loadingimage').show();
	  	   var fdata = new FormData();
		   fdata.append("folder_id",jQuery("#folder_id").val());
         if(jQuery("#file")[0].files.length>0) {
          // fdata.append("school_certificates",jQuery("#file")[0].files[0])
		  jQuery.each(jQuery('#file')[0].files, function(i, file) {
           fdata.append('school_certificates[]', file);
           });
		  }
		
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>teacherlearningfolder/uploaddocuments/',
           // data : jQuery("#uploaddocumentForm").serialize(),
		    data:fdata, 
			//dataType : "html",
			contentType: false,
            processData: false, 
            beforeSend: function()
            {
            },
                 success :  function(data) {
					// alert(data);
				    data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
					  jQuery('#attachmentError').hide();
					  jQuery('#attachmentAlreadyError').hide();
					  jQuery("#uploaddocumentForm")[0].reset();
					   jQuery('#attachmentSuccess').show();
					   jQuery('#loadingimage').hide();
					   
			             setTimeout(profilesteps, 3000);
    			         function profilesteps() {
							jQuery('#myModal').modal('hide');
							window.location.href="<?php echo base_url(); ?>teacherlearningfolder/index/<?php echo $Folder_id;?>";
					       }
					   
					} else if(data.Status == 'false') {
						jQuery('#attachmentSuccess').hide();
						jQuery('#attachmentAlreadyError').hide();
						jQuery('#attachmentError').show();
						jQuery('#loadingimage').hide();
					} else if(data.Status == 'alreadyexist') {
						jQuery('#attachmentSuccess').hide();
						jQuery('#attachmentError').hide();
						jQuery('#attachmentAlreadyError').show();
						jQuery('#loadingimage').hide();
					} 
             }
        });
        return false;
      }
		
			 
	 });
		 
</script>

 <script>
  jQuery(document).ready(function(){  		
	
	jQuery.validator.addMethod("alphaLnumber", function(value, element) {
      return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);
    });		
		
   jQuery("#createfolderForm").validate({
        rules: {
			  foldername: {
                required: true,
				alphaLnumber: true,
				maxlength: 40,
		       /* remote: {
                   url: "<?php echo base_url(); ?>schooltermreport/check_progressreport_archive",
                  type: "POST"
                   }*/
                 }
            },
        messages: {
			  foldername: {
                   required: "Please enter progress folder name.",
				   alphaLnumber: "Letter and number are allowed.",
				   maxlength: "Maximum 40 character are allowed.",
				  // remote: "Progress report archive name already in use"
                 }
             },
       
       /* submitHandler: function(form) {
            form.submit();
          }*/
		 submitHandler: submitForm
        });
		
	  	function submitForm() {
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>teacherlearningfolder/addFolder',
            data : jQuery("#createfolderForm").serialize(),
			dataType : "html",
           beforeSend: function()
            {
            },
                 success :  function(data) {
					// alert(data);
				    data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
					  jQuery('#folderError').hide();
					  jQuery('#folderAlreadyError').hide();
					  jQuery("#createfolderForm")[0].reset();
					   jQuery('#folderSuccess').show();
					   
			             setTimeout(profilesteps, 3000);
    			         function profilesteps() {
							 jQuery('#myModal1').modal('hide');
							window.location.href="<?php echo base_url(); ?>teacherlearningfolder/index/<?php echo $Folder_id;?>";
					       }
					   
					} else if(data.Status == 'false') {
						 jQuery("#createfolderForm")[0].reset();
						jQuery('#folderSuccess').hide();
						jQuery('#folderAlreadyError').hide();
						jQuery('#folderError').show();
					} else if(data.Status == 'alreadyexist') {
						 jQuery("#createfolderForm")[0].reset();
						jQuery('#folderSuccess').hide();
						jQuery('#folderError').hide();
						jQuery('#folderAlreadyError').show();
					} 
             }
        });
        return false;
      }
		
			 
	 });
		 
</script>
  
    
    <script>
          $(function(){
    $(".dropdown.docmenu").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
	</script>
          <script>
		  
	  $(function() {
		  
    function unifyHeights() {
        var maxHeight = 0;
        $('.editdetailpage').children('.detailleft, .menulist').each(function() {
            var height = $(this).outerHeight();
            // alert(height);
            if ( height > maxHeight ) {
                maxHeight = height;
            }
        });
        $('.detailleft, .menulist').css('height', maxHeight);
    }
    unifyHeights();
});
	  </script>
      
      <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>teacherlearningfolder/deletefile/"+id;
		}else{
			return false;
		}
	}
</script>

   <script type="text/javascript">
	function delConfirm2(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>teacherlearningfolder/deletefolder/"+id ;
		}else{
			return false;
		}
	}
</script> 

<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('[tool-tip-toggle="tooltip-demo"]').tooltip({
        placement : 'top'
    });
});
</script>
      
<style>

.tooltip > .tooltip-inner {background-color:#090 !important; color:#fff;}
.tooltip > .tooltip-arrow {border-top-color:#090 !important;}

.img-responsive {
    max-height: 64px;
    min-height: 64px;
}
.save_btn > input {
    background: #37b248 none repeat scroll 0 0;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #fff;
    font-size: 14px;
    padding: 7px 20px;
}
.save_btn {
    padding-top: 20px;
}
.popup_save_btn {
    background: #37b248 none repeat scroll 0 0;
    border: medium none;
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    color: #fff;
    padding: 12px 25px;
}
.modal-body {
    padding: 15px 0 70px;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-radius: 3px;
    min-height: 45px;
    width: 100%;
	padding: 10px;
}
.btn.btn-default {
    background: #37b248 none repeat scroll 0 0;
    color: #fff;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
    min-height: 44px;
    padding: 10px;
    width: 100%;
}
.save_btn {
    text-align: right;
}
.text_box_btn {
    text-align: left;
}
.modal-header {
    text-align: center;
}
.delbtna {
    left: 100px;
    position: absolute;

}
.folderssec .col-sm-3 {
    margin-bottom: 25px;
}
.form-control.srch {
    color: #fff;
}
 .ancr-content {
    display: inline-block;
    text-align: center;
}

.fa.fa-times-circle {
    color: #ff0000;
}
</style>