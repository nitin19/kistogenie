<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>

<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>

		<li><a href="#">Home</a></li>

        <li><a href="#">Enrollment</a>

        <li class="edit"><a href="#"> Students</a></li>        

        </ul>

        </div>

        

        

                 <div style="clear:both"></div>

 <?php if($this->session->flashdata('error')): ?>

   <div class="alert alert-danger alert-dismissable" >

			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

			<b>Alert!</b> 

			  <?php echo $this->session->flashdata('error'); ?>

		</div>

<?php endif; ?>



<?php if($this->session->flashdata('success')): ?>

     <div class="alert alert-success alert-dismissable" >

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		   <?php echo $this->session->flashdata('success'); ?>

	   </div>

<?php endif; ?>

 <div style="clear:both"></div>

        

        

        <div class="attendancesec">	

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <div class="tabletitlediv">

    <h1>Students Registration Detail</h1>

    <div class="col-sm-12 tablenavdiv">

    <div class="col-sm-7 tablenevleft">

    <ul>

    <?php if($branch_data!=''){?>
     
     <li><a href="<?php echo base_url();?>studentenrolement/index/<?php echo $last_page;?>/<?php echo $branch_data;?>">Applied Students</a></li>
  
  <?php }else{?>
  
  <li><a href="<?php echo base_url();?>studentenrolement/index">Applied Students</a></li>
  
  <?php } ?>
     
     
 <?php if($branch_data!=''){?>
 
   <li><a href="<?php echo base_url();?>studentenrolement/selectedstudents/<?php echo $last_page;?>/<?php echo $branch_data;?>">Selected Students</a></li>

<?php }else{?>

	<li><a href="<?php echo base_url();?>studentenrolement/selectedstudents">Selected Students</a></li>
  
<?php } ?>

 <?php if($branch_data!=''){?>
      
   <li><a href="<?php echo base_url();?>studentenrolement/rejectedstudents/<?php echo $last_page;?>/<?php echo $branch_data;?>">Rejected Students</a></li>

<?php }else{?>

    <li><a href="<?php echo base_url();?>studentenrolement/rejectedstudents">Rejected Students</a></li>
 
<?php } ?>

 <?php if($branch_data!=''){?>
     
   <li class="active"><a href="<?php echo base_url();?>studentenrolement/waitingstudents/<?php echo $last_page;?>/<?php echo $branch_data;?>">Waiting Students</a></li>
 
<?php }else{?>

    <li class="active"><a href="<?php echo base_url();?>studentenrolement/waitingstudents">Waiting Students</a></li>

<?php } ?>


    </ul>

    </div>

    

    

    <div class="col-sm-5 tablenevright">

<form action="<?php echo base_url();?>studentenrolement/waitingstudents/0/" name="studentSearchForm" id="studentSearchForm">

             <div class="col-sm-12 col-xs-12 applycodediv nopadding datepickerspacing">

               

                <div class="col-sm-3 col-xs-3 nopadding dateblk">

        		<div class="form-group">

    <div class="inputbox datepickerdiv nopadding">

      <!--<input type="text" name="MyDate1" class="datepicker1">-->

       <input type="text" name="sdate" id="sdate" value="<?php echo @$sdate_search;?>" class="datepickerattendancesummary1">

    </div>

  </div> 

  				</div>

                

                <div class="col-sm-1 datepickercenter">

                <span>-</span>

                </div>

                

                <div class="col-sm-3 col-xs-3 nopadding dateblk">

        		 <div class="form-group">

    <div class="inputbox datepickerdiv nopadding">
    
<?php if($branch_data !='') {
?>
<input type="hidden" name="branchidsrch" value="<?php echo $branch_data; ?>" />

<?php
} ?>

       <input type="text" name="edate" id="edate" value="<?php echo @$edate_search;?>" class="datepickerattendancesummary2">

    </div>

  </div> 

  				</div>

                

                 <div class="col-sm-3 col-xs-3">

        		  <div class="form-group generatereport">

   					 <input type="submit" class="btn btn-danger confirm" value="View Student">

 					 </div> 

  				  </div>

                

                

               </div>

         </form>

        </div>



    </div>

    </div>

    </div>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings enrolment-headings">

					  <th class="column1">No.</th>

					  <th class="column3">Name</th>

					  <th class="column3">Applied for Branch</th>

                      <th class="column3">Applied Date</th>

                      <th class="column4">Email Address</th>

                      <th class="column3">Phone Number</th>

                      <th class="column3">Action</th>

				  </tr>

			  </thead>

				<tbody>

                

                <?php 

					  if(count($data_rows) > 0){

					      $sr=$last_page;

					  foreach($data_rows as $student) { 

					     $sr++;

						  $student_school_branch = $student->student_school_branch;

						  $this->load->model(array('studentenrolement_model'));

						  $branchName = $this->studentenrolement_model->getbranchName($student_school_branch);

					?>

                

                

					<tr class="familydata student-registration enrolment-table">

						<td class="column1"><?php echo $sr; ?></td>

						<td class="column3"><?php echo $student->student_fname.' '. $student->student_lname;?></td>

						<td class="column3"><?php echo @$branchName->branch_name;?></td>

                        <td class="column3"><?php echo date('M d, Y', strtotime($student->student_application_date)); ?> </td>

                        <td class="column4"><?php

						 if($student->email!='') {

						   echo $student->email; } else {

						   echo '<span style="color:transparent;">-</span>';

								    }				

						 ?></td>

                        <td class="column3"><?php 

						 if($student->student_telephone!='') {

						   echo $student->student_telephone; } else {

						   echo '<span style="color:transparent;">-</span>';

								    }

						?></td>

                        <td class="column3 action-btn"><!--<a href="#">Action <i class="fa fa-angle-down"></i></a>-->

                        

                        

                        <div class="btn-group">

        <button type="button" class="btn btn-info btn-flat">Action</button>

        <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">

            <span class="caret"></span>

            <span class="sr-only">Toggle Dropdown</span>

        </button>

        <ul class="dropdown-menu" role="menu">

                                                
        <li> <a href="<?php echo base_url();?>studentenrolement/view_studentaccount_detail/<?php echo $student->id;?>"> View </a> </li> 
      <!--  <li> <a href="#"> Edit </a> </li> -->                                     
		
        <li class="divider"></li>
        
        <li> <a href="#" data-toggle="modal" data-target="#myModal_applied_<?php echo $student->id;?>"> Applied </a> </li>

        <li><a href="#" data-toggle="modal" data-target="#myModal_enrole_<?php echo $student->id;?>"> Enroled </a>  </li>

    	<li><a href="#" data-toggle="modal" data-target="#myModal_reject_<?php echo $student->id;?>"> Rejected </a>  </li>

		<li class="divider"></li>

  		<li> <a href="#" data-toggle="modal" data-target="#myModal_request_detail<?php echo $student->id;?>"> Request details </a> </li>

        <li><a href="#" data-toggle="modal" data-target="#myModal_schedule<?php echo $student->id;?>"> Schedule Email </a>  </li>

        <li><a href="#" data-toggle="modal" data-target="#myModal_reschedule<?php echo $student->id;?>"> Reschedule Email</a>  </li>

		<li class="divider"></li>

         <li> <a href="#" data-toggle="modal" data-target="#myModal_notes_<?php echo $student->id;?>"> Notes  </a> </li>

        <li class="divider"></li>

        <li><a onclick="delConfirm(<?php echo $student->id;?>)" style="cursor: pointer;"> Delete </a> </li>

									

                    </ul>

                </div>

<!-- ******************************** Change to APPLIED ***************************************** -->
                        

   <div class="modal fade" id="myModal_applied_<?php echo $student->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Applied Email </h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

        

         <div class="alert alert-danger alert-dismissable" id="sendappliedMailError-<?php echo $student->id;?>" style="display:none" >

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		  <b>Alert!</b> 

			Some problem exists. Message has not been sent. 

		</div>

        

         <div class="alert alert-success alert-dismissable" id="sendappliedMailSuccess-<?php echo $student->id;?>" style="display:none">

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		    Message has been sent successfully. 

             </div>

        

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>studentenrolement/applied_user_enrolement" name="appliedemailForm" id="appliedemailForm_<?php echo $student->id;?>" method="post">

     
		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $student->id; ?>" />        
		
         <input type="hidden" name="school_id" id="school_id" value="<?php echo $student->school_id;?>" />
        

         <input type="hidden" name="student_email" id="student_appliedemail_<?php echo $student->id;?>" value="<?php echo $student->email;?>" class="form-control"  placeholder="" maxlength="121">

         

         <input type="hidden" name="student_email_subject" id="student_appliedemail_subject_<?php echo $student->id;?>" value="<?php echo $appliedInfo->student_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="student_email_message" id="student_appliedemail_message_<?php echo $student->id;?>"> 

		<?php

		  if($student->student_enrolment_date=='0000-00-00') {

			 $student_enrolment_date = date('d-m-Y');

		   } else {

			 $student_enrolment_date = date('d-m-Y', strtotime($student->student_enrolment_date));

		    }

 $tex1 = preg_replace('/\b(name of parent)\b/u', $student->student_father_name, $appliedInfo->student_email_message);

 $text2 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $tex1);

 $text3 = preg_replace('/\b(name of student)\b/u', $student->student_fname.' '. $student->student_lname, $text2);

 $text4 = preg_replace('/\b(date of enrolment)\b/u', $student_enrolment_date, $text3);

 $text5 = preg_replace('/\b(branch contact number)\b/u', $schoolContactNo->school_phone, $text4);

 $text6 = str_replace('(', '', $text5);

 $Finaltext = str_replace(')', '', $text6);

		echo   $Finaltext; 

        ?>

      </textarea>

      <script>

       CKEDITOR.replace( 'student_appliedemail_message_<?php echo $student->id;?>' );

     </script>

    </div>

  </div>

  

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingappliedmsg-<?php echo $student->id;?>"></div>

        <input class="sendappliedemail" id="save_applied-<?php echo $student->id;?>" value="Send" type="submit">

        </div>

        </div>

          

             </div>  

          </form>

        </div>

        <div class="modal-footer">

        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->

        </div>

      </div>

      

    </div>

  </div>                     
  
<!-- ******************************** ENROLEMENT OF STUDENT ***************************************** -->
                        

   <div class="modal fade" id="myModal_enrole_<?php echo $student->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Enrollement Email</h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

        

         <div class="alert alert-danger alert-dismissable" id="sendenroleMailError-<?php echo $student->id;?>" style="display:none" >

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		  <b>Alert!</b> 

			Some problem exists. Message has not been sent. 

		</div>

        

         <div class="alert alert-success alert-dismissable" id="sendenroleMailSuccess-<?php echo $student->id;?>" style="display:none">

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		    Message has been sent successfully. 

             </div>

        

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>studentenrolement/add_user_enrolement" name="enrolementemailForm" id="enrolementemailForm_<?php echo $student->id;?>" method="post">

     
		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $student->id; ?>" />        
		
         <input type="hidden" name="school_id" id="school_id" value="<?php echo $student->school_id;?>" />

        

         <input type="hidden" name="student_email" id="student_email_<?php echo $student->id;?>" value="<?php echo $student->email;?>" class="form-control"  placeholder="" maxlength="121">

         

         <input type="hidden" name="student_email_subject" id="student_email_subject_<?php echo $student->id;?>" value="<?php echo $selectedInfo->student_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="student_email_message" id="student_email_message_<?php echo $student->id;?>"> 

		<?php

		  if($student->student_enrolment_date=='0000-00-00') {

			 $student_enrolment_date = date('d-m-Y');

		   } else {

			 $student_enrolment_date = date('d-m-Y', strtotime($student->student_enrolment_date));

		    }

 $tex1 = preg_replace('/\b(name of parent)\b/u', $student->student_father_name, $selectedInfo->student_email_message);

 $text2 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $tex1);

 $text3 = preg_replace('/\b(name of Student)\b/u', $student->student_fname.' '. $student->student_lname, $text2);

 $text4 = preg_replace('/\b(date of enrolment)\b/u', $student_enrolment_date, $text3);

 $text5 = preg_replace('/\b(branch contact number)\b/u', $schoolContactNo->school_phone, $text4);

 $text6 = str_replace('(', '', $text5);

 $Finaltext = str_replace(')', '', $text6);

		echo   $Finaltext; 

        ?>

      </textarea>

      <script>

       CKEDITOR.replace( 'student_email_message_<?php echo $student->id;?>' );

     </script>

    </div>

  </div>

  

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingenrolemsg-<?php echo $student->id;?>"></div>

        <input class="sendenrolementemail" id="save_enrole-<?php echo $student->id;?>" value="Send" type="submit">

        </div>

        </div>

          

             </div>  

          </form>

        </div>

        <div class="modal-footer">

        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->

        </div>

      </div>

      

    </div>

  </div>
  
<!-- ******************************** REJECTION OF STUDENT ***************************************** -->
                        

   <div class="modal fade" id="myModal_reject_<?php echo $student->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Rejection Email </h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

        

         <div class="alert alert-danger alert-dismissable" id="sendrejectMailError-<?php echo $student->id;?>" style="display:none" >

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		  <b>Alert!</b> 

			Some problem exists. Message has not been sent. 

		</div>

        

         <div class="alert alert-success alert-dismissable" id="sendrejectMailSuccess-<?php echo $student->id;?>" style="display:none">

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		    Message has been sent successfully. 

             </div>

        

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>studentenrolement/reject_user_enrolement" name="rejectemailForm" id="rejectemailForm_<?php echo $student->id;?>" method="post">

    
		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $student->id; ?>" />        
		
         <input type="hidden" name="school_id" id="school_id" value="<?php echo $student->school_id;?>" />

         <input type="hidden" name="student_email" id="student_rejectemail_<?php echo $student->id;?>" value="<?php echo $student->email;?>" class="form-control"  placeholder="" maxlength="121">

         <input type="hidden" name="student_email_subject" id="student_rejectemail_subject_<?php echo $student->id;?>" value="<?php echo $rejectedInfo->student_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="student_email_message" id="student_rejectemail_message_<?php echo $student->id;?>"> 

		<?php

		  if($student->student_enrolment_date=='0000-00-00') {

			 $student_enrolment_date = date('d-m-Y');

		   } else {

			 $student_enrolment_date = date('d-m-Y', strtotime($student->student_enrolment_date));

		    }

 $tex1 = preg_replace('/\b(name of parent)\b/u', $student->student_father_name, $rejectedInfo->student_email_message);

 $text2 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $tex1);

 $text3 = preg_replace('/\b(name of Student)\b/u', $student->student_fname.' '. $student->student_lname, $text2);

 $text4 = preg_replace('/\b(date of enrolment)\b/u', $student_enrolment_date, $text3);

 $text5 = preg_replace('/\b(branch contact number)\b/u', $schoolContactNo->school_phone, $text4);

 $text6 = str_replace('(', '', $text5);

 $Finaltext = str_replace(')', '', $text6);

		echo   $Finaltext; 

        ?>

      </textarea>

      <script>

       CKEDITOR.replace( 'student_rejectemail_message_<?php echo $student->id;?>' );

     </script>

    </div>

  </div>

  

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingrejectmsg-<?php echo $student->id;?>"></div>

        <input class="sendrejectemail" id="save_reject-<?php echo $student->id;?>" value="Send" type="submit">

        </div>

        </div>

          

             </div>  

          </form>

        </div>

        <div class="modal-footer">

        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->

        </div>

      </div>

      

    </div>

  </div>                  

     <!----- ******************************Generate Pop-up  for sending Email for request details of student ******************* -----> 
  	
    <div class="modal fade" id="myModal_request_detail<?php echo $student->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Email for Request Detail </h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

        

         <div class="alert alert-danger alert-dismissable" id="requestdetailError-<?php echo $student->id;?>" style="display:none" >

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		  <b>Alert!</b> 

			Some problem exists. Message has not been sent. 

		</div>

         <div class="alert alert-success alert-dismissable" id="requestdetailSuccess-<?php echo $student->id;?>" style="display:none">

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		    Message has been sent successfully. 

             </div>

        

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>studentenrolement/request_user_detail" name="requestdetailForm" id="requestdetailForm_<?php echo $student->id;?>" method="post">

     
   
		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $student->id; ?>" />        
		
         <input type="hidden" name="school_id" id="school_id" value="<?php echo $student->school_id;?>" />

        

         <input type="hidden" name="student_email" id="student_request_<?php echo $student->id;?>" value="<?php echo $student->email;?>" class="form-control"  placeholder="" maxlength="121">

         

         <input type="hidden" name="student_email_subject" id="student_request_subject_<?php echo $student->id;?>" value="<?php echo $requestdetailsInfo->student_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="student_email_message" id="student_request_message_<?php echo $student->id;?>"> 

        <?php //echo $staff->staff_fname.' '. $staff->staff_lname;?> <?php //echo $requestdetailsInfo->staff_email_message;?>
				<?php
		 
			 $text1 = preg_replace('/\b(name of parent)\b/u', $student->student_father_name, $requestdetailsInfo->student_email_message);
			 $text2 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $text1);
			 $text3 = preg_replace('/\b(name of Student)\b/u', $student->student_fname.' '. $student->student_lname, $text2);
			 $text4 = preg_replace('/\b(username)\b/u', $student->username, $text3);
			 $text5 = preg_replace('/\b(password)\b/u', $student->password, $text4);
			 $text6 = str_replace('(', '', $text5);
			 $Finaltext = str_replace(')', '', $text6);
		echo   $Finaltext; 
        ?>
      </textarea>

      <script>

       CKEDITOR.replace( 'student_request_message_<?php echo $student->id;?>' );

     </script>

    </div>

  </div>

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingrequestdetailmsg-<?php echo $student->id;?>"></div>

        <input class="requestdetailemail" id="save_request_details-<?php echo $student->id;?>" value="Send" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
     </div>

    </div>

  </div>                      

    <!----- ******************************Generate Pop-up  for sending Email for Schedule of student ******************* -----> 
  	
    <div class="modal fade" id="myModal_schedule<?php echo $student->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Email for Schedule Class</h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

        

         <div class="alert alert-danger alert-dismissable" id="scheduleError-<?php echo $student->id;?>" style="display:none" >

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		  <b>Alert!</b> 

			Some problem exists. Message has not been sent. 

		</div>

         <div class="alert alert-success alert-dismissable" id="scheduleSuccess-<?php echo $student->id;?>" style="display:none">

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		    Message has been sent successfully. 

             </div>

        

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>studentenrolement/schedule_user_detail" name="scheduleForm" id="scheduleForm_<?php echo $student->id;?>" method="post">

     
		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $student->id; ?>" />        
		
         <input type="hidden" name="school_id" id="school_id" value="<?php echo $student->school_id;?>" />
  

         <input type="hidden" name="student_email" id="student_schedule_<?php echo $student->id;?>" value="<?php echo $student->email;?>" class="form-control"  placeholder="" maxlength="121">

         

         <input type="hidden" name="student_email_subject" id="student_schedule_subject_<?php echo $student->id;?>" value="<?php echo $scheduleInfo->student_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="student_email_message" id="student_schedule_message_<?php echo $student->id;?>"> 

        <?php //echo $staff->staff_fname.' '. $staff->staff_lname;?> <?php //echo $requestdetailsInfo->staff_email_message;?>
				<?php
		 
			 $text1 = preg_replace('/\b(name of parent)\b/u', $student->student_father_name, $scheduleInfo->student_email_message);
			 $text2 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $text1);
			 $text3 = preg_replace('/\b(name of Student)\b/u', $student->student_fname.' '. $student->student_lname, $text2);
			 $text4 = preg_replace('/\b(username)\b/u', $student->username, $text3);
			 $text5 = preg_replace('/\b(password)\b/u', $student->password, $text4);
			 $text6 = str_replace('(', '', $text5);
			 $Finaltext = str_replace(')', '', $text6);
		echo   $Finaltext; 
        ?>
      </textarea>

      <script>

       CKEDITOR.replace( 'student_schedule_message_<?php echo $student->id;?>' );

     </script>

    </div>

  </div>

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingschedulemsg-<?php echo $student->id;?>"></div>

        <input class="scheduleemail" id="save_schedule_details-<?php echo $student->id;?>" value="Send" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
     </div>

    </div>

  </div>    
  
      <!----- ******************************Generate Pop-up  for sending Email for ReSchedule of student ******************* -----> 
  	
    <div class="modal fade" id="myModal_reschedule<?php echo $student->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Email for Reschedule Class </h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

        

         <div class="alert alert-danger alert-dismissable" id="rescheduleError-<?php echo $student->id;?>" style="display:none" >

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		  <b>Alert!</b> 

			Some problem exists. Message has not been sent. 

		</div>

         <div class="alert alert-success alert-dismissable" id="rescheduleSuccess-<?php echo $student->id;?>" style="display:none">

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		    Message has been sent successfully. 

             </div>

        

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>studentenrolement/schedule_user_detail" name="rescheduleForm" id="rescheduleForm_<?php echo $student->id;?>" method="post">

		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $student->id; ?>" />        
		
         <input type="hidden" name="school_id" id="school_id" value="<?php echo $student->school_id;?>" />

        

         <input type="hidden" name="student_email" id="student_reschedule_<?php echo $student->id;?>" value="<?php echo $student->email;?>" class="form-control"  placeholder="" maxlength="121">

         

         <input type="hidden" name="student_email_subject" id="student_reschedule_subject_<?php echo $student->id;?>" value="<?php echo $rescheduleInfo->student_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="student_email_message" id="student_reschedule_message_<?php echo $student->id;?>"> 

        <?php //echo $staff->staff_fname.' '. $staff->staff_lname;?> <?php //echo $requestdetailsInfo->staff_email_message;?>
				<?php
		 
			 $text1 = preg_replace('/\b(name of parent)\b/u', $student->student_father_name, $rescheduleInfo->student_email_message);
			 $text2 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $text1);
			 $text3 = preg_replace('/\b(name of child)\b/u', $student->student_fname.' '. $student->student_lname, $text2);
			 $text4 = preg_replace('/\b(username)\b/u', $student->username, $text3);
			 $text5 = preg_replace('/\b(password)\b/u', $student->password, $text4);
			 $text6 = str_replace('(', '', $text5);
			 $Finaltext = str_replace(')', '', $text6);
		echo   $Finaltext; 
        ?>
      </textarea>

      <script>

       CKEDITOR.replace( 'student_reschedule_message_<?php echo $student->id;?>' );

     </script>

    </div>

  </div>

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingreschedulemsg-<?php echo $student->id;?>"></div>

        <input class="rescheduleemail" id="save_reschedule_details-<?php echo $student->id;?>" value="Send" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
     </div>

    </div>

  </div>  
  <!----- Generate Pop-up  for Notes on Enrolled student-----> 
  
  
<div class="modal fade" id="myModal_notes_<?php echo $student->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Notes for <?php echo $student->student_fname;?></h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

         <div class="alert alert-danger alert-dismissable" id="sendenroleNotesError-<?php echo $student->id;?>" style="display:none" >

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		  <b>Alert!</b> 

			Some problem exists. Notes has not been saved. 

		</div>

         <div class="alert alert-success alert-dismissable" id="sendenroleNotesSuccess-<?php echo $student->id;?>" style="display:none">

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		    Message has been saved successfully. 

             </div>


        <div class="modal-body">

     <form action="<?php echo base_url();?>studentenrolement/notesfor_user_enrolement" name="enrolementnotesForm" id="enrolementnotesForm_<?php echo $student->id;?>" method='post'>
	 
		<input type="hidden" name="tab_value"  id="tab_value"  value="waiting" />

         <input type="hidden" name="school_id" id="school_id" value="<?php echo $student->school_id;?>" />

           <input type="hidden" name="user_id" id="user_id" value="<?php echo $student->id;?>" />

          <div class="col-sm-12">

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor1" name="student_notes" id="student_notes_<?php echo $student->id;?>"> 
		<?php echo $student->student_notes ; ?>
      </textarea>

      <script>

	 CKEDITOR.replace( 'student_notes_<?php echo $student->id;?>' );

     </script>

    </div>

  </div>


  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingnotesmsg-<?php echo $student->id;?>"></div>

        <input class="savenotes" id="notes" value="Save" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>

    </div>

  </div>                   
                  

 
                        </td>

                        </tr>

                       <?php	}		

					} else { ?>

                    <tr><th colspan="7" style="text-align: center; width:1215px;height:100px;font-size:25px; background:#FFF;  color: #6a7a91;">No record to show.</th></tr>	

				   <?php } ?> 

                                        

				</tbody>

		  </table>

          

    <div class="profile-bg">

	 <div class="col-sm-12 paginationdiv nopadding">



     <div class="col-sm-8 col-xs-6 paginationblk">

         <?php  if(count($data_rows) > 0){

			       $last_page1=$last_page;

			   ?>

			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries.

			<?php  } ?>

	    <ul class="pagination">

		<?php echo $pagination;?>

	     </ul>

      </div>
<div class="col-sm-4 exceldiv">
<?php  if(count($data_rows) > 0){


			   ?>

        <form method="post"  name="export_form" action="<?php echo base_url();?>studentenrolement/excel_action">
                
      <input type="hidden" name="branchsrch_id" id="branchsrch_id" value="<?php echo @$branchsrch_id; ?>"  /> 
       
      <input type="hidden" name="branch_data" id="branch_data" value="<?php echo $branch_data; ?>"  />
        
     <input type="hidden" name="exceltab" id="exceltab" value="waiting"  /> 
   
    <input type="hidden" name="sdate_search" id="sdate_search" value="<?php echo $sdate_search; ?>"  />  
    
    <input type="hidden" name="edate_search" id="edate_search" value="<?php echo $edate_search; ?>"  /> 
   
      <input type="submit" name="export" class="btn btn-success excelbtn" value="Export to Excel" />
    
    </form>
    	<?php  } ?>
</div>
    

	 </div>

    </div>

          

          

          

          

         </div>



	   </div>

    

	 </div>

    </div>

  

  <script type="text/javascript">

	function delConfirm(id){

		if( confirm("Are you sure want to Delete?") ){

			window.location.href ="<?php echo base_url();?>studentenrolement/deletewaiting/"+id;

		}else{

			return false;

		}

	}

</script>

 <script>

setTimeout(function() {
    $('.alert').fadeOut('fast');
}, 2000);

</script> 

 <script>

  jQuery(document).ready(function(){

	    $("#sdate").keydown(false);

	    $("#edate").keydown(false);

		

  	 jQuery("#studentSearchForm").validate({

        rules: {

			 sdate: {

    					required: true

   					},

			  edate: {

    					required: true

   					}

            },

        messages: {

			  sdate: {

    				required: "Please select start date."

   				 },

			  edate: {

    				required: "Please select end date."

   				 }

             },

       

        submitHandler: function(form) {

            form.submit();

          }

        });

	 });

</script>



<style>

.btn-info { 

	background-color: #37B148;

    border-color: #37B148; 

	}

.btn-info:hover {

    background-color: #37B148;

    border-color: #37B148;

    color: #fff;

}



.action-btn a {

	background:none;

	border:none;

}

#myModal { margin-top:140px; } 



.save_btn > input {

    background: #37b248 none repeat scroll 0 0;

    border: 1px solid #ccc;

    border-radius: 3px;

    color: #fff;

    font-size: 14px;

    padding: 7px 20px;

}

.save_btn {

    padding-top: 20px;

}

.popup_save_btn {

    background: #37b248 none repeat scroll 0 0;

    border: medium none;

    border-bottom-right-radius: 3px;

    border-top-right-radius: 3px;

    color: #fff;

    padding: 12px 25px;

}

.modal-body {

    padding: 35px 0 70px;

}

.text_box_sec > input {

    border: 1px solid #ccc;

    border-radius: 3px;

    min-height: 45px;

    width: 100%;

	padding: 10px;

}

.btn.btn-default {

    background: #37b248 none repeat scroll 0 0;

    color: #fff;

}

.text_box_sec > input {

    border: 1px solid #ccc;

    border-bottom-left-radius: 3px;

    border-top-left-radius: 3px;

    min-height: 44px;

    padding: 10px;

    width: 100%;

}

.save_btn {

    text-align: right;

}

.text_box_btn {

    text-align: left;

}

.red_line { color:#F00 !important; }

.yellow_line { color:#990 !important; }

.blank_trans_data {

    color: transparent;

}

.search {

    padding: 10px 0 60px !important;

}

.invitemember {

    background: #37b248 none repeat scroll 0 0;

    color: #fff;

    padding-bottom: 5px;

    padding-top: 5px;

}



.close.closenotify {
    background: #fff none repeat scroll 0 0;
    border: 2px solid #37b248;
    border-radius: 40px;
    color: #36b047;
    float: right;
    font-size: 28px;
    height: 40px;
    left: 30px;
    margin-top: -23px;
    opacity: 1;
    position: relative;
    width: 40px;
}
.modal-content.enrolmentpopup{float:left;width:100%;}
.confirmlink .sendenrolementemail {
    border: 0 none;
    font-weight: bold;
    padding: 10px 30px;
    text-transform: uppercase;
}
.enrolmentpopup .modal-body{float:left;width:100%;}
.savepostponedate {
    background: #37b248 none repeat scroll 0 0;
    border: 0 none;
    color: #fff;
    padding: 12px 30px;
}
.modal-dialog.popupdiv{width:90%;}
.cke_contents.cke_reset {
    height: 350px !important;
}
.enrolmentpopup .modal-body {
    float: left;
    padding: 5px 0 0;
    width: 100%;
}
.confirmlink input{margin:6px 0px;}
.popup_section_form {
    padding: 30px 10px;
}



</style>