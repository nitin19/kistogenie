<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Viewprogressreport_model extends CI_Model {
	
	var $tbl	= "progress_report";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $classes = "classes";
	var $term = "term";
	var $staff = "staff";
	var $subjects = "subjects";
	var $student_report_levels = "student_report_levels";
	
	
    function __construct() {
        parent::__construct();
    }
	
	function  getStudentinfo($student_id) {
        $this->db->select('*');
        $this->db->from($this->student);
		$this->db->where('student_id', $student_id);
        $query = $this->db->get();
        return  $query->row();
	 }
	
	function  getTerminfo($term_id) {
        $this->db->select('*');
        $this->db->from($this->term);
		$this->db->where('term_id', $term_id);
        $query = $this->db->get();
        return  $query->row();
	 }
	 
	 function  getBranchinfo($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
	 }
	 
	 function  getClassinfo($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where('class_id', $class_id);
        $query = $this->db->get();
        return  $query->row();
	 }
	 
   	function get_term_report($student_id,$school_id,$branch_id,$class_id,$term_id,$year) {
		$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("student_id="=>$student_id,"school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"term"=>$term_id,"progress_report_year"=>$year,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query(); 
         return  $query->result();
	   }
	   
	function get_subject_name($subjectid,$school_id,$branch_id,$class_id) {
        $this->db->select('*');
        $this->db->from($this->subjects);
		$this->db->where(array("subject_id="=>$subjectid,"school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
         return  $query->row();
    }	   
  	 
	/* function get_teacher_name($school_id,$branch_id,$class_id,$subjectid) {
        $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$branch_id));
		
		$where = "FIND_IN_SET('".$class_id."', class_name)";  
		$this->db->where( $where ); 
		
		$where = "FIND_IN_SET('".$subjectid."', subject_name)";  
		$this->db->where( $where ); 
		
        $query = $this->db->get();
		 $this->db->last_query();
         return  $query->row();
    }*/

    function get_teacher_name($staff_id) {
        $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("staff_id="=>$staff_id));
				
        $query = $this->db->get();
		 $this->db->last_query();
         return  $query->row();
    }


    function get_current_level($sublevel_id) {
        $this->db->select('*');
        $this->db->from($this->student_report_levels);
		$this->db->where('level_id', $sublevel_id);
        $query = $this->db->get();
        return  $query->row();
	 }	  
	 
}



 
