    </div>

    <footer class="dashboard-footer">

   <div class="copyright">

   <p>© <?php echo date("Y");?> kistogenie.com. All Rights Reserved. | Powered by : <span><a href="http://www.binarydata.in/" target="_blank">Binary Data</a></span></p>

   </div>

   </footer>

    </div>

    <script>
	 
	 jQuery().ready(function() {
		 
var dateToday = new Date();
var yrRange1 = dateToday.getFullYear();
var yrRange2 = (dateToday.getFullYear() - 86);
var yrRange3 = (dateToday.getFullYear() - 18);
		 
	      jQuery( ".datepicker" ).datepicker({
			   yearRange: "-57:-18",
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy" 
			   });
		  jQuery( ".datepicker1" ).datepicker({ 
		       //yearRange: '1960:' + (new Date().getFullYear()-2),
			   yearRange: "-16:-3",
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy",
			   maxDate: '0' 
		  });
		  jQuery( ".datepicker2" ).datepicker({ 
		       yearRange: '1990:' + new Date().getFullYear(),
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy",
			   maxDate: '0' 
		  });
		  jQuery( ".datepicker3" ).datepicker({ 
		   	   yearRange: '1990:' + (new Date().getFullYear()+1),
			   changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy",
			   maxDate: '31-12-17'
			});
		  jQuery( ".datepicker4" ).datepicker({ 
		       yearRange: '1990:' + (new Date().getFullYear()+5),
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy",
			   maxDate: '31-12-2022' 
			 });
			 
			   jQuery( ".datepicker5" ).datepicker({
			   yearRange: "-57:-18",
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy" 
			   });
			   
			   jQuery( ".datepickerattendance" ).datepicker({
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy" ,
			  /* minDate: '0',*/
			   maxDate: '0'
			   });
			   
			   jQuery( ".datepickerattendancereport1" ).datepicker({
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy" ,
			  /* minDate: '0',*/
			   maxDate: '0',
			   
		 onSelect: function(date){            
            var date1 = jQuery('.datepickerattendancereport1').datepicker('getDate');           
            var date = new Date( Date.parse( date1 ) ); 
            date.setDate( date.getDate() + 1 );        
            var newDate = date.toDateString(); 
            newDate = new Date( Date.parse( newDate ) );                      
            jQuery('.datepickerattendancereport2').datepicker("option","minDate",newDate);            
             }
			   
			   }); 
			   
		   jQuery( ".datepickerattendancereport2" ).datepicker({
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy" ,
			  /* minDate: '0',*/
			   maxDate: '0',
          });   
		  
		  
		   jQuery( ".datepickerattendancesummary1" ).datepicker({
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy" ,
			  /* minDate: '0',*/
			   maxDate: '0',
			   
		 onSelect: function(date){            
            var date1 = jQuery('.datepickerattendancesummary1').datepicker('getDate');           
            var date = new Date( Date.parse( date1 ) ); 
            date.setDate( date.getDate() + 1 );        
            var newDate = date.toDateString(); 
            newDate = new Date( Date.parse( newDate ) );                      
            jQuery('.datepickerattendancesummary2').datepicker("option","minDate",newDate);            
             }
			   
			   }); 
			   
		   jQuery( ".datepickerattendancesummary2" ).datepicker({
		       changeMonth: true,
    		   changeYear: true,
		       dateFormat: "dd-mm-yy" ,
			  /* minDate: '0',*/
			   maxDate: '0',
          });
		   jQuery( ".datepickerpostpone" ).datepicker({
		       changeMonth: true,
    		  // changeYear: true,
		       dateFormat: "dd-mm-yy" 			  
			   });   
		  
			   
	   });
       
      </script>

  </body>

</html>