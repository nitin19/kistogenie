<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="editprofile-content">
    	<div class="profilemenus">
        <ul>
          <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

         <li><a href="<?php echo base_url(); ?>staffenrolement">Staff Enrolment</a></li>
       <li class="edit">Waiting Staff</li>        
        </ul>
        </div>
               <div style="clear:both"></div>

 <?php if($this->session->flashdata('error')): ?>

   <div class="alert alert-danger alert-dismissable" >

			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

			<b>Alert!</b> 

			  <?php echo $this->session->flashdata('error'); ?>

		</div>

<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>

     <div class="alert alert-success alert-dismissable" >

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		   <?php echo $this->session->flashdata('success'); ?>

	   </div>

<?php endif; ?>

 <div style="clear:both"></div> 
        <div class="attendancesec">	
    <div class="col-sm-12 tablediv nopadding">
    <div class="col-sm-12 nopadding">
    <div class="tabletitlediv">
    <h1>Staff Registration Detail</h1>
    <div class="col-sm-12 tablenavdiv">
    <div class="col-sm-7 tablenevleft">
    <ul>
     <?php if($branch_data!=''){?>
      <li><a href="<?php echo base_url();?>staffenrolement/index/<?php echo $applied_last_page;?>/<?php echo $branch_data;?>">Applied Staff</a></li>
      <?php }else {?>
    <li><a href="<?php echo base_url();?>staffenrolement/index">Applied Staff</a></li>
    <?php }?>
 
      <?php if($branch_data!=''){?>
    <li><a href="<?php echo base_url();?>staffenrolement/selectedstaffs/<?php echo $select_last_page;?>/<?php echo $branch_data;?>">Selected Staff</a></li>
      <?php }else{?>
    <li><a href="<?php echo base_url();?>staffenrolement/selectedstaffs">Selected Staff</a></li>
     <?php } ?>
     
<?php if($branch_data!=''){?>
	<li><a href="<?php echo base_url();?>staffenrolement/rejectedstaffs/<?php echo $reject_last_page;?>/<?php echo $branch_data;?>">Rejected Staff</a></li>
  <?php }else {?>
     <li><a href="<?php echo base_url();?>staffenrolement/rejectedstaffs">Rejected Staff</a></li>
  <?php }?>
  
  <?php if($branch_data!=''){?>
  <li  class="active"><a href="<?php echo base_url();?>staffenrolement/waitingstaffs/<?php echo $last_page;?>/<?php echo $branch_data;?>">Waiting Staff</a></li>
  <?php }else {?>
    <li class="active"><a href="<?php echo base_url();?>staffenrolement/waitingstaffs">Waiting Staff</a></li>
    <?php }?>
    </ul>
    </div>
    <div class="col-sm-5 tablenevright">
<form action="<?php echo base_url();?>staffenrolement/waitingstaffs/0/" name="staffSearchForm" id="staffSearchForm" method="get">
             <div class="col-sm-12 col-xs-12 applycodediv nopadding datepickerspacing">
               
                <div class="col-sm-3 col-xs-3 nopadding dateblk">
        		<div class="form-group">
    <div class="inputbox datepickerdiv nopadding">
   <?php if($branch_data !='') {
?>
<input type="hidden" name="branchidsrch" value="<?php echo $branch_data; ?>" />

<?php
} ?>
       <input type="text" name="sdate" id="sdate" value="<?php echo @$sdate_search;?>" class="datepickerattendancesummary1">
    </div>
  </div> 
  				</div>
                
                <div class="col-sm-1 datepickercenter">
                <span>-</span>
                </div>
                
                <div class="col-sm-3 col-xs-3 nopadding dateblk">
        		 <div class="form-group">
    <div class="inputbox datepickerdiv nopadding">
      <!--<input type="text" name="MyDate1" class="datepicker1">-->
       <input type="text" name="edate" id="edate" value="<?php echo @$edate_search;?>" class="datepickerattendancesummary2">
    </div>
  </div> 
  				</div>
                
                 <div class="col-sm-3 col-xs-3">
        		  <div class="form-group generatereport">
   					 <input type="submit" class="btn btn-danger confirm" value="View Staff">
 					 </div> 
  				  </div>
               </div>
         </form>
        </div>

    </div>
    </div>
    </div>
    <div class="tablewrapper">
<table class="table-bordered table-striped">
			  <thead>
				  <tr class="headings enrolment-headings">
					  <th class="column1">No.</th>
					  <th class="column3">Name</th>
					  <th class="column3">Applied for Branch</th>
                      <th class="column3">Postponed Date</th>
                      <th class="column4">Email Address</th>
                      <th class="column3">Phone Number</th>
                      <th class="column3">Action</th>
				  </tr>
			  </thead>
				<tbody>
                <?php  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $staff) { 
					     $sr++;
								$branch_id=$staff->branch_id;
								$user_id = $staff->user_id;
								 $this->load->model(array('staffenrolement_model'));
								 $branchName = $this->staffenrolement_model->getbranchName($branch_id);
								 //$user=$this->staffenrolement_model->getuser($user_id);
					    ?>
					<tr class="familydata student-registration enrolment-table">

						<td class="column1"><?php echo $sr; ?></td>
						
                        <td class="column3"><?php echo $staff->staff_fname.' '. $staff->staff_lname; ?></td>
						
                        <td class="column3"><?php echo @$branchName->branch_name;?></td>
                        
                        <td class="column3"><?php echo date('M d, Y', strtotime( $staff->staff_postpone_date));?></td>
                       
                        <td class="column4"><?php
						if($staff->email!='')
								{echo $staff->email; }
							else{
							 echo '<span style="color:transparent;">-</span>';	
							} ?>
                            </td>
                        
                        <td class="column3"><?php 
						if($staff->staff_telephone!='')
								{ echo $staff->staff_telephone; }
							else{
							 	echo '<span style="color:transparent;">-</span>';	
							}?> </td>
                        
                        <td class="column3 action-btn"><!--<a href="#">Action <i class="fa fa-angle-down"></i></a>-->
                         
                         <div class="btn-group">
        <button type="button" class="btn btn-info btn-flat">Action</button>
        <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
        <li> <a href="<?php echo base_url();?>staffenrolement/view_enrolmentstaff/<?php echo $staff->id;?>" target="_blank"> View </a> </li>  
         <li> <a href="<?php echo base_url();?>staffenrolement/edit_enrolmentstaff/<?php echo $staff->id;?>" target="_blank"> Edit </a> </li>
        <li> <a href="#" data-toggle="modal" data-target="#myModal_request_detail<?php echo $staff->id;?>"> Request details  </a> </li>
        <li class="divider"></li>  
        <li><a href="#"  data-toggle="modal" data-target="#myModal_applied_<?php echo $staff->id;?>"> Applied </a>  </li>                                
        <li> <a href="#" data-toggle="modal" data-target="#myModal_enrole_<?php echo $staff->id;?>"> Enrolled  </a> </li>
        <li><a href="#"  data-toggle="modal" data-target="#myModal_reject_<?php echo $staff->id;?>"> Rejected </a>  </li>

		<li class="divider"></li>
         <li> <a href="#"  data-toggle="modal" data-target="#myModal_schedule_<?php echo $staff->id;?>"> Schedule Email</a> </li>
        <li><a href="#"  data-toggle="modal" data-target="#myModal_reschedule_<?php echo $staff->id;?>" > Reschedule Email</a>  </li>   
        <li class="divider"></li>
        <li> <a href="#" data-toggle="modal" data-target="#myModal_notes_<?php echo $staff->id;?>">Notes </a> </li>
        
		<li class="divider"></li>
            
		<li><a onclick="delConfirm(<?php echo $staff->id;?>)" style="cursor: pointer;"> Delete </a> </li>
									
                    </ul>
                </div>                     

                          </td></tr>
 
 <!--  Generate Pop-up  for sending Email for request details of staff --> 
   
 	<div class="modal fade" id="myModal_request_detail<?php echo $staff->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Email for Request Detail </h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>staffenrolement/request_user_detail/<?php echo $staff->school_id;?>" name="requestdetailForm" id="requestdetailForm_<?php echo $staff->id;?>" method="post">

     
		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="school_id" id="school_id" value="<?php echo $staff->school_id;?>" />

		 <input type="hidden" name="user_id" id="user_id" value="<?php echo $staff->id;?>" />

        

         <input type="hidden" name="staff_email" id="staff_request_<?php echo $staff->id;?>" value="<?php echo $staff->email;?>" class="form-control"  placeholder="" maxlength="121">

         

         <input type="hidden" name="staff_email_subject" id="staff_request_subject_<?php echo $staff->id;?>" value="<?php echo $requestdetailsInfo->staff_email_subject;?>" class="form-control"  placeholder="" >

     

          <div class="col-sm-12">

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="staff_email_message" id="staff_request_message_<?php echo $staff->id;?>"> 

        <?php //echo $staff->staff_fname.' '. $staff->staff_lname;?> <?php //echo $requestdetailsInfo->staff_email_message;?>
				<?php

       if($staff->created_date=='0000-00-00') {
       $staff_enrolment_date = date('d-m-Y');
       } else {
       $staff_enrolment_date = date('d-m-Y', strtotime($staff->created_date));
        }

        $description = $requestdetailsInfo->staff_email_message;

 if($staff->staff_title == 'Mrs' || $staff->staff_title == 'Ms' ){

  if (strpos($description,'(his-her)') > 0) {
      $pronoun = 'her';
    }else {
      $pronoun='Her';
    }

  if (strpos($description,'(gender)') > 0 ) {

    $var= strpos($description,'(gender)');

      $gender='she';
    }else {
      $gender='She';
    }
  if (strpos($description,'(Gender)') > 0 ) {

      $gender1='she';
    }else {
      $gender1='She';
    }
  $pronoun1 = 'her';
   
   }else{

    if (strpos($description,'(his-her)') > 0) {
       $pronoun = 'his';
    }else {
       $pronoun = 'His';
    }
    if (strpos($description,'(gender)') > 0) {
      $gender='he';
    }else {
      $gender='He';
    }
    if (strpos($description,'(Gender)') > 0 ) {

      $gender1='he';
    }else {
      $gender1='He';
    }
    $pronoun1 = 'him';

   }
		 
			$text1 = preg_replace('/\b(name of staff)\b/u', $staff->staff_fname.' '. $staff->staff_lname, $requestdetailsInfo->staff_email_message);
			$text2 = preg_replace('/\b(school name)\b/u', $school->school_name, $text1);
      $text3 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $text2);
      $text4 = preg_replace('/\b(date of enrolment)\b/u', $staff_enrolment_date, $text3);
      $text5 = preg_replace('/\b(branch contact number)\b/u', $school->school_phone, $text4);
      $text6 = preg_replace('/\b(qualification)\b/u',$staff->staff_education_qualification, $text5);
      $text7 = preg_replace('/\b(date of birth)\b/u',$staff->staff_dob, $text4);
      $text8 = preg_replace('/\b(email id)\b/u',$staff->email, $text7);
      $text9 = preg_replace('/\b(teaching days)\b/u',$staff->teaching_days, $text8);
      $text10 = preg_replace('/\b(salary)\b/u',$staff->salary_currency.' '.$staff->salary_amount.' '.$staff->salary_mode, $text9);
      $text11 = preg_replace('/\b(username)\b/u',$staff->username, $text10);
      $text12 = preg_replace('/\b(password)\b/u',$staff->password, $text11);
      $text13 = preg_replace('/\b(gender)\b/u',$gender, $text12);
      $text14 = preg_replace('/\b(Gender)\b/u',$gender1, $text13);
      $text15 = preg_replace('/\b(his-her)\b/u',$pronoun, $text14);
      $text16 = preg_replace('/\b(him-her)\b/u', $pronoun1, $text15);

      $text17 = str_replace('(', '', $text16);
      $Finaltext = str_replace(')', '', $text17);
		echo   $Finaltext; 
        ?>
      </textarea>

      <script>

       CKEDITOR.replace( 'staff_request_message_<?php echo $staff->id;?>' );

     </script>

    </div>

  </div>

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingrequestdetailmsg-<?php echo $staff->id;?>"></div>

        <input class="requestdetailemail" id="save_request_details-<?php echo $staff->id;?>" value="Send" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
     </div>

    </div>

  </div>
  
    <!-- Generate Pop-up  for sending Email on Applied  staff --> 
    
 	<div class="modal fade" id="myModal_applied_<?php echo $staff->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Applied Email</h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>staffenrolement/applied_user_enrolement/<?php echo $staff->id;?>" name="appliedemailForm" id="appliedemailForm_<?php echo $staff->id;?>" method="post">

		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="school_id" id="school_id" value="<?php echo $staff->school_id;?>" />

		 <input type="hidden" name="user_id" id="user_id" value="<?php echo $staff->id;?>" />
        

         <input type="hidden" name="staff_email" id="staff_email_<?php echo $staff->id;?>" value="<?php echo $staff->email;?>" class="form-control"  placeholder="" maxlength="121">

         

         <input type="hidden" name="staff_email_subject" id="staff_appliedemail_subject_<?php echo $staff->id;?>" value="<?php echo $appliedInfo->staff_email_subject;?>" class="form-control"  placeholder="" >

     

          <div class="col-sm-12">

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="staff_email_message" id="staff_appliedemail_message_<?php echo $staff->id;?>"> 

        <?php // echo $staff->staff_fname.' '. $staff->staff_lname;?> <?php //echo $rejectedInfo->staff_email_message;?>
        
		<?php
     if($staff->created_date=='0000-00-00') {
       $staff_enrolment_date = date('d-m-Y');
       } else {
       $staff_enrolment_date = date('d-m-Y', strtotime($staff->created_date));
        }

$description = $appliedInfo->staff_email_message;

 if($staff->staff_title == 'Mrs' || $staff->staff_title == 'Ms' ){

  if (strpos($description,'(his-her)') > 0) {
      $pronoun = 'her';
    }else {
      $pronoun='Her';
    }

  if (strpos($description,'(gender)') > 0 ) {

    $var= strpos($description,'(gender)');

      $gender='she';
    }else {
      $gender='She';
    }
  if (strpos($description,'(Gender)') > 0 ) {

      $gender1='she';
    }else {
      $gender1='She';
    }
  $pronoun1 = 'her';
   
   }else{

    if (strpos($description,'(his-her)') > 0) {
       $pronoun = 'his';
    }else {
       $pronoun = 'His';
    }
    if (strpos($description,'(gender)') > 0) {
      $gender='he';
    }else {
      $gender='He';
    }
    if (strpos($description,'(Gender)') > 0 ) {

      $gender1='he';
    }else {
      $gender1='He';
    }
    $pronoun1 = 'him';

   }

		 
			$text1 = preg_replace('/\b(name of staff)\b/u', $staff->staff_fname.' '. $staff->staff_lname, $appliedInfo->staff_email_message);
      $text2 = preg_replace('/\b(school name)\b/u', $school->school_name, $text1);
      $text3 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $text2);
      $text4 = preg_replace('/\b(date of enrolment)\b/u', $staff_enrolment_date, $text3);
      $text5 = preg_replace('/\b(branch contact number)\b/u', $school->school_phone, $text4);
      $text6 = preg_replace('/\b(qualification)\b/u',$staff->staff_education_qualification, $text5);
      $text7 = preg_replace('/\b(date of birth)\b/u',$staff->staff_dob, $text4);
      $text8 = preg_replace('/\b(email id)\b/u',$staff->email, $text7);
      $text9 = preg_replace('/\b(teaching days)\b/u',$staff->teaching_days, $text8);
      $text10 = preg_replace('/\b(salary)\b/u',$staff->salary_currency.' '.$staff->salary_amount.' '.$staff->salary_mode, $text9);
      $text11 = preg_replace('/\b(username)\b/u',$staff->username, $text10);
      $text12 = preg_replace('/\b(password)\b/u',$staff->password, $text11);
      $text13 = preg_replace('/\b(gender)\b/u',$gender, $text12);
      $text14 = preg_replace('/\b(Gender)\b/u',$gender1, $text13);
      $text15 = preg_replace('/\b(his-her)\b/u',$pronoun, $text14);
      $text16 = preg_replace('/\b(him-her)\b/u', $pronoun1, $text15);

      $text17 = str_replace('(', '', $text16);
      $Finaltext = str_replace(')', '', $text17);
		echo   $Finaltext; 
        ?>
      </textarea>

      <script>

       CKEDITOR.replace( 'staff_appliedemail_message_<?php echo $staff->id;?>' );

     </script>

    </div>

  </div>

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingappliedmsg-<?php echo $staff->id;?>"></div>

        <input class="sendappliedemail" id="save_applied-<?php echo $staff->id;?>" value="Send" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
     </div>

    </div>

  </div>

    
    <!-- Generate Pop-up  for sending Email on Enrolled staff--> 
   
    <div class="modal fade" id="myModal_enrole_<?php echo $staff->id;?>" role="dialog">
        
            <div class="modal-dialog popupdiv">
        
              <!-- Modal content-->
        
              <div class="modal-content enrolmentpopup">
        
                <div class="modal-header" style="padding:0px;">
        
                  <div class="col-sm-12 col-xs-11 nopadding invitemember">
        
                  <div class="col-sm-9"><h3 class="modal-title inviteheader">Enrolled Email</h3></div>
        
                   <div class="col-sm-3">
        
                   <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>
        
                   </div>
        
                  </div>
        
                </div>
        
    <div style="clear:both"></div>  
        
    <div class="modal-body">
        
    <form action="<?php echo base_url(); ?>staffenrolement/add_user_enrolement/<?php echo $staff->id;?>" name="enrolementemailForm" id="enrolementemailForm_<?php echo $staff->id;?>" method="post">
        
     <input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
     <input type="hidden" name="school_id" id="school_id" value="<?php echo $staff->school_id;?>" />

		 <input type="hidden" name="user_id" id="user_id" value="<?php echo $staff->id;?>" />
        
     <input type="hidden" name="staff_email" id="staff_email_<?php echo $staff->id;?>" value="<?php echo $staff->email;?>" class="form-control"  placeholder="" maxlength="121">
        
     <input type="hidden" name="staff_email_subject" id="staff_email_subject_<?php echo $staff->id;?>" value="<?php echo $selectedInfo->staff_email_subject;?>" class="form-control"  placeholder="" maxlength="251">
        
             
        
      <div class="col-sm-12">
        
      <div class="form-group">
      
      <div class="col-sm-12 inputbox nopadding">
        
      <textarea class="ckeditor" name="staff_email_message" id="staff_email_message_<?php echo $staff->id;?>"> 
        
      <?php /*?> <?php echo $staff->staff_fname.' '. $staff->staff_lname;?> <?php echo $selectedInfo->staff_email_message;?><?php */?>
      <?php
      if($staff->created_date=='0000-00-00') {
        $staff_enrolment_date = date('d-m-Y');
      } else {
        $staff_enrolment_date = date('d-m-Y', strtotime($staff->created_date));
      }

$description = $selectedInfo->staff_email_message;

 if($staff->staff_title == 'Mrs' || $staff->staff_title == 'Ms' ){

  if (strpos($description,'(his-her)') > 0) {
      $pronoun = 'her';
    }else {
      $pronoun='Her';
    }

  if (strpos($description,'(gender)') > 0 ) {

    $var= strpos($description,'(gender)');

      $gender='she';
    }else {
      $gender='She';
    }
  if (strpos($description,'(Gender)') > 0 ) {

      $gender1='she';
    }else {
      $gender1='She';
    }
  $pronoun1 = 'her';
   
   }else{

    if (strpos($description,'(his-her)') > 0) {
       $pronoun = 'his';
    }else {
       $pronoun = 'His';
    }
    if (strpos($description,'(gender)') > 0) {
      $gender='he';
    }else {
      $gender='He';
    }
    if (strpos($description,'(Gender)') > 0 ) {

      $gender1='he';
    }else {
      $gender1='He';
    }
    $pronoun1 = 'him';

   }

      $text1 = preg_replace('/\b(name of staff)\b/u', $staff->staff_fname.' '. $staff->staff_lname, $selectedInfo->staff_email_message);
      $text2 = preg_replace('/\b(school name)\b/u', $school->school_name, $text1);
      $text3 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $text2);
      $text4 = preg_replace('/\b(date of enrolment)\b/u', $staff_enrolment_date, $text3);
      $text5 = preg_replace('/\b(branch contact number)\b/u', $school->school_phone, $text4);
      $text6 = preg_replace('/\b(qualification)\b/u',$staff->staff_education_qualification, $text5);
      $text7 = preg_replace('/\b(date of birth)\b/u',$staff->staff_dob, $text4);
      $text8 = preg_replace('/\b(email id)\b/u',$staff->email, $text7);
      $text9 = preg_replace('/\b(teaching days)\b/u',$staff->teaching_days, $text8);
      $text10 = preg_replace('/\b(salary)\b/u',$staff->salary_currency.' '.$staff->salary_amount.' '.$staff->salary_mode, $text9);
      $text11 = preg_replace('/\b(username)\b/u',$staff->username, $text10);
      $text12 = preg_replace('/\b(password)\b/u',$staff->password, $text11);
      $text13 = preg_replace('/\b(gender)\b/u',$gender, $text12);
      $text14 = preg_replace('/\b(Gender)\b/u',$gender1, $text13);
      $text15 = preg_replace('/\b(his-her)\b/u',$pronoun, $text14);
      $text16 = preg_replace('/\b(him-her)\b/u', $pronoun1, $text15);

      $text17 = str_replace('(', '', $text16);
      $Finaltext = str_replace(')', '', $text17);
                echo   $Finaltext; 
                ?>
        
              </textarea>
        
              <script>
        
               CKEDITOR.replace( 'staff_email_message_<?php echo $staff->id;?>' );
        
             </script>
        
            </div>
        
          </div>
        
        
          <div class="col-sm-12 nopadding">
        
                <div class="confirmlink">
        
                <div id="Loadingenrolemsg-<?php echo $staff->id;?>"></div>
        
                <input class="sendenrolementemail" id="save_enrole-<?php echo $staff->id;?>" value="Send" type="submit">
        
                </div>
        
                </div>

        
                     </div>  
        
                  </form>
        
                </div>
                <div class="modal-footer">
                <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
              </div>
        
            </div>
        
          </div> 
                        
    <!-- Generate Pop-up  for sending Email on Rejected staff--> 
  
  	<div class="modal fade" id="myModal_reject_<?php echo $staff->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Rejection Email</h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

       <div style="clear:both"></div> 

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>staffenrolement/reject_user_enrolement" name="rejectemailForm" id="rejectemailForm_<?php echo $staff->id;?>" method="post">

     
		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="school_id" id="school_id" value="<?php echo $staff->school_id;?>" />

		 <input type="hidden" name="user_id" id="user_id" value="<?php echo $staff->id;?>" />

     <input type="hidden" name="staff_email" id="staff_rejectemail_<?php echo $staff->id;?>" value="<?php echo $staff->email;?>" class="form-control"  placeholder="" maxlength="121">

     <input type="hidden" name="staff_email_subject" id="staff_rejectemail_subject_<?php echo $staff->id;?>" value="<?php echo $rejectedInfo->staff_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

       <div class="col-sm-12">

       <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="staff_email_message" id="staff_rejectemail_message_<?php echo $staff->id;?>"> 

        <?php // echo $staff->staff_fname.' '. $staff->staff_lname;?> <?php //echo $rejectedInfo->staff_email_message;?>
        
		<?php
		if($staff->created_date=='0000-00-00') {
        $staff_enrolment_date = date('d-m-Y');
      } else {
        $staff_enrolment_date = date('d-m-Y', strtotime($staff->created_date));
      }

$description = $rejectedInfo->staff_email_message;

 if($staff->staff_title == 'Mrs' || $staff->staff_title == 'Ms' ){

  if (strpos($description,'(his-her)') > 0) {
      $pronoun = 'her';
    }else {
      $pronoun='Her';
    }

  if (strpos($description,'(gender)') > 0 ) {

    $var= strpos($description,'(gender)');

      $gender='she';
    }else {
      $gender='She';
    }
  if (strpos($description,'(Gender)') > 0 ) {

      $gender1='she';
    }else {
      $gender1='She';
    }
  $pronoun1 = 'her';
   
   }else{

    if (strpos($description,'(his-her)') > 0) {
       $pronoun = 'his';
    }else {
       $pronoun = 'His';
    }
    if (strpos($description,'(gender)') > 0) {
      $gender='he';
    }else {
      $gender='He';
    }
    if (strpos($description,'(Gender)') > 0 ) {

      $gender1='he';
    }else {
      $gender1='He';
    }
    $pronoun1 = 'him';

   }
			$text1 = preg_replace('/\b(name of staff)\b/u', $staff->staff_fname.' '. $staff->staff_lname, $rejectedInfo->staff_email_message);
			$text2 = preg_replace('/\b(school name)\b/u', $school->school_name, $text1);
      $text3 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $text2);
      $text4 = preg_replace('/\b(date of enrolment)\b/u', $staff_enrolment_date, $text3);
      $text5 = preg_replace('/\b(branch contact number)\b/u', $school->school_phone, $text4);
      $text6 = preg_replace('/\b(qualification)\b/u',$staff->staff_education_qualification, $text5);
      $text7 = preg_replace('/\b(date of birth)\b/u',$staff->staff_dob, $text4);
      $text8 = preg_replace('/\b(email id)\b/u',$staff->email, $text7);
      $text9 = preg_replace('/\b(teaching days)\b/u',$staff->teaching_days, $text8);
      $text10 = preg_replace('/\b(salary)\b/u',$staff->salary_currency.' '.$staff->salary_amount.' '.$staff->salary_mode, $text9);
      $text11 = preg_replace('/\b(username)\b/u',$staff->username, $text10);
      $text12 = preg_replace('/\b(password)\b/u',$staff->password, $text11);
      $text13 = preg_replace('/\b(gender)\b/u',$gender, $text12);
      $text14 = preg_replace('/\b(Gender)\b/u',$gender1, $text13);
      $text15 = preg_replace('/\b(his-her)\b/u',$pronoun, $text14);
      $text16 = preg_replace('/\b(him-her)\b/u', $pronoun1, $text15);

      $text17 = str_replace('(', '', $text16);
      $Finaltext = str_replace(')', '', $text17);
		echo   $Finaltext; 
        ?>
      </textarea>

      <script>

       CKEDITOR.replace( 'staff_rejectemail_message_<?php echo $staff->id;?>' );

     </script>

    </div>

  </div>

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingrejectmsg-<?php echo $staff->id;?>"></div>

        <input class="sendrejectemail" id="save_reject-<?php echo $staff->id;?>" value="Send" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
     </div>

    </div>

  </div>                
                        
    
  <!-- Generate Pop-up  for sending Email for Schedule of staff interview--> 
  
	<div class="modal fade" id="myModal_schedule_<?php echo $staff->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Schedule Interview Email</h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

      <div style="clear:both"></div>  

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>staffenrolement/user_schedule_email" name="scheduleemailForm" id="scheduleemailForm_<?php echo $staff->id;?>" method="post">

   		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="school_id" id="school_id" value="<?php echo $staff->school_id;?>" />

		 <input type="hidden" name="user_id" id="user_id" value="<?php echo $staff->id;?>" />
         
         <input type="hidden" name="staff_email" id="staff_schedule_email_<?php echo $staff->id;?>" value="<?php echo $staff->email;?>" class="form-control"  placeholder="" maxlength="121">

         <input type="hidden" name="staff_email_subject" id="staff_schedule_email_subject_<?php echo $staff->id;?>" value="<?php echo $scheduleInfo->staff_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="staff_email_message" id="staff_schedule_email_message_<?php echo $staff->id;?>"> 

       <?php /*?> <?php echo $staff->staff_fname.' '. $staff->staff_lname;?> <?php echo $selectedInfo->staff_email_message;?><?php */?>
		<?php
		  if($staff->created_date=='0000-00-00') {
			 $staff_enrolment_date = date('d-m-Y');
		   } else {
			 $staff_enrolment_date = date('d-m-Y', strtotime($staff->created_date));
		    }
$description = $scheduleInfo->staff_email_message;

 if($staff->staff_title == 'Mrs' || $staff->staff_title == 'Ms' ){

  if (strpos($description,'(his-her)') > 0) {
      $pronoun = 'her';
    }else {
      $pronoun='Her';
    }

  if (strpos($description,'(gender)') > 0 ) {

    $var= strpos($description,'(gender)');

      $gender='she';
    }else {
      $gender='She';
    }
  if (strpos($description,'(Gender)') > 0 ) {

      $gender1='she';
    }else {
      $gender1='She';
    }
  $pronoun1 = 'her';
   
   }else{

    if (strpos($description,'(his-her)') > 0) {
       $pronoun = 'his';
    }else {
       $pronoun = 'His';
    }
    if (strpos($description,'(gender)') > 0) {
      $gender='he';
    }else {
      $gender='He';
    }
    if (strpos($description,'(Gender)') > 0 ) {

      $gender1='he';
    }else {
      $gender1='He';
    }
    $pronoun1 = 'him';

   }

      $text1 = preg_replace('/\b(name of staff)\b/u', $staff->staff_fname.' '. $staff->staff_lname, $scheduleInfo->staff_email_message);
      $text2 = preg_replace('/\b(school name)\b/u', $school->school_name, $text1);
      $text3 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $text2);
      $text4 = preg_replace('/\b(date of enrolment)\b/u', $staff_enrolment_date, $text3);
      $text5 = preg_replace('/\b(branch contact number)\b/u', $school->school_phone, $text4);
      $text6 = preg_replace('/\b(qualification)\b/u',$staff->staff_education_qualification, $text5);
      $text7 = preg_replace('/\b(date of birth)\b/u',$staff->staff_dob, $text4);
      $text8 = preg_replace('/\b(email id)\b/u',$staff->email, $text7);
      $text9 = preg_replace('/\b(teaching days)\b/u',$staff->teaching_days, $text8);
      $text10 = preg_replace('/\b(salary)\b/u',$staff->salary_currency.' '.$staff->salary_amount.' '.$staff->salary_mode, $text9);
      $text11 = preg_replace('/\b(username)\b/u',$staff->username, $text10);
      $text12 = preg_replace('/\b(password)\b/u',$staff->password, $text11);
      $text13 = preg_replace('/\b(gender)\b/u',$gender, $text12);
      $text14 = preg_replace('/\b(Gender)\b/u',$gender1, $text13);
      $text15 = preg_replace('/\b(his-her)\b/u',$pronoun, $text14);
      $text16 = preg_replace('/\b(him-her)\b/u', $pronoun1, $text15);

      $text17 = str_replace('(', '', $text16);
      $Finaltext = str_replace(')', '', $text17);
		echo   $Finaltext; 
        ?>

      </textarea>

      <script>

       CKEDITOR.replace( 'staff_schedule_email_message_<?php echo $staff->id;?>' );

     </script>

    </div>

  </div>


  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingschedulemsg-<?php echo $staff->id;?>"></div>

        <input class="sendscheduleemail" id="save_schedule-<?php echo $staff->id;?>" value="Send" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>

    </div>

  </div>

   <!-- Generate Pop-up  for sending Email for Re-Schedule of staff interview--> 
    
    <div class="modal fade" id="myModal_reschedule_<?php echo $staff->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Reschedule Interview Email</h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

  
       <div class="modal-body">

     <form action="<?php echo base_url(); ?>staffenrolement/user_schedule_email" name="rescheduleemailForm" id="rescheduleemailForm_<?php echo $staff->id;?>" method="post">

        
   		<input type="hidden" name="tab_value" id="tab_value" value="waiting" />
        
        <input type="hidden" name="school_id" id="school_id" value="<?php echo $staff->school_id;?>" />

		 <input type="hidden" name="user_id" id="user_id" value="<?php echo $staff->id;?>" />

         <input type="hidden" name="staff_reschedule_email" id="staff_reschedule_email_<?php echo $staff->id;?>" value="<?php echo $staff->email;?>" class="form-control"  placeholder="" maxlength="121">

         <input type="hidden" name="staff_reschedule_email_subject" id="staff_reschedule_email_subject_<?php echo $staff->id;?>" value="<?php echo $rescheduleInfo->staff_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          <div class="form-group">

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="staff_reschedule_email_message" id="staff_reschedule_email_message_<?php echo $staff->id;?>"> 

       <?php /*?> <?php echo $staff->staff_fname.' '. $staff->staff_lname;?> <?php echo $selectedInfo->staff_email_message;?><?php */?>
		<?php
		  if($staff->created_date=='0000-00-00') {
			 $staff_enrolment_date = date('d-m-Y');
		   } else {
			 $staff_enrolment_date = date('d-m-Y', strtotime($staff->created_date));
		    }

      $description = $rescheduleInfo->staff_email_message;

 if($staff->staff_title == 'Mrs' || $staff->staff_title == 'Ms' ){

  if (strpos($description,'(his-her)') > 0) {
      $pronoun = 'her';
    }else {
      $pronoun='Her';
    }

  if (strpos($description,'(gender)') > 0 ) {

    $var= strpos($description,'(gender)');

      $gender='she';
    }else {
      $gender='She';
    }
  if (strpos($description,'(Gender)') > 0 ) {

      $gender1='she';
    }else {
      $gender1='She';
    }
  $pronoun1 = 'her';
   
   }else{

    if (strpos($description,'(his-her)') > 0) {
       $pronoun = 'his';
    }else {
       $pronoun = 'His';
    }
    if (strpos($description,'(gender)') > 0) {
      $gender='he';
    }else {
      $gender='He';
    }
    if (strpos($description,'(Gender)') > 0 ) {

      $gender1='he';
    }else {
      $gender1='He';
    }
    $pronoun1 = 'him';

   }
   
      $text1 = preg_replace('/\b(name of staff)\b/u', $staff->staff_fname.' '. $staff->staff_lname, $rescheduleInfo->staff_email_message);
      $text2 = preg_replace('/\b(school name)\b/u', $school->school_name, $text1);
      $text3 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $text2);
      $text4 = preg_replace('/\b(date of enrolment)\b/u', $staff_enrolment_date, $text3);
      $text5 = preg_replace('/\b(branch contact number)\b/u', $school->school_phone, $text4);
      $text6 = preg_replace('/\b(qualification)\b/u',$staff->staff_education_qualification, $text5);
      $text7 = preg_replace('/\b(date of birth)\b/u',$staff->staff_dob, $text4);
      $text8 = preg_replace('/\b(email id)\b/u',$staff->email, $text7);
      $text9 = preg_replace('/\b(teaching days)\b/u',$staff->teaching_days, $text8);
      $text10 = preg_replace('/\b(salary)\b/u',$staff->salary_currency.' '.$staff->salary_amount.' '.$staff->salary_mode, $text9);
      $text11 = preg_replace('/\b(username)\b/u',$staff->username, $text10);
      $text12 = preg_replace('/\b(password)\b/u',$staff->password, $text11);
      $text13 = preg_replace('/\b(gender)\b/u',$gender, $text12);
      $text14 = preg_replace('/\b(Gender)\b/u',$gender1, $text13);
      $text15 = preg_replace('/\b(his-her)\b/u',$pronoun, $text14);
      $text16 = preg_replace('/\b(him-her)\b/u', $pronoun1, $text15);

      $text17 = str_replace('(', '', $text16);
      $Finaltext = str_replace(')', '', $text17);
		echo   $Finaltext; 
        ?>

      </textarea>

      <script>

       CKEDITOR.replace( 'staff_reschedule_email_message_<?php echo $staff->id;?>' );

     </script>

    </div>

  </div>


  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingreschedulemsg-<?php echo $staff->id;?>"></div>

        <input class="sendrescheduleemail" id="save_reschedule-<?php echo $staff->id;?>" value="Send" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>

    </div>

  </div>
  
  
   <!-- Generate Pop-up  for Notes on WAITING staff-->
   
    
	<div class="modal fade" id="myModal_notes_<?php echo $staff->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-11 nopadding invitemember">

          <div class="col-sm-9"><h3 class="modal-title inviteheader">Notes for <?php echo $staff->staff_fname;?></h3></div>

           <div class="col-sm-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

  <div style="clear:both"></div>  

    <div class="modal-body">

     <form action="<?php echo base_url();?>staffenrolement/notesfor_user_enrolement" name="enrolementnotesForm" id="enrolementnotesForm_<?php echo $staff->id;?>" method='post'>
	 
		<input type="hidden" name="tab_value"  id="tab_value"  value="waiting" />

         <input type="hidden" name="school_id" id="school_id" value="<?php echo $staff->school_id;?>" />

           <input type="hidden" name="user_id" id="user_id" value="<?php echo $staff->id;?>" />

          <div class="col-sm-12">

          <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Notes:</label>

    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor1" name="staff_notes" id="staff_notes_<?php echo $staff->id;?>"> 
		<?php echo $staff->staff_notes ; ?>
      </textarea>

      <script>

	 CKEDITOR.replace( 'staff_notes_<?php echo $staff->id;?>' );

     </script>

    </div>

  </div>


  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingnotesmsg-<?php echo $staff->id;?>"></div>

        <input class="savenotes" id="notes" value="Save" type="submit">

        </div>

        </div>

             </div>  

          </form>

        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>

    </div>

  </div> 
                        
					<?php 
						  }
							}
							 else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px;height: 100px;font-size:25px ; background:#FFF;  color: #6a7a91;">No record to show.</th></tr>	
				   <?php } ?>
				</tbody>
		  </table>
     <div class="box-footer clearfix profile-bg">
          	
          <div class="col-sm-8"  style="margin:20px 0px;">
			<div class="col-sm-4 paginationblk">
			<?php
							if(count($data_rows)>0){
								$last_page1=$last_page;
								?>
								<span class="pag_txt">Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries.</span>
								<?php
							}
							?>
                            </div>
		<div class="col-sm-8 paginationblk2">	    
        <ul class="pagination">
		<?php echo $pagination;?>
	     </ul>
         </div>
        </div>
                 <div class="col-sm-3 col-xs-6 totaldiv nopadding">           
    		<div class="col-sm-12 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>
    <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>staffenrolement/waitingstaffs/0/" method="get">
    
      <input type="hidden" name="branchidsrch" id="branchidsrch" value="<?php echo @$branch_data; ?>"  />    
   
      <input type="hidden" name="sdate" id="sdate" value="<?php echo $sdate_search; ?>"  />  
    
      <input type="hidden" name="edate" id="edate" value="<?php echo $edate_search; ?>"  /> 
      
       <select class="form-control" name="perpage" id="perpage">

          <option >Select</option>
          
           <!--<option <?php//if($per_page == "10"){ echo 'selected'; } ?> value="10">10</option>-->
           
          <option <?php if($per_page == "20"){ echo 'selected'; } ?> value="20">20</option>
        
          <option <?php if($per_page == "40"){ echo 'selected'; } ?> value="40">40</option>
        
          <option <?php if($per_page == "50"){ echo 'selected'; } ?> value="50">50</option>
        
          <option <?php if($per_page == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>
 </form>
      </div>
					</div>
    <div class="col-sm-12 exceldiv">
    <?php  if(count($data_rows) > 0){ ?>
                       
         <form method="post"  name="export_form" action="<?php echo base_url();?>staffenrolement/excel_action">
    
          <input type="hidden" name="exceltab" id="exceltab" value="waiting"  /> 
                         
          <input type="hidden" name="branch_data" id="branch_data" value="<?php echo @$branch_data; ?>"  />
                       
          <input type="hidden" name="sdate_search" id="sdate_search" value="<?php echo $sdate_search; ?>"  />  
                        
          <input type="hidden" name="edate_search" id="edate_search" value="<?php echo $edate_search; ?>"  /> 
    
    			<input type="submit" name="export" class="btn btn-success excelbtn" value="Export to Excel" />
   
    		 </form>
		<?php } ?>
    </div>
</div>
          
</div>


	</div>
    
	</div>
    </div>
    
     
<script type="text/javascript"> 
	jQuery("#perpage").on('change', function(e){ 
		jQuery('#perPageForm').submit();
}); 
</script>
 <script type="text/javascript">

	function delConfirm(id){

		if( confirm("Are you sure want to Delete?") ){

			window.location.href ="<?php echo base_url();?>staffenrolement/deletewaiting/"+id;

		}else{

			return false;

		}

	}

</script>
<script>

setTimeout(function() {
    $('.alert').fadeOut('fast');
}, 2000);

</script>
<script>
jQuery(document).ready(function(){

	    $("#sdate").keydown(false);

	    $("#edate").keydown(false);

		

  	 jQuery("#staffSearchForm").validate({

        rules: {

			 sdate: {

    					required: true

   					},

			  edate: {

    					required: true

   					}

            },

        messages: {

			  sdate: {

    				required: "Please select start date."

   				 },

			  edate: {

    				required: "Please select end date."

   				 }

             },

        submitHandler: function(form) {

            form.submit();

          }

        });

 });

</script>

<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}

.action-btn a {
	background:none;
	border:none;
}
.paginationblk2 .pagination {
    margin: 0;
}
.paginationblk2 {
    text-align: center;
}
</style>
<style>

#myModal { margin-top:140px; } 

.save_btn > input {

    background: #37b248 none repeat scroll 0 0;

    border: 1px solid #ccc;

    border-radius: 3px;

    color: #fff;

    font-size: 14px;

    padding: 7px 20px;

}

.save_btn {

    padding-top: 20px;

}

.popup_save_btn {

    background: #37b248 none repeat scroll 0 0;

    border: medium none;

    border-bottom-right-radius: 3px;

    border-top-right-radius: 3px;

    color: #fff;

    padding: 12px 25px;

}

.modal-body {

    padding: 35px 0 70px;

}

.text_box_sec > input {

    border: 1px solid #ccc;

    border-radius: 3px;

    min-height: 45px;

    width: 100%;

	padding: 10px;

}

.btn.btn-default {

    background: #37b248 none repeat scroll 0 0;

    color: #fff;

}

.text_box_sec > input {

    border: 1px solid #ccc;

    border-bottom-left-radius: 3px;

    border-top-left-radius: 3px;

    min-height: 44px;

    padding: 10px;

    width: 100%;

}

.save_btn {

    text-align: right;

}

.text_box_btn {

    text-align: left;

}

.modal-header {

    text-align: center;

}



.red_line { color:#F00 !important; }

.yellow_line { color:#990 !important; }

.blank_trans_data {

    color: transparent;

}

.search {

    padding: 10px 0 60px !important;

}

.invitemember {

    background: #37b248 none repeat scroll 0 0;

    color: #fff;

    padding-bottom: 5px;

    padding-top: 5px;

}


.close.closenotify {
    background: #fff none repeat scroll 0 0;
    border: 2px solid #37b248;
    border-radius: 40px;
    color: #36b047;
    float: right;
    font-size: 28px;
    height: 40px;
    left: 30px;
    margin-top: -23px;
    opacity: 1;
    position: relative;
    width: 40px;
}
.modal-content.enrolmentpopup{float:left;width:100%;}
.confirmlink .sendenrolementemail {
    border: 0 none;
    font-weight: bold;
    padding: 10px 30px;
    text-transform: uppercase;
}
.enrolmentpopup .modal-body{float:left;width:100%;}
.savepostponedate {
    background: #37b248 none repeat scroll 0 0;
    border: 0 none;
    color: #fff;
    padding: 12px 30px;
}
.modal-dialog.popupdiv{width:90%;}
.cke_contents.cke_reset {
    height: 350px !important;
}
.enrolmentpopup .modal-body {
    float: left;
    padding: 5px 0 0;
    width: 100%;
}
.confirmlink input{margin:6px 0px;}
.popup_section_form {
    padding: 30px 10px;
}
.inviteheader{text-align:left;}
.exceldiv .excelbtn{
	margin-bottom: 15px;
}
</style>