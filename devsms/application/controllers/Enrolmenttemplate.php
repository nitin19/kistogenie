<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Enrolmenttemplate extends CI_Controller {
	
	var $logmode;
	function __construct(){
       
      parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','Enrolmenttemplate_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }

	

	public function index(){

		
		$data = array();
		$user_id						= $this->session->userdata('user_id');
		$status_search					= isset($_GET['status'])?$_GET['status']:NULL; 
		$word_search				    = isset($_GET['seachword'])?trim($_GET['seachword']):NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		$email_user_type		   		= $this->input->post('user_type');

		$search_condition['is_deleted']	= '0';
		
		$school_id						= $this->session->userdata('user_school_id');
		
		$data['title']	      			= "Enrolment Template";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage					= $PerPage;
		} else {
			$perpage					= 10;
		}
	
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'enrolmenttemplate/index';

		/**************************** records depends on user type *******************/

		if($email_user_type == 'staff_enrolment'){

			$row_count 					= $this->Enrolmenttemplate_model->getstaffRows($search_condition,$school_id,$word_search);

		}else{

			$row_count 					= $this->Enrolmenttemplate_model->getRows($search_condition,$school_id,$word_search);
		}


	    $config['total_rows'] 			= $row_count;

	   	$config['per_page'] 			= $perpage;
        $config['postfix_string'] 		= "/?status=$status_search&seachword=$word_search&perpage=$perpage"; 
		
		$this->pagination->initialize($config);

		$odr 							= "id";
		$dirc							= "desc";

		/**************************** records depends on user type *******************/

		if($email_user_type == 'staff_enrolment'){

			$data_rows	= $this->Enrolmenttemplate_model->getstaffPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$word_search);

		}else{

			$email_user_type = 'student_enrolment' ;

			$data_rows	= $this->Enrolmenttemplate_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$word_search);
		}
	
		$messageid 						= $this->uri->segment(4);
		$emailusertype					= $this->uri->segment(6);

		if($messageid!='') {
		  $template_data_rows			= $this->Enrolmenttemplate_model->get_single_template($messageid , $emailusertype);
		  $data['info']				    = $template_data_rows[0];
		 }else{

		  $template_data_rows			= $this->Enrolmenttemplate_model->get_first_template();
		  $data['info']				    = $template_data_rows;	
		 }

		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;


		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['status_search']			= $status_search;
		$data['word_search']		    = $word_search;
		$data['PerPage']				= $PerPage;
		$data['email_user_type']		= $email_user_type;

		$data['PerPage']				= $PerPage;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');


		$this->load->view('header');
		$this->load->view('master/enrolment_template',$data);
		$this->load->view('footer');
	}

public function updatetemplate(){
	
			
			  $start						    = $this->uri->segment(3);
		      $editedTemplateid					= $this->uri->segment(4);
			  $Action							= $this->uri->segment(5);  
			  $email_user_type					= $this->input->post('email_user');

	
		  $postURL	= $this->createPostURL($_GET);
		if($editedTemplateid==NULL){
			$this->session->set_flashdata('error', 'Select the Template first.');
			redirect(base_url()."enrolmenttemplate");exit;
		   }
		   
		 $msgdata 					= array();
         $school_id 					= $this->session->userdata('user_school_id');
		 $where							= array('id'=>$editedTemplateid);
		 $data['title']	      			= "Edit Template";
		 $data['mode']					= "Edit";
		
		 if($email_user_type == 'student_enrolment'){


			if (trim($this->input->post('msgtitle')) != '') {
                $msgdata['student_email_subject'] = trim($this->input->post('msgtitle'));
				
            }
            
			if (trim($this->input->post('msgdesc')) != '') {
                $msgdata['student_email_message'] = trim($this->input->post('msgdesc'));
            }
    
	     
		}else{

		if (trim($this->input->post('msgtitle')) != '') {
                $msgdata['staff_email_subject'] = trim($this->input->post('msgtitle'));
				
            }
            
			if (trim($this->input->post('msgdesc')) != '') {
                $msgdata['staff_email_message'] = trim($this->input->post('msgdesc'));
            }


		}
			$msgdata['updated_date'] = date('Y-m-d');
			
		   if($this->Enrolmenttemplate_model->updateTemplates($email_user_type,$msgdata,$where)) {
			   $this->session->set_flashdata('success', 'Message has been updated successfully.');
			   redirect(base_url()."enrolmenttemplate/index/$start/$editedTemplateid/edit/$email_user_type"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Message has not been updated.');
				redirect(base_url()."enrolmenttemplate/index/$start/$editedTemplateid/edit/$email_user_type");exit;
		      }
	   }
	 


	
		function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}
}

