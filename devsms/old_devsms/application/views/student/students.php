<?php   
$user_type		= $this->session->userdata('user_type');
?>
<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		<li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>

        <li class="edit"><a href="<?php echo base_url(); ?>students">Students</a></li>        

        </ul>

        </div>

        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

       <!-- <input type="button" class="activequickaction" value="Quick actions">-->

        </div>

        </div>

         <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>

        <div class="attendancesec">	

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

  <form action="<?php echo base_url();?>students/index/0/" name="studentSearchForm" id="studentSearchForm">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding">

				<div class="col-sm-8 nopadding selectoption">

<div class="form-group fullwidthinput">

    <label class="col-sm-1 col-xs-1 control-label nopadding filterlabel">Show:</label>

      <div class="col-sm-11 col-xs-11 nopadding selectfilter">

    <div class="col-sm-3 col-xs-3 inputbox termselect">
    

      <select class="form-control" name="branch" id="branch">
  <option value="">Select Branch</option>    
  <?php
   $staff_branch_id = $this->session->userdata('staff_branch_id');
  
     if($user_type=='teacher' && $staff_branch_id!=''){
      $staff_branch = explode(',',$staff_branch_id); 
	  foreach($staff_branch as $staff_branch_ids){
		   $this->load->model(array('student_model'));
	 $branchdta =$this->student_model->getstaff_branches($staff_branch_ids);
	  ?>
	 <option <?php if($branch_search == $branchdta->branch_id){ echo 'selected'; } ?> value="<?php echo $branchdta->branch_id; ?>"><?php echo $branchdta->branch_name;?></option>
	 <?php  }  } 
	 else if($user_type=='admin'){
	 
	 	foreach($branches as $branch){
	 ?>
     	  <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
		<?php  
		   
   }}
   else{
   	 ?>
     <option value=""></option>
     <?php } ?>
</select>

    </div>

    <div class="col-sm-3 col-xs-3 inputbox termselect">
<?php //$getbranch = $_GET['branch']; ?>
      <select class="form-control" name="class" id="class">
  <option value="">Select Class</option>    
<?php
   if( $_GET['branch']!='' ) {
	  
		  $school_id		= $this->session->userdata('user_school_id');
	   $schoolbranch 	= $_GET['branch'];
		  $this->load->model(array('student_model'));
	      $stclasses 		= $this->student_model->getclasses($school_id,$schoolbranch);

		  foreach($stclasses as $sclass) { ?>
          <option <?php if($class_search == $sclass->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>
</select>

    </div>

    <div class="col-sm-2 col-xs-2 inputbox termselect">

<select class="form-control" name="status" id="status">
  <option value="">Select Status</option>    
   <option <?php if($status_search == "1"){ echo 'selected'; } ?> value="1">Active</option>
   <option <?php if($status_search == "0"){ echo 'selected'; } ?> value="0">Inactive</option>
</select>

    </div>

    <div class="col-sm-2 col-xs-2 inputbox termselect">

      <select class="form-control" name="shortbyname" id="shortbyname">
   <option value="">Sort By</option>    
   <option  <?php if($shortbyname_search == "0"){ echo 'selected'; } ?> value="0">Select Name A-Z</option>    
   <option  <?php if($shortbyname_search == "1"){ echo 'selected'; } ?> value="1">Select Name Z-A</option>    
</select>

    </div>
    
    <div class="col-sm-2 col-xs-2 inputbox termselect">

      <select class="form-control" name="sby" id="sby">
            <option <?php if($sby == "name"){ echo 'selected'; } ?> value="name">Name</option>    
            <option <?php if($sby == "email"){ echo 'selected'; } ?> value="email">Email</option>  
            <option <?php if($sby == "phone"){ echo 'selected'; } ?> value="phone">Phone</option> 
            <option <?php if($sby == "address"){ echo 'selected'; } ?> value="address">Address</option>    
            <option <?php if($sby == "fname"){ echo 'selected'; } ?> value="fname">Father name</option>
            <option <?php if($sby == "mname"){ echo 'selected'; } ?> value="mname">Mother name</option>
            <option <?php if($sby == "femail"){ echo 'selected'; } ?> value="femail">Father email</option>
            <option <?php if($sby == "memail"){ echo 'selected'; } ?> value="memail">Mother email</option>
	 </select>

    </div>

    </div>

  </div>

</div>		

                <div class="col-sm-2 col-xs-5 searching termsearch nopadding">

        <div class="form-group">

      <input type="text" class="form-control searchbox" placeholder="Search here" name="seachword" id="seachword" value="<?php echo $word_search;?>">

   <!--  <input type="submit" value="" class="searchbtn">-->

     </div>

        </div>

                <div class="col-sm-2 col-xs-2 rightspace viewreport">

<div class="form-group">

    <input type="submit" class="btn btn-danger" value="Find Students">

  <!--  <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>-->

  </div>

</div>

        </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <h1>Students<span><?php echo $Totalrec = $total_rows;?></span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">S.No.</th>

					  <th class="column6"> Name</th>

					  <!--<th class="column4"> Branch </th>-->
                        
					<!--  <th class="column3">Class/Group</th>-->

                      <th class="column6">Email</th>

                      <th class="column3">Phone</th>

                   <!-- <th class="column4">Application/Start Date</th>-->

                     <th class="column2">Status</th>
                     
                      <th class="column2">Action</th>

				  </tr>

			  </thead>

				<tbody>

					<?php 
					
						 if(count(array_filter($data_rows)) > 0){
					      $sr=$last_page;
						  
					  foreach($data_rows as $student) { 
					     $sr++;
						  $student_school_branch = $student->student_school_branch;
						  $student_class_group = $student->student_class_group;
						  $this->load->model(array('student_model'));
						  $branchName = $this->student_model->getbranchName($student_school_branch);
						  $className = $this->student_model->getclassName($student_class_group);
					?>
					<tr class="familydata students">
						<td class="column1"><?php echo $sr; ?></td>
						<td class="column6"><?php echo $student->student_fname.' '. $student->student_lname;?></td>
                       <!-- <td class="column4"><?php /*?><?php echo @$branchName->branch_name;?><?php */?> </td>-->
                        <!--<td class="column3"><?php /*?><?php echo $className->class_name;?><?php */?></td>-->
                        <td class="column6"><?php
						 if($student->email!='') {
						   echo $student->email; } else {
						   echo '<span style="color:transparent;">-</span>';
								    }				
						 ?></td>
						<td class="column3"><?php 
						 if($student->student_telephone!='') {
						   echo $student->student_telephone; } else {
						   echo '<span style="color:transparent;">-</span>';
								    }
						
						?></td>
                     <!-- <td class="column4"><?php //echo date('d/m/Y', strtotime($student->student_application_date)).' - '.date('d/m/Y', strtotime($student->student_leaving_date)); ?></td>-->
                        <td class="column2"><span class="acceptbtn statusbtn <?php if($student->is_active==1) { echo 'Accepted'; } else { echo 'Suspended'; } ?>"><?php if($student->is_active==1) { echo 'Accepted'; } else { echo 'Suspended'; } ?></span></td>
                        
                        
                        <td class="column2">
											<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
                                                
           <li> <a href="<?php echo base_url();?>students/view_student/<?php echo $student->id;?>" target="_blank"> View  </a> </li>                                    
	       <li><a href="<?=base_url();?>students/edit/<?php echo $student->id;?>" target="_blank"> Edit </a>  </li>
          
				<li>
				<?php if($student->is_active == '1'){ ?>
	<a href="<?php echo base_url();?>students/deactivate/<?php echo $student->id;?>" title="click to deactivate student"> Active </a>
					<?php
					 } else {
					?>
	<a href="<?=base_url();?>students/activate/<?php echo $student->id;?>" title="click to activate student">De-Activated </a>
				<?php } ?>
				</li>
                
    <li><a href="<?=base_url();?>modifyprogressreport/index/<?php echo $student->student_id;?>"> Progress Report </a>  </li>
    <li><a href="<?=base_url();?>studentsrelationperson/index/<?php echo $student->student_id;?>"> Relation Person  </a>  </li>
    
			<li class="divider"></li>
			 <li><a onclick="delConfirm(<?php echo $student->id;?>)" style="cursor: pointer;"> Delete </a> </li>
									</ul>
								</div>
											
							</td>
                                        
                                        
                        </tr>
				<?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px;height:100px;font-size:25px; background:#FFF;  color: #6a7a91;">No record to show.</th></tr>	
				   <?php } ?>

				</tbody>

		  </table>

     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-8 col-xs-6 paginationblk">
         <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries.
			<?php  } ?>
	<ul class="pagination">
		
		<?php echo $pagination;?>
        

	</ul>

    </div>

    <div class="col-sm-4 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total Students: <?php echo $Totalrec ;?></h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>students/index/0/" method="get">

     <input type="hidden" name="branch" id="branch_search" value="<?php echo $branch_search; ?>"  />
    
    <input type="hidden" name="class" id="class_search" value="<?php echo $class_search; ?>"  />
    
    <input type="hidden" name="status" id="status_search" value="<?php echo $status_search; ?>"  />
    
    <input type="hidden" name="shortbyname" id="shortbyname_search" value="<?php echo $shortbyname_search; ?>"  />
    
    <input type="hidden" name="searchword" id="word_search" value="<?php echo $word_search; ?>"  />
    
    <input type="hidden" name="sby" id="sby" value="<?php echo $sby; ?>"  />  
 
  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>

  <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>
        <div class="col-sm-12 exceldiv">
        
     <form method="post"  name="export_form" action="<?php echo base_url();?>students/excel_action">
    
    <input type="hidden" name="branch_search" id="branch_search" value="<?php echo $branch_search; ?>"  />
    
    <input type="hidden" name="class_search" id="class_search" value="<?php echo $class_search; ?>"  />
    
    <input type="hidden" name="status_search" id="status_search" value="<?php echo $status_search; ?>"  />
    
    <input type="hidden" name="shortbyname_search" id="shortbyname_search" value="<?php echo $shortbyname_search; ?>"  />
    
    <input type="hidden" name="word_search" id="word_search" value="<?php echo $word_search; ?>"  />
    
    <input type="hidden" name="sby" id="sby" value="<?php echo $sby; ?>"  />   
    
    <input type="hidden" name="PerPage" id="PerPage" value="<?php echo $PerPage; ?>"  />  
    
    <input type="hidden" name="last_page" id="last_page" value="<?php echo $last_page; ?>"  />      
   
      <input type="submit" name="export" class="btn btn-success excelbtn" value="Export to Excel" />
    
    </form>
        
        
        </div>
        

    </div>

	</div>

    </div>     

</div>

	</div>

    

	</div>

    </div>

    </div>
    
        
 <script type="text/javascript">
  
  jQuery(document).ready(function(){
	  
			  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
					
	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#studentSearchForm")[0].reset();
			   window.location.href='<?php echo base_url()?>students';
			});   
			
	 jQuery("#branch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			// var BranchID =  jQuery('input[name=student_school_branch]:checked').val();
			jQuery("#class").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>students/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#class').append(resp);
			          }
				   });
				 } else {
				 jQuery("#class").find('option[value!=""]').remove();
				 return false;
				 }
			 });	
			
		}); 
		
 </script>	
 
  
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>students/delete/"+id;
		}else{
			return false;
		}
	}
</script>	

<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.col-sm-8.col-xs-6.paginationblk {
    padding: 25px;
}
.Suspended {background: #ff0000 none repeat scroll 0 0;}


.pagination{
	margin-left:50px;
}
#perpage{
	margin-right:35px;
}
@media screen and (min-width:320px) and (max-width:480px){
.termsearch {
    padding: 0 4px 10px;
    width: 100% !important;
}
.rightspace.viewreport {
    width: 100%;
}
.cleanSearchFilter {
    padding: 9px 5px 8px;
}
}
@media screen and (min-width:481px) and (max-width:767px){
#studentSearchForm .selectfilter select {
    font-size: 11px;
}
#studentSearchForm .selectoption {
    padding-bottom: 15px;
}
.col-sm-8.col-xs-6.paginationblk {
    font-size: 11px;
}
.totaldiv h3 {
    font-size: 11px;
}
.selectfilter > span {
    font-size: 11px;
}
}
@media screen and (min-width:768px) and (max-width:991px){
#studentSearchForm .selectfilter {
    padding-bottom: 10px;
}
.cleanSearchFilter {
    padding: 9px 5px 8px !important;
}
.btn {
    padding: 6px 5px !important;
}
#studentSearchForm .selectfilter select {
    font-size: 11px !important;
}
}
@media screen and (min-width:992px) and (max-width:1200px){
.applycodediv .form-group input {
    font-size: 11px;
}
.cleanSearchFilter {
    padding: 9px 5px 8px !important;
}
.btn {
    padding: 6px 4px !important;
}
#studentSearchForm .selectfilter .inputbox.termselect select {
    font-size: 11px;
}
.statusbtn {
    padding: 6px 5px;
}
}
</style>	