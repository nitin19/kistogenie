<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Staffenrolementreport_model extends CI_Model {
	var $staff_enrolement			= "staff_enrolement";
	var $staff  		= "staff";
	var $users 			= "users";
	var $branch			= "branch";
	var $school			="school";
	var $classes 		= "classes";
	var $subjects 		= "subjects";
	var $teacher_role 	= "teacher_role";
	var $teacher_level 	= "teacher_level";
	var $teacher_type 	= "teacher_type";
	var $currency		="currency";
	var $staff_access	= "staff_access";
	
	var $staff_enrolement_email_messages="staff_enrolement_email_messages"; 
	

    function __construct() {
        parent::__construct();
	}
		
	function getRows($where=NULL,$school_id,$startdate,$enddate){
	     $this->db->select('*');
         $this->db->from($this->tbl);
		 $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		 $this->db->where(array($this->tbl.'.school_id'=>$school_id));
	     if($where != NULL){
			$this->db->where($where);
		  }
		 if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->users.'.created_date >='=>$startdate));
			$this->db->where(array($this->users.'.created_date <='=>$enddate));
		  }
		 $query = $this->db->get();
		 $this->db->last_query(); 
		 return $query->num_rows();
	}	
	
	function getPagedData($where=NULL,$school_id,$limit=NULL, $offset=NULL,$startdate,$enddate){
		 $this->db->limit($limit, $offset);
		 $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->users.'.created_date >='=>$startdate));
			$this->db->where(array($this->users.'.created_date <='=>$enddate));
		  }
		   $query = $this->db->get();
		  $this->db->last_query(); 
           return  $query->result();
		}
		
		
	function total_branches($school_id,$limit=NULL, $offset=NULL,$branch_search,$startdate,$enddate){
		$this->db->select('*');
		$this->db->from($this->branch);
	    //$this->db->join('staff_enrolement', 'staff_enrolement.branch_id = '.$this->branch.'.branch_id','INNER');
		$this->db->where(array($this->branch.'.school_id'=>$school_id, "is_active"=>'1',"is_deleted"=>'0'));
		if($branch_search != NULL){
			$this->db->where(array($this->branch.'.branch_id'=>$branch_search));
		  }
		/*if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->staff_enrolement.'.staff_application_date >='=>$startdate));
			$this->db->where(array($this->staff_enrolement.'.staff_application_date <='=>$enddate));
		  }*/
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->result();
	}
	
	function get_branches($school_id,$branch_search,$startdate,$enddate){
		$this->db->select('*');
		$this->db->from($this->branch);
		//$this->db->join('staff_enrolement', 'staff_enrolement.branch_id = '.$this->branch.'.branch_id','INNER');
		$this->db->where(array($this->branch.'.school_id'=>$school_id, "is_active"=>'1',"is_deleted"=>'0'));
		if($branch_search != NULL){
			$this->db->where(array($this->branch.'.branch_id'=>$branch_search));
		  }
		 /*if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->staff_enrolement.'.staff_application_date >='=>$startdate));
			$this->db->where(array($this->staff_enrolement.'.staff_application_date <='=>$enddate));
		  }*/
		$query=$this->db->get();
	     $this->db->last_query(); 
		return $query->num_rows();
	
	}
	
		function getbranchName($branch_id){
		$this->db->select('*');
		$this->db->from($this->branch);
	    $this->db->where('branch_id',$branch_id);
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->result();
	}
	
	function get_all_branch($school_id){
		$this->db->select('*');
		$this->db->from($this->branch);
	    $this->db->where(array("school_id"=> $school_id, "is_active"=>'1',"is_deleted"=>'0'));
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->result();
	}
	
	function get_applied($school_id,$branch_id,$startdate,$enddate){
		
		$this->db->select ('*');
		
		$this->db->from($this->staff_enrolement);
		
	    $this->db->where(array("school_id"=> $school_id, "branch_id"=>$branch_id,"staff_enrolement_status"=>'applied'));
		
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->staff_enrolement.'.staff_application_date >='=>$startdate));
			$this->db->where(array($this->staff_enrolement.'.staff_application_date <='=>$enddate));
		  }
		$query = $this->db->get();
	    //echo  $this->db->last_query(); exit();
		return $query->num_rows();
	
	}
	
	
	function get_selected($school_id,$branch_id,$startdate,$enddate){
		$this->db->select ('*');
		$this->db->from($this->staff_enrolement);
	    $this->db->where(array("school_id"=> $school_id, "branch_id"=>$branch_id,"staff_enrolement_status"=>'selected'));
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->staff_enrolement.'.staff_application_date >='=>$startdate));
			$this->db->where(array($this->staff_enrolement.'.staff_application_date <='=>$enddate));
		  }
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->num_rows();
	
	}
	function get_rejected($school_id,$branch_id,$startdate,$enddate){
		$this->db->select ('*');
		$this->db->from($this->staff_enrolement);
	    $this->db->where(array("school_id"=> $school_id, "branch_id"=>$branch_id,"staff_enrolement_status"=>'rejected'));
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->staff_enrolement.'.staff_application_date >='=>$startdate));
			$this->db->where(array($this->staff_enrolement.'.staff_application_date <='=>$enddate));
		  }
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->num_rows();
	
	}

function get_waiting($school_id,$branch_id,$startdate,$enddate){
		$this->db->select ('*');
		$this->db->from($this->staff_enrolement);
	    $this->db->where(array("school_id"=> $school_id, "branch_id"=>$branch_id,"staff_enrolement_status"=>'waiting'));
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->staff_enrolement.'.staff_application_date >='=>$startdate));
			$this->db->where(array($this->staff_enrolement.'.staff_application_date <='=>$enddate));
		  }
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->num_rows();
	
	}
	
/******************************************************************************************************************/


function getExcelData($school_id,$branch_search){
		
		$this->db->select('*');
		$this->db->from($this->branch);
	    
		$this->db->where(array($this->branch.'.school_id'=>$school_id, "is_active"=>'1',"is_deleted"=>'0'));
		if($branch_search != NULL){
			$this->db->where(array($this->branch.'.branch_id'=>$branch_search));
		  }

		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->result();
	}	
	
}
