
<div class="editprofile-content">

<div class="col-sm-12 profilemenus nopadding">



    <div class="col-sm-9 col-xs-12 nopadding menubaritems">



       <ul>



		<li><a href="#">Home</a></li>



        <li><a href="#">Attendance</a></li>



        <li class="edit"><a href="#">View Attendance</a></li>        



        </ul>



        </div>



        <!--<div class="col-sm-3 col-xs-4 actionbtn nopadding">



        <input type="button" value="Quick actions" class="activequickaction">



        </div>-->



        </div>

    	

        <div class="attendancesec">	



        <div class="col-sm-12 profile-bg filterbox generatefilter">



        <div class="filterdiv">


<form action="<?php echo base_url();?>viewattendance/index/" name="viewattendanceForm" id="viewattendanceForm">

    <input type="hidden" name="id" id="id" value="<?php echo @$studentid;?>">
 
    <div class="col-sm-12 col-xs-12 applycodediv">

        <div class="col-sm-4 nopadding tabfilterhalf">
                <div class="col-sm-2 col-xs-2 selectfilter datetext nopadding">
                <span>Date:</span>
                </div>
                <div class="col-sm-9 col-xs-10 nopadding datepickdiv">
                <div class="col-sm-5 col-xs-5 nopadding dateblk">
        		<div class="form-group">

    <div class="inputbox datepickerdiv nopadding">
      <input type="text" name="sdate" id="sdate" value="<?php echo @$sdate_search;?>" class="datepickerattendancereport1">
    </div>
  </div> 
  				</div>


<div class="col-sm-1 col-xs-1 dividerline nopadding"><span><img src="<?php echo base_url();?>assets/images/between.png"></span> </div>

                <div class="col-sm-5 col-xs-5 nopadding dateblk">
  				<div class="form-group">
    <div class="inputbox datepickerdiv nopadding">
      <input type="text" name="edate" id="edate" value="<?php echo @$edate_search;?>" class="datepickerattendancereport2">
    </div>
  </div>

  </div>

</div>

</div>

 <div class="col-sm-8 nopadding selectoption tabfilterfull mobfullfilter">

   <div class="col-sm-5 rightspace selectoption resselectfil">
<div class="form-group generatereport">
    <input type="submit" class="btn btn-danger" value="View Attendance">
     <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>
  </div>

</div>

		</div>
        
        

     </div>

        </form>



        </div>



    </div>



    <div class="col-sm-12 tablediv nopadding">

<?php  $Totalrec = $total_rows; ?>

    <div class="col-sm-12 nopadding">


    <h1>View Attendance</h1>



    <div class="tablewrapper">

    

    <div id="printTable">



<table class="table-bordered table-striped">



			  <thead>



				  <tr class="headings">



					  <th class="column1">No.</th>



					  <th class="column3">First Name</th>

					  <th class="column3">Last Name</th>

					  <th class="column5">Branch</th>

					  <th class="column3"> Class</th>

 					  <th class="column2">Date</th> 

                      <th class="column3"> Attendance status</th>


				  </tr>



			  </thead>



				<tbody>

				 <?php 
				 		$Astatus = '';
					  if(count(@$data_rows) > 0){
					      $sr=$last_page;
			 foreach($data_rows as $reports) { 
					     $sr++;
						 
						 if($reports->attendance_status==1) {
							 $Astatus = 'Present';
						 } elseif($reports->attendance_status==2) {
							  $Astatus = 'Absent';
						 } elseif($reports->attendance_status==3) {
							  $Astatus = 'Late';
						 }
			     ?>

					<tr class="familydata report">



						<td class="column1"><?php echo $sr; ?></td>

						<td class="column3"><?php echo $studentinfo->student_fname;?></td>

						<td class="column3"><?php echo $studentinfo->student_lname;?></td>

                         <td class="column5"><?php echo $branchName->branch_name;?> </td>

                         <td class="column3"><?php echo $className->class_name;?> </td>
                           
                         <td class="column2"><?php echo date('d M Y', strtotime($reports->attendance_date));?></td>

						 <td class="column3"><?php echo  $Astatus;?></td>



                        </tr>
                        
                   <?php }		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px; height:100px; background:#FFF; color: #6a7a91; font-size:25px">No record found.</th></tr>	
				   <?php } ?> 


				</tbody>



		  </table>
          
          
          <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-8 paginationblk">
    
    <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
			<?php  } ?>

	<ul class="pagination">
    
<?php echo $pagination;?>

	</ul>

    </div>

    <div class="col-sm-4 totaldiv nopadding">

    <div class="col-sm-8">

    <h3> Total Records:&nbsp;<?php echo $Totalrec ;?></h3>

    </div>


    <div class="col-sm-4 col-xs-5 selectfilter paginationselbox nopadding">

      <span>Showing:</span>

 <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>viewattendance/index">

<input type="hidden" name="id" id="id" value="<?php echo @$studentid;?>">
<input type="hidden" name="sdate" id="sdate" value="<?php echo @$sdate_search;?>" />
<input type="hidden" name="edate" id="edate" value="<?php echo @$edate_search;?>" /> 


  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>

  <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>
        
        

    </div>

	</div>

    </div>

          

     </div>     

          




       <!--<div class="cancelconfirm cancelconfirmdiv">



        <div class="col-sm-12 nopadding">

        <div class="confirmlink">
        
       <a href="#null" onclick="printContent('printTable')"> <input type="button" value="Print Report" class="btn btn-default"> </a>
        </div>



        </div>



        </div>-->        







</div>



	</div>



    



	</div>



    </div>



    </div>

    

    

    <script type="text/javascript">

<!--

function printContent(id){

str=document.getElementById(id).innerHTML

newwin=window.open('','printwin','left=100,top=100,width=400,height=400')

newwin.document.write('<HTML>\n<HEAD>\n')

newwin.document.write('<TITLE>Print Page</TITLE>\n')

newwin.document.write('<script>\n')

newwin.document.write('function chkstate(){\n')

newwin.document.write('if(document.readyState=="complete"){\n')

newwin.document.write('window.close()\n')

newwin.document.write('}\n')

newwin.document.write('else{\n')

newwin.document.write('setTimeout("chkstate()",2000)\n')

newwin.document.write('}\n')

newwin.document.write('}\n')

newwin.document.write('function print_win(){\n')

newwin.document.write('window.print();\n')

newwin.document.write('chkstate();\n')

newwin.document.write('}\n')

newwin.document.write('<\/script>\n')

newwin.document.write('</HEAD>\n')

newwin.document.write('<BODY onload="print_win()">\n')

newwin.document.write(str)

newwin.document.write('</BODY>\n')

newwin.document.write('</HTML>\n')

newwin.document.close()

}

//-->

</script>

<script>
  jQuery(document).ready(function(){
	  
	    jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
	  
	  	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#viewattendanceForm")[0].reset();
			   window.location.href='<?php echo base_url()?>viewattendance/index?id=<?php echo @$studentid;?>';
			}); 
			 
	 });
		 
</script>

<style>

 .beforeresult_section > h1 {

    min-height: 300px;

	}
 .paginationblk {
    padding: 25px;
}
.datetext{width:55px;}
</style>