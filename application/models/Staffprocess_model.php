<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Staffprocess_model extends CI_Model {
	var $tbl	= "staff_enrolement";
	var $staff  = "staff";
	var $users = "users";
	var $branch= "branch";
	var $school="school";
	var $classes = "classes";
	var $subjects = "subjects";
	var $teacher_role = "teacher_role";
	var $teacher_level = "teacher_level";
	var $teacher_type = "teacher_type";
	var $currency="currency";
	var $staff_access	= "staff_access";
	var $staff_enrolement_email_messages="staff_enrolement_email_messages"; 
	var $staff_education = "staff_education";
	var $staff_experience = "staff_experience";
	

    function __construct() {
        parent::__construct();
    }
	
		
	function getRows($where=NULL,$school_id,  $startdate, $enddate,$currentdate,$enrolementtype){
		
	     $this->db->select('*');
         
		 $this->db->from($this->tbl);
		 
		 $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		 
		 $this->db->where(array($this->tbl.'.school_id'=>$school_id));
		 
		 if($where != NULL){
			$this->db->where($where);
		  }
		 if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->users.'.created_date >='=>$startdate));
			$this->db->where(array($this->users.'.created_date <='=>$enddate));
		  }
		  
		  if($enrolementtype=='applied'){
		  
		  $this->db->where(array($this->tbl.'.staff_application_date <='=>$currentdate));
		  }
		  if($enrolementtype=='waiting'){
			$this->db->where(array($this->tbl.'.staff_postpone_date >='=>$currentdate));  
		  }
		  
		   $this->db->order_by($this->tbl.".staff_id", "desc"); 
		   
		 $query = $this->db->get();
		 
		 $this->db->last_query(); 
		 
		 return $query->num_rows();
	}
	
	function getPagedData($where=NULL,$school_id, $limit=NULL, $offset=NULL,  $startdate, $enddate,$currentdate,$enrolementtype){
		
		 $this->db->limit($limit, $offset);
		 
		 $this->db->select('*');
        
		 $this->db->from($this->tbl);
		
		 $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		
		 $this->db->where(array($this->tbl.'.school_id'=>$school_id));
		
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->users.'.created_date >='=>$startdate));
			$this->db->where(array($this->users.'.created_date <='=>$enddate));
		  }
		  
		  if($enrolementtype=='applied'){
		  
		  $this->db->where(array($this->tbl.'.staff_application_date <='=>$currentdate));
		  }
		  
		  if($enrolementtype=='waiting'){
			$this->db->where(array($this->tbl.'.staff_postpone_date >='=>$currentdate));  
		  }
		  
		   $this->db->order_by($this->tbl.".staff_id", "desc"); 
		  
		 $query = $this->db->get();
		 
		//echo $this->db->last_query();exit();
		  
         return  $query->result();
		}
	
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id"=> $branch_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		//echo $this->db->last_query();
        return  $query->row();
    }
		
	function getsingleClassName($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where('class_id', $class_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	
	 
	  function updatedata($user_id,$data){
		  
		$this->db->where('user_id', $user_id);
		
		$result = $this->db->update($this->tbl, $data);
		
		return $result;
		
}
	/*function update($where,$data){
		
		$this->db->where($where);
		
	if( $this->db->update($this->tbl, $data))
		{
		return true;
		}
		else
		{
			return false;
		}

	 }*/
	 
 	function delete($id){

		$where		= array('id' => $id);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->users, $data))
			return true;
		 else
			return false;
	}
	

    function updateUser($userdata,$userwhere){

		$this->db->where($userwhere);

		if($this->db->update($this->users, $userdata))

			return true;

		else

			return false;

		//echo $this->db->last_query();exit;

	 }

 function get_single_staff($user_id) {

          $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.user_id'=>$user_id));

          $query = $this->db->get();

          return  $query->row();

    }
	
	
 function insertStaff($data){

		if($this->db->insert($this->staff, $data))

			return $this->db->insert_id();

		  else

			return false;

		//echo $this->db->last_query();exit;

	 }
	 

	function get_selected_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->staff_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"staff_email_type="=>'selected',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }
	 
	function get_rejected_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->staff_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"staff_email_type="=>'rejected',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }
	
	function get_applied_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->staff_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"staff_email_type="=>'applied',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	

	function get_waiting_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->staff_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"staff_email_type="=>'waiting',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	

	function get_requestdetail_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->staff_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"staff_email_type="=>'requested details',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }
	
	function get_schedule_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->staff_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"staff_email_type="=>'schedule',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }
	
	function get_reschedule_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->staff_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"staff_email_type="=>'reschedule',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	function get_single_enrolstaff($sid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.user_id'=>$sid));
          $query = $this->db->get();
          return  $query->row();
    }
	
	function get_school_info($school_id) {
          $this->db->select('*');
          $this->db->from($this->school);
		  $this->db->where(array($this->school.'.school_id'=>$school_id));
          $query = $this->db->get();
          return  $query->row();
    }

	function get_branches($school_id) {
		$this->db->select('*');
		$this->db->from($this->branch);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
		$query = $this->db->get();
		$this->db->last_query(); 
		return $query->result();
	}
	
	function get_classes($school_id, $branch_id) {
		$this->db->select('*');
		$this->db->from($this->classes);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
		$this->db->where_in('branch_id', $branch_id); 
		$query = $this->db->get();
		$this->db->last_query();
		return $query->result();
}

	function getsubjects($school_id, $branch_id, $class_id) {
		$this->db->select('*');
		$this->db->from($this->subjects);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
		$this->db->where_in('branch_id', $branch_id); 
		$this->db->where_in('class_id', $class_id); 
		$query = $this->db->get();
		$this->db->last_query(); 
		return $query->result();
}

	function teacher_role($school_id) {
		$this->db->select('*');
        $this->db->from($this->teacher_role);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0',"is_active="=>'1'));
		$query = $this->db->get();
	//	echo $this->db->last_query(); exit();
        return $query->result();
	}
	
	function teacher_type($school_id) {
	 $this->db->select('*');
        $this->db->from($this->teacher_type);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0',"is_active="=>'1'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
         return $query->result();
	}
	
	function teacher_level($school_id) {
	 $this->db->select('*');
     $this->db->from($this->teacher_level);
	 $this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0',"is_active="=>'1'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
       return $query->result();
	}
	function get_single_staff_access($sid) {
        $this->db->select('*');
        $this->db->from($this->staff_access);
		$this->db->where(array("user_id="=>$sid,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	function updatestaff($sid,$staffdata){
		$this->db->where('user_id', $sid);
		$this->db->update($this->tbl, $staffdata);
	}

	function updateenrolementuser($sid,$userdata){
		$this->db->where('id', $sid);
		$this->db->update($this->users, $userdata);
	}

	function updatestaffAccess($sid,$staffaccess){
		$this->db->where('user_id', $sid);
		$this->db->update($this->staff_access, $staffaccess);
		//echo $this->db->last_query();exit;
	}

	function getcurrencies() {
        $this->db->select('*');
        $this->db->from($this->currency);
		$query = $this->db->get();
        return  $query->result();
    }
	
	function updatedate($data,$where){
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }

/************************************************************ EXCEL FUNCTION **********************************************************************/

	function getExcelData($where=NULL,$school_id, $startdate, $enddate,$currentdate,$enrolementtype){
		
		 $this->db->select('*');
        
		 $this->db->from($this->tbl);
		
		 $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		
		 $this->db->where(array($this->tbl.'.school_id'=>$school_id));
		
		  if($where != NULL){
			  
			$this->db->where($where);
		  
		  }
		  
		  if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->users.'.created_date >='=>$startdate));
			$this->db->where(array($this->users.'.created_date <='=>$enddate));
		  }
		  
		  if($enrolementtype=='applied'){
		  
		  $this->db->where(array($this->tbl.'.staff_application_date <='=>$currentdate));
		  }
		  
		  if($enrolementtype=='waiting'){
			$this->db->where(array($this->tbl.'.staff_postpone_date >='=>$currentdate));  
		  }
		  
		 $query = $this->db->get();
		 
		 //echo $this->db->last_query(); exit();
		  
         return  $query->result();
		}
		
		function display_experience($staff_enrol_id) {
		
          $this->db->select('*');
		  
          $this->db->from($this->staff_experience);
		  
		  $this->db->where(array('staff_enrol_id'=>$staff_enrol_id , 'is_deleted'=>'0'));
		  
		  $this->db->order_by($this->staff_experience.".exp_startyear", "desc"); 
		  
          $query = $this->db->get();
		  //echo $this->db->last_query();exit();
		  
          return  $query->result();
    }
	
function display_education($staff_enrol_id) {
		
          $this->db->select('*');
		  
          $this->db->from($this->staff_education);
		  
		  $this->db->where(array('staff_enrol_id'=>$staff_enrol_id , 'is_deleted'=>'0'));
		  
		  $this->db->order_by($this->staff_education.".degree", "asc"); 
		  
          $query = $this->db->get();
		  //echo $this->db->last_query();exit();
		  
          return  $query->result();
    }
		
	
}
