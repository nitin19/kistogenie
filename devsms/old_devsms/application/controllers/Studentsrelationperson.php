<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentsrelationperson extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','studentrelationperson_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){
		$data								= array();
		
		$data['student_id']					= $this->uri->segment(3);
		
		$student_id							= $this->uri->segment(3);
		$data['relation']					= $this->studentrelationperson_model->show_relation($student_id);
		
		$relationid 						= $this->uri->segment(4);
		if($relationid!='') {
		  $relation_data_rows				= $this->studentrelationperson_model->get_single_relation($relationid);
		  $data['info']				    	= $relation_data_rows[0];
		 }
		
		$this->load->view('header');
		$this->load->view('student/student_relation_person',$data);
		$this->load->view('footer');
     }
	 
	 public function add_relation(){
		 
		    $relationdata 					= 		   array();
		
			$school_id						= 		$this->session->userdata('user_school_id');
			$student_id						=		$this->input->post('studentid');
			
			$relationdata['student_id'] 	=		$this->input->post('studentid');
			$relationdata['school_id']  	=       $this->session->userdata('user_school_id');
			$relationdata['created_by'] 	= 		$this->session->userdata('user_name');
			$relationdata['is_active'] 		= 		'1';
			$relationdata['is_deleted']		=  		'0';
			$relationdata['created_date']   = 		date('Y-m-d H:i:s');;
			$relationdata['updated_date']   = 		date('Y-m-d H:i:s');
			$relationdata['student_relation_fullname'] = $this->input->post('relation_full_name');
			$relationdata['student_relation'] = $this->input->post('relation');
			$relationdata['student_relation_mobile_number'] = $this->input->post('mobile_no'); 
			
			$relationname = $this->input->post('relation_full_name');
			$contact = $this->input->post('mobile_no');
			
			$checkstudent_relation = $this->studentrelationperson_model->check_studentrelation($relationname, $contact, $student_id);
			if($checkstudent_relation > 0 ){
				$this->session->set_flashdata('error', 'Student Relation is already exist');
				redirect(base_url()."studentsrelationperson/index/$student_id");exit();
			}else {
				if($this->studentrelationperson_model->insert_student_relation_info($relationdata)){
					$this->session->set_flashdata('success', 'Your Data has been Saved successfully.');
					redirect(base_url()."studentsrelationperson/index/$student_id");exit();
				}else {
					$this->session->set_flashdata('error', 'Some problem exists. Relation has not been updated.');
				redirect(base_url()."studentsrelationperson/index/$student_id");exit();
				}
			}
        }
		
		public function update_relation(){
			
			  $student_id					    = $this->uri->segment(3);

		      $editedRelationid					= $this->uri->segment(4); 

			  $Action							= $this->uri->segment(5); 
		 
		  	if( $editedRelationid==NULL){
			$this->session->set_flashdata('error', 'Select relation first.');
			redirect(base_url()."studentsrelationperson/index");exit;
		   }
		   
		
		   
			 $relationdata 					= array();
		   
			 $where							= array('student_relation_id'=>$editedRelationid,'student_id'=>$student_id);
						
			if ($this->input->post('relation_full_name')!= '') {
                 $relationdata['student_relation_fullname'] = trim($this->input->post('relation_full_name'));
				 
            }
			
			if ($this->input->post('relation')!= '') {
                $relationdata['student_relation'] = trim($this->input->post('relation'));
				
            }
			
			if ($this->input->post('mobile_no') != '') {
                $relationdata['student_relation_mobile_number'] = trim($this->input->post('mobile_no'));
            }
			
			$relationdata['updated_date'] = date('Y-m-d H:i:s');

			
		   if($this->studentrelationperson_model->updateRelation($relationdata,$where)) {
			   $this->session->set_flashdata('success', 'Relation has been updated successfuly.');
			   redirect(base_url()."studentsrelationperson/index/$student_id/$editedRelationid/edit/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Relation has not been updated.');
				redirect(base_url()."studentsrelationperson/index/$student_id/$editedRelationid/edit/");exit;
		      }
			
		

	   }
	   
	   	function deactivaterelation(){
		
		  $student_id					    = $this->uri->segment(3);
		  $relationid						= $this->uri->segment(4); 
		  $where							= array('student_relation_id'=>$relationid);
		  $data								= array('is_active'=>'0');
		  
		if($this->studentrelationperson_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Relation has been updated successfully.');
			redirect(base_url()."studentsrelationperson/index/$student_id");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Fee band has not been updated.');
			redirect(base_url()."studentsrelationperson/index/$student_id");exit;
		}
		
	}
	function activaterelation(){
		  $student_id					    = $this->uri->segment(3);
		  $relationid						= $this->uri->segment(4); 
		  $where							= array('student_relation_id'=>$relationid);
		  $data								= array('is_active'=>'1');
		  
		if($this->studentrelationperson_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Relation has been updated successfully.');
			redirect(base_url()."studentsrelationperson/index/$student_id");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Relation has not been updated.');
			redirect(base_url()."studentsrelationperson/index/$student_id");exit;
		}
		
	}
	
	
		public function deleterelation(){
					
				   $studid 	 = 	$this->input->get('studid');
	
 				   $relid	 = 	$this->input->get('relid');
				 
		 		 if($this->studentrelationperson_model->delete($relid)){
					$this->session->set_flashdata('success', 'Relation has been deleted successfully.');
					redirect(base_url()."studentsrelationperson/index/$studid");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."studentsrelationperson/index/$studid");exit;
		          }
			  }
	
	
		function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	 }

}