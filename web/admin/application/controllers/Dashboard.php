<?php

class Dashboard extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

	}
	
	function index()
	{
		$this->load->view('dashboard');	
		$this->load->model('Dashboard_model');
	}
	
	public function view_student() 
    {
      $this->load->model("Dashboard_model");
    
            $d_id = $this->uri->segment(1);
            $data['viewstudent_detail'] = $this->Dashboard_model->view_student();
            $this->load->view('student', $data);
    }
    
    public function view_studentdetail() 
    {
      $this->load->model("Dashboard_model");
    
            $d1_id = $this->uri->segment(3);
            
            
            
            $data['detail'] = $this->Dashboard_model->view_studentdetail($d1_id);
            
           // print_r( $data['detail']);
            $this->load->view('student_detail', $data);
    }
    
    
    
    public function delete_student()
    {   
    		
         $this->load->model("Dashboard_model");
      
            $std_id = $this->uri->segment(3);
           
            $data['deletestudent_detail'] = $this->Dashboard_model->delete_student($std_id);
           redirect ('Dashboard/view_student');
    
    }
    
    
    
    public function view_staff() 
    {
      $this->load->model("Dashboard_model");
    
            $f_id = $this->uri->segment(3);
            $data['viewstaff_detail'] = $this->Dashboard_model->view_staff();
            $this->load->view('staff', $data);
    }
    
    public function view_staffdetail() 
    {
      $this->load->model("Dashboard_model");
    
            $f1_id = $this->uri->segment(3);
            $data['details'] = $this->Dashboard_model->view_staffdetail($f1_id);
            $this->load->view('staff_detail', $data);
    }
    
    
    
    public function delete_staff()
    {   
    		
         $this->load->model("Dashboard_model");
      
            $stf_id = $this->uri->segment(3);
           
            $data['deletestaff_detail'] = $this->Dashboard_model->delete_staff($stf_id);
           redirect ('Dashboard/view_staff');
    
    }


}
?>