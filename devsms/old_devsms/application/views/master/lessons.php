<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="#">Lessons</a></li>        

        </ul>

        </div>

        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>

        </div>

    	
                <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>

        

        <div class="attendancesec">	
        
        
        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">
             
             
        <?php 
		$editedLessonid = $this->uri->segment(4); 
		$Action = $this->uri->segment(5); 
		if($Action == 'edit') {
		?>
        
<form action="<?php echo base_url();?>lessons/updatelesson/<?php echo $last_page;?>/<?php echo $editedLessonid;?>" name="lessonsForm" id="lessonsForm" method="post">

<div class="col-sm-12 col-xs-12 applycodediv nopadding">

 <div class="col-sm-12 nopadding selectoption">

    <div class="form-group fullwidthinput">

    <label class="col-sm-2 control-label nopadding filterlabel">Branch:</label>

    <div class="col-sm-10 nopadding selectfilter" style="margin-bottom:35px">

    <div class="col-sm-3 inputbox termselect">

      <select class="form-control" name="schoolbranch" id="schoolbranch">
      <option value="">Select Branch</option>   
     <?php  foreach($branches as $branch) { ?> 
       <option <?php if($branch->branch_id == $info->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>  
         <?php } ?>
     </select>

    </div>
    
    <div class="col-sm-3 inputbox termselect">
      <select class="form-control" name="classname" id="classname">
      <option value="">Select Class</option>
       <?php 
  if( $info->branch_id != '') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = $info->branch_id;
		  $this->load->model(array('lessons_model'));
		  $sclasses =$this->lessons_model->getclasses($schoolbranch,$school_id);
     foreach($sclasses as $sclass) { ?>
          <option <?php if($sclass->class_id == $info->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>   
     </select>
      </div>
      
    <div class="col-sm-3 inputbox termselect">
      <select class="form-control" name="subjectname" id="subjectname">
      <option value="">Select Subject</option> 
      
       <?php 
  if( $info->branch_id != '' && $info->class_id != '') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = $info->branch_id;
		  $classname = $info->class_id;
		  $this->load->model(array('lessons_model'));
		  $getsubjects =$this->lessons_model->getsubjects($school_id,$schoolbranch,$classname);
     foreach($getsubjects as $getsubject) { ?>
          <option <?php if($getsubject->subject_id == $info->subject_id){ echo 'selected'; } ?> value="<?php echo $getsubject->subject_id;?>"><?php echo $getsubject->subject_name;?></option>
        <?php  } 
          }
	    ?> 
        
     </select>
      </div>
    
    <div class="col-sm-3 inputbox termselect">
    <input type="text" name="lessonname" id="lessonname" value="<?php echo $info->lesson_name;?>" placeholder="Enter lesson name" class="form-control" >
        </div>
   
     </div>
     
    </div>
    
    
    <div class="form-group fullwidthinput">
    <label class="col-sm-2 control-label nopadding filterlabel">Description:</label>
    <div class="col-sm-10 nopadding selectfilter" id="selectfilter">
    <div class="col-sm-12 inputbox termselect">
    <textarea  class="form-control" name="lessondescription" id="lessondescription" placeholder="Write Description"><?php echo $info->lesson_description;?> </textarea>
    </div>
   

      <div class="confirmlink">
        <input type="submit" class="open1" value="Update">
        </div>
        
    
          </div>
       </div>
    
      </div>
    </div>

    </form>
        
     <?php } else { ?>

    <form action="<?php echo base_url();?>lessons/addlesson" name="lessonsForm" id="lessonsForm" method="post" autocomplete="off">


<div class="col-sm-12 col-xs-12 applycodediv nopadding">

 <div class="col-sm-12 nopadding selectoption">

    <div class="form-group fullwidthinput">

    <label class="col-sm-2 control-label nopadding filterlabel">Branch:</label>

    <div class="col-sm-10 nopadding selectfilter" style="margin-bottom:35px">

    <div class="col-sm-3 inputbox termselect">

      <select class="form-control" name="schoolbranch" id="schoolbranch">
      <option value="">Select Branch</option>   
      <?php  foreach($branches as $branch) { ?> 
       <option value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>  
         <?php } ?>
     </select>

    </div>
    
    <div class="col-sm-3 inputbox termselect">
      <select class="form-control" name="classname" id="classname">
      <option value="">Select Class</option>   
     </select>
      </div>
      
    <div class="col-sm-3 inputbox termselect">
      <select class="form-control" name="subjectname" id="subjectname">
      <option value="">Select Subject</option>   
     </select>
      </div>
    
    <div class="col-sm-3 inputbox termselect">
    <input type="text" name="lessonname" id="lessonname" value=""  placeholder="Enter lesson name" class="form-control" autocomplete="off" maxlength="61" >
        </div>
   
     </div>
     
    </div>
    
    
    <div class="form-group fullwidthinput">
    <label class="col-sm-2 control-label nopadding filterlabel">Description:</label>
    <div class="col-sm-10 nopadding selectfilter" id="selectfilter">
    <div class="col-sm-12 inputbox termselect">
    <textarea  class="form-control" name="lessondescription" id="lessondescription" placeholder="Write Description" autocomplete="off" maxlength="61" > </textarea>
    </div>
   

      <div class="confirmlink">
        <input type="submit" class="open1" value="Add">
        </div>
        
    
          </div>
       </div>
    
      </div>
    </div>

    </form>
        
    <?php } ?>
  

        </div>

    </div>
        

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

  <form action="<?php echo base_url();?>lessons/index" name="lessonsfilterform" id="lessonsfilterform">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding">

 <div class="col-sm-7 nopadding selectoption">

<div class="form-group fullwidthinput">

    <!--<label class="col-sm-1 col-xs-1 control-label nopadding filterlabel">Show</label>-->

    <div class="col-sm-12 col-xs-11 nopadding selectfilter">

    <div class="col-sm-3 col-xs-3 inputbox termselect">

      <select class="form-control" name="branch" id="branch">
  <option value="">Select Branch</option>    
  <?php foreach($branches as $branch) { ?>
     <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
    <?php  } ?>
</select>

    </div>
    
    
    <div class="col-sm-3 col-xs-3 inputbox termselect">

      <select class="form-control" name="class" id="class">
       <option value="">Select Class</option>  
         <?php 
  if( @$_GET['branch']!='') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = @$_GET['branch'];
		  $this->load->model(array('lessons_model'));
		  $sclasses =$this->lessons_model->getclasses($schoolbranch,$school_id);
     foreach($sclasses as $sclass) { ?>
          <option <?php if($class_search == $sclass->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>
     </select>

    </div>
    
    
    <div class="col-sm-3 col-xs-3 inputbox termselect">

      <select class="form-control" name="subject" id="subject">
       <option value="">Select Subject</option>  
       
        <?php 
  if( @$_GET['branch']!='' && @$_GET['class']!='' ) {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = @$_GET['branch'];
		  $classname =  @$_GET['class'];
		  $this->load->model(array('lessons_model'));
		  $getsubjects =$this->lessons_model->getsubjects($school_id,$schoolbranch,$classname);
     foreach($getsubjects as $getsubject) { ?>
          <option <?php if($subject_search == $getsubject->subject_id){ echo 'selected'; } ?> value="<?php echo $getsubject->subject_id;?>"><?php echo $getsubject->subject_name;?></option>
        <?php  } 
          }
	    ?> 
         
      </select>

    </div>

    <div class="col-sm-3 col-xs-3 inputbox termselect">

<select class="form-control" name="status" id="status">
  <option value="">Select Status </option>    
   <option <?php if($status_search == "1"){ echo 'selected'; } ?> value="1">Active</option>
   <option <?php if($status_search == "0"){ echo 'selected'; } ?> value="0">Inactive</option>
</select>

    </div>

    </div>

  </div>

</div>		

        <div class="col-sm-2 col-xs-5 searching termsearch nopadding">

        <div class="form-group">

      <input type="text" class="form-control searchbox" placeholder="Search By Lesson" name="seachword" id="seachword" value="<?php echo $word_search;?>">

   <!--<input type="submit" value="" class="searchbtn">-->

     </div>

        </div>

        <div class="col-sm-3 col-xs-7 rightspace viewreport">

<div class="form-group">

    <input type="submit" class="btn btn-danger" value="Find Lesson">

    <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>

  </div>

</div>

        </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <h1>Lessons<span><?php echo $Totalrec = $total_rows;?></span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2"> No.</th>
                      
                      <th class="column2">Lesson</th>
                      
                      <th class="column2">Subject</th>
                      
                      <th class="column3"> Class </th>

					  <th class="column4"> Branch </th>

                     <th class="column3">Status</th>
                     
                      <th class="column3">Action</th>

				  </tr>

			  </thead>

				<tbody>

					<?php 
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $lesson) { 
					     $sr++;
						  $branch_id = $lesson->branch_id;
						  $class_id = $lesson->class_id;
						  $subject_id = $lesson->subject_id;
						  $this->load->model(array('lessons_model'));
						  $branchName = $this->lessons_model->getbranchName($branch_id);
						  $ClassName = $this->lessons_model->getclassName($class_id);
						  $SubjectName = $this->lessons_model->getsubjectName($subject_id);
					?>
					<tr class="familydata students">
						<td class="column2"><?php echo $sr; ?></td>
                        <td class="column2"><?php echo $lesson->lesson_name;?></td>
                        <td class="column2"><?php echo @$SubjectName->subject_name;?> </td>
                        <td class="column3"><?php echo @$ClassName->class_name;?></td>
                        <td class="column4"><?php echo @$branchName->branch_name;?> </td>

                        <td class="column3"><span class="acceptbtn statusbtn <?php if($lesson->is_active==1) { echo 'Active'; } else { echo 'Inactive'; } ?>"><?php if($lesson->is_active==1) { echo 'Active'; } else { echo 'Inactive'; } ?></span></td>
                        
                        
                         <td class="column3">
                         
                         <div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
                                                
	      <li><a href="<?=base_url();?>lessons/index/<?php echo $last_page;?>/<?php echo $lesson->lesson_id;?>/edit<?php echo $post_url;?>"> Edit </a></li>
          
				<li>
				<?php if($lesson->is_active == '1'){ ?>
	<a href="<?php echo base_url();?>lessons/deactivatelesson/<?php echo $last_page;?>/<?php echo $lesson->lesson_id;?><?php echo $post_url;?>" title="click to deactivate lesson"> Active </a>
					<?php
					 } else {
					?>
	<a href="<?=base_url();?>lessons/activatelesson/<?php echo $last_page;?>/<?php echo $lesson->lesson_id;?><?php echo $post_url;?>" title="click to activate lesson">De-Activated </a>
				<?php } ?>
				</li>
                
			<li class="divider"></li>
				 <li><a onclick="delConfirm('<?php echo $lesson->lesson_id;?>')" style="cursor: pointer;"> Delete </a> </li>
									</ul>
								</div>
                         
                         </td>
                                        
                                        
                        </tr>
				<?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1056px; background:#FFF;">No record to show.</th></tr>	
				   <?php } ?>

				</tbody>

		  </table>

     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-9 col-xs-6 paginationblk">
         <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
			<?php  } ?>
	<ul class="pagination">
		
		<?php echo $pagination;?>
        

	</ul>

    </div>

    <div class="col-sm-3 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total: <?php echo $Totalrec ;?> </h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>lessons/index" method="post">

  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>

  <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>

    </div>

	</div>

    </div>     

</div>





	</div>

    

	</div>

    </div>

    </div>
    
        
	
 
 <script>
  jQuery(document).ready(function(){
	  
	  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
					
	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#lessonsfilterform")[0].reset();
			   window.location.href='<?php echo base_url()?>lessons/index';
			}); 
			
			
	  jQuery("#schoolbranch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			// var BranchID =  jQuery('input[name=student_school_branch]:checked').val();
			jQuery("#classname").find('option[value!=""]').remove();
			jQuery("#subjectname").find('option[value!=""]').remove();
			
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>lessons/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#classname').append(resp);
			          }
				   });
				 } else {
			     jQuery("#classname").find('option[value!=""]').remove();
				 jQuery("#subjectname").find('option[value!=""]').remove();
				 return false;
				 }
			 });
			 
		
		 jQuery("#classname").on('change', function(e){ 
   		   var classname = jQuery(this).val();
		   if(classname!='') {
			var schoolbranch =  jQuery('#schoolbranch').val();
			jQuery("#subjectname").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>lessons/check_subjects',
            data:{ 'schoolbranch': schoolbranch,'classname': classname },
            success :  function(resp) {
					    jQuery('#subjectname').append(resp);
			          }
				   });
				 } else {
			     jQuery("#subjectname").find('option[value!=""]').remove();
				 return false;
				 }
			 });
			 
			 
	 jQuery("#branch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			// var BranchID =  jQuery('input[name=student_school_branch]:checked').val();
			jQuery("#class").find('option[value!=""]').remove();
			jQuery("#subject").find('option[value!=""]').remove();
			
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>students/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#class').append(resp);
			          }
				   });
				 } else {
			     jQuery("#class").find('option[value!=""]').remove();
				 jQuery("#subject").find('option[value!=""]').remove();
				 return false;
				 }
			 });	 
					
					
	  jQuery("#class").on('change', function(e){ 
   		   var classname = jQuery(this).val();
		   if(classname!='') {
			var schoolbranch =  jQuery('#branch').val();
			jQuery("#subject").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>lessons/check_subjects',
            data:{ 'schoolbranch': schoolbranch,'classname': classname },
            success :  function(resp) {
					    jQuery('#subject').append(resp);
			          }
				   });
				 } else {
			     jQuery("#subject").find('option[value!=""]').remove();
				 return false;
				 }
			 });
	 
	 
  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });
	
    jQuery("#lessonsForm").validate({
        rules: {
              schoolbranch: {
                 required: true
                 },
			  classname: {
                required: true
                 },
			   subjectname: {
                   required: true
                 },
			   lessonname: {
                   required: true,
                   lettersonly: true,
				   maxlength: 60
                 },
			   lessondescription: {
                  required: true
               }
        },
        messages: {
              schoolbranch: {
                  required: "Please select branch."
                 },
			 classname: {
                  required: "Please select class."
                 },
			  subjectname: {
                  required: "Please select subject."
                 },
			  lessonname: {
                  required: "Please enter lesson name.",
				  lettersonly: "Please Enter Character Value",
				  maxlength: "Maximum 60 characters allowed."
				  
                 },
			  lessondescription: {
                  required: "Please enter description"
                 }
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
	});
</script>
 
  
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>lessons/deletelesson/"+id;
		}else{
			return false;
		}
	}
</script>	

<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.fullwidthinput select {
    background-position: 95% center;
}
.Inactive {
    background: #ff0000 none repeat scroll 0 0;
}

<!--changes-->
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px 0 0 0;
}
.cleanSearchFilter {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533 !important;
    border-radius: 3px;
    color: #fff !important;
    padding: 9px;
    text-decoration: none !important;
}
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px 0 0;
}
@media screen and (min-width:320px) and (max-width:480px){
.searching {
    width: 100% !important;
}
.col-sm-9.col-xs-6.paginationblk {
	text-align:center;
}
.cleanSearchFilter {
    padding: 8px 5px !important;
}
.termsearch {
    padding-bottom: 10px !important;
}
.viewreport {
    padding: 0 4px;
    width: 100%;
}
}
@media screen and (min-width:481px) and (max-width:767px){
	.col-sm-4.inputbox.termselect {
    padding-bottom: 15px;
}
.selectoption {
    width: 100%;
	padding-bottom: 10px;
}
.col-sm-3.inputbox.termselect {
    padding-bottom: 10px;
}
.inputbox.termselect select {
    font-size: 12px;
}
.selectfilter .col-sm-3.col-xs-3.inputbox.termselect {
    width: 25% !important;
}
}
@media screen and (min-width:768px) and (max-width:991px){
.col-sm-12.col-xs-11.nopadding.selectfilter {
    padding-bottom: 10px;
}
.fullwidthinput label {
	width:auto;
}
.totaldiv h3 {
    text-align: center;
}
}
@media screen and (min-width:992px) and (max-width:1087px){
.cleanSearchFilter {
    padding: 9px 4px;
}
.btn {
    padding: 6px 5px;
}
.selectfilter .inputbox.termselect select {
    font-size: 11px;
}
.statusbtn {
    padding: 6px 12px;
}
}
</style>	