<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>

<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>
				<?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

 <li><a href="<?php echo base_url(); ?>help">Help and Knowledgebase</a></li>


        <li class="edit">Students Attendance</li>    

        </ul>

        </div>

 

   <div class="block-content">	

      <div class="col-sm-12 tablediv nopadding">
      
      
      
      
      

    <div class="col-sm-3">
	<div class="list-block">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#admin-dashboard" aria-controls="home" role="tab" data-toggle="tab">WHAT IS STUDENT ATTENDANCE SECTION?</a></li>
	  <li role="presentation"><a href="#modify-student" aria-controls="profile" role="tab" data-toggle="tab">MODIFY ATTENDANCE</a></li>
      <li role="presentation"><a href="#summary-update" aria-controls="profile" role="tab" data-toggle="tab">SUMMARY</a></li>
	  <li role="presentation"><a href="#generate-report" aria-controls="profile" role="tab" data-toggle="tab">GENERATE REPORT</a></li>
	  <li role="presentation"><a href="#attendence-archive" aria-controls="profile" role="tab" data-toggle="tab">ATTENDANCE ARCHIEVE</a></li>
    </ul>
	</div>
    </div>
    <div class="col-sm-9">
      <div class="tab-content adm-dsh">
		<div role="tabpanel" class="tab-pane active" id="admin-dashboard">
			<h1 class="tab-title">Student Attendence</h1>
			<p class="main-heading-cntnt"> It is a measure of number of students who attend school  and the amount of time they are present.</p>
			<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/attendanc-progress-report 1.png" class="stt">
			<div class="admn-content">
					<h3 class="para-heading">This module consist in 4 sections:</h3>
					<p class="heading-cntnt"> <strong>1) MODIFY ATTENDANCE</strong></p>
					<p class="heading-cntnt"><strong>2) SUMMARY</strong></p>
					<p class="heading-cntnt"><strong>2) GENERATE REPORT</strong></p>
					<p class="heading-cntnt"><strong>2) ATTENDANCE ARCHIEVE</strong></p>	
			</div>
	 </div>
	 
	 
	
	
	
      
	 
<!-- Modify-student--->
	 
	  <div role="tabpanel" class="tab-pane" id="modify-student">
			<h1 class="tab-title">Modify Attendence</h1>
			<p class="main-heading-cntnt"></p>
			
			<div class="admn-content">
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/modify_report.jpg" class="stt">
				<div class="compse-msg">
					<p class="heading-cntnt"><strong> 1)</strong> This section is like a student attendance register.</p>
					<p class="heading-cntnt"><strong> 2)</strong> On selecting branch, class, date and  also select session for attendance i.e a.m or p.m then clicked on  find student,  students of all class will display.</p>
					<p class="heading-cntnt"><strong> 3)</strong> In student listing, there is a field named status in which user can select the status for student i.e he or she is present, absent or late.</p>
					<p class="heading-cntnt"><strong> 4)</strong> In case if user would not select the attendance status for  student, then by default , status for student will be set as present.</p>
					<p class="heading-cntnt"><strong> 5)</strong> In note field user can write the notes regarding the student attendance.</p>
					<p class="heading-cntnt"><strong> 6)</strong> Clicked on update register to update the student attendance.</p>
					<p class="heading-cntnt"><strong> 6)</strong> User successfully updated the student attendance.</p>
				
				</div>
		
			</div>
	
	
	 </div>	
	 
<!--summary-->
	 <div role="tabpanel" class="tab-pane" id="summary-update">	 
		<h1 class="tab-title">Summary</h1>
			<p class="main-heading-cntnt">This section represents:</p>
			<div class="admn-content">
				<img src="<?php echo base_url();?>uploads/school_certificates/summary.jpg" class="stt">
				<div class="compse-msg">
					<p class="heading-cntnt"><strong> DAILY ATTENDANCE:</strong> What students are here or not here for the day.</p>
					<p class="heading-cntnt"><strong> ATTENDANCE BY STUDENT: </strong> It display attendance data for specific student or by student on selecting date in terms of range, branch, class and clicked on view student button user can view the student attendance summary.
						<br>
						In student attendance summary, user can view the student attendance in terms of percentage and also user can view the session i.e how many session of the student attendance is  updated.
						<br>
						User can also view the average total of the student’s attendance.</p>
				</div>
			
			</div>
			
			
	  </div>
	  
	<!--summary-->  
	
	
	<!--summary-->
	 <div role="tabpanel" class="tab-pane" id="generate-report">	 
		<h1 class="tab-title">Generate Report</h1>
			<p class="main-heading-cntnt">In this section, user can create a generate report  of the student attendance.</p>
			<div class="admn-content">
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/generate_report.jpg" class="stt">
				<div class="compse-msg">
					<h3 class="para-heading">How It Works:</h3>
					<p class="heading-cntnt"><strong> 1)</strong> What students are here or not here for the day.</p>
					<p class="heading-cntnt"><strong> 2)</strong> User successfully create a generate report.</p>
					<p class="heading-cntnt"><strong> 3)</strong> User  can view the generate report  using “VIEW REPORT” button provide within the action.</p>
					<p class="heading-cntnt"><strong> 4)</strong> User can also view the student’s attendance by selecting starting and ending date.</p>
					<h3 class="para-heading">FUNCTIONALITY OF SAVE ARCHIVE BUTTON</h3>
					<p class="heading-cntnt">User can also save the student generate report by clicked on save archive button.
							<br>
							When clicked on save archive button, the popup will generate in which user will write the  report archive name , then clicked on save button to save the archive.</p>
						
					<h3 class="para-heading">FUNCTIONALITY OF PRINT REPORT BUTTON</h3>
					<p class="heading-cntnt">On clicking print report, user can print the generate report of student’s attendance.

							<br>
					In showing dropdown ,user can select the numbers of records to view  per page.
							<br>
					Total represents  the total number of generate reports in the listing.</p>
					
				</div>
			
			</div>
			
			
	  </div>
	  
	<!--summary-->  
	
	
	
	<!--summary-->
	 <div role="tabpanel" class="tab-pane" id="attendence-archive">	 
		<h1 class="tab-title">Attendence Archive</h1>
			<p class="main-heading-cntnt">On selecting the starting & ending date, branch ,class and clicked on view report, user can view the saved generate report.</p>
			<div class="admn-content">
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/attendence_archive.jpg" class="stt">
				<div class="compse-msg">
					<p class="heading-cntnt"><strong> 1)</strong> User can view the listing of generate report with action button of functionalities:</p>
					<p class="heading-cntnt"><strong> 2) Download:- </strong>  User can download the report</p>
					<p class="heading-cntnt"><strong> 3) Delete:- </strong> User can delete the report</p>
					<p class="heading-cntnt"><strong> 4)</strong> In showing dropdown ,user can select the numbers of records to view  per page.
					<br>
					Total represents  the total number of  saved reports in the listing.</p>
					
				</div>
			
			</div>
			
			
	  </div>
	  
	<!--summary-->  
	  
    </div>
    </div>
      
      

         

	   </div>
    
	 </div>

    </div>

<style>
.tabs-left, .tabs-right {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}

.tab-content.adm-dsh {
    background: #fff;
    width: 100%;
    display: inline-block;
    padding: 10px 20px;
    border: 1px solid #eee;
}
h1.tab-title {
    margin-top: 0 !important;
    margin-left: 0 !important;
    border: none !important;
    font-weight: 900;
    font-size: 32px !important;
}
.adm-dsh p {
    margin-left: 0;
}
p.main-heading-cntnt {
    margin-top: -44px;
    border-bottom: 1px solid #eeeeee;
    padding: 10px 0;
	margin-left:20px;
}
.admn-content {
    margin-top: -2px;
    padding-top: 10px;
    padding-left: 20px;
}
h3.para-heading {
        margin-top: 0px;
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
.admn-content p {
    margin-top: -7px;
}
img.stt {
    width: 100%;
    padding: 30px 0;
	margin-top:-10px;
}
.compse-msg {
    padding: 4px 0 0 0px;
}
h4.txt-frmt {
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
ul.msg-point {
    padding-left: 0;
    margin: 0 0 0 14px;
    line-height: 30px;
    font-size: 15px;
}
.compse-msg {
    padding: 20px 0 16px 0px;
    line-height: 28px;
}
h3.para-heading.mgn {
    margin-top: 20px;
}
.list-block {
    height: 634px;
    width: 100% !important;
    background: #fff;
}
p.spacing {
    line-height: 15px;
    margin-top: -17px;
}
</style>