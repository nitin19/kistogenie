<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Register extends CI_Controller {



	   function __construct() {

        parent::__construct();

		$this->load->model(array('login_model','authorization_model','register_model'));

    }

	public function index()	{
		$data = array();
		$data['error']		= $this->session->flashdata('error');
		$data['success']	= $this->session->flashdata('success');
		$this->load->view('register',$data);
	}
	
	public function check_schoolname()

	{
		 $schoolname = trim($_POST['school_name']); 
	     $checksName = $this->register_model->check_registred_schoolname($schoolname);
		  $countSchools = count($checksName);
		 if( $countSchools > 0 ) {
			echo 'false';
	        } else {
		    echo 'true';
	        }
	    }
	
	public function check_username()

	{
		 $Uname = trim($_POST['username']); 
	     $checkusername = $this->register_model->check_registred_username($Uname);
		  $countUsers = count($checkusername);
		 if( $countUsers > 0 ) {
			echo 'false';
	        } else {
		    echo 'true';
	        }
	    }
		
   
   public function addschool(){
		 
		$userdata = array();
		$schooldata = array();						
		$data							= array();
		$data['title']	      			= "Add School";
		$data['mode']					= "Add";
			 
			$schooldata['is_active'] = '1';
			 
			if ($this->input->post('school_name') != '') {
                $schooldata['school_name'] = trim($this->input->post('school_name'));
				$schoolname = trim($this->input->post('school_name'));
            }
			
			if ($this->input->post('school_email') != '') {
                $schooldata['school_email'] = trim($this->input->post('school_email'));
				$schoolemail = trim($this->input->post('school_email'));
            }
			
			if ($this->input->post('school_phone') != '') {
                $schooldata['school_phone'] = trim($this->input->post('school_phone'));
				$schoolphone = trim($this->input->post('school_phone'));
            }
			
			if ($this->input->post('school_address') != '') {
                $schooldata['school_address'] = trim($this->input->post('school_address'));
				$schooladdress = trim($this->input->post('school_address'));
            }
			
			if ($this->input->post('school_contact_person') != '') {
                $schooldata['school_contact_person'] = trim($this->input->post('school_contact_person'));
				$schoolcontactperson = trim($this->input->post('school_contact_person'));
            }
			
			
			 if ($this->input->post('username') != '') {
                $userdata['username'] = trim($this->input->post('username'));
				$Uname =  trim($this->input->post('username'));
            }
			
			if ($this->input->post('password') != '') {
                $userdata['password'] = trim($this->input->post('password'));
				$password = trim($this->input->post('password'));
            }
			
			if ($this->input->post('school_email') != '') {
                $userdata['email'] = trim($this->input->post('school_email'));
            }
			
			//$userdata['created_by'] = $this->session->userdata('user_name');
			$userdata['user_type'] = 'admin';
			$userdata['is_active'] = '1';
			$userdata['created_date'] = date('Y-m-d H:i:s');;
			$userdata['updated_date'] = date('Y-m-d H:i:s');
			
			  $checksName = $this->register_model->check_registred_schoolname($schoolname);
		      $countSchools = count($checksName);
			
			if( $countSchools > 0 ) {
				$this->session->set_flashdata('error', 'School name already exists. School has not been added.');
				redirect(base_url()."register");exit;
			
	            } else {
					
				      $checkusername = $this->register_model->check_registred_username($Uname);
		              $countUsers = count($checkusername);
				
				        if( $countUsers > 0 ) {
				         $this->session->set_flashdata('error', 'User name already exists. School has not been added.');
				          redirect(base_url()."register");exit;
			
	                      } else {
							   $registred_school_id =  $this->register_model->insertSchool($schooldata);
							  if($registred_school_id) {
								  
								  $userdata['school_id'] = $registred_school_id;
								  
							      if($this->register_model->insertUser($userdata)) {
									  
									        $to_admin	= $schoolemail;
											$subject	= "New school registration";
											$message	= "<font face='arial'>";
											$message	.= "Hello ".$Uname.",<br />";
											$message	.= "<b> Your school " .$schoolname. " has been registred successfully.</b><br />";
											$message	.= "<b> Here is your school details: </b> <br />";
											$message	.= "<b> Name: </b> " .$schoolname. "<br />";
											$message	.= "<b> Email: </b> " .$schoolemail. "<br />";
											$message	.= "<b> Phone: </b> " .$schoolphone. "<br />";
											$message	.= "<b> Address: </b> " .$schooladdress. "<br />";
											$message	.= "<b> Contact Person: </b> " .$schoolcontactperson. "<br />";
											$message	.= "<b> Username: </b> " .$Uname. "<br />";
											$message	.= "<b> Password: </b> " .$password. "<br />";
											$message	.= "<br />Thanks.<br />";
											$message	.= "</font>";
											$this->authorize->send_email($to_admin,$message,$subject);
											
											$to_superadmin	= 'binarydata2015@gmail.com';
											$subject_superadmin	= "New school registration";
											$message_superadmin	= "<font face='arial'>";
											$message_superadmin	.= "Hello Superadmin,<br />";
											$message_superadmin	.= "<b>A new school " .$schoolname. " has been registred successfully. </b><br />";
											$message_superadmin	.= "<b> Here is school details:</b> <br />";
											$message_superadmin	.= "<b> Name: </b> " .$schoolname. "<br />";
											$message_superadmin	.= "<b> Email: </b> " .$schoolemail. "<br />";
											$message_superadmin	.= "<b> Phone: </b> " .$schoolphone. "<br />";
											$message_superadmin	.= "<b> Address: </b> " .$schooladdress. "<br />";
											$message_superadmin	.= "<b> Contact Person: </b> " .$schoolcontactperson. "<br />";
											$message_superadmin	.= "<b> Username: </b> " .$Uname. "<br />";
											$message_superadmin	.= "<b> Password: </b> " .$password. "<br />";
											$message_superadmin	.= "<br />Thanks.<br />";
											$message_superadmin	.= "</font>";
											$this->authorize->send_email($to_superadmin,$message_superadmin,$subject_superadmin);
									  
	$this->session->set_flashdata('success', 'School has been added successfuly. Please click here for <a href="'.base_url().'">login</a>');
				 					   redirect(base_url()."register");exit;
									   
								  } else {
								    $this->session->set_flashdata('error', 'Some problem exists. School has not been added.');
									 redirect(base_url()."register");exit;
									}
							 
							    } else {
									$this->session->set_flashdata('error', 'Some problem exists. School has not been added.');
									 redirect(base_url()."register");exit;
			                         }
							  
				             }

	                  }
		
		
		$data['logmode']			= $this->logmode;
		$data['error']				= $this->session->flashdata('error');
		$data['success']			= $this->session->flashdata('success');

		$this->load->view('header');
		$this->load->view('register',$data);
		$this->load->view('footer');
	}
	

}

