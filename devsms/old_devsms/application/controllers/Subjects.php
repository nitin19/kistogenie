<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subjects extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','subjects_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){
		
		$subject_id = $this->uri->segment(4);
		
		$school_id		= $this->session->userdata('user_school_id');
			
		$search_condition = array();
		$data = array();
		
		$PerPage			= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		$srch_branch		= isset($_GET['srch_branch'])?$_GET['srch_branch']:NULL;
		$srch_class		    = isset($_GET['srch_class'])?$_GET['srch_class']:NULL;
		$srch_subject		= isset($_GET['srch_subject'])?$_GET['srch_subject']:NULL;
		
		if($srch_branch!=''){
			$search_condition['branch.branch_id']	= $srch_branch;
		}
		if($srch_class!=''){
			$search_condition['classes.class_id']	= $srch_class;
		}
		
		$totrows 					= $this->subjects_model->total_subjects($search_condition, $school_id, $srch_subject);
		$data["total_rows"] 		= count($totrows);
		$data["base_url"]			= base_url() . "subjects/index";
		
		if($PerPage!='') {
			$data["per_page"] 		= $PerPage;
			$perpage				= $data["per_page"];
		} else {
			 $data["per_page"]		= 10;
			 $perpage				= $data["per_page"];
			 
		}
		
		 $data["uri_segment"]		 = 3;
		 
		 $data['postfix_string'] 	 = "/?branch=$srch_branch&class=$srch_class&subject=$srch_subject&perpage=$perpage";
		  
		 $this->pagination->initialize($data);
		 $page 						 =  $this->uri->segment(3,0) ;
		 $data['last_page']			 = $page;
		 $data["links"] 			 = $this->pagination->create_links();
		 
		 $data['branch_search']		 = $srch_branch;
		 $data['class_search']		 = $srch_class;
		 $data['subject_search']	 = $srch_subject;		
		  
		$data['branch'] 			 = $this->subjects_model->get_branch($school_id);
		$data['detail'] 			 = $this->subjects_model->get_subjects($search_condition, $school_id, $data["per_page"], $page, $srch_subject);
		
		if($subject_id!=''){
		$subjectata 				 = $this->subjects_model->get_single_record($subject_id);
		$data['info']				 = $subjectata[0];
		}
					
		$this->load->view('header');

		$this->load->view('master/subjects' , $data);
	
		$this->load->view('footer');
		
				}
				
	public function get_class(){
			 $branch_id  = trim($this->input->post('branch_id'));
			 $school_id	= $this->session->userdata('user_school_id');
			 $getclasses = $this->subjects_model->get_classes($branch_id, $school_id);	
			  foreach($getclasses as $getclass) {
 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
			}	 		  
		  }
		  
	
	public function add_new_subject(){
		
		$subjectdata = array();
		
		$school_id		= $this->session->userdata('user_school_id');					
		$subjectdata['school_id'] = $school_id;
		
		 if (trim($this->input->post('branch')) != '') {
		 $subjectdata['branch_id'] = trim($this->input->post('branch'));
		 }
		 if(trim($this->input->post('subclass'))!= ''){
    	$subjectdata['class_id'] = trim($this->input->post('subclass'));
		 }
		 if(trim($this->input->post('subject'))!= ''){
		$subjectdata['subject_name'] = trim($this->input->post('subject'));
		  }
		  if(trim($this->input->post('description'))!= ''){
		$subjectdata['subject_description'] = trim($this->input->post('description'));
		  }
		     $subjectdata['is_active'] = 1 ;
		   $subjectdata['is_deleted'] = 0 ;
		    $subjectdata['created_by'] = $this->session->userdata('user_name');
			$subjectdata['created_date'] = date('Y-m-d H:i:s');
			$subjectdata['updated_date'] = date('Y-m-d H:i:s');
						
			$subject = trim($this->input->post('subject'));
			$branch = trim($this->input->post('branch'));
			$class = trim($this->input->post('subclass'));
						
		$checksubject = $this->subjects_model->check_subject($branch, $subject, $class, $school_id);
		           $countsubject = count($checksubject);
				   
				   if($countsubject > 0 ) {
					   
					   $this->session->set_flashdata('error', 'This Subject already exists For This Class in This School. Subject has not been added.');
				       redirect(base_url()."subjects/");exit;
				
				     } else {
					     if($this->subjects_model->insert_subject($subjectdata)) {
							$this->session->set_flashdata('success', 'Subject has been added successfuly.');
							redirect(base_url()."subjects/");exit; 
						 } else {
							$this->session->set_flashdata('error', 'Some problem exists. Subject has not been added.');
							redirect(base_url()."subjects/");exit; 
						 }
				   }
	     }
		 
		 public function update_subject(){
			 $id = $this->uri->segment(4);
			  $page = $this->uri->segment(3);
			  $edit = $this->uri->segment(5);
			 $school_id		= $this->session->userdata('user_school_id');
			 $data							= array();
			 $id = trim($this->input->post('subject_id'));
			 
			 $data = array(
'branch_id' => trim($this->input->post('branch')),
'class_id' => trim($this->input->post('subclass')),
'subject_name' => trim($this->input->post('subject')),
'subject_description' => trim($this->input->post('description'))
); 
			$class = trim($this->input->post('subclass'));
			$subject = trim($this->input->post('subject'));
			$branch = trim($this->input->post('branch'));
			$checksubject = $this->subjects_model->check_update_subject($branch, $subject, $class, $school_id, $id);
		     $countsubject = count($checksubject);
				   
				   if($countsubject > 0 ) {
					    $this->session->set_flashdata('error', 'This Subject Is already exists in this class at This branch of School. Subject has not been Updated.');
				       redirect(base_url()."subjects/index/".$page."/".$id.'/'.$edit);exit;
				
				     } else {
					$updatedata = $this->subjects_model->update_subject($id,$data);
					 if($updatedata = 1){
					 $this->session->set_flashdata('success', 'Subject has been updated successfuly.');
				redirect(base_url()."subjects/index/".$page."/".$id.'/'.$edit);exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Subject has not been updated.');
				redirect(base_url()."subjects/index/".$page."/".$id.'/'.$edit);exit;
			   }
			 }
		 }
		 
		  public function subject_action(){
			$id = $this->uri->segment(3);  
			$activity = $this->uri->segment(4);
			$data							= array();
			if($activity == 'active'){ 
			 $data = array(
				'is_active' => '1'
				);
		  } 
		  if($activity == 'inactive'){ 
			 $data = array(
				'is_active' => '0'
				);
		  } 
		  $actiondata = $this->subjects_model->subject_act_inact($id,$data);
		  if($actiondata = 1){	
				$this->session->set_flashdata('success', 'Subject has been updated successfuly.');
				redirect(base_url()."subjects/");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Subject has not been updated.');
				redirect(base_url()."subjects/");exit;
			   }
			  
		  }
		  
		    public function checkstaff_subject(){
			$subject_id = $this->uri->segment(3); 
			$school_id		= $this->session->userdata('user_school_id'); 
			$data = array(
				'is_deleted' => '1'
				);
		$subject_datarows = $this->subjects_model->get_delsubjects($school_id, $subject_id);
		$class_id = $subject_datarows->class_id;	
		$branch_id = $subject_datarows->branch_id;	
		
		$where = 'school_id ='.$school_id .' AND branch_id ='.$branch_id. ' AND FIND_IN_SET('.$class_id.', class_name) AND FIND_IN_SET('.$subject_id.', subject_name)' ;
		
		  $actiondata = $this->subjects_model->check_staffassignsubject($where);
		  if($actiondata > 0 ) {					   
					   $this->session->set_flashdata('error', 'Subject has Already Assign to staff. You Cannot Delete it');
				       redirect(base_url()."subjects/");exit;	
				     }	   else {
						 			$updatedata = $this->subjects_model->delete_subject($subject_id,$data);
									if( $updatedata = 1){
					 $this->session->set_flashdata('success', 'Subjects has been Deleted successfuly.');
				redirect(base_url()."subjects/");exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Subject has not been Deleted.');
				redirect(base_url()."subjects/");exit;
				}
						 }		  
		  }
	
}

