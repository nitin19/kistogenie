<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

             <ul>

		<li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>

        <li><a href="<?php echo base_url(); ?>attendance">Attendance</a></li>

 <!--       <li><a href="#">GCS - SAT - Class 1 </a></li>-->

        <li class="edit">Modify Attendance</li>        

        </ul>

        </div>

<!--        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" value="Quick actions" class="activequickaction">

        </div>-->

        </div>
        
       <div style="clear:both"></div>
         
     <div id="attendancestatusError" style="display:none;">
     <div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			 Please fill all students attendance status field first.
		       </div>
     </div>
                
        <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>
 

        <div class="attendancesec">	

        <div class="col-sm-12 profile-bg filterbox showbox">

        <div class="filterdiv">

  <form action="<?php echo base_url();?>modifyattendance/index" name="loadstudentsForm" id="loadstudentsForm" autocomplete="off">

          <div class="col-sm-5 col-xs-12 applycodediv nopadding fullwidsec">

                <div class="col-sm-2 selectfilter nopadding resselectfil">

                <span>Show:</span>

                </div>

		  <div class="col-sm-5 selectblk resselectbox">

<select class="form-control branchstyle" name="branch" id="branch">
      <option value="">Select Branch</option>   
     <?php foreach($branches as $branch) { ?>
     <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
    <?php  } ?>
      </select>

  </div>
  
  
		  <div class="col-sm-5 selectblk resselectbox">

        <select class="form-control" name="class" id="class">
         <option value="">Select Class</option> 
         <?php 
  if( @$_GET['branch']!='') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = @$_GET['branch'];
		  $this->load->model(array('modifyattendance_model'));
		  $sclasses =$this->modifyattendance_model->getclasses($school_id,$schoolbranch);
     foreach($sclasses as $sclass) { ?>
          <option <?php if($class_search == $sclass->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>  
        </select>

 			 </div>

         </div>


		<div class="col-sm-2 nopadding datepickdiv">

        <div class="form-group">

    <div class="col-sm-12 inputbox datepickerdiv nopadding dateinput">

      <input type="text" name="date" id="date" value="<?php echo $attendancedate_search;?>" class="datepickerattendance">

    </div>

  </div>

  		</div>
        
        
        <div class="col-sm-2 checkboxes resselectfil nopadding  session">
		 <div class="col-sm-10">
        <div class="radiobtn radiobutton">
   <input type="radio" name="session" id="radio_am" class="" <?php if($attendance_session_search == 'AM'){ echo "checked=checked"; } ?> value="AM" checked >
   <label for="radio_am">AM</label>
        <div class="check"></div>
          </div>

		<div class="radiobtn radiobutton">
   <input type="radio" name="session" id="radio_pm" class="" <?php if($attendance_session_search == 'PM'){ echo "checked=checked"; } ?> value="PM">
   <label for="radio_pm">PM</label>
        <div class="check"></div>
          </div>
 		</div>
        <div class="col-sm-1 nopadding">
<span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="Select session for Attendance" ></span>
        </div>
         </div>
        <div class="col-sm-3 col-xs-7 rightspace viewreport rightbtn">

<div class="form-group">

   <input type="submit" class="btn btn-danger" value="Find Students">
    <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>

  </div>

</div>

        

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding showbox">

    <div class="col-sm-12 nopadding fullwidsec">
    
    <?php if(@$_GET['branch']!='' && @$_GET['class']!='' ) { ?>

    <form action="<?php echo base_url(); ?>modifyattendance/update_attendance" name="updateattendanceForm" id="updateattendanceForm" method="post" >
    
      <input type="hidden" name="branchid" id="branchid" value="<?php echo @$_GET['branch'];?>" />
      <input type="hidden" name="classid" id="classid" value="<?php echo @$_GET['class'];?>" />
      <input type="hidden" name="attendancedate" id="attendancedate" value="<?php echo @$_GET['date'];?>" />
      <input type="hidden" name="attendance_session" id="attendance_session" value="<?php echo @$_GET['session'];?>" />

    <h1>Modify Attendance</h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">
					  <th class="column2">No.</th>
					  <th class="column5">First Name</th>
					  <th class="column5">Last Name</th>
                      <th class="column4">Status</th>
                      <th class="column4">Note</th>
                     <!-- <th class="column5">Other attendance today</th>
                      <th class="column2">Copy</th>-->
				  </tr>

			  </thead>

				<tbody>
                
                <?php 
					  if(count(@$data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $student) { 
					     $sr++;
		        $studentid = $student->student_id; 
		       $this->load->model(array('modifyattendance_model'));
		       $studentinfo =$this->modifyattendance_model->getstudentinfo($studentid);
					   ?>

				 <input type="hidden" name="studentid[]" value="<?php echo $student->student_id;?>" />
                 
					<tr class="familydata modifyattendance">
						<td class="column2"><?php echo $sr; ?></td>
						<td class="column5"><?php if($studentinfo->student_fname!='') { echo $studentinfo->student_fname; } else { echo '<span style="color:transparent;">-</span>'; } ?></td>
						<td class="column5"><?php if($studentinfo->student_lname!='') { echo $studentinfo->student_lname; } else { echo '<span style="color:transparent;">-</span>'; }?></td>
                        <td class="column4 statusblk"><div class="form-group">

    <div class="col-sm-9 inputbox nopadding">
<?php if($Attendencerecords = "Attendencerecords"){  ?>
		<select class="form-control selectstyle" name="attendance_status[<?php echo $student->student_id;?>]">
     	<option value="">Select</option>
  		<option <?php if(@$student->attendance_status == 1){ echo 'selected'; } else {echo 'selected'; }?> value="1">Present</option>
  		<option <?php if(@$student->attendance_status == 2){ echo 'selected'; } ?> value="2">Absent</option>
  		<option <?php if(@$student->attendance_status == 3){ echo 'selected'; } ?> value="3">Late</option>
		</select>	 
        <?php	 
 } 
 
 else 
 {	
 ?>
 <select class="form-control selectstyle" name="attendance_status[<?php echo $student->student_id;?>]">
     	<option value="">Select</option>
  		<option value="1" selected>Present</option>
  		<option value="2">Absent</option>
  		<option value="3">Late</option>
		</select> 	
        <?php
	 } 
   ?>
    </div>

  </div></th>
                        <td class="column4"><div class="form-group">

    <div class="col-sm-9 inputbox nopadding">

      <input type="text" class="form-control" name="attendance_note[<?php echo $student->student_id;?>]" id="attendance_note" value="<?php echo @$student->attendance_note;?>" placeholder="">

    </div>

  </div></th>
                   <!--      <td class="column5"><i class="fa fa-eye"></i></th>
                          <td class="column2"><i class="fa fa-folder"></i></th>-->
                        </tr>

                      
                          <?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1056px;height:100px; font-size:25px; background:#FFF; color: #6a7a91; ">No record to show.</th></tr>	
				   <?php } ?>  

				</tbody>

		  </table>

 <?php if(count(@$data_rows) > 0) { ?>
 
        <div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-6 nopadding">

        <div class="cancellink">

       
        </div>

        </div>

        <div class="col-sm-6 nopadding">

        <div class="confirmlink">

         <input type="submit" name="updateattendanceBtn" id="updateattendanceBtn" class="btn btn-default" value="Update Register">

        </div>

        </div>

        </div>  
        
        <?php } ?>

</div>

</form>

    <?php } else { ?>
   
    
    <div class="beforeresult_section"><h1></h1></div>

       <?php } ?>

	</div>

    <!--<div class="col-sm-3 attendencerightdiv fullwidsec">

    <div class="keysection">

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    <p><b>B</b> -  Educated off site (NOT dual registration)</p>

    </div>

    </div>-->

	</div>

    </div>

    </div>
    
    
    <script>
  jQuery(document).ready(function(){
	   $("#date").keydown(false);
	  
	  	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#loadstudentsForm")[0].reset();
			   window.location.href='<?php echo base_url()?>modifyattendance/index';
			}); 
			
			jQuery("#cancelBtn").click(function() {
	          jQuery("#updateattendanceForm")[0].reset();
window.location.href='<?php echo base_url()?>modifyattendance/index/?branch=<?php echo @$_GET['branch'];?>&class=<?php echo @$_GET['class'];?>&date=<?php echo @$_GET['date'];?>&session=<?php echo @$_GET['session']; ?>';
			}); 
			
	  jQuery("#branch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			// var BranchID =  jQuery('input[name=student_school_branch]:checked').val();
			jQuery("#class").find('option[value!=""]').remove();
			
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>modifyattendance/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#class').append(resp);
			          }
				   });
				 } else {
			     jQuery("#class").find('option[value!=""]').remove();
				 return false;
				 }
			 });
			 
	
    jQuery("#loadstudentsForm").validate({
        rules: {
              branch: {
                 required: true
                 },
				 
			  class: {
    					required: true
   					}, 
			
			  date: {
    					required: true
   					}, 
					
			  session: {
                required: true
                 }
			   
            },
        messages: {
              branch: {
                  required: "Please select branch."
                 },
			
			  class: {
    					required: "Please select class."
   					},
				
			  date: {
    					required: "Please select date."
   				 },
						 
			  session: {
                   required: "Please select session."
                 }
             },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
			 
 var validFrom=false;
	 jQuery("#updateattendanceForm").validate({
		ignore: [],
        rules: {
			'attendance_status[]': {
    					required: true
   					}
            },
        messages: {
			 'attendance_status[]': {
    						required: "Required field",
   					}
               },
			   
	   submitHandler: submitForm
      });
	  
	  function submitForm() {
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>modifyattendance/check_student_Attentance_status',
            data : jQuery("#updateattendanceForm").serialize(),
			dataType : "html",
            beforeSend: function()
            {
            },
            success :  function(data) {
				data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
					  validFrom=true;
					   jQuery('#updateattendanceForm').attr('action', '<?php echo base_url(); ?>modifyattendance/update_attendance');
                       document.getElementById("updateattendanceForm").submit();
					} else if(data.Status == 'false') {
						jQuery('#attendancestatusError').show();
					} 
             }
        });
        return false;
      }
			 
	});
</script>
    <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>

<script type="text/javascript">
 
$(document).ready(function(){
 
    $('[tool-tip-toggle="tooltip-demo"]').tooltip({
 
        placement : 'top'
 
    });
 
});
 
</script>
    <style>
.resselectbox {
    padding: 0 4px;
}

.radiobutton label{padding:0 20px 16px 37px;}
.radiobutton {
    float: left;
    position: relative;
    width: 50%;
}
.selectstyle{
	width: 60% !important;}
.branchstyle{
	 padding: 0 25px 0 0;
    width: 100% !important;
}
.radiobutton .check{top: 10px;}
.radiobutton label{margin:5px auto;}

 .beforeresult_section > h1 {
    min-height: 590px;
	}
	.btnstyle{ 
	background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0 !important ;
    border: 1px solid #249533 !important;
    border-radius: 3px  ;
    color: #fff !important;
    float: left !important;
    font-size: 14px !important;
    margin: 20px 0 !important;
    padding: 6px 20px !important;}

.tooltip-arrow {
    left: 60.5% !important;
}

.rightbtn {
    float: right;
    padding: 0 0 0 10px;
}
.errspan {
    padding: 10px 30px 0 10px;
}
.datepickdiv {
    padding: 0 0 0 3px;
}
.rightbtn {
    float: right;
    padding: 0 0 0 20px;
}

.dateinput input{width:100% !important;}
.fa-question-circle + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.fa.fa-question-circle{color:#090;}
	</style>