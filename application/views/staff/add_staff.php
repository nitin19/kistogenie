<div class="editprofile-content">
    	<div class="profilemenus">
        <ul>
		    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>
        <li><a href="<?php echo base_url(); ?>staff">Staff</a></li>
        <li class="edit">Add</li>        
        </ul>
        </div>
 <!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>
       <div class="col-sm-12 nopadding profile-bg accsection">
        <h1>Add Details</h1>
        </div>
        <form action="<?php echo base_url(); ?>staff/add_new_staff" method="post" name="staffprofileForm" id="staffprofileForm" enctype="multipart/form-data">
        <div class="col-sm-6 formleft">
        <div class="editprofileform editprofile_left">
   
        <div class="profile-bg prfltitle profile_titlebg">
         <h2>Personal Details</h2>
        <div class="col-sm-12 nopadding contentdiv">
        <div class="form-group">
    <label class="col-sm-3 control-label nopadding">First Name<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="staff_fname" id="staff_fname" placeholder="Enter your first name" onBlur="GetUsername(this.value)">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Last Name<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="staff_lname" id="staff_lname" placeholder="Enter your last name">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Username</label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" id="username" name="username" placeholder="" readonly="readonly">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Password</label>
    <div class="col-sm-9 inputbox">  
      <input name="password" class="form-control" id="password" placeholder="" readonly="readonly">
    </div>
  </div>
 <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Avtar<br/><span class="tagline">JPG, PNG</span></label>
    <div class="col-sm-9 profile-picture inputbox">
      <img src="<?php echo base_url();?>assets/images/avtar.png" id="ppp">
       <img src="" id="ppp1">
      <div class="profilepicbtns"><i class="fa fa-picture-o"></i>
      <input type="file" name="profile_image" id="profile_image" style="display:none;"/>
      </div>
    </div>
    </div>
  
  
   <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Title<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <select class="form-control" name="staff_title">
  <option value="">Select Title</option>
  <option value="ms.">Ms.</option>
  <option value="mr.">Mr.</option>
  <option value="mrs.">Mrs.</option>
 </select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Date of Birth<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="staff_dob" class="datepicker" placeholder="DD-MM-YYYY" id ="staff_dob">
    </div>
  </div>
<div class="form-group">
    <label class="col-sm-3 control-label nopadding">Email<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="email" class="form-control" id="email" name="email" placeholder="Enter your E-mail id">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Telephone<span>*</span></label>
    <div class="col-sm-9 inputbox">
     <input type="text" name="staff_telephone" id="telephon" class="form-control" placeholder="Enter your contact number"  maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Address<span>*</span></label>
    <div class="col-sm-9 inputbox">
     <input type="text" name="staff_address" id="address" class="form-control" placeholder="Enter your Address">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Address(Optional)</label>
    <div class="col-sm-9 inputbox">
     <input type="text" name="staff_address_1" id="staff_address_1" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Qualification<span>*</span></label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_education_qualification" id="staff_education_qualification" class="form-control">
    </div>
  </div>


    <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Existing Branch</label>
    <div class="col-sm-9 inputbox">
      <select class="form-control" name="exist_branch"  id="exist_branch" >   
             
  <option value="" disabled="" selected="">Select Branch </option>  
   <?php
     foreach($branch as $brnh) {
         ?>		
<option value="<?php echo $brnh['branch_id'];?>"><?php echo $brnh['branch_name'];?> </option>

	<?php } ?>				                  
    </select>
                    
    </div>
    </div>

    <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Account number</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_acc_no" id="staff_acc_no" class="form-control" onKeyUp="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Sort code</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_sort_code" id="staff_sort_code" class="form-control">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">National insurance</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_insurance" id="staff_insurance" class="form-control">
    </div>
  </div>

 <!-- <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Hourly rate</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_hourly_rate" id="staff_hourly_rate" class="form-control" onkeypress="return isNumberKey(event)">
    </div>
  </div> -->

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Personal Summary</label>
    <div class="col-sm-9 inputbox">
     <textarea class="form-control" name="staff_personal_summery" rows="1"></textarea><span class="fa fa-question-circle errspan errspan1"  tool-tip-toggle="tooltip-demo" data-original-title="Enter your previous experience/s or achievements,if any." ></span>
    </div>
  </div>
  </div>
	</div>	
        </div> 
       </div>
       
       <div class="col-sm-6 formright"> 
        <div class="editprofileform editprofile_right">
     
        <div class="profile-bg prfltitle profile_titlebg">
        <h2>Professional Details</h2>
		<div class="col-sm-12 nopadding contentdiv">
    <?php /*?>   
  
    <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Branch Name<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <select class="form-control" name="branch[]"  id="branch" multiple="multiple" required style="height:80px; !important;">          
    <!-- <option value="">Select Branch </option>  -->   
   <?php
     foreach($branch as $brnh) {
         ?>		
<option value="<?php echo $brnh['branch_id'];?>"><?php echo $brnh['branch_name'];?> </option>

	<?php } ?>				                  
    </select>
                    
    </div>
    </div>
    <div class="form-group multipleselbox">
    <label class="col-sm-3 control-label nopadding">Class Name<span>*</span></label>
    <div class="col-sm-9 inputbox" >
<select class="form-control" name="class_name[]" id="class_name" multiple="multiple" required style="height:80px; !important;">  
<!--<option value="">Select Class</option>  --> 

 </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>   
    </div>
    </div>
    <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Subject Name<span>*</span></label>
    <div class="col-sm-9 inputbox" id="subject_name">
    </div>
  </div>
  
  <?php */?>
  
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Teacher Type<span></span></label>
    <div class="col-sm-9 inputbox">
<select class="form-control" name="staff_teacher_type" id="teachtype"> 
<option value=""></option>
<?php	foreach($getteachertype as $teach_type)
		{
?>
		<option value="<?php echo $teach_type->id;?>"><?php echo $teach_type->teacher_type; ?></option>
          <?php }
?>     
 </select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Teacher Level<span></span></label>
    <div class="col-sm-9 inputbox">
<select class="form-control" name="staff_teacher_level" id="teachlevel">  
<option value=""></option>    
<?php foreach($getteacherlevel as $teach_level)
		{
?>
		<option value="<?php echo $teach_level->id;?>"><?php echo $teach_level->teacher_level;?></option>
      <?php   }
 ?>
 </select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Role at the School<span></span></label>
    <div class="col-sm-9 inputbox">
<select class="form-control" name="staff_role_at_school"  id="teachrole"> 
<option value=""></option> 
<?php	foreach($getteacherrole as $teach_role)
		{
		?>
        <option value="<?php echo $teach_role->id;?>"><?php echo $teach_role->teacher_role;?></option>
		<?php
			}
			?>    
</select>
  </div>
   </div>
  </div>
  </div>
  
        
        <div class="profile-bg prfltitle profile_titlebg">
        <h2>Access system</h2>
		<div class="col-sm-12 nopadding contentdiv">
       
    <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Branch Name</label>
    <div class="col-sm-9 inputbox">
      <select class="form-control" name="branch[]"  id="branch" multiple="multiple" style="height:80px; !important;">          
    <!-- <option value="">Select Branch </option>  -->   
   <?php
     foreach($branch as $brnh) {
         ?>		
<option value="<?php echo $brnh['branch_id'];?>"><?php echo $brnh['branch_name'];?> </option>

	<?php } ?>				                  
    </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>  
                    
    </div>
    </div>
  <div class="form-group multipleselbox">
    <label class="col-sm-3 control-label nopadding">Class Name</label>
    <div class="col-sm-9 inputbox" >
<select class="form-control" name="class_name[]" id="class_name" multiple="multiple" style="height:80px; !important;">  
<!--<option value="">Select Class</option>  --> 

 </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>   
    </div>
    </div>
    <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Subject Name</label>
    <div class="col-sm-9 inputbox" id="subject_name_old">
    
    <select class="form-control" name="subjectsname[]" id="subject_name" multiple="multiple" style="height:80px; !important;"> 
    </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>
    </div>
  </div>

     
     <?php /*?><div class="form-group">
 <?php $AccessmenuArr = array("Students","Student attendance","Documents","Progress reports","Configurations","Student enrolement","Staff enrolement","Staff attendence","Staff salary","Teaching & learing","Basic masters"); ?>
 
    <label class="col-sm-3 control-label nopadding">Access Menu</label>
    <div class="col-sm-9 inputbox">
         <?php foreach($AccessmenuArr as $Accessmenu) { ?>
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="access_menus[]" id="access_menu_<?php echo $Accessmenu;?>" value="<?php echo $Accessmenu;?>" >
    			<label for="access_menu_<?php echo $Accessmenu;?>"><span class="checkbox"><?php echo $Accessmenu;?></span></label>
                </div>
           <?php } ?>     
        </div>
     </div><?php */?>

    <div class="col-sm-12 nopadding">
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Students</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="students_menu" id="students_menu" value="Yes" >
    			<label for="students_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Student attendance</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="student_attendance_menu" id="student_attendance_menu" value="Yes" >
    			<label for="student_attendance_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     </div>
     
     <div class="col-sm-12 nopadding">
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Documents</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="documents_menu" id="documents_menu" value="Yes" >
    			<label for="documents_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Progress reports</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="progress_reports_menu" id="progress_reports_menu" value="Yes" >
    			<label for="progress_reports_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     </div>
     
     <div class="col-sm-12 nopadding">
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Configurations</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="configurations_menu" id="configurations_menu" value="Yes" >
    			<label for="configurations_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
   <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Student enrolment</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="student_enrolement_menu" id="student_enrolement_menu" value="Yes" >
    			<label for="student_enrolement_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     </div>
     
     <div class="col-sm-12 nopadding">
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Staff</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="staff_menu" id="staff_menu" value="Yes" >
    			<label for="staff_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Staff enrolment</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="staff_enrolement_menu" id="staff_enrolement_menu" value="Yes" >
    			<label for="staff_enrolement_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     
     </div>
          
     <div class="col-sm-12 nopadding">
     
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Staff attendance</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="staff_attendence_menu" id="staff_attendence_menu" value="Yes" >
    			<label for="staff_attendence_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Staff salary</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="staff_salary_menu" id="staff_salary_menu" value="Yes" >
    			<label for="staff_salary_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>

     </div>
     
     <div class="col-sm-12 nopadding">
          <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Teaching & learning</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="teaching_learing_menu" id="teaching_learing_menu" value="Yes" >
    			<label for="teaching_learing_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     <div class="col-sm-6 nopadding">
     <div class="form-group">
     
    <label class="col-sm-8 control-label nopadding">Basic Setup</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox">
			    <input type="checkbox" name="basic_masters_menu" id="basic_masters_menu" value="Yes" >
    			<label for="basic_masters_menu"><span class="checkbox"></span></label>
                </div>
        </div>
        </div>
     </div>
     </div>
     
     
     
     
     
  
  </div>
  </div>
  
  
        <div class="profile-bg prfltitle profile_titlebg">
        <h2>Salary</h2>
		<div class="col-sm-12 nopadding contentdiv">
  
      <div class="form-group">
    <label class="col-sm-2 control-label nopadding">Salary</label>
    <div class="col-sm-10 nopadding inputbox">
         <div class="col-sm-5 inputbox">
      <select class="form-control" name="salary_currency"  id="salary_currency">  
       <option value="">Select currency</option>   
      <?php  foreach($currencies as $currency) { ?> 
       <option value="<?php echo $currency->symbol;?>"><?php echo $currency->symbol;?></option>  
         <?php } ?>
        </select>
        </div>
        
         <div class="col-sm-3 nopadding inputbox">
    <input type="text" name="salary_amount" id="salary_amount" value="" placeholder="Salary" class="form-control" maxlength="10"  onkeypress="return isNumberKey(event)">
        </div>
        
         <div class="col-sm-4 inputbox">
      <select class="form-control" name="salary_mode"  id="salary_mode" > 
        <option value="">Select Mode</option>  
        <option value="Hourly">Hourly</option> 
		<option value="Weekly">Weekly</option>
        <option value="Monthly">Monthly</option>
      <!--  <option value="Half-Yearly">Half-Yearly</option>
        <option value="Yearly">Yearly</option> -->       
        </select>
        </div>
   
    
       </div>
  </div>
  
      <div class="form-group">
 <?php $weekdaysArr = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat"); ?>
    <label class="col-sm-3 control-label nopadding">Teaching Days</label>
    <div class="col-sm-9 nopadding inputbox">
         <?php foreach($weekdaysArr as $weekday) { ?>
		  <div class="checkboxdiv profilecheckbox checkbox_cls">
			    <input type="checkbox" name="teaching_days[]" id="week_day_<?php echo $weekday;?>" value="<?php echo $weekday;?>" >
    			<label for="week_day_<?php echo $weekday;?>"><span class="checkbox cls_day"><?php echo $weekday;?></span></label>
                </div>
           <?php } ?>     
        </div>
     </div>
  
  </div>
  </div>
  
         
 	    <div class="padd">
        <div class="col-sm-6 nopadding">
        <div class="cancellink">
        </div>
        </div>
        <div class="col-sm-6 nopadding">
        <div class="col-sm-12 confirmlink link">
         <input type="submit" name="Confirmbtn" class="btn btn-default" value="Submit">
        </div>
        </div>
        </div>
		
        </div>
        </div>
         </form>
	   	</div>
        
        
        
<script>
function GetUsername(x) {
	 if(x!='') {	
		$('#username').val('');
		$('#password').val('');
		var randomusername = Math.ceil(Math.random()*100)
		var randomPassword = Math.ceil(Math.random()*100000)
		$('#username').val(x + randomusername);
		$('#password').val(x + randomPassword);
	 }
	
}

function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57)) {
             return false;
      } else {
          return true;
      }
       }
</script>   

        
<script>
  jQuery(document).ready(function(){
	  
	  $("#staff_dob").keydown(false);
	  
	 // jQuery("#branch").on('change', function(e){ 
	   jQuery("#branch").on('click', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/getclass',
            data:{ 'branch_id': branch_id },
            success :  function(response) {
				        response=jQuery.parseJSON(response);
						jQuery('#class_name').empty();
					    jQuery('#class_name').html(response.datacls);
						jQuery('#subject_name').empty();
					    jQuery('#subject_name').html(response.datasub);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
		jQuery("#class_name").on('click', function(e){ 
   		  var class_id = jQuery(this).val();
		   var branch_id = jQuery('#branch').val();
		  if(branch_id!='' && class_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/get_subject',
            data:{ 'branch_id': branch_id, 'class_id':class_id},
            success :  function(resp) {
						jQuery('#subject_name').empty();
					    jQuery('#subject_name').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
//		  jQuery("#branch").on('change', function(e){ 
//   		  var branch_id = jQuery(this).val();
//		  if(branch_id!='') {
//			jQuery.ajax({
//            type : 'POST',
//            url  : '<?php echo base_url(); ?>staff/get_teacher_role',
//            data:{ 'branch_id': branch_id },
//            success :  function(resp) {
//						jQuery('#teachrole').empty();
//					    jQuery('#teachrole').html(resp);
//			          }
//				   });
//				 } else {
//				 return false;
//				 }
//			 });
//			 
//			 jQuery("#branch").on('change', function(e){ 
//   		  var branch_id = jQuery(this).val();
//		  if(branch_id!='') {
//			jQuery.ajax({
//            type : 'POST',
//            url  : '<?php echo base_url(); ?>staff/get_teacher_level',
//            data:{ 'branch_id': branch_id },
//            success :  function(resp) {
//						jQuery('#teachlevel').empty();
//					    jQuery('#teachlevel').html(resp);
//			          }
//				   });
//				 } else {
//				 return false;
//				 }
//			 });
//			 
//			 jQuery("#branch").on('change', function(e){ 
//   		  var branch_id = jQuery(this).val();
//		  if(branch_id!='') {
//			jQuery.ajax({
//            type : 'POST',
//            url  : '<?php echo base_url(); ?>staff/get_teacher_type',
//            data:{ 'branch_id': branch_id },
//            success :  function(resp) {
//						jQuery('#teachtype').empty();
//					    jQuery('#teachtype').html(resp);
//			          }
//				   });
//				 } else {
//				 return false;
//				 }
//			 });
//		
			 
	  	  
		   
	jQuery.validator.addMethod("alphaUname", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    });
	
	jQuery.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaLnumber", function(value, element) {
      return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaUpwd", function(value, element) {
    return this.optional(element) || value == value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/);
    });
	
	  // validate form on keyup and submit
    jQuery("#staffprofileForm").validate({
		ignore: [],
        rules: {
                username: {
                required: true,
          		minlength: 4,
          		maxlength: 40,
				 remote: {
             url: "<?php echo base_url(); ?>staff/check_username",
             type: "POST"
             }
               },
			   password: {
                required: true,
          		minlength: 6,
          		maxlength: 16
               },
			   /*'branch[]':{
                required: true
               },
			   'class_name[]':{
                required: true
               },
			   staff_teacher_level: {
                required: true
               },
			   staff_teacher_type: {
                required: true
               },
			  staff_role_at_school: {
                required: true
               },*/
			    staff_education_qualification: {
                required: true
               },
			   staff_title: {
                required: true
               },
			   staff_fname: {
                required: true,
				alphaLetter: true
               },
			   staff_lname: {
                required: true,
				alphaLetter: true
               },
			   staff_dob: {
                required: true
               },
			   email: {
                required: true,
		  		email: true,
		  		maxlength: 120,
				 remote: {
             url: "<?php echo base_url(); ?>staff/check_email",
             type: "POST"
             }
				
               },
			staff_telephone: {
				   required: true,
				   number:true,
                   minlength: 11,
				   maxlength: 11
               },
			 staff_address: {
                required: true
               },
			salary_amount: {
				number: true,
				maxlength: 9
               }
			 /*salary_currency: {
                required: true
               },
			 salary_amount: {
                required: true,
				number: true,
				maxlength: 9
               }, 
			 salary_mode: {
                required: true
               } */
        },
		
		errorElement: "span",
      errorClass: "help-inline-error",
	  
        messages: {
                username: {
                  required: "Please enter Username",
				  minlength: "Minimum 4 characters required.",
				  maxlength: "Maximum 40 characters allowed.",
				  remote: "User name already in use."
                 },
				  password: {
                  required: "Please Enter Password.",
				  minlength: "Minimum 7 characters required.",
				  maxlength: "Maximum 16 characters allowed."
                 },
				 /*'branch[]': {
                  required: "Please select School Branch",
                 },
				  'class_name[]': {
                  required: "Please select Class",
                 },
				 staff_teacher_level: {
                  required: "Please Select level",
                 },
				 staff_teacher_type: {
                  required: "Please select type",
                 },
				 staff_role_at_school: {
                  required: "Please select role",
                 },*/
				 staff_education_qualification: {
                  required: "Please select Qualification"
                 },
				 staff_title: {
                  required: "Please select Title"
                 },
				  staff_fname: {
                  required: "Please enter Firstname",
				  alphaLetter: "Letters only please."
                 },
				   staff_lname: {
                  required: "Please enter Lastname",
				  alphaLetter: "Letters only please."                 
				  },
				  staff_dob: {
                  required: "Please enter Date OF Birth"
                 },
				  email: {
                  required: "Please Enter Email.",
				  email: "Please enter a valid email address.",
				  maxlength: "Maximum 120 characters allowed.",
				  remote: "User Email already in use."
                 },
				staff_telephone: {
				  number: "Numbers only please.",
				  minlength: "Minimum 11 characters required.",
				  maxlength: "Maximum 11 characters allowed."
                 },
				staff_address:  {
                  required: "Please enter address"
                  },
				  salary_amount:  {
				   number: "Numbers only please.",
				   maxlength: "Maximum 9 characters allowed."
                  }
				/*salary_currency:  {
                  required: "Please select currency"
                  },
				salary_amount:  {
                   required: "Please enter salary",
				   number: "Numbers only please.",
				   maxlength: "Maximum 9 characters allowed."
                  },
			   salary_mode:  {
                  required: "Please select salary mode"
                  }*/
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		
	});
</script>

<script>
 $(document).ready( function() {
	$(".fa-picture-o").click(function () {
    $("input[type='file']").trigger('click');
   $("#ppp").hide();
   $("#ppp1").show();
  });
$("#ppp1").hide();

$('input[type="file"]').on('change', function() {
  var val = $(this).val();
  $(this).siblings('span').text(val);
})

$('input[type="file"]').on('change', function() {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#ppp1").css("background-image", "url("+this.result+")");
            }
        }
        });
 });
		</script>
   <script type="text/javascript">
 
$(document).ready(function(){
 
    $('[tool-tip-toggle="tooltip-demo"]').tooltip({
 
        placement : 'top'
 
    });
 
});
 
</script>  
    <SCRIPT language=Javascript>
   
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
   
    </SCRIPT>   
<style>
#ppp,#ppp1 {   
    background-position: center center;
    background-size: cover;
    -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
    display: inline-block;
}
.profilepicbtns {
  display: inline-flex;
}
i.fa-picture-o {
  cursor: pointer;
}
i:hover {
  opacity: 0.6;
}
#profile_image{
	border:0;
	padding:0;
	}
.cancellink > input {
   background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0 !important;
   color:#FFF;
}

.errspan {
    float: right;
    margin-right:-13px;
    margin-top: -80px;
    position: relative;
    /*z-index: 2;*/
    }
.errspan1{
	margin-top: -305px;
}
.fa-question-circle + .tooltip > .tooltip-inner {background-color:#090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.fa-question-circle{color:green;}
.editprofileform {
    border-radius: 4px;
    float: left;
    margin: 0 2% 0 0;
    width: 48%;
}
.profile_titlebg h2{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1); 
	padding: 10px 15px;
	margin:0px; }
.profiletitltbox{padding-top:0px;}
.profiletitltbox .profile_titlebg {
    padding: 0;
}
.contentdiv{
	padding-left:15px;
	padding-top:15px;

}
.profile-bg {
	padding: 0px;
}
.accsection h1 {
    padding: 0 0 10px 15px;
	color:#393C3E;
}

.formright{
	padding-right:0px;
}
.formleft{
	padding-left:0px;
}
.editprofile_left{width:100%;}
.editprofile_right{width:100%;}
.radio-btn{
    float: left;
    margin: 0 !important;
    width: 40px;
}
.radio-btn > input {
    float: left !important;
    height: auto !important;
}
.radio-btn > span {
    color: #000 !important;
    font-size: 13px !important;
    padding-left: 10px;
}
</style>
<style>
.profilecheckbox .checkbox.cls_day::before{
	left:-20px;
}
.profilecheckbox .checkbox.cls_day::after{
	left:-20px;
}
.checkboxdiv.checkbox_cls {
    padding: 0 0 0 18px;
}

.link input{
    font-size: 19px;
    margin: 0;
	width: 100%;
}
</style>