<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Progressreportarchive_model extends CI_Model {
	
	var $tbl	= "progress_report_archive";
	var $progress_report	= "progress_report";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $classes = "classes";
	var $term = "term";
	
	
    function __construct() {
        parent::__construct();
    }
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }  
	 
	 
 	function getclasses($school_id,$schoolbranch) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$schoolbranch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	  
   function getterms($school_id) {
        $this->db->select('*');
        $this->db->from($this->term);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		$this->db->last_query(); 
        return  $query->result();
    }
	
	
	function getyears($school_id) {
        $this->db->select('*');
		$this->db->group_by('progress_report_year');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }
	
	function getRows($where=NULL){
		$this->db->select('*');
		//$this->db->group_by('student_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$query = $this->db->get($this->tbl);
		 $this->db->last_query();
		return $query->num_rows();
	}
	  
	  function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL){
		$this->db->select('*');
		//$this->db->group_by('student_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by("archive_id", "desc"); 	
		$this->db->limit($limit, $start);
		$query = $this->db->get($this->tbl);
		 $this->db->last_query(); 
		return $query->result();
	  }
	  

      function delete($id){
		$where		= array('archive_id' => $id);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		 else
			return false;
	  }
  	 
}



 
