<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Modifyattendance_model extends CI_Model {
	
	var $tbl	= "attendance";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $classes = "classes";
	var $staff = "staff";
	
	
    function __construct() {
        parent::__construct();
    }
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }  
	 
	 
 	function getclasses($school_id,$schoolbranch) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$schoolbranch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	  
	function getRows($where=NULL){
		$this->db->select('*');
		if($where != NULL){
			$this->db->where($where);
		}
		$query = $this->db->get($this->tbl);
		 $this->db->last_query();
		return $query->num_rows();
	}
	  
	  function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL){
		$this->db->select('*');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by("attendance_id", "asc"); 	
		$this->db->limit($limit, $start);
		$query = $this->db->get($this->tbl);
		$this->db->last_query();
		return $query->result();
	  }
	   
	 
	  function getstudents($school_id,$schoolbranch,$classname) {
        $this->db->select('*');
        $this->db->from($this->student);
		$this->db->join('users', 'users.id = '.$this->student.'.user_id','INNER'); 
		$this->db->where(array($this->student.".school_id="=>$school_id,$this->student.".student_school_branch"=>$schoolbranch,$this->student.".student_class_group="=>$classname,"users.user_type="=>'student',"users.is_active="=>'1',"users.is_deleted="=>'0'));
		
		$this->db->order_by("users.id", "asc");
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
      }
	 
	 
	   
	  function  getstudentinfo($studentid) {
        $this->db->select('*');
        $this->db->from($this->student);
		$this->db->where('student_id', $studentid);
        $query = $this->db->get();
        return  $query->row();
	   }
	   
	   
	 function check_attendance($school_id,$branch_id,$class_id,$attendance_date,$attendance_session) {
		$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"attendance_date"=>$attendance_date,"attendance_session"=>$attendance_session,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
	   }
	   
	 
	 
	function updateAttendance($input){
		
    $school_id = $this->session->userdata('user_school_id');
	$branchid  = 	$input['branchid'];
	$classid  = 	$input['classid'];
	$attendancedate  = 	date('Y-m-d', strtotime($input['attendancedate']));
	$attendance_session  = 	$input['attendance_session'];
	$studentid  = 	$input['studentid'];
	$attendance_status  = 	$input['attendance_status'];
	$attendance_note  = 	$input['attendance_note'];
	
	$created_by = $this->session->userdata('user_id');
	$created_date = date('Y-m-d H:i:s');;
	$updated_date = date('Y-m-d H:i:s');
	$is_active = '1';
	
	$data =array();
	for ($i = 0; $i < count($studentid); $i++) {
		 $sv = $studentid[$i];
		 
		 $data[$i] = array(
           'school_id' => $school_id, 
           'branch_id' => $branchid,
           'class_id' => $classid,
           'student_id' => $sv,
		   'attendance_status' => $attendance_status[$sv], 
           'attendance_note' => $attendance_note[$sv],
           'attendance_date' => $attendancedate,
           'attendance_session' => $attendance_session,
		   'created_by' => $created_by, 
           'created_date' => $created_date,
           'updated_date' => $updated_date,
           'is_active' => $is_active,
          );
	}
	
	/*$this->db->where('school_id',$school_id);	
	$this->db->where('branch_id',$branchid);
	$this->db->where('class_id',$classid);
	$this->db->where('attendance_date',$attendancedate);
	$this->db->where('attendance_session',$attendance_session);*/
	
	$this->db->where(array('school_id' => $school_id, 'branch_id' => $branchid,'class_id' => $classid, 'attendance_date' => $attendancedate,'attendance_session' => $attendance_session));
	
		if($this->db->update_batch($this->tbl, $data, 'student_id')) {
		//echo $this->db->last_query();exit;
		return true;
		} else {
		//echo $this->db->last_query();exit;
		return false;
		     }
	  }
	  
	  
	  function insertAttendance($input){
		
    $school_id = $this->session->userdata('user_school_id');
			
	$branchid  = 	$input['branchid'];
	$classid  = 	$input['classid'];
	$attendancedate  = 	date('Y-m-d', strtotime($input['attendancedate']));
	$attendance_session  = 	$input['attendance_session'];
	$studentid  = 	$input['studentid'];
	$attendance_status  = 	$input['attendance_status'];
	$attendance_note  = 	$input['attendance_note'];
	
	$created_by = $this->session->userdata('user_id');
	$created_date = date('Y-m-d H:i:s');;
	$updated_date = date('Y-m-d H:i:s');
	$is_active = '1';
	
	$data =array();
	for ($i = 0; $i < count($studentid); $i++) {
		 $sv = $studentid[$i];
		 
		 $data[$i] = array(
           'school_id' => $school_id, 
           'branch_id' => $branchid,
           'class_id' => $classid,
           'student_id' => $sv,
		   'attendance_status' => $attendance_status[$sv], 
           'attendance_note' => $attendance_note[$sv],
           'attendance_date' => $attendancedate,
           'attendance_session' => $attendance_session,
		   'created_by' => $created_by, 
           'created_date' => $created_date,
           'updated_date' => $updated_date,
           'is_active' => $is_active,
          );
	}
		
		//$this->db->insert_batch($this->tbl, $data); 
		if($this->db->insert_batch($this->tbl, $data))
			return $this->db->insert_id();
		   else
			return false;
		 //echo $this->db->last_query();exit;
   }
   
   
   
  	 
}



 
