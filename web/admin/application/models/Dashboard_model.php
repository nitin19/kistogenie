<?php

class Dashboard_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
		
    }
     
    function view_student()
	{
	
		$this->db->select('*');
	    $this->db->from('regestration');	
	   
	    $query = $this->db->get()->result_array();   
		
		return $query;

	}
	
	function view_studentdetail($d1_id)
	{  
	    
	
		$this->db->select('*');
	    $this->db->from('regestration');	
	    $this->db->where('id',$d1_id);
	    $query2 = $this->db->get()->result_array();   
		
		return $query2;

	}
	
	function delete_student($std_id)
    {
        $this->db->where('id',$std_id);
        $this->db->delete('regestration');
    }
    
    function view_staff()
	{
	
		$this->db->select('*');
	    $this->db->from('staff');	
	   
	    $query3 = $this->db->get()->result_array();   
		
		return $query3;

	}
	
	function view_staffdetail($f1_id)
	{
	
		$this->db->select('*');
	    $this->db->from('staff');	
	    $this->db->where('id',$f1_id);
	    $query4 = $this->db->get()->result_array();   
		
		return $query4;

	}
	
	function delete_staff($stf_id)
    {
        $this->db->where('id',$stf_id);
        $this->db->delete('staff');
    }
    
}
?>