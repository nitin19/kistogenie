<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li><a href="#"> Attendance</a></li>

        <li class="edit"><a href="#">Attendance Archives</a></li>        

        </ul>

        </div>

        <!--<div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>-->

        </div>
        
         <div style="clear:both"></div>
        
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>
 

        <div class="attendancesec" style="width:100%">	

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

<form action="<?php echo base_url();?>attendancereportarchive/index" name="attendancereportarchiveForm" id="attendancereportarchiveForm">

    <div class="col-sm-12 col-xs-12 applycodediv">

        <div class="col-sm-4 nopadding tabfilterhalf">
                <div class="col-sm-2 col-xs-2 selectfilter datetext nopadding">
                <span>Date:</span>
                </div>
                <div class="col-sm-10 col-xs-10 nopadding datepickdiv">
                <div class="col-sm-5 col-xs-5 nopadding dateblk">
        		<div class="form-group">

    <div class="inputbox datepickerdiv nopadding">
      <input type="text" name="sdate" id="sdate" value="<?php echo @$sdate_search;?>" class="datepickerattendancereport1">
    </div>
  </div> 
  				</div>


<div class="col-sm-1 col-xs-1 dividerline nopadding"><span><img src="<?php echo base_url();?>assets/images/between.png"></span> </div>

                <div class="col-sm-5 col-xs-5 nopadding dateblk">
  				<div class="form-group">
    <div class="inputbox datepickerdiv nopadding">
      <input type="text" name="edate" id="edate" value="<?php echo @$edate_search;?>" class="datepickerattendancereport2">
    </div>
  </div>

  </div>

</div>

</div>


<div class="col-sm-8 nopadding selectoption tabfilterfull mobfullfilter">



   <div class="col-sm-4 nopadding resselectfil">
    <div class="form-group fullwidthinput">
    <label class="col-sm-3 control-label">Branch:</label>
    <div class="col-sm-9 inputbox nopadding">
      
       <select class="form-control" name="branch" id="branch">
      <option value="">Select Branch</option>   
    <?php foreach($branches as $branch) { ?>
     <option <?php if(@$branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
    <?php  } ?>
      </select>

    </div>

  </div>

</div>

   <div class="col-sm-4 nopadding selectoption resselectfil">
<div class="form-group fullwidthinput">

    <label class="col-sm-3 control-label">Class:</label>
    <div class="col-sm-9 inputbox nopadding">
      <select class="form-control" name="class" id="class">
         <option value="">Select Class</option>
         <?php 
  if( @$_GET['branch']!='') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = @$_GET['branch'];
		  $this->load->model(array('attendancereportarchive_model'));
		  $sclasses =$this->attendancereportarchive_model->getclasses($school_id,$schoolbranch);
     foreach($sclasses as $sclass) { ?>
          <option <?php if(@$class_search == $sclass->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>   
        </select>

    </div>

  </div>
</div>

   <div class="col-sm-4 rightspace selectoption resselectfil">
<div class="form-group generatereport">

    <input type="submit" class="btn btn-danger" value="View Report">
  <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>

  </div>

</div>

		</div>
        
        

     </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

<?php  $Totalrec = $total_rows; ?>

    <h1>Attendance Archives</h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">No.</th>

					  <th class="column16">Attendance Archive</th>

					  <th class="column2 actiondiv">Actions</th>

				  </tr>

			  </thead>

				<tbody>
                
                   <?php 
					  if(count(@$data_rows) > 0){
					      $sr=$last_page;
			 foreach($data_rows as $reports) { 
					     $sr++;
				     ?>

					<tr class="familydata reportarchives">

						<td class="column1"><?php echo $sr; ?></td>

						<td class="column16"><?php echo $reports->archive_filename; ?> </td>
                        


<!--<a href="<?php echo base_url();?>attendancereport/index?sdate=<?php echo date('d-m-Y', strtotime($reports->start_date));?>&edate=<?php echo  date('d-m-Y', strtotime($reports->end_date));?>&branch=<?php echo $reports->branch_id;?>&class=<?php echo $reports->class_id;?>" style="cursor: pointer;"  class="change">  <i class="fa fa-eye" data-toggle="tooltip" title="View Report" data-placement="top"></i></a>-->
<!--<a onclick="delConfirm(<?php echo $reports->archive_id;?>)" style="cursor: pointer;"  class="change">  <i class="fa fa-trash-o" data-toggle="tooltip" title="Delete" data-placement="top"></i></a>-->

			<td class="column2">
                            <a href="<?php echo base_url();?>attendancereportarchive/download_excel_backup/<?php echo $reports->archive_id;;?>" style="cursor: pointer;" class="change">  <i class="fa fa-download" data-toggle="tooltip" title="Download Report" data-placement="top"></i></a>
                            
                            <a onclick="delConfirm(<?php echo $reports->archive_id;?>)" style="cursor: pointer;"  class="change">  <i class="fa fa-trash-o" data-toggle="tooltip" title="Delete" data-placement="top"></i></a>

				</td>
                        </tr>

			  <?php

				}	} 
				else{ ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px; height:100px;font-size:25px; background:#FFF; color: #6a7a91; ">No record found.</th></tr>	
				   <?php } ?> 

				</tbody>

		  </table>
          

<div class="profile-bg profile-cls">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-8 footer">
    
    <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
			<?php  } ?>

	<ul class="pagination">
    
<?php echo $pagination;?>

	</ul>

    </div>

    <div class="col-sm-4 totaldiv nopadding">

    <div class="col-sm-8">

    <h3>Total Report Archives:<?php echo $Totalrec ;?>  </h3>

    </div>


    <div class="col-sm-4 col-xs-5 selectfilter paginationselbox nopadding">

      <span>Showing:</span>

 <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>attendancereportarchive">

<input type="hidden" name="sdate" id="sdate" value="<?php echo @$_GET['sdate'];?>" />
<input type="hidden" name="edate" id="edate" value="<?php echo @$_GET['edate'];?>" /> 
<input type="hidden" name="branch" id="branch" value="<?php echo @$_GET['branch'];?>" />
<input type="hidden" name="class" id="class" value="<?php echo @$_GET['class'];?>" />


  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>

  <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>
        
        

    </div>

	</div>

    </div>
          

          

  </div>





	</div>

    

	</div>

    </div>

    </div>
    
     <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>attendancereportarchive/delete/"+id;
		}else{
			return false;
		}
	}
</script>
    
     <script>
  jQuery(document).ready(function(){
	  
	    jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
	  
	  	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#attendancereportarchiveForm")[0].reset();
			   window.location.href='<?php echo base_url()?>attendancereportarchive/index';
			}); 
			
	jQuery("#branch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			jQuery("#class").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>attendancereportarchive/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#class').append(resp);
			          }
				   });
				 } else {
				 jQuery("#class").find('option[value!=""]').remove();
				 return false;
				 }
			 });
			 
	 });
		 
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<style>
.footer {
    padding: 25px 0 0;
}
.applycodediv {
    padding-left: 0;
    padding-right: 0;
}
.selectfilter > span {
    padding: 6px 10px 0 0 !important;
}

.change .fa-eye:hover {
  color: #57cdf7;
}
.change .fa-trash-o:hover{
  color: #57cdf7;
}
.fa-trash-o + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-trash-o+ .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

.fa-eye + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-eye+ .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

.profile-bg.profile-cls{
	padding:0px 20px 0px;
}


</style>