<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Student_enrol extends CI_Controller
{
	function __construct(){
       
      parent::__construct();        
		$this->load->database();
		$this->load->library('image_lib');
		$this->load->model("Registration_model");  
    }
	public function index()
	{

		$this->load->view('student_enrol');
		
	}

	function generate_random_username(){

    	$characters = 'abcdefghijklmnopqrstuvwxyz';

    	$string = '';

    	for ($i = 0; $i < 6; $i++) {

        	$string .= $characters[rand(0, strlen($characters) - 1)];

    	}
    	return $string;
	}

	function generate_random_password(){

        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';

        $string = '';

        for ($i = 0; $i < 8; $i++) {

            $string .= $characters[rand(0, strlen($characters) - 1)];

        }
        return $string;

    }

	public function insert_student()
	{
	  
	  $this->load->model("Registration_model");           
             

        $random_username = $this->generate_random_username();
        while(sizeof($this->Registration_model->check_registred_username("olivetreestudy")) == '0')
        {
        	$random_username = $this->generate_random_username();
        }
        $random_password = $this->generate_random_password();

        $get_school_id = $this->Registration_model->get_School_ID_from_branch($this->input->post('branch'));




        $created_date = date("Y-m-d H:i:s");
		$school_id = $get_school_id->school_id;
		$username = $random_username;
		$password = $random_password;
		$email = $this->input->post('email');


        $user_data['school_id'] = $school_id;
        $user_data['username'] = $username;
        $user_data['password'] = $password;
        $user_data['email'] = $email;
        $user_data['profile_image'] = "";
        $user_data['user_type'] = "enrolementstudent";
        $user_data['is_active'] = 1;
        $user_data['is_deleted'] = 0;
        $user_data['total_login'] = 0;
        //$user_data['last_login']
        $user_data['created_by'] = "";
        $user_data['created_date'] = $created_date;
        //$user_data['updated_date']
        //$user_data['notification_checked_at']

        $insert_user_data = $this->Registration_model->add_user($user_data);

		if($insert_user_data){ 

			$student_data['school_id'] = $school_id;
			$student_data['user_id'] = $insert_user_data;
			$student_data['student_fname'] = $this->input->post('child_first');
			$student_data['student_lname'] = $this->input->post('child_last');
			$student_data['student_gender'] = $this->input->post('gender');
			$student_data['student_dob'] = '';
			$student_data['student_age'] = $this->input->post('age');
			$student_data['student_nationality'] = '';
			$student_data['student_country_of_birth'] = '';
			$student_data['student_first_language'] = '';
			$student_data['student_other_language'] = '';
			$student_data['student_religion'] = '';
			$student_data['student_ethnic_origin'] = '';
			$student_data['student_telephone'] = $this->input->post('phone');
			$student_data['student_address'] = '';
			$student_data['student_address_1'] = '';
			$student_data['student_father_name'] = $this->input->post('father');
			$student_data['student_father_occupation'] = '';
			$student_data['student_father_mobile'] = '';
			$student_data['student_father_email'] = '';
			$student_data['student_father_address'] = '';
			$student_data['student_mother_name'] = $this->input->post('mother');
			$student_data['student_mother_occupation'] = '';
			$student_data['student_mother_mobile'] = '';
			$student_data['student_mother_email'] = '';
			$student_data['student_mother_address'] = '';
			$student_data['student_emergency_name'] = '';
			$student_data['student_emergency_relationship'] = '';
			$student_data['student_emergency_mobile'] = '';
			$student_data['student_doctor_name'] = '';
			$student_data['student_doctor_mobile'] = '';
			$student_data['student_doctor_surgery_address'] = '';
			$student_data['student_helth_notes'] = '';
			$student_data['student_allergies'] = '';
			$student_data['student_other_comments'] = '';
			$student_data['student_school_branch'] = $this->input->post('branch');;
			$student_data['student_class_group'] = '';
			$student_data['student_application_date'] = date("Y-m-d");
			$student_data['student_application_comment'] = '';
			//$student_data['student_enrolment_date'] = $school_id;
			//$student_data['student_leaving_date'] = $school_id;
			$student_data['student_fee_band'] = '';
			$student_data['student_certificates'] = '';			
			$student_data['student_family_address'] = '';
			$student_data['student_sibling'] = '';
			$student_data['card_no'] = '';
			$student_data['card_exp'] = '';
			$student_data['student_enrolement_status'] = 'applied';
			//$student_data['student_postpone_date'] = $school_id;
			$student_data['student_notes'] = '';
			$student_data['notified'] = 1;

	        $insert_student_data = $this->Registration_model->add_student($student_data);
	        if($insert_student_data)
	        {

	        	$branchname = $this->Registration_model->getbranchName($this->input->post('branch'));
	      
	        	$to = $email;
	        	

				$email_message = '<p>Assalamualaikum</p>

									<p><em>We&nbsp;pray this email reaches you in the best state of health and imaan.</em></p>

									<p>&nbsp;</p>

									<p>Dear Brother<em> <strong>'.$this->input->post('father').'</strong></em></p>

									<p>We are delighted to receive your online registration form for your <strong>'.$this->input->post('child_first') .' '.$this->input->post('child_last').'</strong> at Olive Tree Study, we are sure <strong>'.$this->input->post('child_first').'</strong> will benefit, learn and enjoy our school. We are currently reviewing your application for a free taster session at our '.$branchname->branch_name.'.  As soon as we have a slot available we will contact you with the details. Insha\'Allah.</p>

									<p>Mean while, join our thousands of passionate followers on social media to see what is going on live in our school on a regular basis. Don\'t forget to browse round our friendly Website www.olivetreestudy.co.uk where you will find lots of useful information for you and your family to benefit from.</p>

									<p>We will like to thank you in advance for your patience and taking the time out to explore and inquire about Olive Tree Study.</p>

									<p>Jazak\'Allah Hu Khairan</p>

									<p><img src="https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif" style="height:8px; width:20px" /></p>

									<p>&nbsp;</p>

									<p><strong>Wa Alikum AsSalam</strong></p>

									<p>&nbsp;</p>

									<p>&nbsp;</p>

									<p><strong>Olive Tree Study Support</strong></p>

									<p>Web:&nbsp;<strong><a href="http://olivetreestudy.co.uk/" target="_blank">olivetreestudy.co.uk</a></strong></p>

									<p><strong><a href="https://www.facebook.com/olivetree.studysupport" target="_blank">Facebook</a>&nbsp;|&nbsp;<a href="https://www.facebook.com/olivetree.studysupport" target="_blank">Twitter</a>&nbsp;|&nbsp;<a href="https://www.youtube.com/watch?v=Dn5wE1pYUdA" target="_blank">YouTube</a>&nbsp;</strong></p>

									<p>&nbsp;</p>';

	        	$subject = "Induction confirmation";
	        	$mail= $this->send_email($to,$email_message,$subject);

	        	$get_school_admin = $this->Registration_model->get_school_admin($school_id);

	        	$notification_data['school_id'] = $school_id;
	        	$notification_data['branch_id'] = $this->input->post('branch');
	        	$notification_data['from'] = $insert_user_data;
	        	$notification_data['to'] = $get_school_admin->id;
	        	$notification_data['subject'] = 'admission';
	        	$notification_data['message'] = 'new admission';
	        	$notification_data['attachments'] = '';
	        	$notification_data['is_active'] = 1;
	        	$notification_data['is_deleted'] = 0;
	        	$notification_data['created_date'] = $created_date;
	        	$notification_data['read_by'] = '';
	        	$notification_data['from_web'] = 1;
	        	$notification_data['applied_by'] = 'student';

	        	$insert_notification_data = $this->Registration_model->insert_notification_data($notification_data);
	        	$this->session->set_flashdata('success', 'Enrolment for student has been done successfully.');
				redirect(base_url()."Student_enrol"); exit;
	        }
	        else
	        {
	        	$this->session->set_flashdata('error', 'Some problem exists. Enrolment can not be done.');
				redirect(base_url()."Student_enrol");exit;
	        }			
		}
		else
		{
			$this->session->set_flashdata('error', 'Some problem exists. Enrolment can not be done.');
			redirect(base_url()."Student_enrol");exit;
		}

	}

	function send_email($to,$message,$subject){

        
            $Olivetree = 'Olivetree';
            $from        = "binarydata2015@gmail.com";

            $headers     = 'MIME-Version: 1.0' . "\r\n";

            $headers    .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers    .= 'To: <'.$to.'>' . "\r\n";

            $headers    .= 'From: '.$Olivetree.'<'.$from.'>' . "\r\n";

            if($_SERVER['HTTP_HOST']!="localhost")

                mail($to, $subject, $message, $headers);

            return true;

    }




}

?>