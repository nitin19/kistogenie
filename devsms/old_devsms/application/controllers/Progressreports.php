<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Progressreports extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','progressreport_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }

	public function index(){
		$school_id		= $this->session->userdata('user_school_id');
		$data['terms'] = $this->progressreport_model->get_terms($school_id);
		$this->load->view('header');
		$this->load->view('progressreport/progress_reports', $data);
		$this->load->view('footer');

	}

}

