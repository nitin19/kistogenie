<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Attendancereportarchive_model extends CI_Model {
	
	var $tbl	= "attendance_archive";
	var $attendance	= "attendance";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $classes = "classes";
	var $term = "term";
	
	
    function __construct() {
        parent::__construct();
    }
	
	function getstaff_branches($staff_branch_ids) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id"=>$staff_branch_ids,"is_active"=>'1',"is_deleted"=>'0'));
        $query = $this->db->get();
      //  echo $this->db->last_query();
        return  $query->row();
    }
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }  
	 
	 
 	function getclasses($school_id,$schoolbranch) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$schoolbranch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	  
	
	function getRows($where=NULL,$startdate,$enddate){
		$this->db->select('*');
		if($where != NULL){
			$this->db->where($where);
		}
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where('start_date >=', $startdate);
			$this->db->where('end_date <=', $enddate);
		 }
		$query = $this->db->get($this->tbl);
		 $this->db->last_query();
		return $query->num_rows();
	}
	  
	  function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$startdate,$enddate){
		$this->db->select('*');
		if($where != NULL){
			$this->db->where($where);
		}
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where('start_date >=', $startdate);
			$this->db->where('end_date <=', $enddate);
		  }
		$this->db->order_by("archive_id", "desc"); 	
		$this->db->limit($limit, $start);
		$query = $this->db->get($this->tbl);
		 $this->db->last_query(); 
		return $query->result();
	  }
	  

      function delete($id){
		$where		= array('archive_id' => $id);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		 else
			return false;
	  }
  	 function filename($id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("archive_id="=>$id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
}



 
