<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Messagetemplate extends CI_Controller {
	
	var $logmode;
	function __construct(){
       
      parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','Messagetemplate_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }

	

	public function index(){

		
		$data = array();
		$user_id						= $this->session->userdata('user_id');
		$status_search					= isset($_GET['status'])?$_GET['status']:NULL; 
		$word_search				    = isset($_GET['seachword'])?trim($_GET['seachword']):NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;

		$search_condition['is_deleted']	= '0';
		
		$school_id						= $this->session->userdata('user_school_id');
		
		$data['title']	      			= "Message Template";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage					= $PerPage;
		} else {
			$perpage					= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'messagetemplate/index';
	    $config['total_rows'] 			= $this->Messagetemplate_model->getRows($search_condition,$school_id,$word_search);

	   	$config['per_page'] 			= $perpage;
        $config['postfix_string'] 		= "/?status=$status_search&seachword=$word_search&perpage=$perpage"; 
		
		$this->pagination->initialize($config);

		$odr 							= "message_id";
		$dirc							= "desc";
		
		$data_rows						= $this->Messagetemplate_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$word_search);

		$messageid 						= $this->uri->segment(4);

		if($messageid!='') {
		  $template_data_rows			= $this->Messagetemplate_model->get_single_template($messageid);
		  $data['info']				    = $template_data_rows[0];
		 }

		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['status_search']			= $status_search;
		$data['word_search']		    = $word_search;
		$data['PerPage']				= $PerPage;

		$data['PerPage']				= $PerPage;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');


		$this->load->view('header');
		$this->load->view('master/message_template',$data);
		$this->load->view('footer');
	}

	 public function addtemplate(){
		 
		    $msgdata 				= array();
			$school_id 					= $this->session->userdata('user_school_id');
			
		    $msgdata['school_id'] 	= $this->session->userdata('user_school_id');
			$msgdata['created_by'] 	= $this->session->userdata('user_name');
			$msgdata['updated_by'] 	= $this->session->userdata('user_name');
			$msgdata['is_active'] 	= '1';
			$msgdata['is_deleted'] 	= '0';
			$msgdata['created_date'] = date('Y-m-d');
			$msgdata['updated_date'] = date('Y-m-d');
			
			
			if (trim($this->input->post('user_type')) != '') {
                $msgdata['user_type'] = trim($this->input->post('user_type'));
				
            }

			if (trim($this->input->post('msgtitle')) != '') {
                $msgdata['title'] = trim($this->input->post('msgtitle'));
				
            }
			
			if (trim($this->input->post('msgdesc')) != '') {
                $msgdata['description'] = trim($this->input->post('msgdesc'));
            }

		   if($this->Messagetemplate_model->insertMessage($msgdata)) {
			   $this->session->set_flashdata('success', 'Message has been added successfully.');
			   redirect(base_url()."messagetemplate/index"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Message has not been added.');
				redirect(base_url()."messagetemplate/index");exit;
		      }
		
			
        }

		public function updatetemplate(){
	
			
			  $start						    = $this->uri->segment(3);
		      $editedTemplateid					= $this->uri->segment(4);
			  $Action							= $this->uri->segment(5);  
	
		  $postURL	= $this->createPostURL($_GET);
		if($editedTemplateid==NULL){
			$this->session->set_flashdata('error', 'Select the Template first.');
			redirect(base_url()."messagetemplate");exit;
		   }
		   
		 $reportdata 					= array();
         $school_id 					= $this->session->userdata('user_school_id');
		 $where							= array('message_id'=>$editedTemplateid);
		 $data['title']	      			= "Edit Template";
		 $data['mode']					= "Edit";
			
			if (trim($this->input->post('msgtitle')) != '') {
                $msgdata['title'] = trim($this->input->post('msgtitle'));
				
            }
            
			if (trim($this->input->post('msgdesc')) != '') {
                $msgdata['description'] = trim($this->input->post('msgdesc'));
            }

            if (trim($this->input->post('user_type')) != '') {
                $msgdata['user_type'] = trim($this->input->post('user_type'));
				
            }
	     
			$msgdata['updated_by'] 	= $this->session->userdata('user_name');
			$msgdata['updated_date'] = date('Y-m-d');
			
		   if($this->Messagetemplate_model->updateTemplates($msgdata,$where)) {
			   $this->session->set_flashdata('success', 'Message has been updated successfully.');
			   redirect(base_url()."messagetemplate/index/$start/$editedTemplateid/edit/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Message has not been updated.');
				redirect(base_url()."messagetemplate/index/$start/$editedTemplateid/edit/");exit;
		      }
	   }
	 
	    

	
		public function deletetemplate($messageid){
			
			 $school_id = $this->session->userdata('user_school_id');

			
		 		 if($this->Messagetemplate_model->delete($messageid)){
					$this->session->set_flashdata('success', 'Message has been deleted successfully.');
					redirect(base_url()."messagetemplate/index");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."messagetemplate/index");exit;
		          }

	    }



	
		function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}
}

