    <div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

        <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="#">Dashboard</a></li>        

        </ul>

        </div>

<!--        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="quickaction" value="Quick actions">

        </div>-->

        </div>

    <div class="col-sm-3 leftspace attendancesec">

        <img class="img-responsive" src="<?php echo base_url();?>assets/images/attendance1.jpg">

        <img class="img-responsive" src="<?php echo base_url();?>assets/images/attendance2.jpg">

        <img class="img-responsive" src="<?php echo base_url();?>assets/images/attendane3.jpg">

        <img class="img-responsive" src="<?php echo base_url();?>assets/images/attendance4.jpg">

        </div>

    <div class="col-sm-9 rightspace fullwidsec">

        <div class="col-sm-12 tablediv dashboardtablediv nopadding">

    <div class="col-sm-12 dashboardtit nopadding">

    <div class="col-sm-8 col-xs-7 dashboardleft nopadding">

    <h1>Communication<span>7</span></h1>

    </div>

        <div class="col-sm-4 col-xs-5 communicationicons nopadding">

    <ul class="folderdropdown">

    <li class="dropdown">

          <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-folder-o"></i> <img src="<?php echo base_url();?>assets/images/dropdownicon.png"></a>

          <ul class="dropdown-menu">

            <li><a href="#">Action</a></li>

            <li><a href="#">Another action</a></li>

          </ul>

        </li>

    <li><i class="fa fa-cog"></i></li>

    </ul>

 </div>

    </div>

    <div class="tablewrapper">

    <table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column11">Title</th>

					  <th class="column3">Date</th>

					  <th class="column3">Status</th>

					  <th class="column3">Actions</th>

				  </tr>

			  </thead>

				<tbody>

					<tr class="familydata communication">

						<td class="column11"><div class="col-sm-2 col-xs-2 nopadding imgsec"><img src="<?php echo base_url();?>assets/images/defaultprofilepic.png"></div><div class="col-sm-10 col-xs-10 contentsec"><p>Today's Attendance</p><span>Nullam quis risus eget urna mollis ornare vel eu leo</span></div></td>

						<td class="column3 spacing">07 May 2016</td>

						<td class="column3 spacing"><span class="statusbtn acceptbtn">Actions</span></td>

                        <td class="column3 spacing actionicons"><i class="fa fa-eye"></i><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

					<tr class="familydata communication">

						<td class="column11"><div class="col-sm-2 col-xs-2 nopadding imgsec"><img src="<?php echo base_url();?>assets/images/defaultprofilepic.png"></div><div class="col-sm-10 col-xs-10 contentsec"><p>Today's Attendance</p><span>Nullam quis risus eget urna mollis ornare vel eu leo</span></div></td>

						<td class="column3 spacing">07 May 2016</td>

						<td class="column3 spacing"><span class="statusbtn activebtn">Activity Feed</span></td>

                        <td class="column3 spacing actionicons"><i class="fa fa-eye"></i><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    

                    <tr class="familydata communication">

						<td class="column11"><div class="col-sm-2 col-xs-2 nopadding imgsec"><img src="<?php echo base_url();?>assets/images/defaultprofilepic.png"></div><div class="col-sm-10 col-xs-10 contentsec"><p>Today's Attendance</p><span>Nullam quis risus eget urna mollis ornare vel eu leo</span></div></td>

						<td class="column3 spacing">07 May 2016</td>

						<td class="column3 spacing"><span class="statusbtn requestbtn">Letters</span></td>

                        <td class="column3 spacing actionicons"><i class="fa fa-eye"></i><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    <tr class="familydata summary">

						<td class="column11"><div class="col-sm-2 col-xs-2 nopadding imgsec"><img src="<?php echo base_url();?>assets/images/defaultprofilepic.png"></div><div class="col-sm-10 col-xs-10 contentsec"><p>Today's Attendance</p><span>Nullam quis risus eget urna mollis ornare vel eu leo</span></div></td>

						<td class="column3 spacing">07 May 2016</td>

						<td class="column3 spacing"><span class="statusbtn acceptbtn suspendbtn">Announcements</span></td>

                        <td class="column3 spacing actionicons"><i class="fa fa-eye"></i><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    <tr class="familydata communication">

						<td class="column11"><div class="col-sm-2 col-xs-2 nopadding imgsec"><img src="<?php echo base_url();?>assets/images/defaultprofilepic.png"></div><div class="col-sm-10 col-xs-10 contentsec"><p>Today's Attendance</p><span>Nullam quis risus eget urna mollis ornare vel eu leo</span></div></td>

						<td class="column3 spacing">07 May 2016</td>

						<td class="column3 spacing"><span class="statusbtn acceptbtn">Actions</span></td>

                        <td class="column3 spacing actionicons"><i class="fa fa-eye"></i><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    <tr class="familydata communication">

						<td class="column11"><div class="col-sm-2 col-xs-2 nopadding imgsec"><img src="<?php echo base_url();?>assets/images/defaultprofilepic.png"></div><div class="col-sm-10 col-xs-10 contentsec"><p>Today's Attendance</p><span>Nullam quis risus eget urna mollis ornare vel eu leo</span></div></td>

						<td class="column3 spacing">07 May 2016</td>

						<td class="column3 spacing"><span class="statusbtn emailbtn">Emails</span></td>

                        <td class="column3 spacing actionicons"><i class="fa fa-eye"></i><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                        <tr class="familydata communication">

						<td class="column11"><div class="col-sm-2 col-xs-2 nopadding imgsec"><img src="<?php echo base_url();?>assets/images/defaultprofilepic.png"></div><div class="col-sm-10 col-xs-10 contentsec"><p>Today's Attendance</p><span>Nullam quis risus eget urna mollis ornare vel eu leo</span></div></td>

						<td class="column3 spacing">07 May 2016</td>

						<td class="column3 spacing"><span class="statusbtn acceptbtn suspendbtn">Announcements</span></td>

                        <td class="column3 spacing actionicons"><i class="fa fa-eye"></i><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                        <tr class="familydata communication">

						<td class="column11"><div class="col-sm-2 col-xs-2 nopadding imgsec"><img src="<?php echo base_url();?>assets/images/defaultprofilepic.png"></div><div class="col-sm-10 col-xs-10 contentsec"><p>Today's Attendance</p><span>Nullam quis risus eget urna mollis ornare vel eu leo</span></div></td>

						<td class="column3 spacing">07 May 2016</td>

						<td class="column3 spacing"><span class="statusbtn acceptbtn suspendbtn">Announcements</span></td>

                        <td class="column3 spacing actionicons"><i class="fa fa-eye"></i><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

				</tbody>

		  </table>   

</div>

	</div>

    <div class="col-sm-12 nopadding behaviourstatus">

    <img class="img-responsive" src="<?php echo base_url();?>assets/images/behaviourimg.jpg">

    </div>

	</div>

    <div class="client_views">

    <div class="col-sm-12 nopadding viewsdiv">

        <div class="col-sm-8 leftspace fullwidsec">

        <div class="testimonials">

        <div class="col-sm-12 testimonialtitdiv nopadding">

        <div class="col-sm-8 col-xs-8">

        <h1>Parents say about Olive Tree Study</h1>

        </div>

        <div class="col-sm-4 col-xs-4 arrowsdiv">

        <div class="leftarrow slidearrow">

          <a class="" href="#carousel-example-generic" role="button" data-slide="prev">

   <i class="fa fa-angle-left"></i></span>

  </a>

  </div>

  <div class="rightarrow slidearrow">

  <a class="" href="#carousel-example-generic" role="button" data-slide="next">

    <i class="fa fa-angle-right"></i></span>

  </a>

  </div>

        </div>

        </div>

        <div id="carousel-example-generic" class="carousel slide testimonialslide" data-ride="carousel">

  <!-- Indicators -->





  <!-- Wrapper for slides -->

  <div class="carousel-inner" role="listbox">

    <div class="item active clientviews">

    <div class="col-sm-12 slideblk">

      <div class="col-sm-2 col-xs-3 clientimg">

      <img src="<?php echo base_url();?>assets/images/clientimg.jpg">

      </div>

      <div class="col-sm-10 col-xs-9 clientview nopadding">

      <h1>Tobias Ali</h1>

      <p>Nullam quis risus eget urna mollis ornare vel eu leo Nullam quis risus eget urna mollis ornare vel eu leoNullam quis risus eget urna mollis ornare vel eu leo eget urna mollis ornare vel eu leoeget urna mollis ornare vel eu leoeget urna mollis ornare vel eu leo</p>

      </div>

      </div>

      <div class="col-sm-12 slideblk">

      <div class="col-sm-2 col-xs-3 clientimg">

      <img src="<?php echo base_url();?>assets/images/clientimg.jpg">

      </div>

      <div class="col-sm-10 col-xs-9 clientview nopadding">

      <h1>Tobias Ali</h1>

      <p>Nullam quis risus eget urna mollis ornare vel eu leo Nullam quis risus eget urna mollis ornare vel eu leoNullam quis risus eget urna mollis ornare vel eu leo eget urna mollis ornare vel eu leoeget urna mollis ornare vel eu leoeget urna mollis ornare vel eu leo</p>

      </div>

      </div>

    </div>

    <div class="item clientviews">

    <div class="col-sm-12 slideblk">

      <div class="col-sm-2 col-xs-3 clientimg">

      <img src="<?php echo base_url();?>assets/images/clientimg.jpg">

      </div>

      <div class="col-sm-10 col-xs-9 clientview nopadding">

      <h1>Tobias Ali</h1>

      <p>Nullam quis risus eget urna mollis ornare vel eu leo Nullam quis risus eget urna mollis ornare vel eu leoNullam quis risus eget urna mollis ornare vel eu leo eget urna mollis ornare vel eu leoeget urna mollis ornare vel eu leoeget urna mollis ornare vel eu leo</p>

      </div>

      </div>

      <div class="col-sm-12 slideblk">

      <div class="col-sm-2 col-xs-3 clientimg">

      <img src="<?php echo base_url();?>assets/images/clientimg.jpg">

      </div>

      <div class="col-sm-10 col-xs-9 clientview nopadding">

      <h1>Tobias Ali</h1>

      <p>Nullam quis risus eget urna mollis ornare vel eu leo Nullam quis risus eget urna mollis ornare vel eu leoNullam quis risus eget urna mollis ornare vel eu leo eget urna mollis ornare vel eu leoeget urna mollis ornare vel eu leoeget urna mollis ornare vel eu leo</p>

      </div>

      </div>

    </div>



  </div>



  <!-- Controls -->



</div>

</div>

        </div>

        <div class="col-sm-4 nopadding fullwidsec">

        <div class="profile-bg videobg">

        <div class="video-parts">

		<iframe width="100%" height="250" src="https://www.youtube.com/embed/FtS4Kw_EtzU" frameborder="0" allowfullscreen></iframe>

        <div class="videocontent">

        <h1>Welcome from the Headmaster</h1>

        <span>Name Headmaster</span>

        </div>

		</div>

        </div>

        </div>

        </div>

        </div>

    <div class="col-sm-12 nopadding">

    <div class="col-sm-6 leftspace fullwidsec">

        <div class="profilecompletion">

        <div class="col-sm-12 prfltitlediv nopadding">

        <div class="col-sm-11 col-xs-10 prfltitle">

        <h1>Your Profile Completed</h1>

        </div>

        <div class="col-sm-1 col-xs-2 settingicon">

        <i class="fa fa-cog"></i>

        </div>

        </div>

        <div class="col-sm-12 processcompleted nopadding">

        <div class="profilecompleted profile-bg">

        <img src="<?php echo base_url();?>assets/images/profilecompleteding.jpg" class="img-responsive">

        </div>

        </div>     

        </div>

        </div>

    <div class="col-sm-6 rightspace studentsec fullwidsec">

        <div class="profilecompletion">

        <div class="col-sm-12 prfltitlediv nopadding">

        <div class="col-sm-11 col-xs-10 prfltitle">

        <h1>The number of new students</h1>

        </div>

        <div class="col-sm-1 col-xs-2 settingicon">

        <i class="fa fa-cog"></i>

        </div>

        </div>

        <div class="col-sm-12 processcompleted nopadding">

        <div class="loadingbars profile-bg">

        <div class="col-sm-8 border-right graph">

        <img src="<?php echo base_url();?>assets/images/loadingbars.jpg" class="img-responsive">

        </div>

        <div class="col-sm-4 nopadding">

        <div class="monthdate borderbottom">

        <h1>24</h1>

        <span>For This month</span>

        </div>

        <div class="monthdate">

        <h1>24</h1>

        <span>For This month</span>

        </div>

        </div>

        </div>

        </div>

        </div>

        </div>

        <div class="col-sm-12 nopadding infosec fullwidsec">

        <div class="col-sm-6 leftspace socialicons fullwidsec">

        <div class="col-sm-6 leftspace socialicon halfwidsec">

        <div class="facebookicon">

        <i class="fa fa-facebook"></i>

        <h3>15k <span>Likes</span></h3>

        </div>

        </div>

        <div class="col-sm-6 rightspace socialicon halfwidsec">

        <div class="twittericon">

        <i class="fa fa-twitter"></i>

        <h3>15k <span>Likes</span></h3>

        </div>

        

        </div>

        </div>

        <div class="col-sm-6 rightspace weatherinfoblk fullwidsec">

        <div class="profilecompletion weathersec">

        <div class="col-sm-12 prfltitlediv nopadding">

        <div class="col-sm-9 col-xs-9 prfltitle weathertit">

        <img class="cloudicon" src="<?php echo base_url();?>assets/images/clouds.png">

        <h1>37°</h1>

        <span>FRIDAY 13 th AUG</span>

        </div>

        <div class="col-sm-3 col-xs-3 locationicon">

        <i class="fa fa-map-marker"></i><span>Location</span>

        </div>

        </div>

        <div class="col-sm-12 processcompleted nopadding">

        <div class="weatherinfo profile-bg">

        <div class="col-sm-3 col-xs-3 weatherblk">

        <h1>Cloudy</h1>

        <img src="<?php echo base_url();?>assets/images/cloundicon.png">

        <h2>20°</h2>

        <span>August 16 Monday</span>

        </div>

        <div class="col-sm-3 col-xs-3 weatherblk">

        <h1>Sunny</h1>

        <img src="<?php echo base_url();?>assets/images/sunicon.png">

        <h2>20°</h2>

        <span>August 16 Monday</span>

        </div>

        <div class="col-sm-3 col-xs-3 weatherblk">

        <h1>Rainy</h1>

        <img src="<?php echo base_url();?>assets/images/rainy.png">

        <h2>20°</h2>

        <span>August 16 Monday</span>

        </div>

        <div class="col-sm-3 col-xs-3 weatherblk">

        <h1>Snowy</h1>

        <img src="<?php echo base_url();?>assets/images/snowy.png">

        <h2>20°</h2>

        <span>August 16 Monday</span>

        </div>

        </div>

        </div>     

        </div>

        </div>

        </div>

        

        </div>

    </div>

