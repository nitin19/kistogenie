<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teacherlearning_model extends CI_Model {
	var $tbl	= "teacher_learning";
	var $teacher_learning_attachments = "teacher_learning_attachments";
	var $users = "users";
	
    function __construct() {
        parent::__construct();
    }
	
	
	function getallFolders($school_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"parent_teacher_learning="=>'0',"is_active="=>'1',"is_deleted="=>'0'));
		$this->db->order_by("teacher_learning_id", "desc");
        $query = $this->db->get();
        return  $query->result();
    }
	
	 function chk_folder_name($foldername,$school_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"parent_teacher_learning="=>'0',"is_active="=>'1',"is_deleted="=>'0','teacher_learning_name'=> $foldername));
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
        return  $query->result();
     }
	 
	 	function insertfolder($folderdata){
		if($this->db->insert($this->tbl, $folderdata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 
	 function getSubfolders($school_id,$parent_folder_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"parent_teacher_learning="=>$parent_folder_id,"is_active="=>'1',"is_deleted="=>'0'));
		$this->db->order_by("teacher_learning_id", "desc");
        $query = $this->db->get();
        return  $query->result();
    }
	
		  function deletefolder($id, $data){
$this->db->where('teacher_learning_id', $id);
$result = $this->db->update($this->tbl, $data);
return $result;
}

}
