<div class="editprofile-content">
    	<div class="profilemenus">
        
    <div class="col-sm-10 menubaritems nopadding">
        <ul>
		<li><a href="#">Home</a></li>
        <li class="edit"><a href="#"> Staff</a></li>        
        </ul>
     </div>
     
     <div class="col-sm-2">
     
<?php

function generate_random_username(){

		$characters = 'abcdefghijklmnopqrstuvwxyz';

		$string = '';

		for ($i = 0; $i < 6; $i++) {

			$string .= $characters[rand(0, strlen($characters) - 1)];

		}

		return $string;

	}
$inviteusername = generate_random_username();
	
function generate_random_password(){

		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';

		$string = '';

		for ($i = 0; $i < 8; $i++) {

			$string .= $characters[rand(0, strlen($characters) - 1)];

		}

		return $string;

	}
	
$invitepassword = generate_random_password();


?>

<input type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" value="Invite Staff" style="padding: 4px 20px;">  


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:0px;">
          <div class="col-sm-12 col-xs-11 nopadding invitemember">
          <div class="col-sm-9"><h3 class="modal-title inviteheader">Invite staff member</h3></div>
           <div class="col-sm-3">
           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>
           </div>
          </div>
        </div>
        <!---------error message ------------------>
        <div style="clear:both"></div>
       <div class="alert alert-danger alert-dismissable" id="invitationError" style="display:none" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			Some problem exists. Invitation has not been sent. 
		</div>
        
         <div class="alert alert-danger alert-dismissable" id="invitationAlreadyError" style="display:none" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			You have already invited this staff member.
		</div>
    
<!---------error message end------------------>
<!---------success message ------------------>
         
      <div class="alert alert-success alert-dismissable" id="invitationSuccess" style="display:none">
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		    Invitation has been sent successfully. 
             </div>

 <div style="clear:both"></div>         
<!---------success message end------------------>
        <div class="modal-body">
        <form action="<?php echo base_url();?>schooltermreport/insert_archive_data" name="invitationForm" id="invitationForm">
            <input type="hidden" name="invited_user_name" id="invited_user_name" value="<?php echo $inviteusername;?>" >
            <input type="hidden" name="invited_user_pwd" id="invited_user_pwd" value="<?php echo  $invitepassword;?>">
          <div class="col-sm-12 popup_section_form">
          
          <div id="Loadingmsg"></div>
          
              <div class="col-sm-10 nopadding text_box_sec">
                 <input type="text" name="invited_user_email" id="invited_user_email" placeholder="Enter email address" maxlength="121" required />
             </div>
             <div class="col-sm-2 nopadding text_box_btn">
                <input value="Send" class="popup_save_btn" id="save" name="save" type="submit">
             </div>
          </div>
          </form>
        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>
      
    </div>
  </div>
  
  
     </div>
     
        
        </div>
         <!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>
        <div class="attendancesec">	
    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">
    	<form action="<?php echo base_url();?>staff/index" name="stafffilterform" id="stafffilterform">
    		<div class=" col-sm-12 search nopadding" id="srchh">
             <div class="col-sm-6 nopadding">Staff Overview</div>
             <div class="col-sm-5 nopadding srchstaff">
            <div class="col-sm-6 col-xs-5 searching termsearch nopadding">

        <div class="form-group staffsrch">

      <input type="text" class="form-control searchbox" placeholder="Name, Email or Username" name="user_srch" id="username"  value="">


     </div>

        </div>
    <div class="col-sm-6 col-xs-7 rightspace viewreport">

		<div class="form-group">

    <input type="submit" class="btn btn-danger" value="Find Staff">

    <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>

  </div>

</div>
   </div>			
</div>
</form>
    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">No.</th>

					  <th class="column4">Name</th>
                      
                      <th class="column4">Branch</th>

					  <th class="column5">Email</th>

                      <th class="column3">Contact</th>

                                     
                       <th class="column3">Options</th>
                       
                        <!--<th class="column1">Edit</th>
                        <th class="column1">View</th>-->

				  </tr>

			  </thead>

				<tbody>
<?php
if(count($staffdata)>0){
	$sr=$last_page;
	$comnCls = '';
        foreach ( $staffdata as $datastaff )
        {    
	     if((trim($datastaff->staff_fname)=='' && trim($datastaff->staff_fname==''))) {
				      $comnCls = 'red_line';
			      } elseif(trim($datastaff->branch_id)=='0' || trim($datastaff->class_name)=='' || trim($datastaff->subject_name)=='' || trim($datastaff->staff_teacher_type)=='' || trim($datastaff->staff_teacher_level)=='' || trim($datastaff->staff_role_at_school)=='') {
					  $comnCls = 'yellow_line';
				  } else {
					   $comnCls = 'green_line';
				     }
            ?>
					<tr class="familydata staff">
					 
                    <td class="column1"><?php echo ++$sr; ?></td>

<td class="column4 <?php echo $comnCls; ?>"><?php if($datastaff->staff_fname!='' && $datastaff->staff_fname!='') { echo $datastaff->staff_fname.' '.$datastaff->staff_lname; } else { echo '<span class="blank_trans_data">-</span>'; } ?></td>
                        
                        <td class="column4 <?php //echo $comnCls; ?>"><?php 
						$branch_id = $datastaff->branch_id;
        $this->load->model(array('staff_model'));
        $branchName = $this->staff_model->getbranchName($branch_id);
			if($branch_id!='0' && $branch_id!='') { echo @$branchName->branch_name; }  else { echo '<span class="blank_trans_data">-</span>'; }
?></td>

						<td class="column5 <?php if(($datastaff->staff_fname)=='') {echo $comnCls;} ?>"><?php echo $datastaff->email?></td>

						<td class="column3 <?php //echo $comnCls; ?>"><?php if($datastaff->staff_telephone!='') { echo $datastaff->staff_telephone; } else { echo '<span class="blank_trans_data">-</span>'; }?> </td>

                     <!--   <td class="column3 <?php //echo $comnCls; ?>"><?php echo $datastaff->user_type; ?></td>-->
         
                        <td class="column3">
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat">Action</button>
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>

                                    <a href="<?php echo base_url() ."staff/edit_staff/".$datastaff->user_id ?>"> Edit </a>
                                    </li>
                                    <li>
                                    <a href="<?php echo base_url() . "staff/view_user/". $datastaff->user_id ?>"> View </a>

                                    </li>
									<li>
									<?php 
									$staff_id = $datastaff->user_id;
                                    $this->load->model(array('staff_model'));
                                    $singletype = $this->staff_model->getdatastaff($staff_id);

										if($singletype->is_active == "1"){ 
										?>
										<a href="<?php echo base_url()."staff/act_inact/".$datastaff->user_id?>/inactive" title="click to deactivate staff"> Active </a>
										<?php
										} else {
										?>
										<a href="<?php echo base_url()."staff/act_inact/".$datastaff->user_id?>/active" title="click to activate staff">De-Activated </a>
										<?php } ?>
									</li>
													<li class="divider"></li>
									<li><a onclick="delConfirm(<?php echo $datastaff->user_id; ?>)" style="cursor: pointer;"> Delete </a> </li>
												</ul>
											</div>
										</td>
           </tr>
	 <?php } 
					} else { ?>
                     <tr><th colspan="7" style="text-align: center; width:1215px;height:100px; background:#FFF; color: #6a7a91;;font-size:25px;">No record to show.</th></tr>
                   <?php } ?>
		</tbody>

		  </table>
          <div class="box-footer clearfix profile-bg">
          	
          <div class="col-sm-4"  style="margin:20px 0px;">
							<? 
							if(count($staffdata)>0){
								$last_page1=$last_page;
								?>
								Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
								<?
							}
							?>
                            </div>
                            <div class="col-sm-5 ">
							<ul class="pagination pagination-sm no-margin ">
								<?php echo $links;?>
							</ul>
                            </div>
                 <div class="col-sm-3 col-xs-6 totaldiv nopadding">           
    		<div class="col-sm-12 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>
                             <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>staff" method="post">
                            <select class="form-control" name="perpage" id="perpage">

  <option >Select</option>
  
   <!--<option <?php//if($per_page == "10"){ echo 'selected'; } ?> value="10">10</option>-->
   
   <!--<option <?php//if($per_page == "10"){ echo 'selected'; } ?> value="10">10</option>-->

  <option <?php if($per_page == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option <?php if($per_page == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option <?php if($per_page == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option <?php if($per_page == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>
 </form>
                            </div>
						</div>
</div>
</div>

	</div>
	</div>

    </div>
    
</div> 
    </div>
    
<script type="text/javascript"> 
	jQuery("#perpage").on('change', function(e){ 
		jQuery('#perPageForm').submit();
}); 
	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#stafffilterform")[0].reset();
			   window.location.href='<?php echo base_url()?>staff/index';
			}); 
</script> 
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url() ;?>staff/act_inact/"+id+"/delete";
		}else{
			return false;
		}
}
</script>	
    <script>
  jQuery(document).ready(function(){
		
   jQuery("#invitationForm").validate({
        rules: {
			  invited_user_email: {
                required: true,
				email: true,
				maxlength: 120
                 }
            },
        messages: {
			  invited_user_email: {
                   required: "Please enter email address.",
				   email: "Please enter a valid email address.",
				   maxlength: "Maximum 120 characters are allowed."
                 }
             },
       
       /* submitHandler: function(form) {
            form.submit();
          }*/
		 submitHandler: submitForm
        });
		
	  	function submitForm() {
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/Invitestaff',
            data : jQuery("#invitationForm").serialize(),
			dataType : "html",
           beforeSend: function()
            {
				jQuery("#Loadingmsg").html('<img src="<?php echo base_url();?>assets/images/ajax-loader.gif"> loading...');
            },
                 success :  function(data) {
				    data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
					  jQuery("#Loadingmsg").hide();
					  jQuery('#invitationError').hide();
					  jQuery('#invitationAlreadyError').hide();
					  jQuery("#invitationForm")[0].reset();
					   jQuery('#invitationSuccess').show();
					   
			            setTimeout(profilesteps, 6000);
    			         function profilesteps() {
							 jQuery('#invitationSuccess').hide();
							 jQuery('#myModal').modal('hide');
							 window.location.href = '<?php echo base_url();?>staff';
					       }
					   
					} else if(data.Status == 'false') {
						jQuery("#Loadingmsg").hide();
						jQuery('#invitationSuccess').hide();
						jQuery('#invitationAlreadyError').hide();
						jQuery('#invitationError').show();
					} else if(data.Status == 'alreadyexist') {
						jQuery("#Loadingmsg").hide();
						jQuery('#invitationSuccess').hide();
						jQuery('#invitationError').hide();
						jQuery('#invitationAlreadyError').show();
					} 
             }
        });
        return false;
      }
			 
	 });
		 
</script>
    
    <style>
	#srchh{
	 margin-bottom: 10px;
    padding: 5px;
	background-color:#fff;
	height:55px;
	}
	#search {
    background-color: #33ab44;
    color: #fff;
    margin: 0 0 5px;
    width: 100%;
}
	#srchh > form {
    float: right;
    padding: 5px;
}

*::after, *::before {
    box-sizing: border-box;
}
*::after, *::before {
    box-sizing: border-box;
}
#srchh > div {
    float: left;
    font-size: 25px;
    padding-left: 10px;
	padding-top:7px;
}
.btn-info {
    background-color: rgb(55, 177, 72);
	border-color:  rgb(55, 177, 72);
}
#perpage {
    float: right;
}
.attendancesec{width:100%;}
.select_box select {
    -moz-appearance: none;
    background-image: url("../images/dropdownicon.png"), linear-gradient(#ffffff, #f3f5f8);
    background-position: 90% center;
    background-repeat: no-repeat;
    border: 1px solid #dfe3e9;
    box-shadow: none;
    height: 30px;
    padding: 0 3px;
    width: 70px;
}

.form-group .staffsrch{
    width: 80% !important;
}
.srchstaff{
	margin:0 0 0 80px;
}
@media screen and (min-width:320px) and (max-width:383px){

#srchh {
    height: 140px !important;
}	
#username {
    border-radius: 4px;
    margin-bottom: 10px;
    width: 100% !important;
}
#srchh > form {
    float: left;
    width: 100%;
}
.input-group {
    width: 100%;
}
}
@media screen and (min-width:384px) and (max-width:480px){
#srchh {
    height: 140px !important;
}
#username {
    border-radius: 4px;
    margin-bottom: 10px;
    width: 100% !important;
}
#srchh > form {
    float: left;
	width: 100%;
}
.input-group {
    width: 100%;
}
}
	</style>
    
<style>

#myModal { margin-top:140px; } 

.save_btn > input {
    background: #37b248 none repeat scroll 0 0;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #fff;
    font-size: 14px;
    padding: 7px 20px;
}
.save_btn {
    padding-top: 20px;
}
.popup_save_btn {
    background: #37b248 none repeat scroll 0 0;
    border: medium none;
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    color: #fff;
    padding: 12px 25px;
}
.modal-body {
    padding: 35px 0 70px;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-radius: 3px;
    min-height: 45px;
    width: 100%;
	padding: 10px;
}
.btn.btn-default {
    background: #37b248 none repeat scroll 0 0;
    color: #fff;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
    min-height: 44px;
    padding: 10px;
    width: 100%;
}
.save_btn {
    text-align: right;
}
.text_box_btn {
    text-align: left;
}
.modal-header {
    text-align: center;
}

.red_line { color:#F00 !important; }
.yellow_line { color:#990 !important; }
.blank_trans_data {
    color: transparent;
}
.search {
    padding: 10px 0 60px !important;
}
.invitemember {
    background: #37b248 none repeat scroll 0 0;
    color: #fff;
    padding-bottom: 5px;
    padding-top: 5px;
}

.close.closenotify{
   background: #fff none repeat scroll 0 0 !important;
    border-radius: 40px;
    float: right;
    height: 40px;
    opacity: 1;
    width: 40px;
	color:#36b047;
}
.inviteheader{
	float:right;
	padding-right:35px;
}
#perpage{
	margin-right:35px;
}
</style>
