<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>

<style>

.blk-frmt {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #57b94a;
    box-shadow: 2px 2px 4px 4px #eaeaea;
    margin-top: 30px;
    padding: 20px 10px;
    text-align: center;
    width: 100%;
}
.blk-frmt a {
    text-decoration: none;
}


</style>

<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>

        <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li class="edit"><a href="#"> Help and Knowledgebase </a></li>        

        </ul>

        </div>

 

   <div class="attendancesec">	

      <div class="col-sm-12 tablediv nopadding">

         <div class="col-sm-12 nopadding">
		 
         <div class="col-sm-4"><div class="blk-frmt"><h2> <a href="<?php echo base_url();?>Loginmodule">Login Module</a></div> </h2></div>
         <div class="col-sm-4"><div class="blk-frmt"><h2> <a href="<?php echo base_url();?>helpdashboard">Dashboard</a></div> </h2></div>
         <div class="col-sm-4 "><div class="blk-frmt"><h2>  <a href="<?php echo base_url();?>helpstudentenrolement">Student Enrolement</a></div> </h2> </div>
         <div class="col-sm-4"><div class="blk-frmt"><h2> <a href="<?php echo base_url();?>helpstudents">Students</a> </h2></div> </div>
		
		
		
         <div class="col-sm-4 "><div class="blk-frmt"> <h2> <a href="<?php echo base_url();?>helpstudentattendance">Student Attendance</a> </h2></div></div>
         <div class="col-sm-4 "><div class="blk-frmt"><h2>  <a href="<?php echo base_url();?>helpstudentprogressreports">Student Progress Reports</a> </h2></div> </div>
         <div class="col-sm-4 "><div class="blk-frmt"><h2> <a href="<?php echo base_url();?>helpstaffenrolement">Staff Enrolement</a> </h2></div> </div>
		
		
		

         <div class="col-sm-4 "> <div class="blk-frmt"><h2> <a href="<?php echo base_url();?>helpstaff">Staff</a> </h2></div></div>
         <div class="col-sm-4 "> <div class="blk-frmt"><h2> <a href="<?php echo base_url();?>Helpdocuments">Documents</a> </h2></div></div>
         <div class="col-sm-4 "><div class="blk-frmt"><h2>  <a href="<?php echo base_url();?>helpuseraccess">User Access</a> </h2> </div></div>
         <div class="col-sm-4 "><div class="blk-frmt"><h2> <a href="<?php echo base_url();?>helpteacherlearning">Teacher Learning</a> </h2></div> </div>
		

		
         <div class="col-sm-4 "> <div class="blk-frmt"><h2> <a href="<?php echo base_url();?>helpconfiguration">Configuration</a> </h2></div></div>
         <div class="col-sm-4 "><div class="blk-frmt"><h2>  <a href="<?php echo base_url();?>helpbasicsetup">Basic Setup</a> </h2></div> </div>
       <!--  <div class="col-sm-4 "><div class="blk-frmt"><h2> <a href="<?php echo base_url();?>helpgenerateattendance">Generate Attendance</a> </h2> </div></div>-->

         </div> 

           

	   </div>
    
	 </div>

    </div>
