<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>School Management</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/table-css.css" rel="stylesheet">
    
    <link href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet">
    
 <link href="<?php echo base_url();?>assets/css/jquery-ui.css" rel="stylesheet">
<!--   <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
   <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->

    

    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->

  <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
  
    <!--<script src="<?php //echo base_url();?>assets/js/jquery.validate.js"></script>-->
    
    <script src="<?php echo base_url();?>assets/js/jquery-validate.bootstrap-tooltip.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
     <script src="<?php echo base_url();?>assets/js/canvasjs.min.js"></script>
     
   
    
   <!-- <script src="<?php echo base_url();?>assets/js/jquery.validationEngine-en.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.validationEngine.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/validationEngine.jquery.css" type="text/css"/>-->

  </head>

  <body>





	<div class="editdetailpage">
    <div class="col-sm-12 detailright enrollmentrightsection nopadding">

    <div class="whitebg">

    <div class="col-sm-5 headerleft">
<?php
 $userid		= $this->session->userdata('user_id');
 $user_school_id = $this->session->userdata('user_school_id');
?>
  <h1 id="schoolName"></h1>
  
       <script>
 jQuery(document).ready(function(){
	  var schoolId = <?php echo $user_school_id;?>;
	   jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>schools/get_school_info',
			dataType: 'html',
            data: { 'schoolId': schoolId },
            success :  function(resp) {
				        jQuery('#schoolName').html(resp);
			          }
				   });
	      }); 
 </script>

    </div>

    <div class="col-sm-7 nopadding headerright">

    <ul class="dashboardright">
<?php  $logmode		= $this->session->userdata('log_mode');  ?>
    <li class="searchbarbox">

 <!--   <form class="navbar-form searchbar navbar-left">

     <div class="form-group">

     <input type="text" class="form-control" placeholder="Search">

     </div>

     <input type="button" value="" class="submitbtn">

     </form>-->

     </li>



<!--     <li class="notification"><img src="<?php //echo base_url();?>assets/images/notification-icon.png">

     <span class="counter">5</span></li>-->

     <li class="dropdown userdropdown">

          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php  
		  echo $this->session->userdata('user_name'); ?> <img src="<?php echo base_url();?>assets/images/dropdownicon.png"></a>

          <ul class="dropdown-menu">
           <?php  
		if ($logmode == 'teacher'){
		?>  
             <li><a href="<?php echo base_url();?>staff/view_user/<?php echo $userid; ?>">Profile</a></li>
             <li><a href="<?php echo base_url();?>login/lockscreen">Lock Screen</a></li>
             <li><a href="#">Membership</a></li>
            <?php } 
		if ($logmode == 'student'){
		?>  
             <li><a href="<?php echo base_url();?>students/view_student/<?php echo $userid; ?>">Profile</a></li>
             <li><a href="<?php echo base_url();?>login/lockscreen">Lock Screen</a></li>
             <li><a href="#">Membership</a></li>
            <?php } 
			
			if ($logmode == 'admin'){
		?>  
             <li><a href="<?php echo base_url();?>admindashboard">Profile</a></li>
             <li><a href="<?php echo base_url();?>login/lockscreen">Lock Screen</a></li>
            <li><a href="#">Membership</a></li>
            <?php }  
			if ($logmode == 'enrolementstudent'){
			?>
             <li><a href="<?php echo base_url();?>login/lockscreen">Lock Screen</a></li>
            <?php } ?>

            <li role="separator" class="divider"></li>
            <li><a href="<?php echo base_url();?>logout">Logout</a></li>

          </ul>

        </li>

        <li class="profilepic">
        
        <?php 
		if ($logmode == 'teacher'){
		if($userimage!=''){
		?>
		<img src="<?php echo base_url();?>uploads/staff/<?php echo $userimage;?>" alt="noimage">
        <?php
		}else{
		?>
        <img src="<?php echo base_url();?>assets/images/avtar.png" alt="noimage" >
        <?php } }?>
        
        
		<?php
		if ($logmode == 'student'){  
			if($userimage!=''){
		?>
        		
		<img src="<?php echo base_url();?>uploads/student/<?php echo $userimage;?>" alt="noimage">
        <?php
		}else{
		?>
        <img src="<?php echo base_url();?>assets/images/avtar.png" alt="noimage" >
        <?php }
		 }?>
         
         
         <?php
		//if ($logmode == 'enrolementstudent'){  
			//if($userimage!=''){
		?>
		<!--<img src="<?php echo base_url();?>uploads/student/<?php echo $userimage;?>" alt="noimage">-->
        <?php
		//}else{
		?>
       <!-- <img src="<?php echo base_url();?>assets/images/avtar.png" alt="noimage" >-->
        <?php // }
		 //}?>
        
      <!--  <img src="<?php //echo base_url();?>assets/images/profileimg.jpg">-->
        </li>

     

     </ul>

    </div>

    </div>

  <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<style>
.fa-cog + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-cog+ .tooltip > .tooltip-arrow {border-top-color:#090 !important;} 
.fa-question-circle + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-question-circle+ .tooltip > .tooltip-arrow {border-top-color:#090 !important;} 
</style>
  