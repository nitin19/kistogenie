<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="#">Schools</a></li>        

        </ul>

        </div>

        <!--<div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>-->

        </div>

    	
 <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>
        

        <div class="attendancesec">	

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

  <form action="<?php echo base_url();?>schools/index" name="schoolSearchForm" id="schoolSearchForm">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding">

				<div class="col-sm-7 nopadding selectoption">

<div class="form-group fullwidthinput">

    <label class="col-sm-1 col-xs-1 control-label nopadding filterlabel">Show</label>

    <div class="col-sm-11 col-xs-11 nopadding selectfilter">

    <div class="col-sm-3 col-xs-3 inputbox termselect">

<select class="form-control" name="status" id="status">
  <option value="">Select Status </option>    
   <option <?php if($status_search == "1"){ echo 'selected'; } ?> value="1">Active</option>
   <option <?php if($status_search == "0"){ echo 'selected'; } ?> value="0">Inactive</option>
</select>

    </div>

    <div class="col-sm-3 col-xs-3 inputbox termselect">

      <select class="form-control" name="shortbyname" id="shortbyname">
   <option value="">Sort By</option>    
   <option  <?php if($shortbyname_search == "0"){ echo 'selected'; } ?> value="0">Select Name A-Z</option>    
   <option  <?php if($shortbyname_search == "1"){ echo 'selected'; } ?> value="1">Select Name Z-A</option>    
</select>

    </div>

    </div>

  </div>

</div>		

<div class="col-sm-2 col-xs-5 searching termsearch nopadding">

        <div class="form-group">

      <input type="text" class="form-control searchbox" placeholder="Search By Name,Email,Phone" name="seachword" id="seachword" value="<?php echo $word_search;?>">

   <!--  <input type="submit" value="" class="searchbtn">-->

     </div>

        </div>

        <div class="col-sm-3 col-xs-7 rightspace viewreport">

<div class="form-group">

    <input type="submit" class="btn btn-danger" value="Find School">

    <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>

  </div>

</div>

        </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <h1>Schools<span><?php echo $Totalrec = $total_rows;?></span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>
				  <tr class="headings">
					  <th class="column2">No.</th>
					  <th class="column4"> Name</th>
					  <th class="column4">Email</th>
                      <th class="column4">Phone</th>
                      <th class="column3">Status</th>
                      <th class="column3">Action</th>
				  </tr>

			  </thead>
				<tbody>
					<?php 
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $school) { 
					     $sr++;
					 ?>
					<tr class="familydata students">
						<td class="column2"><?php echo $sr; ?></td>
						<td class="column4"><?php echo $school->school_name;?></td>
                        <td class="column4"><?php echo $school->school_email;?> </td>
						<td class="column4"><?php echo $school->school_phone;?></td>
                        <td class="column3"><span class="acceptbtn statusbtn <?php if($school->is_active==1) { echo 'Active'; } else { echo 'Inactive'; } ?>"><?php if($school->is_active==1) { echo 'Active'; } else { echo 'Inactive'; } ?></span></td>
                        
                        
                        <td class="column3">
											<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
                                                
            <li> <a href="<?php echo base_url();?>schools/view_school/<?php echo $school->school_id;?>"> View  </a> </li>                                    
	        <li><a href="<?=base_url();?>schools/editschool/<?php echo $school->school_id;?>"> Edit </a>  </li>
          
				<li>
				<?php if($school->is_active == '1'){ ?>
	<a href="<?php echo base_url();?>schools/deactivate/<?php echo $last_page;?>/<?php echo $school->school_id;?>" title="click to deactivate school"> Active </a>
					<?php
					 } else {
					?>
	<a href="<?=base_url();?>schools/activate/<?php echo $last_page;?>/<?php echo $school->school_id;?>" title="click to activate school">De-Activated </a>
				<?php } ?>
				</li>
			<li class="divider"></li>
			 <li><a onclick="delConfirm(<?php echo $school->school_id;?>)" style="cursor: pointer;"> Delete </a> </li>
									</ul>
								</div>
											
							</td>
                                        
                                        
                        </tr>
				<?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px;height:100px; background:#FFF;">No record to show.</th></tr>	
				   <?php } ?>

				</tbody>

		  </table>

     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-9 col-xs-6 paginationblk">
         <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
			<?php  } ?>
	<ul class="pagination">
		
		<?php echo $pagination;?>
        

	</ul>

    </div>

    <div class="col-sm-3 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total Schools:<?php echo $Totalrec ;?></h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

<form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>schools/index" method="post">

  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>

  <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>

    </div>

	</div>

    </div>     

</div>





	</div>

    

	</div>

    </div>

    </div>
    
        
 <script type="text/javascript">
  
  jQuery(document).ready(function(){
	  
			  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
					
	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#schoolSearchForm")[0].reset();
			   window.location.href='<?php echo base_url()?>schools';
			});   
			
		}); 
		
 </script>	
 
  
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>schools/deleteschool/"+id;
		}else{
			return false;
		}
	}
</script>	

<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px;
}
.cleanSearchFilter {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533 !important;
    border-radius: 3px;
    color: #fff !important;
    padding: 9px;
    text-decoration: none !important;
}
.Inactive {background: #ff0000 none repeat scroll 0 0;}
@media screen and (min-width:320px) and (max-width:480px){
.cleanSearchFilter {
    padding: 8px 5px !important;
}
.termsearch {
    padding-bottom: 10px !important;
}
.searching {
    width: 100% !important;
}
.paginationblk {
    text-align: center !important;
}
.paginationselbox {
    justify-content: end;
}
.statusbtn {
    font-size: 12px !important;
}
}
@media screen and (min-width:481px) and (max-width:767px){
.fullwidthinput label {
	width:auto;
}
#schoolSearchForm .inputbox.termselect {
    padding-bottom: 10px;
    width: 100%;
}
#schoolSearchForm .selectfilter {
    width: 100%;
}
#schoolSearchForm .selectoption {
    width: 100%;
}
#schoolSearchForm .searching.termsearch {
    padding-bottom: 10px;
    padding-left: 4px;
    padding-right: 4px;
    width: 100%;
}
.selectfilter > span {
    font-size: 14px;
}
.totaldiv h3 {
    font-size: 14px;
}
.col-sm-9.col-xs-6.paginationblk {
    font-size: 14px;
}
.statusbtn {
    font-size: 12px !important;
}
}
@media screen and (min-width:768px) and (max-width:991px){
#schoolSearchForm .inputbox.termselect {
    padding-left: 10px;
    width: 50%;
	padding-bottom:10px;
}
.totaldiv h3 {
    font-size: 14px;
}
.statusbtn {
    font-size: 12px !important;
}
}
@media screen and (min-width:992px) and (max-width:1200px){
#schoolSearchForm .inputbox.termselect {
    padding: 0 4px;
    width: 50%;
}
#schoolSearchForm .selectfilter {
    width: 80%;
}
#schoolSearchForm .nopadding.selectoption {
    width: 50%;
}
#schoolSearchForm .searching.termsearch {
    width: 23%;
}
.cleanSearchFilter {
    padding: 10px 5px;
}
.btn {
    padding: 6px 5px;
}
.statusbtn {
    padding: 6px 10px;
}
.selectfilter > span {
    font-size: 14px;
}
.totaldiv h3 {
    font-size: 14px !important;
    text-align: left;
}
.col-sm-9.col-xs-6.paginationblk {
    font-size: 14px;
}
.statusbtn {
    font-size: 12px !important;
}
}
</style>	