<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>

<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>

    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li><a href="<?php echo base_url(); ?>help">Help and Knowledgebase</a></li>  


<li>Staff</li>

        </ul>

        </div>


   <div class="block-content">	

      <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-3">
	<div class="list-block">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#admin-dashboard" aria-controls="home" role="tab" data-toggle="tab">ADD  STAFF</a></li>
      <li role="presentation"><a href="#teacher-dashboard" aria-controls="profile" role="tab" data-toggle="tab">VIEW  STAFF</a></li>
    </ul>
	</div>
    </div>
    <div class="col-sm-9">
      <div class="tab-content adm-dsh">
		<div role="tabpanel" class="tab-pane active" id="admin-dashboard">
			<h1 class="tab-title">What is Staff Section?</h1>
			<p class="main-heading-cntnt"> This module will introduce the number of staff members in an organization , how to add the new staff member in an organization or also view, update the records of the staff member  and delete etc.</p>
			
			<div class="admn-content">
					<h3 class="para-heading">Staff  module consist of two sections:</h3>
					<p class="heading-cntnt"> <strong>1) ADD  STAFF</strong></p>
					<p class="heading-cntnt"><strong>2) VIEW  STAFF </strong></p>
					
					<div class="compse-msg">
						<h2 class="main_heading_txt text_font">HOW TO  ADD  A STAFF?</h2>
                         <img src="<?php echo base_url();?>uploads/staff/add_staff.jpg" class="stt">
						<p class="heading-cntnt"> In this section, user can add a new staff member. 
When user clicked on add staff, then add detail page is opened where user can fill the personal detail, professional detail, access system detail in which user will mention which kind of responsibility is given to the staff member and also salary detail of staff is added by concerned person.
<span>Clicked on submit to add the staff member.</span>
</p>                  
</div>

					</div>	
			</div>
	
	 <!-- Teacher_dashboard-->
      <div role="tabpanel" class="tab-pane" id="teacher-dashboard">
	  
			<h1 class="tab-title"> HOW TO VIEW STAFF?</h1>
			<div class="admn-content">
					<img src="<?php echo base_url();?>uploads/staff/staff-screen.jpg" class="stt"><div class="compse-msg">
                    <p><b>This section display the listing of all the staff members with action button.</b></p>
						<h3 class="para-heading">SEARCH:</h3>
						<p class="heading-cntnt">   User can also search by first name,last name and full name, username and email using search   textbox.
</p>
						<p><strong>Action button provides following functionalities such as:</strong>
						
						<p class="heading-cntnt"> user can  also view, update the records of the staff members and delete etc.</p>
                        <p class="heading-cntnt">User can also deactivate the staff member by click on active of action button.
</p>

						<p class="heading-cntnt"> In  showing dropdown, user can select the number of records at one time.</p>
                        <p class="heading-cntnt"><b>Export to excel</b> button is used to save the record in an excel file.</p>
				<h3 class="para-heading">WHAT IS THE FUNCTIONALITY OF  INVITE STAFF</h3>
                <img src="<?php echo base_url();?>uploads/staff/invite_staff.jpg" class="stt">
                <p class="heading-cntnt"> This link is used by concerned person to invite a staff member.
</p>
				<p> <b>STEPS TO INVITE A STAFF MEMBER:</b></p>
				<div class="compse-msg">
					<ul class="msg-point">
					<li>In this, when user clicked on invite staff button, the  popup  will show up with name" invite staff member"  where concerned person entered the email address of the staff member which they want to be invite then clicked on send button to send the request.
</li>
					<li>Email received by  the invited staff member with username and password.</li>
					<li> Now, invited staff member displayed in the staff  list.</li>
 <p class="heading-cntnt">Concerned person and invited staff member both can fill the personal details.</p>
	<ul>
   
<b>User detail will display in three different colors:</b>
	<li>RED – When personal detail is not filled.</li>
	<li>GREEN- When personal detail is filled but professional detail is not filled.</li>
    <li>BLACK- When all detail are  filled.</li>
	</ul>
    <p class="heading-cntnt">Same as other staff members, this invited staff member is also display with action button.</p>
	<b>Action button provides following functionalities such as:</b>
    <ul>
    <li>user can  also view, update the records of the staff members and delete etc.</li>
    <li>User can also deactivate the staff member by click on active of action button.</li>
    </ul>
    <p class="textspacing"><b>NOTE-</b> -  fields with asterisk(*)  is mandatory to fill. </p>
    <ul>
   <li> In showing dropdown ,user can select the numbers of records to view  per page.</li>
 <li><b>EXPORT TO EXCEL </b> button is used to save the records in excel file on their personal computer.</li>
    </ul>
	</li>
	</ul>
	
	</div>
	
					</div>	
			</div>
			

	  
	  
	  </div>
		
			
	</div><!-- STUDENT view Report -->	
	  
	  
	  
	  </div>
    </div>
    </div>

    </div>

<style>
.tabs-left, .tabs-right {
  border-bottom: none;

  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}

.tab-content.adm-dsh {
    background: #fff;
    width: 100%;
    display: inline-block;
    padding: 10px 20px;
    border: 1px solid #eee;
}
h1.tab-title {
    margin-top: 0 !important;
    margin-left: 0 !important;
    border: none !important;
    font-weight: 900;
    font-size: 32px !important;
}
.adm-dsh p {
    margin-left: 0;
}
p.main-heading-cntnt {
    border-bottom: 1px solid #eeeeee;
    padding: 10px 0;
	margin-left:20px;
}
.admn-content {
    margin-top: -2px;
    padding-top: 10px;
    padding-left: 20px;
}
h3.para-heading {
        margin-top: 0px;
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
.admn-content p {
    margin-top: -7px;
}
img.stt {
    width: 100%;
    padding: 30px 0;
	margin-top:-10px;
}
.compse-msg {
    padding: 4px 0 0 0px;
}
h4.txt-frmt {
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
ul.msg-point {
    padding-left: 0;
    margin: 0 0 0 14px;
    line-height: 30px;
    font-size: 15px;
}
.compse-msg {
    padding: 20px 0 16px 0px;
    line-height: 28px;
}
h3.para-heading.mgn {
    margin-top: 20px;
}
.list-block {
    height: 634px;
    width: 100% !important;
    background: #fff;
}
p.spacing {
    line-height: 15px;
    margin-top: -17px;
}
.textspacing{margin-bottom:0px;}
.msg-point > ul {
    margin-bottom: 10px;
}
.text_font{font-weight:bold;}
.editprofile-content .tab-content h1{padding: 15px 22px 0;margin-bottom:0px;}
</style>