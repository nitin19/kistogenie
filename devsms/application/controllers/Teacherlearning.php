<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teacherlearning extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','teacherlearning_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{
		$data							= array();
		$filesearch			= isset($_GET['filesearch'])?$_GET['filesearch']:NULL;
		$school_id		= $this->session->userdata('user_school_id');
		$data['Folders'] = $this->teacherlearning_model->getallFolders($school_id, $filesearch);
		
		$this->load->view('header');
		$this->load->view('teacherlearning/documents', $data);
		$this->load->view('footer');
	}

	function addFolder() {
			 
			     $folderdata = array();
				 $school_id		= $this->session->userdata('user_school_id'); 

			/*if (trim($this->input->post('parent')) != '') {
                 $folderdata['parent_folder'] = trim($this->input->post('parent'));
                }*/
				
			if (trim($this->input->post('foldername')) != '') {
                 $folderdata['teacher_learning_name'] = trim($this->input->post('foldername'));
				 $foldername = trim($this->input->post('foldername'));
                }
			
			$folderdata['school_id'] = $this->session->userdata('user_school_id');
			$folderdata['created_by'] = $this->session->userdata('user_name');
			$folderdata['is_active'] = '1';
			$folderdata['created_date'] = date('Y-m-d H:i:s');;
			$folderdata['updated_date'] = date('Y-m-d H:i:s');
			
			$chk_foldername = $this->teacherlearning_model->chk_folder_name($foldername, $school_id);
			   if( count($chk_foldername) > 0 ){
				   echo json_encode(array('Status'=>"alreadyexist"));
			    } else {
              if($this->teacherlearning_model->insertfolder($folderdata)) {
				 echo json_encode(array('Status'=>"true"));
			    } else {
				 echo json_encode(array('Status'=>"false"));
			       }
			     }
	        }
			
			 function deletefolder(){
	    $id = $this->uri->segment(3); 
/*		 $folderid = $this->uri->segment(4);
*/		$school_id		= $this->session->userdata('user_school_id'); 
		$data = array(
				'is_deleted' => '1'
				);
		$deletefolder = $this->teacherlearning_model->deletefolder($id, $data);
		if($deletefolder){
			$this->session->set_flashdata('success', 'Folder has been Deleted successfuly.');
				redirect(base_url()."teacherlearning/");exit;}
				else {
					$this->session->set_flashdata('error', 'Folder has not been Deleted ');
				redirect(base_url()."teacherlearning/");exit;}
			}

}

