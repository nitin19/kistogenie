<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentdashboard extends CI_Controller {
	
	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model', 'Student_model_dashboard'));
		$this->load->database();
        $this->load->library('session');
    }

	

	public function index(){
		
		$data = array();
		$user_id		=   $this->session->userdata('user_id');
		
		
		$search_condition['to']	= $user_id;
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		$perpage						= 2;
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'studentdashboard/index';
	    $config['total_rows'] 			= $this->Student_model_dashboard->totalnotification($user_id);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = ""; 
		
		$this->pagination->initialize($config);
		
		$odr =   "notification_id";
		$dirc	= "desc";
		
		$data['show']   =	$this->Student_model_dashboard->show_notification($search_condition,$start,$perpage,$odr,$dirc);
			 
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		$data['total_rows']				= $config['total_rows'];
		$data['data_rows']				= $data['show'];	 
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['page_name']				= $this->uri->segment(1);
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		

		$attendeceinterval = 'attendance_date BETWEEN (CURDATE() - INTERVAL 30 DAY) AND CURDATE()';
		$data['studentattendence']					= $this->Student_model_dashboard->student_attendence($attendeceinterval, $user_id);
		$data['info']      =  $this->Student_model_dashboard->get_single_student($user_id);
		$data['todayattendance'] = $this->Student_model_dashboard->gettodayattendence($user_id);
		
		
		$this->load->view('header');
		$this->load->view('studentdashboard/studentdashboard', $data);
		$this->load->view('footer');

	}
	
   public function myattendance(){
	    $data	=	 array();
	   $school_id		= $this->session->userdata('user_school_id');
	       $user_id		= $this->session->userdata('user_id');
		   
		$sdate =  isset($_GET['sdate'])?$_GET['sdate']:NULL;
			$startingdate = date('Y-m-d', strtotime($sdate));
		
		$edate =  isset($_GET['edate'])?$_GET['edate']:NULL;
			$endingdate = date('Y-m-d', strtotime($edate));
		
		$type =  isset($_GET['type'])?$_GET['type']:NULL;
		
		$data['attendence'] = $this->Student_model_dashboard->get_attendence($startingdate, $endingdate, $type, $user_id);
		$this->load->view('header');
		$this->load->view('studentdashboard/studentattendence', $data);
		$this->load->view('footer');

   }
	   public function getattendance(){
		   $data	=	 array();
	    $user_id		= $this->session->userdata('user_id');
		$sdate  =  $_REQUEST['sdate']; 
		$startingdate = date('Y-m-d', strtotime($sdate));
		$edate  =  $_REQUEST['edate'];
		$endingdate = date('Y-m-d', strtotime($edate));
		$type  =  $_REQUEST['type']; 
		$result= $this->Student_model_dashboard->get_attendence($startingdate, $endingdate, $type, $user_id);
		echo count($result);
		$this->load->view('header');
		$this->load->view('studentdashboard/studentattendence',$data);
		$this->load->view('footer');
   }
   
    public function progressreport(){
		$user_id				= 		$this->session->userdata('user_id');
		$data['studentinfo'] 	= 		$this->Student_model_dashboard->getStudentinfo($user_id);
		$school_id 				= 		$data['studentinfo']->school_id;
		$branch_id 				= 		$data['studentinfo']->student_school_branch;
		$class_id 				= 		$data['studentinfo']->student_class_group;
		
		/*$data['terms'] 		 = 		$this->Student_model_dashboard->getterms($school_id,$branch_id);*/
		
		$data['terms'] 			= 		$this->Student_model_dashboard->getterms($school_id);
		$data['progressreport'] = $this->Student_model_dashboard->getprogressreport($school_id,$branch_id,$class_id,$user_id);
		$this->load->view('header');
		$this->load->view('studentdashboard/progress_reports', $data);
		$this->load->view('footer');
   }
   
   	 public function download_attachments($fileName = NULL) { 
	
       if ($fileName) {
        $file		=	realpath ( FCPATH.'uploads/notification/'.$fileName );
        if (file_exists ( $file )) {
         $data		=	file_get_contents ( $file );
         $this->load->helper('download');
         force_download ( $fileName, $data );
		 echo"doen";
        } else {
		 $this->session->set_flashdata('error', 'Requested file does not exists.');
         redirect(base_url()."studentdashboard/");exit;
          }
        }
      }
	  
	  public function deletemsg($notificationid){
				if($this->Student_model_dashboard->delete($notificationid))
					{
					$this->session->set_flashdata('success', 'Message has been deleted successfully.');
					redirect(base_url()."studentdashboard/");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."studentdashboard/");exit;
					}
				}
   
      	
}

