<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teachertype extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','teachertypemodel'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){

		$id 						= $this->uri->segment(4);
		
		$search_condition 			= array();
		$school_id					= $this->session->userdata('user_school_id');
		
		$PerPage				   	= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		$type_search				= isset($_GET['type_search'])?$_GET['type_search']:NULL;
		$branch_search				= isset($_GET['branch'])?$_GET['branch']:NULL;
		if($branch_search!=''){
			$search_condition['branch.branch_name']	= $branch_search;
		}
		
		$totrows 					= $this->teachertypemodel->totaltypes($search_condition, $school_id, $type_search);
		$data                		= array();
	    $data["total_rows"]  		= count($totrows);
		$data["base_url"]    		= base_url() . "teachertype/index";
		
	   if($PerPage!='') {
			$data["per_page"] 		= $PerPage;
			$perpage				= $data["per_page"];
		} else {
			 $data["per_page"]		= 10;
			 $perpage				= $data["per_page"];
		}
		
		$data["uri_segment"] 		= 3;
		$data['postfix_string'] 	= "/?type=$type_search&perpage=$perpage";
		
		$this->pagination->initialize($data);
		
	    $page                		= $this->uri->segment(3,0) ;
	    $data['last_page']	 		= $page;
        $data["links2"]       		= $this->pagination->create_links();
		$typedata 					= $this->teachertypemodel->get_single_record($id);
		$data['info']				= $typedata;
		$data['type_search'] 		= $type_search;
		$data['branch']      		= $this->teachertypemodel->get_branch3($school_id);
		
		$data['teachtype']  		= $this->teachertypemodel->get_teach_type($search_condition, $school_id, $data["per_page"], $page, $type_search);
				
		$this->load->view('header');
		$this->load->view('master/teacher_type', $data);
		$this->load->view('footer');
	}
	
	public function add_teach_type() {
    
    	   $school_id		= $this->session->userdata('user_school_id');
		   
		   $typedata = array();
		   $typedata['school_id'] = $school_id;
		   
			  
		   //$typedata['branch_id'] = trim($this->input->post('branch'));
		  
		
	       $typedata['teacher_type'] = trim($this->input->post('select_type'));
		   
		
	       $typedata['type_description'] = trim($this->input->post('desc'));
		  
		   
		   $typedata['is_active'] = 1 ;
		   $typedata['is_deleted'] = 0 ;
		   $typedata['created_by'] = $this->session->userdata('user_name');
		   $typedata['created_date'] = date('Y-m-d H:i:s');
		   $typedata['updated_date'] = date('Y-m-d H:i:s');
		   
		   $type = trim($this->input->post('select_type'));
/*		changes in teacher_type
		   $branch = trim($this->input->post('branch'));
		   $checkteacher_type = $this->teachertypemodel->check_teach_type($branch, $type, $school_id);*/
		
		   $checkteacher_type = $this->teachertypemodel->check_teach_type($type, $school_id);
		           $countteachertype = count($checkteacher_type);
				   
				   if($countteachertype > 0 ) {
					   
					   $this->session->set_flashdata('error', 'Teacher type already exists. Teacher type has not been added.');
				       redirect(base_url()."teachertype/");exit;
				
				     } else {
					     if($this->teachertypemodel->insert_teach_type($typedata)) {
							$this->session->set_flashdata('success', 'Teacher type has been added successfully.');
							redirect(base_url()."teachertype/");exit; 
						 } else {
							$this->session->set_flashdata('error', 'Some problem exists. Teacher type has not been added.');
							redirect(base_url()."teachertype/");exit; 
						 }
				   }
	}
	public function update_teachtype(){
		 $id = $this->uri->segment(4);
			  $page = $this->uri->segment(3);
			 $school_id		= $this->session->userdata('user_school_id');
			 $data							= array();
			 $id = trim($this->input->post('id'));

			 
			 $data = array(
               'branch_id' => trim($this->input->post('branch')),
               'teacher_type' => trim($this->input->post('select_type')),
               'type_description' => trim($this->input->post('desc'))
); 
			$type = trim($_POST['select_type']);
			//$branch = trim($_POST['branch']);
			$id = trim($_POST['id']);
			
			//$checkteacher_type = $this->teachertypemodel->check_update_teach_type($branch, $type, $school_id, $id);
			
			$checkteacher_type = $this->teachertypemodel->check_update_teach_type($type, $school_id, $id);
		    $countteachertype = count($checkteacher_type);
				   
				   if($countteachertype > 0 ) {
					    $this->session->set_flashdata('error', 'Teacher Type already exists. Teacher Type has not been Updated.');
				       redirect(base_url()."teachertype/index/".$page."/".$id);exit;
				
				     } else {
					$updatedata = $this->teachertypemodel->update_teacher_type($id,$data);
					 if($updatedata = 1){
					 $this->session->set_flashdata('success', 'Data has been updated successfully.');
				redirect(base_url()."teachertype/index/".$page."/".$id);exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Type has not been updated.');
				redirect(base_url()."teachertype/index/".$page."/".$id);exit;
			   }
			 }
		 }
		 public function type_action(){
			$id = $this->uri->segment(3);  
			$activity = $this->uri->segment(4);
			$data							= array();
			if($activity == 'active'){ 
			 $data = array(
				'is_active' => '1'
				);
		  } 
		  if($activity == 'inactive'){ 
			 $data = array(
				'is_active' => '0'
				);
		  } 
		  $actiondata = $this->teachertypemodel->teacher_action($id, $data);
		  if($actiondata = 1){	
				$this->session->set_flashdata('success', 'Teacher Type has been updated successfully.');
				redirect(base_url()."teachertype/");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Type has not been updated.');
				redirect(base_url()."teachertype/");exit;
			   }
			  
		  }
		  
		  
		  public function checktype_staff(){
			$id = $this->uri->segment(3); 
			$school_id		= $this->session->userdata('user_school_id'); 
			$data = array(
				'is_deleted' => '1'
				);
		$typedata_rows = $this->teachertypemodel->get_deltype($school_id, $id);
		/*$teacher_type = $typedata_rows->teacher_type;*/
		$teachertype_id = $typedata_rows->id;
/*		$branch_id = $typedata_rows->branch_id;		 
   	    $actiondata = $this->teachertypemodel->check_staffassigntype($teachertype_id, $school_id, $branch_id);*/
		  
		  $actiondata = $this->teachertypemodel->check_staffassigntype($teachertype_id, $school_id);
		  if($actiondata > 0 ) {					   
					   $this->session->set_flashdata('error', 'Teacher Level has Already Assign to staff. You Cannot Delete it');
				       redirect(base_url()."teachertype/");exit;	
				     }	   else {
						 			$updatedata = $this->teachertypemodel->teacher_action($id, $data);
									if( $updatedata = 1){
					 $this->session->set_flashdata('success', 'Data has been Deleted successfully.');
				redirect(base_url()."teachertype/");exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Level has not been Deleted.');
				redirect(base_url()."teachertype/");exit;
				}
						 }
		  
		  }
 

/******************** update data***************************/

                   public function display() {
                       $data = array();
                       $data['result'] = $this->teachertypemodel->get_contents();
				   print_r($data);
                       $this->load->view('teacher_type', $data);
                  }

                  public function edit() {
					   $id = $this->uri->segment(4);
                       $data = array();
                       $data['result'] = $this->teachertypemodel->entry_update($id);
                      $this->load->view('teacher_type', $data);
                       
                     $info =  $this->teachertypemodel->entry_update1($id);
            
        }
		
		

 
 
 
	
}