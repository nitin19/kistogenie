<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>



<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>
            <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>
        <li><a href="<?php echo base_url(); ?>help">Help and Knowledgebase</a></li>
        <li class="edit">Login Module</li>    

        </ul>

        </div>

   <div class="block-content">	

      <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-3">
	<div class="list-block">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#login_dashboaed" aria-controls="home" role="tab" data-toggle="tab">Login Dashboard</a></li>
      <li role="presentation"><a href="#registration_dashboaed" aria-controls="profile" role="tab" data-toggle="tab">Registration Dashboard</a></li>
    </ul>
	</div>
    </div>
    <div class="col-sm-9">
      <div class="tab-content adm-dsh">
		<div role="tabpanel" class="tab-pane active" id="login_dashboaed">
			<h1 class="tab-title">Login Dashboard</h1>
			<p class="main-heading-cntnt"> </p>
			<div class="admn-content">
				<h3 class="para-heading">What is login module ?</h3>
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/login_dashboard.jpg" class="stt">
				<p class="heading-cntnt"><strong>1) </strong>Log  in is the process by  which an  individual gains access to a computer system by identifying and authenticating themselves.</p>
				<p class="heading-cntnt"><strong>2) </strong>The user credentials are typically some form of “username”and a matching “password” and these credentials are sometimes to be referred to as login</p>
				<p class="heading-cntnt"><strong>3) </strong>This module displays a username and password login form.</p>
				<p class="heading-cntnt"><strong>4) </strong>After successfully login ,if user wants to end access to a computer system or a website then they will click on logout button which is display on the top of the right hand side of the dashboard.
				<p class="heading-cntnt"><strong>5) </strong>Logout also known as log off, sign off or sign out.</p>

				<h3 class="para-heading">Remember Me:</h3>
					<p class="heading-cntnt"><strong>In this user can also saved their credentials by clicked on remember me.<br>
					“ Remember me” checkbox saves your login id and password. Next time you visit the website you will automatically logged in.It actually a time saver that means you don’t put your password again and again
					</strong>
					</p>
					<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/remember_me.jpg" class="stt">
					
					<h3 class="para-heading">Forgot Password:</h3>
					<p class="heading-cntnt">It also display a link to retrieve a forget password. We can use this link in case if user forgot their password.</p>
					<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/forgot_password.jpg" class="stt">
					
					<h3 class="para-heading">For this, user must follow these steps:</h3>
					<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/forgot_password-email-input_screen.jpg" class="stt">
					<ul class="msg-point">
								<li><strong>Visit forgot password</strong></li>
								<li><strong>Enter the  email address</strong></li>
								<li> <strong>Select Submit Button</strong></li>
								<li> <strong>Check your inbox for password reset email</strong></li>
								<li> <strong>Now enter the new password in the login form</strong></li>
							</ul>
			</div>
	 </div>
	  
<!-- staff-enrolment-report--->
	 
	  <div role="tabpanel" class="tab-pane" id="registration_dashboaed">
			<h1 class="tab-title">Registration Dashboard</h1>
			<p class="main-heading-cntnt"></p>
			<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/regiser_screen.jpg" class="stt">
			<div class="admn-content">
				<h3 class="para-heading">GET STARTED:</h3>
							<li>If user want to create an account for their school, there is a link <strong>“Get Started” </strong> to enable self registration of school for users.</li>
							<li>A form will be open. You have to  fill all the details to register your school.</li>
							<li>After filling  all the details , you need to checked the check box to agree with the “ terms and conditions” and then clicked on <strong>“Register”</strong> button, user will successfully registered their school.</li>
							<li>If user already have account then click on “Login” link  and get  directly to access their account.</li>
							</ul>
		
			</div>
	
	
	 </div>	
	<!-- staff-enrolment-report---> 
    
	 </div>
    </div>
    </div>
    </div>

<style>
.tabs-left, .tabs-right {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}

.tab-content.adm-dsh {
    background: #fff;
    width: 100%;
    display: inline-block;
    padding: 10px 20px;
    border: 1px solid #eee;
}
h1.tab-title {
    margin-top: 0 !important;
    margin-left: 0 !important;
    border: none !important;
    font-weight: 900;
    font-size: 32px !important;
}
.adm-dsh p {
    margin-left: 0;
}
p.main-heading-cntnt {
    margin-top: -44px;
    border-bottom: 1px solid #eeeeee;
    padding: 10px 0;
	margin-left:20px;
}
.admn-content {
    margin-top: -2px;
    padding-top: 10px;
    padding-left: 20px;
}
h3.para-heading {
        margin-top: 0px;
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
.admn-content p {
    margin-top: -7px;
}
img.stt {
    width: 100%;
    padding: 30px 0;
	margin-top:-10px;
}
.compse-msg {
    padding: 4px 0 0 0px;
}
h4.txt-frmt {
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
ul.msg-point {
    padding-left: 0;
    margin: 0 0 0 14px;
    line-height: 30px;
    font-size: 15px;
}
.compse-msg {
    padding: 20px 0 16px 0px;
    line-height: 28px;
}
h3.para-heading.mgn {
    margin-top: 20px;
}
.list-block {
    height: 634px;
    width: 100% !important;
    background: #fff;
}
p.spacing {
    line-height: 15px;
    margin-top: -17px;
}
</style>