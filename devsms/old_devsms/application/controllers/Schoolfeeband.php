<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schoolfeeband extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','school_feeband_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){
		
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;
		$status_search					= isset($_GET['status'])?$_GET['status']:NULL; 
		$word_search				    = isset($_GET['seachword'])?$_GET['seachword']:NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$search_condition['is_deleted']	= '0';
		 
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		if($class_search!=''){
			$search_condition['class_id']	= $class_search;
		}
		if($status_search!=''){
            $search_condition['is_active']	= $status_search;
		  }
		 
		$data							= array();
		
		$school_id		= $this->session->userdata('user_school_id');
		$data['branches'] = $this->school_feeband_model->getbranches($school_id);
		$data['currencies'] = $this->school_feeband_model->getcurrencies($school_id);
		
		//$data['sclasses'] = $this->school_feeband_model->getschoolclasses($school_id);
		
		$data['title']	      			= "Fee Band";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'schoolfeeband/index';
	    $config['total_rows'] 			= $this->school_feeband_model->getRows($search_condition,$school_id,$word_search);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = "/?branch=$branch_search&class=$class_search&status=$status_search&seachword=$word_search&perpage=$perpage"; 
		
		$this->pagination->initialize($config);

		$odr =   "fee_band_id";
		$dirc	= "desc";
		
		$data_rows= $this->school_feeband_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$word_search);
		
		$feebandid = $this->uri->segment(4);
		if($feebandid!='') {
		  $feeband_data_rows				= $this->school_feeband_model->get_single_feeband($feebandid);
		  $data['info']				   		= $feeband_data_rows[0];
		 }
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['class_search']			= $class_search;
		$data['status_search']			= $status_search;
		$data['word_search']		    = $word_search;
		$data['PerPage']				= $PerPage;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
						
		$this->load->view('header');
		$this->load->view('master/school_feeband', $data);
		$this->load->view('footer');
     }
	 
	 public function addfeeband(){
		 
		    $feebanddata 		= array();
			
			$school_id = $this->session->userdata('user_school_id');
			
		    $feebanddata['school_id'] = $this->session->userdata('user_school_id');
			$feebanddata['created_by'] = $this->session->userdata('user_name');
			$feebanddata['is_active'] = '1';
			$feebanddata['created_date'] = date('Y-m-d H:i:s');;
			$feebanddata['updated_date'] = date('Y-m-d H:i:s');
			 
			if (trim($this->input->post('schoolbranch'))!= '') {
                $feebanddata['branch_id'] = trim($this->input->post('schoolbranch'));
				 $branch_id = trim($this->input->post('schoolbranch'));
            }
			
			if (trim($this->input->post('classname')) != '') {
                $feebanddata['class_id'] = trim($this->input->post('classname'));
				$class_id = trim($this->input->post('classname'));
            }
			
			if (trim($this->input->post('mode')) != '') {
                $mode = trim($this->input->post('mode'));
            }
			
			if (trim($this->input->post('currency')) != '') {
                $currency = trim($this->input->post('currency'));
            }
			
			if (trim($this->input->post('classfee')) != '') {
                $classfee = trim($this->input->post('classfee'));
            }
			
			$feebanddata['fee_band_price'] = $currency.' '.$classfee.' '.$mode;
			$feeprice = $currency.' '.$classfee.' '.$mode;
			
			if (trim($this->input->post('classfeedescription')) != '') {
                $feebanddata['fee_band_description'] = trim($this->input->post('classfeedescription'));
            }
		
			 $checkfeeprice = $this->school_feeband_model->check_fee_price($school_id,$branch_id,$class_id,$feeprice);
			 $countfeeprice = count($checkfeeprice);
			 
			 if( $countfeeprice > 0 ) {
				  $this->session->set_flashdata('error', 'Fee band already exists. Fee band has not been added.');
				  redirect(base_url()."schoolfeeband/");exit;
			 } else {
			
		   if($this->school_feeband_model->insertFee($feebanddata)) {
			   $this->session->set_flashdata('success', 'Fee band has been added successfully.');
			   redirect(base_url()."schoolfeeband/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Fee band has not been added.');
				redirect(base_url()."schoolfeeband/");exit;
		      }
			}
			
        }
		
		
		public function updatefeeband(){
			
			  $start						    = $this->uri->segment(3);
		      $editedFeebandid					= $this->uri->segment(4); 
			  $Action							= $this->uri->segment(5); 
		 
		  $postURL	= $this->createPostURL($_GET);
		if($editedFeebandid==NULL){
			$this->session->set_flashdata('error', 'Select fee band first.');
			redirect(base_url()."schoolfeeband");exit;
		   }
		   
		 $feebanddata 		= array();
         $school_id = $this->session->userdata('user_school_id');
		 $where							= array('fee_band_id'=>$editedFeebandid);
		 $data['title']	      			= "Edit Fee band";
		 $data['mode']					= "Edit";
			
			if (trim($this->input->post('schoolbranch')) != '') {
                $feebanddata['branch_id'] = trim($this->input->post('schoolbranch'));
				 $branch_id = trim($this->input->post('schoolbranch'));
            }
			
			if (trim($this->input->post('classname')) != '') {
                $feebanddata['class_id'] = trim($this->input->post('classname'));
				$class_id = trim($this->input->post('classname'));
            }
			
			if (trim($this->input->post('mode')) != '') {
                $mode = trim($this->input->post('mode'));
            }
			
			if (trim($this->input->post('currency')) != '') {
                $currency = trim($this->input->post('currency'));
            }
			
			if (trim($this->input->post('classfee')) != '') {
                $classfee = trim($this->input->post('classfee'));
            }
			$feebanddata['fee_band_price'] = $currency.' '.$classfee.' '.$mode;
			$feeprice = $currency.' '.$classfee.' '.$mode;
			
			if (trim($this->input->post('classfeedescription')) != '') {
                $feebanddata['fee_band_description'] = trim($this->input->post('classfeedescription'));
            }
			
			$feebanddata['updated_date'] = date('Y-m-d H:i:s');
			
			 $checkfeeprice = $this->school_feeband_model->check_edited_feeprice($school_id,$branch_id,$class_id,$feeprice,$editedFeebandid);
			 $countfeeprice = count($checkfeeprice);
			 
			 if( $countfeeprice > 0 ) {
				  $this->session->set_flashdata('error', 'Fee band already exists. Fee band has not been Updated.');
				  redirect(base_url()."schoolfeeband/index/$start/$editedFeebandid/edit/");exit;
			 } else {
			
		   if($this->school_feeband_model->updateFee($feebanddata,$where)) {
			   $this->session->set_flashdata('success', 'Fee band has been updated successfuly.');
			   redirect(base_url()."schoolfeeband/index/$start/$editedFeebandid/edit/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Fee band has not been updated.');
				redirect(base_url()."schoolfeeband/index/$start/$editedFeebandid/edit/");exit;
		      }
			}
		

	   }
	   
	   
	   	function deactivatefeeband(){
		
		  $start						    = $this->uri->segment(3);
		  $feebandid						= $this->uri->segment(4); 
		  $where		= array('fee_band_id'=>$feebandid);
		  $data		= array('is_active'=>'0');
		if($this->school_feeband_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Fee band has been updated successfuly.');
			redirect(base_url()."schoolfeeband/index/$start/$feebandid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Fee band has not been updated.');
			redirect(base_url()."schoolfeeband/index/$start/$feebandid");exit;
		}
		
	}
	function activatefeeband(){
		 
		  $start						    = $this->uri->segment(3);
		  $feebandid						= $this->uri->segment(4); 
			  
		$where		= array('fee_band_id'=>$feebandid);
		$data		= array('is_active'=>'1');
		if($this->school_feeband_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Fee band has been updated successfuly.');
			redirect(base_url()."schoolfeeband/index/$start/$feebandid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Fee band has not been updated.');
			redirect(base_url()."schoolfeeband/index/$start/$feebandid");exit;
		}
		
	}
	
	
		public function deletefeeband($feebandid){
			
			 $school_id = $this->session->userdata('user_school_id');
			 
			 $feeband_data_rows = $this->school_feeband_model->check_deletedfeeband($school_id,$feebandid);
			 
			 $branch_id = $feeband_data_rows->branch_id;
			 $class_id = $feeband_data_rows->class_id;
			 
			 $chk_student_feeband = $this->school_feeband_model->check_student_feeband($school_id,$branch_id,$class_id,$feebandid);
			
			 $countStudentfeeband = count($chk_student_feeband); 
			 
			   if( $countStudentfeeband > 0 ) {
				  $this->session->set_flashdata('error', 'Fee band has not been deleted. As this fee band is assigned to students');
				  redirect(base_url()."schoolfeeband/index/");exit;
				 } else {
			
		 		 if($this->school_feeband_model->delete($feebandid)){
					$this->session->set_flashdata('success', 'Fee band has been deleted successfuly.');
					redirect(base_url()."schoolfeeband/index");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."schoolfeeband/index");exit;
		          }
			  }

	    }
		
		
		
	
		function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	 }
	  
	  
	   public function check_classes()

		{     
		    $school_id = $this->session->userdata('user_school_id');
			$schoolbranch = $_POST['schoolbranch']; 
	        $getclasses = $this->school_feeband_model->getclasses($schoolbranch,$school_id);
			foreach($getclasses as $getclass) {
 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
			}
	     }
	
}