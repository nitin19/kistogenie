<?php
$this->load->view('common/header');
?>
<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Table</a></li>
                            <li><i class="fa fa-table"></i><a href="<?php echo base_url('Dashboard');?>/view_student">Student table</a></li>
                            
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Student Table</strong>
                        </div>
                        <div class="card-body">
                            
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Branch</th>
                        <th>Position</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                         $i=1;
                        foreach ($viewstudent_detail as $student) {
                        //print_r($student);
                        
                        ?>
                        
                      <tr id="std_<?php echo $i;?>">
                        <td><?php echo $student['date'];?></td>
                        <td><?php echo $student['child_first'];?> <?php echo $student['child_last'];?></td>
                        <td><?php echo $student['area'];?></td>
                        <td><?php echo $student['age'];?></td>
                        <td><?php echo $student['email'];?></td>
                        <td><?php echo $student['phone'];?></td>
                        <td>
                            <ol class="breadcrumb text-right">
                            <li><i class="fa fa-table"></i><a href="<?php echo base_url('Dashboard');?>/view_studentdetail/<?php echo $student['id'];?>">view detail</a></li> 
                            <li><i class="fa fa-table"></i><a href="<?php echo base_url('Dashboard');?>/delete_student/<?php echo $student['id'];?>">Delete</a></li>
                            </ol>
                        </td>
                      </tr>
                    
                    <?php
                        $i++;
                      }
                     ?>
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->
    
<?php
$this->load->view('common/footer');
?>