<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Messagetemplate_model extends CI_Model {
	var $tbl		= "staff";
	var $users 		= "users";
	var $branch 	= "branch";
	var $classes 	= "classes";
	var $student 	= "student";
	var $subjects 	= "subjects";
	var $teacher_role  = "teacher_role";
	var $teacher_level = "teacher_level";
	var $teacher_type  = "teacher_type";
	var $notification  = "notification";
	var $message_template	= "message_template";
	
	
	 
    function __construct() {
        parent::__construct();
    }
	
	 function getRows($where=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->message_template);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  if($word_search != NULL){
		
			  $this->db->where("(title LIKE '%$word_search%' OR description LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	  function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->message_template);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  if($word_search != NULL){
	
			 $this->db->where("(title LIKE '%$word_search%' OR description LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("message_id", "desc"); 	
		   }
		
		     $this->db->limit($limit, $start);
             $query = $this->db->get();
		     $this->db->last_query(); 
             return  $query->result();

	}
	
	
	function insertMessage($msgdata){
	
		if($this->db->insert($this->message_template, $msgdata))
			return $this->db->insert_id();
		  else
			return false;
		//$this->db->insert($this->message_template, $msgdata);
		//echo $this->db->last_query(); exit();
	   }
	
	 function updateTemplates($msgdata,$where) {
		$this->db->where($where);

		if($this->db->update($this->message_template, $msgdata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit();
	 }
	 

	function delete($messageid){
		$where		= array('message_id' => $messageid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
	
		if($this->db->update($this->message_template, $data))
			return true;
		 else
			return false;
	   }

	 function get_single_template($messageid) {
          $this->db->select('*');
          $this->db->from($this->message_template);
		  $this->db->where('message_id', $messageid);
          $query = $this->db->get();
          return  $query->result();
          // echo $this->db->last_query(); exit();
     }


    
}
