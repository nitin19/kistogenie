<?php
$this->load->view('common/header');
?>
<style>
   .detailul li {
    list-style: none;
    margin: 5px 0px;
}
</style>
<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Table</a></li>
                            <li><i class="fa fa-table"></i><a href="<?php echo base_url('Dashboard');?>/view_student">Student table</a></li>
                           
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Student Table</strong>
                        </div>
                        <div class="card-body">
                             
                   <div class="row detailul">
                        <div class="col-md-12">
                              <div class="col-md-6">
                              <ul>
                                <li>Date         :</li>
                                <li>Name         :</li>
                                <li>Branch       :</li>
                                <li>Age          :</li>
                                <li>Gender       :</li>
                                <li>Father's Name:</li>
                                <li>Mother's Name:</li>
                                <li>Email Number :</li>
                                <li>Phone Number :</li>
                                
                                
                                
                              </ul>
                    
                    </div>
                    <div class="col-md-6">
                    
                        <?php
                         $i=1;
                        foreach ($detail as $student) {
                        //print_r($student);
                        
                        ?>
                        
                      <ul id="std_<?php echo $i;?>">
                        <li><?php echo $student['date'];?></li>
                        <li><?php echo $student['child_first'];?> <?php echo $student['child_last'];?></li>
                        <li><?php echo $student['area'];?></li>
                        <li><?php echo $student['age'];?></li>
                        <li><?php echo $student['gender'];?></li>
                        <li><?php echo $student['father'];?></li>
                        <li><?php echo $student['mother'];?></li>
                        <li><?php echo $student['email'];?></li>
                        <li><?php echo $student['phone'];?></li>
                        
                        
                        
                      </ul>
                    
                    <?php
                        $i++;
                      }
                     ?>
                        </div>
                     </div>
                     </div>
                  <!--  </tbody>-->
                  <!--</table>-->
                  
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->
    
<?php
$this->load->view('common/footer');
?>