<?php 
$this->load->view('common/header');
?>


<section class="banner">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="banner_head">Student Enrolment</h2>
            <ol class="breadcrumb">
              <li><a href="http://www.olivetreestudy.co.uk">Home</a></li>
              <li><a href="#">Enrolment</a></li>
            </ol>
          </div>
        </div>
      </div>
    </section>

       <!-- start steps -->
   <section class="step">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <h1>3 Simple Steps</h1>
           <span class="mapline"><i class="fa fa-map map"></i></span>
           <div class="row space60">
             <div class="col-md-4 col-sm-4 col-xs-12">
               <img src="<?php echo base_url();?>assets/img/no-1.png" class="num hidden-xs">
               <div class="media space70">
                <div class="media-left">
                  <a href="#">
                    <img class="media-object icon" src="<?php echo base_url();?>assets/img/i1.png" alt="...">
                  </a>
                </div>
                <div class="media-body">
                  <h4 class="media-heading">Register Now</h4>
                  
                </div>
              </div>
              <div class="row">
                  <div class="col-md-offset-1 col-md-11  col-sm-offset-1 col-sm-11 col-xs-12">
                    <p>Complete the quick and <br>simple application form<br> below.</p>
                  </div>
               </div>   
              <img src="<?php echo base_url();?>assets/img/a1.png" class="arrow hidden-xs">
             </div>

             <div class="col-md-4 col-sm-4 col-xs-12">
               <img src="<?php echo base_url();?>assets/img/no-2.png" class="num hidden-xs">
               <img src="<?php echo base_url();?>assets/img/a2.png" class="arrow hidden-xs">
               <div class="media">
                <div class="media-left">
                  <a href="#">
                    <img class="media-object icon" src="<?php echo base_url();?>assets/img/i2.png" alt="...">
                  </a>
                </div>
                <div class="media-body">
                  <h4 class="media-heading">Hear from us within 48 hours</h4>
                  
                </div>
              </div>
              <div class="row">
                  <div class="col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11 col-xs-12">
                    <p>Our friendly team will <br>contact you to arrange a Free<br> day for your child at one of<br> our schools.</p>
                  </div>
              </div>
             </div>

             <div class="col-md-4 col-sm-4 col-xs-12">
               <img src="<?php echo base_url();?>assets/img/no-3.png" class="num hidden-xs">
               <div class="media space70">
                <div class="media-left">
                  <a href="#">
                    <img class="media-object icon" src="<?php echo base_url();?>assets/img/i1.png" alt="...">
                  </a>
                </div>
                <div class="media-body">
                  <h4 class="media-heading">Experience our school for absolutely free.</h4>
                  
                </div>
                <div class="row">
                  <div class="col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11 col-xs-12">
                    <p>Get a tour of our school and<br> meet our team while your<br> child spends a day at our<br> school learning and having<br> fun!</p>
                  </div>
                </div>
                
              </div>
           </div>
         </div>
       </div>
     </div>
   </section>
   <!-- end steps -->




   <!-- start reg -->
    <section class="reg">
     <div class="container">
       <div class="row">
        <?php
          if($this->session->flashdata('success'))
          {
            ?>
            <div class="col-md-offset-3 col-md-5 col-sm-offset-3 col-sm-6 col-xs-12 ">
              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
              </div>
            </div>
            <?php
            
          }

          if($this->session->flashdata('error'))
          {
            ?>
            <div class="col-md-offset-3 col-md-5 col-sm-offset-3 col-sm-6 col-xs-12 ">
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> <?php echo $this->session->flashdata('error');?>
              </div>
            </div>
            <?php
            
          }
          ?>
         <div class="col-md-12">
           <h2>Registration Form</h2>
            <span class="mapline"><i class="fa fa-map map"></i></span>
            <div class="space60"></div>
            <div class="row">
             <div class="col-md-6 col-sm-6 col-xs-12">
                <form role="form" id="studentForm" name="studentForm" method="POST" action="<?php echo site_url("Student_enrol") ?>/insert_student" method="post" novalidate="novalidate" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="branch" class="col-sm-4 control-label">Area(Branch)</label>
                    <div class="col-sm-8">
                      <!-- <select class="form-control">
                        <option>Select one</option>
                        <option>4</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select> -->
                      <select name="branch" id="branch" class="form-control">
                          <option value="" disabled="" selected="">Select one </option>
                          <?php 
                          $branches = $this->Registration_model->get_all_branch();
                          if(sizeof($branches) > 0)
                          {
                            foreach ($branches as $branch) {
                              ?>
                              <option value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>
                              <?php
                            }
                            
                          }
                        ?> 
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="child_first" class="col-sm-4 control-label">Child's First Name</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" class="form-control" name="child_first" id="child_first" value="" maxlength="61">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="child_last" class="col-sm-4 control-label">Child's Last Name</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" class="form-control" name="child_last" id="child_last" value="" maxlength="61">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="age" class="col-sm-4 control-label">Age</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" class="form-control number" id="age" name="age" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="gender" class="col-sm-4 control-label">Gender</label>
                    <div class="col-sm-8">
                     <!--  <select class="form-control">
                        <option>select one</option>
                        <option>4</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select> -->
                      <select name="gender" id="gender" class="form-control">
                            <option value="" disabled="" selected="">Select one </option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="father" class="col-sm-4 control-label">Father's Full Name</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" name="father" id="father" class="form-control" value="" maxlength="61">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="mother" class="col-sm-4 control-label">Mother's Full Name</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" name="mother" id="mother" class="form-control" value="" maxlength="61">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-sm-4 control-label">E-mail Address</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="email" class="form-control" id="email" name="email" value="" maxlength="121">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="phone" class="col-sm-4 control-label">Phone</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" name="phone" id="phone" class="form-control" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                    </div>
                  </div>

                 <!--  <button class="btn btn_enrole">Enrole Me </button> -->
                  <input type="submit"  class="btn btn_enrole" name="studentenrolebtn" id="studentenrolebtn" value="Enrole Me">
                </form>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12">
                <img src="<?php echo base_url();?>assets/img/p1.png" class="img-responsive center-block">
                <img src="<?php echo base_url();?>assets/img/p2.png" class="img-responsive center-block space40">
              </div>
            </div>
         </div>
       </div>
     </div>
   </section>
   <!-- end reg -->
   


<?php $this->load->view('common/footer'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<style type="text/css">
  .error {
    color: red !important;
}
form#studentForm .col-sm-8 {
    margin-bottom: 20px;
}

</style>

       <script>
     
            (function ($, W, D)
            {
                var JQUERY4U = {};
                JQUERY4U.UTIL =
                        {
                            setupFormValidation: function ()
                            {
                                //form validation rules
                                $("#studentForm").validate({
                                    rules: {
                                         
                                        branch:
                                        {
                                            required: true
                                        },
                                        child_first :
					                    {
					                        required : true
					                    },
                                        child_last :
                                        {
                                            required : true
                                        },
                                        age :
                                        {
                                            required : true
                                        },
                                        gender :
                                        {
                                            required : true
                                        },
                                        father :
                                        {
                                            required : true
                                        },
                                        mother :
                                        {
                                            required : true
                                        },
                                        email :
                                        {
                                            required : true,
                                            email : true
                                        },
                                        phone :
                                        {
                                            required : true
                                        }   
					                  
					                },


                                    messages: {
                                        branch :
                                        {
                                        required: "Please select branch"
                                        },
                                        child_first : 
                                        {
                                        required:"Please enter first name"
                                        },
                                        child_last : 
                                        {
                                        required:"Please enter last name"
                                        },
                                        age : 
                                        {
                                        required:"Please enter age"
                                        },
                                        gender : 
                                        {
                                        required:"Please enter gender"
                                        },
                                        father : 
                                        {
                                        required:"Please enter father name"
                                        },
                                        mother : 
                                        {
                                        required:"Please enter mother name"
                                        },
                                        email : 
                                        {
                                        required:"Please enter email"
                                        },
                                        phone :
                                        {
                                        required:"Please enter phone number"  
                                        }
                                    },
                  
                                    submitHandler: function (form) {
                                        form.submit();
                                    }
                                });
                            }
                        }

                //when the dom has loaded setup form validation rules
                $(D).ready(function ($) {
                    JQUERY4U.UTIL.setupFormValidation();
                });
            })(jQuery, window, document);
        </script>
        
 
</body>
</html>

