<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teacherlevel extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','teacherlevel_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){
		$id 				= $this->uri->segment(4);	
		
		$search_condition 	= array();
		$school_id			= $this->session->userdata('user_school_id');
		
		$PerPage			= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		$level_srch			= isset($_GET['level_srch'])?$_GET['level_srch']:NULL;
		$branch_search		= isset($_GET['branch'])?$_GET['branch']:NULL;
		
		if($branch_search!=''){
			$search_condition['branch.branch_name']	= $branch_search;
		}
		
		$totrows 			= $this->teacherlevel_model->totallevel($search_condition, $school_id, $level_srch);
		
		$data 				= array();
		$data["total_rows"] = count($totrows);
		
		
		$data["base_url"] 	= base_url() . "teacherlevel/index";
		
		if($PerPage!='') {
			$data["per_page"] 		= $PerPage;
			$perpage				= $data["per_page"];
		} else {
			 $data["per_page"]		= 10;
			 $perpage				= $data["per_page"];
		}
		
		 $data["uri_segment"] 		= 3;
		 
		 $data['postfix_string'] 	= "/?level=$level_srch&perpage=$perpage";
		 
		 $this->pagination->initialize($data);
		 
		 $page 						 =  $this->uri->segment(3,0) ;
		 $data['last_page']			 = $page;
         $data["links"] 			 = $this->pagination->create_links();
		 
		 $data['level_search']		 = $level_srch;
	  
		//$data['branch'] = $this->teacherlevel_model->get_branch($school_id);
		$data['detail'] 		= $this->teacherlevel_model->get_levels($search_condition, $school_id, $data["per_page"], $page,$level_srch);
		
		$leveldata 				= $this->teacherlevel_model->get_single_record($id);
		$data['info']			= $leveldata;	
	 
					
		$this->load->view('header');

		$this->load->view('master/teacher_level' , $data);
	
		$this->load->view('footer');
		
				}
	
	public function add_new_teachlevel(){
		
		$leveldata = array();
		
		$school_id		= $this->session->userdata('user_school_id');					
		$leveldata['school_id'] = $school_id;
		
//		 if ($this->input->post('branch') != '') {
//		 $leveldata['branch_id'] = $this->input->post('branch');
//		 }
		 if($this->input->post('level')!= ''){
    	 $leveldata['teacher_level'] = $this->input->post('level');
		 }
		  if($this->input->post('description')!= ''){
		 $leveldata['level_description'] = $this->input->post('description');
		  }
		     $leveldata['is_active'] = 1 ;
		    $leveldata['is_deleted'] = 0 ;
		    $leveldata['created_by'] = $this->session->userdata('user_name');
			$leveldata['created_date'] = date('Y-m-d H:i:s');
			$leveldata['updated_date'] = date('Y-m-d H:i:s');
						
			$level = $this->input->post('level');
			//$branch = $this->input->post('branch');
//						
//		$checkteacher_level = $this->teacherlevel_model->check_teach_role($branch, $level, $school_id);
			$checkteacher_level = $this->teacherlevel_model->check_teach_role($level, $school_id);
		           $countteacherlevel = count($checkteacher_level);
				   
				   if($countteacherlevel > 0 ) {
					   
					   $this->session->set_flashdata('error', 'Teacher level already exists. Teacher level has not been added.');
				       redirect(base_url()."teacherlevel/");exit;
				
				     } else {
					     if($this->teacherlevel_model->insert_teach_level($leveldata)) {
							$this->session->set_flashdata('success', 'Teacher level has been added successfully.');
							redirect(base_url()."teacherlevel/");exit; 
						 } else {
							$this->session->set_flashdata('error', 'Some problem exists. Teacher level has not been added.');
							redirect(base_url()."teacherlevel/");exit; 
						 }
				   }
	     }
		 
		 public function update_teachlevel(){
			 $id = $this->uri->segment(4);
			  $page = $this->uri->segment(3);
			 $school_id		= $this->session->userdata('user_school_id');
			 $data							= array();
			 $id = $this->input->post('id');
			 
			 $data = array(
							//'branch_id' => $this->input->post('branch'),
							'teacher_level' => $this->input->post('level'),
							'level_description' => $this->input->post('description')
							); 
			$level = $this->input->post('level');
			//$branch = $this->input->post('branch');
			//$checkteacher_level = $this->teacherlevel_model->check_update_teach_level($branch, $level, $school_id, $id);
			
			$checkteacher_level = $this->teacherlevel_model->check_update_teach_level($level, $school_id, $id);
		     $countteacherlevel = count($checkteacher_level);
				   
				   if($countteacherlevel > 0 ) {
					    $this->session->set_flashdata('error', 'Teacher level already exists. Teacher level has not been Updated.');
				       redirect(base_url()."teacherlevel/index/".$page."/".$id);exit;
				
				     } else {
					$updatedata = $this->teacherlevel_model->update_teacher_level($id,$data);
					 if($updatedata = 1){
					 $this->session->set_flashdata('success', 'Data has been updated successfully.');
				redirect(base_url()."teacherlevel/index/".$page."/".$id);exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Teacher level has not been updated.');
				redirect(base_url()."teacherlevel/index/".$page."/".$id);exit;
			   }
			 }
		 }
		 
		  public function level_action(){
			$id = $this->uri->segment(3);  
			$activity = $this->uri->segment(4);
			$data							= array();
			if($activity == 'active'){ 
			 $data = array(
				'is_active' => '1'
				);
		  } 
		  if($activity == 'inactive'){ 
			 $data = array(
				'is_active' => '0'
				);
		  } 
		  $actiondata = $this->teacherlevel_model->teacher_action($id, $data);
		  if($actiondata = 1){	
				$this->session->set_flashdata('success', 'Teacher level has been updated successfully.');
				redirect(base_url()."teacherlevel/");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Teacher level has not been updated.');
				redirect(base_url()."teacherlevel/");exit;
			   }
			  
		  }
		  
		    public function checklevel_staff(){
			$id = $this->uri->segment(3); 
			$school_id		= $this->session->userdata('user_school_id'); 
			$data = array(
				'is_deleted' => '1'
				);
		$leveldata_rows = $this->teacherlevel_model->get_dellevel($school_id, $id);
		/*$teacher_level = $leveldata_rows->teacher_level;*/
		$teacherlevel_id = $leveldata_rows->id;
/*		$branch_id = $leveldata_rows->branch_id;		 
		$actiondata = $this->teacherlevel_model->check_staffassignlevel($teacherlevel_id, $school_id, $branch_id);*/
		  
		  $actiondata = $this->teacherlevel_model->check_staffassignlevel($teacherlevel_id, $school_id);
		  if($actiondata > 0 ) {					   
					   $this->session->set_flashdata('error', 'Teacher Level has Already Assign to staff. You Cannot Delete it');
				       redirect(base_url()."teacherlevel/");exit;	
				     }	   else {
						 			$updatedata = $this->teacherlevel_model->teacher_action($id, $data);
									if( $updatedata = 1){
					 $this->session->set_flashdata('success', 'Data has been Deleted successfully.');
				redirect(base_url()."teacherlevel/");exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Level has not been Deleted.');
				redirect(base_url()."teacherlevel/");exit;
				}
						 }
		  
		  }
	
}

