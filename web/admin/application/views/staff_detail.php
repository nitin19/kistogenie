<?php
$this->load->view('common/header');
?>
<style>
  .detailul li {
    list-style: none;
    margin: 5px 0px;
}
</style>
<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Table</a></li>
                            <li><i class="fa fa-table"></i><a href="<?php echo base_url('Dashboard');?>/view_staff">Staff table</a></li>
                           
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Staff Table</strong>
                        </div>
                        <div class="card-body">
                            
                  <!--<table id="bootstrap-data-table" class="table table-striped table-bordered">-->
                    <!------------->
                    <div class="row detailul">
                        <div class="col-md-12">
                      <div class="col-md-6">
                      <ul>
                        <li>Date         :</li>
                        <li>Title        :</li>
                        <li>Name         :</li>
                        <li>Branch       :</li>
                        <li>Date oF Birth:</li>
                        <li>Education    :</li>
                        <li>Email        :</li>
                        <li>Phone Number :</li>
                        
                        
                        
                      </ul>
                    
                    </div>
                    <div class="col-md-6">
                        <?php
                         $i=1;
                        foreach ($details as $staff) {
                        //print_r($student);
                        
                        ?>
                        
                      <ul id="std_<?php echo $i;?>">
                        <li><?php echo $staff['today_date'];?></li>
                        <li><?php echo $staff['title'];?></li>
                        <li><?php echo $staff['staff_first'];?> <?php echo $staff['staff_first'];?></li>
                        <li><?php echo $staff['branch'];?></li>
                        <li><?php echo $staff['dob'];?></li>
                        <li><?php echo $staff['education'];?></li>
                        <li><?php echo $staff['email'];?></li>
                        <li><?php echo $staff['phone'];?></li>
                        
                        
                        
                      </ul>
                    
                    <?php
                        $i++;
                      }
                     ?>
                     </div>
                     </div>
                     </div>
                  <!--  </tbody>-->
                  <!--</table>-->
                  
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->
    
<?php
$this->load->view('common/footer');
?>