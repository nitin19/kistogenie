<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="editprofile-content">
    <div class="profilemenus">
        <ul>
            <?php
                $user_type = $this->session->userdata('user_type');
                if ($user_type == 'teacher') { ?>
                    <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
                <?php } ?>
            <li><a href="<?php echo base_url(); ?>staffenrolement">Staff Process</a></li>
            </li><li class="edit"><a href="#"> Contract Staff</a></li>        
        </ul>
    </div>
<div style="clear:both"></div>
    <?php if ($this->session->flashdata('error')): ?>
        <div class="alert alert-danger alert-dismissable" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> 
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?>
    <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissable" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> 
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
<div style="clear:both"></div>
<div class="attendancesec">	
    <div class="col-sm-12 tablediv nopadding">
        <div class="col-sm-12 nopadding">
            <div class="tabletitlediv">
                <h1>Staff Registration Detail</h1>
                <div class="col-sm-12 tablenavdiv">
                    <div class="col-sm-8 tablenevleft">
                        <ul>
                            <?php if ($branch_data != '') { ?>
                                    <li><a href="<?php echo base_url(); ?>staffenrolement/traininglist/<?php echo $applied_last_page; ?>/<?php echo $branch_data; ?>">Training Staff</a></li>
                                <?php } else { ?>
                                    <li><a href="<?php echo base_url(); ?>staffenrolement/traininglist">Training Staff</a></li>
                                <?php } ?>

                            <?php if ($branch_data != '') { ?>
                                    <li><a href="<?php echo base_url(); ?>staffenrolement/dbslist/<?php echo $last_page; ?>/<?php echo $branch_data; ?>">DBS Staff</a></li>
                                <?php } else { ?>
                                    <li><a href="<?php echo base_url(); ?>staffenrolement/dbslist">DBS Staff</a></li>
                                <?php } ?>

                            <?php if ($branch_data != '') { ?>
                                    <li class="active"><a href="<?php echo base_url(); ?>staffenrolement/contractlist/<?php echo $reject_last_page; ?>/<?php echo $branch_data; ?>">Contract Staff</a></li>
                                <?php } else { ?>
                                    <li class="active"><a href="<?php echo base_url(); ?>staffenrolement/contractlist">Contract Staff</a></li>
                                <?php } ?>
                        </ul>
                    </div>
                    <div class="col-sm-4 tablenevright">
                        <form action="<?php echo base_url(); ?>staffenrolement/traininglist/0/" name="staffSearchForm" id="staffSearchForm" method="get">
                            <div class="col-sm-12 col-xs-12 applycodediv nopadding datepickerspacing">
                                <div class="col-sm-3 col-xs-3 nopadding dateblk">
                                    <div class="form-group">
                                        <div class="inputbox datepickerdiv nopadding">
                                            <?php if ($branch_data != '') {
                                                    ?>
                                                    <input type="hidden" name="branchidsrch" value="<?php echo $branch_data; ?>" />

                                                    <?php }
                                            ?>
                                            <input type="text" name="sdate" id="sdate" value="<?php echo @$sdate_search; ?>" class="datepickerattendancesummary1">
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-sm-1 datepickercenter">
                                    <span>-</span>
                                </div>
                                <div class="col-sm-3 col-xs-3 nopadding dateblk">
                                    <div class="form-group">
                                        <div class="inputbox datepickerdiv nopadding">
                                          <!--<input type="text" name="MyDate1" class="datepicker1">-->
                                            <input type="text" name="edate" id="edate" value="<?php echo @$edate_search; ?>" class="datepickerattendancesummary2">
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-sm-3 col-xs-3">
                                    <div class="form-group generatereport">
                                        <input type="submit" class="btn btn-danger confirm" value="View Staff">
                                    </div> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="tablewrapper">
            <table class="table-bordered table-striped">
                <thead>
                    <tr class="headings enrolment-headings">
                        <th class="column1">No.</th>
                        <th class="column3">Contract Type</th>
                        <th class="column3">Probation Start Date</th>
                        <th class="column3">Probation End Date</th>
                        <th class="column3">Branch</th>
                        <th class="column3">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    //print_r($dbslist); 
                        if (count($contractlist) > 0) {
                            $sr = $last_page;
                            foreach ($contractlist as $staff) {
                                $sr++;
                                $branch_id = $staff->branch_id;
                                $this->load->model(array('staffenrolement_model'));
                                $branchName = $this->staffenrolement_model->getbranchName($branch_id);
                                //print_r($branchName); 
                                ?>
                                <tr class="familydata student-registration enrolment-table">
                                    <td class="column1"><?php echo $sr; ?></td>
                                    <td class="column3"><?php echo $staff->cont_type; ?></td>
                                    <td class="column3"><?php echo $staff->probstartdate; ?></td>
                                    <td class="column3"><?php echo $staff->probenddate; ?></td>
                                    <td class="column3"><?php echo $branchName->branch_name; ?></td>
                                    <td class="column3 action-btn"><!--<a href="#">Action <i class="fa fa-angle-down"></i></a>-->
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-flat">Action</button>
                                            <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                               <li> <a href="<?php echo base_url(); ?>staffenrolement/contractview/<?php echo $staff->staff_id; ?>" target="_blank"> View </a> </li>
                                               <li> <a href="#" data-toggle="modal" data-target="#myModal_notes_<?php echo $staff->id; ?>"> Notes  </a> </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <!-- Generate Pop-up  for Notes on Enrolled staff  --> 
                            <div class="modal fade" id="myModal_notes_<?php echo $staff->id; ?>" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header" style="padding:0px;">
                                            <div class="col-sm-12 col-xs-11 nopadding invitemember">
                                                <div class="col-sm-9"><h3 class="modal-title inviteheader">Notes for <?php echo $staff->staff_fname; ?></h3></div>
                                                <div class="col-sm-3">
                                                    <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>  
                                        <div class="modal-body">
                                            <form action="<?php echo base_url(); ?>staffenrolement/notesfor_user_enrolement" name="enrolementnotesForm" id="enrolementnotesForm_<?php echo $staff->id; ?>" method='post'>
                                                <input type="hidden" name="school_id" id="school_id" value="<?php echo $staff->school_id; ?>" />
                                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $staff->id; ?>" />
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label nopadding">Notes:</label>
                                                        <div class="col-sm-9 inputbox">
                                                            <textarea class="ckeditor1" name="staff_notes" id="staff_notes_<?php echo $staff->id; ?>"> 
                                                                <?php echo $staff->staff_notes; ?>
                                                            </textarea>
                                                            <script>
                                                                CKEDITOR.replace('staff_notes_<?php echo $staff->id; ?>');
                                                            </script>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 nopadding">
                                                        <div class="confirmlink">
                                                            <div id="Loadingnotesmsg-<?php echo $staff->id; ?>"></div>
                                                            <input class="savenotes" id="notes" value="Save" type="submit">
                                                        </div>
                                                    </div>
                                                </div>  
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <?php
                        }
                    } else {
                        ?>
                        <tr><th colspan="7" style="text-align: center; width:1215px;height: 100px;font-size:25px ; background:#FFF;  color: #6a7a91;">No record to show.</th></tr>	
                    <?php } ?>
                </tbody>
            </table>
            <div class="box-footer clearfix profile-bg">
                <div class="col-sm-8"  style="margin:20px 0px;">
                    <div class="col-sm-4 paginationblk">
                        <?php
                            if (count($data_rows) > 0) {
                                $last_page1 = $last_page;
                                ?>
                                <span class="pag_txt">Showing <?php echo ++$last_page; ?> to <?php echo $sr++; ?> of <?php echo $total_rows++; ?> entries.</span>
                                <?php
                            }
                        ?>
                    </div>
                    <div class="col-sm-8 paginationblk2">	    
                        <ul class="pagination">
                            <?php echo $pagination; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 totaldiv nopadding">           
                    <div class="col-sm-12 col-xs-6 selectfilter paginationselbox nopadding">
                        <span>Showing:</span>
                        <form name="perPageForm" id="perPageForm" action="<?php echo base_url(); ?>staffenrolement/selectedstaffs/0/" method="get">
                            <input type="hidden" name="branchidsrch" id="branchidsrch" value="<?php echo @$branch_data; ?>"  />    
                            <input type="hidden" name="sdate" id="sdate" value="<?php echo $sdate_search; ?>"  />  
                            <input type="hidden" name="edate" id="edate" value="<?php echo $edate_search; ?>"  /> 
                            <select class="form-control" name="perpage" id="perpage">
                                <option >Select</option>
                                <!--<option <?php //if($per_page == "10"){ echo 'selected'; }  ?> value="10">10</option>-->
                                <option <?php if ($per_page == "20") { echo 'selected'; } ?> value="20">20</option>
                                <option <?php if ($per_page == "40") { echo 'selected'; } ?> value="40">40</option>
                                <option <?php if ($per_page == "50") { echo 'selected'; } ?> value="50">50</option>
                                <option <?php if ($per_page == "100") { echo 'selected'; } ?> value="100">100</option>
                            </select>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12 exceldiv">
                <?php
                    if (count($data_rows) > 0) {
                        ?>
                            <form method="post"  name="export_form" action="<?php echo base_url(); ?>staffenrolement/excel_action">
                                <input type="hidden" name="exceltab" id="exceltab" value="selected"  />
                                <input type="hidden" name="branch_data" id="branch_data" value="<?php echo @$branch_data; ?>"  /> 
                                <input type="hidden" name="sdate_search" id="sdate_search" value="<?php echo $sdate_search; ?>"  />  
                                <input type="hidden" name="edate_search" id="edate_search" value="<?php echo $edate_search; ?>"  /> 
                                <input type="submit" name="export" class="btn btn-success excelbtn" value="Export to Excel" />
                            </form>
                        <?php
                    }
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<input type="hidden" name="postponedate" value="<?php echo date("Y-m-d"); ?>" id="postponedate" />  
<script type="text/javascript">
    jQuery("#perpage").on('change', function (e) {
        jQuery('#perPageForm').submit();
    });
</script>
<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
<script type="text/javascript">
  function delConfirm(id) {
      if (confirm("Are you sure want to Delete?")) {
          window.location.href = "<?php echo base_url(); ?>staffenrolement/deleteenrolement/" + id;
      } else {
          return false;
      }
  }
</script>
<script>
    setTimeout(function () {
        $('.alert').fadeOut('fast');
    }, 2000);
</script>
<script>
    jQuery(document).ready(function () {
        var postpone_date = jQuery('#postponedate').val();
        jQuery.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>staffenrolement/updatestaff',
            data: {'stdate': postpone_date},
            success: function (response) {
            }
        });
        $("#sdate").keydown(false);
        $("#edate").keydown(false);
        jQuery("#staffSearchForm").validate({
            rules: {
                sdate: {
                    required: true
                },
                edate: {
                    required: true
                }
            },
            messages: {
                sdate: {
                    required: "Please select start date."
                },
                edate: {
                    required: "Please select end date."
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
<style>
    .btn-info { 
        background-color: #37B148;
        border-color: #37B148; 
    }
    .btn-info:hover {
        background-color: #37B148;
        border-color: #37B148;
        color: #fff;
    }
    .action-btn a {
        background:none;
        border:none;
    }
</style>
<style>
    #myModal { margin-top:140px; } 
    .save_btn > input {
        background: #37b248 none repeat scroll 0 0;
        border: 1px solid #ccc;
        border-radius: 3px;
        color: #fff;
        font-size: 14px;
        padding: 7px 20px;
    }
    .save_btn {
        padding-top: 20px;
    }
    .popup_save_btn {
        background: #37b248 none repeat scroll 0 0;
        border: medium none;
        border-bottom-right-radius: 3px;
        border-top-right-radius: 3px;
        color: #fff;
        padding: 12px 25px;
    }
    .text_box_sec > input {
        border: 1px solid #ccc;
        border-radius: 3px;
        min-height: 45px;
        width: 100%;
        padding: 10px;
    }
    .btn.btn-default {
        background: #37b248 none repeat scroll 0 0;
        color: #fff;
    }
    .text_box_sec > input {
        border: 1px solid #ccc;
        border-bottom-left-radius: 3px;
        border-top-left-radius: 3px;
        min-height: 44px;
        padding: 10px;
        width: 100%;
    }
    .save_btn {
        text-align: right;
    }
    .text_box_btn {
        text-align: left;
    }
    .red_line { color:#F00 !important; }
    .yellow_line { color:#990 !important; }
    .blank_trans_data {
        color: transparent;
    }
    .search {
        padding: 10px 0 60px !important;
    }
    .inviteheader{
        float:right;
        padding-right:35px;
    }
    .paginationblk2 .pagination {
        margin: 0;
    }
    .paginationblk2 {
        text-align: center;
    }
    .close.closenotify {
        background: #fff none repeat scroll 0 0;
        border: 2px solid #37b248;
        border-radius: 40px;
        color: #36b047;
        float: right;
        font-size: 28px;
        height: 40px;
        left: 30px;
        margin-top: -23px;
        opacity: 1;
        position: relative;
        width: 40px;
    }
    .modal-content.enrolmentpopup{float:left;width:100%;}
    .confirmlink .sendenrolementemail {
        border: 0 none;
        font-weight: bold;
        padding: 10px 30px;
        text-transform: uppercase;
    }
    .enrolmentpopup .modal-body{float:left;width:100%;}
    .savepostponedate {
        background: #37b248 none repeat scroll 0 0;
        border: 0 none;
        color: #fff;
        padding: 12px 30px;
    }
    .modal-dialog.popupdiv{width:90%;}
    .cke_contents.cke_reset {
        height: 350px !important;
    }
    .enrolmentpopup .modal-body {
        float: left;
        padding: 5px 0 0;
        width: 100%;
    }
    .confirmlink input{margin:6px 0px;}
    .popup_section_form {
        padding: 30px 10px;
    }
    .exceldiv{
        margin-bottom:15px;
    }
</style>