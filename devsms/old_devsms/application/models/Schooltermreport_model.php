<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Schooltermreport_model extends CI_Model {
	
	var $tbl	= "progress_report";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $classes = "classes";
	var $term = "term";
	var $progress_report_archive = 'progress_report_archive';
	
    function __construct() {
        parent::__construct();
    }
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }  
	 
	 
 	function getclasses($school_id,$schoolbranch) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$schoolbranch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	  
   function getterms($school_id) {
        $this->db->select('*');
        $this->db->from($this->term);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		$this->db->last_query(); 
        return  $query->result();
    }
	
	
	function getyears($school_id) {
        $this->db->select('*');
		$this->db->group_by('progress_report_year');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }
	
	function getRows($where=NULL){
		$this->db->select('*');
		$this->db->group_by('student_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$query = $this->db->get($this->tbl);
		 $this->db->last_query();
		return $query->num_rows();
	}
	  
	  function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL){
		$this->db->select('*');
		$this->db->group_by('student_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by("progress_report_id", "desc"); 	
		$this->db->limit($limit, $start);
		$query = $this->db->get($this->tbl);
		 $this->db->last_query(); 
		return $query->result();
	  }
	  
	  
	  function  getstudentinfo($studentid) {
        $this->db->select('*');
        $this->db->from($this->student);
		$this->db->where('student_id', $studentid);
        $query = $this->db->get();
        return  $query->row();
	   }
	   
	 function chk_archive_filename($filename) {
        $this->db->select('*');
        $this->db->from($this->progress_report_archive);
		$this->db->where('archive_filename', $filename);
        $query = $this->db->get();
        return  $query->result();
     }
	 
	 	function insertarchiveReport($archivedata){
		if($this->db->insert($this->progress_report_archive, $archivedata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	 }
  	 
}



 
