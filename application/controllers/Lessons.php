<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Lessons extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','lessons_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){
		
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;
		$subject_search					= isset($_GET['subject'])?$_GET['subject']:NULL;
		$status_search					= isset($_GET['status'])?$_GET['status']:NULL; 
		$word_search				    = isset($_GET['seachword'])?$_GET['seachword']:NULL;
		$PerPage				   		= isset($_POST['perpage'])?$_POST['perpage']:NULL;
		
		$search_condition['is_deleted']	= '0';
		 
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		if($class_search!=''){
			$search_condition['class_id']	= $class_search;
		}
		if($subject_search!=''){
			$search_condition['subject_id']	= $subject_search;
		}
		if($status_search!=''){
            $search_condition['is_active']	= $status_search;
		  }
		 
		$data							= array();
		
		$school_id		= $this->session->userdata('user_school_id');
		$data['branches'] = $this->lessons_model->getbranches($school_id);
		
		//$data['sclasses'] = $this->lessons_model->getschoolclasses($school_id);
		
		$data['title']	      			= "Lessons";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'lessons/index';
	    $config['total_rows'] 			= $this->lessons_model->getRows($search_condition,$school_id,$word_search);
		$config['per_page'] 			= $perpage;
 $config['postfix_string'] = "/?branch=$branch_search&class=$class_search&subject=$subject_search&status=$status_search&seachword=$word_search"; 
		
		$this->pagination->initialize($config);

		$odr =   "lesson_id";
		$dirc	= "desc";
		
		$data_rows= $this->lessons_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$word_search);
		
		$lessonid = $this->uri->segment(4);
		if($lessonid!='') {
		  $lesson_data_rows				= $this->lessons_model->get_single_lesson($lessonid);
		  $data['info']				   	= $lesson_data_rows[0];
		 }
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['class_search']			= $class_search;
		$data['subject_search']			= $subject_search;
		$data['status_search']			= $status_search;
		$data['word_search']		    = $word_search;
		$data['PerPage']				= $PerPage;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
						
		$this->load->view('header');
		$this->load->view('master/lessons', $data);
		$this->load->view('footer');
     }
	 
	 public function addlesson(){
		 
		    $lessondata 		= array();
			
			$school_id = $this->session->userdata('user_school_id');
			
		    $lessondata['school_id'] = $this->session->userdata('user_school_id');
			$lessondata['created_by'] = $this->session->userdata('user_name');
			$lessondata['is_active'] = '1';
			$lessondata['created_date'] = date('Y-m-d H:i:s');;
			$lessondata['updated_date'] = date('Y-m-d H:i:s');
			 
			if (trim($this->input->post('schoolbranch')) != '') {
                $lessondata['branch_id'] = trim($this->input->post('schoolbranch'));
				 $branch_id = trim($this->input->post('schoolbranch'));
            }
			
			if (trim($this->input->post('classname')) != '') {
                $lessondata['class_id'] = trim($this->input->post('classname'));
				$class_id = trim($this->input->post('classname'));
            }
			
			if (trim($this->input->post('subjectname')) != '') {
                $lessondata['subject_id'] = trim($this->input->post('subjectname'));
				$subject_id = trim($this->input->post('subjectname'));
            }
			
			if (trim($this->input->post('lessonname')) != '') {
                $lessondata['lesson_name'] = trim($this->input->post('lessonname'));
				$lesson_name = trim($this->input->post('lessonname'));
            }
			
			if (trim($this->input->post('lessondescription')) != '') {
                $lessondata['lesson_description'] = trim($this->input->post('lessondescription'));
            }
		
			 $checkLessonname = $this->lessons_model->check_lesson_name($school_id,$branch_id,$class_id,$subject_id,$lesson_name);
			 $countLessonname = count($checkLessonname);
			 
			 if( $countLessonname > 0 ) {
				  $this->session->set_flashdata('error', 'Lesson already exists. Lesson has not been added.');
				  redirect(base_url()."lessons/");exit;
			 } else {
			
		   if($this->lessons_model->insertLesson($lessondata)) {
			   $this->session->set_flashdata('success', 'Lesson has been added successfuly.');
			   redirect(base_url()."lessons/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Lesson has not been added.');
				redirect(base_url()."lessons/");exit;
		      }
			}
			
        }
		
		
		public function updatelesson(){
			
			  $start						    = $this->uri->segment(3);
		      $editedLessonid					= $this->uri->segment(4); 
			  $Action							= $this->uri->segment(5); 
		 
		  $postURL	= $this->createPostURL($_GET);
		if($editedLessonid==NULL){
			$this->session->set_flashdata('error', 'Select Lesson first.');
			redirect(base_url()."lessons");exit;
		   }
		   
		 $lessondata 		= array();
         $school_id = $this->session->userdata('user_school_id');
		 $where							= array('lesson_id'=>$editedLessonid);
		 $data['title']	      			= "Edit Lesson";
		 $data['mode']					= "Edit";
			
			if (trim($this->input->post('schoolbranch')) != '') {
                $lessondata['branch_id'] = trim($this->input->post('schoolbranch'));
				 $branch_id = trim($this->input->post('schoolbranch'));
            }
			
			if (trim($this->input->post('classname')) != '') {
                $lessondata['class_id'] = trim($this->input->post('classname'));
				$class_id = trim($this->input->post('classname'));
            }
			
			if (trim($this->input->post('subjectname')) != '') {
                $lessondata['subject_id'] = trim($this->input->post('subjectname'));
				$subject_id = trim($this->input->post('subjectname'));
            }
			
			if (trim($this->input->post('lessonname')) != '') {
                $lessondata['lesson_name'] = trim($this->input->post('lessonname'));
				$lesson_name = trim($this->input->post('lessonname'));
            }
			
			if (trim($this->input->post('lessondescription')) != '') {
                $lessondata['lesson_description'] = trim($this->input->post('lessondescription'));
            }
			
			$lessondata['updated_date'] = date('Y-m-d H:i:s');
			
  $checkLessonname = $this->lessons_model->check_edited_lessonname($school_id,$branch_id,$class_id,$subject_id,$lesson_name,$editedLessonid);
			 $countLessonname = count($checkLessonname);
			 
			 if( $countLessonname > 0 ) {
				  $this->session->set_flashdata('error', 'Lesson already exists. Lesson has not been updated.');
				  redirect(base_url()."lessons/index/$start/$editedLessonid/edit/");exit;
			 } else {
			
		   if($this->lessons_model->updateLesson($lessondata,$where)) {
			   $this->session->set_flashdata('success', 'Lesson has been updated successfuly.');
			   redirect(base_url()."lessons/index/$start/$editedLessonid/edit/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Lesson has not been updated.');
				redirect(base_url()."lessons/index/$start/$editedLessonid/edit/");exit;
		      }
			}
			
	   }
	   
	   
	   	function deactivatelesson(){
		
		  $start						    = $this->uri->segment(3);
		  $lessonid						    = $this->uri->segment(4); 
		  $where							= array('lesson_id'=>$lessonid);
		  $data								= array('is_active'=>'0');
		if($this->lessons_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Lesson has been updated successfuly.');
			redirect(base_url()."lessons/index/$start/$lessonid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Lesson has not been updated.');
			redirect(base_url()."lessons/index/$start/$lessonid");exit;
		}
	}
	
	function activatelesson(){
		 
		  $start						    = $this->uri->segment(3);
		  $lessonid							= $this->uri->segment(4); 
		  $where								= array('lesson_id'=>$lessonid);
		  $data								= array('is_active'=>'1');
		if($this->lessons_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Lesson has been updated successfuly.');
			redirect(base_url()."lessons/index/$start/$lessonid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Lesson has not been updated.');
			redirect(base_url()."lessons/index/$start/$lessonid");exit;
		}
		
	}
	
	
		public function deletelesson($lessonid) {
			
			if($this->lessons_model->delete($lessonid)){
					$this->session->set_flashdata('success', 'Lesson has been deleted successfuly.');
					redirect(base_url()."lessons/index");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."lessons/index");exit;
		          }
	     }
		
		
		 public function check_classes()

		{     
		    $school_id = $this->session->userdata('user_school_id');
			$schoolbranch = $_POST['schoolbranch']; 
	        $getclasses = $this->lessons_model->getclasses($schoolbranch,$school_id);
			foreach($getclasses as $getclass) {
 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
			}
	     }
		 
		 
		 public function check_subjects()

		{     
		    $school_id = $this->session->userdata('user_school_id');
			$schoolbranch = $_POST['schoolbranch']; 
			$classname = $_POST['classname']; 
	        $getsubjects = $this->lessons_model->getsubjects($school_id,$schoolbranch,$classname);
			foreach($getsubjects as $getsubject) {
 			 echo '<option value="'.$getsubject->subject_id.'">'.$getsubject->subject_name.'</option>';
			}
	     }
		
	
		function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	   }
	  
	
}