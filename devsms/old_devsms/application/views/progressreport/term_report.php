<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li><a href="#"> Progress Reports</a></li>

        <li class="edit"><a href="#">Term </a></li>        

        </ul>

        </div>

        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>

        </div>

        <div class="termreportsection">	

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

<form action="<?php echo base_url();?>termreport/index" name="termreportForm" id="termreportForm">

    <div class="col-sm-12 col-xs-12 applycodediv nopadding">

<div class="col-sm-2 nopadding selectoption onethirdfilter">

<div class="form-group fullwidthinput">

    <label class="col-sm-4 control-label nopadding">Branch</label>

    <div class="col-sm-8 inputbox nopadding">

 <select class="form-control" name="branch" id="branch">
      <option value="">Select Branch</option>   
    <?php foreach($branches as $branch) { ?>
     <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
    <?php  } ?>
      </select>
      
    </div>

  </div>

</div>

<div class="col-sm-2 nopadding selectoption onethirdfilter">

<div class="form-group fullwidthinput">

    <label class="col-sm-4 control-label nopadding">Class</label>

    <div class="col-sm-8 inputbox nopadding">

      <select class="form-control" name="class" id="class">
         <option value="">Select Class</option>
         <?php 
  if( @$_GET['branch']!='') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = @$_GET['branch'];
		  $this->load->model(array('termreport_model'));
		  $sclasses =$this->termreport_model->getclasses($school_id,$schoolbranch);
     foreach($sclasses as $sclass) { ?>
          <option <?php if($class_search == $sclass->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>   
        </select>

    </div>

  </div>

</div>

<div class="col-sm-2 nopadding selectoption onethirdfilter">

<div class="form-group fullwidthinput">

    <label class="col-sm-4 control-label nopadding">Term</label>

    <div class="col-sm-8 inputbox nopadding">

<select class="form-control" name="term" id="term">
  <option value="">Select</option>  
  
  <?php 
  if( @$_GET['branch']!='') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = @$_GET['branch'];
		  $this->load->model(array('termreport_model'));
		  $sterms =$this->termreport_model->getterms($school_id,$schoolbranch);
     foreach($sterms as $sterm) { ?>
          <option <?php if($term_search == $sterm->term_id){ echo 'selected'; } ?> value="<?php echo $sterm->term_id;?>"><?php echo $sterm->term_name;?></option>
        <?php  } 
          }
	    ?>     
</select>

    </div>

  </div>

</div>

				
<div class="col-sm-2 nopadding selectoption onethirdfilter">

<div class="form-group fullwidthinput">

    <label class="col-sm-4 control-label nopadding">Year</label>

    <div class="col-sm-8 inputbox nopadding">

 <select class="form-control" name="year" id="year">
  <option value="">Select</option> 
   <?php 
  if( @$_GET['branch']!='') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = @$_GET['branch'];
		  $this->load->model(array('termreport_model'));
		  $syears =$this->termreport_model->getyears($school_id,$schoolbranch);
     foreach($syears as $syear) { ?>
          <option <?php if($year_search == $syear->progress_report_year){ echo 'selected'; } ?> value="<?php echo $syear->progress_report_year;?>"><?php echo $syear->progress_report_year;?></option>
        <?php  } 
          }
	    ?>    

</select>

    </div>

  </div>

</div>

<!--<div class="col-sm-3 searching termsearch">
        <div class="form-group">
     <input type="text" class="form-control searchbox" placeholder="Search">
     <input type="submit" value="" class="searchbtn">
     </div>
        </div>-->

<div class="col-sm-3 rightspace selectoption viewreport topspacing tabfilterhalf">

<div class="form-group">

    <input type="submit" class="btn btn-danger" value="View Report">

  <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>

  </div>

</div>


</div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">
    
<?php if(@$_GET['branch']!='' && @$_GET['class']!='' ) { ?>

<?php  $Totalrec = $total_rows; ?>

    <h1>Generate Report</h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">No.</th>

					  <th class="column6">First Name</th>

					  <th class="column6">Last Name</th>

					  <th class="column2">Gender</th>

					  <th class="column2"> View</th>

                      <th class="column2">Modify Reports</th>

                     

				  </tr>

			  </thead>

				<tbody>
                
                <?php 
					  if(count(@$data_rows) > 0){
					      $sr=$last_page;
			 foreach($data_rows as $reports) { 
					     $sr++;
		        $studentid = $reports->student_id; 
		        $this->load->model(array('termreport_model'));
		        $studentinfo =$this->termreport_model->getstudentinfo($studentid);
			     ?>

					<tr class="familydata term">

						<td class="column1"><?php echo $sr; ?></td>

						<td class="column6"><?php echo $studentinfo->student_fname; ?></td>

						<td class="column6"><?php echo $studentinfo->student_lname;?></td>

                        <td class="column2"><?php echo $studentinfo->student_gender;?></td>

        <td class="column2"><a href="<?php echo base_url();?>viewprogressreport/index/<?php echo $studentinfo->student_id;?>/<?php echo @$_GET['term']?>/<?php echo @$_GET['year']?>"><i class="fa fa-eye"></i> </a><span class="blankblk"></span></td>

	<td class="column2"><a href="<?php echo base_url();?>modifyprogressreport/index/<?php echo $studentinfo->student_id;?>/<?php echo @$_GET['term']?>/<?php echo @$_GET['year']?>"><i class="fa fa-edit"></i> </a><span class="blankblk">-</span></td>

                        </tr>
						
				  <?php	}		
					} else { ?>
             <tr><th colspan="7" style="text-align: center; width:1215px; height:50px; background:#FFF;">No record found.</th></tr>	
				   <?php } ?>  

				</tbody>

		  </table>

          

     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-8">
    
    <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
			<?php  } ?>

	<ul class="pagination">
    
<?php echo $pagination;?>
<!--	<li><a href="#"><img src="<?php //echo base_url();?>assets/images/lefticon.png"></a></li>

	<li><a href="#">1</a></li>

	<li class="active"><a href="#">2</a></li>

	<li><a href="#">...</a></li>

	<li><a href="#">34</a></li>

	<li><a href="#"><img src="<?php //echo base_url();?>assets/images/righticon.png"></a></li>
-->
	</ul>

    </div>

    <div class="col-sm-4 totaldiv nopadding">

    <div class="col-sm-6">

    <h3><?php echo $Totalrec ;?> Students Total</h3>

    </div>

    

    <div class="col-sm-6 col-xs-5 selectfilter paginationselbox nopadding">

      <span>Showing:</span>

 <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>termreport">
 
<input type="hidden" name="branch" id="branch" value="<?php echo @$_GET['branch'];?>" />
<input type="hidden" name="class" id="class" value="<?php echo @$_GET['class'];?>" />
<input type="hidden" name="term" id="term" value="<?php echo @$_GET['term'];?>" />
<input type="hidden" name="year" id="year" value="<?php echo @$_GET['year'];?>" />

  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>

  <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>

    </div>

	</div>

    </div>     

</div>


    <?php } else { ?>
    
    <div class="beforeresult_section"><h1></h1></div>

     <?php } ?>

	</div>

    

	</div>

    </div>

    </div>
    
    
    <script>
  jQuery(document).ready(function(){
	  
	    jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
	  
	  	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#termreportForm")[0].reset();
			   window.location.href='<?php echo base_url()?>termreport/index';
			}); 
			
	  jQuery("#branch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			jQuery("#class").find('option[value!=""]').remove();
			jQuery("#term").find('option[value!=""]').remove();
			jQuery("#year").find('option[value!=""]').remove();
			
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>termreport/check_filters',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(data) {
				        data = jQuery.parseJSON(data);
					    jQuery('#class').append(data.Class);
						jQuery('#term').append(data.Term);
						jQuery('#year').append(data.Year);
			          }
				   });
				 } else {
			     jQuery("#class").find('option[value!=""]').remove();
				 jQuery("#term").find('option[value!=""]').remove();
			     jQuery("#year").find('option[value!=""]').remove();
				 return false;
				 }
			 });
			 
			 
	    jQuery("#termreportForm").validate({
        rules: {
					
              branch: {
                 required: true
                 },
			  class: {
    					required: true
   					}, 
			  term: {
                required: true
                 },
			  year: {
                required: true
                 }
            },
        messages: {
             branch: {
                  required: "Please select branch."
                 },
			  class: {
    					required: "Please select class."
   					},
			  term: {
                   required: "Please select term."
                 },
			  year: {
                   required: "Please select year."
                 }
             },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		
			 
	 });
		 
</script>
<style>
 .beforeresult_section > h1 {
    min-height: 300px;
	}
</style>