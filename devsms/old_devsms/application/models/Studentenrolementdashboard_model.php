<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Studentenrolementdashboard_model extends CI_Model {
	var $tbl	= "student_enrolement";
	var $users = "users";
	var $nationality = "nationality";
	var $country = "country";
	var $religion = "religion";
	var $ethnic_origin = "ethnic_origin";
	var $branch = "branch";
	var $fee_band = "fee_band";
	var $classes = "classes";
	var $attendance = "attendance";

    function __construct() {
        parent::__construct();
    }
	  
	  
   function getnationality() {
        $this->db->select('*');
        $this->db->from($this->nationality);
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
    }
	
	function getcountry() {
        $this->db->select('*');
        $this->db->from($this->country);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getreligion() {
        $this->db->select('*');
        $this->db->from($this->religion);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getethnicorigin() {
        $this->db->select('*');
        $this->db->from($this->ethnic_origin);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
   
   	function get_single_student($user_id) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.user_id'=>$user_id));
          $query = $this->db->get();
          return  $query->row();
    }
	
	function updateUser($userdata,$whereuser){
		$this->db->where($whereuser);
		if($this->db->update($this->users, $userdata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 function udateStudent($studentdata,$wherestudent){
		$this->db->where($wherestudent);
		if($this->db->update($this->tbl, $studentdata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	
}
