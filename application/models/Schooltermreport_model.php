<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Schooltermreport_model extends CI_Model {
	
	var $tbl	= "progress_report";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $classes = "classes";
	var $term = "term";
	var $staff = "staff";
	var $progress_report_archive = 'progress_report_archive';
	var $student_report_levels = 'student_report_levels';
	var $subjects = "subjects";
	
    function __construct() {
        parent::__construct();
    }
	
	function getstaff_branches($staff_branch_ids) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id"=>$staff_branch_ids,"is_active"=>'1',"is_deleted"=>'0'));
        $query = $this->db->get();
      //  echo $this->db->last_query();
        return  $query->row();
    }
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }  
	 
	 
 	function getclasses($school_id,$schoolbranch) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$schoolbranch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	  
   function getterms($school_id) {
        $this->db->select('*');
        $this->db->from($this->term);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		$this->db->last_query(); 
        return  $query->result();
    }
	
	
	function getyears($school_id) {
        $this->db->select('*');
		$this->db->group_by('progress_report_year');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }
	
	function getRows($where=NULL){
		$this->db->select('*');
		$this->db->group_by('student_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$query = $this->db->get($this->tbl);
		 $this->db->last_query();
		return $query->num_rows();
	}
	  
	  function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL){
		$this->db->select('*');
		$this->db->group_by('student_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by("progress_report_id", "desc"); 	
		if($limit!='all'){
			$this->db->limit($limit, $start);
		}
		$query = $this->db->get($this->tbl);
		 $this->db->last_query(); 
		return $query->result();
	  }
	  
	  
	  function  getstudentinfo($studentid) {
        $this->db->select('*');
        $this->db->from($this->student);
		$this->db->where('student_id', $studentid);
        $query = $this->db->get();
        $this->db->last_query();
        return  $query->row();
	   }
	   
	 function chk_archive_filename($filename) {
        $this->db->select('*');
        $this->db->from($this->progress_report_archive);
		$this->db->where('archive_filename', $filename);
        $query = $this->db->get();
        return  $query->result();
     }
	 
	 	function insertarchiveReport($archivedata){
		if($this->db->insert($this->progress_report_archive, $archivedata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	 }

	 function get_print_pdf_data($chk_student_ids_str,$where=NULL){
	 	$this->db->select('*');
		$this->db->group_by('student_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where_in('student_id', $chk_student_ids_str);
		$this->db->order_by("progress_report_id", "desc"); 	
		$query = $this->db->get($this->tbl);
		 $this->db->last_query(); 
		return $query->result();
	  }
	 function get_term_reports($student_id,$where=NULL ,$class_id) {

		$this->db->select('*');
        
        $this->db->join($this->subjects, $this->subjects.'.subject_id = '.$this->tbl.'.subject_id','INNER');
        $this->db->from($this->tbl);

        if($where != NULL){
			$this->db->where($where);
		}

		$this->db->where(array("student_id"=>$student_id , $this->tbl.".class_id"=>$class_id ));
		$this->db->order_by($this->subjects.'.subject_name', "desc");
        $query = $this->db->get();
		$this->db->last_query(); 
         return  $query->result();
	   } 
	   function  getTerminfo($term_id) {
        $this->db->select('*');
        $this->db->from($this->term);
		$this->db->where('term_id', $term_id);
        $query = $this->db->get();
        return  $query->row();
	 }
	 
	 function  getBranchinfo($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
	 }
	 
	 function  getClassinfo($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where('class_id', $class_id);
        $query = $this->db->get();
        return  $query->row();
	 }
  	 
  	 	function get_subject_name($subject_id) {
        $this->db->select('*');
        $this->db->from($this->subjects);
		$this->db->where(array("subject_id"=>$subject_id));
        $query = $this->db->get();
      //  echo $this->db->last_query();
        return  $query->row();
    }
     	 	function get_staff_name($staff_id) {
        $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("staff_id"=>$staff_id));
        $query = $this->db->get();
        //echo $this->db->last_query();
        return  $query->row();
    }
    	function get_current_level($current_level) {
          $this->db->select('*');
          $this->db->from($this->student_report_levels);
		  $this->db->where(array('level_id' => $current_level));
          $query = $this->db->get();
		 // echo $this->db->last_query();
          return  $query->row();
     }
}



 
