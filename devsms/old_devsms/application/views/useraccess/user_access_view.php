<div class="editprofile-content">
    	<div class="profilemenus">
        <ul>
		<li><a href="#">Home</a></li>
        <li class="edit"><a href="#">User Access System</a></li>        
        </ul>
        </div>
        
        <div class="user_acceess_system">
			<div class="col-sm-12 nopadding">
				<div class="col-sm-12 nopadding acc_systm">
						<div class="col-sm-6 nopadding">
							<h1>User Access System</h1>
						</div>	
						<div class="col-sm-6 nopadding">
							<form action="<?php echo base_url();?>useraccess/index/0/" name="useraccesssearchForm" class="navbar-form searchbar navbar-right searchbar-wdth" method="get">
								 <div class="form-group">
  								<input type="text" class="form-control" placeholder="Search" name="seachword" id="seachword" value="<?php echo $word_search;?>">
								 </div>
								<input type="submit" value="" class="submitbtn" style="margin-top:7px;">
							 </form>
						</div>

				</div>
                
                  <?php 
				        $counter = 0;
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $staff) { 
					      $sr++;
						  $counter++;
						  
					   $branch_id_Arr = explode(',', $staff->branch_id); 
					   $this->load->model(array('useraccess_model'));
		               $staffAccessdata = $this->useraccess_model->get_staff_access($staff->school_id,$staff->user_id);
					     ?> 
                       
			     	<div class="access_profile_dtl <?php if( $counter!=1) { echo 'mgn_top'; } ?>">
					<div class="col-sm-2 img-pd-lft">
                          <?php if($staff->profile_image !=''){?>
     <img src="<?php echo base_url();?>uploads/staff/<?php echo $staff->profile_image ; ?>" class="" style="width:95px; height:94px;">
                         <?php } else { ?>
                           <img src="<?php echo base_url();?>assets/images/pro_pic.png" class="">
                           <?php } ?>
					</div>
					<div class="col-sm-10">
                    <div class="col-sm-8 nopadding">
						<h1 class="usr_name"><?php echo $staff->staff_fname.' '.$staff->staff_lname ; ?></h1>
						<h4>Email Address:<span class="eml_text"> <?php echo $staff->email ; ?></span></h4>
						<hr>
                        </div>
                       <div class="col-sm-4">
                    
                     <a type="button" href="<?php echo base_url();?>staff/view_user/<?php echo $staff->user_id;?>" class="btn btn-success" target="_blank"/>View Profile</a>
                     
             
                     </div>  
						<div class="chkboxes">
				<form>
              
                <input type="hidden" name="school_id" id="school_id" value="<?php echo $staff->school_id;?>"  />   
                
                    <div class="sms_acc_sys col-sm-12 nopadding">
                        <h1 class="checkboxess_heading">SMS Access System</h1>
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "students_menu" id = "students-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->students == "Yes"){ echo 'checked'; } ?>> Students
                        </label>
                        <label class = "checkbox-inline">
        <input type = "checkbox" class="useraccessmenus" name = "student_attendance_menu" id = "student_attendance-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->student_attendance == "Yes"){ echo 'checked'; } ?>> Students Attendance
                        </label>
                        
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "progress_reports_menu" id = "progress_reports-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->progress_reports == "Yes"){ echo 'checked'; } ?>> Progress Report
                        </label>
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "student_enrolement_menu" id = "student_enrolement-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->student_enrolement == "Yes"){ echo 'checked'; } ?>> Student Enrollment
                        </label>
                         <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "staff_menu" id = "staff-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->staff == "Yes"){ echo 'checked'; } ?>> Staff 
                        </label>
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "staff_enrolement_menu" id = "staff_enrolement-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->staff_enrolement == "Yes"){ echo 'checked'; } ?>> Staff Enrollment
                        </label>
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "staff_attendence_menu" id = "staff_attendence-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->staff_attendence == "Yes"){ echo 'checked'; } ?>> Staff Attendance
                        </label>
                        
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "staff_salary_menu" id = "staff_salary-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->staff_salary == "Yes"){ echo 'checked'; } ?>> Staff Salary
                        </label>
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "teaching_learing_menu" id = "teaching_learing-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->teaching_learing == "Yes"){ echo 'checked'; } ?>> Teaching Learning
                        </label>
                        
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "documents_menu" id = "documents-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->documents == "Yes"){ echo 'checked'; } ?>> Documents
                        </label>
                        
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "configurations_menu" id = "configurations-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->configurations == "Yes"){ echo 'checked'; } ?>> Configuration
                        </label>
                        <label class = "checkbox-inline">
                                <input type = "checkbox" class="useraccessmenus" name = "basic_masters_menu" id = "basic_masters-<?php echo $staff->user_id;?>" value = "Yes" <?php if(@$staffAccessdata->basic_masters == "Yes"){ echo 'checked'; } ?>> Basic Setup
                        </label>
                        
                    </div>
                    
					<div class="sms_acc_sys col-sm-12 nopadding">
									<h1 class="checkboxess_heading">Branch Access System </h1>
                                    
                              <?php  if( count($branches) > 0 ) {
								  $branchcounter = 0;
							       foreach($branches as $branch) {
									    $branchcounter++;
								      ?>      
									<label class="checkbox-inline">
	<input type="checkbox" class="branchName" name="branch_name[]" id="branch_user-<?php echo $staff->user_id;?>" value="<?php echo $branch->branch_id;?>" <?php if(in_array($branch->branch_id, $branch_id_Arr)){ echo 'checked'; } ?>> <?php echo $branch->branch_name;?>
									</label>
                                    <?php }
							            } ?>
									
								</div>
                                
				</form>
						</div>
					</div>
				</div>
                
                <?php	}		
					} else { ?>
                    <div class="profile-bg">
	<div class="col-sm-12 paginationdiv nopadding">
    <div class="col-sm-12"> 
	<ul class="pagination">
    <li><th colspan="7" style="text-align: center; width:1215px;height:100px;font-size:25px; background:#FFF;  color: #6a7a91;">No record to show.</th></li>
	</ul>
  </div>
	</div>
    </div>
                        
                    <?php } ?>
                
			</div>
	
    </div>
	<!--<div class="profile-bg">
	<div class="col-sm-12 paginationdiv nopadding">
    <div class="col-sm-12">
     
	<ul class="pagination">
    <?php echo $pagination;?>
<!--	<li><a href="#"><img src="<?php //echo base_url();?>assets/images/lefticon.png"></a></li>
	<li><a href="#">1</a></li>
	<li class="active"><a href="#">2</a></li>
	<li><a href="#">...</a></li>
	<li><a href="#">34</a></li>
	<li><a href="#"><img src="<?php //echo base_url();?>assets/images/righticon.png"></a></li>
	</ul>-->
     <div style="text-align:center">  
     <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries.
			<?php  } ?>
            </div>
                       	<ul class="pagination">
		<?php echo $pagination;?>	
	</ul>
    </div>
  
	</div>
    </div>  
    </div>
    
 <script>
  jQuery(document).ready(function(){
	 // jQuery("#branch").on('change', function(e){ 
	   jQuery(".useraccessmenus").on('click', function(e){ 
		     var school_id = jQuery('#school_id').val();
			 var accessMenuIdstr = jQuery(this).attr('id');
			 var accessMenuIdArr = accessMenuIdstr.split("-");
			 var accessMenuName = accessMenuIdArr[0];
			 var accessuserId = accessMenuIdArr[1];
			 
			if(jQuery(this).prop("checked") == true){
                   var accessMenuvalue = 'Yes';
            } else {
				   var accessMenuvalue = 'No';
			    }
			
		  if(accessuserId!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>useraccess/add_user_access',
            data:{ 'school_id': school_id,'accessuserId': accessuserId,'accessMenuName': accessMenuName,'accessMenuvalue': accessMenuvalue },
            success :  function(response) {
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
		jQuery(".branchName").on('click', function(e){ 
		
		     var school_id = jQuery('#school_id').val();
		     var branch_userid = jQuery(this).attr('id');
			 var userid = branch_userid.replace('branch_user-','');
   		     var branch_id = jQuery(this).val();
			
		   if(jQuery(this).prop("checked") == true){
                  var actionValue = 'add';
                } else {
				   var actionValue = 'remove';
			    }
		   
		  if(branch_id!='' && userid!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>useraccess/update_staff_branch',
            data:{ 'school_id': school_id,'userid': userid,'branch_id':branch_id,'actionValue':actionValue},
            success :  function(data) {
			          }
				   });
				 } else {
				 return false;
				 }
				 
			 });

	});
</script>
   <style>
   .profile-bg {
    margin-top: 10px;
}
 </style>