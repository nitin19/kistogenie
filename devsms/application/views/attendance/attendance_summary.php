<div class="editprofile-content">

    	<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

             <ul>

		          <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

           <?php  } elseif($user_type == 'student'){?>

              <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>
    <?php }else {?>
          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
    <?php } ?>

        <li><a href="<?php echo base_url(); ?>modifyattendance">Attendance</a></li>


        <li class="edit">Summary</li>        

        </ul>

        </div>


        </div>

        
 <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>
        

      <div class="attendancesec">	

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

       <form action="<?php echo base_url();?>attendancesummary/index" name="attendancesummaryForm" id="attendancesummaryForm" autocomplete="off">

                <div class="col-sm-12 col-xs-12 applycodediv">

                <div class="col-sm-4 nopadding tabfilterhalf">

                <div class="col-sm-2 col-xs-2 selectfilter datetext nopadding">

                <span>Date:</span>

                </div>

                <div class="col-sm-9 col-xs-10 nopadding datepickdiv">

                <div class="col-sm-5 col-xs-5 nopadding dateblk">

        		<div class="form-group">

    <div class="inputbox datepickerdiv nopadding">

      <input type="text" name="sdate" id="sdate" value="<?php echo @$sdate_search;?>" class="datepickerattendancesummary1">

    </div>

  </div> 

  				</div>

                <div class="col-sm-1 col-xs-1 dividerline nopadding"><span><img src="<?php echo base_url();?>assets/images/between.png"></span> </div>

                <div class="col-sm-5 col-xs-5 nopadding dateblk">

  				<div class="form-group">

    <div class="inputbox datepickerdiv nopadding">

      <input type="text" name="edate" id="edate" value="<?php echo @$edate_search;?>" class="datepickerattendancesummary2">

    </div>

  </div>

  </div>

</div>

</div>

	<?php /*?><div class="col-sm-2 nopadding selectoption tabfilterhalf resselectfil mobhalffilter">

<div class="form-group fullwidthinput">

    <label class="col-sm-6 control-label nopadding">Attendance Type</label>

    <div class="col-sm-6 inputbox nopadding">
		
      <select class="form-control" name="type" id="type">
     	<option value="">Select</option>
  		<option <?php if($type_search == 1){ echo 'selected'; } ?> value="1">Present</option>
  		<option <?php if($type_search == 2){ echo 'selected'; } ?> value="2">Absent</option>
  		<option <?php if($type_search == 3){ echo 'selected'; } ?> value="3">Late</option>
	 </select>

      

    </div>

  </div>

</div><?php */?>

 <div class="col-sm-8 nopadding selectoption tabfilterfull mobfullfilter">

   <div class="col-sm-4 nopadding resselectfil">

    <div class="form-group fullwidthinput">
    
    <label class="col-sm-4 control-label">Branch:</label>

    <div class="col-sm-8 inputbox nopadding">

<?php /*?>      <select class="form-control" name="branch" id="branch">
      <option value="">Select Branch</option>   
    <?php foreach($branches as $branch) { ?>
     <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
    <?php  } ?>
      </select><?php */?>
      
      
            <select class="form-control" name="branch" id="branch">
  <option value="">Select Branch</option>    
  <?php
  
   $staff_branch_id = $this->session->userdata('staff_branch_id');
   $user_type = $this->session->userdata('user_type');
   
     if($user_type=='teacher' && $staff_branch_id!=''){
      $staff_branch = explode(',',$staff_branch_id); 
	  foreach($staff_branch as $staff_branch_ids){
		   $this->load->model(array('attendancesummary_model'));
	 $branchdta =$this->attendancesummary_model->getstaff_branches($staff_branch_ids);
	  ?>
	 <option <?php if($branch_search == $branchdta->branch_id){ echo 'selected'; } ?> value="<?php echo $branchdta->branch_id; ?>"><?php echo $branchdta->branch_name;?></option>
	 <?php  }  } 
	 else if($user_type=='admin'){
	 
	 	foreach($branches as $branch){
	 ?>
     	  <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
		<?php  
		   
   }}
   else{
   	 ?>
     <option value=""></option>
     <?php } ?>
</select>


    </div>

  </div>

</div>

<div class="col-sm-4 nopadding selectoption resselectfil">

<div class="form-group fullwidthinput">


    <label class="col-sm-4 control-label">Class:</label>

    <div class="col-sm-8 inputbox nopadding">

      <select class="form-control" name="class" id="class">
         <option value="">Select Class</option>
         <?php 
  if( @$_GET['branch']!='') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = @$_GET['branch'];
		  $this->load->model(array('attendancesummary_model'));
		  $sclasses =$this->attendancesummary_model->getclasses($school_id,$schoolbranch);
     foreach($sclasses as $sclass) { ?>
          <option <?php if($class_search == $sclass->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>   
        </select>

    </div>

  </div>

</div>

<div class="col-sm-4 rightspace selectoption resselectfil">

<div class="form-group generatereport">

    <input type="submit" class="btn btn-danger confirm" value="View Student">

     <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>

  </div>

</div>

				</div>

        </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding fullwidsec">

<?php if(@$_GET['sdate']!='' && @$_GET['edate']!='' ) { ?>

    <h1>Summary</h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2">No.</th>

					  <th class="column3">First Name</th>

					  <th class="column3">Last Name</th>

					  <th class="column3">Present</th>

					  <th class="column3"> Late</th>

                      <th class="column3"> Absent</th>

                      <!--<th class="column3">Unauth Absent</th>-->

                      <th class="column3">Total Sessions</th>

				  </tr>

			  </thead>

				<tbody>
                
                     <?php 
					  if(count(@$data_rows) > 0){
						 
						 $totalrecords = count($data_rows);
						  
						  $present_attendance_Arry  = array();
						  $absent_attendance_Arry  = array();
						  $late_attendance_Arry  = array();
						  $total_session_Arry  = array();
						  
					      $sr=$last_page;
					  foreach($data_rows as $student) { 
					     $sr++;
		        $studentid = $student->student_id; 
		       $this->load->model(array('attendancesummary_model'));
		       $studentinfo =$this->attendancesummary_model->getstudentinfo($studentid);
			   
			       $startdate	= date('Y-m-d', strtotime($sdate_search));
			       $enddate	= date('Y-m-d', strtotime($edate_search));
			   
			    $totalAttendancestatus = $this->attendancesummary_model->get_total_Attendancestatus($studentid,$startdate,$enddate);
			
				$presentAttendancestatus = $this->attendancesummary_model->get_present_Attendancestatus($studentid,$startdate,$enddate);
				
				$absentAttendancestatus = $this->attendancesummary_model->get_absent_Attendancestatus($studentid,$startdate,$enddate);
				
				$lateAttendancestatus = $this->attendancesummary_model->get_late_Attendancestatus($studentid,$startdate,$enddate);
			  
			   $present_percentage =  ($presentAttendancestatus * 100) / $totalAttendancestatus;
			   $absent_percentage =  ($absentAttendancestatus * 100) / $totalAttendancestatus;
			   $late_percentage =  ($lateAttendancestatus * 100) / $totalAttendancestatus;
			   
			     
			       $present_attendance_Arry[] = $present_percentage;
				   $absent_attendance_Arry[] = $absent_percentage;
				   $late_attendance_Arry[] = $late_percentage;
			       $total_session_Arry[]  = $totalAttendancestatus;
				   
					   ?>

					<tr class="familydata summary">

						<td class="column2"><?php echo $sr; ?></td>

						<td class="column3"><?php if($studentinfo->student_fname!='') { echo $studentinfo->student_fname; } else { echo '<span style="color:transparent;">-</span>'; } ?></td>

						<td class="column3"><?php if($studentinfo->student_lname!='') { echo $studentinfo->student_lname; } else { echo '<span style="color:transparent;">-</span>'; } ?></td>

                        <td class="column3">(<?php echo  ceil($present_percentage);?>%)</td>

                        <td class="column3">(<?php echo  ceil($late_percentage);?>%)</td>

                        <td class="column3">(<?php echo  ceil($absent_percentage);?>%)</td>

						<!--<td class="column3">0 : (0%)</td>-->

                        <td class="column3"><?php echo  $totalAttendancestatus;?></td>

                        </tr>
						
                      <?php } ?>

                       <?php  
					   $avg_present_attendance = array_sum($present_attendance_Arry) / count($present_attendance_Arry);
					   $avg_late_attendance = array_sum($late_attendance_Arry) / count($late_attendance_Arry);
					   $avg_absent_attendance = array_sum($absent_attendance_Arry) / count($absent_attendance_Arry);
					   $avg_session_attendance = array_sum($total_session_Arry) / count($total_session_Arry);
					   
					   
					   
					   ?>

                    <tr class="familydata summary totalsec">

						<td class="column8">Avg Totals:</td>

                        <td class="column3">(<?php echo ceil($avg_present_attendance);?>%)</td>

                        <td class="column3">(<?php echo ceil($avg_late_attendance);?>%)</td>

                        <td class="column3">(<?php echo ceil($avg_absent_attendance);?>%)</td>

						<!--<td class="column3">0 : (0%)</td>-->

                        <td class="column3">(<?php echo ceil($avg_session_attendance);?>%)</td>

                        </tr>

						<?php			
					    } else { ?>
                    <tr><th colspan="7" style="text-align: center; height:100px; width:1215px; font-size:25px; background:#FFF; color: #6a7a91; ">No record to show.</th></tr>	
				   <?php } ?>  

				</tbody>

		  </table>

          

          

</div>
<div class="col-sm-12 exceldiv">
    
     <form method="post"  name="export_form" action="<?php echo base_url();?>attendancesummary/excel_action">
     
    				<input type="hidden" name="branch_search" id="branch_search" value="<?php echo $branch_search; ?>"  />  
     
                      <input type="hidden" name="class_search" id="class_search" value="<?php echo $class_search; ?>"  />
                      
                       <input type="hidden" name="sdate_search" id="sdate_search" value="<?php echo $sdate_search; ?>"  />
                       
                       <input type="hidden" name="edate_search" id="edate_search" value="<?php echo $edate_search; ?>"  />
                        
                       <input type="submit" name="export" class="btn btn-success excelbtn" value="Export to Excel" />
    
   					 </form>
                     
    </div>    

    <?php } else { ?>
    
    <div class="beforeresult_section"><h1></h1></div>

     <?php } ?>


	</div>

<!--    <div class="col-sm-3 tablerightdiv fullwidsec">

    <img class="img-responsive" src="<?php echo base_url();?>assets/images/summary1.jpg">

	<br/>

    <img class="img-responsive" src="<?php echo base_url();?>assets/images/summary2.jpg">

    </div>-->

	</div>

    </div>

    </div>
    
 <script>
  jQuery(document).ready(function(){
	  
	   $("#sdate").keydown(false);
	    $("#edate").keydown(false);
	  
	  	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#attendancesummaryForm")[0].reset();
			   window.location.href='<?php echo base_url()?>attendancesummary/index';
			}); 
			
	  jQuery("#branch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			// var BranchID =  jQuery('input[name=student_school_branch]:checked').val();
			jQuery("#class").find('option[value!=""]').remove();
			
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>attendancesummary/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#class').append(resp);
			          }
				   });
				 } else {
			     jQuery("#class").find('option[value!=""]').remove();
				 return false;
				 }
			 });
			 
			 
	    jQuery("#attendancesummaryForm").validate({
        rules: {
			 sdate: {
    					required: true
   					},
			  edate: {
    					required: true
   					}
             /* branch: {
                 required: true
                 },
			  class: {
    					required: true
   					}, 
			  type: {
                required: true
                 }*/
            },
        messages: {
			  
			  sdate: {
    					required: "Please select start date."
   				 },
			  edate: {
    					required: "Please select end date."
   				 }
				 
            /*  branch: {
                  required: "Please select branch."
                 },
			  class: {
    					required: "Please select class."
   					},
			  
			  type: {
                   required: "Please select type."
                 }*/
             },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		
			 
	 });
		 
</script>
<style>
 .beforeresult_section > h1 {
    min-height: 300px;
	}
.confirm{
	padding:5px 2px !important;
}
.selectfilter > span {
    padding: 6px 10px 0 0;
}
.col-sm-12.col-xs-12 .applycodediv{
	padding-left:0px !important;
	padding-right:0px !important;
}
.confirm {
    padding: 6px !important;
}
.exceldiv .excelbtn {
    margin-top: 15px;
}
</style>