<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {
	
	var $tbl		= "users";
	var $username	= "username";
	var $password	= "password";
	var $school		= "school";
	var $staff_access		= "staff_access";
	var $staff		= "staff";
	
    function __construct() {
        parent::__construct();
    }

	function getUserInfo($user,$pass){
		$this->db->select('*');
		$this->db->where(array($this->username => $user,$this->password=>$pass));
		$query = $this->db->get($this->tbl);
		return $query->result();
	}
	
	function getUseraccess($userid) {
        $this->db->select('*');
        $this->db->from($this->staff_access);
		 $this->db->where('user_id', $userid);
        $query = $this->db->get();
        return  $query->row();
    }
	function getstaffdetails($userid) {
        $this->db->select('*');
        $this->db->from($this->staff);
		 $this->db->where('user_id', $userid);
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getUserSchoolInfo($user_school_id) {
          $this->db->select('*');
          $this->db->from($this->school);
		  $this->db->where('school_id', $user_school_id);
          $query = $this->db->get();
          return  $query->row();
    }
	
	function savelogin_time($id,$total_login_count){
		$updated_date= date("Y-m-d H:i:s");
		$data['last_login']=$updated_date;
		$data['total_login']=$total_login_count;
		$this->db->where('id',$id);
		if($this->db->update($this->tbl, $data)){
			return true;
		} else {
			return false;
		 $this->db->last_query();exit;
		}
	 }
	 
	 function getData($where=NULL,$order=NULL){
		$this->db->select('*');
		if($where != NULL){
			$this->db->where($where);
		}
		if($order != NULL){
			$this->db->order_by($order,'desc');
		}
		$query = $this->db->get($this->tbl);
		return $query->result();
	}
    
		function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	  }
}
