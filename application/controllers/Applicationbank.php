<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Applicationbank extends CI_Controller {

	var $logmode;
	function __construct()
	{
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false )
        {
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','staff_model', 'applicationbank_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
		$this->load->library('authorize');
    }
    public function index()

	{
		$PerPage				= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$user_srch_val			= isset($_GET['user_srch'])?$_GET['user_srch']:NULL;
		
		$school_id				= $this->session->userdata('user_school_id');
		
		$staff_branch_id		= $this->session->userdata('staff_branch_id');
		
		$user_type				= $this->session->userdata('user_type');
		
		//echo $staff_branch_id;
			
		$user_srch				= trim($user_srch_val);
		
		$rr 					= "'%".$user_srch."%'";
		if($rr!="")
		{
			$gg= 'AND (  u.username LIKE '. $rr . ' OR b.branch_name LIKE '. $rr . ' OR se.staff_telephone LIKE '. $rr . ' OR u.email LIKE ' . $rr .')';
		}
		else
		{
			$gg = '';
		}
	    


		$data = array();
		 
		$config = array();
			
        $config['base_url'] 		= base_url() . "applicationbank/index";

        if($PerPage!='') {
			
			$config["per_page"] 	= $PerPage;
			$perpage				= $config["per_page"] ;
			$data["per_page"]		= $config["per_page"];
		} else {
			 $config["per_page"]	= 10;
             $perpage				= $config["per_page"] ;
		}
		$page 					=  $this->uri->segment(3,0) ;
		$data["per_page"]		= $config["per_page"];
			
        //$config['total_rows']  		= $this->staff_model->record_count($user_srch, $school_id, $gg,$user_srch);
		
		//$data['staffdata'] 		= $this->staff_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);
		
		/************** Changes for user access system **************************/
		 
		 if($user_type=='teacher')
		 {
			if($staff_branch_id!='')
			{
				$config["total_rows"] 			= $this->applicationbank_model->record_count($user_srch, $school_id, $gg,$user_srch);
				$data['applicationbankdata'] 				= $this->applicationbank_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);

			}
			else
			{
				$config["total_rows"] 			= $this->applicationbank_model->record_count($user_srch, $school_id, $gg,$user_srch);
				$data['applicationbankdata'] 				= $this->applicationbank_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);
			}
		 }
		 else if($user_type=='admin')
		 {
			$config["total_rows"] 			= $this->applicationbank_model->record_count($user_srch, $school_id, $gg,$user_srch);
			$data['applicationbankdata'] 				= $this->applicationbank_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);
			
		 }
		

        $config["uri_segment"] 		= 3;
			
		$this->pagination->initialize($config);
		
		$config['postfix_string'] 	= "/?user_search=$rr&perpage=$perpage"; 

        $page 					=  $this->uri->segment(3,0) ;
		
		$data["uri_segment"]	= $config["uri_segment"];
		
		$data['total_rows']		= $config['total_rows']; 
		
		$data["per_page"]		= $config["per_page"];
		
		$data['post_url']		= $config['postfix_string'];
	
		$data['user_srch']		= $user_srch;
		
		$data['last_page']		= $page;
       
        $data["links"] 			= $this->pagination->create_links();
		
		
		
		
		$this->load->view('header');
 		$this->load->view('applicationbank/applicationbank', $data);
		$this->load->view('footer');
		
	}

	public function delete_application($id){
		if($this->applicationbank_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Application has been deleted successfully.');
			redirect(base_url()."applicationbank/");exit;
		}
		else{
			$this->session->set_flashdata('error', 'Some problem exists. Try again.');
			redirect(base_url()."applicationbank/");exit;
		}
	}
	
	
		

}

