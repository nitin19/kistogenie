<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>
        <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

       <li>Progress Reports</li>   

        </ul>

        </div>

        <h1>Progress Reports</h1>

        <div class="col-sm-12 progressreports nopadding">
        <?php 
		$sr = 0; 
		foreach($terms as $termdetails){
			$sr++;
			?>

<a href="<?php echo base_url();?>schooltermreport/index?branch=&class=&term=<?php echo $termdetails->term_id;?>&year=<?php echo date('Y');?>">
  
        <div class="reportsec progressreport">

        <div class="<?php if($sr % 5 =='1'){echo 'greenbox';} if($sr % 5 == '2'){echo 'bluebox';} if($sr % 5 =='3'){echo 'yellowbox';} if($sr % 5 =='4'){echo 'pinkbox';} if($sr % 5== '0'){echo 'purplebox';}?> reportblk">

        <div class="reportblocks">

        <div class="col-sm-9 col-xs-9 reportname">

        <h1><?php echo $termdetails->term_name; ?></h1>

        <h2 class="marks">
        <?php
		  $school_id		= $this->session->userdata('user_school_id');
          $term_id = $termdetails->term_id;
		  $year = date('Y'); 
		  $this->load->model(array('progressreport_model'));
		  echo $progressreportcount = $this->progressreport_model->getprogressreportcount($school_id,$term_id,$year);
		  ?>
        </h2>

        </div>

        <div class="col-sm-3 col-xs-3 reportarrow">

        <i class="fa fa-arrow-circle-o-up"></i>

        </div>


        </div>

        <div class="progressindicator">

 <span><?php 
		  $branch_id = $termdetails->branch_id; 
		  $this->load->model(array('progressreport_model'));
		  $branch = $this->progressreport_model->getbranchName($branch_id);
		  // echo $branch->branch_name;
		?></span>

        </div>

        </div>

        </div>
        
        </a>
        
      <?php } ?>
    </div>

    </div>
    
    <style>
	.reportsec{padding:7px 7px 0px 0px;}
	.reportblk { width:100%;}
	</style>