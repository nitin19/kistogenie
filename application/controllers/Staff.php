<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','staff_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
		$this->load->library('authorize');
    }

	public function index()

	{
		$PerPage				= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$user_srch_val			= isset($_GET['user_srch'])?$_GET['user_srch']:NULL;
		
		$school_id				= $this->session->userdata('user_school_id');
		
		$staff_branch_id		= $this->session->userdata('staff_branch_id');
		
		$user_type				= $this->session->userdata('user_type');
		
		//echo $staff_branch_id;
			
		$user_srch				= trim($user_srch_val);
		
		$rr 					= "'%".$user_srch."%'";
		
	$gg= '(  users.username LIKE '. $rr . ' OR staff.staff_telephone LIKE '. $rr . ' OR staff.staff_fname LIKE '. $rr . ' OR staff.staff_lname LIKE ' . $rr .' OR users.email LIKE '. $rr . ')';


		$data = array();
		 
		$config = array();
			
        $config['base_url'] 		= base_url() . "staff/index";

        if($PerPage!='') {
			
			$config["per_page"] 	= $PerPage;
			$perpage				= $config["per_page"] ;
			$data["per_page"]		= $config["per_page"];
		} else {
			 $config["per_page"]	= 10;
             $perpage				= $config["per_page"] ;
		}
		$page 					=  $this->uri->segment(3,0) ;
		$data["per_page"]		= $config["per_page"];
			
        //$config['total_rows']  		= $this->staff_model->record_count($user_srch, $school_id, $gg,$user_srch);
		
		//$data['staffdata'] 		= $this->staff_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);
		
		/************** Changes for user access system **************************/
		 
		 if($user_type=='teacher'){
			 if($staff_branch_id!=''){
			$config["total_rows"] 			= $this->staff_model->getStaff_record_count($user_srch, $staff_branch_id, $gg,$user_srch);
			$data['staffdata'] 				= $this->staff_model->getStaff_records($user_srch, $staff_branch_id, $data["per_page"], $page, $gg,$user_srch);
			 }
			 else{
			$config["total_rows"] 			= $this->staff_model->getStaff_record_count($user_srch, $staff_branch_id, $gg,$user_srch);
			$data['staffdata'] 				= $this->staff_model->getStaff_records($user_srch, $staff_branch_id, $data["per_page"], $page, $gg,$user_srch);
			 }
		 }
		 else if($user_type=='admin'){
			$config["total_rows"] 			= $this->staff_model->record_count($user_srch, $school_id, $gg,$user_srch);
			$data['staffdata'] 				= $this->staff_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);
		 }
		

        $config["uri_segment"] 		= 3;
			
		$this->pagination->initialize($config);
		
		$config['postfix_string'] 	= "/?user_search=$rr&perpage=$perpage"; 

        $page 					=  $this->uri->segment(3,0) ;
		
		$data["uri_segment"]	= $config["uri_segment"];
		
		$data['total_rows']		= $config['total_rows']; 
		
		$data["per_page"]		= $config["per_page"];
		
		$data['post_url']		= $config['postfix_string'];
	
		$data['user_srch']		= $user_srch;
		
		$data['last_page']		= $page;
       
        $data["links"] 			= $this->pagination->create_links();
		
		
		
		$this->load->view('header');
 		$this->load->view('staff/staff', $data);
		$this->load->view('footer');
		
}
		
	
public function act_inact()
	 {
		 $opt = $this->uri->segment(4);
		  $id = $this->uri->segment(3);
		 if($opt=='active'){ 
			 $data1 = array(
				'is_active' => '1'
				);
		  } 
		  if($opt=='inactive') { 
		  	 $data1 = array(
				'is_active' => '0'
				);
		  }
		  if($opt=='delete') {    
          $data1 = array(
				'is_deleted' => '1'
				); 		  
		  }
		
		$dttt=$this->staff_model->act_inact($id,$data1);
						
		if($dttt=1){
				if($opt=='delete') {
					$this->session->set_flashdata('success', 'Data has been Deleted successfuly.');
					} else {
                      $this->session->set_flashdata('success', 'Data has been updated successfuly.');
					}
				redirect(base_url()."staff/");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Staff has not been updated.');
				redirect(base_url()."staff/");exit;
			   }
	  }
	
	public function add_new()
	 {
		$school_id		= $this->session->userdata('user_school_id');
		$data['branch'] = $this->staff_model->get_branches($school_id);
		$data['currencies'] = $this->staff_model->getcurrencies($school_id);
		$data['getteacherrole'] = $this->staff_model->teacher_role($school_id);
		$data['getteacherlevel'] = $this->staff_model->teacher_level($school_id);
		$data['getteachertype'] = $this->staff_model->teacher_type($school_id);
		$this->load->view('header');
		$this->load->view('staff/add_staff', $data);
		$this->load->view('footer');
	  }
	  
public function check_username(){
	$Uname = trim($_POST['username']); 
	$checkstaffusername = $this->staff_model->checkstaffname($Uname);
	$countstaffname = count($checkstaffusername);
	if($countstaffname > 0 ) {
	echo 'false';
	} else {
	echo 'true';
	}
}

public function check_email(){
	$school_id		= $this->session->userdata('user_school_id');
	$email = $_REQUEST['email']; 
	$countstaffemail = $this->staff_model->checkstaffemail($email, $school_id);
	if( $countstaffemail > 0 ) {
	echo 'false';
	} else {
	echo 'true';
	}
}

public function edit_check_email(){
	$school_id		= $this->session->userdata('user_school_id');
	$id = $this->uri->segment(3);
	$email = $_REQUEST['email']; 
	$countstaffemail = $this->staff_model->checkeditstaffemail($email, $id, $school_id);
	if( $countstaffemail > 0 ) {
	echo 'false';
	} else {
	echo 'true';
	}
}
	  
 public function getclass() {
	$branch_id  = $this->input->post('branch_id');
	$school_id	= $this->session->userdata('user_school_id');
	$getclasses = $this->staff_model->get_classes($school_id, $branch_id);	
	$HTML1='';
	$cls_id_str = '';
	foreach($getclasses as $getclass)
		{
		 $cls_id_str.= $getclass->class_id.',';
		$branchInfo = $this->staff_model->getbranchName($getclass->branch_id);
		$HTML1.='<option value="'.$getclass->class_id.'"';
		 if($getclass->class_id == $getclass->class_id){ 
		  $HTML1.= 'selected';
		  }
		 $HTML1.= '>'.$getclass->class_name.' ('.$branchInfo->branch_name.') </option>';
			}
			
			$classIDSstring = rtrim($cls_id_str,',');
			$class_id = explode(',', $classIDSstring); 
			$getsubjects = $this->staff_model->getsubjects($school_id, $branch_id, $class_id);
		$HTML2 = '';	
	foreach($getsubjects as $getsubject) {
		$branchInfo = $this->staff_model->getbranchName($getsubject->branch_id);
		$classesInfo = $this->staff_model->getsingleClassName($getsubject->class_id);
		
		$HTML2.='<option value="'.$getsubject->subject_id.'"';
		 if($getsubject->subject_id == $getsubject->subject_id){ 
		  $HTML2.= 'selected';
		  }
		 $HTML2.= '>'.$getsubject->subject_name.' ('.$classesInfo->class_name.')  ('.$branchInfo->branch_name.') </option>';
		
/*		$HTML2.= '<div class="checkboxdiv profilecheckbox">';
		$HTML2.= '<input type="checkbox" name="subjectsname[]" id="'.$getsubject->subject_id.'" value="'.$getsubject->subject_id.'"';
		if($getsubject->subject_id == $getsubject->subject_id){ 
		$HTML2.= 'checked'; 
		}
		$HTML2.= '>
    			<label for="'.$getsubject->subject_id.'"><span class="checkbox">'.$getsubject->subject_name.' ('.$classesInfo->class_name.') ('.$branchInfo->branch_name.') </span></label></div>';*/
			}
			  echo json_encode(array('datacls'=>$HTML1,'datasub'=>$HTML2));
	}
	
public function get_subject(){
	$class_id  = $this->input->post('class_id');
	/*$class_id = implode(',', $class_ids);*/
	$branch_id  = $this->input->post('branch_id');
	$school_id	= $this->session->userdata('user_school_id');
	$getsubjects = $this->staff_model->getsubjects($school_id, $branch_id, $class_id);	
	foreach($getsubjects as $getsubject) {
		$branchInfo = $this->staff_model->getbranchName($getsubject->branch_id);
		$classesInfo = $this->staff_model->getsingleClassName($getsubject->class_id);
		
		  echo '<option value="'.$getsubject->subject_id.'"';
		 if($getsubject->subject_id == $getsubject->subject_id){ 
		 echo 'selected';
		  }
		 echo '>'.$getsubject->subject_name.' ('.$classesInfo->class_name.')  ('.$branchInfo->branch_name.') </option>';
		
/*		echo '<div class="checkboxdiv profilecheckbox">';
		echo '<input type="checkbox" name="subjectsname[]" id="'.$getsubject->subject_id.'" value="'.$getsubject->subject_id.'"';
		if($getsubject->subject_id == $getsubject->subject_id){ 
		echo 'checked'; 
		}
		echo '>
    			<label for="'.$getsubject->subject_id.'"><span class="checkbox">'.$getsubject->subject_name.' ('.$classesInfo->class_name.') ('.$branchInfo->branch_name.')  </span></label></div>'; */
				 
			}		   		  
		  }
	    
 /*public function get_teacher_role() {
	$branch_id  = $this->input->post('branch_id');
	$school_id	= $this->session->userdata('user_school_id');		
	$getteacherrole = $this->staff_model->teacher_role($school_id, $branch_id);
	$HTML='<option value="">Select Role</option>';
	foreach($getteacherrole as $teach_role)
		{
		$HTML.='<option value="'.$teach_role->id.'">'.$teach_role->teacher_role.'</option>';
			}
			echo $HTML;
	}

 public function get_teacher_level() {
	$branch_id  = $this->input->post('branch_id');
	$school_id	= $this->session->userdata('user_school_id');		
	$getteacherlevel = $this->staff_model->teacher_level($school_id, $branch_id);
	$HTML='<option value="">Select Level</option>';
	foreach($getteacherlevel as $teach_level)
		{
		$HTML.='<option value="'.$teach_level->id.'">'.$teach_level->teacher_level.'</option>';
         }
		 echo $HTML;
	}
		 
 public function get_teacher_type() {
	$branch_id  = $this->input->post('branch_id');
	$school_id	= $this->session->userdata('user_school_id');		
	$getteachertype = $this->staff_model->teacher_type($school_id, $branch_id);
	$HTML ='<option value="">Select Type</option>';
	foreach($getteachertype as $teach_type)
		{
		$HTML.='<option value="'.$teach_type->id.'">'.$teach_type->teacher_type.'</option>';
           }
		   echo $HTML;
	}
*/
public function add_new_staff(){
    // print_r($_POST); exit;
	$userdata = array();
	$staffdata = array();						
	$data	=	 array();
	$staffaccess = array();
	$data['title']	= 	"Add New Staff";
	$data['mode']	=	 "Add";

if ($this->input->post('username') != '') {
	$userdata['username'] = $this->input->post('username');
}
if ($this->input->post('password') != '') {
	$userdata['password'] = $this->input->post('password');
}
	$userdata['school_id'] = $this->session->userdata('user_school_id');
	$userdata['created_by'] = $this->session->userdata('user_name');
	$userdata['user_type'] = 'teacher';
	$userdata['is_active'] = '1';
	$userdata['created_date'] = date('Y-m-d H:i:s');;
	$userdata['updated_date'] = date('Y-m-d H:i:s');
	$staffdata['school_id'] = $this->session->userdata('user_school_id');
	
if ($this->input->post('staff_fname') != '') {
	$staffdata['staff_fname'] = $this->input->post('staff_fname');
}
if ($this->input->post('staff_lname') != '') {
	$staffdata['staff_lname'] = $this->input->post('staff_lname');
}
if ($this->input->post('staff_title') != '') {
	$staffdata['staff_title'] = $this->input->post('staff_title');
}
if ($this->input->post('staff_dob') != '') {
	$sdob = $this->input->post('staff_dob');
    $staffdata['staff_dob'] = date('Y-m-d', strtotime($sdob));
            }
if ($this->input->post('email') != '') {
 $userdata['email'] = $this->input->post('email');
            }

if ($this->input->post('staff_teacher_type') != '') {
	$staffdata['staff_teacher_type'] = $this->input->post('staff_teacher_type');
            }
if ($this->input->post('staff_teacher_level') != '') {
	$staffdata['staff_teacher_level'] = $this->input->post('staff_teacher_level');
            }
if ($this->input->post('staff_role_at_school') != '') {
	$staffdata['staff_role_at_school'] = $this->input->post('staff_role_at_school');
            }
if ($this->input->post('staff_education_qualification') != '') {
	$staffdata['staff_education_qualification'] = $this->input->post('staff_education_qualification');
            }
if ($this->input->post('exist_branch') != '') {
	$staffdata['existing_branch'] = $this->input->post('exist_branch');
            }
if ($this->input->post('staff_personal_summery') != '') {
	$staffdata['staff_personal_summery'] = $this->input->post('staff_personal_summery');
            }
if ($this->input->post('staff_telephone') != '') {
	$staffdata['staff_telephone'] = $this->input->post('staff_telephone');
            }
if ($this->input->post('staff_address') != '') {
	$staffdata['staff_address'] = $this->input->post('staff_address');
            }
if ($this->input->post('staff_address_1') != '') {
	$staffdata['staff_address_1'] = $this->input->post('staff_address_1');
 }

 if ($this->input->post('staff_acc_no') != '') {
	$staffdata['account_no'] = $this->input->post('staff_acc_no');
 }

 if ($this->input->post('staff_sort_code') != '') {
	$staffdata['sort_code'] = $this->input->post('staff_sort_code');
 }

 if ($this->input->post('staff_insurance') != '') {
	$staffdata['staff_insurance'] = $this->input->post('staff_insurance');
 }

 if ($this->input->post('staff_hourly_rate') != '') {
	$staffdata['hourly_rate'] = $this->input->post('staff_hourly_rate');
 }

 if ($this->input->post('salary_currency') != '') {
	$staffdata['salary_currency'] = $this->input->post('salary_currency');
 }
 
 if ($this->input->post('salary_amount') != '') {
	$staffdata['salary_amount'] = $this->input->post('salary_amount');
 }
 
 if ($this->input->post('salary_mode') != '') {
	$staffdata['salary_mode'] = $this->input->post('salary_mode');
 }
 
  if ($this->input->post('teaching_days') != '') {
   $teaching_days = $this->input->post('teaching_days');
   $staffdata['teaching_days'] = implode(',', $teaching_days);
        }
 
 if ($this->input->post('branch') != '') {
   $branchids = $this->input->post('branch');
   $staffdata['branch_id'] = implode(',', $branchids);
            }	
if ($this->input->post('class_name') != '') {
	$classes = $this->input->post('class_name');
	$staffdata['class_name'] = implode(',', $classes);
            }
if ($this->input->post('subjectsname') != '') {
	$subjects = $this->input->post('subjectsname');
	$staffdata['subject_name'] = implode(',', $subjects);
            }

       if ($this->input->post('students_menu') == 'Yes') {
			      $staffaccess['students'] = 'Yes';
					} else {
			      $staffaccess['students'] = 'No';
					}
		
		if ($this->input->post('student_attendance_menu') == 'Yes') {
			      $staffaccess['student_attendance'] = 'Yes';
					} else {
			      $staffaccess['student_attendance'] = 'No';
					}
					
		if ($this->input->post('documents_menu') == 'Yes') {
			       $staffaccess['documents'] = 'Yes';
					} else {
				   $staffaccess['documents'] = 'No';
					}
		if ($this->input->post('progress_reports_menu') == 'Yes') {
			       $staffaccess['progress_reports'] = 'Yes';
					} else {
					$staffaccess['progress_reports'] = 'No';
					  }
					
		if ($this->input->post('configurations_menu') == 'Yes') {
			         $staffaccess['configurations'] = 'Yes';
					} else {
					 $staffaccess['configurations'] = 'No';
					  }
					
		if ($this->input->post('student_enrolement_menu') == 'Yes') {
			        $staffaccess['student_enrolement'] = 'Yes';
					} else {
					$staffaccess['student_enrolement'] = 'No';
					  }
	    if ($this->input->post('staff_menu') == 'Yes') {
					$staffaccess['staff'] = 'Yes';
					} else {
					$staffaccess['staff'] = 'No';
		  }	
					
		if ($this->input->post('staff_enrolement_menu') == 'Yes') {
			        $staffaccess['staff_enrolement'] = 'Yes';
					} else {
					$staffaccess['staff_enrolement'] = 'No';
					  }
					
		if ($this->input->post('staff_attendence_menu') == 'Yes') {
			        $staffaccess['staff_attendence']  = 'Yes';
					} else {
					$staffaccess['staff_attendence']  = 'No';
					   }
					
		if ($this->input->post('staff_salary_menu') == 'Yes') {
			       $staffaccess['staff_salary'] = 'Yes';
					} else {
					$staffaccess['staff_salary'] = 'No';
					  }
					
		if ($this->input->post('teaching_learing_menu') == 'Yes') {
			       $staffaccess['teaching_learing'] = 'Yes';
					} else {
					$staffaccess['teaching_learing'] = 'No';
				      }
		
		if ($this->input->post('basic_masters_menu') == 'Yes') {
			       $staffaccess['basic_masters'] = 'Yes';
					} else {
				   $staffaccess['basic_masters'] = 'No';
					   }	
					   
/* Notification code */
	$school_id	= $this->session->userdata('user_school_id');
	$get_school_admin = $this->staff_model->get_school_admin($school_id);

	$notification_data['school_id'] = $school_id;
	$notification_data['branch_id'] = $this->input->post('branch_id');
	$notification_data['from'] = $school_id;
	$notification_data['to'] = $get_school_admin->id;
	$notification_data['subject'] = 'Staff Added';
	$notification_data['message'] = 'New Staff Added';
	$notification_data['attachments'] = $cv;
	$notification_data['is_active'] = 1;
	$notification_data['is_deleted'] = 0;
	$notification_data['created_date'] = $created_date;
	$notification_data['read_by'] = '';
	$notification_data['from_web'] = 1;
	$notification_data['applied_by'] = 'staff';
// 	print_r($notification_data); exit;
	$notification_data = array_filter($notification_data);
	$insert_notification_data = $this->staff_model->insert_notification_data($notification_data);
/* Notification code */	
	  $staffaccess['is_active'] = '1';	
	  $staffaccess['created_date'] = date('Y-m-d H:i:s');		
			
			$config['upload_path']   = './uploads/staff/'; 
			$config['allowed_types'] =  'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload("profile_image")){
		    $data = array('upload_data' => $this->upload->data());
	        $image_name = $data['upload_data']['file_name'];
		    if($_FILES['profile_image']['error'] == 0 ){
				$file_name					= $data['upload_data']['file_name'];
				$userdata['profile_image']		= $file_name;
			    }
			}
				
			$last_id_staff =  $this->staff_model->insertUser($userdata);
		  if($last_id_staff){
			  
			    $staffdata['user_id'] = $last_id_staff;
				
				$staffaccess['user_id'] = $last_id_staff;
				$staffaccess['school_id'] = $this->session->userdata('user_school_id');
				
			    $this->staff_model->insertStaff($staffdata);
				$this->staff_model->insertStaffAccess($staffaccess);
			
				$this->session->set_flashdata('success', 'Staff has been added successfully.');
				redirect(base_url()."staff/add_new");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Staff has not been added.');
				redirect(base_url()."staff/add_new");exit;
			   }
        
		$data['logmode']			= $this->logmode;
		$data['error']				= $this->session->flashdata('error');
		$data['success']			= $this->session->flashdata('success');
		//$data['alert_msg']			= $this->load->view('admin/alert_msg',$data,true);
		//$data['content']			= $this->load->view('admin/quiz/details',$data,true);
		//$this->load->view('admin/template',$data);
		$this->load->view('header');
		$this->load->view('staff/add_staff',$data);
		$this->load->view('footer');
	}
	
	public function view_staff()

	{
		$this->load->view('header');
		$this->load->view('staff/staff_detail');
		$this->load->view('footer');
	}
	
function view_user() {
	$id = $this->uri->segment(3);
	$this->load->view('header');
	$data['result']=$this->staff_model->get_single($id);
	
	$staffAccessdata	= $this->staff_model->get_single_staff_access($id);
	
	if($staffAccessdata!=''){
		
	 $access_system		= '';
	 if($staffAccessdata->student_attendance=='Yes')
	 {
		$access_system.=', '.'Student attendance'; 
	 }
	 	 if($staffAccessdata->students=='Yes')
	 {
		$access_system.=', '.'Students'; 
	 }
	 	 if($staffAccessdata->documents=='Yes')
	 {
		$access_system.=', '.'Documents'; 
	 }
	 	 if($staffAccessdata->progress_reports=='Yes')
	 {
		$access_system.=', '.'Progress reports'; 
	 }
	 	 if($staffAccessdata->configurations=='Yes')
	 {
		$access_system.=', '.'Configurations'; 
	 }
	 	 if($staffAccessdata->student_enrolement=='Yes')
	 {
		$access_system.=', '.'Student enrolement'; 
	 }
	  	 if($staffAccessdata->staff=='Yes')
	 {
		$access_system.=','.'Staff'; 
	 }
	 	 if($staffAccessdata->staff_enrolement=='Yes')
	 {
		$access_system.=', '.'Staff enrolement'; 
	 }
	 	 if($staffAccessdata->staff_attendence=='Yes')
	 {
		$access_system.=', '.'Staff attendence'; 
	 }
	 	 if($staffAccessdata->staff_salary=='Yes')
	 {
		$access_system.=', '.'Staff salary'; 
	 }	
	  if($staffAccessdata->teaching_learing=='Yes')
	 {
		$access_system.=', '.'Teaching learing'; 
	 }	
	  if($staffAccessdata->basic_masters=='Yes')
	 {
		$access_system.=', '.'Basic setup'; 
	 }
	
	$data['access']=ltrim($access_system,",");
	
	}else{
		
	$data['access']='';
	}
	
	$this->load->view('staff/staff_detail',$data);
	$this->load->view('footer');
}

function show_user_id() {
	$id = $this->uri->segment(3);
	$school_id		= $this->session->userdata('user_school_id');
	$data['branch'] = $this->staff_model->get_branches($school_id);
	$data['staffdata']=$this->staff_model->get_singlerecord($id); 
	 /*$data['result'] = $this->staff_model->teacher_role( $school_id);
	 $data['result1'] = $this->staff_model->teacher_type( $school_id);
	$data['result2'] = $this->staff_model->teacher_level( $school_id);*/
	$this->load->view('header');
    $this->load->view('staff/edit_staff_detail',$data);
	$this->load->view('footer');
}


function delete_img() {	
$img = $this->uri->segment(4);
$id = $this->uri->segment(3);

$profile_image = $_POST['profile_image'];

$config['upload_path']   = 'uploads/staff/'; 
			$config['allowed_types'] =  'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload("profile_image")){
		    $data = array('upload_data' => $this->upload->data());
	        $image_name = $data['upload_data']['file_name'];
		    if($_FILES['profile_image']['error'] == 0 ){
				$file_name					= $data['upload_data']['file_name'];
				$data5['profile_image']		= $file_name;
			}}

if($img='del') {$data5 = array('profile_image' => $this->input->post(''), ); }


	$data['result']=$this->staff_model->get_singlerecord($id);	
	$dtt=$this->staff_model->del_img($id,$data5);	
	if($dtt=1){	
$this->session->set_flashdata('success', '');
				redirect(base_url()."staff/show_user_id/".$id);exit;
			}else{
				$this->session->set_flashdata('error', '');
				redirect(base_url()."staff/show_user_id/" .$id);exit;
			   }
}


function update() {
$id= $this->input->post('id');
$sdob = $this->input->post('MyDate1');
$classes = $this->input->post('class_name');
$subjects = $this->input->post('subjectsname');
	
$staffdata = array(
'staff_fname' => $this->input->post('firstname'),
'staff_lname' => $this->input->post('lastname'),
'staff_title' => $this->input->post('title'),
'account_no' => $this->input->post('staff_acc_no'),
'sort_code' => $this->input->post('staff_sort_code'),
'staff_insurance' => $this->input->post('staff_insurance'),
'hourly_rate' => $this->input->post('staff_hourly_rate'),
'staff_education_qualification' => $this->input->post('quali'),
'staff_personal_summery' => $this->input->post('persummary'),
'staff_telephone' => $this->input->post('phone'),
'staff_address' => $this->input->post('address'),
'staff_address_1' => $this->input->post('optionaladdress')
);

$staffdata['staff_dob'] = date('Y-m-d', strtotime($sdob));

$userdata = array(
		'username' => $this->input->post('username'),
		'password' => $this->input->post('password'),
		'email' => $this->input->post('email')
		);

$config['upload_path']   = './uploads/staff/'; 
			$config['allowed_types'] =  'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload("profile_image")){
		    $data = array('upload_data' => $this->upload->data());
	        $image_name = $data['upload_data']['file_name'];
		    if($_FILES['profile_image']['error'] == 0 ){
				$file_name					= $data['upload_data']['file_name'];
				$userdata['profile_image']		= $file_name;
			    }
			}

/*$config['upload_path']   = './uploads/staff/'; 
			$config['allowed_types'] =  'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload("profile_image")){
		    $data = array('upload_data' => $this->upload->data());
	        $image_name = $data['upload_data']['file_name'];
		    if($_FILES['profile_image']['error'] == 0 ){
				$file_name					= $data['upload_data']['file_name'];
				$data2['profile_image']		= $file_name;
			}
			}*/


$dttt=$this->staff_model->updatestaff($id,$staffdata);
$dttt1=$this->staff_model->updateuser($id,$userdata);	
if($dttt=1 && $dttt1=1){	
$this->session->set_flashdata('success', 'Data has been updated successfully.');
				redirect(base_url()."staff/show_user_id/".$id);exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Staff has not been added.');
				redirect(base_url()."staff/show_user_id/".$id);exit;
			   }
	         }
			 
			 
function Invitestaff() {
			 
			    $Inviteduserdata = array();
				$Invitedstaffdata = array();		
			
			   if ($this->input->post('invited_user_email') != '') {
				 $Inviteduserdata['email'] = trim($this->input->post('invited_user_email'));
				 $invited_user_email = trim($this->input->post('invited_user_email'));
           		}
			
		    	if ($this->input->post('invited_user_name') != '') {
				$Inviteduserdata['username'] = trim($this->input->post('invited_user_name'));
				$invited_user_name = trim($this->input->post('invited_user_name'));
				}
				if ($this->input->post('invited_user_pwd') != '') {
					$Inviteduserdata['password'] = trim($this->input->post('invited_user_pwd'));
					$invited_user_pwd = trim($this->input->post('invited_user_pwd'));
				}
				
			$school_id = $this->session->userdata('user_school_id');
			
			$Inviteduserdata['school_id'] = $this->session->userdata('user_school_id');
			$Inviteduserdata['created_by'] = $this->session->userdata('user_name');
			$Inviteduserdata['user_type'] = 'teacher';
			$Inviteduserdata['is_active'] = '1';
			$Inviteduserdata['created_date'] = date('Y-m-d H:i:s');;
			$Inviteduserdata['updated_date'] = date('Y-m-d H:i:s');
			
			$schoolInfo = $this->staff_model->get_school_name($school_id);
			$SchoolName = $schoolInfo->school_name;
			$SchoolEmail = $schoolInfo->school_email;
			
			$chkinviteduseremail = $this->staff_model->chk_invited_user_email($school_id,$invited_user_email);
			   if( count($chkinviteduseremail) > 0 ){
				   echo json_encode(array('Status'=>"alreadyexist"));
			      } else {
				$last_invited_user_id = $this->staff_model->insertInviteduser($Inviteduserdata);
                if($last_invited_user_id) {
					 $Invitedstaffdata['user_id'] = $last_invited_user_id;
					 $Invitedstaffdata['school_id'] = $this->session->userdata('user_school_id');
			         $this->staff_model->insertInvitedstaff($Invitedstaffdata);
					 
			$to			= $invited_user_email;
			$subject	= "Registration Successful";
			$message	= "<font face='arial'>";
			$message	.= "Hello Dear,<br />";
			$message	.= "<b> Your account is successfully created by admin on ".$SchoolName." </b><br />";
			$message	.= "<b> Here is your login details: </b> <br />";
			$message	.= "<b> Username: </b> " .$invited_user_name. "<br />";
			$message	.= "<b> Password: </b> " .$invited_user_pwd. "<br />";
			$message	.=  "<br />";
			$message	.= "Please click on give link to login on ".$SchoolName." and update your profile information <a href='".base_url()."'><b>Click Here</b></a><br />";
			$message	.= "<br />Thanks.<br />";
			$message	.= "</font>";
			$this->authorize->send_email($to,$message,$subject);
			
			$to_superadmin		= $SchoolEmail;
			$subject_superadmin	= "New User Invitation Sent";
			$message_superadmin	= "<font face='arial'>";
			$message_superadmin	.= "Hello Admin,<br />";
	        $message_superadmin	.= "<b>Your invitation sent successfully </b><br />";
			$message_superadmin	.= "<b> Here is invited user account details:</b> <br />";
			$message_superadmin	.= "<b> Username: </b> " .$invited_user_name. "<br />";
			$message_superadmin	.= "<b> Password: </b> " .$invited_user_pwd. "<br />";
			$message_superadmin	.= "<b> Email: </b> " .$invited_user_email. "<br />";
			$message_superadmin	.= "<br />Thanks.<br />";
			$message_superadmin	.= "</font>";
			$this->authorize->send_email($to_superadmin,$message_superadmin,$subject_superadmin);
			
				 echo json_encode(array('Status'=>"true"));
				 
			    } else {
				 echo json_encode(array('Status'=>"false"));
			       }
			     }
	     }


		function edit_staff() {
			$id = $this->uri->segment(3);
			$school_id		= $this->session->userdata('user_school_id');
			$data['branch'] = $this->staff_model->get_branches($school_id);
			$data['currencies'] = $this->staff_model->getcurrencies($school_id);
			$data['staffdata']=$this->staff_model->get_singlerecord($id);
			$data['staffAccessdata']=$this->staff_model->get_single_staff_access($id);
			
			$data['getteacherrole'] = $this->staff_model->teacher_role($school_id);
			$data['getteacherlevel'] = $this->staff_model->teacher_level($school_id);
			$data['getteachertype'] = $this->staff_model->teacher_type($school_id); 
			$this->load->view('header');
			$this->load->view('staff/edit_staff_detail_admin',$data);
			$this->load->view('footer');
         }

		
	function update_staff() {
		
		$staffdata= array();
		$userdata= array();
		$staffaccess = array();
		
		$id= $this->input->post('id');
		$sdob = $this->input->post('MyDate1');
		
		$staffdata = array(
		'staff_fname' => $this->input->post('firstname'),
		'staff_lname' => $this->input->post('lastname'),
		'staff_title' => $this->input->post('title'),
		'staff_role_at_school' => $this->input->post('staff_teacher_role'),
		'staff_teacher_type' => $this->input->post('staff_teacher_type'),
		'staff_teacher_level' => $this->input->post('staff_teacher_level'),
		'staff_education_qualification' => $this->input->post('quali'),
		'staff_personal_summery' => $this->input->post('persummary'),
		'staff_telephone' => $this->input->post('phone'),
		'staff_address' => $this->input->post('address'),
		'staff_address_1' => $this->input->post('optionaladdress'),
		'existing_branch' => $this->input->post('exist_branch'),
		'account_no' => $this->input->post('staff_acc_no'),
		'sort_code' => $this->input->post('staff_sort_code'),
		'staff_insurance' => $this->input->post('staff_insurance'),
		'hourly_rate' => $this->input->post('staff_hourly_rate'),
		);
		
		$staffdata['staff_dob'] = date('Y-m-d', strtotime($sdob));
		
		if($this->input->post('branch')!=''){
			$branches = $this->input->post('branch');
		    $staffdata['branch_id'] = implode(',', $branches);
		}
		
		if($this->input->post('class_name')!=''){
			$classes = $this->input->post('class_name');
		$staffdata['class_name'] = implode(',', $classes);
		}
		if($this->input->post('subjectsname')!=''){
		$subjects = $this->input->post('subjectsname');	
		$staffdata['subject_name'] = implode(',', $subjects);
		}
		
		
		 if ($this->input->post('salary_currency') != '') {
			$staffdata['salary_currency'] = $this->input->post('salary_currency');
		 }
		 
		 if ($this->input->post('salary_amount') != '') {
			$staffdata['salary_amount'] = $this->input->post('salary_amount');
		 }
		 
		 if ($this->input->post('salary_mode') != '') {
			$staffdata['salary_mode'] = $this->input->post('salary_mode');
		 }
		 
		 if ($this->input->post('teaching_days') != '') {
		   $teaching_days = $this->input->post('teaching_days');
		   $staffdata['teaching_days'] = implode(',', $teaching_days);
		  }  else {
			  $staffdata['teaching_days']='';
		  }


       if ($this->input->post('students_menu') == 'Yes') {
			      $staffaccess['students'] = 'Yes';
					} else {
			      $staffaccess['students'] = 'No';
					}
		if ($this->input->post('student_attendance_menu') == 'Yes') {
			      $staffaccess['student_attendance'] = 'Yes';
					} else {
			      $staffaccess['student_attendance'] = 'No';
					}
					
		if ($this->input->post('documents_menu') == 'Yes') {
			       $staffaccess['documents'] = 'Yes';
					} else {
				   $staffaccess['documents'] = 'No';
					}
		if ($this->input->post('progress_reports_menu') == 'Yes') {
			       $staffaccess['progress_reports'] = 'Yes';
					} else {
					$staffaccess['progress_reports'] = 'No';
					  }
					
		if ($this->input->post('configurations_menu') == 'Yes') {
			         $staffaccess['configurations'] = 'Yes';
					} else {
					 $staffaccess['configurations'] = 'No';
					  }
					
		if ($this->input->post('student_enrolement_menu') == 'Yes') {
			        $staffaccess['student_enrolement'] = 'Yes';
					} else {
					$staffaccess['student_enrolement'] = 'No';
					  }
					  
	    if ($this->input->post('staff_menu') == 'Yes') {
					$staffaccess['staff'] = 'Yes';
					} else {
					$staffaccess['staff'] = 'No';
		  }	
					
		if ($this->input->post('staff_enrolement_menu') == 'Yes') {
			        $staffaccess['staff_enrolement'] = 'Yes';
					} else {
					$staffaccess['staff_enrolement'] = 'No';
					  }
					
		if ($this->input->post('staff_attendence_menu') == 'Yes') {
			        $staffaccess['staff_attendence']  = 'Yes';
					} else {
					$staffaccess['staff_attendence']  = 'No';
					   }
					
		if ($this->input->post('staff_salary_menu') == 'Yes') {
			       $staffaccess['staff_salary'] = 'Yes';
					} else {
					$staffaccess['staff_salary'] = 'No';
					  }
					
		if ($this->input->post('teaching_learing_menu') == 'Yes') {
			       $staffaccess['teaching_learing'] = 'Yes';
					} else {
					$staffaccess['teaching_learing'] = 'No';
				      }
		
		if ($this->input->post('basic_masters_menu') == 'Yes') {
			       $staffaccess['basic_masters'] = 'Yes';
					} else {
				   $staffaccess['basic_masters'] = 'No';
					   }
	    $staffaccess['updated_date'] = date('Y-m-d H:i:s');		

		
		$userdata = array(
		'username' => $this->input->post('username'),
		'password' => $this->input->post('password'),
		'email' => $this->input->post('email')
		);
		
		$config['upload_path']   = './uploads/staff/'; 
				$config['allowed_types'] =  'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload("profile_image")){
				$data = array('upload_data' => $this->upload->data());
				$image_name = $data['upload_data']['file_name'];
				if($_FILES['profile_image']['error'] == 0 ){
					$file_name					= $data['upload_data']['file_name'];
					$userdata['profile_image']		= $file_name;
					}
				}
$count_staffaccess = $this->staff_model->count_staffaccess($id);	
		$dttt=$this->staff_model->updatestaff($id,$staffdata);
		$dttt1=$this->staff_model->updateuser($id,$userdata);
		if($count_staffaccess=='0'){
			$staffaccess['user_id']=$id;
			$school_id= $this->session->userdata('user_school_id');
			$staffaccess['school_id']=$school_id;
			$staffaccess['is_active']='1';
			$this->staff_model->insertstaffAccess_update($staffaccess);	
			}
		else {
		 $this->staff_model->updatestaffAccess($id,$staffaccess);	
		}
		if($dttt=1 && $dttt1=1){	
		$this->session->set_flashdata('success', 'Data has been updated successfully.');
				redirect(base_url()."staff/edit_staff/".$id);exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Staff has not been added.');
				redirect(base_url()."staff/edit_staff/".$id);exit;
			  }
	 }
	 
	  function excel_action()
  
  {
	  
  		$search_condition				=	array();
   		
		$school_id						= $this->session->userdata('user_school_id');
   
     	$user_srch     					= $this->input->post('user_srch');
  
  		$rr 							= "'%".$user_srch."%'";
	    $gg								= '(  users.username LIKE '. $rr . ' OR staff.staff_telephone LIKE '. $rr . ' OR staff.staff_fname LIKE '. $rr . ' OR staff.staff_lname     											LIKE ' . $rr .' OR users.email LIKE '. $rr . ')';  
     
  		$staffdata	 					= $this->staff_model->getexceldata($user_srch, $school_id, $gg,$user_srch);
  
   		$this->load->model("staff_model");
    
   		$this->load->library("Excel");
    
    	$object = new PHPExcel();
     
    	$object->setActiveSheetIndex(0);
     
    $table_columns = array("Firstname", "Lastname", "Date of Birth", "Contact details", "Email-id","Qualification","Address","Branch","Class","Teaching Days","Salary","Access System");
     
    $column = 0;
     
    foreach($table_columns as $field)
    {
     $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
     $column++;
    }
    
      
    $excel_row = 2;
     
    foreach($staffdata as $row)
    {
     $id=$row->user_id;
     
     $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->staff_fname);
     $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->staff_lname);
     $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->staff_dob);
     $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->staff_telephone);
     $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->email);
     $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->staff_education_qualification);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->staff_address);
	  
	  $branchid=$row->branch_id;
	   if($branchid=='' || $branchid=='0')
	   {
		   $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, @$row->branch_id);
	   }
	   else{
		   $branch_id		=	explode(',',$branchid);
		   
		   $getbranchname	=	'';
		   
		   foreach($branch_id as $branchid){
			   
			$getbranch		=$this->staff_model->getbranchName($branchid);
		  	$getbranchname.=",".$getbranch->branch_name;
		   }
			$staffbranch=ltrim($getbranchname,",");
	   		$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row,$staffbranch);
	
	   }
	   
	   if($row->class_name!='')
	   {
	  	$classid		=	$row->class_name;

		$class			=	explode(',',$classid);
		
		$getclassname	='';
		
		foreach($class as $class_name)
		{	
			$classname=$this->staff_model->getexcel_className($class_name);
			$getclassname.=','.$classname->class_name;
		}
		$staffclass=ltrim($getclassname,",");
		$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $staffclass);
		
	   }
	   else
	   {
		   $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row,@$row->class_name);
	  }
	   
		
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->teaching_days);
	 
	 $salary 	= $row->salary_currency .' ' . $row->salary_amount.' '. $row->salary_mode;
	 
     $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $salary);
	 
	 $access_system='';
	 if($row->student_attendance=='Yes')
	 {
		$access_system.=','.'Student attendance'; 
	 }
	 	 if($row->students=='Yes')
	 {
		$access_system.=','.'Students'; 
	 }
	 	 if($row->documents=='Yes')
	 {
		$access_system.=','.'Documents'; 
	 }
	 	 if($row->progress_reports=='Yes')
	 {
		$access_system.=','.'Progress reports'; 
	 }
	 	 if($row->configurations=='Yes')
	 {
		$access_system.=','.'Configurations'; 
	 }
	 	 if($row->student_enrolement=='Yes')
	 {
		$access_system.=','.'Student enrolement'; 
	 }
	  	 if($row->staff=='Yes')
	 {
		$access_system.=','.'Staff'; 
	 }
	 	 if($row->staff_enrolement=='Yes')
	 {
		$access_system.=','.'Staff enrolement'; 
	 }
	 	 if($row->staff_attendence=='Yes')
	 {
		$access_system.=','.'Staff attendence'; 
	 }
	 	 if($row->staff_salary=='Yes')
	 {
		$access_system.=','.'Staff salary'; 
	 }	
	  if($row->teaching_learing=='Yes')
	 {
		$access_system.=','.'Teaching learing'; 
	 }	
	  if($row->basic_masters=='Yes')
	 {
		$access_system.=','.'Basic setup'; 
	 }	
	
	$access=ltrim($access_system,",");
	
     $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $access);
	 
     $excel_row++;
    
    }
     
    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Staff Data.xls"');
    $object_writer->save('php://output');
 }
 


}

