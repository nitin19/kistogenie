<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subjects_model extends CI_Model {
	
	 var $tbl = 'subjects';
	
	 var $branch = 'branch';
	
	 var $staff = 'staff';
	 
	 var $classes = 'classes';
	 
	//var $username	= "username";
	//var $password	= "password";

    function __construct() {
        parent::__construct();
    }
	
	function get_branch($school_id){
	 $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
         return $query->result_array();
	}
	
	function get_classes($branch_id, $school_id){
	 $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch_id,"is_active"=>'1', "is_deleted="=>'0'));
		  $query = $this->db->get();
		   return  $query->result();
	}
	
	function check_subject($branch, $subject, $class, $school_id){
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"class_id"=>$class,"subject_name"=>$subject));
		  $query = $this->db->get();
		 $this->db->last_query(); 
        return $query->result();   
	}
	
	function insert_subject($subjectdata){
		if($this->db->insert($this->tbl, $subjectdata))
			return $this->db->insert_id();
		  else
			return false;
	}
	
	 function get_subjects($where=NULL, $school_id, $limit=NULL, $offset=NULL, $srch_subject){
	 $this->db->limit($limit, $offset);
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->join($this->classes, $this->classes.'.class_id = '.$this->tbl.'.class_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0',$this->branch.".is_deleted"=>'0',$this->branch.".is_active"=>'1',$this->classes.".is_deleted"=>'0',$this->classes.".is_active"=>'1', ));
		if($where != NULL){
		$this->db->where($where);
		}
		if($srch_subject != NULL){
			$this->db->where("($this->tbl.subject_name LIKE '%$srch_subject%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".subject_id", "desc"); 
		 $query = $this->db->get(); 
		 $this->db->last_query();  
        return $query->result_array();
	}
	
	
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
  $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
	}
	
	function getclassName($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
  $this->db->where('class_id', $class_id);
        $query = $this->db->get();
        return  $query->row();
	}
	
	function getsubject($subject_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
  $this->db->where('subject_id', $subject_id);
        $query = $this->db->get();
        return  $query->row();
	}
	
	 function total_subjects($where=NULL, $school_id, $srch_subject){
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->join($this->classes, $this->classes.'.class_id = '.$this->tbl.'.class_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0',$this->branch.".is_deleted"=>'0',$this->branch.".is_active"=>'1',$this->classes.".is_deleted"=>'0',$this->classes.".is_active"=>'1', ));
		if($where != NULL){
		$this->db->where($where);
		}
		if($srch_subject != NULL){
			$this->db->where("($this->tbl.subject_name LIKE '%$srch_subject%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".subject_id", "desc"); 
		 $query = $this->db->get(); 
		 $this->db->last_query(); 
        return $query->result_array();
	}
	
	function get_single_record($subject_id) {
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where('subject_id', $subject_id);
		$query = $this->db->get();
		$this->db->last_query(); 
		 return  $query->result();
	}
		
		function check_update_subject($branch, $subject, $class, $school_id, $id){
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"subject_name"=>$subject,"class_id"=>$class));
		$this->db->where('subject_id !=', $id);
		  $query = $this->db->get();
		 $this->db->last_query(); 
        return $query->result();   
	}
	
	function update_subject($id,$data){
$this->db->where('subject_id', $id);
$this->db->update($this->tbl, $data);
}

function subject_act_inact($id,$data){
$this->db->where('subject_id', $id);
$this->db->update($this->tbl, $data);
}

function get_delsubjects($school_id, $subject_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
  		$this->db->where('subject_id', $subject_id);
        $query = $this->db->get();
        return  $query->row();
	}
	
	function check_staffassignsubject($where=NULL){
	 $this->db->select('*');
        $this->db->from($this->staff);
		if($where != NULL){
		$this->db->where($where);
		}
		  $query = $this->db->get();
		 return $query->num_rows(); 
	}
	
function delete_subject($subject_id,$data){
	$this->db->where('subject_id', $subject_id);
	$this->db->update($this->tbl, $data);
}
	

}
