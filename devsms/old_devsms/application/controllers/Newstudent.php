<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Newstudent extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('student_model','authorization_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->helper(array('form', 'url'));
    }

	public function index()
	{
		$data = array();
		$data['page_name']		= $this->uri->segment(2);
		//$data['name']     	 	= $this->session->userdata('user_name');
		$data['logmode']		= $this->logmode;
		$data['error']			= $this->session->flashdata('error');
		$data['success']		= $this->session->flashdata('success');
		$data['alert_msg']		= $this->load->view('alert_msg',$data,true);
		 $this->load->view('header');
	     $this->load->view('student/new_student',$data);
		 $this->load->view('footer');

	}
	
		public function add_student()
	{
		$data = array();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('student_username', 'User Name', 'trim|required|min_length[5]|max_length[25]|xss_clean');
		$this->form_validation->set_rules('student_password', 'Password', 'trim|required|min_length[5]|max_length[25]|xss_clean');
		$this->form_validation->set_rules('student_fname', 'First Name', 'required|min_length[5]|max_length[25]');
		$this->form_validation->set_rules('student_lname', 'Last Name', 'required|min_length[5]|max_length[25]');
		$this->form_validation->set_rules('student_telephone', 'Telephone ', 'regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('student_father_mobile', 'Father Mobile ', 'regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('student_mother_mobile', 'Mother Mobile ', 'regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('student_emergency_mobile', 'Emergency Mobile ', 'regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('student_doctor_mobile', 'Doctor Mobile ', 'regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('student_father_email', 'Father Email', 'valid_email');
		$this->form_validation->set_rules('student_mother_email', 'Mother Email', 'valid_email');
		

		 if ($this->form_validation->run() == TRUE){
		    $config['upload_path']   = './uploads/'; 
			 $config['allowed_types'] =  'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload("student_avtar")){
		    $data = array('upload_data' => $this->upload->data());
	        $image_name = $data['upload_data']['file_name'];
		    if($_FILES['student_avtar']['error'] == 0 ){
				$file_name					= $data['upload_data']['file_name'];
				$data['student_avtar']		= $file_name;
			}
			}else {
			//	echo $this->upload->display_errors();
				//exit;
				 $this->session->set_flashdata('error', 'Some problem exists. Profile Image has not been added.');
                  redirect(base_url()."newstudent");exit;
              }
		 $data	= $this->input->post();
		 //$data  = $this->input->post('student_avtar');
		if($this->student_model->newstudent($data)){
				$this->session->set_flashdata('error', 'An error occured please try again later.');
				redirect(base_url()."newstudent/");exit;
			}else{
				$this->session->set_flashdata('success', 'Your Data saved sucessfully! Now continue to next steps.');
				redirect(base_url()."newstudent/#tab2");exit;
			}
		}		
		$data['page_name']		= $this->uri->segment(2);
		$data['logmode']		= $this->logmode;
		$data['error']			= $this->session->set_flashdata('error', 'Error occure');
		$data['success']		= $this->session->set_flashdata('success', 'Data Inserted Successfully');
		$data['alert_msg']		= $this->load->view('alert_msg',$data,true);
		$this->load->view('header');
		$this->load->view('student/new_student',$data);
		$this->load->view('footer');
	}
}

