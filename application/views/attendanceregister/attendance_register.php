<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>School Management</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">
    
    <link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/table-css.css" rel="stylesheet">
    
    <link href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet">
    
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
       
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
       
    <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/jquery-validate.bootstrap-tooltip.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/canvasjs.min.js"></script>
  </head>

  <body>


<div class="attendancescreen">
   		<div class="container">
        	<div class="col-sm-12">
            	<div class="loginsection">
                	<div class="logodiv">
                    	<a href="#"><img class="logo" src="<?php echo base_url();?>assets/images/logo.PNG"></a>
                        </div>
                	<div class="loginheader">
                        <h1>Olive Tree Study Support</h1>
                    </div>
<!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>
                    <div class="loginform-content">
                    <form class="loginform attendancescreen-form" method="post" action="<?php echo base_url(); ?>attendanceregister/attendance_add" id="addrole" autocomplete="off">
                    
                    <div class="loginformarea attendancescreen-area">
                    	<div class="col-sm-12 logintitle attendancescreen-title nopadding">
                        <img src="<?php echo base_url();?>assets/images/dividers.png">
                        <h1>Attendence</h1>
                        </div>
                        <div class="loginformblk">
                        <div class="form-group">
    					<label>Log In with your username</label>
    					<input type="text" class="form-control" id="username" placeholder="username" name="username">
                                        <span id="email_status"></span>
  						</div>
  						<div class="form-group">
    					<label for="exampleInputPassword1">Password</label>
    					<input type="password" class="form-control" id="password" placeholder="Password"  name="password">
                        <span id="password_status"></span>
  						</div>
                        <div class="form-group">
                        <label>In/Out</label>
  						<select class="form-control" name="in_out_status" id="inout_status">
  <option value="">select Your Status</option>
  <option value="In" >In</option>
  <option value="Out">Out</option>
</select>
</div>
                        <div class="login-button">
  						<input type="submit" class="btn btn-default" value="Save My Dashboard" name="submit">
                        </div>
                        <div class="logininfo">
                         <p>If you would like more information regarding the School Kompanion, or need to get in touch, please visit our corporate website <a href="#">here</a> or send us an <a href="#">email </a>
                        </p>
                        </div>
                        </div>
                       
                    </div>
                    
					</form>
                    </div>
                    <div class="login-links">
                    	
                        <div class="col-sm-12 col-xs-12 linkright loginlinks nopadding">
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>

<script>
   jQuery(document).ready(function(){
   jQuery.validator.addMethod("alphaUname", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/);
    });
	
	jQuery.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaLnumber", function(value, element) {
      return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);
    });

	jQuery.validator.addMethod("alphaUpwd", function(value, element) {
    return this.optional(element) || value == value.match(/^(?=.*[0-9])[a-zA-Z0-9]{6,16}$/);
    });

    // validate form on keyup and submit
    var v = jQuery("#addrole").validate({
		
		rules: {
			username: {
				  required: true,
				  maxlength: 40
			          },
			 password: {
						  required: true,
						  maxlength: 40
					   },
		     in_out_status: {
		                       required: true		 
			                }
       },
	  
	  messages: {
                 username: {

                  required: "Please Enter Your Username",
				  maxlength: "Maximum 40 characters allowed."
                 },
			    password: {
                  required: "Please Enter Your Password",
     	   		  maxlength: "Maximum 40 characters allowed."
                 },
				in_out_status: {
                    required: "Please Select Your Status"
                   }
            },
submitHandler: function(form) {
            form.submit();
}
		
    });
  });

</script>
</body>
</html>