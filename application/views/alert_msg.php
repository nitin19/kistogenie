<div class="box-header">
	<?
	if($error){
		?>
		<div class="alert alert-danger alert-dismissable" >
			
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			<?=$error;?>
		</div>
		<?
	}
	if($success){
		?>
		<div class="alert alert-success alert-dismissable" >
		  
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?=$success;?>
	   </div>
		<?
	}
   if(validation_errors()){
	   ?>
		<div class="alert alert-danger alert-dismissable">
			
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			<? echo validation_errors();?>
		</div>
		<?
	}
   ?>
</div><!-- /.box-header -->
<!--<script>
	jQuery(document).ready(function(){
		$('body').click(function(){
			$('.alert-dismissable').fadeOut('slow');
		})
	})
</script>-->

<style>
.alert
{
margin-top:20px;	
}

</style>