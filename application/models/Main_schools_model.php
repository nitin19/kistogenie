<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 class Main_schools_model extends CI_Model {
	var $tbl	= "school";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $fee_band = "fee_band";
	var $classes = "classes";
	

    function __construct() {
        parent::__construct();
    }
	
	 function getRows($where=NULL,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  if($word_search != NULL){
			  $this->db->where("(school_name LIKE '%$word_search%' OR school_phone LIKE '%$word_search%' OR school_email LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	   function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  if($word_search != NULL){
			$this->db->where("(school_name LIKE '%$word_search%' OR school_phone LIKE '%$word_search%' OR school_email LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("school_id", "desc"); 	
		   }
		
		$this->db->limit($limit, $start);
          $query = $this->db->get();
		  $this->db->last_query(); 
           return  $query->result();

	}
	
	function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	  }
	  
   function delete($schoolid) {
		$where		= array('school_id' => $schoolid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		 else
			return false;
	 }
	
	function get_single_school($schoolid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $schoolid);
          $query = $this->db->get();
          return  $query->result();
    }
	
	function getSchoolinfo($schoolid) {
          $this->db->select('*');
          $this->db->from($this->users);
		  $this->db->where('school_id', $schoolid);
          $query = $this->db->get();
          return  $query->row();
    }
	
	function udateSchool($schooldata,$whereschool) {
		$this->db->where($whereschool);
		if($this->db->update($this->tbl, $schooldata))
			return true;
		  else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
   function updateUser($userdata,$whereuser) {
		$this->db->where($whereuser);
		if($this->db->update($this->users, $userdata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	
   function check_edited_schoolname($schoolid,$schoolname) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_name"=>$schoolname));
		$this->db->where('school_id !=', $schoolid);
		  $query = $this->db->get();
		  $this->db->last_query();
          return $query->result();   
	}

       function get_school_name($school_id) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $school_id);
          $query = $this->db->get();
          return  $query->row();
    }


/*	function insertUser($data){
		if($this->db->insert($this->users, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
	function insertStudent($data){
		if($this->db->insert($this->tbl, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}*/
     
	
}
