<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Attendancereport extends CI_Controller {



	var $logmode;

	function __construct(){

       

        parent::__construct();

        if( $this->authorize->is_user_logged_in() == false ){

			$this->session->set_flashdata('error', 'Please login first.');

			redirect(base_url());

		   }

		$this->logmode	= $this->session->userdata('log_mode');

        $this->load->model(array('login_model','authorization_model','attendancereport_model'));

		$this->load->database();

        $this->load->library('session');

    }



	public function index()



	{    

	    $data							= array();

		$school_id		= $this->session->userdata('user_school_id');

		$data['branches'] = $this->attendancereport_model->getbranches($school_id);

		

		$sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;

		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;


		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;

		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;


		if($sdate_search!=''){

			//$search_condition['attendance_date']	= date('Y-m-d', strtotime($sdate_search));

			$startdate	= date('Y-m-d', strtotime($sdate_search));

		}

		

		if($edate_search!=''){

			//$search_condition['attendance_date']	= date('Y-m-d', strtotime($edate_search));

			$enddate	= date('Y-m-d', strtotime($edate_search));

		}



		if($branch_search!=''){

			$search_condition['branch_id']	= $branch_search;

		}

		if($class_search!=''){

			$search_condition['class_id']	= $class_search;

		}


		$search_condition['school_id']	= $this->session->userdata('user_school_id');

		$search_condition['is_active']	= '1';

		$search_condition['is_deleted']	= '0';

		$data['title']	      			= "Attendance report";

		$data['name']      				= $this->session->userdata('user_name');

		$data['school_id']				= $this->session->userdata('user_school_id');

		$start						    = $this->uri->segment(3,0);

		$ordrBY							= $this->uri->segment(4,0); 

		$dirc							= $this->uri->segment(5,"asc");

		$perpage						= 2500;

		$config['uri_segment']			= 3;

		$config['base_url'] 			= base_url().'attendancereport/index';

		

if($sdate_search!='' && $edate_search!='') {

	    $config['total_rows'] 			= $this->attendancereport_model->getRows($search_condition,$startdate,$enddate);

	   }


		$config['per_page'] 			= $perpage;

 		$config['postfix_string'] = "/?sdate=$sdate_search&edate=$edate_search&branch=$branch_search&class=$class_search"; 

		$this->pagination->initialize($config);

		$odr =   "attendance_id";

		$dirc	= "asc";

		

if($sdate_search!='' && $edate_search!='') {

		    $data_rows  = $this->attendancereport_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$startdate,$enddate);

	   }

		

		$data['dirc']					= $dirc;

		$data['ordrBY']					= $ordrBY;

if($sdate_search!='' && $edate_search!='') {

		$data['data_rows']				= $data_rows;

		$data['total_rows']				= $config['total_rows'];

      }

		$data['page_name']				= $this->uri->segment(1);

		$data['last_page']				= $start;

		$data['pagination']				= $this->pagination->create_links();

		$data['branch_search']			= $branch_search;

		$data['class_search']			= $class_search;

		$data['sdate_search']			= $sdate_search;

		$data['edate_search']			= $edate_search;


		$data['post_url']				= $config['postfix_string'];

		$data['logmode']				= $this->logmode;

		$data['error']					= $this->session->flashdata('error');

		$data['success']				= $this->session->flashdata('success');

		



		$this->load->view('header');



		$this->load->view('attendance/attendance_report', $data);



		$this->load->view('footer');



	}

	

	 public function check_classes()



	{     

		    $school_id = $this->session->userdata('user_school_id');

			$schoolbranch = trim($_POST['schoolbranch']); 

	        $getclasses = $this->attendancereport_model->getclasses($school_id,$schoolbranch);

			

			foreach($getclasses as $getclass) {

 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';

			}

	   }
	   
	function insert_archive_data() {
			 
			     $archivedata = array();
			
			   if (trim($this->input->post('att_sdate')) != '') {
                 $archivedata['start_date'] = trim(date('Y-m-d', strtotime($this->input->post('att_sdate'))));
                }
			
		    	if (trim($this->input->post('att_edate')) != '') {
                 $archivedata['end_date'] = trim(date('Y-m-d', strtotime($this->input->post('att_edate'))));
                }
				
				if (trim($this->input->post('att_branch')) != '') {
                 $archivedata['branch_id'] = trim($this->input->post('att_branch'));
                }
				
				if (trim($this->input->post('att_class')) != '') {
                 $archivedata['class_id'] = trim($this->input->post('att_class'));
                }
				
				if (trim($this->input->post('filename')) != '') {
                 $archivedata['archive_filename'] = trim($this->input->post('filename'));
				  $filename = trim($this->input->post('filename'));
                }
			
			$archivedata['school_id'] = $this->session->userdata('user_school_id');
			$archivedata['created_by'] = $this->session->userdata('user_name');
			$archivedata['is_active'] = '1';
			$archivedata['created_date'] = date('Y-m-d H:i:s');;
			$archivedata['updated_date'] = date('Y-m-d H:i:s');
			
			$chk_pra_filename = $this->attendancereport_model->chk_archive_filename($filename);
			
			   if( count($chk_pra_filename) > 0 ){
				   echo json_encode(array('Status'=>"alreadyexist"));
			    } else {
				  $search_condition			= array();
				  
					if($this->input->post('att_sdate')!=''){
			
						$startdate	= date('Y-m-d', strtotime($this->input->post('att_sdate')));
			
					}
			
					if($this->input->post('att_edate')!=''){
			
						$enddate	= date('Y-m-d', strtotime($this->input->post('att_edate')));
			
					}
			
					if($this->input->post('att_branch')!=''){
			
						$search_condition['branch_id']	= $this->input->post('att_branch');
			
					}
			
					if($this->input->post('att_class')!=''){
			
						$search_condition['class_id']	= $this->input->post('att_class');
			
					}
					
					$odr 	= "attendance_id";
			
					$dirc	= "asc";
					
					$attendancereport_data  = $this->attendancereport_model->getExcelData($search_condition,$odr,$dirc,$startdate,$enddate);
							
					$this->load->library("Excel");
					
					$object 			= new PHPExcel();
					 
					$object->setActiveSheetIndex(0);
					 
					$table_columns 		= array("Firstname", "Lastname", "Present","Absent","Late");
					 
					$column = 0;
					 
					foreach($table_columns as $field)
					{
					 $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
					 $column++;
					}
					
			  
					$excel_row = 2;
			 
					foreach($attendancereport_data as $row)
					{
				
					   $studentid 					= $row->student_id; 
				
					   $studentinfo 				= $this->attendancereport_model->getstudentinfo($studentid);
				
					   $totalAttendancestatus 		= $this->attendancereport_model->get_total_Attendancestatus($studentid,$startdate,$enddate);
		
					   $presentAttendancestatus 	= $this->attendancereport_model->get_present_Attendancestatus($studentid,$startdate,$enddate);
		
					   $absentAttendancestatus 		= $this->attendancereport_model->get_absent_Attendancestatus($studentid,$startdate,$enddate);
		
					   $lateAttendancestatus 		= $this->attendancereport_model->get_late_Attendancestatus($studentid,$startdate,$enddate);
		
					   $present_percentage 			=  ($presentAttendancestatus * 100) / $totalAttendancestatus;
		
					   $absent_percentage 			=  ($absentAttendancestatus * 100) / $totalAttendancestatus;
		
					   $late_percentage 			=  ($lateAttendancestatus * 100) / $totalAttendancestatus;
		
		   
			 
					  $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $studentinfo->student_fname);
					  $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $studentinfo->student_lname);
					  $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $present_percentage);
					  $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $absent_percentage);
					  $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $late_percentage);
			 
					 $excel_row++;
			
				}
		
			   $report_filename = $filename . ".xls";
			   
			   $path='/home/kistogenie/public_html/uploads/generate_report_archive';
			   
			   $filepath = $path . DIRECTORY_SEPARATOR . $report_filename;
		
			   $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		
			   $object_writer->save($filepath);
		
			   $archivedata['archive_fileurl']=	$filepath;
				
              if($this->attendancereport_model->insertarchiveReport($archivedata)) {
				 echo json_encode(array('Status'=>"true"));
			    } else {
				 echo json_encode(array('Status'=>"false"));
			       }
			     }
	        }
	  
    function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}

/******************************************************** EXCEL FUNCTION ************************************************/
	
/*public function excel_action()
  
  {
	  $search_condition			= array();
	  
	  $sdate_search 			= $this->input->post('sdate_search');
	  
	  $edate_search 			= $this->input->post('edate_search');
	  
	  $branch_search 			= $this->input->post('branch_search');
	  
	  $class_search 			= $this->input->post('class_search');
	  
		if($sdate_search!=''){

			$startdate	= date('Y-m-d', strtotime($sdate_search));

		}

		if($edate_search!=''){

			$enddate	= date('Y-m-d', strtotime($edate_search));

		}

		if($branch_search!=''){

			$search_condition['branch_id']	= $branch_search;

		}

		if($class_search!=''){

			$search_condition['class_id']	= $class_search;

		}
		
		$odr 	= "attendance_id";

		$dirc	= "asc";

		
if($sdate_search!='' && $edate_search!='') {

		    $attendancereport_data  = $this->attendancereport_model->getExcelData($search_condition,$odr,$dirc,$startdate,$enddate);

	   }
	   


			$this->load->library("Excel");
			
			$object 			= new PHPExcel();
			 
			$object->setActiveSheetIndex(0);
			 
			$table_columns 		= array("Firstname", "Lastname", "Present","Absent","Late");
			 
    $column = 0;
     
    foreach($table_columns as $field)
    {
     $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
     $column++;
    }
    
      
    $excel_row = 2;
     
    foreach($attendancereport_data as $row)
    {
		
		       $studentid 					= $row->student_id; 
		
			   $studentinfo 				= $this->attendancereport_model->getstudentinfo($studentid);
		
			   $totalAttendancestatus 		= $this->attendancereport_model->get_total_Attendancestatus($studentid,$startdate,$enddate);

			   $presentAttendancestatus 	= $this->attendancereport_model->get_present_Attendancestatus($studentid,$startdate,$enddate);

			   $absentAttendancestatus 	= $this->attendancereport_model->get_absent_Attendancestatus($studentid,$startdate,$enddate);

			   $lateAttendancestatus 		= $this->attendancereport_model->get_late_Attendancestatus($studentid,$startdate,$enddate);

			   $present_percentage 			=  ($presentAttendancestatus * 100) / $totalAttendancestatus;

			   $absent_percentage 			=  ($absentAttendancestatus * 100) / $totalAttendancestatus;

			   $late_percentage 			=  ($lateAttendancestatus * 100) / $totalAttendancestatus;

   
     
     		  $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $studentinfo->student_fname);
    		  $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $studentinfo->student_lname);
     		  $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $present_percentage);
     		  $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $absent_percentage);
     		  $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $late_percentage);
     
     $excel_row++;
    
    }

	   $filename = "Generate Report". $class_search ." ". date('Y-m-d') . ".xls";
	   
	   $path='/home/kistogenie/public_html/uploads/generate_report_archive';

	   
	   $filepath = $path . DIRECTORY_SEPARATOR . $filename;

    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    // header('Content-Type: application/vnd.ms-excel');
   //	header('Content-Disposition: attachment;filename="'.$filename.'"');
	//  $object_writer->save('php://output');
	 

		$object_writer->save($filepath);
		
		$objresult = fopen($filepath,'r');
		
  if($objresult){

			$this->session->set_flashdata('success', 'Attendance report saved successfully.');

			redirect(base_url()."attendancereport/index");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."attendancereport/index");exit;

		}
  
 }*/
 

}



