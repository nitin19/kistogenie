<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Staff_enrol extends CI_Controller
{
	function __construct(){
       
      parent::__construct();        
		$this->load->database();
		$this->load->library('image_lib');
		$this->load->model("Registration_model");  
    }

	public function index()
	{
		$this->load->view('staff_enrol');
	}

	function generate_random_username(){

    	$characters = 'abcdefghijklmnopqrstuvwxyz';

    	$string = '';

    	for ($i = 0; $i < 6; $i++) {

        	$string .= $characters[rand(0, strlen($characters) - 1)];

    	}
    	return $string;
	}

	function generate_random_password(){

        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';

        $string = '';

        for ($i = 0; $i < 8; $i++) {

            $string .= $characters[rand(0, strlen($characters) - 1)];

        }
        return $string;

    }

	public function insert_staff()
	{
	  
	  $this->load->model("Registration_model");           
           
		$data['branch']=$this->input->post('branch');
		$data['title']=$this->input->post('title');
		$data['staff_first']=$this->input->post('staff_first');
		$data['staff_last']=$this->input->post('staff_last');
		$data['dob']=$this->input->post('dob');
		$data['education']=$this->input->post('education');
		$data['email']=$this->input->post('email');
		$data['phone']=$this->input->post('phone');
        $data['today_date']= date("Y/m/d");
       

        $random_username = $this->generate_random_username();
        while(sizeof($this->Registration_model->check_registred_username("olivetreestudy")) == '0')
        {
        	$random_username = $this->generate_random_username();
        }
        $random_password = $this->generate_random_password();

        $get_school_id = $this->Registration_model->get_School_ID_from_branch($this->input->post('branch'));




        $created_date = date("Y-m-d H:i:s");
		$school_id = $get_school_id->school_id;
		$username = $random_username;
		$password = $random_password;
		$email = $this->input->post('email');


        $user_data['school_id'] = $school_id;
        $user_data['username'] = $username;
        $user_data['password'] = $password;
        $user_data['email'] = $email;
        $user_data['profile_image'] = "";
        $user_data['user_type'] = "enrolementstaff";
        $user_data['is_active'] = 1;
        $user_data['is_deleted'] = 0;
        $user_data['total_login'] = 0;
        //$user_data['last_login']
        $user_data['created_by'] = "";
        $user_data['created_date'] = $created_date;
        //$user_data['updated_date']
        //$user_data['notification_checked_at']

        $insert_user_data = $this->Registration_model->add_user($user_data);

		if($insert_user_data){

            
			$cv='';
			if(isset($_FILES['cv']['name']) && $_FILES['cv']['name'] != "")
			{
				$target_path = "../uploads/staff/";               

	            $ext = pathinfo($_FILES['cv']['name'], PATHINFO_EXTENSION);              
	            $File = md5(uniqid().time()). ".".$ext;
	            $cv = $File; 
	            $path = $target_path . $File;
	            $notification_path = "../uploads/notification/". $File;
	            move_uploaded_file($_FILES['cv']['tmp_name'], $path);
	            copy($path, $notification_path);
			}

			$staff_data['school_id'] = $school_id;
	        $staff_data['branch_id'] = $this->input->post('branch');
	        $staff_data['user_id']= $insert_user_data;
	        $staff_data['staff_fname'] = $this->input->post('staff_first');
	        $staff_data['staff_lname'] = $this->input->post('staff_last');
	        $staff_data['staff_title'] = $this->input->post('title');
	        $staff_data['staff_dob'] = date("Y-m-d", strtotime(str_replace("/", "-", $this->input->post('dob'))));
	        $staff_data['staff_education_qualification'] = $this->input->post('education');
	        $staff_data['staff_telephone'] = $this->input->post('phone');
	        $staff_data['staff_enrolement_status'] = 'applied';
	        //$staff_data['staff_postpone_date']
	        $staff_data['staff_application_date'] = $created_date;
	        $staff_data['notified']=1;
	        $staff_data['cv'] = $cv;

	        $insert_staff_data = $this->Registration_model->add_staff($staff_data);
	        if($insert_staff_data)
	        {
	        	$branchname = $this->Registration_model->getbranchName($this->input->post('branch'));
	      
	        	$to = $email;
	        	$email_message = '<p>Assalamualaikum</p>

									<p>&nbsp;</p>

									<p>We pray this email reaches you in the best state of Imaan.</p>

									<p>&nbsp;</p>

									<p>Dear <em>'.$this->input->post('staff_first').' '.$this->input->post('staff_last').'</em></p>

									<p>&nbsp;</p>

									<p>At our <em>'.$branchname->branch_name.'</em> we are currently experiencing a very high demand of enrolment for students of a similar age group.</p>

									<p>&nbsp;</p>

									<p>We would like to thank you immensely for your patience in waiting for a free taster session. We are doing our utmost best to allocate a class space for your child, and&nbsp;as soon as we have a free taster slot available we will make it a priority to contact you with the details,&nbsp;Insha&#39;Allah.</p>

									<p>&nbsp;</p>

									<p>Please accept our sincere apologies for any inconveniences caused and once again thank you for your patience and hope to contact you very soon. Insha&#39;Allah.</p>

									<p>&nbsp;</p>

									<p>Jazak&#39;Allah Hu Khairan</p>

									<p><img src="https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif" style="height:8px; width:20px" /></p>

									<p>&nbsp;</p>

									<p><strong>Wa Alikum AsSalam</strong></p>

									<p>&nbsp;</p>

									<p>&nbsp;</p>

									<p><strong>Olive Tree Study Support</strong></p>

									<p>Web:&nbsp;<strong><a href="http://olivetreestudy.co.uk/" target="_blank">olivetreestudy.co.uk</a></strong></p>

									<p><strong><a href="https://www.facebook.com/olivetree.studysupport" target="_blank">Facebook</a>&nbsp;|&nbsp;<a href="https://www.facebook.com/olivetree.studysupport" target="_blank">Twitter</a>&nbsp;|&nbsp;<a href="https://www.youtube.com/watch?v=Dn5wE1pYUdA" target="_blank">YouTube</a>&nbsp;</strong></p>

									<p>&nbsp;</p>';

	        	$subject = "Applied for School";
	        	$mail= $this->send_email($to,$email_message,$subject);

	        	$get_school_admin = $this->Registration_model->get_school_admin($school_id);

	        	$notification_data['school_id'] = $school_id;
	        	$notification_data['branch_id'] = $this->input->post('branch');
	        	$notification_data['from'] = $insert_user_data;
	        	$notification_data['to'] = $get_school_admin->id;
	        	$notification_data['subject'] = 'job application';
	        	$notification_data['message'] = 'new job application';
	        	$notification_data['attachments'] = $cv;
	        	$notification_data['is_active'] = 1;
	        	$notification_data['is_deleted'] = 0;
	        	$notification_data['created_date'] = $created_date;
	        	$notification_data['read_by'] = '';
	        	$notification_data['from_web'] = 1;
	        	$notification_data['applied_by'] = 'staff';

	        	$insert_notification_data = $this->Registration_model->insert_notification_data($notification_data);

	        	$this->session->set_flashdata('success', 'Enrolment for staff has been done successfully.');
				redirect(base_url()."Staff_enrol"); exit;
	        }
	        else
	        {
	        	$this->session->set_flashdata('error', 'Some problem exists. Enrolment can not be done.');
				redirect(base_url()."Staff_enrol");exit;
	        }			
		}
		else
		{
			$this->session->set_flashdata('error', 'Some problem exists. Enrolment can not be done.');
			redirect(base_url()."Staff_enrol");exit;
		}

	}

	function send_email($to,$message,$subject){

        
            $Olivetree = 'Olivetree';
            $from        = "binarydata2015@gmail.com";

            $headers     = 'MIME-Version: 1.0' . "\r\n";

            $headers    .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $headers    .= 'To: <'.$to.'>' . "\r\n";

            $headers    .= 'From: '.$Olivetree.'<'.$from.'>' . "\r\n";

            if($_SERVER['HTTP_HOST']!="localhost")

                mail($to, $subject, $message, $headers);

            return true;

    }


}

?>