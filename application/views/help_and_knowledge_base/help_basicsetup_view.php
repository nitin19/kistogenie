<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>



<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>

        <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>
 <li><a href="<?php echo base_url(); ?>help">Help and Knowledgebase</a></li>

    

        <li class="edit"> Basic Setup </li>        

        </ul>

        </div>


   <div class="block-content">	

      <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-3">
	<div class="list-block">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#branch" aria-controls="home" role="tab" data-toggle="tab">Branch</a></li>
      <li role="presentation"><a href="#classes" aria-controls="profile" role="tab" data-toggle="tab">Classes</a></li>
       <li role="presentation" class=""><a href="#feeband" aria-controls="home" role="tab" data-toggle="tab">Feeband</a></li>
      <li role="presentation"><a href="#subjects" aria-controls="profile" role="tab" data-toggle="tab">Subjects</a></li>
       <li role="presentation"><a href="#teacherlevel" aria-controls="profile" role="tab" data-toggle="tab">Teacher Level</a></li>
        <li role="presentation"><a href="#teacherrole" aria-controls="profile" role="tab" data-toggle="tab">Teacher Role</a></li>
         <li role="presentation"><a href="#teachertype" aria-controls="profile" role="tab" data-toggle="tab">Teacher Type</a></li>
        <li role="presentation"><a href="#term" aria-controls="profile" role="tab" data-toggle="tab">Term</a></li>
    </ul>
	</div>
    </div>
    <div class="col-sm-9">
      <div class="tab-content adm-dsh">
		<div role="tabpanel" class="tab-pane active" id="branch">
						<h1 class="tab-title">What is Branch?</h1>
			<div class="admn-content">
					<img src="<?php echo base_url();?>uploads/basicsetup/Basic-setup-branch.png" class="stt">
                    <p class="heading-cntnt">This section display the listing of all the branches with action button.</p>
                    <h2 class="main_heading_txt text_font">HOW TO ADD THE BRANCH?</h2>
                    <p class="heading-cntnt">User can  add the branch by doing these steps:</p>
					<ul class="msg-point">
                    <li>Enter the branch name</li>
                    <li>Enter the start time and end time in branch timing</li>
                    <li>Enter the branch address</li>
                    <li>Select the days in which the particular branch is open</li>
                    <li>Now clicked on add button</li>
                    <li>User successfully added the branch</li>
                    </ul>	
                    <p class="heading-cntnt">User can search the  branch either by selecting status i.e active or inactive or search by name .</p>
                    	<ul class="msg-point">
                        <b>Action button provides following functionalities such as:</b>
                    <li><b>EDIT –</b> user can edit the record of the particular branch</li>
                    <li><b>DELETE –</b> User can delete the branch</li>
                    <li><b>ACTIVE-</b> User can also deactivate the branch by click on active of action button.</li>
                    <li><b>TOTAL-</b>  represents the total number of branches in the list.</li>
                    </ul>	
                    <p class="heading-cntnt"><b>In showing dropdown ,user can select the numbers of records to view  per page.</b></p>		
			</div>
            </div>
	 <!-- Teacher_dashboard-->
      <div role="tabpanel" class="tab-pane" id="classes">
	  
			<h1 class="tab-title">What is Classes?</h1>
			<div class="admn-content">
					<img src="<?php echo base_url();?>uploads/basicsetup/basic-setup-find-class.jpg" class="stt">
                    <p class="heading-cntnt">This section display the listing of all the Classes available in an organisation with action button.</p>
                    <h2 class="main_heading_txt text_font">HOW TO ADD THE CLASS?</h2>
                    <p class="heading-cntnt">User can  add the class  by doing these steps:</p>
                    <ul class="msg-point">
                    <li>Select the branch name</li>
                    <li>Enter the class name</li>
                    <li>Write the description about the class in the description box.</li>
                    <li>Now clicked on add button</li>
                    <li>User successfully added the class.</li>
                    </ul>
                    <div class="compse-msg">
						<h3 class="para-heading">SEARCH:</h3>
						<p class="heading-cntnt">  User can search the class either by select branch, select status or search by class name .</p>
						<p><strong>Action button provides following functionalities such as:</strong>
						<ul class="msg-point">
                        <li><b>EDIT – </b>user can edit the record of the particular class</li>
                         <li><b>DELETE – </b>User can delete the class</li>
                          <li><b>ACTIVE-</b> User can also deactivate the class by click on active of action button</li>
                          <li> Pagination  displays the number of pages</li>
                          <li><b>TOTAL-</b>  represents the total number of classes in the list.</li>
                        </ul>
                        <p><b>In showing dropdown user can select the number of records to view per page</b></p>
					</div>	
			</div>
			

	  
	  
	  </div>
		<div role="tabpanel" class="tab-pane" id="feeband">
						<h1 class="tab-title">What is Feeband?</h1>
			<div class="admn-content">
					<img src="<?php echo base_url();?>uploads/basicsetup/basic-setup-feeband.png" class="stt">
                    <p class="heading-cntnt">This section display the listing of Fees with their branch and the Classes available in an organisation with action button .</p>
                    <h2 class="main_heading_txt text_font">HOW TO ADD A FEE BAND?</h2>
                    <p class="heading-cntnt">User can  add the fees  by doing these steps:</p>
					<ul class="msg-point">
                    <li>Select  the branch name</li>
                    <li>Select the class</li>
                    <li>Select the mode like fee amount for weekly, monthly, quarterly or half yearly </li>
                    <li>Select the currency</li>
                    <li>Enter the fee amount</li>
                    <li>Write the description about the fees in the description box.</li>
                    <li>Now clicked on add button</li>
                    <li>User successfully added the fees</li>
                    <li>User can search the fees either by selected branch, class, status or searched by fee.</li>
                    </ul>	
                    	<ul class="msg-point">
                        <b>Action button provides following functionalities such as:</b>
                    <li><b>EDIT –</b> user can edit  any detail regarding the fee</li>
                    <li><b>DELETE –</b> User can delete the fee band</li>
                    <li><b>ACTIVE-</b> User can also deactivate the fee band  by click on active of action button</li>
                    <li>Pagination display the number of pages</li>
                    <li><b>TOTAL-</b>  represents the total number of fee bands  in the list.</li>
                    </ul>	
                    <p class="heading-cntnt"><b>In showing dropdown,user can select the numbers of records to view  per page</b></p>		
			</div>
            </div>
	 <!-- Teacher_dashboard-->
      <div role="tabpanel" class="tab-pane" id="classes">
	  
			<h1 class="tab-title">What is Classes?</h1>
			<div class="admn-content">
					<img src="<?php echo base_url();?>uploads/basicsetup/basic-setup-find-class.jpg" class="stt">
                    <p class="heading-cntnt">This section display the listing of all the Classes available in an organisation with action button.</p>
                    <h2 class="main_heading_txt text_font">HOW TO ADD THE CLASS?</h2>
                    <p class="heading-cntnt">User can  add the class  by doing these steps:</p>
                    <ul class="msg-point">
                    <li>Select the branch name</li>
                    <li>Enter the class name</li>
                    <li>Write the description about the class in the description box.</li>
                    <li>Now clicked on add button</li>
                    <li>User successfully added the class.</li>
                    </ul>
                    <div class="compse-msg">
						<h3 class="para-heading">SEARCH:</h3>
						<p class="heading-cntnt">  User can search the class either by select branch, select status or search by class name .</p>
						<p><strong>Action button provides following functionalities such as:</strong>
						<ul class="msg-point">
                        <li><b>EDIT – </b>user can edit the record of the particular class</li>
                         <li><b>DELETE – </b>User can delete the class</li>
                          <li><b>ACTIVE-</b> User can also deactivate the class by click on active of action button</li>
                          <li> Pagination  displays the number of pages</li>
                          <li><b>TOTAL-</b>  represents the total number of classes in the list.</li>
                        </ul>
                        <p><b>In showing dropdown user can select the number of records to view per page</b></p>
					</div>	
			</div>
	  </div>
		
		<div role="tabpanel" class="tab-pane" id="subjects">
	  
			<h1 class="tab-title">What is Subjects?</h1>
            <img src="<?php echo base_url();?>uploads/basicsetup/basic-setup-subject.png" class="stt">
           
			<div class="admn-content">
            <p class="heading-cntnt">This section display the listing Of subjects with their branch and the Classes available in an organisation with action button.</p>
                    <h2 class="main_heading_txt text_font">HOW TO ADD A SUBJECT?</h2>
                    <p class="heading-cntnt">User can add the subject by doing these steps:</p>
                    <ul class="msg-point">
                    <li>Select the branch name</li>
                    <li>Select the class</li>
                    <li>Enter the name of the subject</li>
                    <li>Write the description about the subject in description box</li>
                    <li>Now clicked on ADD button</li>
                    <li>User successfully added the subject</li>
                    </ul>
                    <div class="compse-msg">
						<p class="heading-cntnt">  User can search the subjects either by selected branch, class or search by subject</p>
						<p><strong>Action button provides following functionalities such as:</strong>
						<ul class="msg-point">
                        <li><b>EDIT – </b>User can edit  any detail regarding the subject</li>
                         <li><b>DELETE – </b>User can delete the subject</li>
                          <li><b>ACTIVE-</b>  User can also deactivate the subject  by click on active of action button</li>
                          <li> Pagination display the number of pages</li>
                          <li><b>TOTAL-</b>  represents the total number of subjects   in the list.</li>
                        </ul>
                        <p><b>In showing dropdown,user can select the numbers of records to view  per page</b></p>
					</div>	
			</div>
			

	  
	  
	  </div>	
      <div role="tabpanel" class="tab-pane" id="teacherlevel">
	  
			<h1 class="tab-title">What is Teacher Level?</h1>
            <img src="<?php echo base_url();?>uploads/basicsetup/basic-setup-teacher-level.png" class="stt">
			<div class="admn-content">
            <p class="heading-cntnt">  This section defines the level of teacher in an organization.</p>
                    <ul class="msg-point">
                    <li><strong>EARLY YEARS</strong></li>
                    <li><strong>LOWER SCHOOL</strong></li>
                    <li><strong>UPPER SCHOOL</strong></li>
                    </ul>
                     <h2 class="main_heading_txt text_font"> HOW TO ADD TEACHER LEVEL?</h2>
                    <p class="heading-cntnt">User can  add the teacher level  by doing these steps:</p>
                    <ul class="msg-point">
                    <li>Enter the teacher level  in the text box</li>
                    <li>Write the description about the teacher level  in the description box</li>
                    <li>Now clicked on ADD button</li>
                    <li>User successfully added the teacher level</li>
                    </ul>
                   
                    <div class="compse-msg">
                    <h3 class="para-heading">SEARCH:</h3>
						<p class="heading-cntnt">  User can search the teacher level by search text box</p>
						<p><strong>Action button provides following functionalities such as:</strong>
						<ul class="msg-point">
                        <li><b>EDIT – </b>user can edit  any detail regarding the teacher level</li>
                         <li><b>DELETE – </b>User can delete the teacher level</li>
                          <li><b>ACTIVE-</b>  User can also deactivate the teacher level by click on active of action button</li>
                          <li> Pagination display the number of pages</li>
                        </ul>
                        <p><b>In showing dropdown,user can select the numbers of records to view  per page</b></p>
					</div>	
			</div>
			

	  
	  
	  </div>
      <div role="tabpanel" class="tab-pane" id="teacherrole">
	  
			<h1 class="tab-title">What is Teacher Role?</h1>
            <img src="<?php echo base_url();?>uploads/basicsetup/basic-setup-role-details.png" class="stt">
			<div class="admn-content">
            <p class="heading-cntnt">This section defines the role  of teacher in an organization.</p>
                     <h2 class="main_heading_txt text_font"> HOW TO ADD A TEACHER ROLE?</h2>
                    <p class="heading-cntnt"><b>User can  add the teacher role  by doing these steps:</b></p>
                    <ul class="msg-point">
                    <li>Enter the teacher role in the text box</li>
                    <li>Write the description about the teacher role in the description box</li>
                    <li>Now clicked on ADD button</li>
                    <li>User successfully added the teacher role</li>
                    </ul>
                   
                    <div class="compse-msg">
                    <h3 class="para-heading">SEARCH:</h3>
						<p class="heading-cntnt"> User can search the teacher role by search text box</p>
						<p><strong>Action button provides following functionalities such as:</strong>
						<ul class="msg-point">
                        <li><b>EDIT – </b>user can edit  any detail regarding the teacher role</li>
                         <li><b>DELETE – </b> User can delete the teacher role </li>
                          <li><b>ACTIVE-</b> User can also deactivate the teacher role  by click on active of action button.</li>
                          <li>Pagination display the number of pages</li>
                        </ul>
                        <p><b>In showing dropdown,user can select the numbers of records to view  per page</b></p>
					</div>	
			</div>
			

	  
	  
	  </div>
      <div role="tabpanel" class="tab-pane" id="teachertype">
	  
			<h1 class="tab-title">What is Teacher Type?</h1>
            <img src="<?php echo base_url();?>uploads/basicsetup/basic-setup--type-details.png" class="stt">
              <ul class="msg-point">
                    <li>This section defines the type of teacher in an organization.</li>
                    <li>This section describe which kind of language or studies will teach by the teacher</li>
                    </ul>
			<div class="admn-content">
                     <h2 class="main_heading_txt text_font"> HOW TO ADD TEACHER TYPE?</h2>
                    <p class="heading-cntnt"><b>User can  add the teacher type  by doing these steps:</b></p>
                    <ul class="msg-point">
                    <li>Enter the teacher type  in the text box</li>
                    <li>Write the description about the teacher  type   in the description box</li>
                    <li>Now clicked on ADD button</li>
                    <li>User successfully added the teacher type</li>
                    </ul>
                   
                    <div class="compse-msg">
                    <h3 class="para-heading">SEARCH:</h3>
						<p class="heading-cntnt"> User can search the teacher type  by search text box</p>
						<p><strong>Action button provides following functionalities such as:</strong>
						<ul class="msg-point">
                        <li><b>EDIT – </b> user can edit  any detail regarding the teacher type</li>
                         <li><b>DELETE – </b> User can delete the teacher type</li>
                          <li><b>ACTIVE-</b>  User can also deactivate the teacher type  by click on active of action button.</li>
                          <li>Pagination displays the number of pages.</li>
                        </ul>
                        <p><b>In showing dropdown,user can select the numbers of records to view per page</b></p>
					</div>	
			</div>
			

	  
	  
	  </div>
      <div role="tabpanel" class="tab-pane" id="term">
	  			<h1 class="tab-title">What is Terms?</h1>
            <img src="<?php echo base_url();?>uploads/basicsetup/basic-setup-term-details.png" class="stt">
			<div class="admn-content">
                     <h2 class="main_heading_txt text_font"> HOW TO ADD TERMS?</h2>
                    <p class="heading-cntnt"><b>User can  add the term  by doing these steps:</b></p>
                    <ul class="msg-point">
                    <li>Enter the term name  in the text box</li>
                    <li>Write the description about the term in the description box</li>
                    <li>Now clicked on ADD button</li>
                    <li>User successfully added the term</li>
                    </ul>
                   
                    <div class="compse-msg">
                    <h3 class="para-heading">SEARCH:</h3>
						<p class="heading-cntnt"> User can search the term by search text box</p>
						<p><strong>Action button provides following functionalities such as:</strong>
						<ul class="msg-point">
                        <li><b>EDIT – </b> User can edit  any detail regarding the term </li>
                         <li><b>DELETE – </b> User can delete the term</li>
                          <li><b>ACTIVE-</b> User can also deactivate the term  by click on active of action button.</li>
                          <li>Pagination displays the number of pages</li>
                        </ul>
                        <p><b>In showing dropdown,user can select the numbers of records to view  per page</b></p>
					</div>	
			</div>
			

	  
	  
	  </div>
	</div><!-- STUDENT view Report -->	
	  
	  
	  
	  </div>
    </div>
    </div>
      
      

         

	   </div>

<style>
.tabs-left, .tabs-right {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}

.tab-content.adm-dsh {
    background: #fff;
    width: 100%;
    display: inline-block;
    padding: 10px 20px;
    border: 1px solid #eee;
}
h1.tab-title {
    margin-top: 0 !important;
    margin-left: 0 !important;
    border: none !important;
    font-weight: 900;
    font-size: 32px !important;
}
.adm-dsh p {
    margin-left: 0;
}
p.main-heading-cntnt {
    margin-top: -44px;
    border-bottom: 1px solid #eeeeee;
    padding: 10px 0;
	margin-left:20px;
}
.admn-content {
    margin-top: -2px;
    padding-top: 10px;
    padding-left: 20px;
}
h3.para-heading {
        margin-top: 0px;
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
.admn-content p {
    margin-top: -7px;
}
img.stt {
    width: 100%;
    padding: 30px 0;
	margin-top:-10px;
}
.compse-msg {
    padding: 4px 0 0 0px;
}
h4.txt-frmt {
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
ul.msg-point {
    padding-left: 0;
    margin: 0 0 0 14px;
    line-height: 30px;
    font-size: 15px;
}
.compse-msg {
    padding: 20px 0 16px 0px;
    line-height: 28px;
}
h3.para-heading.mgn {
    margin-top: 20px;
}
.list-block {
    height: 634px;
    width: 100% !important;
    background: #fff;
}
p.spacing {
    line-height: 15px;
    margin-top: -17px;
}
.text_font{font-weight:bold;}
.editprofile-content .tab-content h1{padding:10px 22px 0;}
</style>