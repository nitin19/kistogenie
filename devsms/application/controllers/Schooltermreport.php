<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schooltermreport extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','schooltermreport_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{
		
		 $data							= array();
		$school_id		= $this->session->userdata('user_school_id');
	   
	    $data['branches'] = $this->schooltermreport_model->getbranches($school_id); 
		$data['sterms'] = $this->schooltermreport_model->getterms($school_id);
		$data['syears'] = $this->schooltermreport_model->getyears($school_id);
		
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;
		$term_search					= isset($_GET['term'])?$_GET['term']:NULL;
		$year_search					= isset($_GET['year'])?$_GET['year']:NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		if($class_search!=''){
			$search_condition['class_id']	= $class_search;
		}
		
		if($term_search!=''){
			$search_condition['term']	= $term_search;
		}
		
		if($year_search!=''){
			$search_condition['progress_report_year']	= $year_search;
		}
		
		$search_condition['school_id']	= $this->session->userdata('user_school_id');
		
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		
		$data['title']	      			= "Term report";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'Schooltermreport/index';
		

	    $config['total_rows'] 			= $this->schooltermreport_model->getRows($search_condition);
	
	   
		$config['per_page'] 			= $perpage;
 		$config['postfix_string'] = "/?branch=$branch_search&class=$class_search&term=$term_search&year=$year_search"; 
		$this->pagination->initialize($config);
		$odr =   "progress_report_id";
		$dirc	= "desc";
		

		$data_rows  = $this->schooltermreport_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc);
	 
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
   
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['class_search']			= $class_search;
		$data['term_search']			= $term_search;
		$data['year_search']			= $year_search;
		$data['PerPage']				= $PerPage;
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');

		$this->load->view('header');

		$this->load->view('progressreport/school_term_report', $data);

		$this->load->view('footer');

	}
	
	
		   public function check_classes()

	{     
		    $school_id = $this->session->userdata('user_school_id');
			$schoolbranch = trim($_POST['schoolbranch']); 
	        $getclasses = $this->schooltermreport_model->getclasses($school_id,$schoolbranch);
			
			foreach($getclasses as $getclass) {
 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
			}
	  }
	
	
/*		 public function check_filters()

	{     
		    $school_id = $this->session->userdata('user_school_id');
			$schoolbranch = trim($_POST['schoolbranch']); 
			
	        $getclasses = $this->schooltermreport_model->getclasses($school_id,$schoolbranch);
			$getterms = $this->schooltermreport_model->getterms($school_id,$schoolbranch);
			$getyears = $this->schooltermreport_model->getyears($school_id,$schoolbranch);
			
			$classHTML = '';
			$termHTML = '';
			$YearHTML = '';
			
			foreach($getclasses as $getclass) {
 			 $classHTML.='<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
			}
		   foreach($getterms as $getterm) {
 			 $termHTML.= '<option value="'.$getterm->term_id.'">'.$getterm->term_name.'</option>';
			}
			
			foreach($getyears as $getyear) {
 			 $YearHTML.= '<option value="'.$getyear->progress_report_year.'">'.$getyear->progress_report_year.'</option>';
			}
			
			  echo json_encode(array('Class'=>$classHTML,'Term'=>$termHTML,'Year'=>$YearHTML,));
	   }*/
	   
	    function check_progressreport_archive() {
			$filename   =  trim($_POST['filename']);
	$chk_pra_filename = $this->schooltermreport_model->chk_archive_filename($filename);
          if( count($chk_pra_filename) > 0 ){
		           echo 'false';
			      } else {
			       echo 'true';
				  }
	        }
			
		 function insert_archive_data() {
			 
		     $archivedata = array();
			
			   if (trim($this->input->post('pra_branch')) != '') {
                 $archivedata['branch_id'] = trim($this->input->post('pra_branch'));
                }
			
		    	if (trim($this->input->post('pra_class')) != '') {
                 $archivedata['class_id'] = trim($this->input->post('pra_class'));
                }
				
				if (trim($this->input->post('pra_term')) != '') {
                 $archivedata['term'] = trim($this->input->post('pra_term'));
                }
				
				if (trim($this->input->post('pra_year')) != '') {
                 $archivedata['progress_report_year'] = trim($this->input->post('pra_year'));
                }
				
				if (trim($this->input->post('filename')) != '') {
                 $archivedata['archive_filename'] = trim($this->input->post('filename'));
				  $filename = trim($this->input->post('filename'));
                }
			
			$archivedata['school_id'] = $this->session->userdata('user_school_id');
			$archivedata['created_by'] = $this->session->userdata('user_name');
			$archivedata['is_active'] = '1';
			$archivedata['created_date'] = date('Y-m-d H:i:s');;
			$archivedata['updated_date'] = date('Y-m-d H:i:s');
			
			$chk_pra_filename = $this->schooltermreport_model->chk_archive_filename($filename);
			   if( count($chk_pra_filename) > 0 ){
				   echo json_encode(array('Status'=>"alreadyexist"));
			    } else {
              if($this->schooltermreport_model->insertarchiveReport($archivedata)) {
				 echo json_encode(array('Status'=>"true"));
			    } else {
				 echo json_encode(array('Status'=>"false"));
			       }
			     }
	        }

	 public function generatepdf()
	{	
		  $data 	    =  array();
		  $where 	  	=  array();  
		
		if(trim($this->input->post('branch'))!=''){
			$branch = trim($this->input->post('branch'));
			$where['branch_id']	= trim($this->input->post('branch'));
		} else {
			$branch = '';
		}

		if(trim($this->input->post('class'))!=''){
			$class = trim($this->input->post('class'));
			$where['class_id']	= trim($this->input->post('class'));
		} else {
			$class = '';
		}
		
		if(trim($this->input->post('term'))!=''){
			$term = trim($this->input->post('term'));
			$where['term']	= trim($this->input->post('term'));
		} else {
			$term = '';
		}
		
		if(trim($this->input->post('year'))!=''){
			$year = trim($this->input->post('year'));
			$where['progress_report_year']	= trim($this->input->post('year'));
		} else {
			$year = '';
		}
		
		if(trim($this->input->post('perlimit'))!='') {
		    $perlimit	= trim($this->input->post('perlimit'));	
		 } else {
			$perlimit	= '';	
		 }

		$where['school_id']	= $this->session->userdata('user_school_id');
		
		$where['is_active']	    = '1';
		$where['is_deleted']	= '0';



		if (count(array_filter($this->input->post('Chk_student_ids'))) > 0) {
		 		$chk_student_ids_str = $this->input->post('Chk_student_ids');
				$data['results'] = $this->schooltermreport_model->get_print_pdf_data($chk_student_ids_str,$where);
			   } else {
			     	}
			  
		$data['where'] = 	$where;  

		$data['content']		   = $this->load->view('progressreport/generate_pdf',$data,true);
		$this->load->helper("dompdf");
		$filename					= "Report";
		pdf_create($data['content'],$filename,TRUE);

		$this->load->view('header');

		$this->load->view('progressreport/generate_pdf', $data);

		$this->load->view('footer');
	}        
	  
    function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}
	


/*pdf print fuction*/
public function print_item()

    {
       //     load library
        //$this->load->library('pdf');
        //$pdf = $this->pdf->load();
       // retrieve data from model
        //$data['all_itemreport'] = $this->itemreport->get_items();
        //$data['title'] = "items";
        //ini_set('memory_limit', '256M'); 
       // boost the memory limit if it's low ;)
        //$html = $this->load->view('itemreport/plist_item', $data, true);
         $data 	    =  array();
		  $where 	  	=  array();  
		
		if(trim($this->input->post('branch'))!=''){
			$branch = trim($this->input->post('branch'));
			$where['branch_id']	= trim($this->input->post('branch'));
		} else {
			$branch = '';
		}

		if(trim($this->input->post('class'))!=''){
			$class = trim($this->input->post('class'));
			$where['class_id']	= trim($this->input->post('class'));
		} else {
			$class = '';
		}
		
		if(trim($this->input->post('term'))!=''){
			$term = trim($this->input->post('term'));
			$where['term']	= trim($this->input->post('term'));
		} else {
			$term = '';
		}
		
		if(trim($this->input->post('year'))!=''){
			$year = trim($this->input->post('year'));
			$where['progress_report_year']	= trim($this->input->post('year'));
		} else {
			$year = '';
		}
		
		if(trim($this->input->post('perlimit'))!='') {
		    $perlimit	= trim($this->input->post('perlimit'));	
		 } else {
			$perlimit	= '';	
		 }

		$where['school_id']	= $this->session->userdata('user_school_id');
		
		$where['is_active']	    = '1';
		$where['is_deleted']	= '0';



		if (count(array_filter($this->input->post('Chk_student_ids'))) > 0) {
		 		$chk_student_ids_str = $this->input->post('Chk_student_ids');
				$data['results'] = $this->schooltermreport_model->get_print_pdf_data($chk_student_ids_str,$where);
			   } else {
			     	}
			  
		$data['where'] = 	$where;  

		//$data['content']		   = $this->load->view('progressreport/generate_pdf',$data,true);
		//$this->load->helper("dompdf");
		//$filename					= "Report";
		//pdf_create($data['content'],$filename,TRUE);


        //$this->load->view('header');

		$this->load->view('progressreport/generate_pdf', $data);

		//$this->load->view('footer');
       /*// render the view into HTML
        $pdf->WriteHTML($html); // write the HTML into the PDF
        $output = 'itemreport' . date('Y_m_d_H_i_s') . '_.pdf';
        $pdf->Output("$output", 'I'); // save to file because we can*/
        //exit();
    }

}