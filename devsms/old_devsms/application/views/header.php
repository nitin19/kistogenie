<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>School Management</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/table-css.css" rel="stylesheet">
    
    <link href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet">
    
    <link href="<?php echo base_url();?>assets/css/jquery-ui.css" rel="stylesheet">
<!--   <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
   <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->

    

    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->

  <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
  
    <!--<script src="<?php //echo base_url();?>assets/js/jquery.validate.js"></script>-->
    
    <script src="<?php echo base_url();?>assets/js/jquery-validate.bootstrap-tooltip.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
     <script src="<?php echo base_url();?>assets/js/canvasjs.min.js"></script>
     
   
    
   <!-- <script src="<?php echo base_url();?>assets/js/jquery.validationEngine-en.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.validationEngine.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/validationEngine.jquery.css" type="text/css"/>-->

  </head>

  <body>





	<div class="editdetailpage">

    	<div class="col-sm-2 detailleft nopadding">

    		<div class="greenbg">

    		<img src="<?php echo base_url();?>assets/images/logo.PNG">

    		</div>

            <div class="sidebardiv">

        <!-- Collect the nav links, forms, and other content for toggling -->

               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">

    <span class="sr-only">Toggle navigation</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

  </button>



 <div class="collapse navbar-collapse" id="navbar-collapse-1">

  <div class="dashboard-listing">

    <ul>
				<?php   $logmode		= $this->session->userdata('log_mode'); 
						$userid		= $this->session->userdata('user_id');
						$userimage = $this->session->userdata('user_image');
						
						if($logmode == 'superadmin'){?>
                        <li><a href="<?php echo base_url();?>schools/">Schools</a></li>
						<?php	
							}
						
				if($logmode == 'admin'){
				?>
            <li class="dashboard"><a href="<?php echo base_url();?>admindashboard" style="cursor:pointer;"><i class="fa fa-folder"></i>Dashboard</a></li>
            
            <li class="student_enrolment"><a href="<?php echo base_url();?>studentenrolement"><i class="fa fa-user-plus"></i>Student Enrolment</a>
            <ul class="submenu">
			 <li><a href="<?php echo base_url();?>studentenrolementreport">Student Enrolement Report</a></li>
            </ul>
            </li>
            <li class="students"><a href="#"><i class="fa fa-user"></i>Students</a>

            <ul class="submenu">
			<li><a href="<?php echo base_url();?>students">View students</a></li>
            <li><a href="<?php echo base_url();?>students/add_new">New student</a></li>
            <!--<li><a href="<?=base_url();?>familyconfig">Family config</a></li>-->

            </ul>
            

            </li>

            <li class="attendance"><a href="#"><i class="fa fa-clock-o"></i>Student Attendance</a>

            <ul class="submenu">

            <li><a href="<?php echo base_url();?>modifyattendance">Modify attendance</a></li>

            <li><a href="<?php echo base_url();?>attendancesummary">Summary</a></li>

            <li><a href="<?php echo base_url();?>attendancereport">Generate report</a></li>

          <li><a href="<?php echo base_url();?>attendancereportarchive">Attendance archives</a></li>

            </ul>

            </li>

            <li class="reports"><a href="<?php echo base_url();?>progressreports"><i class="fa fa-book"></i>Student Progress Reports</a>

             <ul class="submenu">

            <li><a href="<?php echo base_url();?>schooltermreport">Term</a></li>
            <li><a href="<?php echo base_url();?>progressreportarchive">Report archives</a></li>
<!--   <li><a href="<?=base_url();?>viewprogressreport"> View report</a></li>
                 <li><a href="<?=base_url();?>modifyprogressreport">Modify report</a></li> -->
            </ul>

            </li>
            <li class="staff_enrolment"><a href="<?php echo base_url();?>staffenrolement"><i class="fa fa-user-plus"></i>Staff Enrolment</a>
            <ul class="submenu">
			 <li><a href="<?php echo base_url();?>staffenrolementreport">Staff Enrolement Report</a></li>
            </ul>
            </li>
            <li class="staff"><a href="#"><i class="fa fa-users"></i>Staff</a>

            <ul class="submenu">
			 <li><a href="<?php echo base_url();?>staff">View staff</a></li>
			 <li><a href="<?php echo base_url();?>staff/add_new">Add staff</a></li>
             
            </ul>

            </li>
            
            
            <!--<li class="staff"><a href="#"><i class="fa fa-address-card"></i>Generate Attendance</a>
                 <ul class="submenu">
                     <li><a href="<?php //echo base_url();?>generateattendance">Generate Attendance report</a></li>
                </ul>
            </li>-->
            
            
            <li class="documents"><a href="<?php echo base_url();?>documents"><i class="fa fa-files-o"></i>Documents</a>
            
             <!-- <ul class="submenu">

            <li><a href="<?php //echo base_url();?>documents">Add Documents</a></li>
            </ul> -->
            
            </li>
            
            <li class="useraccess"><a href="<?php echo base_url();?>useraccess"><i class="fa fa-address-book"></i>User access</a>
            <!--<li class="fees"><a href="#"><i class="fa fa-gbp"></i>Fees</a></li>-->
            <li class="useraccess"><a href="<?php echo base_url();?>teacherlearning"><i class="fa fa-address-book"></i>Teacher Learning</a>

            <li class="config"><a href="<?php echo base_url();?>schoolconfig"><i class="fa fa-cogs"></i>Configuration</a>
             <!--<ul class="submenu">
               <li><a href="<?php echo base_url();?>schools/">Schools</a></li>
            </ul>-->
            </li>
            
            
                    
                    
            <li class="masters"><a href="#"><i class="fa fa-briefcase"></i>Basic Setup</a>
            
            <ul class="submenu">
               <li><a href="<?php echo base_url();?>schoolbranch/">Branch</a></li>
               <li><a href="<?php echo base_url();?>schoolclasses/">Classes</a></li>
               <li><a href="<?php echo base_url();?>schoolfeeband/">Feeband</a></li>
               <li><a href="<?php echo base_url();?>subjects/">Subjects</a></li>
             <!--  <li><a href="<?php //echo base_url();?>lessons/">Lessons</a></li>-->
               <li><a href="<?php echo base_url();?>teacherlevel">Teacher Level</a></li>
               <li><a href="<?php echo base_url();?>teacher_role/">Teacher Role</a></li>
               <li><a href="<?php echo base_url();?>teachertype/">Teacher Type</a></li>
               <li><a href="<?php echo base_url();?>terms/">Terms</a></li>
            </ul>

       </li>
       
       <li class="staff"><a href="#"><i class="fa fa-address-card" aria-hidden="true"></i>Generate Salary</a>
            <ul class="submenu">
            <li><a href="<?php echo base_url();?>generateattendance">Generate Salary Report</a></li>
            <li><a href="<?php echo base_url();?>generateattendance/view_salary">View Salary Report</a></li>
            </ul>
            </li>

        
       <?php }
	   
	   
	   
	   if ($logmode == 'teacher'){
				?>
           <li class="dashboard"><a href="<?php echo base_url();?>teacherdashboard" style="cursor:pointer;"><i class="fa fa-folder"></i>Dashboard</a></li>
            
            <?php if($this->session->userdata('access_student_enrolement')=='Yes') { ?>
            <li class="student_enrolment"><a href="<?php echo base_url();?>studentenrolement"><i class="fa fa-user-plus"></i>Student Enrolment</a>
             <ul class="submenu">
			 <li><a href="<?php echo base_url();?>studentenrolementreport">Student Enrolement Report</a></li>
            </ul>
            </li>
            
            <?php } ?>
            
           <?php if($this->session->userdata('access_students')=='Yes') { ?>
           
           <li class="students"><a href="#"><i class="fa fa-user"></i>Students</a>
            <ul class="submenu">
			<li><a href="<?php echo base_url();?>students">View students</a></li>
            <li><a href="<?php echo base_url();?>students/add_new">New student</a></li>
             </ul>
              </li>
            
		 <?php } ?>
         
         <?php if($this->session->userdata('access_student_attendance')=='Yes') { ?>
            <li class="attendance"><a href="#"><i class="fa fa-clock-o"></i>Student Attendance</a>

            <ul class="submenu">

            <li><a href="<?php echo base_url();?>modifyattendance">Modify attendance</a></li>

            <li><a href="<?php echo base_url();?>attendancesummary">Summary</a></li>

            <li><a href="<?php echo base_url();?>attendancereport">Generate report</a></li>

          <li><a href="<?php echo base_url();?>attendancereportarchive">Attendance archives</a></li>

            </ul>
            </li>
 		<?php } ?>
        
         <?php if($this->session->userdata('access_progress_reports')=='Yes') { ?>
            <li class="reports"><a href="<?php echo base_url();?>progressreports"><i class="fa fa-book"></i>Student Progress Reports</a>

             <ul class="submenu">

            <li><a href="<?php echo base_url();?>schooltermreport">Term</a></li>
            <li><a href="<?php echo base_url();?>progressreportarchive">Report archives</a></li>
                <!--   <li><a href="<?=base_url();?>viewprogressreport"> View report</a></li>
                 <li><a href="<?=base_url();?>modifyprogressreport">Modify report</a></li> -->
            </ul>
            </li>
            
            <?php } ?>
            <?php if($this->session->userdata('access_staff')=='Yes') { ?>
            
                  <li class="staff"><a href="#"><i class="fa fa-users"></i>Staff</a>

            <ul class="submenu">
			 <li><a href="<?php echo base_url();?>staff">View staff</a></li>
			 <li><a href="<?php echo base_url();?>staff/add_new">Add staff</a></li>
             
            </ul>

            </li>
            <?php } ?>
            <?php if($this->session->userdata('access_staff_enrolement')=='Yes') { ?>
            <li class="staff_enrolment"><a href="<?php echo base_url();?>staffenrolement"><i class="fa fa-user-plus"></i>Staff Enrolment</a>
            <ul class="submenu">
			 <li><a href="<?php echo base_url();?>staffenrolementreport">Staff Enrolement Report</a></li>
            </ul>
            </li>
            <?php } ?>
            
            <?php /*?><li class="staff"><a href="#"><i class="fa fa-users"></i>Staff</a>
            <ul class="submenu">
			 <li><a href="<?php echo base_url();?>staff">View staff</a></li>
			 <li><a href="<?php echo base_url();?>staff/add_new">Add staff</a></li>
            </ul>
            </li><?php */?>
            
            <!--<li class="staff"><a href="#"><i class="fa fa-address-card"></i>Generate Attendance</a>
                 <ul class="submenu">
                     <li><a href="<?php //echo base_url();?>generateattendance">Generate Attendance report</a></li>
                </ul>
            </li>-->
            
           <?php if($this->session->userdata('access_documents')=='Yes') { ?> 
            <li class="documents"><a href="<?php echo base_url();?>documents"><i class="fa fa-files-o"></i>Documents</a>
             <!-- <ul class="submenu">
            <li><a href="<?php //echo base_url();?>documents">Add Documents</a></li>
            </ul> -->
            </li>
            <?php } ?>
            
        
            <!--<li class="fees"><a href="#"><i class="fa fa-gbp"></i>Fees</a></li>-->
            
           <?php if($this->session->userdata('access_teaching_learing')=='Yes') { ?> 
            <li class="useraccess"><a href="<?php echo base_url();?>teacherlearning"><i class="fa fa-address-book"></i>Teacher Learning</a></li>
				<?php } ?>
                
                
              <?php if($this->session->userdata('access_configurations')=='Yes') { ?>              
          <li class="config"><a href="#"><i class="fa fa-cogs"></i>Configuration</a>
             <ul class="submenu">
               <li><a href="<?php echo base_url();?>staff/show_user_id/<?php echo $userid; ?>">My Profile</a></li>
            </ul>
          </li>
            <?php } ?>
            
        <?php if($this->session->userdata('access_basic_masters')=='Yes') { ?>           
                    
            <li class="masters"><a href="#"><i class="fa fa-briefcase"></i>Basic Setup</a>
            
            <ul class="submenu">
               <li><a href="<?php echo base_url();?>schoolbranch/">Branch</a></li>
               <li><a href="<?php echo base_url();?>schoolclasses/">Classes</a></li>
               <li><a href="<?php echo base_url();?>schoolfeeband/">Feeband</a></li>
               <li><a href="<?php echo base_url();?>subjects/">Subjects</a></li>
             <!--  <li><a href="<?php //echo base_url();?>lessons/">Lessons</a></li>-->
               <li><a href="<?php echo base_url();?>teacherlevel">Teacher Level</a></li>
               <li><a href="<?php echo base_url();?>teacher_role/">Teacher Role</a></li>
               <li><a href="<?php echo base_url();?>teachertype/">Teacher Type</a></li>
               <li><a href="<?php echo base_url();?>terms/">Terms</a></li>
            </ul>

       </li>
       
       <?php } ?>
       
                 <?php if($this->session->userdata('access_staff_attendence')=='Yes') { ?>              
          <li class="config"><a href="#"><i class="fa fa-cogs"></i>Staff Attendance</a>
          
         <?php } ?>
               
                <?php if($this->session->userdata('access_staff_salary')=='Yes') { ?>              
          <li class="config"><a href="#"><i class="fa fa-cogs"></i>Staff Salary</a>
          
         <?php } ?>
         
       <?php } 
		if ($logmode == 'student'){
		?>   
        <li class="dashboard"><a href="<?php echo base_url();?>studentdashboard" style="cursor:pointer;"><i class="fa fa-folder"></i>Dashboard</a></li>
       <li class="config"><a href="#"><i class="fa fa-briefcase"></i>Configuration</a>
             <ul class="submenu">
               <li><a href="<?php echo base_url();?>students/view_student/<?php echo $userid; ?>">My Profile</a></li>
                <li><a href="<?php echo base_url();?>studentdashboard/myattendance">My Attendence</a></li>
                 <li><a href="<?php echo base_url();?>studentdashboard/progressreport">My Progress Report</a></li>
            </ul>
            </li>
            <?php } ?>
            
             <?php 
		if ($logmode == 'enrolementstudent'){
		?>   
<li class="dashboard"><a href="<?php echo base_url();?>studentenrolementdashboard" style="cursor:pointer;"><i class="fa fa-folder"></i>Dashboard</a></li>
            <?php } ?>
            
            
       </ul>

  </div>

            

           

            </div>

            </div>

    	</div>

    <div class="col-sm-10 detailright nopadding">

    <div class="whitebg">

    <div class="col-sm-5 headerleft">
<?php
 $userid		= $this->session->userdata('user_id');
 $user_school_id = $this->session->userdata('user_school_id');
?>
  <h1 id="schoolName"></h1>
  
       <script>
 jQuery(document).ready(function(){
	  var schoolId = <?php echo $user_school_id;?>;
	   jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>schools/get_school_info',
			dataType: 'html',
            data: { 'schoolId': schoolId },
            success :  function(resp) {
				        jQuery('#schoolName').html(resp);
			          }
				   });
	      }); 
 </script>

    </div>

    <div class="col-sm-7 nopadding headerright">

    <ul class="dashboardright">

    <li class="searchbarbox">

 <!--   <form class="navbar-form searchbar navbar-left">

     <div class="form-group">

     <input type="text" class="form-control" placeholder="Search">

     </div>

     <input type="button" value="" class="submitbtn">

     </form>-->

     </li>
     
     <li class="notification dropdown">
  <a href="#" id="notificationChecking"class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">  
     <img src="<?php echo base_url();?>assets/images/notification-icon.png">
     <span id="notificationsCount" class="counter" style="display:none"></span>
      </a>
          <ul id="userNotifications" class="dropdown-menu">
          </ul>
     </li>
     
     <script>
 jQuery(document).ready(function(){
	  	 var userId = <?php echo $userid;?>;
		 var schoolId = <?php echo $user_school_id;?>;			   
		jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>notifications/count_notifications',
			dataType: 'html',
            data: { 'userId': userId,'schoolId': schoolId },
            success :  function(responsenotificationcount) {
				           if(responsenotificationcount!=0) {
							 jQuery('#notificationsCount').show();
				           jQuery('#notificationsCount').html(responsenotificationcount);
						  } else {
							 jQuery('#notificationsCount').hide();
						    }
			          }
				   });
				   
	function autoloadNotifications() {
		 var userId = <?php echo $userid;?>;
		 var schoolId = <?php echo $user_school_id;?>;			   
		jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>notifications/count_notifications',
			dataType: 'html',
            data: { 'userId': userId,'schoolId': schoolId },
            success :  function(responsenotificationcount) {
				           if(responsenotificationcount!=0) {
							 jQuery('#notificationsCount').show();
				           jQuery('#notificationsCount').html(responsenotificationcount);
						  } else {
							 jQuery('#notificationsCount').hide();
						    }
			          }
				   });		   
	          }
			
    autoloadNotifications();
    /*setInterval(function(){ 
	 autoloadNotifications() 
	 }, 25000); */
			
	jQuery('#notificationChecking').click(function(){
		 var userId = <?php echo $userid;?>;
		 var schoolId = <?php echo $user_school_id;?>;
		 jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>notifications/update_notification_checking_status',
			dataType: 'html',
            data: { 'userId': userId,'schoolId': schoolId },
            success :  function(responseupdate) {
				        if(responseupdate=='true') {
				           jQuery('#notificationsCount').hide();
						 } else {
						   }
			          }
		    });
	 }); 
	 
	 jQuery('#notificationChecking').click(function(){
		 var userId = <?php echo $userid;?>;
		 var schoolId = <?php echo $user_school_id;?>;
		 jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>notifications/get_notifications',
			dataType: 'html',
            data: { 'userId': userId,'schoolId': schoolId },
            success :  function(responsedata) {
				        jQuery('#userNotifications').html(responsedata);
			          }
		    });
	 });


}); 
		  
</script>

     <li class="dropdown userdropdown">

          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php  
		  echo $this->session->userdata('user_name'); ?> <img src="<?php echo base_url();?>assets/images/dropdownicon.png"></a>

          <ul class="dropdown-menu">
           <?php  
		if ($logmode == 'teacher'){
		?>  
             <li><a href="<?php echo base_url();?>staff/view_user/<?php echo $userid; ?>">Profile</a></li>
             <li><a href="<?php echo base_url();?>login/lockscreen">Lock Screen</a></li>
             <!--<li><a href="#">Membership</a></li>-->
            <?php } 
		if ($logmode == 'student'){
		?>  
             <li><a href="<?php echo base_url();?>students/view_student/<?php echo $userid; ?>">Profile</a></li>
             <li><a href="<?php echo base_url();?>login/lockscreen">Lock Screen</a></li>
             <!--<li><a href="#">Membership</a></li>-->
            <?php } 
			
			if ($logmode == 'admin'){
		?>  
             <li><a href="<?php echo base_url();?>admindashboard">Profile</a></li>
             <li><a href="<?php echo base_url();?>login/lockscreen">Lock Screen</a></li>
            <!--<li><a href="#">Membership</a></li>-->
            <?php }  
			if ($logmode == 'enrolementstudent'){
			?>
             <li><a href="<?php echo base_url();?>login/lockscreen">Lock Screen</a></li>
            <?php } ?>

            <li role="separator" class="divider"></li>
            <li><a href="<?php echo base_url();?>logout">Logout</a></li>

          </ul>

        </li>

        <li class="profilepic">
        
        <?php 
		if ($logmode == 'teacher'){
		if($userimage!=''){
		?>
		<img src="<?php echo base_url();?>uploads/staff/<?php echo $userimage;?>" alt="noimage">
        <?php
		}else{
		?>
        <img src="<?php echo base_url();?>assets/images/avtar.png" alt="noimage" >
        <?php } }?>
        
        
		<?php
		if ($logmode == 'student'){  
			if($userimage!=''){
		?>
        		
		<img src="<?php echo base_url();?>uploads/student/<?php echo $userimage;?>" alt="noimage">
        <?php
		}else{
		?>
        <img src="<?php echo base_url();?>assets/images/avtar.png" alt="noimage" >
        <?php }
		 }?>
         
         
         <?php
		if ($logmode == 'enrolementstudent'){  
			if($userimage!=''){
		?>
		<img src="<?php echo base_url();?>uploads/student/<?php echo $userimage;?>" alt="noimage">
        <?php
		}else{
		?>
        <img src="<?php echo base_url();?>assets/images/avtar.png" alt="noimage" >
        <?php }
		 }?>
        
      <!--  <img src="<?php //echo base_url();?>assets/images/profileimg.jpg">-->
        </li>

     

     </ul>

    </div>

    </div>

  <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<style>
.fa-cog + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-cog+ .tooltip > .tooltip-arrow {border-top-color:#090 !important;} 
.fa-question-circle + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-question-circle+ .tooltip > .tooltip-arrow {border-top-color:#090 !important;} 
</style>
  