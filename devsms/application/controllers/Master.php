<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','Master_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }

	public function index(){
	    $this->load->view('header');
        $this->load->view('master/teacher_type');
        $this->load->view('footer');
		
}
    public function teacher_level()
	 {
		$this->load->view('header');
		$this->load->view('master/teacher_level');
		$this->load->view('footer');
	  }
	  
	
	public function teacher_role(){
		$school_id		= $this->session->userdata('user_school_id');
		$data['result'] = $this->master_model->get_branch($school_id);
		$data['result1'] = $this->master_model->get_role($school_id);
				
		$this->load->view('header');
		$this->load->view('master/teacher_role' ,$data);
		$this->load->view('footer');
	}
	
	public function add_new_teachrole(){
		$school_id		= $this->session->userdata('user_school_id');
		
		$roledata = array();
		
		$roledata['school_id'] = $school_id;
		 $roledata['branch_id'] = $this->input->post('branch');
		 $roledata['teacher_role'] = $this->input->post('role');
		 $roledata['role_description'] = $this->input->post('description');
		  $roledata['is_active'] = 1 ;
		   $roledata['is_deleted'] = 0 ;
		    $roledata['created_by'] = $this->session->userdata('user_name');
			$roledata['created_by'] = $this->session->userdata('user_name');
			$roledata['created_date'] = date('Y-m-d H:i:s');
			$roledata['updated_date'] = date('Y-m-d H:i:s');
					
	 
		 $ttt = $this->master_model->insert_teach_role($roledata);
		 
		  if($ttt){
				$this->session->set_flashdata('success', 'Teacher Role has been added successfuly.');
				redirect(base_url()."master/teacher_role");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Role has not been added.');
				redirect(base_url()."master/teacher_role");exit;
			   }
	}
	
	
}

