<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teacherlevel_model extends CI_Model {
	
	 var $tbl = 'teacher_level';
	
	 var $branch = 'branch';
	
	 var $staff = 'staff';
	 
	 var $school='school';
	//var $username	= "username";
	//var $password	= "password";

    function __construct() {
        parent::__construct();
    }
	
	function get_branch($school_id){
	 	$this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
		$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result_array();
	}
 
 /*function get_levels($where=NULL, $school_id, $limit=NULL, $offset=NULL, $level_srch){
		$this->db->limit($limit, $offset);
		$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', $this->branch.".is_deleted"=>'0', $this->branch.".is_active"=>'1'));
		if($where != NULL){
		$this->db->where($where);
		}
		if($level_srch != NULL){
			$this->db->where("($this->tbl.teacher_level LIKE '%$level_srch%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		 $query = $this->db->get(); 
         $this->db->last_query();
        return $query->result_array();
	}*/
	
	
	/*function check_teach_role($level,$branch,$school_id){
	 	$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"teacher_level"=>$level ));
		$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();   
	}*/
	
		function insert_teach_level($leveldata){
		if($this->db->insert($this->tbl, $leveldata))
			return $this->db->insert_id();
		  else
			return false;
	}
	
	/* function totallevel($where=NULL, $school_id, $level_srch){
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', $this->branch.".is_deleted"=>'0', $this->branch.".is_active"=>'1'));
		if($where != NULL){
		$this->db->where($where);
		}
		if($level_srch != NULL){
			$this->db->where("($this->tbl.teacher_level LIKE '%$level_srch%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		$query = $this->db->get(); 
		$this->db->last_query(); 
        return $query->result_array();
	}
*/	
	function get_single_record($id) {
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where('id', $id);
		$query = $this->db->get();
		$this->db->last_query(); 
		return $query->result_array();
	}
	
/*function check_update_teach_level($branch, $level, $school_id, $id){
	 	$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"teacher_level"=>$level));
		$this->db->where('id !=', $id);
    	$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();   
	}
*/
	  function update_teacher_level($id,$data){
		$this->db->where('id', $id);
		$this->db->update($this->tbl, $data);
	}

	  function teacher_action($id,$data){
		$this->db->where('id', $id);
		$this->db->update($this->tbl, $data);
	}

/*	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
  		$this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }*/
		
	function getteacherlevel($techerlevel_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
  		$this->db->where('id', $techerlevel_id);
        $query = $this->db->get();
        return  $query->row();
	}
	

	 function get_dellevel($school_id, $id) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		 // $this->db->where('class_id', $classid);
		  $this->db->where(array("school_id"=>$school_id, "id"=>$id));
          $query = $this->db->get();
		  $this->db->last_query();
          return  $query->row();
      }
	
/*	function check_staffassignlevel($teacherlevel_id, $school_id, $branch_id){
	 $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("school_id"=>$school_id,"staff_teacher_level"=>$teacherlevel_id,"branch_id"=>$branch_id));
		  $query = $this->db->get();
		 $this->db->last_query();  
		 return $query->num_rows(); 
       // return $query->result();   
	}
	*/
	
	/* ***************************** changes for teacherlevel ******************************* */	
	function getschoolName($school_id) {
        $this->db->select('*');
        $this->db->from($this->school);
  		$this->db->where('school_id', $school_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	function check_update_teach_level($level, $school_id, $id){
	 	$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"teacher_level"=>$level));
		$this->db->where('id !=', $id);
    	$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();   
	}

	 function get_levels($where=NULL, $school_id, $limit=NULL, $offset=NULL, $level_srch){
	 $this->db->limit($limit, $offset);
	 $this->db->select('*');
        $this->db->from($this->tbl);
		//$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0'));
		if($where != NULL){
		$this->db->where($where);
		}
		if($level_srch != NULL){
			$this->db->where("($this->tbl.teacher_level LIKE '%$level_srch%')", NULL, FALSE);
		}
		 $this->db->order_by($this->tbl.".id", "desc"); 
		 $query = $this->db->get(); 
         $this->db->last_query();
        return $query->result_array();
	}
	
	 function totallevel($where=NULL, $school_id, $level_srch){
		$this->db->select('*');
        $this->db->from($this->tbl);
		//$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0'));
		if($where != NULL){
		$this->db->where($where);
		}
		if($level_srch != NULL){
			$this->db->where("($this->tbl.teacher_level LIKE '%$level_srch%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		$query = $this->db->get(); 
		$this->db->last_query(); 
        return $query->result_array();
	}
	function check_teach_role($level, $school_id){
	 	$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"teacher_level"=>$level,"is_deleted"=>'0'));
		$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();   
	}
	
	function check_staffassignlevel($teacherlevel_id, $school_id){
	 	$this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("school_id"=>$school_id,"staff_teacher_level"=>$teacherlevel_id));
		$query = $this->db->get();
		$this->db->last_query();  
		return $query->num_rows(); 
       /* return $query->result();*/   
	}
}
