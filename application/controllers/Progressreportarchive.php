<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Progressreportarchive extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','progressreportarchive_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{
		 $data							= array();
		$school_id		= $this->session->userdata('user_school_id');
	   
	    $data['branches'] = $this->progressreportarchive_model->getbranches($school_id); 
		$data['sterms'] = $this->progressreportarchive_model->getterms($school_id);
		$data['syears'] = $this->progressreportarchive_model->getyears($school_id);
		
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;
		$term_search					= isset($_GET['term'])?$_GET['term']:NULL;
		$year_search					= isset($_GET['year'])?$_GET['year']:NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		if($class_search!=''){
			$search_condition['class_id']	= $class_search;
		}
		
		if($term_search!=''){
			$search_condition['term']	= $term_search;
		}
		
		if($year_search!=''){
			$search_condition['progress_report_year']	= $year_search;
		}
		
		$search_condition['school_id']	= $this->session->userdata('user_school_id');
		
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		
		$data['title']	      			= "Progress report archive";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'progressreportarchive/index';
		

	    $config['total_rows'] 			= $this->progressreportarchive_model->getRows($search_condition);
	
	   
		$config['per_page'] 			= $perpage;
 		$config['postfix_string'] = "/?branch=$branch_search&class=$class_search&term=$term_search&year=$year_search"; 
		$this->pagination->initialize($config);
		$odr =   "archive_id";
		$dirc	= "desc";
		

		$data_rows  = $this->progressreportarchive_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc);
	 
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
   
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['class_search']			= $class_search;
		$data['term_search']			= $term_search;
		$data['year_search']			= $year_search;
		$data['PerPage']				= $PerPage;
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		$this->load->view('header');
		$this->load->view('progressreport/progress_report_archive', $data);
		$this->load->view('footer');

	}
	
	
	  public function check_classes()

	{     
		    $school_id = $this->session->userdata('user_school_id');
			$schoolbranch = trim($_POST['schoolbranch']); 
	        $getclasses = $this->progressreportarchive_model->getclasses($school_id,$schoolbranch);
			
			foreach($getclasses as $getclass) {
 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
			}
	  }
	  
	  
	  public function delete($id){
		if($this->progressreportarchive_model->delete($id)){
			$this->session->set_flashdata('success', 'Report archive has been deleted successfuly.');
			redirect(base_url()."progressreportarchive");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Try again.');
			redirect(base_url()."progressreportarchive");exit;
		}
	 }
	  

}

