<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reporttemplate_model extends CI_Model {
	var $tbl		= "staff";
	var $users 		= "users";
	var $branch 	= "branch";
	var $classes 	= "classes";
	var $student 	= "student";
	var $subjects 	= "subjects";
	var $teacher_role  = "teacher_role";
	var $teacher_level = "teacher_level";
	var $teacher_type  = "teacher_type";
	var $notification  = "notification";
	var $report_template	= "student_report_template";
	var $report_dropdowns	= "report_dropdowns";
	
	 
    function __construct() {
        parent::__construct();
    }
	
	 function getRows($where=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->report_template);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  if($word_search != NULL){
		
			  $this->db->where("(title LIKE '%$word_search%' OR description LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	  function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->report_template);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  if($word_search != NULL){
	
			 $this->db->where("(title LIKE '%$word_search%' OR description LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("template_id", "desc"); 	
		   }
		
		     $this->db->limit($limit, $start);
             $query = $this->db->get();
		     $this->db->last_query(); 
             return  $query->result();

	}
	
	
	 function get_single_template($templateid) {
          $this->db->select('*');
          $this->db->from($this->report_template);
		  $this->db->where('template_id', $templateid);
          $query = $this->db->get();
          return  $query->result();
          // echo $this->db->last_query(); exit();
     }

	
	function insertTemplate($reportdata){
	
		if($this->db->insert($this->report_template, $reportdata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query(); exit();
	   }
	
	 function updateTemplates($reportdata,$where) {
		$this->db->where($where);

		if($this->db->update($this->report_template, $reportdata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit();
	 }
	 
	 function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->report_template, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	function delete($templateid){
		$where		= array('template_id' => $templateid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->report_template, $data))
			return true;
		 else
			return false;
	   }

	 function get_subcategory($subject_name,$cat_name) {
          $this->db->select('*');
          $this->db->from($this->report_dropdowns);
		  $this->db->where(array('subject_name' => $subject_name,'cat_name' => $cat_name ));
          $query = $this->db->get();
          return  $query->result();
           //echo $this->db->last_query(); exit();
     }

	 function only_subcategory($dropdownid) {
          $this->db->select(dropdown_name);
          $this->db->from($this->report_dropdowns);
		  $this->db->where(array('dropdown_id' => $dropdownid));
          $query = $this->db->get();
          return  $query->row();
           //echo $this->db->last_query(); exit();
     }
    
}
