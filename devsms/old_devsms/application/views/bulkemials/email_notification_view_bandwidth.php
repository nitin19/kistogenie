<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="editprofile-content">
    <div class="col-sm-12 profilemenus nopadding">
     <div class="col-sm-9 col-xs-8 nopadding menubaritems">
         <ul>
		<li><a href="#">Home</a></li>
        <li class="edit"><a href="#">Send Notification</a></li>        
        </ul>
        </div>
        </div>

          <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div> 	

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-12 col-xs-7 nopadding profile-bg accsection">

        <h1>Send Notification</h1>

        </div>

        </div>

        <div class="col-sm-12 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">
        

        
         <div class="staff_div" id="staffdiv">
         
             <form name="SendNotificationStaffForm" id="SendNotificationStaffForm" method="post" action="<?php echo base_url(); ?>emailnotifications/sendnotificationStaff" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
     
       <div class="editprofileform">
    
         <div class="profile-bg prfltitle profile_titlebg">
         <label class="radio-inline">
          <input type="radio" class="userType" name="user_type" id="inlineRadio1" value="staff" checked="checked"> Staff
        </label>
        <label class="radio-inline">
          <input type="radio" class="userType" name="user_type" id="inlineRadio2" value="student"> Student
        </label>
        <div class="notification_type">
        <label class="radio-inline">
          <input type="radio" name="notification_type_staff" id="inlineRadio13" value="both" checked="checked"> Both
        </label>
         
        <label class="radio-inline">
          <input type="radio" name="notification_type_staff" id="inlineRadio11" value="email"> Only Email
        </label>
        <label class="radio-inline">
          <input type="radio" name="notification_type_staff" id="inlineRadio12" value="sms"> Only SMS Notification
        </label>
         <br />
         </div>
            
        <div class="form-group">
        <label class="col-sm-3 control-label nopadding">Branch</label>
        <div class="col-sm-9 inputbox">
       <select class="form-control" name="branch_staff[]" id="branch_staff" multiple="multiple" required style="height:80px; !important;" title="Please select branch">          <?php foreach($branches as $branch) { ?>		
            <option value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>		
        <?php } ?>				                  
        </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>  
                        
        </div>
        </div>
        
        
        <div class="form-group multipleselbox">
        <label class="col-sm-3 control-label nopadding">Staff</label>
        <div class="col-sm-9 inputbox" >
    <select class="form-control" name="staff_name[]" id="staff_name" multiple="multiple" required style="height:80px; !important;" title="Please select staff">  
     </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>   
        </div>
        </div>

     <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Subject</label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="staff_email_subject" id="staff_email_subject" value="">
    </div>
  </div>

     <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Message</label>
    <div class="col-sm-9 inputbox">
      <textarea class="ckeditor" name="staff_email_message" id="staff_email_message">  </textarea>
      <script>
       CKEDITOR.replace( 'staff_email_message' );
     </script>
    </div>
  </div>
   
           </div>        
       </div>     
        
        
 <div class="editprofileform">
        <div class="cancelconfirm prevnextbtns">
        <div class="col-sm-6 nopadding">
        </div>
        <div class="col-sm-6 nopadding">
        <div class="confirmlink">
           <input type="submit" class="send_notfication" value="Send Notification">
        </div>
        </div>
        </div>

        </div>
        
         </form>
         
           </div>
        
        <div class="student_div" id="studentdiv" style="display:none"> 
        
              <form name="SendNotificationForm" id="SendNotificationForm" method="post" action="<?php echo base_url(); ?>emailnotifications/sendnotification" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
     
       <div class="editprofileform">
         <div class="profile-bg prfltitle profile_titlebg">
         
          <label class="radio-inline">
          <input type="radio" name="notification_type" id="inlineRadio5" value="both" checked="checked"> Both
        </label>

        <label class="radio-inline">
          <input type="radio" name="notification_type" id="inlineRadio3" value="email"> Only Email
        </label>
        <label class="radio-inline">
          <input type="radio" name="notification_type" id="inlineRadio4" value="sms"> Only SMS Notification
        </label>
       
         <br />
            
        <div class="form-group">
        <label class="col-sm-3 control-label nopadding">Branch</label>
        <div class="col-sm-9 inputbox">
          <select class="form-control" name="branch[]"  id="branch" multiple="multiple" required style="height:80px; !important;" title="Please select branch">          
       <?php foreach($branches as $branch) { ?>		
            <option value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>		
        <?php } ?>				                  
        </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>  
                        
        </div>
        </div>
        
        
        <div class="form-group multipleselbox">
        <label class="col-sm-3 control-label nopadding">Class</label>
        <div class="col-sm-9 inputbox" >
    <select class="form-control" name="class_name[]" id="class_name" multiple="multiple" required style="height:80px; !important;" title="Please select class">  
     </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>   
        </div>
        </div>
        
        
         <div class="form-group">
        <label class="col-sm-3 control-label nopadding">Students</label>
        <div class="col-sm-9 inputbox" id="student_name_old">
        <select class="form-control" name="studentsname[]" id="student_name" multiple="multiple" required style="height:80px; !important;" title="Please select student">
        </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>
        </div>
      </div>

     <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Subject</label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="student_email_subject" id="student_email_subject" value="">
    </div>
  </div>

     <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Message</label>
    <div class="col-sm-9 inputbox">
      <textarea class="ckeditor" name="student_email_message" id="student_email_message">  </textarea>
      <script>
       CKEDITOR.replace( 'student_email_message' );
     </script>
    </div>
  </div>
  
   
           </div>        
       </div>     
        
        
 <div class="editprofileform">
        <div class="cancelconfirm prevnextbtns">
        <div class="col-sm-6 nopadding">
        </div>
        <div class="col-sm-6 nopadding">
        <div class="confirmlink">
           <input type="submit" class="send_notfication" value="Send Notification">
        </div>
        </div>
        </div>

        </div>
        
        
         </form>
         
         </div>

             </div>

           </div>

        </div>
   
  
<script>
  jQuery(document).ready(function(){
	 
	   jQuery('.userType').click(function(){
          var inputValue = jQuery(this).attr("value");
          if(inputValue == 'staff') {
			  jQuery('#staffdiv').show();
			  jQuery('#studentdiv').hide();
		   } else {
			  jQuery('#staffdiv').hide();
			  jQuery('#studentdiv').show();
		   }
       });
	   
	   jQuery("#branch_staff").on('click', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if( branch_id!='' && branch_id!='null' ) {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/getStaff',
            data:{ 'branch_id': branch_id },
            success :  function(responsestaff) {
						jQuery('#staff_name').empty();
					    jQuery('#staff_name').html(responsestaff);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
	   jQuery("#branch").on('click', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if( branch_id!='' && branch_id!='null' ) {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/getclass',
            data:{ 'branch_id': branch_id },
            success :  function(response) {
				        response=jQuery.parseJSON(response);
						jQuery('#class_name').empty();
					    jQuery('#class_name').html(response.datacls);
						jQuery('#student_name').empty();
					    jQuery('#student_name').html(response.datastu);
			          }
				   });
				 } else {
				 return false;
				 }
			 }); 
			 
	 jQuery("#class_name").on('click', function(e){ 
   		  var class_id = jQuery(this).val();
		   var branch_id = jQuery('#branch').val();
		  if(branch_id!='' && class_id!='' && branch_id!='null' && class_id!='null') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/get_students',
            data:{ 'branch_id': branch_id, 'class_id':class_id},
            success :  function(resp) {
						jQuery('#student_name').empty();
					    jQuery('#student_name').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
	  
	  jQuery('[data-toggle="tooltip"]').tooltip();   

  jQuery("#SendNotificationStaffForm").validate({
		ignore: [],
        rules: {
			  'branch_staff[]':{
                required: true
                },
			 'staff_name[]':{
                required: true
               },
			  staff_email_subject: {
                required: true
               },
			   staff_email_message: {
                required: true
               }
          },
        messages: {
				'branch_staff[]': {
                  required: "Please select branch."
                 },
				'staff_name[]': {
                  required: "Please select staff."
                 },
				staff_email_subject: {
                  required: "Please enter subject."
                 },
				staff_email_message: {
                  required: "Please enter message."
                 }
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		
		jQuery("#SendNotificationForm").validate({
		ignore: [],
        rules: {
			  'branch[]':{
                required: true
                },
			 'class_name[]':{
                required: true
               },
			  'studentsname[]':{
                required: true
               },
			  student_email_subject: {
                required: true
               },
			   student_email_message: {
                required: true
               }
          },
        messages: {
				'branch[]': {
                  required: "Please select branch."
                 },
				'class_name[]': {
                  required: "Please select class."
                 },
			    'studentsname[]': {
                  required: "Please select student."
                 },
				student_email_subject: {
                  required: "Please enter subject."
                 },
				student_email_message: {
                  required: "Please enter message."
                 }
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });			 

 
   });
</script>  
<style>
.fa-question-circle + .tooltip > .tooltip-inner {background-color:#090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

</style>