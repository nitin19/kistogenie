<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Registration_model extends CI_Model
{
    var $tbl        = "school";
    var $users      = "users";
    var $staff_enrolement      = "staff_enrolement";
    var $notification      = "notification";
    var $branch      = "branch";
    var $username   = "username";
    var $password   = "password";
    var $student_enrolement = "student_enrolement";

    function __construct() {
        parent::__construct();
    }


    function check_registred_username($Uname) {
        $this->db->select('*');
        $this->db->from($this->users);
        $this->db->where('username', $Uname);
        $query = $this->db->get();
        return  $query->result();
    }

    function get_all_branch(){
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where(array('is_active'=>'1', 'is_deleted' => '0'));
        $query = $this->db->get();

        return $query->result();
    }

    function get_school_admin($school_id){
        $this->db->select('*');
        $this->db->from($this->users);
        $this->db->where(array('school_id' => $school_id, 'user_type' => 'admin'));
        $query = $this->db->get();

        return   $query->row();
    }

    function get_School_ID_from_branch($branch_id){
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();

        return   $query->row();
    }

    function add_user($data)
    {
        if($this->db->insert($this->users,$data))
        {
            return $this->db->insert_id();
        }        
        else            
        {
            return false;
        }
    }

    function add_staff($data)
    {
        if($this->db->insert($this->staff_enrolement,$data))
        {
            return $this->db->insert_id();
        }        
        else            
        {
            return false;
        }
    }

    function add_student($data)
    {
        if($this->db->insert($this->student_enrolement,$data))
        {
            return $this->db->insert_id();
        }        
        else            
        {
            return false;
        }
    }

    function insert_notification_data($data)
    {
        if($this->db->insert($this->notification,$data))
        {
            return $this->db->insert_id();
        }        
        else            
        {
            return false;
        }
    }


    function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from("branch");
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }

  function insert_data($data)
  {


          $this->load->library('email');
          $this->load->helper('path');
          $this->load->helper('directory'); 
          $this->load->helper('form'); 
          $subject = "Regestration Form";
          $this->db->select('*');
          $this->db->from('regestration');
          

     
     
     $to_email= "decode.success@gmail.com,info@olivetreestudy.co.uk";
     
    
     $area = $data['area'];
     $child_first = $data['child_first'];
     $child_last = $data['child_last'];
     $age = $data['age'];
     $gender = $data['gender'];
     $father = $data['father'];
     $mother = $data['mother'];
     $email = $data['email'];
     $phone = $data['phone'];
     $date =  $data['date'];
  
     // $prjectid = $dt['area'];
      //print_r($area);
      //exit();
    
  $msg = 
 " <html>
<body>
<div>
<center>
<div>
<h1>Olive Tree</h1>
</div>
</center>
<center>
<div>
<h1>
".$child_first." has filled student registered form.
</h1>
</div>
</center>
<center>
<table style='border:dotted 1px black; padding:20px; border-radius:5px;background:#fbeae4;'>

<tr>
  <td>Area(Branch): </td>
  <td>".$area."</td>
</tr>
<tr>
  <td>Child's First Name: </td>
  <td>".$child_first."</td>
</tr>
<tr>
  <td>Child's Last Name: </td>
  <td>".$child_last."</td>
</tr>
<tr>
  <td>Age: </td>
  <td>".$age."</td>
</tr>
<tr>
  <td>Gender: </td>
  <td>".$gender."</td>
</tr>
<tr>
  <td>Father's Full Name: </td>
  <td>".$father."</td>
</tr>
<tr>
  <td>Mother's Full Name: </td>
  <td>".$mother."</td>
</tr>
<tr>
  <td>E-mail Address: </td>
  <td>".$email."</td>
</tr>
<tr>
  <td>Phone: </td>
  <td>".$phone."</td>
</tr>


<br><br><br>

</div>
</div>
</table>
</center>
</body>
</html>";





    $this->email->clear(TRUE);
    $this->email->to($to_email);
    $this->email->from($email,$child_first);
    $this->email->set_mailtype("html");
    
    $this->email->subject('Regestration Form For Student');
    $this->email->message($msg); 


   $this->email->send();

   $this->db->insert('regestration',$data);

  
  
}

function insert_staff($data)
  {


          $this->load->library('email');
          $this->load->helper('path');
          $this->load->helper('directory'); 
          $this->load->helper('form'); 
          $subject = "Staff Regestration Form";
          $this->db->select('*');
          $this->db->from('staff');
          

     
     
     $to_email= "decode.success@gmail.com,info@olivetreestudy.co.uk";
     
    
     $branch = $data['branch'];
     $title = $data['title'];
     $staff_first = $data['staff_first'];
     $staff_last = $data['staff_last'];
     $dob = $data['dob'];
     
     $education = $data['education'];
     $email = $data['email'];
     $phone = $data['phone'];
     
     
  $msg = 
 " <html>
<body>
<div>
<center>
<div>
<h1>Olive Tree</h1>
</div>
</center>
<center>
<div>
<h1>
".$staff_first." has filled staff registered form.
</h1>
</div>
</center>
<center>
<table style='border:dotted 1px black; padding:20px; border-radius:5px;background:#fbeae4;'>

<tr>
  <td>Branch: </td>
  <td>".$branch."</td>
</tr>
<tr>
  <td>Title: </td>
  <td>".$title."</td>
</tr>
<tr>
  <td>Staff's First Name: </td>
  <td>".$staff_first."</td>
</tr>
<tr>
  <td>Staff's Last Name: </td>
  <td>".$staff_last."</td>
</tr>
<tr>
  <td>Date of Birth: </td>
  <td>".$dob."</td>
</tr>
<tr>
  <td>Education Qualification: </td>
  <td>".$education."</td>
</tr>
<tr>
  <td>E-mail Address: </td>
  <td>".$email."</td>
</tr>
<tr>
  <td>Phone: </td>
  <td>".$phone."</td>
</tr>

<br><br><br>

</div>
</div>
</table>
</center>
</body>
</html>";


    $this->email->clear(TRUE);
    $this->email->to($to_email);
    $this->email->from($email,$staff_first);
    $this->email->set_mailtype("html");
    
    $this->email->subject('Regestration Form For Staff');
    $this->email->message($msg); 


   $this->email->send();

   $this->db->insert('staff',$data);

  
  
}


}
?>