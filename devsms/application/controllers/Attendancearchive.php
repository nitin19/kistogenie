<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Attendancearchive extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{
		
		
		

		$this->load->view('header');

		$this->load->view('attendance/attendance_archive');

		$this->load->view('footer');

	}
	function edit_content()
    {
        $data   = array();
		$id = $this->uri->segment(4);
        $data['result'] = $this->attendancearchive_model->get_contents();
		$data['teachtype']  = $this->attendancearchive_model->get_contents($search_condition, $school_id, $data["per_page"], $page, $type_search);
		$this->load->view('attendancearchive/edit_content', $result);

    }
	

	

}

