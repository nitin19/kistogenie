<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logout extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $session_data = $this->session->userdata('logged_in');

        $this->session->unset_userdata($session_data);
        $this->session->sess_destroy();
        redirect("Login");
        
       
    }

}

?>