<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>

<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">
   

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		          <?php 

          $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

           <?php  } elseif($user_type == 'student'){?>

              <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>
    <?php }else {?>
          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
    <?php } ?>

        <li class="edit">Edit Account Details</li>        

        </ul>

        </div>


        </div>

          <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div> 	
        

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-12 col-xs-7 nopadding profile-bg accsection">

        <h1>Account Details</h1>

        </div>

        </div>

        <div class="col-sm-8 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

        
<form name="basicform" id="basicform" method="post" action="<?php echo base_url(); ?>students/edit_student/<?php echo $info->id; ?>" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
        
        <!----tab1-start---->
<div id="sf1" class="tab-pane active frm">
    <fieldset>
        
       <div class="editprofileform">

  <div class="profile-bg prfltitle profile_titlebg">
  <h1>General details</h1>


  <div class=" col-sm-12 nopadding contentdiv">
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Username<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="username" id="username" value="<?php echo $info->username; ?>"  placeholder="" required="required" autocomplete="off" maxlength="41" disabled="disabled">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Password<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input class="form-control" name="password" id="password" value="<?php echo $info->password; ?>" placeholder="" required="required" autocomplete="off" maxlength="16">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Avtar<br/><span class="tagline">JPG, PNG</span></label>
    <div class="col-sm-9 profile-picture inputbox">
  
    <input type="hidden" name="avtaruserid" id="avtaruserid" value="<?php echo $info->id; ?>" />
    
    <div class="upload-pimg-msg"></div>
    
 <div class="pimg-preview" id="dynamicavtar"> 
<?php if($info->profile_image!='') { ?> <img src="<?php echo base_url();?>uploads/student/<?php echo $info->profile_image; ?>"> 
 <?php } else { ?> 
 <img src="<?php echo base_url();?>assets/images/avtar.png">
 <?php } ?>
 </div>
   <div class="profilepicbtns"> <input type="file" name="profile_image" id="file" accept="image/*" style="padding: 0 12px; display:none;"/><i class="fa fa-picture-o" id="changeprofilepic" data-toggle="tooltip" title="Select your image" data-placement="right"></i><br/><hr class="borderline"><i class="fa fa-trash-o deleteprofilepic" id="<?php echo $info->profile_image; ?>" data-toggle="tooltip" title="Delete your image" data-placement="right"></i></div>
    </div>
  </div>
  

  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Child's FirstName<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="student_fname" id="student_fname" value="<?php echo $info->student_fname;?>" placeholder="" required="required" autocomplete="off" maxlength="61">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Child's LastName<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="student_lname" id="student_lname" value="<?php echo $info->student_lname;?>" placeholder="" required="required" autocomplete="off" maxlength="61">
    </div>
  </div>


  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Date of Birth<span>*</span></label>
    <div class="col-sm-9 inputbox datepickerdiv">
      <input type="text" name="student_dob" id="student_dob" class="datepicker1" value="<?php if($info->student_dob=='0000-00-00') { echo ''; } else { echo date('d-m-Y', strtotime($info->student_dob)); } ?>" required="required" maxlength="10">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Payment Email Address<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="email" class="form-control" id="email" name="email" value="<?php echo $info->email;?>" placeholder="" required="required" autocomplete="off" maxlength="121">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Gender<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <select name="student_gender" id="student_gender" class="form-control" required="required">
          <option value="" disabled="disabled" selected="selected">Select one </option>
 		 <option <?php if($info->student_gender == "Male"){ echo 'selected'; } ?> value="Male">Male</option>
  		 <option <?php if($info->student_gender == "Female"){ echo 'selected'; } ?> value="Female">Female</option>
		</select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Nationality <span>*</span></label>
    <div class="col-sm-9 inputbox">
  <select name="student_nationality" id="student_nationality" class="form-control" required="required">
 
    <?php 
	$countryname = 'United Kingdom';
	foreach($nationalities as $nationality) { ?>
	 <option <?php if($nationality->nationality_name == $info->student_nationality){ echo 'selected'; } else{ echo $countryname;} ?> value="<?php echo $nationality->nationality_name;?>"><?php echo $nationality->nationality_name;?></option>
    <?php } ?>
</select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Country of Birth </label>
    <div class="col-sm-9 inputbox">
<select name="student_country_of_birth" id="student_country_of_birth" class="form-control">

     <?php 
		if($info->student_country_of_birth!=""){
	 foreach($countries as $country) { ?>
	 <option <?php if($country->country_name == $info->student_country_of_birth){ echo 'selected'; } ?> value="<?php echo $country->country_name;?>"><?php echo $country->country_name;?></option>
    <?php } } else {
		
		$countryname = 'United Kingdom';
	foreach($countries as $country) { ?>
	 <option <?php if($country->country_name==$countryname) { echo 'selected'; } ?> value="<?php echo $country->country_name;?>"><?php echo $country->country_name; ?></option>
  <?php   } }?>
		
</select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">First Language</label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="student_first_language" id="student_first_language" value="<?php echo $info->student_first_language;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Other Language</label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="student_other_language" id="student_other_language" value="<?php echo $info->student_other_language;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Religion</label>
    <div class="col-sm-9 inputbox">
      <select name="student_religion" id="student_religion" class="form-control">
<option value=""> Select one</option>
 <?php foreach($religions as $religion) { ?>
	 <option <?php if($religion->religion_name == $info->student_religion){ echo 'selected'; } ?> value="<?php echo $religion->religion_name;?>"><?php echo $religion->religion_name;?></option>
   <?php  } ?>
</select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Ethnic Origin</label>
    <div class="col-sm-9 inputbox">
<select name="student_ethnic_origin" id="student_ethnic_origin" class="form-control">
   <option>Select One</option>
        <?php foreach($ethnicorigins as $ethnicorigin) { ?>
	    <option <?php if($ethnicorigin->ethnic_origin_name == $info->student_ethnic_origin){ echo 'selected'; } ?> value="<?php echo $ethnicorigin->ethnic_origin_name;?>"><?php echo $ethnicorigin->ethnic_origin_name;?></option>
       <?php } ?>
</select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Telephone <span>*</span></label>
    <div class="col-sm-9 inputbox">
     <input type="text" name="student_telephone" id="student_telephone" value="<?php echo $info->student_telephone;?>" class="form-control" required="required" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Full Address <span>*</span></label>
    <div class="col-sm-9 inputbox">
     <input type="text" name="student_address" id="student_address" value="<?php echo $info->student_address;?>" class="form-control" required="required" autocomplete="off" maxlength="251">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Full Address(Optional)</label>
    <div class="col-sm-9 inputbox">
     <input type="text" name="student_address_1" id="student_address_1" value="<?php echo $info->student_address_1;?>" class="form-control" autocomplete="off" maxlength="251">
    </div>
  </div>
  </div>
  </div>
  <div class="profile-bg prfltitle profile_titlebg">
        <h1>Parents details</h1>
        <div class=" col-sm-12 nopadding contentdiv">
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Father's Name</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_father_name" id="student_father_name" value="<?php echo $info->student_father_name;?>" class="form-control" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Father's Occupation</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_father_occupation" id="student_father_occupation" value="<?php echo $info->student_father_occupation;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="121">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Father's Mobile</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_father_mobile" id="student_father_mobile" value="<?php echo $info->student_father_mobile;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Father's Email</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_father_email" id="student_father_email" value="<?php echo $info->student_father_email;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="121">
    </div>
  </div>
 <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Mother's Name</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_mother_name" id="student_mother_name" value="<?php echo $info->student_mother_name;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
   <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Mother's Occupation</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_mother_occupation" id="student_mother_occupation" value="<?php echo $info->student_mother_occupation;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="121">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Mother's Mobile</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_mother_mobile" id="student_mother_mobile" value="<?php echo $info->student_mother_mobile;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Mother's Email</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_mother_email" id="student_mother_email" value="<?php echo $info->student_mother_email;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="121">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Emergency Name</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_emergency_name" class="form-control" id="student_emergency_name" value="<?php echo $info->student_emergency_name;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Emergency Relationship</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_emergency_relationship" class="form-control" id="student_emergency_relationship" value="<?php echo $info->student_emergency_relationship;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Emergency Mobile</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_emergency_mobile" class="form-control" id="student_emergency_mobile" value="<?php echo $info->student_emergency_mobile;?>" placeholder="" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  </div>
  </div>
  <div class="profile-bg prfltitle profile_titlebg">
        <h1>Medical details</h1>
        <div class=" col-sm-12 nopadding contentdiv">
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Doctor's Name</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="student_doctor_name" class="form-control" id="student_doctor_name" value="<?php echo $info->student_doctor_name;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Doctor's Mobile</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_doctor_mobile" class="form-control" id="student_doctor_mobile" value="<?php echo $info->student_doctor_mobile;?>" placeholder="" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Health Notes</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_helth_notes" class="form-control" id="student_helth_notes" value="<?php echo $info->student_helth_notes;?>" placeholder="" autocomplete="off" maxlength="251">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Allergies</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_allergies" class="form-control" id="student_allergies" value="<?php echo $info->student_allergies;?>" placeholder="" autocomplete="off" maxlength="251">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Other Comments</label>
    <div class="col-sm-9 inputbox">
     <textarea rows="3" class="form-control" name="student_other_comments" id="student_other_comments" autocomplete="off" maxlength="251"><?php echo $info->student_other_comments;?></textarea>
    </div>
  </div>
  
  </div>
  
   
</div>        
   </div>     
     </fieldset> 
       
        </div>
        <!----tab1-end------>
        
         <!----tab2-start---->
<div id="sf2" class="tab-pane frm" style="display: block;">
        
 <fieldset>
 <div class="editprofileform">
  <div class="profile-bg prfltitle profile_titlebg">
  <h1> Branch </h1>
  <div class="col-sm-12 contentdiv">
  <?php 
  $info->student_school_branch;
  $b = 111;
  $staff_branch_id = $this->session->userdata('staff_branch_id');
  $user_type= $this->session->userdata('user_type');
  if($user_type=='teacher' && $staff_branch_id!='' ){
  $staff_branch = explode(',',$staff_branch_id);
  foreach($staff_branch as $staff_branch_ids) { 
  $this->load->model(array('student_model'));
  $branchdta =$this->student_model->getstaff_branches($staff_branch_ids);
  ?>
	 <div class="radiobtn">
   <input type="radio" name="student_school_branch" id="radio<?php echo $b;?>" class="student_school_branch" <?php if($branchdta->branch_id == $info->student_school_branch){ echo "checked=checked"; } ?> value="<?php echo $branchdta->branch_id;?>" required="required">
   <label for="radio<?php echo $b;?>"><?php echo $branchdta->branch_name;?> <br/> <span class="checkboxline"><?php echo $branchdta->branch_address;?></span></label>
        <div class="check"></div>
           </div>
	 <?php	$b++;
		 } 
		 }elseif ($user_type=='admin'){
			 foreach($branches as $branch) { ?>
	 <div class="radiobtn">
   <input type="radio" name="student_school_branch" id="radio<?php echo $b;?>" class="student_school_branch" <?php if($branch->branch_id == $info->student_school_branch){ echo "checked=checked"; } ?> value="<?php echo $branch->branch_id;?>" required="required">
   <label for="radio<?php echo $b;?>"><?php echo $branch->branch_name;?> <br/> <span class="checkboxline"><?php echo $branch->branch_address;?></span></label>
        <div class="check"></div>
           </div>
	 <?php	$b++;
	  }
	  } else {?>
		  <h2 class="no_branch_rocords">No Branch Assign</h2>
	  <?php }?>
</div>
  </div>
  
 </div>
        
     </fieldset>
         
    </div>
        <!----tab2-end------>
        
        <!----tab3-start---->
   <div id="sf3" class="tab-pane frm" style="display: block;">
   
   <?php $info->student_school_branch; ?>
        
        <fieldset>

 <div class="editprofileform">
       
  <div class="profile-bg prfltitle profile_titlebg">
  <h1>Class Details</h1>
  <div class=" col-sm-12 nopadding contentdiv">
	<div class="form-group">
    <label class="col-sm-3 control-label nopadding">Class / Group <span></span></label>
    <div class="col-sm-9 inputbox">
<select class="form-control" name="student_class_group[]" id="student_class_group" multiple="multiple" style="height:110px; !important;">
  <option value="">Select</option>
  
   <?php 
  $staff_class_id = $this->session->userdata('staff_class_id');

  $user_type= $this->session->userdata('user_type');
  if($user_type=='teacher' && $staff_class_id!=''){
  $staff_class = explode(',',$staff_class_id);
  foreach($staff_class as $staff_class_ids) {
	  $this->load->model(array('student_model'));
	  $sclasses =$this->student_model->getstaff_classes($staff_class_ids);
  /*  $staff_class_id = $this->session->userdata('staff_class_id');
  $user_type= $this->session->userdata('user_type');
   if($user_type=='teacher' && $staff_branch_id!='') {
		  $staff_class = explode(',',$staff_class_id);
		  //$school_id = $this->session->userdata('user_school_id');
		  //$schoolbranch = $info->student_school_branch;
     foreach($staff_class as $staff_class_ids) { 
          $this->load->model(array('student_model'));
		  $sclasses =$this->student_model->getstaff_classes($staff_class_ids);*/
   ?>
   
<!--   <option <?php //if($info->student_class_group == $sclasses->class_id){ echo 'selected'; } ?> value="<?php //echo $sclasses->class_id;?>"><?php //echo $sclasses->class_name;?></option>-->
   
      <option <?php if(in_array( $sclasses->class_id, explode(',',$info->student_class_group))) { echo 'selected'; } ?> value="<?php echo $sclasses->class_id;?>"><?php echo $sclasses->class_name;?></option>
      
   <?php } 
   }elseif($user_type=='admin' && $info->student_school_branch!='') {
		  $school_id = $this->session->userdata('user_school_id');
		  $schoolbranch = $info->student_school_branch;
		  $this->load->model(array('student_model'));
		  $sclasses =$this->student_model->getclasses($school_id,$schoolbranch);
     foreach($sclasses as $sclass) { ?>
     <!--<option <?php //if($info->student_class_group == $sclass->class_id){ echo 'selected'; } ?> value="<?php //echo $sclass->class_id;?>"><?php //echo $sclass->class_name;?></option>-->
     
     <option <?php if(in_array( $sclass->class_id, explode(',',$info->student_class_group))) { echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
   
   
    <?php  } 
	}else {?>
	<option  value=""></option>	
		
<?php	}
	?>
  
</select>
    </div>
  </div>
  
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Induction Date <span></span></label>
    <div class="col-sm-9 inputbox datepickerdiv">
      <input type="text" name="student_application_date" id="student_application_date" class="datepicker2" value="<?php if($info->student_application_date=='0000-00-00') { echo ''; } else { echo date('d-m-Y', strtotime($info->student_application_date)); } ?>"  >
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Comments</label>
    <div class="col-sm-9 inputbox">
     <textarea class="form-control" rows="3" name="student_application_comment" id="student_application_comment"><?php echo $info->student_application_comment;?></textarea>
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Enrolment Date</label>
    <div class="col-sm-9 inputbox datepickerdiv">
      <input type="text" name="student_enrolment_date" id="student_enrolment_date" class="datepicker3" value="<?php if($info->student_enrolment_date=='0000-00-00') { echo ''; } else {echo date('d-m-Y', strtotime($info->student_enrolment_date)); } ?>" >
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Leaving Date</label>
    <div class="col-sm-9 inputbox datepickerdiv">
    

      <input type="text" name="student_leaving_date" id="student_leaving_date" class="datepicker4" value="<?php if($info->student_leaving_date=='0000-00-00') { echo ''; } else { echo date('d-m-Y', strtotime($info->student_leaving_date)); } ?>" onfocus="" >
      <input type="hidden" name="student_email_status" id="student_email_status" value="<?php echo $info->student_email_status; ?>" >
      <a href="#" id="request_payment" name="request_payment" data-toggle="modal" data-target="#myModal_request_payment_<?php echo $info->id;?>">Request Payment</a>
      <a href="#" id="payment_receive" name="payment_receive" data-toggle="modal" data-target="#myModal_payment_receive_<?php echo $info->id;?>">Payment Receive</a>

    </div>
  </div>
  </div>

 </div>
   </div>     
       </fieldset>
        
        
         
        </div>
        <!----tab3-end------>
         <!----tab4-start---->
        
 <div id="sf4" class="tab-pane frm" style="display: block;">
        
         <fieldset>
            
 <div class="editprofileform">

  <div class="profile-bg prfltitle profile_titlebg">
  <h1>Fee Status</h1>

 <div id="fee-band">
  <?php 
  if( $info->student_school_branch!='' && $info->student_class_group!='') {
		 
		  $school_id = $this->session->userdata('user_school_id');
		  $BranchID = $info->student_school_branch;
		  $schoolclass = $info->student_class_group;
		  $this->load->model(array('student_model'));
		  
		  $feebands = $this->student_model->getfeebands($school_id,$BranchID,$schoolclass);
		   
		  $f = 2222;
     foreach($feebands as $feeband) {  
	 $clasName = $this->student_model->getclassName($feeband->class_id); 
	     ?>
         <!--<div class="radiobtn">
   <input type="radio" name="student_fee_band" id="radio<?php //echo $f;?>" <?php //if($feeband->fee_band_id == $info->student_fee_band){ echo "checked=checked"; } ?> value="<?php //echo $feeband->fee_band_id;?>" required="required">
   <label for="radio<?php //echo $f;?>"><?php //echo $feeband->fee_band_price.' - '.$feeband->fee_band_description;?></label>
        <div class="check"></div>
          </div>-->
          
          <div class="form-group">
    <label class="col-sm-6 control-label"><?php echo $clasName->class_name; ?></label>
    <div class="col-sm-6 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="student_fee_band[]" id="fb<?php echo $f;?>" <?php if(in_array( $feeband->fee_band_id, explode(',',$info->student_fee_band))) { echo "checked=checked"; } ?> value="<?php echo $feeband->fee_band_id;?>" required="required">
    			<label for="fb<?php echo $f;?>"><span class="checkbox"><?php echo $feeband->fee_band_price.'   '.$feeband->fee_band_description;?></span></label>
                </div>
          </div>
        </div>
        <?php   $f++;
		     } 
          }
	    ?>

    </div>


  </div>
  
 </div>
        
         </fieldset>
        
        </div>
        
        <!----tab4-end------>
        
        <!----tab5-start---->
     
        
        <?php /*?><div id="sf5" class="tab-pane frm" style="display: block;">
        
        <fieldset>
            

        <div class="editprofileform">
       
  <div class="profile-bg">
  <h2>Certificates</h2>
  
  
  
   <?php 
		
		 if($info->student_certificates!='') {
			
			 $certificates =  explode(',', $info->student_certificates);
			// echo count($certificates);
			  $c = 0;
			 foreach($certificates as $certificate) {
				 $c++;
				  ?>
                <!-- <a href="<?php echo base_url();?>uploads/student_certificates/<?php echo $certificate;?>" target="_blank"><?php echo $certificate;?></a><br />-->
                
   <div id="delcertificate_<?php echo $c;?>" class=""> <h4> <?php echo $certificate;?> &nbsp;&nbsp;<a href="javascript:void(0);" class="deletecertificate" id="<?php echo $c.','.$certificate;?>"><i class="fa fa-trash-o"></i></a></h4> </div>
              
			<?php }
		   } else {
		      echo 'No certificates';
			  }

		 ?>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Upload files (Multiple)<br/><span class="tagline"></span></label>
    <div class="col-sm-9 profile-picture inputbox">
        <input type="hidden" name="scertificates" id="scertificates" value="<?php echo $info->student_certificates;?>" />
        <input type="hidden" name="postedstudentid" id="postedstudentid" value="<?php echo $info->student_id;?>" />
       <input type="file" name="student_certificates[]" multiple style="padding: 0 12px;" />
    </div>
  </div>
  
  
   </div>

 </div>
        
         </fieldset>
         
        </div><?php */?>
        
      
        <!----tab5-end------>
        
          <!----tab6-start---->
        
        <div id="sf6" class="tab-pane frm" style="display: block;">
        
        <fieldset>
        
 <div class="editprofileform">
  <div class="profile-bg prfltitle profile_titlebg">
    <h1>Sibling Details</h1>

  <div class="col-sm-12 nopadding contentdiv">
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Family Address</label>
    <div class="col-sm-9 inputbox">
      <input type="text" name="student_family_address" value="<?php echo $info-> student_family_address ;?>" placeholder="" id="student_family_address" class="form-control">
    </div>
  </div>
  
  <div class="form-group">
  <label class="col-sm-3 control-label nopadding">Add a Sibling </label>
   <div class="col-sm-9 inputbox multipleselbox">
   <select class="form-control" name="student_sibling[]" id="student_sibling" multiple size="5" style="height:130px !important;">
<!--     <option value="">Select</option>
     -->
      <?php 
	 
  if( $info->student_address!='') {
		   $school_id = $this->session->userdata('user_school_id');
		   $Favalue = $info->student_address;
		  $this->load->model(array('student_model'));
		  $siblings = $this->student_model->getsibling($Favalue,$info->email,$school_id);
		  
		  if($info->student_sibling!='') { 
			 $siblingArr = explode(',',$info->student_sibling);
			 } else {
			   $siblingArr = array();
			  }
		  if( count($siblings) > 0 ) {
        foreach($siblings as $sibling) { 
		if($sibling->student_id!=$info->student_id) {
		?>
          <option <?php if(in_array($sibling->student_id, $siblingArr)) { echo 'selected'; } ?> value="<?php echo $sibling->student_id;?>"><?php echo $sibling->student_fname.' '.$sibling->student_lname;?></option>
        <?php     }
		        }
		     }
          }
	    ?>
        
   </select>
    </div>
  </div>
  </div>
  
  
  </div>
  
  <div class="cancelconfirm prevnextbtns">
        <div class="col-sm-6 nopadding">
        <div class="cancellink">
        <input type="button" class="cancelbtn" id="cancelbtn" onclick="cancelupdate('<?php echo $info->id;?>')" value="Cancel">
        </div>
        </div>
        <div class="col-sm-6 nopadding">
        <div class="confirmlink">
        <input type="button" class="open6" value="Submit">
        </div>
        </div>
        </div>

        </div>
        
        </fieldset>
        
        </div>
        
        <!----tab6-end------>
        
         </form>
    
   

        </div>

        </div>
 <!----------------------Popup Request Payment Email Section--------------------------------------------> 
 
<div class="modal fade" id="myModal_request_payment_<?php echo $info->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-12 nopadding invitemember">

          <div class="col-sm-9 col-xs-9"><h3 class="modal-title inviteheader">Request Payment Email</h3></div>

           <div class="col-sm-3 col-xs-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

        

         <div class="alert alert-danger alert-dismissable" id="sendenroleMailError-<?php echo $info->id;?>" style="display:none" >

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		  <b>Alert!</b> 

			Some problem exists. Message has not been sent. 

		</div>

        

         <div class="alert alert-success alert-dismissable" id="sendenroleMailSuccess-<?php echo $info->id;?>" style="display:none">

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		    Message has been sent successfully. 

             </div>

        

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url();?>students/request_payment" name="enrolementemailForm" id="enrolementemailForm_<?php echo $info->id;?>" method="post">

     
		 <input type="hidden" name="student_email_status" id="student_email_status" value="<?php echo $info->student_email_status; ?>" >
        
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $info->id; ?>" />        
		
         <input type="hidden" name="school_id" id="school_id" value="<?php echo $info->school_id;?>" />

        

         <input type="hidden" name="student_email" id="student_email_<?php echo $info->id;?>" value="<?php echo $info->email;?>" class="form-control"  placeholder="" maxlength="121">

         

         <input type="hidden" name="student_email_subject" id="student_email_subject_<?php echo $info->id;?>" value="<?php echo $request_payment_email->student_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          

          <div class="form-group">


    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="student_email_message" id="student_email_message_<?php echo $info->id;?>"> 

		<?php
         $tex1 = preg_replace('/\b(name of parent)\b/u', $info->student_father_name, $request_payment_email->student_email_message);

        //$text2 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $tex1);

        $text3 = preg_replace('/\b(name of student)\b/u', $info->student_fname.' '. $info->student_lname, $tex1);

        //$text4 = preg_replace('/\b(date of enrolment)\b/u', $student_enrolment_date, $text3);

        //$text5 = preg_replace('/\b(branch contact number)\b/u', $schoolContactNo->school_phone, $text4);

        $text6 = str_replace('(', '', $text3);

        $Finaltext = str_replace(')', '', $text6);

		echo   $Finaltext; 
		  
		  

        ?>

      </textarea>

      <script>

       CKEDITOR.replace( 'student_email_message_request<?php echo $info->id;?>' );

     </script>

    </div>

  </div>

  

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingenrolemsg-<?php echo $info->id;?>"></div>

        <input class="sendenrolementemail" id="save_enrole-<?php echo $info->id;?>" value="Send" type="submit">

        </div>

        </div>

          

             </div>  

          </form>

        </div>

        <!--<div class="modal-footer">

        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->

        </div>

      </div>

      

    </div>
  
  
       
<!----------------------Popup Request Payment Email Section End-------------------------------------------->         
<!----------------------Popup Payment Receive Email Section--------------------------------------------> 
 
<div class="modal fade" id="myModal_payment_receive_<?php echo $info->id;?>" role="dialog">

    <div class="modal-dialog popupdiv">

      <!-- Modal content-->

      <div class="modal-content enrolmentpopup">

        <div class="modal-header" style="padding:0px;">

          <div class="col-sm-12 col-xs-12 nopadding invitemember">

          <div class="col-sm-9 col-xs-9"><h3 class="modal-title inviteheader">Payment Receive Email</h3></div>

           <div class="col-sm-3 col-xs-3">

           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>

           </div>

          </div>

        </div>

        

      <div style="clear:both"></div>  

        

         <div class="alert alert-danger alert-dismissable" id="sendenroleMailError-<?php echo $info->id;?>" style="display:none" >

		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		  <b>Alert!</b> 

			Some problem exists. Message has not been sent. 

		</div>

        

         <div class="alert alert-success alert-dismissable" id="sendenroleMailSuccess-<?php echo $info->id;?>" style="display:none">

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		    Message has been sent successfully. 

             </div>

        

       <div style="clear:both"></div> 

        

        <div class="modal-body">

     <form action="<?php echo base_url(); ?>students/payment_receive" name="enrolementemailForm" id="enrolementemailForm_<?php echo $info->id;?>" method="post">

     
		 <input type="hidden" name="student_email_status" id="student_email_status" value="<?php echo $info->student_email_status; ?>" >
        
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $info->id; ?>" />        
		
         <input type="hidden" name="school_id" id="school_id" value="<?php echo $info->school_id;?>" />

        

         <input type="hidden" name="student_email" id="student_email_<?php echo $info->id;?>" value="<?php echo $info->email;?>" class="form-control"  placeholder="" maxlength="121">

         

         <input type="hidden" name="student_email_subject" id="student_email_subject_<?php echo $info->id;?>" value="<?php echo $payment_receive_email->student_email_subject;?>" class="form-control"  placeholder="" maxlength="251">

     

          <div class="col-sm-12">

          

          <div class="form-group">


    <div class="col-sm-12 inputbox nopadding">

      <textarea class="ckeditor" name="student_email_message" id="student_email_message_<?php echo $info->id;?>"> 

		<?php
        $tex1 = preg_replace('/\b(name of parent)\b/u', $info->student_father_name, $payment_receive_email->student_email_message);

        //$text2 = preg_replace('/\b(name of branch)\b/u', $branchName->branch_name, $tex1);

        $text3 = preg_replace('/\b(name of child)\b/u', $info->student_fname.' '. $info->student_lname, $tex1);

        //$text4 = preg_replace('/\b(date of enrolment)\b/u', $student_enrolment_date, $text3);

        //$text5 = preg_replace('/\b(branch contact number)\b/u', $schoolContactNo->school_phone, $text4);

        $text6 = str_replace('(', '', $text3);

        $Finaltext = str_replace(')', '', $text6);

		echo   $Finaltext; 
		  

        ?>

      </textarea>

      <script>

       CKEDITOR.replace( 'student_email_message_payment<?php echo $info->id;?>' );

     </script>

    </div>

  </div>

  

  <div class="col-sm-12 nopadding">

        <div class="confirmlink">

        <div id="Loadingenrolemsg-<?php echo $info->id;?>"></div>

        <input class="sendenrolementemail" id="save_enrole-<?php echo $info->id;?>" value="Send" type="submit">

        </div>

        </div>

          

             </div>  

          </form>

        </div>

        <!--<div class="modal-footer">

        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->

        </div>

      </div>

      

    </div>
  
       
<!----------------------Popup Payment Receive Email Section End-------------------------------------------->           
        
        
        
        <div class="col-sm-4 rightspace fullwidsec document">
        
        
        

        <div class="profilecompletion">
        
        <div class="col-sm-12 prfltitlediv  certificate nopadding">

        <div class="col-sm-12 col-xs-10 prfltitle profile_titlebg"> <h1>Documents</h1> </div>
        
        <div style="clear:both"></div>

         <div class="profilecompleted profile-bg documenttable document">
       
 <form name="certificatesform" id="certificatesform" method="post" action="<?php echo base_url(); ?>students/edit_certificates/<?php echo $info->id; ?>" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">     
  
  <div class="form-group">
    <div class="col-sm-12"><label class="control-label nopadding">Upload files (Multiple)<br/><span class="tagline"></span></label></div>
    <div class="col-sm-12">
    <div class="col-sm-7 nopadding profile-picture inputbox">
        <input type="hidden" name="scertificates" id="scertificates" value="<?php echo $info->student_certificates;?>" />
        <input type="hidden" name="postedstudentid" id="postedstudentid" value="<?php echo $info->student_id;?>" />
        <input type="file"  class="browsebtn" name="student_certificates[]" id="uploadcertificates" multiple style="display:block;" />
       <!-- <i class="fa fa-picture-o" id="mycertificates"></i>-->
    </div>
 
  

        <div class="col-sm-5 nopadding confirmlink">
        
        <input type="submit" class="editcertificatebtn" value="Upload">
        </div>
     </div>
   </div>
  </form>
  
  
  <?php 
		$crterrorMsg = '';
		 if(trim($info->student_certificates)!='') {
			
			 $certificates =  explode(',', $info->student_certificates);
			// echo count($certificates);
			  $c = 0;
			 foreach($certificates as $certificate) {
				 $c++;
				  ?>
                <!-- <a href="<?php echo base_url();?>uploads/student_certificates/<?php echo $certificate;?>" target="_blank"><?php echo $certificate;?></a><br />-->
       
       <?php if($certificates!='1495024304') { ?>
               
                <div id="delcertificate_<?php echo $c;?>" class="certificate"> 
   		<div class="col-sm-8 nopadding" >
        <h4>
        <span id="delcertificateflash_<?php echo $c;?>"></span> 
        	 <?php 
			 echo substr($certificate, 0, 10);
			 if(strlen($certificate) > 10 ) {
				 echo '...';
			 }
			 
			//  $string =$certificate;
        		//$string1 = (strlen($string) > 10) ? substr($string,0,15).'...' : $string;  
      			//   echo $string1;
			?>
          </h4>
        </div>
        <div class="col-sm-3 nopadding">
        <h4>
        <span class="downloadspan">
        
        	<a href="<?php echo base_url();?>/students/download_certificates/<?php echo $certificate;?>" class="downloadcertificate" id="<?php echo $c.','.$certificate;?>" 																								 			data-toggle="tooltip" title="Download">
            	<i class="fa fa-download"></i></a>
        </span>
        <span><a href="javascript:void(0);" class="deletecertificate" id="<?php echo $c.','.$certificate;?>" data-toggle="tooltip" title="Delete">
        	<i class="fa fa-trash-o"></i></a>
         </span>
         </h4> 
        </div>
   </div>
              
              <?php } ?>
              
			<?php }
		   } else {
		      $crterrorMsg =  'No Document(s) Attached.';
			  }

		 ?>
  
         <div id="certificateError"> <?php if($crterrorMsg!='') { echo $crterrorMsg; } ?></div>
         
           </div>

        </div>

        

        </div>

        </div>

        </div>

 <script type="text/javascript">
	function cancelupdate(id){
		window.location.href ="<?php echo base_url();?>students/edit/"+id;
	}
</script>
 <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>    
        
 <script type="text/javascript">
  
  jQuery(document).ready(function(){
	  var fill_date = jQuery('#student_leaving_date').val();
	  if(fill_date!=''){
	   $("#request_payment").show();
	   $("#payment_receive").show();
	  }
	  else{
	      $("#student_leaving_date").focus(function(){
	  $("#request_payment").show();
		$("#payment_receive").show();
   });
   $("#request_payment").hide();
		$("#payment_receive").hide();
	  }

	   $("#student_dob").keydown(false);
	    $("#student_application_date").keydown(false);
		 $("#student_enrolment_date").keydown(false);
		  $("#student_leaving_date").keydown(false);
	  
	  	jQuery('body').click(function(){
			jQuery('.alert-dismissable').fadeOut('slow');
		})
		
  jQuery("#file").change(function() {
	jQuery(".upload-pimg-msg").empty(); 
	var file = this.files[0];
	var imagefile = file.type;
	var imageTypes= ["image/jpeg","image/png","image/jpg"];
		if(imageTypes.indexOf(imagefile) == -1)
		{
			jQuery(".upload-pimg-msg").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.onload = function(e){
				jQuery(".pimg-preview").html('<img src="' + e.target.result + '" width="120px" height="100px" />');				
			};
			reader.readAsDataURL(this.files[0]);
		}
	});	
	  
	  jQuery('#changeprofilepic').on('click', function() {
             jQuery('#file').click();
       });
	   
	  jQuery('.deleteprofilepic').on('click', function() {
            var avtarName =  jQuery(this).attr('id');
			
			 var avtarUid = jQuery('#avtaruserid').val();
			 
			 if(avtarName!='' && avtarUid!='') {
			 jQuery.ajax({
         		          type : 'POST',
                          url  : '<?php echo base_url(); ?>students/deleteAvtar',
                          data:{ 'avtarName': avtarName,'avtarUid': avtarUid },
                          success :  function(resp) {
							  if(resp==1) {
					            jQuery('#dynamicavtar').html('<img src="<?php echo base_url();?>assets/images/avtar.png">');
								jQuery('.deleteprofilepic').attr('id','');
							  } 
			            }
				   });
			 } else {
				// return false;
				jQuery("#file").val(null);
				jQuery('#dynamicavtar').html('<img src="<?php echo base_url();?>assets/images/avtar.png">');
			   }
			 
       }); 
	   
	   
	    jQuery('#mycertificates').on('click', function() {
             jQuery('#uploadcertificates').click();
       });
	  
	  jQuery(".deletecertificate").click(function() {
		  
		  if( confirm("Are you sure want to Delete?") ){
			  
   			     var certificateValue = jQuery(this).attr('id');
				 
				 var certificateValueArr = certificateValue.split(',');
				 
				 var deldivid = certificateValueArr[0];
				 
				 var crtV = certificateValueArr[1];
				
				// jQuery('#delcertificate_'+deldivid).remove();
				 
				    var hdncrtvalue =  jQuery('#scertificates').val(); 
					
					
				    var hdncrtvalueArr = hdncrtvalue.split(',');
					var NewValuescrt = removeImg(hdncrtvalueArr, crtV);
	                jQuery("#scertificates").val(NewValuescrt);	
					
					var postedstudentid =  jQuery('#postedstudentid').val(); 
					
jQuery('#delcertificateflash_'+deldivid).html('<img src="<?php echo base_url(); ?>assets/images/ajax-loader.gif">');	
					
					 jQuery.ajax({
         		          type : 'POST',
                          url  : '<?php echo base_url(); ?>students/deleteCertificates',
                          data:{ 'NewValuescrt': NewValuescrt,'postedstudentid': postedstudentid,'crtV': crtV },
                          success :  function(resp) {
							  if(resp==1) {
							        jQuery('#delcertificate_'+deldivid).remove();
							       } else {
									  jQuery('#delcertificate_'+deldivid).remove();
									  jQuery('#certificateError').html('No certificates');
							       }
			                  }
				        });
						
					}else{
			     return false;
		             }	
	       });
		
function removeImg(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
 }
		
		
		jQuery("#email").focusout(function(){
   			     var stuEmail = jQuery(this).val();
				 if(stuEmail!='') {
			           var Favalue = jQuery('#student_address').val();
					 jQuery("#student_sibling").find('option[value!=""]').remove();
					 jQuery.ajax({
         		          type : 'POST',
                          url  : '<?php echo base_url(); ?>students/check_sibling',
                          data:{ 'Favalue': Favalue, 'stuEmail': stuEmail },
                          success :  function(resp) {
					     jQuery('#student_sibling').append(resp);
			           }
				   });
				   
				 } else {
				   return false;
				  }
			});
			
			
	 	 jQuery("#student_address").focusout(function(){
   			     var Favalue = jQuery(this).val();
				 if(Favalue!='') {
			         jQuery('#student_family_address').val(Favalue);
					 var stuEmail = jQuery('#email').val();
					 jQuery("#student_sibling").find('option[value!=""]').remove();
					 jQuery.ajax({
         		          type : 'POST',
                          url  : '<?php echo base_url(); ?>students/check_sibling',
                          data:{ 'Favalue': Favalue, 'stuEmail': stuEmail  },
                          success :  function(resp) {
					     jQuery('#student_sibling').append(resp);
			           }
				   });
				   
				 } else {
				   return false;
				  }
			});
			
	 jQuery("#student_family_address").focusout(function(){
   		 var Favalue = jQuery(this).val();
		  if(Favalue!='') {
			  var stuEmail = jQuery('#email').val();
			  jQuery("#student_sibling").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>students/check_sibling',
            data:{ 'Favalue': Favalue, 'stuEmail': stuEmail  },
            success :  function(resp) {
					 jQuery('#student_sibling').append(resp);
			          }
				   });
				 } else {
				 return false;
				 }
				 
			});
			
		
	   jQuery(".student_school_branch").click(function() {
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			  jQuery("#student_class_group").find('option[value!=""]').remove();
			  jQuery("#fee-band").html('');
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>students/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					 jQuery('#student_class_group').append(resp);
			          }
				   });
				 } else {
				 return false;
				 }
				 
			});	
		
		
	 jQuery("#student_class_group").on('change', function(e){ 
   		  var schoolclass = jQuery(this).val();
		  if(schoolclass!='') {
			 var BranchID =  jQuery('input[name=student_school_branch]:checked').val();
			 // jQuery("#student_class_group").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>students/check_feeband',
            data:{ 'schoolclass': schoolclass,'BranchID': BranchID },
            success :  function(resp) {
					    jQuery('#fee-band').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
				 
			});	
				
	  
	   });
</script>

<script type="text/javascript">
  
  jQuery().ready(function() {

	jQuery.validator.addMethod("alphaUname", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    });
	
	jQuery.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaLnumber", function(value, element) {
      return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaUpwd", function(value, element) {
    return this.optional(element) || value == value.match(/^(?=.*[a-zA-Z0-9])[a-zA-Z0-9!@#$%&*.]{7,}$/);
    });

    // validate form on keyup and submit
    var v = jQuery("#basicform").validate({

      rules: {
        username: {
          required: true,
		  alphaUname: true,
          minlength: 4,
          maxlength: 40
		  /*remote: {
             url: "<?php //echo base_url(); ?>students/check_username",
             type: "POST"
             }*/
        },
        password: {
          required: true,
		  alphaUpwd: true,
          minlength: 6,
          maxlength: 15
        },
		email: {
          required: true,
		  email: true,
		  maxlength: 120
        },
       student_fname: {
          required: true,
		  alphaLetter: true,
		  maxlength: 60
        },
		student_lname: {
          required: true,
		  alphaLetter: true,
		  maxlength: 60
        },
		student_dob: {
          required: true
        },
		student_gender: {
          required: true
        },
		student_first_language: {
		 /* required: true,*/	
          alphaLetter: true,
		  maxlength: 60
        },
		student_other_language: {
          alphaLetter: true,
		  maxlength: 60
        },
		student_telephone: {
		  required: true,	
          number: true,
		  minlength: 11,
          maxlength: 11
        },
		student_address: {
		   required: true,
           maxlength: 250
        },
		student_address_1: {
           maxlength: 250
        },
		student_father_name: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_father_occupation: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_father_mobile: {
         /* required: true,*/	
          number: true,
		  minlength: 10,
          maxlength: 14
        },
		student_father_email: {
		  email: true,
		  maxlength: 120
        },
		student_mother_name: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_mother_occupation: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_mother_mobile: {
         /* required: true,*/	
          number: true,
		  minlength: 11,
          maxlength: 11
        },
		student_mother_email: {
		  email: true,
		  maxlength: 120
        },
		student_emergency_name: {
		  /*required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_emergency_relationship: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_emergency_mobile: {
         /* required: true,*/	
          number: true,
		  minlength: 10,
          maxlength: 14
        },
		student_doctor_name: {
		  alphaLetter: true,
		  maxlength: 60
        },
		student_doctor_mobile: {
		  number: true,
		  minlength: 11,
          maxlength: 11
        },
		student_helth_notes: {
		  maxlength: 250
        },
		student_allergies: {
		  maxlength: 250
        },
		student_other_comments: {
		  maxlength: 250
        },
		
		student_school_branch: {
          required: true
        },
		student_class_group: {
        },
		student_application_date: {
        },
		student_fee_band: {
          required: true
        }
      },
     
	  errorElement: "span",
      errorClass: "help-inline-error",
	  
	  messages: {
                 username: {
                  required: "This field is required.",
				  alphaUname: "Letters only please.",
				  minlength: "Minimum 4 characters required.",
				  maxlength: "Maximum 40 characters allowed.",
				 // remote: "User name already in use."
                 },
			    password: {
                  required: "This field is required.",
				  alphaUpwd: "Letters, numbers and special characters allowed",
				  minlength: "Minimum 6 characters required.",
				  maxlength: "Maximum 15 characters allowed."
                 },
				email: {
                    required: "This field is required.",
					email: "Please enter a valid email address.",
					maxlength: "Maximum 120 characters allowed."
                   },
				student_fname: {
                  required: "This field is required.",
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
                 },
				student_lname: {
                  required: "This field is required.",
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
                 },
				student_dob: {
                  required: "This field is required."
                 },
			    student_gender: {
                  required: "This field is required."
                 },
				 student_first_language: {
				 /* required: "This field is required.",	*/
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
				},
				student_other_language: {
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
				},
				student_telephone: {
				  required: "This field is required.",
				  number: "Numbers only please.",
				  minlength: "Minimum 11 characters required.",
				  maxlength: "Maximum 11 characters allowed."
				},
				student_address: {
				   required: "This field is required.",
				   maxlength: "Maximum 250 characters allowed."
				},
				student_address_1: {
				   maxlength: "Maximum 250 characters allowed."
				},
				student_father_name: {
				/*  required: "This field is required.",*/
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
				},
				student_father_occupation: {
				  /*required: "This field is required.",*/
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
				},
				student_father_mobile: {
				 /* required: "This field is required.",*/
				  number: "Numbers only please.",
				  minlength: "Minimum 10 characters required.",
				  maxlength: "Maximum 14 characters allowed."
				},
				student_father_email: {
				    email: "Please enter a valid email address.",
					maxlength: "Maximum 120 characters allowed."
				},
				student_mother_name: {
				 /*  required: "This field is required.",*/
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_mother_occupation: {
				  /* required: "This field is required.",*/
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_mother_mobile: {
				 /* required: "This field is required.",*/
				  number: "Numbers only please.",
				  minlength: "Minimum 11 characters required.",
				  maxlength: "Maximum 14 characters allowed."
				},
				student_mother_email: {
				    email: "Please enter a valid email address.",
					maxlength: "Maximum 120 characters allowed."
				},
				student_emergency_name: {
				 /*  required: "This field is required.",*/
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_emergency_relationship: {
				  /* required: "This field is required.",*/
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_emergency_mobile: {
				/*  required: "This field is required.",*/
				  number: "Numbers only please.",
				  minlength: "Minimum 10 characters required.",
				  maxlength: "Maximum 14 characters allowed."
				},
				student_doctor_name: {
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_doctor_mobile: {
				  number: "Numbers only please.",
				  minlength: "Minimum 10 characters required.",
				  maxlength: "Maximum 14 characters allowed."
				},
				student_helth_notes: {
				  maxlength: "Maximum 250 characters allowed."
				},
				student_allergies: {
				 maxlength: "Maximum 250 characters allowed."
				},
				student_other_comments: {
				  maxlength: "Maximum 250 characters allowed."
				},
				 student_school_branch: {
                  required: "This field is required."
                 },
				 student_class_group: {
                 },
				 student_application_date: {
                 },
				 student_fee_band: {
         		   required: "This field is required."
      			  }
            },
			
	focusInvalid: false,
    invalidHandler: function(form, validator) {
        if (!validator.numberOfInvalids())
            return;
        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top
        }, 1000);
      },		
		
		/*submitHandler: function(form) {
            form.submit();
          }*/
    });

	 $(".open6").click(function() {
      if (v.form()) {
		  $( "#basicform" ).submit();
		 /*$("#loader").show();
         setTimeout(function(){
           $("#basicform").html('<h2>Thanks for your time.</h2>');
         }, 1000);*/
        return false;
      }
    });

  });
</script>
<style>
#request_payment, #payment_receive {
    background: #36b047 none repeat scroll 0 0;
    border-radius: 5px;
    color: #fff;
    padding: 15px 20px;
}
#student_leaving_date {
    margin-bottom: 20px;
}
.datepickerdiv {
    text-align: right;
}


.certificate{
  border-bottom: 1px solid #ccc;
  padding-top: 0px;
  font-size:14px;
  padding-bottom:0px;

}

.profile-picture.inputbox {
    padding: 15px 0 0 !important;
}

.document{
	padding:0px;
	}
.prfltitlediv.nopadding {
    border: 1px solid #ecedf0;
    border-radius: 3px;
}
.documenttable{
	margin-bottom:0px !important;
}

*::after, *::before {
    box-sizing: border-box;
}
*::after, *::before {
    box-sizing: border-box;
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    cursor: pointer;
}
#cancelbtn {
    background:rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0 !important;
	color:#FFF !important;
}

.fa-picture-o { cursor:pointer; }
.fa-trash-o { cursor:pointer; }
.prfltitlediv.certificate {
    background: rgba(0, 0, 0, 0) linear-gradient(#ffffff, #f3f5f8) repeat scroll 0 0;
    border: 1px solid #ecedf0;
    }
.prfltitle {padding: 0px;}
.profilecompleted.profile-bg {padding: 10px;}
.profilecompletion{
	 background: rgba(0, 0, 0, 0) linear-gradient(#ffffff, #f3f5f8) repeat scroll 0 0 !important; 
	}
#certificatesform .confirmlink {float: left !important;}
.confirmlink .editcertificatebtn {float: right;margin: 10px 0;}
.profilecompleted.profile-bg > div {
    /*border:1px solid #ccc;
    border-bottom:none;*/
    float: left;
    padding-left: 15px;
    padding-right: 15px;
    width: 100%;
}
.deletecertificate {float: right;}
.deletecertificate + .tooltip > .tooltip-inner {background-color: #090 !important;}
.deletecertificate + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.fa-picture-o + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-picture-o + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

.downloadspan {
    padding: 0 25px;
}
.downloadcertificate + .tooltip > .tooltip-inner {background-color: #090 !important;}
.downloadcertificate + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.fa.fa-download + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa.fa-download + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

.deletecertificate + .tooltip > .tooltip-inner {background-color: #090 !important;}
.deletecertificate + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.fa-trash-o + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-trash-o+ .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.profilecompleted.profile-bg div h4 {
   
    color: #555;
    font-size: 14px;
    padding-bottom: 10px;
}
.document{
	padding:0px !important;
}

.profile_titlebg h1{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1);  padding: 10px 15px; }
.profiletitltbox{padding-top:0px;}
.profiletitltbox .profile_titlebg {
    padding: 0;
}
.contentdiv{
	padding-left:15px;
	margin-top:15px;
}
.profile-bg {
    border: medium none;
	padding: 0px;
}
.accsection h1 {
    padding: 0 0 10px 15px;
}
/********************************modelcss***************************************/
#myModal { margin-top:140px; } 



.save_btn > input {

    background: #37b248 none repeat scroll 0 0;

    border: 1px solid #ccc;

    border-radius: 3px;

    color: #fff;

    font-size: 14px;

    padding: 7px 20px;

}

.save_btn {

    padding-top: 20px;

}

.popup_save_btn {

    background: #37b248 none repeat scroll 0 0;

    border: medium none;

    border-bottom-right-radius: 3px;

    border-top-right-radius: 3px;

    color: #fff;

    padding: 12px 25px;

}

.text_box_sec > input {

    border: 1px solid #ccc;

    border-radius: 3px;

    min-height: 45px;

    width: 100%;

	padding: 10px;

}

.btn.btn-default {

    background: #37b248 none repeat scroll 0 0;

    color: #fff;

}

.text_box_sec > input {

    border: 1px solid #ccc;

    border-bottom-left-radius: 3px;

    border-top-left-radius: 3px;

    min-height: 44px;

    padding: 10px;

    width: 100%;

}

.save_btn {

    text-align: right;

}

.text_box_btn {

    text-align: left;

}
.red_line { color:#F00 !important; }

.yellow_line { color:#990 !important; }

.blank_trans_data {

    color: transparent;

}

.search {

    padding: 10px 0 60px !important;

}

.invitemember {

    background: #37b248 none repeat scroll 0 0;

    color: #fff;

    padding-bottom: 5px;

    padding-top: 5px;

}
.close.closenotify {
    background: #fff none repeat scroll 0 0;
    border: 2px solid #37b248;
    border-radius: 40px;
    color: #36b047;
    float: right;
    font-size: 28px;
    height: 40px;
    left: 30px;
    margin-top: -23px;
    opacity: 1;
    position: relative;
    width: 40px;
}
.modal-content.enrolmentpopup{float:left;width:100%;}
.confirmlink .sendenrolementemail {
    border: 0 none;
    font-weight: bold;
    padding: 10px 30px;
    text-transform: uppercase;
}
.enrolmentpopup .modal-body{float:left;width:100%;}
.savepostponedate {
    background: #37b248 none repeat scroll 0 0;
    border: 0 none;
    color: #fff;
    padding: 12px 30px;
}
.modal-dialog.popupdiv{width:90%;}
.cke_contents.cke_reset {
    height: 350px !important;
}
.enrolmentpopup .modal-body {
    float: left;
    padding: 5px 0 0;
    width: 100%;
}
.confirmlink input{margin:6px 0px;}
.popup_section_form {
    padding: 30px 10px;
}
#fee-band {
    padding: 20px !important;
}
/******************************modelcssend***************************************/

</style>
 <script>
  jQuery(document).ready(function(){
    jQuery("#certificatesform").validate({
        ignore: [],
	    rules: {
                'student_certificates[]': {
    							 required: true
   								 },
        },
			
        messages: {
                'student_certificates[]': {
    							  required: "Please select files",
   								   },
            },
       
        submitHandler: function(form) {
            form.submit();
          }

       });
	});
</script>
<!--<script>
	jQuery(document).ready(function(){
		$('body').click(function(){
			$('.alert-dismissable').fadeOut('slow');
		})
		
  jQuery("#file").change(function() {
	jQuery(".upload-pimg-msg").empty(); 
	var file = this.files[0];
	var imagefile = file.type;
	var imageTypes= ["image/jpeg","image/png","image/jpg"];
		if(imageTypes.indexOf(imagefile) == -1)
		{
			jQuery(".upload-pimg-msg").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.onload = function(e){
				jQuery(".pimg-preview").html('<img src="' + e.target.result + '" width="120px" height="100px" />');				
			};
			reader.readAsDataURL(this.files[0]);
		}
	});	
		
	})
</script>-->



       