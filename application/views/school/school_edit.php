   <?php 
	    $schoolid = $info->school_id;
		$this->load->model(array('main_schools_model'));
		$schoolInfo = $this->main_schools_model->getSchoolinfo($schoolid);
	  ?>
<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">
   

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>


        <li class="edit"><a href="#">Edit School</a></li>        

        </ul>

        </div>

        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>

        </div>

          <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div> 	
        

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-8 col-xs-7 nopadding">

        <h1>School Details</h1>

        </div>

        </div>

        <div class="col-sm-8 leftspace fullwidsec">
        
        
        <div class="editprofileform accdetailinfo">
		
  <form action="<?php echo base_url(); ?>schools/updateschool/<?php echo $info->school_id; ?>" method="post" name="editschoolinfoForm" id="editschoolinfoForm" enctype="multipart/form-data">
       
        <div class="profile-bg">

        <h2>School details</h2>
        
        
          
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Username<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="username" id="username" value="<?php echo $schoolInfo->username; ?>"  placeholder="" required="required" autocomplete="off" maxlength="41" disabled="disabled">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Password<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="password" class="form-control" name="password" id="password" value="<?php echo $schoolInfo->password; ?>" placeholder="" required="required" autocomplete="off" maxlength="16">
    </div>
  </div>
  
  


  		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">School Name<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="text" placeholder="" name="schoolname" id="schoolname" class="form-control" value="<?php echo $info->school_name;?>">

    </div>

  </div>

  		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">School Email<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="text" placeholder="" name="email" id="email" class="form-control"  value="<?php echo $info->school_email ; ?>">

    </div>

  </div>

  		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">Telephon<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="text" placeholder="" name="telephone" id="telephone" class="form-control" value="<?php echo $info->school_phone; ?>">

    </div>

  </div>

  		

 		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">Address<span>*</span> </label>

    <div class="col-sm-9 inputbox">

      <input type="text" placeholder="" name="address" id="address" class="form-control" value="<?php echo $info->school_address;?>">

    </div>

  </div>

  		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">Contact person</label>

    <div class="col-sm-9 inputbox">

 <input type="text" placeholder="" name="contact_person" id="contact_person" class="form-control" value="<?php echo $info->school_contact_person;?>">

    </div>

  </div>

  		</div>

<div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-12 nopadding">

        <div class="confirmlink">

         <input type="submit" value="Update" name="updateschoolBtn" id="updateschoolBtn" class="btn btn-default">

        </div>

        </div>

        </div>

        </form>

         </div>

        </div>

        
        
        
        
        
        
        <div class="col-sm-4 rightspace fullwidsec">

        </div>

        </div>
        
<script>
  jQuery(document).ready(function(){
	  
  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");

jQuery.validator.addMethod("alphaUname", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    });
	
	jQuery.validator.addMethod("alphaUpwd", function(value, element) {
    return this.optional(element) || value == value.match(/^(?=.*[a-zA-Z0-9])[a-zA-Z0-9!@#$%&*.]{7,}$/);
    });
	
    jQuery("#editschoolinfoForm").validate({
        rules: {
			
				username: {
				  required: true,
				  alphaUname: true,
				  minlength: 4,
				  maxlength: 40
				},
				
				password: {
				  required: true,
				  alphaUpwd: true,
				  minlength: 6,
				  maxlength: 15
				},
				
                schoolname: {
                required: true,
				lettersonly: true,
				maxlength: 120
               },
			   email: {
                  required: true,
                  email: true,
				  maxlength: 120
               },
			   telephone: {
                  required: true,
				  number:true,
                  minlength: 10,
				  maxlength: 14
               },
			   
			   address: {
                required: true
               }
        },
        messages: {
			
			   username: {
                  required: "This field is required.",
				  alphaUname: "Letters only please.",
				  minlength: "Minimum 4 characters required.",
				  maxlength: "Maximum 40 characters allowed."
                 },
			    password: {
                  required: "This field is required.",
				  alphaUpwd: "Letters, numbers and special characters allowed",
				  minlength: "Minimum 6 characters required.",
				  maxlength: "Maximum 15 characters allowed."
                 },
				 
                schoolname: {
                  required: "Please Enter Schoolname",
				  lettersonly: "Please enter only character value",
				  maxlength: "Maximum 120 characters allowed."
                 },
				  email: {
                  required: "Please Enter Email",
				  email: "Please enter a valid email address",
				  maxlength: "Maximum 120 characters allowed."
                 },
				  telephone: {
				  required: "Please Enter Telephone",
                  number: "Please Enter Number",
				  minlength: "Minimum 10 characters required.",
				  maxlength: "Maximum 14 characters allowed."
                 },
				  address: {
                  required: "Please Enter Address"
                 }
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
	});
</script>
        
        
<script type="text/javascript"> 
      $(document).ready( function() {
		$('body').click(function(){
			$('.alert-dismissable').fadeOut('slow');
		})
	  });
    </script>   

