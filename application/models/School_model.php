<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class School_model extends CI_Model {
	
	var $tbl	= "school";
	var $users = "users";
	
    function __construct() {
        parent::__construct();
    }
	   
	   function insertSchool($data){
		if($this->db->insert($this->tbl, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
		function get_records($sid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.school_id = '.$this->tbl.'.school_id','INNER'); 
		  $this->db->where(array($this->users.'.id'=>$sid));
          $query = $this->db->get();
		//  echo $this->db->last_query();exit;
          return  $query->result();
        }
		
	  function udateSchool($schooldata,$whereschool) {
		$this->db->where($whereschool);
		if($this->db->update($this->tbl, $schooldata))
			return true;
		  else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	  function updateUser($userdata,$whereuser) {
		$this->db->where($whereuser);
		if($this->db->update($this->users, $userdata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 
	   function check_edited_schoolname($school_id,$schoolname) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_name"=>$schoolname));
		$this->db->where('school_id !=', $school_id);
		  $query = $this->db->get();
		  $this->db->last_query();
          return $query->result();   
	}
	
	/* function getSchoolinfo($schoolid) {
          $this->db->select('*');
          $this->db->from($this->users);
		  $this->db->where('school_id', $schoolid);
          $query = $this->db->get();
          return  $query->row();
    }*/
	 
}



 
