    <div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">
    
    <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

        <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="<?php echo base_url();?>Teacherdashboard/attendance">Teacher Dashboard</a></li>        

        </ul>

        </div>

        <!--div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="quickaction" value="Quick actions">
        

        </div-->

        </div>
        
    <div class="col-sm-12 leftspace attendancesec">

        
        <div class="col-sm-3 nopadding">

		<div class="attendance-chart">
           <?php
		if($ids){
		
		$this->load->model(array('Teacherdashboard_model'));

        $class_id=explode(",",$ids);
		
		$user_id=$this->session->userdata('user_id');
		$i = 0;
		foreach($class_id as $classid)
		
		{
			$cid=$classid;
			$class= $this->Teacherdashboard_model->get_classname($cid);
			$class_name = $class->class_name;
			$i = $i + 1;
		?>
   
        <div class="col-sm-12 todayattendancediv dashboard-attendance">
        <div class="col-sm-12 nopadding attendanceicon">
        </div>
        <div class="col-sm-12 nopadding titlediv">
      
        <div class="col-sm-12 stuatten">
        <h1>Class:<?php echo $class_name; ?><br/><span></span></h1>
        </div>
        </div>
        
        <div class="col-sm-12 nopadding">
        
        <div id="chartContainer<?php echo $i; ?>" class="chartdiv" style="width: 150px;height:200px; margin:auto;"></div>
        <div class="attendance-status">
        <div class="present statusdiv">
        <span><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/greenicon.png">Present</span>
        </div>
        <div class="present statusdiv">
        <span><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/blueicon.png">Absent</span>
        </div>
        <div class="present statusdiv">
        <span><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/yellowicon.png">Late</span>
        </div>
        </div>
        </div>
        </div>
        
    <?php } }?>
 
  
</div>
        
        </div>
       

       
    

    <div class="col-sm-9 rightspace fullwidsec">

        <div class="col-sm-12 tablediv dashboardtablediv nopadding">

    <div class="col-sm-12 dashboardtit nopadding">

    <div class="col-sm-7 col-xs-6 dashboardleft nopadding">

    <h1>Notifications<span></span></h1>

    </div>
    	<div class="col-sm-4 col-xs-5 dashboardleft nopadding" style="float:right">
    
       
        <a href="<?php echo base_url()?>emailnotifications" class="admin_msg"><input type="button" class="btn btn-success sndmsg" value="Send Message" style="padding: 4px 20px;"></a>

       </div>

        
    </div>

    <div class="tablewrapper">

    <table class="table-bordered table-striped">

<thead>

				  <tr class="headings">

					  <th class="column3">Sender</th>

                      <th class="column9">Sender Name & Message</th>
                   
					  <th class="column5">Date</th>

					  <th class="column3">Actions</th>

				  </tr>

			  </thead>

				<tbody>
<?php
if($show){
        foreach ( $show as $notification )
        {
			
            ?>
					<tr class="familydata communication">

						<td class="column3">
                        <div class="imgsec">
						<?php 
						$sender_id = $notification->from;
						//$user_profile_img=$notification->from;
						$this->load->model(array('teacherdashboard_model'));
						$senderName = $this->Teacherdashboard_model->getsenderName($sender_id);
						$senderimage = $this->Teacherdashboard_model->getsenderimage($sender_id);
						//echo $senderName->username;
						$img_name=$senderimage->profile_image;
					?>
					  <!--<img src="<?php //echo base_url().'application/uploads/staff/'.$img_name;?>" alt="no image">-->
                   <?php   if( file_exists( $img_name ) ) {?>
                              <img src="<?php echo base_url().'application/uploads/staff/'.$img_name;?>" alt="no image">
    <?php   }else {?>
		              
		            <img src="<?php echo base_url();?>assets/images/avtar.png">
	<?php }	?>
					  					
                        </div>
                        </td>
                        <td class="column9">
                        <div class="contentsec">
                       <!-- <td class="column3"><?php 
											//$string = $notification->subject;
											//$string1 = (strlen($string) > 5) ? substr($string,0,10).'...' : $string;		
											//echo $string1;?></td>-->
                        
                        <p><?php 
						$sender_id = $notification->from;
						$this->load->model(array('teacherdashboard_model'));
						$senderName = $this->Teacherdashboard_model->getsenderName($sender_id);
						echo $senderName->username; 
						?></p>
                        <span><?php echo substr($notification->message, 0,60); if(strlen($notification->message) > 60 ) { echo '...'; } ?></span>
                        </div>
                        </td>

						<td class="column5"><?php $date= $notification->created_date;
												echo date('d F y',strtotime($date));
						?></td>
                        <td class="column3 spacing">
                        	 <div class="dropdown">
				    <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">Action
				    <span class="caret"></span></button>
                        			<ul class="dropdown-menu">
                        				<li><a onclick="delConfirm('<?php echo $notification->notification_id;?>')">Thank You</a></li>
                        				<li><a href="">Not Recruiting</a></li>
	                				<li><a href="">Reject</a></li>
		        				<li class="dropdown-submenu">
		        					<a class="subMenu">Send To <span class="caret"></span></a>
		        					<ul class="dropdown-menu">
		        						<li><a href="">Oak</a></li>
		        						<li><a href="">TVJ</a></li>
		        						<li><a href="">TVI</a></li>
		        						<li><a href="">SM</a></li>
		        					</ul>
		        				</li>
		        				<li class="dropdown-submenu">
		        					<a class="subMenu">Move To​ <span class="caret"></span></a>
		        					<ul class="dropdown-menu">
		        						<li><a href="">Application Bank</a></li>
		        						<li><a href="">Short listed Candidates</a></li>
		        					</ul>
		        				</li>
	                        		</ul>
                        	</div>
                       <!-- <a class="viewmessage"  id="viewmsg" data-value="<?php echo $notification->notification_id;?>"       										    		data-toggle="modal" data-target="#viewModal<?php echo $notification->notification_id;?>"><i class="fa fa-eye icnclass"></i></a>
                        
                        <a onclick="delConfirm('<?php echo $notification->notification_id;?>')" class="deletemsg"><i class="fa fa-trash-o icnclass"></i></a>
                        -->
                        
                        </td>

                        </tr>
                        <div id="viewModal<?php echo $notification->notification_id;?>" class="modal" style="z-index:99999999999">
        
          <!-- Modal content -->
          <div class="modal-content">
           			<div class="modalbg">
					 <div class="col-sm-12 nopadding" style=" background:#36b047 none repeat scroll 0 0;"><div class="col-sm-9 nopadding viewmodal"><h3>View Message</h3></div>	 						<div class="col-sm-3 btncls"><button type="button" class="close closenotify" data-dismiss="modal" aria-hidden="true">&times;</button></div></div>
                    	<div class="col-sm-12 msgbox">
                        	
                        	<div class="col-sm-12 nopadding">
                            <div class="col-sm-3 nopadding popup">
                              <label for="sender">From:</label>
                            </div>
                            <div class="col-sm-9 nopadding popup">
                            <?php $sender_id = $notification->from;
						$this->load->model(array('teacherdashboard_model'));
						$senderName = $this->Teacherdashboard_model->getsenderName($sender_id);?>
							<label> <?php echo $senderName->username; ?></label>
                             </div>
                             </div>
                             <div class="col-sm-12 nopadding">
                                 <div class="col-sm-3 nopadding popup">
                              <label for="subject">Subject:</label>
                            </div>
                            <div class="col-sm-9 nopadding popup">
                            	<label>
								<?php 
									 echo $notification->subject;?>
							
                                </label>
                             </div>
                             </div>
                             <div class="col-sm-12 nopadding">
                             <div class="col-sm-3 nopadding popup">
                        		<label for="message">Message:</label>
                              </div>
                              <div class="col-sm-9 nopadding popup"> 
                        		<label><?php echo $notification->message; ?></label>
                        	  </div>
                              </div>
                              <div class="col-sm-12 nopadding">
                                  <div class="col-sm-3 nopadding popup" >
                              <label for="attachment">Attachments:</label>
                            </div>
                           	<div class="col-sm-9 nopadding popup">
                            	
         <ul class="attachment_section">
         <?php 
		
		 if($notification->attachments!='') {
			
			 $attachments =  explode(',',$notification->attachments);
			// echo count($certificates);
			 
			 foreach($attachments as $attachment) { ?>
           <li>
                <?php //echo $attachment;
                  $string = $attachment;
                  $string1 = (strlen($string) > 5) ? substr($string,0,30).'...' : $string;  
                  echo $string1; 
	              ?>
                    <div class="icon_section">
                  <a href="<?php echo base_url();?>Teacherdashboard/download_attachments/<?php echo $attachment;?>"><i class="fa fa-download" data-toggle="tooltip" title="Download" data-placement="top"></i></a>
        
                  <a href="<?php echo base_url();?>uploads/notification/<?php echo $attachment;?>" target="_blank"><i class="fa fa-eye " data-toggle="tooltip" title="View" data-placement="top"></i></a>
                  </div>
                  
               </li> 
			<?php }
		   } else {
		      echo 'No attachments';
			  }
		 
		 ?>
         </ul>
       
                             </div>
                             </div>
							</div>
                   </div>    
          	
              </div>  
	</div>
                        
                        
<?php	}		
			}  else { ?>
 <tr><th colspan="7" style="text-align: center; width:1215px;height:70px;Font-size:22px; background:#FFF;">No Message to show.</th></tr>
				   <?php } ?>

				</tbody>

		  </table>   
 	<div class="col-sm-12 paginationdiv nopadding">
     <div class="col-sm-9 col-xs-6 paginationblk">
    <? 
							if($show){
								$sr= $last_page;
								
								$last_page1=$last_page;
								?>
							<span id="shortlist">	Showing <?=++$last_page;?> to <?=++$sr;?> of <?=$total_rows++;?> entries </span>
								<?
							}
							?>   
			
	<ul class="pagination">
		<?=$links;?>
	</ul>

    </div>
    </div>
</div>
	</div>


	<div class="profilecompletion">

        <div class="col-sm-12 prfltitlediv nopadding">

        <div class="col-sm-11 col-xs-10 prfltitle">

        <h1> Number of students in each class</h1>

        </div>

        <div class="col-sm-1 col-xs-2 settingicon">

        <!--<i class="fa fa-cog"></i>-->

        </div>

        </div>

        <div class="col-sm-12 processcompleted nopadding">

        <div class="loadingbars profile-bg">
        
                <div class="col-sm-8 border-right graph">
        
        

        <!--<img src="<?php echo base_url();?>assets/images/loadingbars.jpg" class="img-responsive">-->
        
        
        <div class="css_bar_graph">
   
   <!-- y_axis labels -->
   <ul class="y_axis">
   		<?php if($ids){
		
		$this->load->model(array('Teacherdashboard_model'));

        $class_id=explode(",",$ids);
		
		$user_id=$this->session->userdata('user_id');
		
		foreach($class_id as $classid)
		
		{
			$cid=$classid;
			$nostudents= $this->Teacherdashboard_model->classstudents($cid);
			
		?>
        <li><?php echo $nostudents;?></li>
        <?php } }?>
   </ul>
   
   <!-- x_axis labels -->
   <ul class="x_axis">

   <?php
		if($ids){
		
		$this->load->model(array('Teacherdashboard_model'));

        $class_id=explode(",",$ids);
		
		$user_id=$this->session->userdata('user_id');
		
		foreach($class_id as $classid)
		
		{
			$cid=$classid;
			$class= $this->Teacherdashboard_model->get_classname($cid);
			$class_name = $class->class_name;
			
		?>
       
        <li><?php echo  substr($class_name, 0, 3);?></li>
        <?php } }?>
   </ul>
   
   <!-- graph -->
   <div class="graph graphbar">
    <!-- grid -->
    <ul class="grid">
     <li><!-- 70 --></li>
     <li><!-- 60 --></li>
     <li><!-- 50 --></li>
     <li><!-- 40 --></li>
     <li><!-- 30 --></li>
     <li><!-- 20 --></li>
     <li><!-- 10 --></li>
     <!--<li class="bottom"><span>Classes</span></li>-->
     <li class="bottom"><span><img src="<?php echo base_url();?>assets/images/right_arrow.png"></span></li>
    </ul>
    
    <!-- bars -->
    <!-- 250px = 100% -->
    <ul class="graph_bars">
         <span class="greybar bar imgstud"><img src="<?php echo base_url();?>assets/images/TOP_arrow1600.png"></span>
    	   		<?php if($ids){
		
		$this->load->model(array('Teacherdashboard_model'));

        $class_id=explode(",",$ids);
		
		$user_id=$this->session->userdata('user_id');
		$i=0;
		foreach($class_id as $classid)
		
		{
			$cid=$classid;
			$nostudents= $this->Teacherdashboard_model->classstudents($cid);
			$i=$i+1;
		?>
        
         <span class="teacherdb greybar bar<?php echo $i; ?>"><li class="bar nr_<?php echo $i; ?>" style="height: <?php echo $nostudents*4;?>%;"><span><?php echo $nostudents;?></span></li></span>
        <?php } }?>

    </ul> 
    
   </div>
   
   <!-- graph label -->
   
  
  </div>
        

        </div>

        <div class="col-sm-4 nopadding">

        <div class="monthdate borderbottom" >

        <h1 id="weekstudent"></h1>

        <span>For This Week</span>

        </div>

        <div class="monthdate">

        <h1 id="monthstudent"></h1>

        <span>For This Month</span>

        </div>

        </div>

        </div>

        </div>

        </div>
	<!--<div class="wheteher" style="float:left; width:100%;">
     <a href="http://www.accuweather.com/en/in/chandigarh/202350/weather-forecast/202350" class="aw-widget-legal">
</a><div id="awcc1493170464695" class="aw-widget-current"  data-locationkey="" data-unit="c" data-language="en-us" data-useip="true" data-uid="awcc1493170464695" data-targeturl="http://www.royalenfieldhimachal.com/demo/olivetree/Teacherdashboard/"></div><script type="text/javascript" src="https://oap.accuweather.com/launch.js"></script>
     </div>-->
 </div>
      
	  <div class="col-sm-9 rightspace fullwidsec recentsec">	
    <div class="col-sm-12 nopadding container recentsec"> 
		
		
        <div class="toggle">
			<div class="toggle-title  ">
				<h3>
				<i></i>
				<span class="title-name">Recently Added Student</span>
				</h3>
			</div>
			
			<div class="toggle-inner toggle_tbl">
				<p>
                <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">No.</th>
					  <th class="column6">Name</th>
                      <!--<th class="column3">Class</th>-->
					  <th class="column7">Email</th>
                      <th class="column3">Contact</th>
                       <th class="column3 heading_view">Options</th>
                       
				  </tr>

			  </thead>

				<tbody>
<?php
if($recentstudent){
	$sr=1;
        foreach ( $recentstudent as $studentrecent )
        {
            ?>                
	 					<tr class="familydata staff">
					 
                   		 <td class="column1"><?php echo $sr++; ?></td>

						<td class="column6"><?php echo $studentrecent->student_fname.' '.$studentrecent->student_lname;?></td>
                        
<!--						<td class="column3">//<?php
//						 		$class = $studentrecent->student_class_group;
//        						$this->load->model(array('Teacherdashboard_model'));
//        						$className = $this->Teacherdashboard_model->getclassName($class);
//								echo @$className->class_name;?></td>-->

						<td class="column7"><?php 
								if(($studentrecent->email)!=""){
									echo $studentrecent->email;
								}else{
									echo '<span style="color:transparent;">-</span>';}
								?>
                           </td>

                        <td class="column3"><?php 
								if(($studentrecent->student_telephone)!=""){
									echo $studentrecent->student_telephone;
								}else{
									echo '<span style="color:transparent;">-</span>';}
								?>
                         </td>
         
                        <td class="column3 recent_view"><a href="<?php echo base_url();?>students/view_student/<?php echo $studentrecent->user_id;?>"> View  </a></td>
										<!--	<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
													<li>
													<a href=""> View  </a>
													</li>
												</ul>
											</div>
										-->
 </tr>
 <?php 	}	 } 	?>
	 		</tbody>

		  </table>
          

</div></p>
			</div>
			
			</div><!-- END OF TOGGLE -->    
 
		<div class="toggle">
			<div class="toggle-title  ">
				<h3>
				<i></i>
				<span class="title-name">Leaving Student</span>
				</h3>
			</div>
			
			<div class="toggle-inner toggle_tbl">
				<p>
  
                <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">No.</th>

					  <th class="column6">Name</th>
                      
              		 <!-- <th class="column3">Class</th>-->

                      <th class="column7">Email</th>

                      <th class="column3">Leaving Date</th>
                      
                       <th class="column3 heading_view">Options</th>
                       
                        <!--<th class="column1">Edit</th>
                        <th class="column1">View</th>-->

				  </tr>

			  </thead>

				<tbody>
 <?php
if($leavingstudent){
	$sr=1;
        foreach ($leavingstudent as $studentleaving )
        {
            ?>               
	 					<tr class="familydata staff">
 					 
                  		 <td class="column1"><?php echo $sr++; ?></td>

						<td class="column6"><?php echo $studentleaving->student_fname.' '.$studentleaving->student_lname;?></td>
                        
<!--						<td class="column2"><?php
//						 		$class = $studentleaving->student_class_group;
//        						$this->load->model(array('Teacherdashboard_model'));
//       						$className = $this->Teacherdashboard_model->leaving_getclassName($class);
//								echo @$className->class_name;?></td>-->

						<td class="column7"><?php if(($studentrecent->email)!=""){echo $studentrecent->email;}else{echo '<span style="color:transparent;">-</span>';}?></td>

                        <td class="column3"><?php echo $studentleaving->student_leaving_date;?></td>
         
                        <td class="column3 recent_view"><a href="<?php echo base_url();?>students/view_student/<?php echo $studentleaving->user_id;?>"> View  </a></td>
											<!--<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
												
													<li></li>
											     </ul>
											</div>-->
										
           </tr>
  <?php } } ?>    
  
      
	 		</tbody>

		  </table>
          

</div></p>
			</div>
			</div>
			</div>
  
		</div>
        
        </div>

   </div>
<script src="<?php echo base_url();?>assets/js/canvasjs.min.js"></script>

 <script>
  jQuery(document).ready(function(){
	  if( jQuery(".toggle .toggle-title").hasClass('active') ){
		jQuery(".toggle .toggle-title.active").closest('.toggle').find('.toggle-inner').show();
	}
	jQuery(".toggle .toggle-title").click(function(){
		if( jQuery(this).hasClass('active') ){
			jQuery(this).removeClass("active").closest('.toggle').find('.toggle-inner').slideUp(200);
		}
		else{	jQuery(this).addClass("active").closest('.toggle').find('.toggle-inner').slideDown(200);
		}
	});
	  
    jQuery("#notificationform").validate({
        rules: {
                subject: {
                   required: true,
				   maxlength: 250
               },
			   message: {
                   required: true,
     			   maxlength: 5000
               }
        },
        messages: {
              subject: {
                  required: "This field is required.",
				  maxlength: "Maximum 250 characters allowed."
                 },
			  message: {
                  required: "This field is required.",
				  maxlength: "Maximum 5000 characters allowed."
                 }
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		
	});
 </script>

 <script type="text/javascript">
window.onload = function () {

		<?php
				if($ids){
		
		        $class_id=explode(",",$ids);
				$user_id=$this->session->userdata('user_id');
				$this->load->model(array('Teacherdashboard_model'));
				$i = 0;
				foreach($class_id as $classid)
		
				{
				$i = $i + 1;
				
				$cid=$classid;
				
				$present=$this->Teacherdashboard_model->attendance_present($user_id,$cid);
				$absent=$this->Teacherdashboard_model->attendance_absent($user_id,$cid);
				$late=$this->Teacherdashboard_model->attendance_late($user_id,$cid);	
		?>
	
			var chart = new CanvasJS.Chart("chartContainer<?php echo $i; ?>", {
				
				animationEnabled: true,
				theme: "theme2",
				data: [
				{
					
					type: "doughnut",
					indexLabelFontFamily: "Garamond",
					indexLabelFontSize: 20,
					startAngle: 90,
					indexLabelFontColor: "dimgrey",
					indexLabelLineColor: "darkgrey",
					toolTipContent: "{legendText}: <strong>#percent% </strong>",
					

					dataPoints: [
					{ y:<?php if($present){echo $present;} else {echo '100';}?>, legendText: "Present" , color: "#57b94a" , },
					{ y: <?php  if($absent){echo $absent;}else{echo '0';} ?>, legendText: "Late" , color: "#3472b2"},
					{ y: <?php if($late){echo $late;}else {echo '0';} ?>, legendText: "Absent" , color: "#f5b73c" },

					]
				}
				]
			});
				
			chart.render();
			
	<?php } } ?>	
}

	</script>

 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>Teacherdashboard/deletemsg/"+id;
		}else{
			return false;
		}
	}
	
	
	// Validate "Other" textbox
var isOther = document.getElementById("All");
var other = document.getElementById("touser");
var admin = document.getElementById("Admin");
var staffcheck=document.getElementById("Staff");
var student=document.getElementById("Student");
isOther.addEventListener("click", function () {
    console.log("All Clicked!");
     if (isOther.checked) {
          other.readOnly = true;
		  other.value='';
		  admin.checked=true;
		  admin.disabled=true;
		  staffcheck.checked=true;
		  staffcheck.disabled=true;
		  student.checked=true;
		  student.disabled=true;
     } else {
          other.readOnly = false;
		  staffcheck.disabled=false;
		  staffcheck.checked=false;
		  student.disabled=false;
		  student.checked=false;
		  admin.disabled=false;
		  admin.checked=false;
     }
});
other.addEventListener("focus", function (evt) {
     // Checkbox must be checked before data can be entered into textbox
     if (All.checked) {
          this.readOnly = true;
     } else {
          this.readOnly = false;
     }
});
</script>
 
 
  <script>
$("#All").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
  

     
   
  
$(function() {
    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }
    
    $( "#touser" ).bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
            event.preventDefault();
        }
    })
    .autocomplete({
        minLength: 1,
        source: function( request, response ) {
            // delegate back to autocomplete, but extract the last term
            $.getJSON("<?php echo base_url();?>/teacherdashboard/searchemail", { term : extractLast( request.term )},response);
        },
        focus: function() {
            // prevent value inserted on focus
            return false;
        },
        select: function( event, ui ) {
            var terms = split( this.value );
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push( ui.item.value );
            // add placeholder to get the comma-and-space at the end
            terms.push( "" );
            this.value = terms.join( ", " );
            return false;
        }
    });
});
</script>

 
 
<style>
.spacing {
    padding: 0;
}
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index:2147483647 !important /* Sit on top */
    padding-top: 50px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    width: 600px;
	top:80px;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}
.close.closenotify{
   background: #fff none repeat scroll 0 0 !important;
    border-radius: 40px;
    float: right;
    height: 40px;
    opacity: 1;
    width: 40px;
	color:#36b047;
}
.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.col-sm-3.popup{
	padding:10px;
}
.col-sm-9.popup{
padding:10px;
}
.compose{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
/*    border: 1px solid #249533;*/
    border-radius: 3px;
    color: #fff;
	padding:5px 30px;
	float:right;
}
/*.viewmessage{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
	padding:5px 7px;
}*/
</style>
     <style>
		/* Styles for page */
.container {
  width: 100%;
  margin: 10px auto;
  float:left;
  padding: 0 20px;
}

body {
  color: #4B4B4B;
  font-family: sans-serif;
}
body a {
  cursor: pointer;
  color: #4B4B4B;
  text-decoration: none;
}
body section {
  margin-bottom: 90px;
}
body section h1 {
  text-transform: uppercase;
  text-align: center;
  font-weight: normal;
  letter-spacing: 10px;
  font-size: 25px;
  line-height: 1.5;
}
body section p, body section a {
  text-align: center;
  letter-spacing: 3px;
}

span {
  letter-spacing: 0px;
}

p {
  font-weight: 200;
  line-height: 1.5;
  font-size: 14px;
}

.toggle_tbl{
	padding:0px !important;
}

/* Styles for Accordion */
.toggle:last-child {
  border-bottom: 1px solid #dddddd;
}
.toggle .toggle-title {
  position: relative;
  display: block;
  border-top: 1px solid #dddddd;
  margin-bottom: 6px;
}
.toggle .toggle-title h3 {
  font-size: 20px;
  margin: 0px;
  line-height: 1;
  cursor: pointer;
  font-weight: 200;
}
.toggle .toggle-inner {
  padding: 7px 25px 10px 25px;
  display: none;
  margin: -7px 0 6px;
}
.toggle .toggle-inner div {
  max-width: 100%;
}
.toggle .toggle-title .title-name {
  display: block;
  padding: 25px 25px 14px;
}
.toggle .toggle-title a i {
  font-size: 22px;
  margin-right: 5px;
}
.toggle .toggle-title i {
  position: absolute;
  background: url("<?php echo base_url();?>assets/images/plus_minus.png") 0px -24px no-repeat;
  width: 24px;
  height: 24px;
  -webkit-transition: all 0.3s ease;
  transition: all 0.3s ease;
  margin: 20px;
  right: 0;
}
.toggle .toggle-title.active i {
  background: url("<?php echo base_url();?>assets/images/plus_minus.png") 0px 0px no-repeat;
}
.toggle{background-color:#fff;}

.recentsec{padding:0px 0 0 5px !important;
			float:right !important;}
@media screen and (min-width:981px){
.toggle .img-responsive {
    display: inline;
    width: 24%;
} }

.editprofile-content {
    min-height: 200px;
}
.contentsec > p {
    color: #333c48;
    font-size: 16px;
    margin-bottom: 0;
}
.staff .recent_view > a {
    background: #57b94a none repeat scroll 0 0;
    border-radius: 3px;
    color: #fff;
    padding: 5px 25px 6px;
	text-decoration:none;
}

.attachment_section {
    list-style: outside none none;
    padding-left: 0;
}
.attachment_section li h4 {
	font-size:14px;
}
.icon_section {
    float: right;
	padding-right:20px;
}
#viewModal40 .col-sm-3.nopadding.popup {
    border: 1px solid #ccc !important;
}
#viewModal40 .col-sm-9.nopadding.popup {
    border: 1px solid #ccc;
}
.imgsec > img {
    height: 50px;
    width: 50px;
}
div.css_bar_graph div.graph li {
    height: 33px;
}
.bottom img {
    height: 40px;
    width: 155px;
}
#myModal .col-sm-12.nopadding > h3 {
    background: #36B047;
    border-radius: 5px;
    color: #fff;
    padding: 13px 0;
    text-align: center;
}	
#myModal .modal-content {
    background-color: #fefefe;
/*    border: 5px solid #36B047;*/
 
    margin: auto;
    padding: 0px;
    top: 80px;
    width: 500px;
}
.viewmodal> h3 {
   background: #36b047 none repeat scroll 0 0;
   color: #fff;
   float:right;
   text-transform: uppercase;
    padding-right:40px;
}
.composemsg> h3 {
   background: #36b047 none repeat scroll 0 0;
   color: #fff;
   float:right;
   text-transform: uppercase;
   padding-right:30px;
}
.greybar.bar.imgstud {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    left: -10px !important;
}
.greybar.bar.imgstud > img {
    height: 200px !important;
    width: 35px;
}
.ui-autocomplete-input, .ui-menu, .ui-menu-item { z-index: 2147483647; }
.col-sm-3.nopadding.popup {
    font-size: 14px;
    font-weight: bold;
}
#toggletbl{
	text-align:center;
}

.compose{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
    padding:5px 30px;
	float:right;
}

.composeclose{
padding:8px 0px 0px 0px;
}
.msgbox{
	padding:10px 0 0 20px;
}
.btncls{padding-top:5px;}
.viewdownload{padding-right:10px;}

.attchmnttxt{font-size:11px;}

.checkboxdiv.profilecheckbox > label {
    width:50px !important;
}
.modalbg {
    background: #fff none repeat scroll 0 0;
    float: left;
    width: 100%;}
	
.fa-download+ .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-download  + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

.fa-eye + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-eye  + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

.recent_view{text-align:right;}
.heading_view{padding-left:30px !important;}

.tablewrapper .familydata td i.icnclass{
    color: inherit !important;}
	
.viewmessage:hover{
	color:#09F !important;
}
.deletemsg:hover{
	color:#09F !important;
}
.rightspace .viewmessage{color:black;}
.rightspace .deletemsg{color:black;}

 /***** 16-aug *****/
 
.ui-autocomplete {
	max-height: 270px;
	overflow-y: auto;
	overflow-x: hidden;

} 

.prfltitle > h1 {
    padding: 0 0 0 15px;
}
.css_bar_graph {
    float: left;
    width: 450px;
	overflow-y:hidden;
}	
.admin_msg::after {
    content: none !important;
}
        </style>
        <script>
$(document).ready(function(){
  $('.dropdown-submenu a.subMenu').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
});
</script>