<div class="editprofile-content enrollmentcontent">



          <div style="clear:both"></div>

 <?php if($this->session->flashdata('error')): ?>

   <div class="alert alert-danger alert-dismissable" >

			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

			<b>Alert!</b> 

			  <?php echo $this->session->flashdata('error'); ?>

		</div>

<?php endif; ?>



<?php if($this->session->flashdata('success')): ?>

     <div class="alert alert-success alert-dismissable" >

		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		   <b>Alert!</b> 

		   <?php echo $this->session->flashdata('success'); ?>

	   </div>

<?php endif; ?>

 <div style="clear:both"></div> 	

        <div class="col-sm-12 leftspace fullwidsec">



        <div class="editprofileform editdetails accdetailinfo">

   

<form name="basicform" id="basicform" method="post" action="<?php echo base_url(); ?>staffenrolementdashboard/edit_staff/<?php echo $info->id; ?>" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">



   <div id="sf1" class="tab-pane active frm">

    <fieldset>

        

       <div class="editprofileform enrollmentprofileform">



  <div class="profile-bg prfltitle profile_titlebg enrollment_titlebg">

  <h1>General details</h1>





  <div class=" col-sm-12 nopadding contentdiv">

  <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding">Username<span>*</span></label>

    <div class="col-sm-7 inputbox">

      <input type="text" class="form-control" name="username" id="username" value="<?php echo $info->username; ?>"  placeholder="" required="required" autocomplete="off" maxlength="41" disabled="disabled">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding">Password<span>*</span></label>

    <div class="col-sm-7 inputbox">

      <input class="form-control" name="password" id="password" value="<?php echo $info->password; ?>" placeholder="" required="required" autocomplete="off" maxlength="16" disabled="disabled">

    </div>

  </div>

  </div>

  <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding">Date of Birth<span>*</span></label>

    <div class="col-sm-7 inputbox datepickerdiv">

      <input type="text" name="staff_dob" id="staff_dob" class="datepicker" value="<?php if($info->staff_dob=='0000-00-00') { echo ''; } else { echo date('d-m-Y', strtotime($info->staff_dob)); } ?>" required="required" maxlength="10">

    </div>

  </div>

  </div>
    
    <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding"> Email Address<span>*</span></label>

    <div class="col-sm-7 inputbox">

      <input type="email" class="form-control" id="email" name="email" value="<?php echo $info->email;?>" placeholder="" required="required" autocomplete="off" maxlength="121">

    </div>

  </div>

  </div>
  

  


<div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding">Avtar<br/><span class="tagline">JPG, PNG</span></label>

    <div class="col-sm-7 profile-picture inputbox">

  

    <input type="hidden" name="avtaruserid" id="avtaruserid" value="<?php echo $info->id; ?>" />

    

    <div class="upload-pimg-msg"></div>

    

 <div class="pimg-preview" id="dynamicavtar"> 

<?php if($info->profile_image!='') { ?> <img src="<?php echo base_url();?>uploads/staff/<?php echo $info->profile_image; ?>"> 

 <?php } else { ?> 

 <img src="<?php echo base_url();?>assets/images/avtar.png">

 <?php } ?>

 </div>

   <div class="profilepicbtns"> <input type="file" name="profile_image" id="file" accept="image/*" style="padding: 0 12px; display:none;"/><i class="fa fa-picture-o" id="changeprofilepic" data-toggle="tooltip" title="Select your image" data-placement="right"></i><br/><hr class="borderline"><i class="fa fa-trash-o deleteprofilepic" id="<?php echo $info->profile_image; ?>" data-toggle="tooltip" title="Delete your image" data-placement="right"></i></div>

    </div>

  </div>

  </div>
  
    <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding">Telephone <span>*</span></label>

    <div class="col-sm-7 inputbox">

     <input type="text" name="staff_telephone" id="staff_telephone" value="<?php echo $info->staff_telephone;?>" class="form-control" required="required" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">

    </div>

  </div>

  </div>
  
       <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding"> Address <span>*</span></label>

    <div class="col-sm-7 inputbox">

     <input type="text" name="staff_address" id="staff_address" value="<?php echo $info->staff_address;?>" class="form-control" required="required" autocomplete="off" maxlength="251">

    </div>

  </div>

  </div>



  

      <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding">Title<span>*</span></label>

    <div class="col-sm-7 inputbox">

      <select name="staff_title" id="staff_title" class="form-control" required="required">

          <option value="" disabled="disabled" selected="selected">Select one </option>

 		 <option <?php if($info->staff_title == "Mr"){ echo 'selected'; } ?> value="Mr">Mr</option>

  		 <option <?php if($info->staff_title == "Ms"){ echo 'selected'; } ?> value="Ms">Ms</option>

         <option <?php if($info->staff_title == "Mrs"){ echo 'selected'; } ?> value="Mrs">Mrs</option>

		</select>

    </div>

  </div>

  </div>
  
     <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding"> Address(Optional)</label>

    <div class="col-sm-7 inputbox">

     <input type="text" name="staff_address1" id="staff_address1" value="<?php echo $info->staff_address_1;?>" class="form-control" autocomplete="off" maxlength="251">

    </div>

  </div>

  </div>


  




  <div class="col-sm-6 enrollmentfield forminputsec"> 

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding">FirstName<span>*</span></label>

    <div class="col-sm-7 inputbox">

      <input type="text" class="form-control" name="staff_fname" id="staff_fname" value="<?php echo $info->staff_fname;?>" placeholder="" required="required" autocomplete="off" maxlength="61">

    </div>

  </div>

  </div>

     <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding"> Qualification <span>*</span></label>

    <div class="col-sm-7 inputbox">

     <input type="text" name="staff_education_qualification" id="staff_education_qualification" value="<?php echo $info->staff_education_qualification;?>" class="form-control" required="required" autocomplete="off" maxlength="251">

    </div>

  </div>

  </div>


  
      <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding">LastName<span>*</span></label>

    <div class="col-sm-7 inputbox">

      <input type="text" class="form-control" name="staff_lname" id="staff_lname" value="<?php echo $info->staff_lname;?>" placeholder="" required="required" autocomplete="off" maxlength="61">

    </div>

  </div>

</div>



  

     <div class="col-sm-6 enrollmentfield forminputsec">

  <div class="form-group">

    <label class="col-sm-5 control-label nopadding"> Personal Summary </label>

    <div class="col-sm-7 inputbox">

     <textarea name="staff_personal_summery" id="staff_personal_summery" class="form-control" autocomplete="off" ><?php echo $info->staff_personal_summery;?></textarea>

    </div>

  </div>

  </div>

  </div>

  </div>

   </div>  
   
   </fieldset>
   </div>
   </form>
   
  <form name="basicform2" id="basicform2" method="post" action="" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
   
      <div id="sf1" class="tab-pane active frm">

    <fieldset>      
	<div class="editprofileform enrollmentprofileform">
	<div class="profile-bg prfltitle profile_titlebg enrollment_titlebg">
   <div class="staffenrolmentreport-tabsec">
   <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#education" aria-controls="education" role="tab" data-toggle="tab">Education</a></li>

    <li role="presentation"><a href="#experience" aria-controls="experience" role="tab" data-toggle="tab">Experience</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
  
    <div role="tabpanel" class="tab-pane active" id="education"> 
    
<div class="addeducation adddiv">


<div class="col-sm-2 addinput">
  <div class="form-group">
    <label>Degree</label>
    <input type="text" class="form-control" id="degree" value="" name="degree" />
      </div>
</div>
<div class="col-sm-2 addinput">
  <div class="form-group">
    <label>Start Year</label>
	 <input type="text" class="form-control" id="startyear" value="" name="startyear"   maxlength="4" onKeyUp ="this.value=this.value.replace(/[^0-9]/g,'')" />
  </div>
</div>
<div class="col-sm-2 addinput">
  <div class="form-group">
    <label>End Year</label>

  <input type="text" class="form-control" id="endyear" value="" name="endyear" maxlength="4" onKeyUp ="this.value=this.value.replace(/[^0-9]/g,'')" />
  </div>
</div>
<div class="col-sm-2 addinput">
  <div class="form-group">
    <label>University</label>
    <input type="text" class="form-control" id="university" value="" name="university">
  </div>
</div>

<div class="col-sm-2 addinput">
  <div class="form-group">
    <label>Grade</label>
    <input type="text" class="form-control" id="percentage" value="" name="percentage" onKeyUp ="this.value=this.value.replace(/[^A-Da-d]/g,'')">
  </div>
</div>
<div class="col-sm-2 addbtn">
  <input type="button" id="addeducation" class="btn btn-default" value="Add Education">
  </div>

</div>	
<div class="error_div"><span class='error' ></span></div>
    <div class="col-sm-12 tablediv nopadding">
	    <div class="tablewrapper">
        <table class="table-bordered table-striped" id="edurecord">
        
        <?php if(count($edu_info)>'0'){
			?>
			
			<thead>
				  <tr class="headings">
					  <th class="column5">Degree</th>
                       <th class="column5">University</th>
					  <th class="column3">Start Year</th>
					  <th class="column3">End Year</th>
					 
					 <th class="column2">Grade</th>
					 <th class="column1">Action</th>
					  
				  </tr>
			  </thead>
              <?php foreach($edu_info as $educationdata){?>
              <tbody > <tr class="familydata educationlisting">
						<td class="column5 numeric"><?php echo $educationdata->degree; ?></td>
                        <td class="column5"><?php echo $educationdata->university; ?></td>
						<td class="column3"><?php echo $educationdata->start_year; ?></td>
						<td class="column3"><?php echo $educationdata->end_year; ?></td>
						
						<td class="column2"><?php echo $educationdata->percentage;?></td>
						<td class="column1">
							<a class="change" onclick="delConfirm(<?php echo $educationdata->id; ?>)" style="cursor: pointer;">
							<i class="fa fa-trash-o" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
							</a>
						</td>
                        </tr> </tbody>
              
           <?php }} else { ?>   
               <tbody>
            <tr><th colspan="7" style="text-align: center; width:999px;height:100px; background:#FFF; color: #6a7a91;;font-size:25px;">No record to show.</th></tr>
            </tbody>
              <?php } ?>
              
			</table>
        
	<table class="table-bordered table-striped" id="table-data">
<!----  Data Coming from Controller through AJAX ----->			
		  </table>
          </div>
        
		  </div>
		  </div>

    <div role="tabpanel" class="tab-pane" id="experience">
<div class="addeducation adddiv">

<div class="col-sm-3 addinput">
  <div class="form-group">
    <label>Company Name</label>
    <input type="text" class="form-control" id="companyname" value="" name="companyname">
  </div>
</div>
<div class="col-sm-2 addinput">
  <div class="form-group">
    <label>Start Year</label>
 <input type="text" class="form-control" id="exp_startyear" value="" name="exp_startyear" onKeyUp ="this.value=this.value.replace(/[^0-9]/g,'')">
  </div>
</div>
<div class="col-sm-2 addinput">
  <div class="form-group">
    <label>End Year</label>
	<input type="text" class="form-control" id="exp_endyear" value="" name="exp_endyear" onKeyUp ="this.value=this.value.replace(/[^0-9]/g,'')">
  </div>
</div>
<div class="col-sm-2 addinput">
  <div class="form-group">
    <label>Position</label>
    <input type="text" class="form-control" id="salary" value="" name="salary" >
  </div>
</div>
<div class="col-sm-3 addbtn">
  <input type="button" class="btn btn-default" id="addexperience" value="Add Experience">
  </div>

</div>		
	    <div class="col-sm-12 tablediv nopadding">
	    <div class="tablewrapper">
        
        <table class="table-bordered table-striped" id="experncdetail">
        
         <?php if(count($exp_info)>0){?>
			  <thead>
				  <tr class="headings">
					  <th class="column7">Company Name</th>
                      <th class="column5">Position</th>	
					  <th class="column3">Start Year</th>
					  <th class="column3">End Year</th>
                      <th class="column2">Action</th>
					  				  
				  </tr>
			  </thead>
              
                <?php foreach($exp_info as $experiencedata){?>
				<tbody>
					<tr class="familydata experiencelist">
						<td class="column7 numeric"><?php echo $experiencedata->company_name; ?></td>
                        <td class="column5"><?php echo $experiencedata->position; ?></td>
						<td class="column3"><?php echo $experiencedata->exp_startyear; ?></td>
						<td class="column3"><?php echo $experiencedata->exp_endyear; ?></td>
						
                        <td class="column2">
							<a class="change" onclick="delConfirm_exp(<?php echo $experiencedata->id; ?>)" style="cursor: pointer;">
							<i class="fa fa-trash-o" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
							</a>
						</td>
                        </tr>

					</tbody>
                    
           <?php }} else { ?>   
               <tbody>
            <tr><th colspan="7" style="text-align: center; width:999px;height:100px; background:#FFF; color: #6a7a91;;font-size:25px;">No record to show.</th></tr>
            </tbody>
              <?php } ?>
		  </table>
          
    	<table class="table-bordered table-striped" id="exp-table-data">
<!----  Data Coming from Controller through AJAX ----->			
		  </table>      
          </div>
		  </div>
          </div>
  </div>
   
   </div>
   </div>
   </div>

     </fieldset> 

        </div>  
     
     
     </form>
     
     
     
     
   <div id="sf6" class="tab-pane frm" style="display: block;">

        <fieldset>

 <div class="editprofileform">

  <div class="cancelconfirm prevnextbtns">

        <div class="col-sm-6 nopadding">

        <div class="cancellink">

       <!-- <input class="cancelbtn" id="cancelbtn" onclick="cancelupdate('766')" value="Cancel" type="button">-->

        </div>

        </div>

        <div class="col-sm-6 nopadding">

        <div class="confirmlink">

        <input class="open6" value="Submit" type="button">

        </div>

        </div>

        </div>


        </div>

        </fieldset>

        </div>

        </div>

        </div>

        </div>



 <script type="text/javascript">

	function cancelupdate(id){

		window.location.href ="<?php echo base_url();?>staffs/edit/"+id;

	}

</script>

 <script>

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip(); 

});
</script>   
 
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url() ;?>staffenrolementdashboard/delete/"+id;
		}else{
			return false;
		}
}
</script>

 <script type="text/javascript">
	function delConfirm_exp(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url() ;?>staffenrolementdashboard/delete_exp/"+id;
		}else{
			return false;
		}
}
</script>
        

 <script type="text/javascript">

  

  jQuery(document).ready(function(){
	  
	     $("#staff_dob").keydown(false);

	  	jQuery('body').click(function(){

		jQuery('.alert-dismissable').fadeOut('slow');

		})

		

  jQuery("#file").change(function() {

	jQuery(".upload-pimg-msg").empty(); 

	var file = this.files[0];

	var imagefile = file.type;

	var imageTypes= ["image/jpeg","image/png","image/jpg"];

		if(imageTypes.indexOf(imagefile) == -1)

		{

			jQuery(".upload-pimg-msg").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");

			return false;

		}

		else

		{

			var reader = new FileReader();

			reader.onload = function(e){

				jQuery(".pimg-preview").html('<img src="' + e.target.result + '" width="120px" height="100px" />');				

			};

			reader.readAsDataURL(this.files[0]);

		}

	});	

	  

	  jQuery('#changeprofilepic').on('click', function() {

             jQuery('#file').click();

       });

	   

	  jQuery('.deleteprofilepic').on('click', function() {

            var avtarName =  jQuery(this).attr('id');

			 var avtarUid = jQuery('#avtaruserid').val();

			 if(avtarName!='' && avtarUid!='') {

			 jQuery.ajax({

         		          type : 'POST',

                          url  : '<?php echo base_url(); ?>staffs/deleteAvtar',

                          data:{ 'avtarName': avtarName,'avtarUid': avtarUid },

                          success :  function(resp) {

							  if(resp==1) {

					            jQuery('#dynamicavtar').html('<img src="<?php echo base_url();?>assets/images/avtar.png">');

								jQuery('.deleteprofilepic').attr('id','');

							  } 
			            }

				   });

			 } else {

				// return false;

				jQuery("#file").val(null);

				jQuery('#dynamicavtar').html('<img src="<?php echo base_url();?>assets/images/avtar.png">');

			   }

       }); 

  });

</script>

<script type="text/javascript">

  jQuery().ready(function() {

	jQuery.validator.addMethod("alphaUname", function(value, element) {

    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);

    });

	jQuery.validator.addMethod("alphaLetter", function(value, element) {

     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);

    });

	jQuery.validator.addMethod("alphaLnumber", function(value, element) {

      return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);

    });

	jQuery.validator.addMethod("alphaUpwd", function(value, element) {

    return this.optional(element) || value == value.match(/^(?=.*[a-zA-Z0-9])[a-zA-Z0-9!@#$%&*.]{7,}$/);

    });
	
	jQuery.validator.addMethod("greaterThan",function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }

    return isNaN(value) && isNaN($(params).val()) 
        || (Number(value) > Number($(params).val())); 
		},'Must be greater than start year.');

    // validate form on keyup and submit

    var v = jQuery("#basicform").validate({



      rules: {

         username: {

          required: true,

		  alphaUname: true,

          minlength: 4,

          maxlength: 40

		  /*remote: {

             url: "<?php //echo base_url(); ?>students/check_username",

             type: "POST"

             }*/

        },

        password: {

          required: true,

		  alphaUpwd: true,

          minlength: 6,

          maxlength: 15

        },

		email: {

          required: true,

		  email: true,

		  maxlength: 120

        },

       staff_fname: {

          required: true,

		  alphaLetter: true,

		  maxlength: 60

        },

		staff_lname: {

          required: true,

		  alphaLetter: true,

		  maxlength: 60

        },

		staff_dob: {

          required: true

        },

		staff_title: {

          required: true

        },

		staff_telephone: {

		  required: true,	

          number: true,

		  minlength: 11,

          maxlength: 11

        },

		staff_address: {

		   required: true,

           maxlength: 250

        },

		staff_address1: {

           maxlength: 250

        },

		staff_education_qualification: {

		   required: true,

           maxlength: 250

        },

		staff_personal_summery: {

		  maxlength: 250

        }

      },

	  errorElement: "span",

      errorClass: "help-inline-error",

	  

	  messages: {

                 username: {

                  required: "This field is required.",

				  alphaUname: "Letters only please.",

				  minlength: "Minimum 4 characters required.",

				  maxlength: "Maximum 40 characters allowed."

				 // remote: "User name already in use."

                 },

			    password: {

                  required: "This field is required.",

				  alphaUpwd: "Letters, numbers and special characters allowed",

				  minlength: "Minimum 6 characters required.",

				  maxlength: "Maximum 15 characters allowed."

                 },

				email: {

                    required: "This field is required.",

					email: "Please enter a valid email address.",

					maxlength: "Maximum 120 characters allowed."

                   },

				staff_fname: {

                  required: "This field is required.",

				  alphaLetter: "Letters only please.",

				  maxlength: "Maximum 60 characters allowed."

                 },

				staff_lname: {

                  required: "This field is required.",

				  alphaLetter: "Letters only please.",

				  maxlength: "Maximum 60 characters allowed."

                 },

				staff_dob: {

                  required: "This field is required."

                 },

			    staff_title: {

                  required: "This field is required."

                 },

				staff_telephone: {

				  required: "This field is required.",

				  number: "Numbers only please.",

				  minlength: "Minimum 11 characters required.",

				  maxlength: "Maximum 11 characters allowed."

				},

				staff_address: {

				   required: "This field is required.",

				   maxlength: "Maximum 250 characters allowed."

				},

				staff_address1: {

				   maxlength: "Maximum 250 characters allowed."

				},

				

				staff_education_qualification: {

			      required: "This field is required.",

				  maxlength: "Maximum 250 characters allowed."

				},

			

				staff_personal_summery: {

				  maxlength: "Maximum 250 characters allowed."

				}

            },


	focusInvalid: false,

    invalidHandler: function(form, validator) {

        if (!validator.numberOfInvalids())

            return;

        $('html, body').animate({

            scrollTop: $(validator.errorList[0].element).offset().top

        }, 1000);

      },		

		

    });



	 $(".open6").click(function() {

      if (v.form()) {

		  $( "#basicform" ).submit();

         return false;

      }

    });


    var otherform = jQuery("#basicform2").validate({

      rules: {

         degree: {

          required: true,

	      minlength: 2,

          maxlength: 100,
		  
		  alphaLetter :true
		
        },

        startyear: {

          required: true,

          maxlength: 4

        },

		endyear: {

          required: true,

		  maxlength: 4,
		  
		 greaterThan: "#startyear" 

        },

       university: {

          required: true,

		  alphaLetter: true

        },

		percentage: {

          required: true,

		  maxlength: 1

        },
		
        companyname: {

          required: true,
		  
		  alphaLetter :true

        },

        exp_startyear: {

          required: true,

          maxlength: 4

        },

		exp_endyear: {

          required: true,

		  maxlength: 4,
		  
		  greaterThan: "#exp_startyear" 

        },

       salary: {

          required: true,
		  
		  alphaLetter :true
	
        }
		
      },

	  errorElement: "span",

      errorClass: "help-inline-error",

	  

	  messages: {

                 degree: {

                  required: "This field is required.",

				  minlength: "Minimum 2 characters required.",

				  maxlength: "Maximum 100 characters allowed.",
				  
				  alphaLetter: "Letters only please."

                 },

			    startyear: {

                  required: "This field is required.",

				  maxlength: "Maximum 4 characters allowed."

                 },

				endyear: {

                    required: "This field is required.",

					maxlength: "Maximum 4 characters allowed."

                   },

				university: {

                  required: "This field is required.",

				  alphaLetter: "Letters only please."

                 },

				percentage: {

                  required: "This field is required.",

				  maxlength: "Maximum 1 characters allowed."

                 },
				 
				companyname: {

                  required: "This field is required.",
				  
				  alphaLetter: "Letters only please."

                 },

			    exp_startyear: {

                  required: "This field is required.",

				  maxlength: "Maximum 4 characters allowed."

                 },

				exp_endyear: {

                    required: "This field is required.",

					maxlength: "Maximum 4 characters allowed."

                   },
				   
				salary: {

                  required: "This field is required.",
				  
				  alphaLetter: "Letters only please."


                 }

            },


	focusInvalid: false,

    invalidHandler: function(form, validator) {

        if (!validator.numberOfInvalids())

            return;

        $('html, body').animate({

            scrollTop: $(validator.errorList[0].element).offset().top

        }, 1000);

      },		

    });
	
	
	 $("#addeducation").click(function() {

      if (otherform.form()) {
		 
	   var staff_enrol_id= <?php echo $this->session->userdata('user_id');?>

	   var degree = $('#degree').val();
       var startyear = $('#startyear').val();
       var endyear = $('#endyear').val();
       var university = $('#university').val();
	   var percentage = $('#percentage').val();
	   
   jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staffenrolementdashboard/save_education',
            data:{'staff_enrol_id':staff_enrol_id,'degree': degree, 'start_year': startyear, 'end_year': endyear,'university': university, 'percentage': percentage  },
			dataType: 'html',
			success :  function(data) {
     			jQuery("#table-data").html(data);
				jQuery('#edurecord').hide();
				jQuery('#degree').val('');
				jQuery('#startyear').val('');
				jQuery('#endyear').val('');
				jQuery('#university').val('');
				jQuery('#percentage').val('');
    		}
    	});

         return false;

      }

    });
	
	
	jQuery("#addexperience").on('click', function(e){ 
   
        if (otherform.form()) {
			
		var staff_enrol_id= <?php echo $this->session->userdata('user_id');?>
		

	   var companyname = $('#companyname').val();
       var exp_startyear = $('#exp_startyear').val();
       var exp_endyear = $('#exp_endyear').val();
       var salary = $('#salary').val();
	
   jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staffenrolementdashboard/save_experience',
            data:{'staff_enrol_id':staff_enrol_id,'companyname': companyname, 'exp_startyear': exp_startyear, 'exp_endyear': exp_endyear,'salary': salary },
			dataType: 'html',
			success :  function(data) {
     			jQuery("#exp-table-data").html(data);
				jQuery('#experncdetail').hide();
				jQuery('#companyname').val('');
				jQuery('#exp_startyear').val('');
				jQuery('#exp_endyear').val('');
				jQuery('#salary').val('');
				}
    	});
		
		         return false;

      }

	});

/*  $('#startyear').datepicker( {

        changeMonth: false,
        changeYear: true,
        showButtonPanel: true,
        yearRange: '1950:date(y)', // Optional Year Range
        dateFormat: 'yy',
        onClose: function(dateText, inst) { 
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, 0, 1));
        }
		});*/
		

    });	

</script>

<style>

.ui-datepicker-calendar {
    display: none;
    }
	
	
.staffenrolmentreport-tabsec .nav-tabs li a{
   background: -webkit-linear-gradient(#ffffff, #dddfe2); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(#ffffff, #dddfe2); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(#ffffff, #dddfe2); /* For Firefox 3.6 to 15 */
background: linear-gradient(#ffffff, #dddfe2); /* Standard syntax */
border:0px;color:#000;font-weight:bold;padding:10px 40px;text-transform:uppercase;}
.staffenrolmentreport-tabsec .nav-tabs li.active a{   background: -webkit-linear-gradient(#39b54a, #33aa44); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(#39b54a, #33aa44); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(#39b54a, #33aa44); /* For Firefox 3.6 to 15 */
background: linear-gradient(#39b54a, #33aa44); /* Standard syntax */
border:0px;color:#fff;}
.addinput{padding:0 1px 0 10px;}
.addbtn input {
    background: #35ae46 none repeat scroll 0 0;
    border: 0 none;
    border-radius: 0;
    color: #fff;
    margin-top: 26px;
    width: 100%;
}
.addeducation.adddiv {
    float: left;
    margin: 10px 0;
    width: 100%;
}
@media (max-width:991px) {
	/**educationlist**/
		
		.educationlisting td:nth-of-type(1):before { content: "Degree"; }

	.educationlisting td:nth-of-type(2):before { content: "Start Year"; }
	
	
	
		.educationlisting td:nth-of-type(3):before { content: "End Year"; }

	.educationlisting td:nth-of-type(4):before { content: "University"; }
	.educationlisting td:nth-of-type(5):before { content: "Total Marks Obtained"; }
.educationlisting td:nth-of-type(6):before { content: "Percentage"; }

	/**experiencelist**/
		
		.experiencelist td:nth-of-type(1):before { content: "Company Name"; }

	.experiencelist td:nth-of-type(2):before { content: "Start Year"; }
	
	
	
		.experiencelist td:nth-of-type(3):before { content: "End Year"; }

	.experiencelist td:nth-of-type(4):before { content: "Salary"; }
	
}

#percentage{
	text-transform:uppercase;
}

</style>

