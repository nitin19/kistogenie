<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

       <ul>

		<li><a href="#">Home</a></li>

        <li><a href="#">Attendance</a></li>

        <li class="edit"><a href="#">Generate Report</a></li>        

        </ul>

        </div>
<!--        
        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" value="Quick actions" class="activequickaction">

        </div>-->

        </div>
        
        
 <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>
        
    	
        <div class="attendancesec">	

        <div class="col-sm-12 profile-bg filterbox generatefilter" >

        <div class="filterdiv">

       <form action="<?php echo base_url();?>Studentdashboard/myattendance" name="attendancereportForm" id="attendancereportForm" >

                <div class="col-sm-12 col-xs-12 applycodediv nopadding">

                <div class="col-sm-4 nopadding tabfilterhalf">

                <div class="col-sm-2 col-xs-2 selectfilter datetext nopadding" style="width:50px">

                <span>Date:</span>

                </div>

                <div class="col-sm-10 col-xs-10 nopadding datepickdiv">

                <div class="col-sm-5 col-xs-5 nopadding dateblk">

        		<div class="form-group">

    <div class="inputbox datepickerdiv nopadding">

      <input type="text" name="sdate" id="sdate" value="<?php echo @$sdate_search;?>" class="datepickerattendancereport1">

    </div>

  </div> 

  				</div>

                <div class="col-sm-1 col-xs-1 dividerline nopadding"><span><img src="<?php echo base_url();?>assets/images/between.png"></span> </div>

                <div class="col-sm-5 col-xs-5 nopadding dateblk">

  				<div class="form-group">

    <div class="inputbox datepickerdiv nopadding">

      <input type="text" name="edate" id="edate" value="<?php echo @$edate_search;?>" class="datepickerattendancereport2">

    </div>

  </div>

  </div>

</div>

</div>

	<div class="col-sm-5 nopadding selectoption tabfilterhalf resselectfil mobhalffilter">

<div class="form-group fullwidthinput">

    <label class="col-sm-4 control-label nopadding">Attendance Type:</label>

    <div class="col-sm-7 inputbox nopadding">
		
      <select class="form-control" name="type" id="type">
     	<option value="">Select</option>
  		<option  value="1">Present</option>
  		<option  value="2">Absent</option>
  		<option  value="3">Late</option>
	 </select>

      

    </div>

  </div>

</div>

 <div class="col-sm-2 nopadding selectoption tabfilterfull mobfullfilter">

<div class="col-sm-12 rightspace selectoption resselectfil">

<div class="form-group generatereport" >

    <input type="submit" class="btn btn-danger" value="Generate Report">

    

  </div>

</div>

				</div>

        </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

   
    
    <h1>Generate Report</h1>

    <div class="tablewrapper">
    
    <div id="printTable">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2">No.</th>

					  <th class="column7">Date</th>

					  <th class="column7">Attendence Type</th>

					  <th class="column4">Attendence Session</th>

				  </tr>

			  </thead>

				<tbody>
    
    <?php
	if($attendence){
		$sr = 1;
		foreach($attendence as $attendata){
	 ?>

					<tr class="familydata report">

						<td class="column2"><?php echo $sr++; ?></td>

						<td class="column7"><?php echo $attendata->attendance_date; ?></td>

						<td class="column7"><?php
						  if($attendata->attendance_status == '1'){echo "Present";} 
						   if($attendata->attendance_status == '2'){echo "Absent";}
						    if($attendata->attendance_status == '3'){echo "Late";}
						  ?></td>

                        <td class="column4"><?php echo $attendata->attendance_session; ?></td>

                        </tr>
<?php } } else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px; background:#FFF; height:100px; color: #6a7a91;font-size:25px">No record to show.</th></tr>	
				   <?php } ?>

				</tbody>

		  </table>
          
     </div>     
          
        </div>        
</div>
    
    <div class="beforeresult_section"><h1></h1></div>
	</div>
	</div>
    </div>
    </div>


    <script type="text/javascript">
<!--
function printContent(id){
str=document.getElementById(id).innerHTML
newwin=window.open('','printwin','left=100,top=100,width=400,height=400')
newwin.document.write('<HTML>\n<HEAD>\n')
newwin.document.write('<TITLE>Print Page</TITLE>\n')
newwin.document.write('<script>\n')
newwin.document.write('function chkstate(){\n')
newwin.document.write('if(document.readyState=="complete"){\n')
newwin.document.write('window.close()\n')
newwin.document.write('}\n')
newwin.document.write('else{\n')
newwin.document.write('setTimeout("chkstate()",2000)\n')
newwin.document.write('}\n')
newwin.document.write('}\n')
newwin.document.write('function print_win(){\n')
newwin.document.write('window.print();\n')
newwin.document.write('chkstate();\n')
newwin.document.write('}\n')
newwin.document.write('<\/script>\n')
newwin.document.write('</HEAD>\n')
newwin.document.write('<BODY onload="print_win()">\n')
newwin.document.write(str)
newwin.document.write('</BODY>\n')
newwin.document.write('</HTML>\n')
newwin.document.close()
}
//-->
</script>
    
 <script>
  jQuery(document).ready(function(){
	  	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#attendancereportForm")[0].reset();
			   window.location.href='<?php echo base_url()?>attendancereport/index';
			}); 
			
	  jQuery("#branch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			// var BranchID =  jQuery('input[name=student_school_branch]:checked').val();
			jQuery("#class").find('option[value!=""]').remove();
			
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>studentdashboard/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#class').append(resp);
			          }
				   });
				 } else {
			     jQuery("#class").find('option[value!=""]').remove();
				 return false;
				 }
			 });
			 
			 
	    jQuery("#attendancereportForm").validate({
        rules: {
			
			  sdate: {
    					required: true
   					},
			   edate: {
    					required: true
   					}
					
            /*  branch: {
                 required: true
                 },
			  class: {
    					required: true
   					}, 
			   
			  type: {
                required: true
                 }*/
            },
        messages: {
			
			  sdate: {
    					required: "Please select start date."
   				 },
			  edate: {
    					required: "Please select end date."
   				 }
				 
              /*branch: {
                  required: "Please select branch."
                 },
			  class: {
    					required: "Please select class."
   					},
			  
			  type: {
                   required: "Please select type."
                 }*/
             },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		
			 
	 });
		 
</script>
<style>
.attendancesec{width:100%;}
 .beforeresult_section > h1 {
    min-height: 300px;
	}
.save_btn > input {
    background: #37b248 none repeat scroll 0 0;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #fff;
    font-size: 14px;
    padding: 7px 20px;
}
.save_btn {
    padding-top: 20px;
}
.popup_save_btn {
    background: #37b248 none repeat scroll 0 0;
    border: medium none;
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    color: #fff;
    padding: 12px 25px;
}
.modal-body {
    padding: 35px 0 70px;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-radius: 3px;
    min-height: 45px;
    width: 100%;
	padding: 10px;
}
.btn.btn-default {
    background: #37b248 none repeat scroll 0 0;
    color: #fff;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
    min-height: 44px;
    padding: 10px;
    width: 100%;
}
.save_btn {
    text-align: right;
}
.text_box_btn {
    text-align: left;
}
.modal-header {
    text-align: center;
}
</style>