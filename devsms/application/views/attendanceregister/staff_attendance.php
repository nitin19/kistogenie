<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

             <ul>

	          <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

           <?php  } elseif($user_type == 'student'){?>

              <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>
    <?php }else {?>
          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
    <?php } ?>

        <li><a href="<?php echo base_url(); ?>modifyattendance">Attendance</a></li>

        <li class="edit">Staff Attendance</li>        

        </ul>

        </div>



        </div>
        
       <div style="clear:both"></div>
         
     <div id="attendancestatusError" style="display:none;">
     <div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			 Please fill all staff attendance status field first.
		       </div>
     </div>
                
        <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>
 

        <div class="col-sm-12 attendancesec nopadding">	

        <div class="col-sm-12 profile-bg filterbox showbox">

        <div class="filterdiv">

  <form action="<?php echo base_url();?>staffattendance/index" name="loadstudentsForm" id="loadstudentsForm" autocomplete="off">

          <div class="col-sm-5 col-xs-12 applycodediv nopadding fullwidsec">

                <div class="col-sm-2 selectfilter nopadding resselectfil">

                <span>Show:</span>

                </div>

		  <div class="col-sm-5 selectblk resselectbox">


      <select class="form-control" name="branch" id="branch">
  <option value="">Select Branch</option>  
  <?php foreach ($all_branch as $branches) { ?>

          <option value="<?php echo $branches->branch_id ; ?>"><?php echo $branches->branch_name ?></option>  

<?php } ?>  

</select>

  </div>

         </div>


		<div class="col-sm-2 nopadding datepickdiv">

        <div class="form-group">

    <div class="col-sm-12 inputbox datepickerdiv nopadding dateinput">

      <input type="text" name="date" id="date" value="<?php echo $attendancedate_search;?>" class="datepickerattendance">

    </div>

  </div>

  		</div>
        

        <div class="col-sm-3 col-xs-7 rightspace viewreport rightbtn">

<div class="form-group">

   <input type="submit" class="btn btn-danger" value="Find Staff">
    <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>

  </div>

</div>

</form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding showbox">

    <div class="col-sm-12 nopadding fullwidsec">
    
    <?php if(@$_GET['branch']!='' ) { ?>

    <form action="<?php echo base_url(); ?>staffattendance/update_attendance" name="updateattendanceForm" id="updateattendanceForm" method="post" >
    
      <input type="hidden" name="branchid" id="branchid" value="<?php echo @$_GET['branch'];?>" />

      <input type="hidden" name="attendancedate" id="attendancedate" value="<?php echo @$_GET['date'];?>" />
     

    <h1>Modify Staff Attendance</h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">
					  <th class="column2">No.</th>
					  <th class="column4">Full Name</th>
            <th class="column3">Phone No.</th>
					  <th class="column3">Branch</th>
            <th class="column4">Status</th>
            <th class="column4">Notes</th>
 
				  </tr>

			  </thead>

				<tbody>
                
  <?php 
					  if(count(@$data_rows) > 0){

					  $sr=$last_page;

           foreach($data_rows as $staff) { 
					 $sr++;
		       $staffid        = $staff->user_id; 
           $branch_id      = @$_GET['branch'];
           $attendancedate = @$_GET['date'];
		       $this->load->model(array('staffattendance_model'));
		       $staffinfo =$this->staffattendance_model->getstaffinfo($staffid);
           $branchName =$this->staffattendance_model->getstaff_branches($branch_id);
					   ?>

				 <input type="hidden" name="staffid[]" value="<?php echo $staff->user_id;?>" />
                 
					<tr class="familydata">
						<td class="column2"><?php echo $sr; ?></td>
						<td class="column4"><?php 
              if($staffinfo->staff_fname!='' || $staffinfo->staff_lname !=''){
            echo $staffinfo->staff_fname.' '.$staffinfo->staff_lname;
            }elseif($staffinfo->email !=''){

             echo $staffinfo->email;
            }else {echo '<span style="color:transparent;">-</span>';}

            ?></td>
					  <td class="column3"><?php if($staffinfo->staff_telephone!=''){ echo $staffinfo->staff_telephone; } else {echo '<span style="color:transparent;">-</span>';}?></td>	
            <td class="column3"><?php echo $branchName->branch_name;?></td>
            <td class="column4 statusblk"><div class="form-group">

    <div class="col-sm-9 inputbox nopadding">
<?php if($Attendencerecords = "Attendencerecords"){ 
           $this->load->model(array('staffattendance_model'));
           $staffstatus =$this->staffattendance_model->getstaffstatus($branch_id , $staffid , $attendancedate );
     
  ?>

		<select class="form-control selectstyle" name="attendance_status[<?php echo $staff->user_id;?>]">
      <option  value="1" <?php if($staffstatus->attendance_status == '1'){ echo 'selected'; }?>>Absent</option>
      <option  value="2" <?php if($staffstatus->attendance_status == '2'){ echo 'selected'; }?>>Present</option>
      <option  value="3" <?php if($staffstatus->attendance_status =='3'){ echo 'selected'; }?>>First Half</option>
      <option  value="4" <?php if($staffstatus->attendance_status =='4'){ echo 'selected'; }?>>Second Half</option>
     
		</select>	 
        <?php	 
 } 
 
 else 
 {	
 ?>
 <select class="form-control selectstyle" name="attendance_status[<?php echo $staff->user_id;?>]">
   		<option value="1" selected>Absent</option>
  		<option value="2">Present</option>
  		<option value="3">First Half</option>
      <option value="4">Second Half</option>
		</select> 	
        <?php
	 } 
   ?>
    </div>

  </div></td>

  <td class="column4 nopadding">
    <div class="col-sm-12 inputbox nopadding">

      <input type="text" class="form-control" name="attendance_note[<?php echo $staff->user_id;?>]" id="attendance_note" value="<?php echo @$staff->attendance_note;?>" placeholder="">

    </div>

  </div></td>
  

      </tr>
                 
  <?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1056px;height:100px; font-size:25px; background:#FFF; color: #6a7a91; ">No record to show.</th></tr>	
				   <?php } ?>  

				</tbody>

		  </table>

 <?php if(count(@$data_rows) > 0) { ?>
 
        <div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-6 nopadding">

        <div class="cancellink">

       
        </div>

        </div>

        <div class="col-sm-6 nopadding">

        <div class="confirmlink">

         <input type="submit" name="updateattendanceBtn" id="updateattendanceBtn" class="btn btn-default" value="Update Register">

        </div>

        </div>

        </div>  
        
        <?php } ?>

</div>

</form>

    <?php } else { ?>
   
    
    <div class="beforeresult_section"><h1></h1></div>

       <?php } ?>

	</div>



	</div>

    </div>

    </div>
    
    
    <script>
  jQuery(document).ready(function(){
	   $("#date").keydown(false);
	  
	  	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#loadstudentsForm")[0].reset();
			   window.location.href='<?php echo base_url()?>staffattendance/index';
			}); 
			
  jQuery("#loadstudentsForm").validate({
        rules: {
              branch: {
                 required: true
                 },
				 
		  date: {
    					required: true
   					}

            },

        messages: {
              branch: {
                  required: "Please select branch."
                 },
			
			  date: {
    					required: "Please select date."
   				 }
						 
             },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
			 
 var validFrom=false;
	 jQuery("#updateattendanceForm").validate({
		ignore: [],
        rules: {
			'attendance_status[]': {
    					required: true
   					}
            },
        messages: {
			 'attendance_status[]': {
    						required: "Required field",
   					}
               },
			   
	   submitHandler: submitForm
      });
	  
	  function submitForm() {
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staffattendance/check_staff_Attentance_status',
            data : jQuery("#updateattendanceForm").serialize(),
			dataType : "html",
            beforeSend: function()
            {
            },
            success :  function(data) {
				data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
					  validFrom=true;
					   jQuery('#updateattendanceForm').attr('action', '<?php echo base_url(); ?>staffattendance/update_attendance');
                       document.getElementById("updateattendanceForm").submit();
					} else if(data.Status == 'false') {
						jQuery('#attendancestatusError').show();
					} 
             }
        });
        return false;
      }
			 
	});
</script>
    <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>

<script type="text/javascript">
 
$(document).ready(function(){
 
    $('[tool-tip-toggle="tooltip-demo"]').tooltip({
 
        placement : 'top'
 
    });
 
});
 
</script>
    <style>
.resselectbox {
    padding: 0 4px;
}


.selectstyle{
	width: 100%;}
.branchstyle{
	 padding: 0 25px 0 0;
    width: 100% !important;
}


 .beforeresult_section > h1 {
    min-height: 590px;
	}
	.btnstyle{ 
	background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0 !important ;
    border: 1px solid #249533 !important;
    border-radius: 3px  ;
    color: #fff !important;
    float: left !important;
    font-size: 14px !important;
    margin: 20px 0 !important;
    padding: 6px 20px !important;}

.tooltip-arrow {
    left: 60.5% !important;
}

.rightbtn {
    float: right;
    padding: 0 0 0 10px;
}
.errspan {
    padding: 10px 30px 0 10px;
}
.datepickdiv {
    padding: 0 0 0 3px;
}
.rightbtn {
    float: right;
    padding: 0 0 0 20px;
}

.dateinput input{width:100% !important;}
.fa-question-circle + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.fa.fa-question-circle{color:#090;}
	</style>