<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">
   

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

	       <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>


        <li class="edit">Contract Details </li>        

        </ul>

        </div>

       
        </div>

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-12 col-xs-7 nopadding profile-bg accsection">

        <h1>Contract Details </h1>

        </div>

        </div>

        <div class="col-sm-8 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

        <div class="profile-bg prfltitle profile_titlebg">

        <h1>General details</h1>
<div class=" col-sm-12 nopadding contentdiv">

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Welcome email</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->welcomemail; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Contract type</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->cont_type; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Salary band</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->salaryband; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Work Day</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->workday; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Probation start date</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->probstartdate; ?></span>
    	</div>
</div>
  
<div class="form-group detailbox">
	<label class="col-sm-3 col-xs-4 control-label nopadding">Branch</label>
	<div class="col-sm-9 col-xs-8 inputbox">
	<?php 
		$branch_id = $info->branch_id;
	        $this->load->model(array('staffenrolement_model'));
	        $branchName = $this->staffenrolement_model->getbranchName($branch_id);
	?>
		<span><?php echo $branchName ->branch_name;?></span>
	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Probation end date</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->probenddate; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Probation extension</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->proext; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Permanent contract & HB</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->permantcontract; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Start date</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->startdate; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Contract received signed</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->contrecsign; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">SMS login/password</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->smslogin; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Appraisal review date</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->apprevdate; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Appraisal outcome</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->appreoutcome; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Pay rise Start date</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->payrisestartdate; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Contract End Date</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->contenddate; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">Reason</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->reason; ?></span>
    	</div>
</div>

<div class="form-group detailbox accdetail">
    	<label class="col-sm-3 col-xs-4 control-label nopadding">ARCHIVED</label>
    	<div class="col-sm-9 col-xs-8 inputinfo">
	      	<span><?php echo $info->archieved; ?></span>
    	</div>
</div>

</div>
</div>
        
        </div>
        </div>
        </div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script> 


<style>
.dashboard-attendance .statusdiv {
    width: 100%;
}
.icon_section {
    float: right;
    padding: 0 15px 0 0;
}
.cetificate_section {
    padding-left:0px;
	list-style:none;
	 font-size: 15px;
}
.cetificate_section > li {
    border: 1px solid #ccc;
    padding: 5px 10px 5px 15px;
}
.prfltitle {
    padding: 0 0px 15px;
}
.fa.fa-download + .tooltip > .tooltip-inner {background-color:#57B94A !important; color:#fff;}
.fa.fa-download + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}
.fa.fa-eye + .tooltip > .tooltip-inner {background-color: #57B94A !important; color:#fff;}
.fa.fa-eye + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}
.cetificate_section li h4 {
    border-bottom: 1px solid #ccc;
    padding-bottom: 10px;
}
.cetificate_section > li {
    border: medium none;

}
.profile-bg {
    border: medium none;
	padding: 0px;
}
.cetificate_section li h4 {
    color: #354052 !important;
    font-size: 14px;
}
.prfltitlediv {
    margin-bottom: 15px;
}
.profile_titlebg h1{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1);  padding: 10px 15px; }
.profiletitltbox{padding-top:0px;padding-bottom:0px;}
.profiletitltbox .profile_titlebg {
    padding: 0;
}
.contentdiv{
	padding:15px 15px 0px 15px;

}
.accsection h1 {
    padding: 0 0 10px 15px;
}

.docsec{
	margin-bottom:0px;}
.grphdiv{
	margin-left:15px;
}
.prfilleft {
    padding: 0;
}
</style> 