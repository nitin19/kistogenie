<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staffenrolementdashboard extends CI_Controller {

	var $logmode;

	function __construct(){

        parent::__construct();

        if( $this->authorize->is_user_logged_in() == false ){

			$this->session->set_flashdata('error', 'Please login first.');

			redirect(base_url());

		   }

		$this->logmode	= $this->session->userdata('log_mode');

        $this->load->model(array('login_model','authorization_model', 'staffenrolementdashboard_model'));

		$this->load->database();

        $this->load->library('session');

    }

	public function index(){

		

		 $data 							= array();

		 $school_id 					= $this->session->userdata('user_school_id');

		 $user_id						= $this->session->userdata('user_id');

		 $data['branches'] 				= $this->staffenrolementdashboard_model->getbranches($school_id);

		$data['info']				    = $this->staffenrolementdashboard_model->get_single_staff($user_id);
		
		$data['edu_info']				= $this->staffenrolementdashboard_model->display_education($user_id);
		
		$data['exp_info']				= $this->staffenrolementdashboard_model->display_experience($user_id);

		$data['title']	      			= "Staff enrolement dashboard";

		$data['error']					= $this->session->flashdata('error');

		$data['logmode']				= $this->logmode;

		$data['success']				= $this->session->flashdata('success');

		

		$this->load->view('header_enrollment');

		$this->load->view('staffenrolement/staff_enrolement_dashboard', $data);

		$this->load->view('footer');



	}  

	
	 public function edit_staff($sid){

		 $postURL	= $this->createPostURL($_GET);

		 if($sid==NULL){

			$this->session->set_flashdata('error', 'Select staff first.');

			redirect(base_url()."staffenrolementdashboard");exit;

		   }

		$whereuser						= array('id'=>$sid);

		$wherestaff						= array('user_id'=>$sid);

		$userdata 						= array();

		$staffdata 						= array();						

		$data							= array();

		$data['title']	      			= "Edit Account Details";

		$data['mode']					= "Edit";

			/*if (trim($this->input->post('password')) != '') {

                $userdata['password'] = trim($this->input->post('password'));

            }*/

			

			if (trim($this->input->post('email')) != '') {

                $userdata['email'] = trim($this->input->post('email'));

            }

			

			if($_FILES['profile_image']!='') {

				$config['upload_path']   = './uploads/staff/'; 

				$config['allowed_types'] =  'gif|jpg|png|jpeg';

				$this->load->library('upload', $config);

				$this->upload->initialize($config);

				if($this->upload->do_upload("profile_image")){

				$data = array('upload_data' => $this->upload->data());

				$image_name = $data['upload_data']['file_name'];

				if($_FILES['profile_image']['error'] == 0 ){

					$file_name					= $data['upload_data']['file_name'];

					$userdata['profile_image']		= $file_name;

					 }

				  }

			   }

			

			 $userdata['updated_date'] = date('Y-m-d H:i:s');

			if (trim($this->input->post('staff_fname')) != '') {
				
				$fname= trim($this->input->post('staff_fname'));
                $staffdata['staff_fname'] = trim($this->input->post('staff_fname'));
            }

			

			if (trim($this->input->post('staff_lname')) != '') {
				$lname= trim($this->input->post('staff_lname'));
                $staffdata['staff_lname'] = trim($this->input->post('staff_lname'));

            }

			

			if (trim($this->input->post('staff_title')) != '') {

                $staffdata['staff_title'] = trim($this->input->post('staff_title'));

            }

			

			if (trim($this->input->post('staff_dob')) != '') {

                  $dob = trim($this->input->post('staff_dob'));

				  $staffdata['staff_dob'] = date('Y-m-d', strtotime($dob));

            }

			if (trim($this->input->post('staff_telephone')) != '') {

                $staffdata['staff_telephone'] = trim($this->input->post('staff_telephone'));

            }

			if (trim($this->input->post('staff_address')) != '') {

                $staffdata['staff_address'] = trim($this->input->post('staff_address'));

            }

			if (trim($this->input->post('staff_address1')) != '') {

                $staffdata['staff_address_1'] = trim($this->input->post('staff_address1'));

            }

			if (trim($this->input->post('staff_education_qualification')) != '') {

                $staffdata['staff_education_qualification'] = trim($this->input->post('staff_education_qualification'));

            }

			if (trim($this->input->post('staff_personal_summery')) != '') {

                $staffdata['staff_personal_summery'] = trim($this->input->post('staff_personal_summery'));

            }
			
			$school_id 			= $this->session->userdata('user_school_id');
			
			$school				= $this->staffenrolementdashboard_model->get_school_info($school_id);
			
			$schoolname			= $school->school_name;
			
			$schoolemail		= $school->school_email;	
			
			$email				= trim($this->input->post('email'));

		if($this->staffenrolementdashboard_model->updateStaff($staffdata,$wherestaff)){

			$this->staffenrolementdashboard_model->updateUser($userdata,$whereuser);
			
			$to_superadmin		= $schoolemail;
			$subject_superadmin	= "Details of Registered Users";
			$message_superadmin	= "<html><body>";
			$message_superadmin	= "<font face='arial'>";
			$message_superadmin	.= "Hello".$schoolname." ,<br />";
			$message_superadmin	.= "<b>A new user " .$fname.' '.$lname. " has been registered successfully during first phase</b><br />";
			$message_superadmin	.= "<b>He/She Fulfill its details:</b> <br />";
			$message_superadmin	.= "<b>View its profile for futher process. <br />";
			
			$message_superadmin	.= "<br />Thanks.<br />";
			$message_superadmin	.= "</font>";
			$message_superadmin .= "</body></html>";
			
			$this->email->set_header('MIME-Version', '1.0; charset=utf-8');
			$this->email->set_header('Content-type', 'text/html');
			
			
			
			$this->email->from($email, 'Olivetree Study');
			$this->email->to($to_superadmin);
			$this->email->subject($subject_superadmin);
			$this->email->message($message_superadmin);
			$result = $this->email->send();
			
			
			
			
			/*$to			= $schoolemail;
			
			$subject	= "Enrolement Staff Reply";
			
			
			
			$message	= "Hello ".$schoolname.",<br />";
			
			$message	.= "<b> Staff has filled their details.</b><br />";
			
			$message	.= "<b> Review their profile. </b> <br />";
			
			$message	.= "<br />Thanks.<br />";
			
			
			
			$mailheader  = 'From:'.$email. "\r\n" .'Reply-To: admin@kistogenie.com' . "\r\n" .'X-Mailer: PHP/' . phpversion();
			
			$mailheader .= 'MIME-Version: 1.0' . "\r\n";
			$mailheader .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			
			$this->authorize->send_email($to,$subject,$message);*/


			$this->session->set_flashdata('success', 'Data has been updated successfully.');



			redirect(base_url()."staffenrolementdashboard");exit;

		 } else {

			$this->session->set_flashdata('error', 'Some problem exists. Data has not been updated.');

			redirect(base_url()."staffenrolementdashboard");exit;

		   }

	  }
	  
	   public function save_education(){
	
		  $edudata['staff_enrol_id']	= trim($this->input->post('staff_enrol_id'));
		  $edudata['degree']			= trim($this->input->post('degree'));
		  $edudata['start_year']		= trim($this->input->post('start_year'));
		  $edudata['end_year']			= trim($this->input->post('end_year'));
		  $edudata['university']		= trim($this->input->post('university'));
		  $edudata['percentage']		= trim($this->input->post('percentage'));
		   
		  $edu_id 						= $this->staffenrolementdashboard_model->save_education($edudata);
		  
		  $staff_enrol_id				= $this->input->post('staff_enrol_id');
		  if($staff_enrol_id)
		  {
			$staff_educationdata		= $this->staffenrolementdashboard_model->display_education($staff_enrol_id);
			echo' <thead>
				  <tr class="headings">
					  <th class="column5">Degree</th>
					  <th class="column5">University</th>
					  <th class="column3">Start Year</th>
					  <th class="column3">End Year</th>
					  <th class="column2">Grade</th>
					  <th class="column1">Action</th>
					  
				  </tr>
			  </thead>';
			
			foreach($staff_educationdata as $educationdata){
				echo '<tbody > <tr class="familydata educationlisting">
						<td class="column5 numeric">'.$educationdata->degree.'</td>
						<td class="column5">'.$educationdata->university.'</td>
						<td class="column3">'.$educationdata->start_year.'</td>
						<td class="column3">'.$educationdata->end_year.'</td>
						<td class="column2">'.$educationdata->percentage.'</td>
						<td class="column1">
							<a class="change" onclick="delConfirm('.$educationdata->id.')" style="cursor: pointer;">
							<i class="fa fa-trash-o" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
							</a>
						</td>
                        </tr> </tbody>';
						}
		  }		   
	   }
	   
	     
	   public function save_experience(){
		
		  $expdata['staff_enrol_id']	= trim($this->input->post('staff_enrol_id'));
		  $expdata['company_name']			= trim($this->input->post('companyname'));
		  $expdata['exp_startyear']		= trim($this->input->post('exp_startyear'));
		  $expdata['exp_endyear']			= trim($this->input->post('exp_endyear'));
		  $expdata['position']		= trim($this->input->post('salary'));
		
		   
		  $exp_id 						= $this->staffenrolementdashboard_model->save_experience($expdata);
		   $staff_enrol_id				= $this->input->post('staff_enrol_id');
		  if($staff_enrol_id)
		  {
			$staff_experiencedata	=	$this->staffenrolementdashboard_model->display_experience($staff_enrol_id);
			echo'<thead>
				  <tr class="headings">
					  <th class="column7">Company Name</th>
					  <th class="column5">Position</th>
					  <th class="column3">Start Year</th>
					  <th class="column3">End Year</th>
					  
					  <th class="column2">Action</th>					  
				  </tr>
			  </thead>';
			
			foreach($staff_experiencedata as $experiencedata){
				echo '<tbody > <tr class="familydata educationlisting">
						<td class="column7 numeric">'.$experiencedata->company_name.'</td>
						<td class="column5">'.$experiencedata->position.'</td>
						<td class="column3">'.$experiencedata->exp_startyear.'</td>
						<td class="column3">'.$experiencedata->exp_endyear.'</td>
						
						
						<td class="column2">
							<a class="change" onclick="delConfirm('.$experiencedata->id.')" style="cursor: pointer;">
							<i class="fa fa-trash-o" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
							</a>
						</td>
                        </tr> </tbody>';
						}
		  }		   
	   }
	   
public function delete($id){
		if($this->staffenrolementdashboard_model->delete($id)){
			$this->session->set_flashdata('success', 'Education record has been deleted successfully.');
			redirect(base_url()."staffenrolementdashboard");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Try again.');
			redirect(base_url()."staffenrolementdashboard");exit;
		}
	 }
	  
public function delete_exp($id){
		if($this->staffenrolementdashboard_model->delete_exp($id)){
			$this->session->set_flashdata('success', 'Experience record has been deleted successfully.');
			redirect(base_url()."staffenrolementdashboard");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Try again.');
			redirect(base_url()."staffenrolementdashboard");exit;
		}
	 }
	  
	  function createPostURL($get){

		$start	= $this->uri->segment(4,0);

		$odrby	= $this->uri->segment(5,0);

		$dirc	= $this->uri->segment(6,"asc");

		$postURL	= "$start/$odrby/$dirc/?";

		foreach($get as $key=>$val){

			$postURL	.="$key=$val&";

		}

		return  $postURL;exit;

	}

}



