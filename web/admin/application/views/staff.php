<?php
$this->load->view('common/header');
?>
<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Table</a></li>
                            <li><i class="fa fa-table"></i><a href="<?php echo base_url('Dashboard');?>/view_staff">Staff table</a></li>
                            
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Student Table</strong>
                        </div>
                        <div class="card-body">
                            
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Branch</th>
                        <th>Education</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                         $i=1;
                        foreach ($viewstaff_detail as $staff) {
                        //print_r($student);
                        
                        ?>
                        
                      <tr id="std_<?php echo $i;?>">
                        <td><?php echo $staff['today_date'];?></td>
                        <td><?php echo $staff['staff_first'];?> <?php echo $staff['staff_last'];?></td>
                        <td><?php echo $staff['branch'];?></td>
                        <td><?php echo $staff['education'];?></td>
                        <td><?php echo $staff['email'];?></td>
                        <td><?php echo $staff['phone'];?></td>
                        <td>
                            <ol class="breadcrumb text-right">
                            <li><i class="fa fa-table"></i><a href="<?php echo base_url('Dashboard');?>/view_staffdetail/<?php echo $staff['id'];?>">view detail</a></li> 
                            <li><i class="fa fa-table"></i><a href="<?php echo base_url('Dashboard');?>/delete_staff/<?php echo $staff['id'];?>">Delete</a></li>
                            </ol>
                        </td>
                      </tr>
                    
                    <?php
                        $i++;
                      }
                     ?>
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->
    
<?php
$this->load->view('common/footer');
?>