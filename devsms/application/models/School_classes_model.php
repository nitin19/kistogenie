<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class School_classes_model extends CI_Model {
	
	var $tbl	= "classes";
	var $users = "users";
	var $branch = "branch";
	var $fee_band = "fee_band";
	var $student = "student";
	var $school = "school";
	
    function __construct() {
        parent::__construct();
    }
	
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		//$this->db->where('school_id', $school_id);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	
	function getschoolclasses($school_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		//$this->db->where('school_id', $school_id);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	function check_class_name($branch_id, $class_name, $school_id) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch_id,"class_name"=>$class_name));
		  $query = $this->db->get();
		  $this->db->last_query(); 
          return $query->result();   
	}
	
	function check_editedclass_name($branch_id, $class_name, $school_id, $editedClassid) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch_id,"class_name"=>$class_name));
		$this->db->where('class_id !=', $editedClassid);
		  $query = $this->db->get();
		  $this->db->last_query(); 
          return $query->result();   
	}
	  
	  
	  function getRows($where=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			//$this->db->like(array($this->tbl.'.student_fname'=>$word_search));
			 $this->db->where("(class_name LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	   function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			//$this->db->like(array($this->tbl.'.student_fname'=>$word_search));
			 $this->db->where("(class_name LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("class_id", "desc"); 	
		   }
		
		     $this->db->limit($limit, $start);
             $query = $this->db->get();
		     $this->db->last_query(); 
             return  $query->result();

	}
	
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	   function insertClasses($classdata){
		if($this->db->insert($this->tbl, $classdata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	   }
	   
	   function get_single_class($classid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('class_id', $classid);
          $query = $this->db->get();
          return  $query->result();
    }
	
	 function updateClasses($classdata,$where) {
		$this->db->where($where);
		if($this->db->update($this->tbl, $classdata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	  function check_deletedclass($school_id,$classid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		 // $this->db->where('class_id', $classid);
		  $this->db->where(array("school_id"=>$school_id,"class_id"=>$classid));
          $query = $this->db->get();
          return  $query->row();
      }
	  
	  /*function check_student_class($school_id,$branch_id,$classid) {
          $this->db->select('*');
          $this->db->from($this->student);
		  $this->db->where(array("school_id"=>$school_id,"student_school_branch"=>$branch_id,"student_class_group"=>$classid));
          $query = $this->db->get();
		  echo $this->db->last_query();
          return  $query->result();
       }*/
	   
	    function check_student_class($school_id,$branch_id,$classid) {
          $this->db->select('*');
          $this->db->from($this->student);
		  $this->db->join('users', 'users.id = '.$this->student.'.user_id','INNER'); 
		 // $this->db->where('class_id', $classid);
		  $this->db->where(array($this->student.".school_id"=>$school_id,$this->student.".student_school_branch"=>$branch_id,$this->student.".student_class_group"=>$classid,"users.is_active"=>'1',"users.is_deleted"=>'0'));
          $query = $this->db->get();
		   $this->db->last_query();
          return  $query->result();
         }
	   
	  function delete($classid){
		$where		= array('class_id' => $classid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		 else
			return false;
	   }
	
}



 
