<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Students extends CI_Controller {

	 var $logmode;

	function __construct(){

       

        parent::__construct();

        if( $this->authorize->is_user_logged_in() == false ){

			$this->session->set_flashdata('error', 'Please login first.');

			redirect(base_url());

		   }

		$this->logmode	= $this->session->userdata('log_mode');

        $this->load->model(array('login_model','authorization_model','student_model'));

		$this->load->database();

        $this->load->library('session');

		$this->load->library('form_validation');

		$this->load->library('image_lib');

    }


	public function index()	{
	    $branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;
		$status_search					= isset($_GET['status'])?$_GET['status']:NULL; 
		$shortbyname_search				= isset($_GET['shortbyname'])?$_GET['shortbyname']:NULL;
		$sby							= isset($_GET['sby'])?$_GET['sby']:NULL;
		$word_search_org				= isset($_GET['seachword'])?$_GET['seachword']:NULL;
		$word_search 					= trim($word_search_org);
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		$search_condition['users.user_type']	= 'student';
		//$search_condition['users.is_deleted']	= '0';
		if($branch_search!=''){
			$search_condition['student.student_school_branch']	= $branch_search;
		}
		//if($class_search!=''){
			//$search_condition['student.student_class_group']	= $class_search;
		//}
		if($status_search!=''){
            $search_condition['users.is_active']	= $status_search;
		  }
		$data							= array();
		$school_id						= $this->session->userdata('user_school_id');
        $staff_branch_id				= $this->session->userdata('staff_branch_id');
		$user_type						= $this->session->userdata('user_type');		
		$data['branches'] 				= $this->student_model->getbranches($school_id);
		$data['sclasses'] 				= $this->student_model->getschoolclasses($school_id);		
    	$data['title']	      			= "Students";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");	
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 10;
		}
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'students/index';
	  //$config['total_rows'] 			= $this->student_model->getRows($search_condition,$school_id,$word_search);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] 		= "/?branch=$branch_search&class=$class_search&status=$status_search&shortbyname=$shortbyname_search&seachword=$word_search&sby=$sby&perpage=$perpage"; 
		if($shortbyname_search!='') {
			$odr =   "student.student_fname";
		} else {
			$odr =   "student.user_id";
		   }
		if($shortbyname_search!='') {
			 $odr =   "student.student_fname";
			 if($shortbyname_search=='0') {
				  $dirc	= "asc";
			  } elseif($shortbyname_search=='1') {
				  $dirc	= "desc";
			     }
		 } else {
			$odr =   "student.user_id";
			 $dirc	= "desc";
		   }
		 if($user_type=='teacher'){
			 if($staff_branch_id!=''){
					$config['total_rows'] = $this->student_model->getstaffRows($search_condition,$class_search,$word_search,$staff_branch_id,$sby);	
					$data_rows	= $this->student_model->getstaffstudent($search_condition,$class_search,$start,$perpage,$odr,$dirc,$staff_branch_id,$word_search,$sby);	
			 }
			 else{
					$config['total_rows'] = $this->student_model->getstaffRows($search_condition,$class_search,$word_search,$staff_branch_id,$sby);	
					$data_rows	= $this->student_model->getstaffstudent($search_condition,$class_search,$start,$perpage,$odr,$dirc,$staff_branch_id,$word_search,$sby);	
			 }
		 }
		 else if($user_type=='admin'){
					$config['total_rows'] = $this->student_model->getRows($search_condition,$class_search,$school_id,$word_search,$sby);
					$data_rows	= $this->student_model->getPagedData($search_condition,$class_search,$start,$perpage,$odr,$dirc,$school_id,$word_search,$sby);
		 }
		$this->pagination->initialize($config);
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['class_search']			= $class_search;
		$data['status_search']			= $status_search;
		$data['shortbyname_search']		= $shortbyname_search;
		$data['word_search']		    = $word_search;
		$data['sby']		    		= $sby;
		$data['PerPage']				= $PerPage;
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		$this->load->view('header');
		$this->load->view('student/students', $data);
		$this->load->view('footer');
	}

/* 	public function random_username() {

		$firstPart 		= 	$this->input->post['student_fname'];

		$secondPart 	=	$this->input->post['student_lname']; 

		$nrRand = rand(0, 100);

		$username = trim($firstPart).trim($secondPart).trim($nrRand);

		echo $username;

		exit();

}*/


	 public function add_new()

	{    

	    $data = array();

		$school_id = $this->session->userdata('user_school_id');

		 $data['nationalities'] = $this->student_model->getnationality();

	     $data['countries'] = $this->student_model->getcountry();

		 $data['religions'] = $this->student_model->getreligion();

		 $data['ethnicorigins'] = $this->student_model->getethnicorigin();

		 $data['branches'] = $this->student_model->getbranches($school_id);

		// $data['feebands'] = $this->student_model->getfeebands($school_id);

		$this->load->view('header');

		$this->load->view('student/new_student', $data);

		$this->load->view('footer');

	  }

	  

	public function check_username()

	{

		 $Uname = trim($_POST['username']); 

	     $checkstudentusername = $this->student_model->check_student_username($Uname);

		  $countstudentname = count($checkstudentusername);

		 if( $countstudentname > 0 ) {

			echo 'false';

	        } else {

		    echo 'true';

	        }

	  }

/*	  public function check_email()

	{

		 $email = trim($_POST['email']); 

	      $countstudentemail = $this->student_model->check_student_email($email);

		 if( $countstudentemail > 0 ) {

			echo 'false';

	        } else {

		    echo 'true';

	        }

	  }

	  */

	  public function check_sibling()
	{     

		    $school_id = $this->session->userdata('user_school_id');

			$Favalue = trim($_POST['Favalue']); 
			
			$stuEmail = trim($_POST['stuEmail']); 

	        $siblings = $this->student_model->getsibling($Favalue,$stuEmail,$school_id);

			foreach($siblings as $sibling) {

 			 echo '<option  selected value="'.$sibling->student_id.'">'.$sibling->student_fname.' '.$sibling->student_lname.'</option>';

			}

	  }

	   public function check_classes()

	{     

	      $staff_class_id = $this->session->userdata('staff_class_id');

          $user_type= $this->session->userdata('user_type');

          if($user_type=='teacher' && $staff_class_id!=''){

          $staff_class = explode(',',$staff_class_id);

          foreach($staff_class as $staff_class_ids) {

			  $sclasses =$this->student_model->getstaff_classes($staff_class_ids);

			  echo '<option value="'.$sclasses->class_id.'">'.$sclasses->class_name.'</option>';

		  }

		  }elseif($user_type=='admin'){

		    $school_id = $this->session->userdata('user_school_id');

			$schoolbranch = trim($_POST['schoolbranch']); 

	        $getclasses = $this->student_model->getclasses($school_id,$schoolbranch);

			foreach($getclasses as $getclass) {

 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';

			}

		  }else{

			  echo '<option value=""></option>';

		  }

	  }

	  

	   public function check_feeband()

	{     
		    $school_id = $this->session->userdata('user_school_id');

			$schoolclass = implode(',',$_POST['schoolclass']); 

			$BranchID = trim($_POST['BranchID']); 
		/*print_r($school_id); echo "-";
		print_r($schoolclass); echo "-";
		print_r($BranchID);echo "-";*/
		//exit;
	        $feebands = $this->student_model->getfeebands($school_id,$BranchID,$schoolclass);
		/*print_r($feebands);
		exit;*/
			 $f = 2222;

			 $HTML = '';

  foreach($feebands as $feeband) {
				
	/* $HTML.= '<div class="radiobtn radioclass">
   <input type="radio" name="student_fee_band" id="radio'.$f.'" value="'.$feeband->fee_band_id.'" required="required">
   <label for="radio'.$f.'">'.$feeband->fee_band_price.'   '.$feeband->fee_band_description.'</label>
        <div class="check"></div>
          </div>';*/
		  
		  $clasName = $this->student_model->getclassName($feeband->class_id);
		  
		  $HTML.= '<div class="form-group">
    <label class="col-sm-6 control-label">'.$clasName->class_name.'</label>
    <div class="col-sm-6 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="student_fee_band[]" id="fb'.$f.'" value="'.$feeband->fee_band_id.'" required="required">
    			<label for="fb'.$f.'"><span class="checkbox">'.$feeband->fee_band_price.'   '.$feeband->fee_band_description.'</span></label>
                </div>
        </div>
     </div>';

		   $f++;

		}

		echo $HTML;

	  }

	    public function add_new_student(){

		$userdata = array();

		$studentdata = array();						

		$data							= array();

		$data['title']	      			= "Add New Student";

		$data['mode']					= "Add";

            if (trim($this->input->post('username')) != '') {

                $userdata['username'] = trim($this->input->post('username'));

            }

			if (trim($this->input->post('password')) != '') {

                $userdata['password'] = trim($this->input->post('password'));

            }

			if (trim($this->input->post('email')) != '') {

                $userdata['email'] = trim($this->input->post('email'));

            }

			$userdata['school_id'] = $this->session->userdata('user_school_id');

			$userdata['created_by'] = $this->session->userdata('user_name');

			$userdata['user_type'] = 'student';

			$userdata['is_active'] = '1';

			$userdata['created_date'] = date('Y-m-d H:i:s');;

			$userdata['updated_date'] = date('Y-m-d H:i:s');

			$studentdata['school_id'] = $this->session->userdata('user_school_id');

			if (trim($this->input->post('student_fname')) != '') {

                $studentdata['student_fname'] = trim($this->input->post('student_fname'));

            }

			if (trim($this->input->post('student_lname')) != '') {

                $studentdata['student_lname'] = trim($this->input->post('student_lname'));

            }

			if (trim($this->input->post('student_gender')) != '') {

                $studentdata['student_gender'] = trim($this->input->post('student_gender'));

            }

			if (trim($this->input->post('student_dob')) != '') {

                  $dob = trim($this->input->post('student_dob'));

				  $studentdata['student_dob'] = date('Y-m-d', strtotime($dob));

            }

			

			if (trim($this->input->post('student_nationality')) !='') {

                $studentdata['student_nationality'] = trim($this->input->post('student_nationality'));

            }

				

			if (trim($this->input->post('student_country_of_birth')) != '') {

                $studentdata['student_country_of_birth'] = trim($this->input->post('student_country_of_birth'));

            }

			

			if (trim($this->input->post('student_first_language')) != '') {

                $studentdata['student_first_language'] = trim($this->input->post('student_first_language'));

            }

			

			if (trim($this->input->post('student_other_language')) != '') {

                $studentdata['student_other_language'] = trim($this->input->post('student_other_language'));

            }

			

			if (trim($this->input->post('student_religion')) != '') {

                $studentdata['student_religion'] = trim($this->input->post('student_religion'));

            }

			

			if (trim($this->input->post('student_ethnic_origin') != '')) {

                $studentdata['student_ethnic_origin'] = trim($this->input->post('student_ethnic_origin'));

            }

			

			if (trim($this->input->post('student_telephone')) != '') {

                $studentdata['student_telephone'] = trim($this->input->post('student_telephone'));

            }

			

			if (trim($this->input->post('student_address')) != '') {

                $studentdata['student_address'] = trim($this->input->post('student_address'));

            }

			

			if (trim($this->input->post('student_address_1')) != '') {

                $studentdata['student_address_1'] = trim($this->input->post('student_address_1'));

            }

			

			if (trim($this->input->post('student_father_name')) != '') {

                $studentdata['student_father_name'] = trim($this->input->post('student_father_name'));

            }

			

			if (trim($this->input->post('student_father_occupation')) != '') {

                $studentdata['student_father_occupation'] = trim($this->input->post('student_father_occupation'));

            }

			

			if (trim($this->input->post('student_father_mobile')) !='' ) {

                $studentdata['student_father_mobile'] = trim($this->input->post('student_father_mobile'));

            }

				

			if (trim($this->input->post('student_father_email') != '')) {

                $studentdata['student_father_email'] = trim($this->input->post('student_father_email'));

            }

			if (trim($this->input->post('student_father_address') != '')) {

                $studentdata['student_father_address'] = trim($this->input->post('student_father_address'));

            }

			

			if (trim($this->input->post('student_mother_name')) != '') {

                $studentdata['student_mother_name'] = trim($this->input->post('student_mother_name'));

            }

			

			if (trim($this->input->post('student_mother_occupation')) != '') {

                $studentdata['student_mother_occupation'] = trim($this->input->post('student_mother_occupation'));

            }

			

			if (trim($this->input->post('student_mother_mobile')) != '') {

                $studentdata['student_mother_mobile'] = trim($this->input->post('student_mother_mobile'));

            }

			

			if (trim($this->input->post('student_mother_email')) != '') {

                $studentdata['student_mother_email'] = trim($this->input->post('student_mother_email'));

            }

			if (trim($this->input->post('student_mother_address')) != '') {

                $studentdata['student_mother_address'] = trim($this->input->post('student_mother_address'));

            }

			

			if (trim($this->input->post('student_emergency_name')) != '') {

                $studentdata['student_emergency_name'] = trim($this->input->post('student_emergency_name'));

            }

			

			

			if (trim($this->input->post('student_emergency_relationship')) != '') {

                $studentdata['student_emergency_relationship'] = trim($this->input->post('student_emergency_relationship'));

            }

			

			if (trim($this->input->post('student_emergency_mobile')) != '') {

                $studentdata['student_emergency_mobile'] = trim($this->input->post('student_emergency_mobile'));

            }

			

			if (trim($this->input->post('student_doctor_name')) != '') {

                $studentdata['student_doctor_name'] = trim($this->input->post('student_doctor_name'));

            }

			

			if (trim($this->input->post('student_doctor_mobile')) != '') {

                $studentdata['student_doctor_mobile'] = trim($this->input->post('student_doctor_mobile'));

            }

			if (trim($this->input->post('student_doctor_surgery_address')) != '') {

                $studentdata['student_doctor_surgery_address'] = trim($this->input->post('student_doctor_surgery_address'));

            }

			

			if (trim($this->input->post('student_helth_notes')) != '') {

                $studentdata['student_helth_notes'] = trim($this->input->post('student_helth_notes'));

            }

			

			if (trim($this->input->post('student_allergies')) != '') {

                $studentdata['student_allergies'] = trim($this->input->post('student_allergies'));

            }

			

			

			if (trim($this->input->post('student_other_comments')) != '') {

                $studentdata['student_other_comments'] = trim($this->input->post('student_other_comments'));

            }

			

			if (trim($this->input->post('student_school_branch')) != '') {

                $studentdata['student_school_branch'] = trim($this->input->post('student_school_branch'));

            }

			if (count(array_filter($this->input->post('student_class_group'))) > 0 ) {

                $studentdata['student_class_group'] = implode(',',$this->input->post('student_class_group'));

            }

			if (count(array_filter($this->input->post('student_fee_band'))) > 0 ) {

                $studentdata['student_fee_band'] = implode(',',$this->input->post('student_fee_band'));

            }

			if (trim($this->input->post('student_application_date')) != '') {

				$application_date = trim($this->input->post('student_application_date'));

                $studentdata['student_application_date'] = date('Y-m-d', strtotime($application_date));

            }

			

			

			if (trim($this->input->post('student_application_comment')) != '') {

                $studentdata['student_application_comment'] = trim($this->input->post('student_application_comment'));

            }

			

			if (trim($this->input->post('student_enrolment_date')) != '') {

				$enrolment_date = trim($this->input->post('student_enrolment_date'));

                $studentdata['student_enrolment_date'] = date('Y-m-d', strtotime($enrolment_date));

            }

			

			if (trim($this->input->post('student_leaving_date')) != '') {

				$leaving_date = trim($this->input->post('student_leaving_date'));

                $studentdata['student_leaving_date'] = date('Y-m-d', strtotime($leaving_date));

            }

			

			if (trim($this->input->post('student_family_address')) != '') {

                $studentdata['student_family_address'] = trim($this->input->post('student_family_address'));

            }

			

			if ( count($this->input->post('student_sibling')) > 0 ) {

                $studentdata['student_sibling'] = implode(',',$this->input->post('student_sibling'));

            }
			

			$Uname = trim($_POST['username']); 

			$chkstudentname = $this->student_model->check_student_username($Uname);

			$countstudentname = count($chkstudentname);

			

			if( $countstudentname > 0 ) {

				$this->session->set_flashdata('error', 'Student name already exists. Student has not been added.');

				redirect(base_url()."students/add_new");exit;

	       } else {

			   

			$config['upload_path']   = './uploads/student/'; 

			$config['allowed_types'] =  'gif|jpg|png|jpeg';

            $this->load->library('upload', $config);

			$this->upload->initialize($config);

			if($this->upload->do_upload("profile_image")){

		    $data = array('upload_data' => $this->upload->data());

	        $image_name = $data['upload_data']['file_name'];

		    if($_FILES['profile_image']['error'] == 0 ){

				$file_name					= $data['upload_data']['file_name'];

				$userdata['profile_image']		= $file_name;

			    }

			}

			

			  $last_id_student =  $this->student_model->insertUser($userdata);

		 

		  if($last_id_student){

			  

			    $studentdata['user_id'] = $last_id_student;

				

			  $files = $_FILES;



	         // $FileCount = count(array_filter($_FILES['student_certificates']['name']));

 

              $count = count(array_filter($_FILES['student_certificates']['name']));

			  if( $count > 0 ) {

              for($i=0; $i<$count; $i++)

                {

                $_FILES['student_certificates']['name']= time().$files['student_certificates']['name'][$i];

                $_FILES['student_certificates']['type']= $files['student_certificates']['type'][$i];

                $_FILES['student_certificates']['tmp_name']= $files['student_certificates']['tmp_name'][$i];

                $_FILES['student_certificates']['error']= $files['student_certificates']['error'][$i];

                $_FILES['student_certificates']['size']= $files['student_certificates']['size'][$i];

                $config['upload_path'] = './uploads/student_certificates/';

                $config['allowed_types'] = '*';

                $this->load->library('upload', $config);

                $this->upload->initialize($config);

                $this->upload->do_upload("student_certificates");

                $fileName = $_FILES['student_certificates']['name'];

                $images[] = $fileName;

                }

                  $fileName = implode(',',$images);

		          $studentdata['student_certificates']		=  $fileName;

				  

			  }

                 /* if($FileCount!=0){
		                $studentdata['student_certificates']		=  $fileName;
		                } else {
	                    $studentdata['student_certificates']		=  "";
		             }*/
		
		
	          $this->student_model->insertStudent($studentdata);
			

				$this->session->set_flashdata('success', 'Student has been added successfully.');
		
				 redirect(base_url()."students/add_new"); exit;

			} else {

				$this->session->set_flashdata('error', 'Some problem exists. Student has not been added.');

				redirect(base_url()."students/add_new");exit;

			   }

	         }

		$data['logmode']			= $this->logmode;

		$data['error']				= $this->session->flashdata('error');

		$data['success']			= $this->session->flashdata('success');

		$this->load->view('header');

		$this->load->view('student/new_student',$data);

		$this->load->view('footer');

	}

	public function delete($id){

		$del_student=$this->student_model->delete($id);

		if($del_student=='1'){

			$this->session->set_flashdata('success', 'Student has been deleted successfully.');

			redirect(base_url()."students");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."students");exit;

		}

	}

	function deactivate($sid){

		$where		= array('id'=>$sid);

		$data		= array('is_active'=>'0');

		if($this->student_model->update($data,$where)){

			$this->session->set_flashdata('success', 'Student has been updated successfully.');

			redirect(base_url()."students");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Student has not been updated.');

			redirect(base_url()."students");exit;

		}

	}

	function activate($sid){

		$where		= array('id'=>$sid);

		$data		= array('is_active'=>'1');

		if($this->student_model->update($data,$where)){

			$this->session->set_flashdata('success', 'Student has been updated successfully.');

			redirect(base_url()."students");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Student has not been updated.');

			redirect(base_url()."students");exit;

		}

	}

	public function view_student($sid) {

		$id = $this->uri->segment(3);

		$data = array();

		if($sid==NULL){

			$this->session->set_flashdata('error', 'Select student first.');

			redirect(base_url()."students");exit;

		   }

		 $attendeceinterval = 'attendance_date BETWEEN (CURDATE() - INTERVAL 30 DAY) AND CURDATE()';

		$data['studentattendence']					= $this->student_model->student_attendence($attendeceinterval, $id);

		

		$data['info']						= $this->student_model->get_single_student($sid);

							

		$data['title']	      			= "Account detail";

		$data['error']					= $this->session->flashdata('error');

		$data['logmode']				= $this->logmode;

		$data['success']				= $this->session->flashdata('success');

	     

		$this->load->view('header');

        $this->load->view('student/student_account_detail',$data);

	    $this->load->view('footer');

    }

	public function edit($sid){

		if($sid==NULL){

			$this->session->set_flashdata('error', 'Select student first.');

			redirect(base_url()."students");exit;

		   }

		$data = array();

		

		 $school_id = $this->session->userdata('user_school_id');

		 $data['nationalities'] = $this->student_model->getnationality();

	     $data['countries'] = $this->student_model->getcountry();

		 $data['religions'] = $this->student_model->getreligion();

		 $data['ethnicorigins'] = $this->student_model->getethnicorigin();

		 $data['branches'] = $this->student_model->getbranches($school_id);
		 
		 $data['payment_receive_email'] = $this->student_model->get_payment_email_info($school_id);
		 
		 $data['request_payment_email'] = $this->student_model->get_request_email_info($school_id);
	

		$data['info']					= $this->student_model->get_single_student($sid);

		$data['title']	      			= "Account detail";

		$data['error']					= $this->session->flashdata('error');

		$data['logmode']				= $this->logmode;

		$data['success']				= $this->session->flashdata('success');

		$this->load->view('header');

        $this->load->view('student/edit_account_detail',$data);

	    $this->load->view('footer');

	}

	 public function edit_student($sid){

		 $postURL	= $this->createPostURL($_GET);

		 if($sid==NULL){

			$this->session->set_flashdata('error', 'Select student first.');

			redirect(base_url()."students");exit;

		   }

		

		$whereuser						= array('id'=>$sid);

		$wherestudent					= array('user_id'=>$sid);

		

		$userdata 						= array();

		$studentdata 					= array();						

		$data							= array();

		$data['title']	      			= "Edit Account Details";

		$data['mode']					= "Edit";


			if (trim($this->input->post('password')) != '') {

                $userdata['password'] = trim($this->input->post('password'));

            }

			

			if (trim($this->input->post('email')) != '') {

                $userdata['email'] = trim($this->input->post('email'));

            }

			

			if($_FILES['profile_image']!='') {

				$config['upload_path']   = './uploads/student/'; 

				$config['allowed_types'] =  'gif|jpg|png|jpeg';

				$this->load->library('upload', $config);

				$this->upload->initialize($config);

				if($this->upload->do_upload("profile_image")){

				$data = array('upload_data' => $this->upload->data());

				$image_name = $data['upload_data']['file_name'];

				if($_FILES['profile_image']['error'] == 0 ){

					$file_name					= $data['upload_data']['file_name'];

					$userdata['profile_image']		= $file_name;

					 }

				  }

			   }

			

			 $userdata['updated_date'] = date('Y-m-d H:i:s');

			

			 

			if (trim($this->input->post('student_fname')) != '') {
                $fname=trim($this->input->post('student_fname'));
                $studentdata['student_fname'] = trim($this->input->post('student_fname'));

            }

			

			if (trim($this->input->post('student_lname')) != '') {
                $lname=trim($this->input->post('student_lname'));
                $studentdata['student_lname'] = trim($this->input->post('student_lname'));

            }

			

			if (trim($this->input->post('student_gender')) != '') {

                $studentdata['student_gender'] = trim($this->input->post('student_gender'));

            }

			

			if (trim($this->input->post('student_dob')) != '') {

                  $dob = trim($this->input->post('student_dob'));

				  $studentdata['student_dob'] = date('Y-m-d', strtotime($dob));

            }

			

			if (trim($this->input->post('student_nationality')) !='') {

                $studentdata['student_nationality'] = trim($this->input->post('student_nationality'));

            }

				

			if (trim($this->input->post('student_country_of_birth')) != '') {

                $studentdata['student_country_of_birth'] = trim($this->input->post('student_country_of_birth'));

            }

			

			if (trim($this->input->post('student_first_language')) != '') {

                $studentdata['student_first_language'] = trim($this->input->post('student_first_language'));

            }

			

			if (trim($this->input->post('student_other_language')) != '') {

                $studentdata['student_other_language'] = trim($this->input->post('student_other_language'));

            }

			

			if (trim($this->input->post('student_religion')) != '') {

                $studentdata['student_religion'] = trim($this->input->post('student_religion'));

            }

			

			if (trim($this->input->post('student_ethnic_origin')) != '') {

                $studentdata['student_ethnic_origin'] = trim($this->input->post('student_ethnic_origin'));

            }

			

			if (trim($this->input->post('student_telephone')) != '') {

                $studentdata['student_telephone'] = trim($this->input->post('student_telephone'));

            }

			

			if (trim($this->input->post('student_address')) != '') {

                $studentdata['student_address'] = trim($this->input->post('student_address'));

            }

			

			if (trim($this->input->post('student_address_1')) != '') {

                $studentdata['student_address_1'] = trim($this->input->post('student_address_1'));

            }

			

			if (trim($this->input->post('student_father_name')) != '') {

                $studentdata['student_father_name'] = trim($this->input->post('student_father_name'));

            }

			

			if (trim($this->input->post('student_father_occupation')) != '') {

                $studentdata['student_father_occupation'] = trim($this->input->post('student_father_occupation'));

            }

			

			if (trim($this->input->post('student_father_mobile')) !='' ) {

                $studentdata['student_father_mobile'] = trim($this->input->post('student_father_mobile'));

            }

				

			if (trim($this->input->post('student_father_email')) != '') {

                $studentdata['student_father_email'] = trim($this->input->post('student_father_email'));

            }

			

			if (trim($this->input->post('student_mother_name')) != '') {

                $studentdata['student_mother_name'] = trim($this->input->post('student_mother_name'));

            }

			

			if (trim($this->input->post('student_mother_occupation')) != '') {

                $studentdata['student_mother_occupation'] = trim($this->input->post('student_mother_occupation'));

            }

			

			if (trim($this->input->post('student_mother_mobile')) != '') {

                $studentdata['student_mother_mobile'] = trim($this->input->post('student_mother_mobile'));

            }

			

			if (trim($this->input->post('student_mother_email')) != '') {

                $studentdata['student_mother_email'] = trim($this->input->post('student_mother_email'));

            }

			

			if (trim($this->input->post('student_emergency_name')) != '') {

                $studentdata['student_emergency_name'] = trim($this->input->post('student_emergency_name'));

            }

			

			

			if (trim($this->input->post('student_emergency_relationship')) != '') {

                $studentdata['student_emergency_relationship'] = trim($this->input->post('student_emergency_relationship'));

            }

			

			if (trim($this->input->post('student_emergency_mobile')) != '') {

                $studentdata['student_emergency_mobile'] = trim($this->input->post('student_emergency_mobile'));

            }

			

			if (trim($this->input->post('student_doctor_name')) != '') {

                $studentdata['student_doctor_name'] = trim($this->input->post('student_doctor_name'));

            }

			

			if (trim($this->input->post('student_doctor_mobile')) != '') {

                $studentdata['student_doctor_mobile'] = trim($this->input->post('student_doctor_mobile'));

            }

			

			if (trim($this->input->post('student_helth_notes')) != '') {

                $studentdata['student_helth_notes'] = trim($this->input->post('student_helth_notes'));

            }

			

			if (trim($this->input->post('student_allergies')) != '') {

                $studentdata['student_allergies'] = trim($this->input->post('student_allergies'));

            }

			

			

			if (trim($this->input->post('student_other_comments')) != '') {

                $studentdata['student_other_comments'] = trim($this->input->post('student_other_comments'));

            }

			

			if (trim($this->input->post('student_school_branch')) != '') {

                $studentdata['student_school_branch'] = trim($this->input->post('student_school_branch'));

            }

			
			if (count(array_filter($this->input->post('student_class_group'))) > 0 ) {
                $studentdata['student_class_group'] = implode(',',$this->input->post('student_class_group'));
            }

			if (count(array_filter($this->input->post('student_fee_band'))) > 0 ) {
                $studentdata['student_fee_band'] = implode(',',$this->input->post('student_fee_band'));
            }
			

			if (trim($this->input->post('student_application_date')) != '') {

				$application_date = trim($this->input->post('student_application_date'));

                $studentdata['student_application_date'] = date('Y-m-d', strtotime($application_date));

            }

			

			

			if (trim($this->input->post('student_application_comment')) != '') {

                $studentdata['student_application_comment'] = trim($this->input->post('student_application_comment'));

            }

			

			if (trim($this->input->post('student_enrolment_date')) != '') {

				$enrolment_date = trim($this->input->post('student_enrolment_date'));

                $studentdata['student_enrolment_date'] = date('Y-m-d', strtotime($enrolment_date));

            }

			

			if (trim($this->input->post('student_leaving_date')) != '') {

				$leaving_date = trim($this->input->post('student_leaving_date'));

                $studentdata['student_leaving_date'] = date('Y-m-d', strtotime($leaving_date));

            }
			
	        if (trim($this->input->post('student_email_status')) != '') {

                $studentdata['student_email_status'] = trim($this->input->post('student_email_status'));

            }
			

			if (trim($this->input->post('student_family_address')) != '') {

                $studentdata['student_family_address'] = trim($this->input->post('student_family_address'));

            }

			

			if ( count($this->input->post('student_sibling')) > 0 ) {

                $studentdata['student_sibling'] = implode(',',$this->input->post('student_sibling'));

            }

			   

			   

		     // $files = $_FILES;

            //  $count = count(array_filter($_FILES['student_certificates']['name']));

			

			//if( $count > 0 ) {

             /* for($i=0; $i<$count; $i++)

                {

                $_FILES['student_certificates']['name']= time().$files['student_certificates']['name'][$i];

                $_FILES['student_certificates']['type']= $files['student_certificates']['type'][$i];

                $_FILES['student_certificates']['tmp_name']= $files['student_certificates']['tmp_name'][$i];

                $_FILES['student_certificates']['error']= $files['student_certificates']['error'][$i];

                $_FILES['student_certificates']['size']= $files['student_certificates']['size'][$i];

                $config['upload_path'] = './uploads/student_certificates/';

                $config['allowed_types'] = '*';

                $this->load->library('upload', $config);

                $this->upload->initialize($config);

                $this->upload->do_upload("student_certificates");

                $fileName = $_FILES['student_certificates']['name'];

                $images[] = $fileName;

                }

                $fileName = implode(',',$images);

				

				$scertificates = $this->input->post('scertificates');

				if($scertificates!='') {

			       $studentdata['student_certificates']		=  $fileName.','.$scertificates;

				 } else {

				   $studentdata['student_certificates']		=  $fileName;

				   }*/

			 // }
			 
			
		/*	$student_email_status  = $this->input->post('student_email_status');
			if($student_email_status=='0'){ 
            $email=$this->input->post('email');
			$to_superadmin		= $email;
			$subject_superadmin	= "Student Request Payment";
			$message_superadmin	= "<html><body>";
			$message_superadmin	= "<font face='arial'>";
			$message_superadmin	.= "Hello" .$fname.' '.$lname. ",<br />";
			$message_superadmin	.= "A student <b> ".$fname.' '.$lname." </b>Your Request Payment is Pending.<br />";
			$message_superadmin	.= "<b>He/She Fullfil Leaving Date</b> <br />";
			$message_superadmin	.= "<b>View its profile for futher process. <br />";
			$message_superadmin	.= "<br />Thank You.<br />";
			$message_superadmin	.= "</font>";
			$message_superadmin .= "</body></html>";
			
			$this->email->set_header('MIME-Version', '1.0; charset=utf-8');
			$this->email->set_header('Content-type', 'text/html');
			
			$this->email->from($email, 'Olivetree Study');
			$this->email->to($to_superadmin);
			
			$this->email->subject($subject_superadmin);
			
			$this->email->message($message_superadmin);
			
			$result = $this->email->send();
			
			if($result){
			$studentdata['student_email_status']='1';
			$this->student_model->udateStudent($studentdata,$wherestudent);
			$this->student_model->updateUser($userdata,$whereuser);
						$this->session->set_flashdata('success', 'Student has been updated successfully.');
			            redirect(base_url()."students/edit/$sid");exit;
			
			}
			else{
				        $this->session->set_flashdata('error', 'Some problem exists. Student has not been updated.');
			            redirect(base_url()."students/edit/$sid");exit;
			}
			
	        } else{
			          if($this->student_model->udateStudent($studentdata,$wherestudent)){
			             $this->student_model->updateUser($userdata,$whereuser);
			             $this->session->set_flashdata('success', 'Student has been updated successfully.');
			             redirect(base_url()."students/edit/$sid");exit;
		 } else {

			             $this->session->set_flashdata('error', 'Some problem exists. Student has not been updated.');
			             redirect(base_url()."students/edit/$sid");exit;

		   }
		}*/

		if($this->student_model->udateStudent($studentdata,$wherestudent)){

			$this->student_model->updateUser($userdata,$whereuser);
	
			$this->session->set_flashdata('success', 'Student has been updated successfully.');

			redirect(base_url()."students/edit/$sid");exit;

		 } else {

			$this->session->set_flashdata('error', 'Some problem exists. Student has not been updated.');

			redirect(base_url()."students/edit/$sid");exit;

		   }

		

		$data['logmode']			= $this->logmode;

		$data['error']				= $this->session->flashdata('error');

		$data['success']			= $this->session->flashdata('success');

		$this->load->view('header');

		$this->load->view('student/edit',$data);

		$this->load->view('footer');

	  }


	 public function deleteCertificates()

	{     

			$data 					= array();	

			$school_id = $this->session->userdata('user_school_id');

			@$NewValuescrt = @$_POST['NewValuescrt']; 

			$postedstudentid = $_POST['postedstudentid']; 

			$crtV = $_POST['crtV']; 

			

			$where					= array('student_id'=>$postedstudentid);

		    $data['student_certificates'] = implode(',',$NewValuescrt);

			if($this->student_model->updateCertificates($data,$where)){

				$this->load->helper("file");

				$this->load->helper("url");

				 $url = FCPATH . 'uploads/student_certificates/'.$crtV;

				 unlink($url);

				echo '1';

			  } else {

				echo '0';

			   }

     	  }

	
	  public function deleteAvtar()

	{     

			$data 					= array();	

			$school_id = $this->session->userdata('user_school_id');

			$avtarName = trim($_POST['avtarName']); 

			$avtarUid = trim($_POST['avtarUid']); 

			$where					= array('id'=>$avtarUid);

		    $data['profile_image'] = '';

			if($this->student_model->updateAvtar($data,$where)){

				$this->load->helper("file");

				$this->load->helper("url");

				 $url = FCPATH . 'uploads/student/'.$avtarName;

				 unlink($url);

				 echo '1';

			   } else {

				echo '0';

			   }

     	  }

	  

	  

	   public function edit_certificates($sid){

		 $postURL	= $this->createPostURL($_GET);

		 if($sid==NULL){

			$this->session->set_flashdata('error', 'Select student first.');

			redirect(base_url()."students");exit;

		   }

		

		$whereuser						= array('id'=>$sid);

		$wherestudent					= array('user_id'=>$sid);

		

		$userdata 						= array();

		$studentdata 					= array();						

		$data							= array();

		$data['title']	      			= "Edit Account Details";

		$data['mode']					= "Edit";

			   

		      $files = $_FILES;

              $count = count(array_filter($_FILES['student_certificates']['name']));

			

			//if( $count > 0 ) {

              for($i=0; $i<$count; $i++)

                {

                $_FILES['student_certificates']['name']= time().$files['student_certificates']['name'][$i];

                $_FILES['student_certificates']['type']= $files['student_certificates']['type'][$i];

                $_FILES['student_certificates']['tmp_name']= $files['student_certificates']['tmp_name'][$i];

                $_FILES['student_certificates']['error']= $files['student_certificates']['error'][$i];

                $_FILES['student_certificates']['size']= $files['student_certificates']['size'][$i];

                $config['upload_path'] = './uploads/student_certificates/';

                $config['allowed_types'] = '*';

                $this->load->library('upload', $config);

                $this->upload->initialize($config);

                $this->upload->do_upload("student_certificates");

                $fileName = $_FILES['student_certificates']['name'];

                $images[] = $fileName;

                }

                $fileName = implode(',',$images);

				

				$scertificates = $this->input->post('scertificates');

				if($scertificates!='') {

			       $studentdata['student_certificates']		=  $fileName.','.$scertificates;

				 } else {

				   $studentdata['student_certificates']		=  $fileName;

				   }

			 // }

		

		if($this->student_model->udateStudent($studentdata,$wherestudent)){

			

			$this->session->set_flashdata('success', 'Student has been updated successfully.');

			redirect(base_url()."students/edit/$sid");exit;

		 } else {

			$this->session->set_flashdata('error', 'Some problem exists. Student has not been updated.');

			redirect(base_url()."students/edit/$sid");exit;

		   }

		

		$data['logmode']			= $this->logmode;

		$data['error']				= $this->session->flashdata('error');

		$data['success']			= $this->session->flashdata('success');



		$this->load->view('header');

		$this->load->view('student/edit',$data);

		$this->load->view('footer');

	  }

	

	 public function download_certificates($fileName = NULL) { 

	    $postURL	= $this->createPostURL($_GET);

       if ($fileName) {

        $file = realpath ( FCPATH.'uploads/student_certificates/'.$fileName );

        if (file_exists ( $file )) {

         $data = file_get_contents ( $file );

         $this->load->helper('download');

         force_download ( $fileName, $data );

        } else {

		 $this->session->set_flashdata('error', 'Requested file does not exists.');

         redirect(base_url()."students/view_student/$postURL");exit;

        }

       }

      }
	  
	  

public function excel_action()
  
  {
 
  $search_condition		= array();
    
  $branch_search     	= $this->input->post('branch_search');
  
  $class_search      	= $this->input->post('class_search');

  $status_search     	= $this->input->post('status_search'); 

  $shortbyname_search  	= $this->input->post('shortbyname_search');

  $sby       			= $this->input->post('sby');

  $word_search_org    	= $this->input->post('word_search_org');

  $word_search      	= trim($this->input->post('word_search'));
 
   
  $search_condition['users.user_type'] = 'student';

  //$search_condition['users.is_deleted'] = '0';

   

  if($branch_search!=''){

   $search_condition['student.student_school_branch'] = $branch_search;

  }

  if($class_search!=''){

  // $search_condition['student.student_class_group'] = $class_search;
	$class_search = $class_search;
  }

  if($status_search!=''){

            $search_condition['users.is_active'] = $status_search;

    }
  
 

  if($shortbyname_search!='') {

   $odr =   "student.student_fname";

  } else {

   $odr =   "student.user_id";

     }

  if($shortbyname_search!='') {

    $odr =   "student.student_fname";

    if($shortbyname_search=='0') {

      $dirc = "asc";

     } elseif($shortbyname_search=='1') {

      $dirc = "desc";

        }

   } else {

    $odr  = "student.user_id";

    $dirc = "desc";

     }

  $school_id      	  = $this->session->userdata('user_school_id');

  $staff_branch_id    = $this->session->userdata('staff_branch_id');

  $user_type      	  = $this->session->userdata('user_type');  
     
  $student_data = $this->student_model->getExcelData($search_condition,$class_search,$odr,$dirc,$school_id,$word_search,$sby);
  
   $this->load->model("student_model");
    
    $this->load->library("Excel");
    
    $object = new PHPExcel();
     
    $object->setActiveSheetIndex(0);
     
    $table_columns = array("Firstname", "Lastname", "Gender","Date of Birth", "Contact details", "Email-id","Address","Fathername","Father's contact details","Father's Email","Mothername","Mother's contact details","Mother's Email","Branch","Class","Fee Band","Application date","Enrollement date","Leaving date");
     
    $column = 0;
     
    foreach($table_columns as $field)
    {
     $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
     $column++;
    }
    
      
    $excel_row = 2;
     
    foreach($student_data as $row)
    {
     $id=$row->user_id;
     
     $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->student_fname);
     $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->student_lname);
     $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->student_gender);
     $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->student_dob);
     $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->student_telephone);
      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->email);
     $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->student_address);
     $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->student_father_name);
     $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->student_father_mobile);
     $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->student_father_email);
     $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->student_mother_name);
     $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->student_mother_mobile);
     $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->student_mother_email);
   /*     $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->student_school_branch);
     $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->student_class_group);
     $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row,$row->student_fee_band);*/
    
    if($row->student_school_branch==''){
     $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, @$row->student_school_branch);
     }else{
     $student_school_branch=$row->student_school_branch;
     $branch= $this->student_model->getbranchName($student_school_branch);
     $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row,@$branch->branch_name);
     }
    
     if($row->student_class_group==''){
     $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, @$row->student_class_group); 
     }
     else{
     $student_class_group=$row->student_class_group;
     $class = $this->student_model->getclassName($student_class_group);
     $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, @$class->class_name);
     }
     if($row->student_fee_band==''){
     $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row,@$row->student_fee_band);
     }
     else{
     $student_fee_band=$row->student_fee_band;
     $fee = $this->student_model->getFeedetail($student_fee_band);
     $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, @$fee->fee_band_price);
     }
     
     $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row->student_application_date);
     $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->student_enrolment_date);
     $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $row->student_leaving_date);
     $excel_row++;
    
    }
     
    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Student Data.xls"');
    $object_writer->save('php://output');
 }
 


	function createPostURL($get){

		$start	= $this->uri->segment(4,0);

		$odrby	= $this->uri->segment(5,0);

		$dirc	= $this->uri->segment(6,"asc");

		$postURL	= "$start/$odrby/$dirc/?";

		foreach($get as $key=>$val){

			$postURL	.="$key=$val&";

		}

		return  $postURL;exit;

	}

	public function payment_receive(){

		  $student_email_status  = $_POST['student_email_status'];
		  
		  $user_id 				 = $_POST['user_id'];
		  
		  $sid					 =	$user_id;
		  
		  $wherestudent			 = array('user_id'=>$sid);
		  
		  $student_email 		 = $_POST['student_email'];
		  
		  $student_email_subject = $_POST['student_email_subject'];

		  $student_email_message = $_POST['student_email_message'];
		   
	
			   
		if($student_email_status=='0'){
			 $to		= $student_email;

			 $subject	= $student_email_subject;

			 $message	= $student_email_message;

			 $mail_success = $this->authorize->send_email($to,$message,$subject);
			 if($mail_success){
			
			 $studentdata['student_email_status']='1';
			 $this->student_model->udateStudent($studentdata,$wherestudent);
             $this->session->set_flashdata('success', 'Message has been send successfully.');
			 redirect(base_url()."students/edit/$sid");exit;
			 }
			 }else{

			$this->session->set_flashdata('error', 'Message has already been send.');
			redirect(base_url()."students/edit/$sid");exit;
			 }
	  
	}
	
	
	public function request_payment(){
		

		  $student_email_status  = $_POST['student_email_status'];
		  
		  $user_id 				 = $_POST['user_id'];
		  
		  $sid					 =	$user_id;
		  
		  $student_email 		 = $_POST['student_email'];
		  
		  $student_email_subject = $_POST['student_email_subject'];

		  $student_email_message = $_POST['student_email_message'];
		   
			   
		if($student_email_status=='0'){
			
			 $to		= $student_email;

			 $subject	= $student_email_subject;

			 $message	= $student_email_message;

			 $this->authorize->send_email($to,$message,$subject);

             $this->session->set_flashdata('success', 'Message has been send successfully.');
			 redirect(base_url()."students/edit/$sid");exit;
		     }else{

			$this->session->set_flashdata('error', 'Message has already been send.');
			redirect(base_url()."students/edit/$sid");exit;
			 }
	    

	
	}




}



