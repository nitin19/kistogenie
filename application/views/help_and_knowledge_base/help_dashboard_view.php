<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>

<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>

				    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>
		   <li><a href="<?php echo base_url(); ?>help">Help and Knowledgebase</a></li>

        <li class="edit">Dashboards</li>        

        </ul>

        </div>

    <div class="block-content">	

      <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-3">
	<div class="list-block">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#admin-dashboard" aria-controls="home" role="tab" data-toggle="tab">Admin Dashboard</a></li>
      <li role="presentation"><a href="#teacher-dashboard" aria-controls="profile" role="tab" data-toggle="tab">Teacher Dashboard</a></li>
      <li role="presentation"><a href="#student-dashboard" aria-controls="messages" role="tab" data-toggle="tab">Student Dashboard</a></li>  
    </ul>
	</div>
    </div>
    <div class="col-sm-9">
      <div class="tab-content adm-dsh">
      <div role="tabpanel" class="tab-pane active" id="admin-dashboard">
	  
			<h1 class="tab-title">Admin Dashboard</h1>
			<p class="main-heading-cntnt">In admin dashboard, on the top of the right hand side, there is an options:</p>
			<div class="admn-content">
				<h3 class="para-heading">Profile:</h3>
			</div>
			<div class="admn-content">
				<h3 class="para-heading">Lock Screen:</h3>
				<p>This link is used  to regulate immediate access to their account  by requiring that the user perform a certain action in order to receive access, such as entering a password, using a certain button combination such as login.
</p>
</div>
<div class="admn-content">
				<h3 class="para-heading">Logout: </h3>
				<p>It is used to securely logout  from their account.</p>
</div>
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/dashboard.jpg" class="stt">
				<div class="admn-content">
				<h3 class="para-heading">Functionality of Send Message Button</h3>
				<p>Admin can send messages to anyone whether it is staff, student or any particular user using SEND MESSAGE button infront of the admin dashboard.</p>
				<div class="compse-msg">
					<h4 class="txt-frmt">FIELDS UNDER COMPOSE MESSAGE:</h4>
					<ul class="msg-point">
					<li><strong>From </strong>- Admin username already mentioned.</li>
					<li><strong>To</strong>- whom will you send the message like in this there are three check boxes (All,  staff and student)  and also below there is a text box  which contains the auto filled functionality that completes data in browser forms without the user needing to type it in full.</li>
					<li><strong>SUBJECT</strong> - textbox  where subject can be written</li>
					<li><strong>Message </strong>- texta area where message can be written</li>
					<li><strong>Attachment </strong> - user can attach multiple files but only those type  and size of files which is mentioned below “choose file button”.
	</li></ul>
	
	</div>
	<h3 class="para-heading mgn">Now click on SEND Button  after that message succesfully sent to the receipient.</h3>
	<div class="compse-msg">
					<h3 class="para-heading">NOTIFICATION SECTION:</h3> 
					<p>In notification section, received messages will be show  with their sender name, sender image with particular date .</p>
					</div>
					<div class="compse-msg">
					<h3 class="para-heading">SCHOOL AND BRANCH DETAIL SECTION:</h3> 
					<p>This section display graphical representation of number of branch/es, staff/s ,student/s and class/es.</p>
					</div>
					
					<div class="compse-msg">
					<h3 class="para-heading">THE NUMBER OF NEW STUDENT:</h3> 
					<p>Displays  graphical representation of the number of new students got admision in each class. Below this ,3 sections provide information regarding recently added student, leaving student and recently added staff. Admin can visit their profile using “VIEW” button provide within the section.
	</p></div>
		
			</div>
	  
	  </div>
	 
	 <!-- Teacher_dashboard-->
      <div role="tabpanel" class="tab-pane" id="teacher-dashboard">
	  
	 <h1 class="tab-title">Teacher Dashboard</h1>
			<p class="main-heading-cntnt">In teacher dashboard, on the top of the right hand side, there is an options:</p>
			<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/teacher-dashboard.jpg" class="stt">
			<div class="admn-content">
				<h3 class="para-heading">Profile:</h3>
				<p>When user clicked on profile button the  teacher detail will display</p>
			</div>
			<div class="admn-content">
				<h3 class="para-heading">Lock Screen:</h3>
				<p>This link is used  to regulate immediate access to their account  by requiring that the user perform a certain action in order to receive access, such as entering a password, using a certain button combination such as login.</p>
				</div>
				<div class="admn-content">
								<h3 class="para-heading">Logout: </h3>
								<p>IIt is used to securely logout  from their account.</p>
				</div>
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/send_message.jpg" class="stt">
				
				<div class="admn-content">
				<h3 class="para-heading">Functionality of Send Message Button</h3>
				<p>Teacher have access of only those sections which is provided by admin  and can view only those modules .Teacher  can send messages to anyone whether it is admin, student or any particular user using SEND MESSAGE button infront of the teacher dashboard.</p>
				<div class="compse-msg">
					<h4 class="txt-frmt">FIELDS UNDER COMPOSE MESSAGE:</h4>
					<ul class="msg-point">
					<li><strong>From </strong>- Admin username already mentioned.</li>
					<li><strong>To</strong>- whom will you send the message like in this there are three check boxes (All,  staff and student)  and also below there is a text box  which contains the auto filled functionality that completes data in browser forms without the user needing to type it in full.</li>
					<li><strong>SUBJECT</strong> - textbox  where subject can be written</li>
					<li><strong>Message </strong>- texta area where message can be written</li>
					<li><strong>Attachment </strong> - user can attach multiple files but only those type  and size of files which is mentioned below “choose file button”.
	</li></ul>
	
	</div>
	<h3 class="para-heading mgn">Now click on SEND Button  after that message succesfully sent to the receipient.</h3>
	<div class="compse-msg">
					<h3 class="para-heading">NOTIFICATION SECTION:</h3> 
					<p>In notification section, received messages will be show  with their sender name, sender image with particular date .</p>
					</div>
					<!--<div class="compse-msg">
					<h3 class="para-heading">SCHOOL AND BRANCH DETAIL SECTION:</h3> 
					<p>This section display graphical representation of number of branch/es, staff/s ,student/s and class/es.</p>
					</div>
					-->
					<div class="compse-msg">
					<h3 class="para-heading">THE NUMBER OF NEW STUDENT:</h3> 
					<p>Displays  graphical representation of the number of new students got admission in each class.
Below this ,2 sections provide information regarding recently added student, leaving  student.
Teacher  can visit their profile using “VIEW” button provide within the section.
</p></div>
		
			</div>
	  
	  
	  </div>
		
			
	<!-- Teacher_dashboard-->	
	<!-- Student Dashboard-->
		
      <div role="tabpanel" class="tab-pane" id="student-dashboard">
	  
	  <h1 class="tab-title">Student Dashboard</h1>
			<p class="main-heading-cntnt">In student dashboard, on the top of the right hand side, there is an options:</p>
			<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/student-dashboard.jpg" class="stt">
			<div class="admn-content">
				<h3 class="para-heading">Profile:</h3>
				<p>When user clicked on profile button the  teacher detail will display</p>
			</div>
			<div class="admn-content">
				<h3 class="para-heading">Lock Screen:</h3>
				<p>This link is used  to regulate immediate access to their account  by requiring that the user perform a certain action in order to receive access, such as entering a password, using a certain button combination such as login.</p>
			</div>
				
				<div class="admn-content">
								<h3 class="para-heading">Logout: </h3>
								<p>IIt is used to securely logout  from their account.</p>
				</div>
			<div class="compse-msg">
					<h3 class="para-heading">NOTIFICATION SECTION:</h3> 
					<p>In notification section, received messages will be show  with their sender name, sender image with particular date .</p>
					</div>
					<div class="compse-msg">
					<h3 class="para-heading">Pagination</h3> 
					<p>Pagination displays the number of pages.</p></div>
		
			</div>	
		
			</div>
	  
	  
	  
	  </div>
    </div>
    </div>
      

	   </div>
    
	 </div>

    </div>

<style>
.tabs-left, .tabs-right {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}

.tab-content.adm-dsh {
    background: #fff;
    width: 100%;
    display: inline-block;
    padding: 10px 20px;
    border: 1px solid #eee;
}
h1.tab-title {
    margin-top: 0 !important;
    margin-left: 0 !important;
    border: none !important;
    font-weight: 900;
    font-size: 32px !important;
}
.adm-dsh p {
    margin-left: 0;
}
p.main-heading-cntnt {
    margin-top: -44px;
    border-bottom: 1px solid #eeeeee;
    padding: 10px 0;
	margin-left:20px;
}
.admn-content {
    margin-top: -2px;
    padding-top: 10px;
    padding-left: 20px;
}
h3.para-heading {
        margin-top: 0px;
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
.admn-content p {
    margin-top: -7px;
}
img.stt {
    width: 100%;
    padding: 30px 0;
}
.compse-msg {
    padding: 4px 0 0 0px;
}
h4.txt-frmt {
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
ul.msg-point {
    padding-left: 0;
    margin: 0 0 0 14px;
    line-height: 30px;
    font-size: 15px;
}
.compse-msg {
    padding: 20px 0 16px 0px;
    line-height: 28px;
}
h3.para-heading.mgn {
    margin-top: 20px;
	color:#333 !important;
}
.list-block {
    height: 634px;
    width: 100% !important;
    background: #fff;
}
</style>