<?php

if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Studentenrolement_model extends CI_Model {

	var $tbl	= "student_enrolement";

	var $student	= "student";

	var $users = "users";

	var $nationality = "nationality";

	var $country = "country";

	var $religion = "religion";

	var $ethnic_origin = "ethnic_origin";

	var $branch = "branch";

	var $fee_band = "fee_band";

	var $classes = "classes";

	var $school = "school";

	var $student_enrolement_email_messages = "student_enrolement_email_messages";

	



    function __construct() {

        parent::__construct();

    }
	

	function getbranches($school_id) {

        $this->db->select('*');

        $this->db->from($this->branch);

		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->result();

    }

	
	function getnationality() {
        $this->db->select('*');
        $this->db->from($this->nationality);
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
    }
	
	function getcountry() {
        $this->db->select('*');
        $this->db->from($this->country);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getreligion() {
        $this->db->select('*');
        $this->db->from($this->religion);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getethnicorigin() {
        $this->db->select('*');
        $this->db->from($this->ethnic_origin);
        $query = $this->db->get();
        return  $query->result();
    }
	

	 function getAppliedRows($where=NULL,$school_id,$startdate,$enddate,$currentdate){

	      $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));

		  if($where != NULL){

			$this->db->where($where);

		  }

		  

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where(array($this->tbl.'.student_application_date >='=>$startdate));

			$this->db->where(array($this->tbl.'.student_application_date <='=>$enddate));

		  }

		  

		  $this->db->where(array($this->tbl.'.student_application_date <='=>$currentdate));

		   

          $query = $this->db->get();

		  $this->db->last_query(); 

		  return $query->num_rows();

	  }

	  

	   function getAppliedPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$startdate,$enddate,$currentdate){

	      $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));

		  if($where != NULL){

			$this->db->where($where);

		  }

		  

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where(array($this->tbl.'.student_application_date >='=>$startdate));

			$this->db->where(array($this->tbl.'.student_application_date <='=>$enddate));

		  } 

		  

		  $this->db->where(array($this->tbl.'.student_application_date <='=>$currentdate));

		  		  

		  if($odr){

			$this->db->order_by($odr, $dirc); 		

		   } else {

			$this->db->order_by("student_enrolement.student_application_date", "desc"); 	

		   }

		

		  $this->db->limit($limit, $start);

          $query = $this->db->get();

		  //echo $this->db->last_query(); exit();

           return  $query->result();



	}

	

	

	 function getSelectedRows($where=NULL,$school_id,$startdate,$enddate){

	      $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));

		  if($where != NULL){

			$this->db->where($where);

		  }

		  

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where(array($this->tbl.'.student_application_date >='=>$startdate));

			$this->db->where(array($this->tbl.'.student_application_date <='=>$enddate));

		  }

		   

          $query = $this->db->get();

		  $this->db->last_query(); 

		  return $query->num_rows();

	  }

	  

	   function getSelectedPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$startdate,$enddate){

	      $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));

		  if($where != NULL){

			$this->db->where($where);

		  }

		  

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where(array($this->tbl.'.student_application_date >='=>$startdate));

			$this->db->where(array($this->tbl.'.student_application_date <='=>$enddate));

		  } 

		  		  

		  if($odr){

			$this->db->order_by($odr, $dirc); 		

		   } else {

			$this->db->order_by("student_enrolement.student_application_date", "desc"); 	

		   }

		

		  $this->db->limit($limit, $start);

          $query = $this->db->get();

		  $this->db->last_query(); 

           return  $query->result();



	}

	

	 function getRejectedRows($where=NULL,$school_id,$startdate,$enddate){

	      $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));

		  if($where != NULL){

			$this->db->where($where);

		  }

		  

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where(array($this->tbl.'.student_application_date >='=>$startdate));

			$this->db->where(array($this->tbl.'.student_application_date <='=>$enddate));

		  }

		   

          $query = $this->db->get();

		  $this->db->last_query(); 

		  return $query->num_rows();

	  }

	  

	   function getRejectedPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$startdate,$enddate){

	      $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));

		  if($where != NULL){

			$this->db->where($where);

		  }

		  

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where(array($this->tbl.'.student_application_date >='=>$startdate));

			$this->db->where(array($this->tbl.'.student_application_date <='=>$enddate));

		  } 

		  		  

		  if($odr){

			$this->db->order_by($odr, $dirc); 		

		   } else {

			$this->db->order_by("student_enrolement.student_application_date", "desc"); 	

		   }

		

		  $this->db->limit($limit, $start);

          $query = $this->db->get();

		  $this->db->last_query(); 

           return  $query->result();



	}

	

	 function getWaitingRows($where=NULL,$school_id,$startdate,$enddate,$currentdate){

	      $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));

		  if($where != NULL){

			$this->db->where($where);

		  }

		  

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where(array($this->tbl.'.student_application_date >='=>$startdate));

			$this->db->where(array($this->tbl.'.student_application_date <='=>$enddate));

		  }

		  $this->db->where(array($this->tbl.'.student_postpone_date >='=>$currentdate));
       	  
		  $query = $this->db->get();

		  $this->db->last_query(); 

		  return $query->num_rows();

	  }

	  

	   function getWaitingPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$startdate,$enddate,$currentdate){

	      $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));

		  if($where != NULL){

			$this->db->where($where);

		  }

		  

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where(array($this->tbl.'.student_application_date >='=>$startdate));

			$this->db->where(array($this->tbl.'.student_application_date <='=>$enddate));

		  } 

		  

		  $this->db->where(array($this->tbl.'.student_postpone_date >='=>$currentdate));

		  		  

		  if($odr){

			$this->db->order_by($odr, $dirc); 		

		   } else {

			$this->db->order_by("student_enrolement.student_postpone_date", "asc"); 	

		   }

		

		  $this->db->limit($limit, $start);

          $query = $this->db->get();

		  $this->db->last_query(); 
	//echo $this->db->last_query();exit;
           return  $query->result();
		
	


	}  

	

	function getbranchName($student_school_branch) {

        $this->db->select('*');

        $this->db->from($this->branch);

		$this->db->where(array("branch_id="=>$student_school_branch,"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	function getclassName($student_class_group) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("class_id="=>$student_class_group,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getFeedetail($student_fee_band) {
        $this->db->select('*');
        $this->db->from($this->fee_band);
		$this->db->where(array("fee_band_id="=>$student_fee_band,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	
	function getSiblingdetail($student_sibling) {
		 $ids = explode(',', $student_sibling);
		  $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where_in($this->tbl.'.student_id', $ids);
		  $this->db->where(array("users.is_active"=>'1',"users.is_deleted"=>'0'));
          $query = $this->db->get();
		  $this->db->last_query(); 
          return  $query->result();
    }
	
	

	function getschoolPhone($school_id) {

        $this->db->select('*');

        $this->db->from($this->school);

		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	

	function update($data,$where){

		$this->db->where($where);
			
		if($this->db->update($this->tbl, $data))
		
			return true;
		
		else
		
			return false;

	 }

	 

	 function updateUser($userdata,$userwhere){

		$this->db->where($userwhere);

		if($this->db->update($this->users, $userdata))

			return true;

		else

			return false;


	 }

	 

	 function get_single_student($user_id) {

          $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.user_id'=>$user_id));

          $query = $this->db->get();

          return  $query->row();

    }

	

	 function insertStudent($data){

		if($this->db->insert($this->student, $data))

			return $this->db->insert_id();

		  else

			return false;

		//echo $this->db->last_query();exit;

	 }

	

	

	 

	  function delete($id){

		$where		= array('id' => $id);

		$data		= array('is_deleted'=>'1');

		$this->db->where($where);

		if($this->db->update($this->users, $data))

			return true;

		 else

			return false;

	}


	function get_selected_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->student_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"student_email_type="=>'selected',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	
	function get_applied_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->student_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"student_email_type="=>'applied',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	function get_rejected_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->student_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"student_email_type="=>'rejected',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	

	function get_waiting_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->student_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"student_email_type="=>'waiting',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	

	function get_requestdetail_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->student_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"student_email_type="=>'requestdetails',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();
	//echo $this->db->last_query();exit;

    }

	

	function get_schedule_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->student_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"student_email_type="=>'schedule',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

	

	function get_reschedule_info($school_id) {

        $this->db->select('*');

        $this->db->from($this->student_enrolement_email_messages);

		$this->db->where(array("school_id="=>$school_id,"student_email_type="=>'reschedule',"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->row();

    }

/************************************************** NEW FUNCTIONS STUDENT EXCEL DATA **************************************************************/	

	
	   function getExcelData($where=NULL,$odr=NULL,$dirc=NULL,$school_id,$startdate,$enddate,$currentdate,$tabvalue){

	      $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));

		  if($where != NULL){

			$this->db->where($where);

		  }

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where(array($this->tbl.'.student_application_date >='=>$startdate));

			$this->db->where(array($this->tbl.'.student_application_date <='=>$enddate));

		  } 
			
		if($tabvalue=='applied')	
			{
			$this->db->where(array($this->tbl.'.student_application_date <='=>$currentdate));
			}
		if($tabvalue=='waiting')
			{
			$this->db->where(array($this->tbl.'.student_postpone_date >='=>$currentdate));
			}	
			
		  if($odr){

			$this->db->order_by($odr, $dirc); 		

		   } else {

			$this->db->order_by("student_enrolement.student_application_date", "desc"); 	

		   }

          $query = $this->db->get();

		 //echo $this->db->last_query(); exit();

           return  $query->result();

	}


}

