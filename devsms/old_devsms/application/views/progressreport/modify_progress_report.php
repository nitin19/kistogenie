<?php
$studentid = $studentinfo->student_id; 
$school_id	= $studentinfo->school_id;
$student_school_branch = $studentinfo->student_school_branch;
$student_class_group = $studentinfo->student_class_group;
$this->load->model(array('modifyprogressreport_model'));
$branchName = $this->modifyprogressreport_model->getbranchName($student_school_branch);
$className = $this->modifyprogressreport_model->getclassName($student_class_group);

$totalAttendancestatus = $this->modifyprogressreport_model->get_total_Attendancestatus($studentid);
$presentAttendancestatus = $this->modifyprogressreport_model->get_present_Attendancestatus($studentid);
$absentAttendancestatus = $this->modifyprogressreport_model->get_absent_Attendancestatus($studentid);
$lateAttendancestatus = $this->modifyprogressreport_model->get_late_Attendancestatus($studentid);
?>
<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li><a href="#">Progress Reports</a></li>

       <!-- <li><a href="#">Term1</a></li>-->

        <li class="edit"><a href="#"><?php echo $studentinfo->student_fname.' '.$studentinfo->student_lname;?></a></li>        

        </ul>

        </div>

        <!--<div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>-->

        </div>
        
        
         <div style="clear:both"></div>
         
     <div id="progressreportError" style="display:none;"><div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			 Please fill all required fields field first.
		       </div></div>
        
         <div style="clear:both"></div>
        
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>


        <div class="col-sm-6 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

        <div class="col-sm-12 nopadding accdetailheading">

        <h1>Modify Report: <?php echo $studentinfo->student_fname.' '.$studentinfo->student_lname;?></h1>

        </div>

 <form action="<?php echo base_url(); ?>modifyprogressreport/update_progressreport" name="modifyprogressreportForm" id="modifyprogressreportForm" method="post">
 
 <input type="hidden" name="student_id" id="student_id" value="<?php echo $studentinfo->student_id;?>" />
 <input type="hidden" name="school_id" id="school_id" value="<?php echo $studentinfo->school_id;?>" />
 <input type="hidden" name="branch_id" id="branch_id" value="<?php echo $studentinfo->student_school_branch;?>" />
 <input type="hidden" name="class_id" id="class_id" value="<?php echo $studentinfo->student_class_group;?>" />
 <input type="hidden" name="year" id="year" value="<?php echo date('Y');?>" /> 
 <input type="hidden" name="p_year" id="p_year" value="<?php echo @$p_year;?>" />

     
<div class="col-sm-12 prfltitlediv titlediv report_cls">
     
        <h2>Report</h2>

        </div>
   <div class="profile-bg report_cls">
  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Student:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $studentinfo->student_fname.' '.$studentinfo->student_lname;?></span>

    </div>

  </div>



  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Branch:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo @$branchName->branch_name;?> </span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-4 col-xs-4 control-label nopadding">Class:</label>

    	<div class="col-sm-8 col-xs-8 inputinfo">

      	<span><?php echo @$className->class_name;?></span>

    	</div>

  		</div>

<!--
 		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Subject:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Arabic Language</span>

    </div>

  </div>
-->

<div class="col-sm-12 col-xs-12 nopadding greyborder headingdiv">

        <h2>Statutory Attendance</h2>

        </div>

        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Present:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>  <?php if( $presentAttendancestatus > 0 ) {		  
$present_percentage =  ($presentAttendancestatus * 100) / $totalAttendancestatus;
echo  ceil($present_percentage).'%';
} ?> </span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Late</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span> <?php if( $lateAttendancestatus > 0 ) {
$late_percentage =  ($lateAttendancestatus * 100) / $totalAttendancestatus;
echo  ceil($late_percentage).'%';
}?> </span>

    </div>

  </div>

  		

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding"> Absent:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span><?php if( $absentAttendancestatus > 0 ) {
$absent_percentage =  ($absentAttendancestatus * 100) / $totalAttendancestatus;
 echo  ceil($absent_percentage).'%';
}?></span>

    </div>

  </div>

  		<!--<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Unauthorised Absences:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

     <span>4.76%</span>

    </div>

  </div>-->

  <div class="form-group detailbox reportselbox">

    <label class="col-sm-3 control-label nopadding">Term</label>

    <div class="col-sm-9 inputbox">

<select class="form-control" name="term" id="term">
  <option value="">Select term</option>
<?php foreach($terms as $term) { ?>
 <option <?php if($term->term_id == @$p_term_id){ echo 'selected'; } ?> value="<?php echo $term->term_id;?>"><?php echo $term->term_name;?></option>
<?php }?>
</select>

    </div>

  </div>
  
  </div>
  
  
<div class="col-sm-12 nopadding dynamic_data_section" id="dynamicdatasection">
 </div>

 </form>

        </div>

        </div>

        <div class="col-sm-6 rightspace legendsec fullwidsec">

<!--       
         <div class="col-sm-12 nopadding accdetailheading">

        <h1>Legend</h1>

        </div>

         <div class="profilecompletion">

        <div class="col-sm-12 leftspace reportblock">

        <div class="col-sm-12 prfltitlediv prfltitle reporttitle">

        <h1>Proof Reading</h1>

        </div>

        <div class="col-sm-12 nopadding">

        <div class="profile-bg">

        <div class="proofreading-lessons">

        <div class="col-sm-12 nopadding">

        <div class="col-sm-12 lessonsdetail">

        <div class="col-sm-10 col-xs-10 lessontext">

        <p>GCS - SUN - Class 1 - Arabic Language</p>

        </div>

        <div class="col-sm-2 col-xs-2 lessonsetting">

        <i class="fa fa-cog"></i>

        </div>

        </div>

        <div class="col-sm-12 lessonsdetail">

        <div class="col-sm-10 col-xs-10 lessontext">

        <p>GCS - SUN - Class 1 - Arabic Language</p>

        </div>

        <div class="col-sm-2 col-xs-2 lessonsetting">

        <i class="fa fa-cog"></i>

        </div>

        </div>

        <div class="col-sm-12 lessonsdetail">

        <div class="col-sm-10 col-xs-10 lessontext">

        <p>GCS - SUN - Class 1 - Arabic Language</p>

        </div>

        <div class="col-sm-2 col-xs-2 lessonsetting">

        <i class="fa fa-cog"></i>

        </div>

        </div>

        <div class="col-sm-12 lessonsdetail">

        <div class="col-sm-10 col-xs-10 lessontext">

        <p>GCS - SUN - Class 1 - Arabic Language</p>

        </div>

        <div class="col-sm-2 col-xs-2 lessonsetting">

        <i class="fa fa-cog"></i>

        </div>

        </div>

        <div class="col-sm-12 lessonsdetail">

        <div class="col-sm-10 col-xs-10 lessontext">

        <p>GCS - SUN - Class 1 - Arabic Language</p>

        </div>

        <div class="col-sm-2 col-xs-2 lessonsetting">

        <i class="fa fa-cog"></i>

        </div>

        </div>

        

        </div>

        </div>

        

        </div>

        </div>

        </div>

        

        

        </div>

-->       
        </div>

        </div>
        

<script>
  jQuery(document).ready(function(){

	  jQuery("#term").on('change', function(e){ 
   		  var termid = jQuery(this).val();
		  if(termid!='') {
			var student_id = jQuery('#student_id').val();
			var school_id = jQuery('#school_id').val();
			var branch_id = jQuery('#branch_id').val();
			var class_id = jQuery('#class_id').val();
			var year = jQuery('#year').val();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>modifyprogressreport/check_term',
     data:{ 'student_id': student_id,'school_id': school_id,'branch_id': branch_id,'class_id': class_id,'termid': termid,'year': year },
            success :  function(data) {
					jQuery('#dynamicdatasection').html(data);
			          }
				   });
				 } else {
				 return false;
				 }
		 });
		  
		    var student_id = jQuery('#student_id').val();
			var school_id = jQuery('#school_id').val();
			var branch_id = jQuery('#branch_id').val();
			var class_id = jQuery('#class_id').val();
			var termid = jQuery('#term').val();
		    var year = jQuery('#p_year').val();
   		  
		  if(student_id!='' && school_id!='' && branch_id!='' && class_id!='' && termid!='' && year!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>modifyprogressreport/get_autofill_term',
      data:{ 'student_id': student_id,'school_id': school_id,'branch_id': branch_id,'class_id': class_id,'termid': termid,'year': year },
            success :  function(data) {
					jQuery('#dynamicdatasection').html(data);
			          }
				   });
				 } else {
				 return false;
				 }
				 
	});
</script>

<script>
  jQuery(document).ready(function(){
 
   var validFrom=false;
	 jQuery("#modifyprogressreportForm").validate({
		ignore: [],
        rules: {
			
			 term: {
    				required: true
   				},
					
			'effort[]': {
    					required: true
   					},
			'behaviour[]': {
    					required: true
   					},
			'homework[]': {
    					required: true
   					},
			'proofreading[]': {
    					required: true
   					},

			'comment[]': {
    					required: true
   					}
            },
        messages: {
			
			  term: {
    						required: "Please select term",
   					},
					
			 'effort[]': {
    						required: "Required field",
   					},
			 'behaviour[]': {
    						required: "Required field",
   					},
			'homework[]': {
    						required: "Required field",
   					},
			'proofreading[]': {
    						required: "Required field",
   					},
			'comment[]': {
    						required: "Required field",
   					}
					
               },
			   
	   submitHandler: submitForm
      });
	  
  	function submitForm() {
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>modifyprogressreport/check_progress_peport_status',
            data : jQuery("#modifyprogressreportForm").serialize(),
			dataType : "html",
           beforeSend: function()
            {
            },
                 success :  function(data) {
					// alert(data);
				      data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
					  validFrom=true;
	 jQuery('#modifyprogressreportForm').attr('action', '<?php echo base_url(); ?>modifyprogressreport/update_progressreport');
                       document.getElementById("modifyprogressreportForm").submit();
					} else if(data.Status == 'false') {
						jQuery('#progressreportError').show();
					} 
             }
        });
        return false;
      }
			 
	});
</script>

<style>
.titlediv > h1 {
    color: #1d2531;
    font-size: 16px;
    margin: 0;
    padding: 0 15px;
}

.greyborder {
    padding-top: 15px;
	border-bottom:0px !important;
}
.report_cls{
	padding:0 0 0 15px;
}
</style>