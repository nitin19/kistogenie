<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class studentenrolementreport_model extends CI_Model {
	
	var $student_enrolement			= "student_enrolement";
	var $student			 		= "student";
	var $users 						= "users";
	var $branch						= "branch";
	var $school						= "school";
	

    function __construct() {
        parent::__construct();
	}
	
		
		
	function total_branches($school_id,$limit=NULL, $offset=NULL,$branch_search){
		
		$this->db->select('*');
		$this->db->from($this->branch);
		$this->db->where(array($this->branch.'.school_id'=>$school_id, "is_active"=>'1',"is_deleted"=>'0'));
		
		if($branch_search != NULL){
			$this->db->where(array($this->branch.'.branch_id'=>$branch_search));
		  }
		
		
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->result();
	}
	
	function get_branches($school_id,$branch_search){
		
		$this->db->select('*');
		$this->db->from($this->branch);
		
		$this->db->where(array($this->branch.'.school_id'=>$school_id, "is_active"=>'1',"is_deleted"=>'0'));
		
		if($branch_search != NULL){
			$this->db->where(array($this->branch.'.branch_id'=>$branch_search));
		  }

	
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->num_rows();
	
	}
	
 function getbranchName($branch_id){
		$this->db->select('*');
		$this->db->from($this->branch);
	    $this->db->where('branch_id',$branch_id);
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->result();
	}
	
	function get_all_branch($school_id){
		$this->db->select('*');
		$this->db->from($this->branch);
	    $this->db->where(array("school_id"=> $school_id, "is_active"=>'1',"is_deleted"=>'0'));
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->result();
	}
	
	function get_applied($school_id,$branch_id,$startdate,$enddate){
		
		$this->db->select ('*');
		
		$this->db->from($this->student_enrolement);
		
	    $this->db->where(array("school_id"=> $school_id, "student_school_branch"=>$branch_id,"student_enrolement_status"=>'applied'));
		
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->student_enrolement.'.student_application_date >='=>$startdate));
			$this->db->where(array($this->student_enrolement.'.student_application_date <='=>$enddate));
		  }
		$query = $this->db->get();
	    //echo  $this->db->last_query(); exit();
		return $query->num_rows();
	
	}
	
	
	function get_selected($school_id,$branch_id,$startdate,$enddate){
		$this->db->select ('*');
		$this->db->from($this->student_enrolement);
	    $this->db->where(array("school_id"=> $school_id, "student_school_branch"=>$branch_id,"student_enrolement_status"=>'selected'));
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->student_enrolement.'.student_application_date >='=>$startdate));
			$this->db->where(array($this->student_enrolement.'.student_application_date <='=>$enddate));
		  }
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->num_rows();
	
	}
	function get_rejected($school_id,$branch_id,$startdate,$enddate){
		$this->db->select ('*');
		$this->db->from($this->student_enrolement);
	    $this->db->where(array("school_id"=> $school_id, "student_school_branch"=>$branch_id,"student_enrolement_status"=>'rejected'));
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->student_enrolement.'.student_application_date >='=>$startdate));
			$this->db->where(array($this->student_enrolement.'.student_application_date <='=>$enddate));
		  }
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->num_rows();
	
	}

function get_waiting($school_id,$branch_id,$startdate,$enddate){
		$this->db->select ('*');
		$this->db->from($this->student_enrolement);
	    $this->db->where(array("school_id"=> $school_id, "student_school_branch"=>$branch_id,"student_enrolement_status"=>'waiting'));
		if($startdate != NULL && $enddate!= NULL){
			$this->db->where(array($this->student_enrolement.'.student_application_date >='=>$startdate));
			$this->db->where(array($this->student_enrolement.'.student_application_date <='=>$enddate));
		  }
		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->num_rows();
	
	}
	
/******************************************************************************************************************/


function getExcelData($school_id,$branch_search){
		
		$this->db->select('*');
		$this->db->from($this->branch);
	    
		$this->db->where(array($this->branch.'.school_id'=>$school_id, "is_active"=>'1',"is_deleted"=>'0'));
		if($branch_search != NULL){
			$this->db->where(array($this->branch.'.branch_id'=>$branch_search));
		  }

		$query=$this->db->get();
	    $this->db->last_query(); 
		return $query->result();
	}	
	
}
