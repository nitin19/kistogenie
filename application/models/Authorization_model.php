<?php
class Authorization_model extends CI_Model {
	function __construct()    {
		parent::__construct();
	}
	function validate_user_credentials($username, $password){
		
		$this->db->select('id');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get('users');
		if ($query->num_rows() > 0){		
			return true;		
		}else{
			return false;
		}
	}
	
	function validate_user_email($email)	{
		$data = array();
		$this->db->select('id');
		$this->db->where('email', $email);
		$query = $this->db->get('users');
		return $query->num_rows();
			
	}
}
?>
