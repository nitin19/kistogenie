<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staffenrolement extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','staffenrolement_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		 
    }

	public function index()

	{
		$data = array();
		$search_condition=array();
		$branchid = $this->uri->segment(4);
		
		$sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;
		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		$branch_id						= isset($_GET['branchidsrch'])?$_GET['branchidsrch']:NULL;
		$perpage						= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$search_condition['users.user_type']	= 'enrolementstaff';
		
		$search_condition['users.is_active']	= '1';
		
		$search_condition['users.is_deleted']	= '0';
		
		$search_condition['staff_enrolement.staff_enrolement_status']	= 'applied';
		
		$currentdate 					= date('Y-m-d');
		
		$enrolementtype					='applied';
		
		
		if($branchid!=''){
		$search_condition['staff_enrolement.branch_id']	= $branchid;
		}
		
		if($branch_id!=''){
		$search_condition['staff_enrolement.branch_id']	= $branch_id;
		}
		
		if($sdate_search!=''){
			$startdate	= date('Y-m-d', strtotime($sdate_search));
			
		} else {
			$startdate= '';
		}
		
		if($edate_search!=''){
			$enddate	= date('Y-m-d', strtotime($edate_search));
			
		} else {
		$enddate = '';
		}
		
		$school_id				= 	$this->session->userdata('user_school_id');
		$data["total_rows"] 	=	$this->staffenrolement_model->getRows($search_condition,$school_id,$startdate,$enddate,$currentdate,$enrolementtype);
		$data["base_url"] 		= 	base_url() . "staffenrolement/index";
		
		
		if($perpage!='') {

			$data["per_page"] 	= $perpage;
			$PerPage			= $data["per_page"];

		} else {

			 $data["per_page"]	= 10;
			 $PerPage			= $data["per_page"];

		}
		
		$data['postfix_string'] 	= "/?startdate=$startdate&enddate=$enddate&branchid=$branch_id&perpage=$PerPage"; 


		$data["uri_segment"] 		= 3;
		
		$this->pagination->initialize($data);
		
		$page 							=  $this->uri->segment(3,0) ;
		
		$data['last_page']				= $page;
		
		$data["pagination"] 			= $this->pagination->create_links();
		
		$data['selectedInfo'] 			= $this->staffenrolement_model->get_selected_info($school_id);

		$data['rejectedInfo'] 			= $this->staffenrolement_model->get_rejected_info($school_id);

		$data['waitingInfo'] 			= $this->staffenrolement_model->get_waiting_info($school_id);

		$data['requestdetailsInfo']		= $this->staffenrolement_model->get_requestdetail_info($school_id);
		
		$data['scheduleInfo'] 			= $this->staffenrolement_model->get_schedule_info($school_id);
		
		$data['rescheduleInfo'] 		= $this->staffenrolement_model->get_reschedule_info($school_id);
		
		$data['school']					= $this->staffenrolement_model->get_school_info($school_id);
		
		$data['branch_data']			= $branchid;
		
		$data['sdate_search']			= $startdate;
		
		$data['edate_search']			= $enddate;
		
		$data['data_rows'] 				= $this->staffenrolement_model->getPagedData($search_condition,$school_id,$data["per_page"],$page, $startdate, $enddate,$currentdate,		  										  $enrolementtype);
		
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		$this->load->view('header');
		$this->load->view('staffenrolement/staff_enrolement_view', $data);
		$this->load->view('footer');

	}
	
	
	public function selectedstaffs()

	{
		$data 							= array();
		
		$search_condition				= array();
		
		$branchid 						= $this->uri->segment(4);		
		
		$sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;
		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		$branch_id						= isset($_GET['branchidsrch'])?$_GET['branchidsrch']:NULL;
		$perpage						= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$search_condition['users.user_type']	= 'teacher';
		$search_condition['users.is_active']	= '1';
		$search_condition['users.is_deleted']	= '0';
		$search_condition['staff_enrolement.staff_enrolement_status']	= 'selected';
		
		$currentdate 					= date('Y-m-d');
		
		$enrolementtype					='applied';
		
		if($branchid!=''){
		$search_condition['staff_enrolement.branch_id']	= $branchid;
		}
		
		if($branch_id!=''){
		$search_condition['staff_enrolement.branch_id']	= $branch_id;
		}
		
		if($sdate_search!=''){
			$startdate			= date('Y-m-d', strtotime($sdate_search));
		} else {
			$startdate= '';
		}
		
		if($edate_search!=''){
			$enddate			= date('Y-m-d', strtotime($edate_search));
		} else {
		$enddate = '';
		}
		
		$school_id				= 	$this->session->userdata('user_school_id');
		$data["total_rows"] 	=	$this->staffenrolement_model->getRows($search_condition,$school_id,$startdate,$enddate,$currentdate,$enrolementtype);
		$data["base_url"] 		= 	base_url() . "staffenrolement/selectedstaffs";
		
		
		
		if($perpage!='') {

			$data["per_page"] 	= $perpage;
			$PerPage			= $data["per_page"];

		} else {

			 $data["per_page"]	= 10;
			 $PerPage			= $data["per_page"];

		}

		$data["uri_segment"] 	= 3;
		
		$data['postfix_string'] 	= "/?startdate=$startdate&enddate=$enddate&branchid=$branch_id&perpage=$PerPage"; 
		
		$this->pagination->initialize($data);
		
		$page 					= $this->uri->segment(3,0) ;
		
		$data['last_page']		= $page;
		
		$data["pagination"] 	= $this->pagination->create_links();

		$data['school']			= $this->staffenrolement_model->get_school_info($school_id);
		
		$data['branch_data']	=  $branchid;
		
		$data['sdate_search']			= $startdate;
		
		$data['edate_search']			= $enddate;
		
		$data['data_rows'] 		= $this->staffenrolement_model->getPagedData($search_condition,$school_id,$data["per_page"],$page, $startdate, $enddate,$currentdate,   			    							  $enrolementtype);
		
		$data['error']			= $this->session->flashdata('error');
		$data['success']		= $this->session->flashdata('success');
		
		$this->load->view('header');
		$this->load->view('staffenrolement/staff_selected_view', $data);
		$this->load->view('footer');

	}
	
	
 public function rejectedstaffs()

	{
		
		$data 							= array();
		
		$search_condition				= array();
		
		$branchid						= $this->uri->segment(4);
		
		$sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;
		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		$branch_id						= isset($_GET['branchidsrch'])?$_GET['branchidsrch']:NULL;
		$perpage						= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$search_condition['users.user_type']	= 'enrolementstaff';
		$search_condition['users.is_active']	= '0';
		$search_condition['users.is_deleted']	= '0';
		$search_condition['staff_enrolement.staff_enrolement_status']	= 'rejected';
		
		$currentdate 					= date('Y-m-d');
		
		$enrolementtype					= 'rejected';
		
		if($branchid!=''){
		$search_condition['staff_enrolement.branch_id']	= $branchid;
		}
		
		if($branch_id!=''){
		$search_condition['staff_enrolement.branch_id']	= $branch_id;
		}
		
		if($sdate_search!=''){
			$startdate			= date('Y-m-d', strtotime($sdate_search));
		} else {
			$startdate= '';
		}
		
		if($edate_search!=''){
			$enddate			= date('Y-m-d', strtotime($edate_search));
		} else {
		$enddate = '';
		}
		
		$school_id				= 	$this->session->userdata('user_school_id');
		$data["total_rows"] 	=	$this->staffenrolement_model->getRows($search_condition,$school_id,$startdate,$enddate,$currentdate,$enrolementtype);
		$data["base_url"] 		= 	base_url() . "staffenrolement/rejectedstaffs";
		
		
		if($perpage!='') {

			$data["per_page"] 	= $perpage;
			$PerPage			= $data["per_page"];

		} else {

			 $data["per_page"]	= 10;
			 $PerPage			= $data["per_page"];

		}

		$data["uri_segment"] 	= 3;
		
		$data['postfix_string'] 	= "/?startdate=$startdate&enddate=$enddate&branchid=$branch_id&perpage=$PerPage"; 
		
		$this->pagination->initialize($data);
		
		$page 							= $this->uri->segment(3,0) ;
		
		$data['last_page']				= $page;
		
		$data["pagination"] 			= $this->pagination->create_links();
		
		$data['selectedInfo'] 			= $this->staffenrolement_model->get_selected_info($school_id);
		
		$data['appliedInfo'] 			= $this->staffenrolement_model->get_applied_info($school_id);

		$data['waitingInfo'] 			= $this->staffenrolement_model->get_waiting_info($school_id);

		$data['requestdetailsInfo'] 	= $this->staffenrolement_model->get_requestdetail_info($school_id);
		
		$data['scheduleInfo'] 			= $this->staffenrolement_model->get_schedule_info($school_id);
		
		$data['rescheduleInfo'] 		= $this->staffenrolement_model->get_reschedule_info($school_id);
		
		$data['school']					= $this->staffenrolement_model->get_school_info($school_id);
		
		$data['branch_data']			= $branchid ;
		
		$data['sdate_search']			= $startdate;
		
		$data['edate_search']			= $enddate;
				
		$data['data_rows'] 				= $this->staffenrolement_model->getPagedData($search_condition,$school_id,$data["per_page"],$page, $startdate, $enddate,$currentdate,         								  $enrolementtype);
		
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		$this->load->view('header');
		$this->load->view('staffenrolement/staff_rejected_view', $data);
		$this->load->view('footer');

	}
	
	
	
	public function waitingstaffs()

	{
		$data 							= array();
		
		$search_condition				= array();
		
		$branchid						= $this->uri->segment(4);
		
									
		$sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;
		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		$branch_id						= isset($_GET['branchidsrch'])?$_GET['branchidsrch']:NULL;
		$perpage						= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$search_condition['users.user_type']	= 'enrolementstaff';
		$search_condition['users.is_active']	= '1';
		$search_condition['users.is_deleted']	= '0';
		$search_condition['staff_enrolement.staff_enrolement_status']	= 'waiting';
		
		
		$currentdate 					= date('Y-m-d');
		
		$enrolementtype					='waiting';
		
		if($branchid!=''){
		$search_condition['staff_enrolement.branch_id']	= $branchid;
		}
		
		if($branch_id!=''){
		$search_condition['staff_enrolement.branch_id']	= $branch_id;
		}
		
		if($sdate_search!=''){
			$startdate			= date('Y-m-d', strtotime($sdate_search));
		} else {
			$startdate= '';
		}
		
		if($edate_search!=''){
			$enddate			= date('Y-m-d', strtotime($edate_search));
		} else {
		$enddate = '';
		}
		
		$school_id				= 	$this->session->userdata('user_school_id');
		$data["total_rows"] 	=	$this->staffenrolement_model->getRows($search_condition,$school_id,$startdate,$enddate,$currentdate,$enrolementtype);
		$data["base_url"] 		= 	base_url() . "staffenrolement/waitingstaffs";
		
		
		if($perpage!='') {

			$data["per_page"] 	= $perpage;
			$PerPage			= $data["per_page"];

		} else {

			 $data["per_page"]	= 10;
			 $PerPage			= $data["per_page"];

		}

		$data["uri_segment"] 	= 3;
		
		$data['postfix_string'] 	= "/?startdate=$startdate&enddate=$enddate&branchid=$branch_id&perpage=$PerPage"; 
		
		$this->pagination->initialize($data);
		
		$page 							= $this->uri->segment(3,0) ;
		
		$data['last_page']				= $page;
		
		$data["pagination"] 			= $this->pagination->create_links();
		
		$data['selectedInfo'] 			= $this->staffenrolement_model->get_selected_info($school_id);

		$data['rejectedInfo'] 			= $this->staffenrolement_model->get_rejected_info($school_id);

		$data['appliedInfo'] 			= $this->staffenrolement_model->get_applied_info($school_id);

		$data['requestdetailsInfo'] 	= $this->staffenrolement_model->get_requestdetail_info($school_id);
		
		$data['scheduleInfo'] 			= $this->staffenrolement_model->get_schedule_info($school_id);
		
		$data['rescheduleInfo'] 		= $this->staffenrolement_model->get_reschedule_info($school_id);
		
		$data['school']					= $this->staffenrolement_model->get_school_info($school_id);
		
		$data['sdate_search']			= $startdate;
		
		$data['edate_search']			= $enddate;
		
		$data['branch_data']			= $branchid ;		
		
		$data['data_rows'] 				= $this->staffenrolement_model->getPagedData($search_condition,$school_id,$data["per_page"],$page, $startdate, $enddate,$currentdate,			 										  $enrolementtype);
		
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		$this->load->view('header');
		$this->load->view('staffenrolement/staff_waiting_view', $data);
		$this->load->view('footer');

	}
	
	
	
/************************************************* staff enrolement view start ************************************************/	

public function wating_enrolement_staff(){

		$sid				= $_POST['user_id'];
		
		//$where				= array('user_id'=>$sid);

		$postponedate		= $_POST['pdate'];
		
		$userwhere			= array('id'=>$sid);
		 
		$userdata			= array('is_deleted'=>'0','is_active'=>'1','user_type'=>'enrolementstaff');
		
		$date				= date('Y-m-d', strtotime($postponedate));
		
		$data				= array('staff_enrolement_status'=>'waiting','staff_postpone_date'=>$date);
		
		$result				= $this->staffenrolement_model->updatedata($sid,$data);
		
		if($result){
			
			$result1= $this->staffenrolement_model->updateUser($userdata,$userwhere);
			
			echo $result1;
					
			return 'true';

		}else{

			return 'false';
		}
   	}

	
	public function deleteenrolement($id){

		$del_staff=$this->staffenrolement_model->delete($id);

		if($del_staff=='1'){

			$this->session->set_flashdata('success', 'Staff has been deleted successfully.');

			redirect(base_url()."staffenrolement/index");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."staffenrolement/index");exit;

		}

	}

/************************************************* staff enrolement view start ends ************************************************/


/********************************************* staff enrolement selected view start ********************************************/


	function wating_selected_staff(){

		$sid				= $_POST['user_id'];
		
		$where				= array('user_id'=>$sid);

		$postponedate		= $_POST['pdate'];
		
		$userwhere			= array('id'=>$sid);
		 
		$userdata			= array('is_deleted'=>'0','is_active'=>'1','user_type'=>'enrolementstaff');
		
		$date				= date('Y-m-d', strtotime($postponedate));
		
		$data				= array('staff_enrolement_status'=>'waiting','staff_postpone_date'=>$date);
		
		$result				= $this->staffenrolement_model->updatedata($data,$where);
		
		if($result){
			
			$result1= $this->staffenrolement_model->updateUser($userdata,$userwhere);
			
			echo $result1;
					
			return 'true';

		}else{

			return 'false';
		}

	}
	public function deleteselected($id){

		$del_student=$this->staffenrolement_model->delete($id);

		if($del_student=='1'){

			$this->session->set_flashdata('success', 'Staff has been deleted successfully.');

			redirect(base_url()."staffenrolement/selectedstaffs/index");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."staffenrolement/selectedstaffs/index");exit;

		}

	}

/********************************************* staff enrolement selected view end ********************************************/	
	
/********************************************* staff enrolement rejected view start ********************************************/

public function wating_rejected_staff(){

		$sid				= $_POST['user_id'];
		
		//$where				= array('user_id'=>$sid);

		$postponedate		= $_POST['pdate'];
		
		$userwhere			= array('id'=>$sid);
		 
		$userdata			= array('is_deleted'=>'0','is_active'=>'1','user_type'=>'enrolementstaff');
		
		$date				= date('Y-m-d', strtotime($postponedate));
		
		$data				= array('staff_enrolement_status'=>'waiting','staff_postpone_date'=>$date);
		
		$result				= $this->staffenrolement_model->updatedata($sid,$data);
		
		if($result){
			
			$result1= $this->staffenrolement_model->updateUser($userdata,$userwhere);
			
			echo $result1;
					
			return 'true';

		}else{

			return 'false';
		}
	}

	public function deleterejected($id){

		$del_student=$this->staffenrolement_model->delete($id);

		if($del_student=='1'){

			$this->session->set_flashdata('success', 'Staff has been deleted successfully.');

			redirect(base_url()."staffenrolement/rejectedstaffs/index");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."staffenrolement/rejectedstaffs/index");exit;

		}

	}



/*********************************************staff enrolement rejected view end ********************************************/

/*********************************************staff enrolement waiting view end ********************************************/

	public function deletewaiting($id){

		$del_student=$this->staffenrolement_model->delete($id);

		if($del_student=='1'){

			$this->session->set_flashdata('success', 'Staff  has been deleted successfully.');

			redirect(base_url()."staffenrolement/waitingstaffs/index");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."staffenrolement/waitingstaffs/index");exit;

		}

	}
/*********************************************staff enrolement waiting view end ********************************************/

	 public function view_enrolmentstaff($sid) {
		$id = $this->uri->segment(3);
		$data = array();
		if($sid==NULL){
			$this->session->set_flashdata('error', 'Select staff first.');
			redirect(base_url()."staffenrolementdashboard");exit;
		   }
		
		$data['info']					= $this->staffenrolement_model->get_single_enrolstaff($sid);
		
		$data['edu_info']				= $this->staffenrolement_model->display_education($sid);
		
		$data['exp_info']				= $this->staffenrolement_model->display_experience($sid);
							
		$data['title']	      			= "Account detail";
		$data['error']					= $this->session->flashdata('error');
		$data['logmode']				= $this->logmode;
		$data['success']				= $this->session->flashdata('success');
	     
		$this->load->view('header');
        $this->load->view('staffenrolement/staffenrol_account_detail',$data);
	    $this->load->view('footer');
    }
	
 
	public function edit_enrolmentstaff($sid) {
		$sid 					= $this->uri->segment(3);
		$school_id				= 	$this->session->userdata('user_school_id');
	
		$data = array();
		if($sid==NULL){
			$this->session->set_flashdata('error', 'Select staff first.');
			redirect(base_url()."staffenrolementdashboard");exit;
		   }
		
		$data['staffdata']				= $this->staffenrolement_model->get_single_enrolstaff($sid);
	
		$data['staffAccessdata']		= $this->staffenrolement_model->get_single_staff_access($sid);
		
		$data['branch'] 				= $this->staffenrolement_model->get_branches($school_id);
		
		$data['currencies'] 			= $this->staffenrolement_model->getcurrencies($school_id);
			
		$data['getteacherrole'] 		= $this->staffenrolement_model->teacher_role($school_id);
		
		$data['getteacherlevel'] 		= $this->staffenrolement_model->teacher_level($school_id);
		
		$data['getteachertype'] 		= $this->staffenrolement_model->teacher_type($school_id); 
							
		$data['title']	      			= "Account detail";
		$data['error']					= $this->session->flashdata('error');
		$data['logmode']				= $this->logmode;
		$data['success']				= $this->session->flashdata('success');
	     
		$this->load->view('header');
        $this->load->view('staffenrolement/staffenrol_editaccount_detail',$data);
	    $this->load->view('footer');
    }
	
	
 function update_staffenrolement() {
		
		$staffdata= array();
		$userdata= array();
		$staffaccess = array();
		
		$sid= $this->input->post('id');
		$sdob = $this->input->post('MyDate1');
		
		$staffdata = array(
		'staff_fname' => $this->input->post('firstname'),
		'staff_lname' => $this->input->post('lastname'),
		'staff_title' => $this->input->post('title'),
		
		'staff_role_at_school' => $this->input->post('staff_teacher_role'),
		'staff_teacher_type' => $this->input->post('staff_teacher_type'),
		'staff_teacher_level' => $this->input->post('staff_teacher_level'),
		'staff_education_qualification' => $this->input->post('quali'),
		'staff_personal_summery' => $this->input->post('persummary'),
		'staff_telephone' => $this->input->post('phone'),
		'staff_address' => $this->input->post('address'),
		'staff_address_1' => $this->input->post('optionaladdress')
		);
		
		$staffdata['staff_dob'] = date('Y-m-d', strtotime($sdob));
		
	/*	if($this->input->post('branch')!=''){
			$branches = $this->input->post('branch');
		    $staffdata['branch_id'] = implode(',', $branches);
		}
		
		if($this->input->post('class_name')!=''){
			$classes = $this->input->post('class_name');
		$staffdata['class_name'] = implode(',', $classes);
		}
		if($this->input->post('subjectsname')!=''){
		$subjects = $this->input->post('subjectsname');	
		$staffdata['subject_name'] = implode(',', $subjects);
		}*/
		
		 if ($this->input->post('salary_currency') != '') {
			$staffdata['salary_currency'] = $this->input->post('salary_currency');
		 }
		 
		 if ($this->input->post('salary_amount') != '') {
			$staffdata['salary_amount'] = $this->input->post('salary_amount');
		 }
		 
		 if ($this->input->post('salary_mode') != '') {
			$staffdata['salary_mode'] = $this->input->post('salary_mode');
		 }
		 
		 if ($this->input->post('teaching_days') != '') {
		   $teaching_days = $this->input->post('teaching_days');
		   $staffdata['teaching_days'] = implode(',', $teaching_days);
		  }  else {
			  $staffdata['teaching_days']='';
		  }

		/*if ($this->input->post('students_menu') == 'Yes') {
			      $staffaccess['students'] = 'Yes';
					} else {
			      $staffaccess['students'] = 'No';
					}
		
		if ($this->input->post('student_attendance_menu') == 'Yes') {
			      $staffaccess['student_attendance'] = 'Yes';
					} else {
			      $staffaccess['student_attendance'] = 'No';
					}
					
		if ($this->input->post('documents_menu') == 'Yes') {
			       $staffaccess['documents'] = 'Yes';
					} else {
				   $staffaccess['documents'] = 'No';
					}
		if ($this->input->post('progress_reports_menu') == 'Yes') {
			       $staffaccess['progress_reports'] = 'Yes';
					} else {
					$staffaccess['progress_reports'] = 'No';
					  }
					
		if ($this->input->post('configurations_menu') == 'Yes') {
			         $staffaccess['configurations'] = 'Yes';
					} else {
					 $staffaccess['configurations'] = 'No';
					  }
					
		if ($this->input->post('student_enrolement_menu') == 'Yes') {
			        $staffaccess['student_enrolement'] = 'Yes';
					} else {
					$staffaccess['student_enrolement'] = 'No';
					  }
					
		if ($this->input->post('staff_enrolement_menu') == 'Yes') {
			        $staffaccess['staff_enrolement'] = 'Yes';
					} else {
					$staffaccess['staff_enrolement'] = 'No';
					  }
					
		if ($this->input->post('staff_attendence_menu') == 'Yes') {
			        $staffaccess['staff_attendence']  = 'Yes';
					} else {
					$staffaccess['staff_attendence']  = 'No';
					   }
					
		if ($this->input->post('staff_salary_menu') == 'Yes') {
			       $staffaccess['staff_salary'] = 'Yes';
					} else {
					$staffaccess['staff_salary'] = 'No';
					  }
					
		if ($this->input->post('teaching_learing_menu') == 'Yes') {
			       $staffaccess['teaching_learing'] = 'Yes';
					} else {
					$staffaccess['teaching_learing'] = 'No';
				      }
		
		if ($this->input->post('basic_masters_menu') == 'Yes') {
			       $staffaccess['basic_masters'] = 'Yes';
					} else {
				   $staffaccess['basic_masters'] = 'No';
					   }*/
	    $staffaccess['updated_date'] = date('Y-m-d H:i:s');		

		
		$userdata = array(
		'username' => $this->input->post('username'),
		'password' => $this->input->post('password'),
		'email' => $this->input->post('email')
		);
		
		$config['upload_path']   = './uploads/staff/'; 
				$config['allowed_types'] =  'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload("profile_image")){
				$data = array('upload_data' => $this->upload->data());
				$image_name = $data['upload_data']['file_name'];
				if($_FILES['profile_image']['error'] == 0 ){
					$file_name					= $data['upload_data']['file_name'];
					$userdata['profile_image']		= $file_name;
					}
				}
		
		$dttt=$this->staffenrolement_model->updatestaff($sid,$staffdata);
		$dttt1=$this->staffenrolement_model->updateenrolementuser($sid,$userdata);
		 $this->staffenrolement_model->updatestaffAccess($sid,$staffaccess);	
		if($dttt=1 && $dttt1=1){	
		$this->session->set_flashdata('success', 'Data has been updated successfully.');
				redirect(base_url()."staffenrolement/edit_enrolmentstaff/".$sid);exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Staff has not been added.');
				redirect(base_url()."staffenrolement/edit_enrolmentstaff/".$sid);exit;
			  }
	 }


	public function add_user_enrolement(){
		
		  $data				   = array();

		  $tab_value		   = $_POST['tab_value'];

		  $school_id 		   = $_POST['school_id'];

		  $user_id 			   = $_POST['user_id'];

		  $staff_email 		   = $_POST['staff_email'];

		  $staff_email_subject = $_POST['staff_email_subject'];

		  $staff_email_message = $_POST['staff_email_message'];

		  

		  //$where			= array('user_id='=>$user_id);

	  	//  $data				= array('staff_enrolement_status'=>'selected');
		
		$data['staff_enrolement_status'] = 'selected';
		
		$data['staff_postpone_date'] 	 = '0000-00-00';
		  

		  $userwhere		= array('id'=>$user_id);

	  	  $userdata			= array('is_deleted'=>'0','is_active'=>'1','user_type'=>'teacher');

		  

		   $enroleinfo		= $this->staffenrolement_model->get_single_staff($user_id);

		  

		    $staffdata 		= array();	

		    $staffdata['school_id'] 					= trim($enroleinfo->school_id);

			$staffdata['branch_id'] 					= trim($enroleinfo->branch_id);
			
			$staffdata['user_id'] 						= trim($enroleinfo->user_id);

			$staffdata['staff_fname'] 					= trim($enroleinfo->staff_fname);

			$staffdata['staff_lname'] 					= trim($enroleinfo->staff_lname);

			$staffdata['staff_title'] 					= trim($enroleinfo->staff_title);

			$staffdata['staff_dob'] 					= trim($enroleinfo->staff_dob);

			$staffdata['class_name'] 					= trim($enroleinfo->class_name);

			$staffdata['subject_name'] 					= trim($enroleinfo->subject_name);

			$staffdata['staff_teacher_type'] 			= trim($enroleinfo->staff_teacher_type);

			$staffdata['staff_teacher_level'] 			= trim($enroleinfo->staff_teacher_level);

			$staffdata['staff_role_at_school'] 			= trim($enroleinfo->staff_role_at_school);

			$staffdata['staff_education_qualification'] = trim($enroleinfo->staff_education_qualification);

			$staffdata['staff_personal_summery'] 		= trim($enroleinfo->staff_personal_summery);

			$staffdata['staff_telephone'] 				= trim($enroleinfo->staff_telephone);

			$staffdata['staff_address']					= trim($enroleinfo->staff_address);

			$staffdata['staff_address_1'] 				= trim($enroleinfo->staff_address_1);

			$staffdata['salary_currency'] 				= trim($enroleinfo->salary_currency);

			$staffdata['salary_amount'] 				= trim($enroleinfo->salary_amount);

			$staffdata['salary_mode'] 					= trim($enroleinfo->salary_mode);

			$staffdata['teaching_days'] 				= trim($enroleinfo->teaching_days);

								  

		if($this->staffenrolement_model->updatedata($user_id,$data)){
		
			 $this->staffenrolement_model->updateUser($userdata,$userwhere);

			 $this->staffenrolement_model->insertStaff($staffdata);

			 $to		= $staff_email;

			 $subject	= $staff_email;

			 $message	= $staff_email_message;

			 $mail= $this->authorize->send_email($to,$message,$subject);
			 
		if($mail){
			
			 $this->session->set_flashdata('success', 'Message has been send successfully.');
				if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}
		}
		     else{
				 
				$this->session->set_flashdata('error', 'Message has not been send. Some error exit.');
				 

			 	if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}
	
		    }
		}
	    }


public function reject_user_enrolement(){
	
		   $tab_value		  	= $_POST['tab_value'];
		 
		  $school_id 			= $_POST['school_id'];

		  $user_id 				= $_POST['user_id'];

		  $staff_email 			= $_POST['staff_email'];

		  $staff_email_subject 	= $_POST['staff_email_subject'];

		  $staff_email_message 	= $_POST['staff_email_message'];

		  
 		  $rejectedInfo	= $this->staffenrolement_model->get_single_staff($user_id);
		  
		  $userwhere	= array('id'=>$user_id);
		 
		  $userdata		= array('is_deleted'=>'0','is_active'=>'0','user_type'=>'enrolementstaff');
 
		 // $where		= array('user_id'=>$user_id);

	  	 // $data			= array('staff_enrolement_status'=>'rejected');
		
			$data['staff_enrolement_status'] = 'rejected';
			
			$data['staff_postpone_date'] 	 = '0000-00-00';
	
			if($this->staffenrolement_model->updatedata($user_id,$data)){
				
			 $this->staffenrolement_model->updateUser($userdata,$userwhere);
			 
			 $to		= $staff_email;

			 $subject	= $staff_email;

			 $message	= $staff_email_message;

			 $this->authorize->send_email($to,$message,$subject);

			 $this->session->set_flashdata('success', 'Message has been send successfully.');
				if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}
			  
		     }else{
				 
				 $this->session->set_flashdata('error', 'Message has not been send . Some error exits.');
				 
			 	if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}
			
		   }

	}
	
	public function applied_user_enrolement(){
	
	 	  $tab_value		    = $_POST['tab_value'];
	
		  $school_id 			= $_POST['school_id'];

		  $user_id 				= $_POST['user_id'];

		  $staff_email 			= $_POST['staff_email'];

		  $staff_email_subject 	= $_POST['staff_email_subject'];

		  $staff_email_message 	= $_POST['staff_email_message'];

		  
 		  $userwhere	= array('id'=>$user_id);
		 
		  $userdata		= array('is_deleted'=>'0','is_active'=>'1','user_type'=>'enrolementstaff');
 
		  //$where		= array('user_id'=>$user_id);

	  	 // $data			= array('staff_enrolement_status'=>'applied');

			$data['staff_enrolement_status'] = 'applied';
			
			$data['staff_postpone_date'] 	 = '0000-00-00';
	
			if($this->staffenrolement_model->updatedata($user_id,$data)){
				
			 $this->staffenrolement_model->updateUser($userdata,$userwhere);
			 
			 $to		= $staff_email;

			 $subject	= $staff_email;

			 $message	= $staff_email_message;

			 $this->authorize->send_email($to,$message,$subject);

			 $this->session->set_flashdata('success', 'Message has been send successfully.');
			
			 if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
			 else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
			 else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}
			  
		     }else{
				 
				 $this->session->set_flashdata('error', 'Message has not been send.Some error exits.');
				 
			 	if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}

			 }

	}
	
	public function request_user_detail(){
		
		  $tab_value		   = $_POST['tab_value'];	
	
		  $school_id 		   = $_POST['school_id'];

		  $user_id 			   = $_POST['user_id'];

		  $staff_email 		   = $_POST['staff_email'];


		  $staff_email_subject = $_POST['staff_email_subject'];

		  $staff_email_message = $_POST['staff_email_message'];

		
 		
			 $to		= $staff_email;

			 $subject	= $staff_email_subject;

			 $message	= $staff_email_message;

			$result		= $this->authorize->send_email($to,$message,$subject);
			
				if($result){ 
			
			 $this->session->set_flashdata('success', 'Message has been send successfully.');
			
			 if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
			 else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
			 else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}
			  
		     }else{
				 
				 $this->session->set_flashdata('error', 'Message has not been send.Some error exits.');
				 
			 	if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}
			 }
	}
	
	public function user_schedule_email(){

	 	  $tab_value		   = $_POST['tab_value'];
		  
		  $school_id 		   = $_POST['school_id'];

		  $user_id             = $_POST['user_id'];

		  $staff_email 		   = $_POST['staff_email'];


		  $staff_email_subject = $_POST['staff_email_subject'];

		  $staff_email_message = $_POST['staff_email_message'];


			 $to		= $staff_email;

			 $subject	= $staff_email_subject;

			 $message	= $staff_email_message;

			$result		= $this->authorize->send_email($to,$message,$subject);
			
				if($result){ 
				
				
			 $this->session->set_flashdata('success', 'Message has been send successfully.');
			
			 if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
			 else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
			 else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}
			  
		     }else{
				 
				 $this->session->set_flashdata('error', 'Message has not been send.Some error exits.');
				 
			 	if($tab_value=='applied')
					{
					redirect(base_url()."staffenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."staffenrolement/rejectedstaffs");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."staffenrolement/waitingstaffs");exit;	
				}
				}
	}
	

	
/*********************************************staff update on page load ********************************************/

public function updatestaff(){
	
	$date		= $_POST['stdate'];
	
	$where		= array('staff_postpone_date'=>$date);
	
	$data		= array('staff_enrolement_status'=>'applied');
	
	$result=$this->staffenrolement_model->updatedate($data,$where);
	
	
	}

public function notesfor_user_enrolement(){

		  $school_id 	= $_POST['school_id'];

		  $user_id 		= $_POST['user_id'];

		  $staff_notes 	= $_POST['staff_notes'];
		  
		  $where		= array('user_id'=>$user_id);

	  	  $data			= array('staff_notes'=>$staff_notes);
	
		if($this->staffenrolement_model->updatedata($user_id,$data)){
			
			$this->session->set_flashdata('success', 'Notes has been saved successfully.');

			redirect(base_url()."staffenrolement/index");exit;

				}
				else{ 
	
			$this->session->set_flashdata('error', 'Notes has not been saved successfully.');

			redirect(base_url()."staffenrolement/index");exit;

				
				}
	}

public function notesfor_rejectedstaffs(){
	
		  $school_id 	= $_POST['school_id'];

		  $user_id 		= $_POST['user_id'];

		  $staff_notes 	= $_POST['staff_notes'];
		  
		  $where		= array('user_id'=>$user_id);

	  	  $data			= array('staff_notes'=>$staff_notes);
	
		if($this->staffenrolement_model->updatedata($user_id,$data)){
			
			$this->session->set_flashdata('success', 'Notes has been saved successfully.');

			redirect(base_url()."staffenrolement/rejectedstaffs");exit;

				}
				else{ 
	
			$this->session->set_flashdata('error', 'Notes has not been saved successfully.');

			redirect(base_url()."staffenrolement/rejectedstaffs");exit;

				
				}
	}
	
	
	
	public function notesfor_selectedstaffs(){
	
		  $school_id 	= $_POST['school_id'];

		  $user_id 		= $_POST['user_id'];

		  $staff_notes 	= $_POST['staff_notes'];
		  
		  $where		= array('user_id'=>$user_id);

	  	  $data			= array('staff_notes'=>$staff_notes);
	
		if($this->staffenrolement_model->updatedata($user_id,$data)){
			
			$this->session->set_flashdata('success', 'Notes has been saved successfully.');

			redirect(base_url()."staffenrolement/selectedstaffs");exit;

				}
				else{ 
	
			$this->session->set_flashdata('error', 'Notes has not been saved successfully.');

			redirect(base_url()."staffenrolement/selectedstaffs");exit;

				
				}
	}
	
	
public function notesfor_waitingstaffs(){
	
		  $school_id 	= $_POST['school_id'];

		  $user_id 		= $_POST['user_id'];

		  $staff_notes 	= $_POST['staff_notes'];
		  
		  $where		= array('user_id'=>$user_id);

	  	  $data			= array('staff_notes'=>$staff_notes);
	
		if($this->staffenrolement_model->updatedata($user_id,$data)){
			
			$this->session->set_flashdata('success', 'Notes has been saved successfully.');

			redirect(base_url()."staffenrolement/waitingstaffs");exit;

				}
				else{ 
	
			$this->session->set_flashdata('error', 'Notes has not been saved successfully.');

			redirect(base_url()."staffenrolement/waitingstaffs");exit;

				
				}
	}
function excel_action()
  
  {
	 		$search_condition	=	array();
			
			$exceltab	     	= 	$this->input->post('exceltab');
    	
			$branchid	     	= 	$this->input->post('branch_data');
			
 		    $sdate_search     	= 	$this->input->post('sdate_search');
  
 			$edate_search     	= 	$this->input->post('edate_search');

			$school_id      	= 	$this->session->userdata('user_school_id');
			
			$currentdate 		= date('Y-m-d');
				
	
			$search_condition['users.is_active']	= '1';
	
			$search_condition['users.is_deleted']	= '0';
			
			if($branchid!=''){
		
				$search_condition['staff_enrolement.branch_id']	= $branchid;
		}
		
			
			if($exceltab == "applied")
			{
					
				$search_condition['staff_enrolement.staff_enrolement_status']	= 'applied';
				
				$search_condition['users.user_type']	= 'enrolementstaff';
				
				$enrolementtype = 'applied';
	
	
			}
			else if($exceltab=="selected"){
				
				$search_condition['staff_enrolement.staff_enrolement_status']	= 'selected';
				
				$search_condition['users.user_type']	= 'teacher';
				
				$enrolementtype='selected';
				
				}
			else if($exceltab=="rejected"){
				
				$search_condition['staff_enrolement.staff_enrolement_status']	= 'rejected';
				
				$search_condition['users.user_type']	= 'enrolementstaff';
				
				$search_condition['users.is_active']	= '0';
				
				$enrolementtype='rejected';
				
				}
			else if($exceltab=="waiting"){
				
				$search_condition['staff_enrolement.staff_enrolement_status']	= 'waiting';
				
				$search_condition['users.user_type']	= 'enrolementstaff';
				
				$enrolementtype='waiting';
				
				}
				
			
			if($sdate_search!=''){
	
				$startdate	= date('Y-m-d', strtotime($sdate_search));
	
			} else {
	
				$startdate	= '';
	
			  }
	
			if($edate_search!=''){
	
				$enddate	= date('Y-m-d', strtotime($edate_search));
	
			} else {
	
				$enddate	= '';

		   }
		   
		$staffdata=$this->staffenrolement_model->getExcelData($search_condition,$school_id, $startdate, $enddate,$currentdate,$enrolementtype);
    
   		$this->load->library("Excel");
    
    	$object = new PHPExcel();
     
    	$object->setActiveSheetIndex(0);
     
    $table_columns = array("Firstname", "Lastname", "Date of Birth", "Contact details", "Email-id","Qualification","Address","Branch","Class","Teaching Days","Salary","Staff Application Date","Staff Postpone Date");
     
    $column = 0;
     
    foreach($table_columns as $field)
    {
     $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
     $column++;
    }
    
      
    $excel_row = 2;
     
    foreach($staffdata as $row)
    {
     $id=$row->user_id;
     
     $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->staff_fname);
     $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->staff_lname);
     $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->staff_dob);
     $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->staff_telephone);
     $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->email);
     $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->staff_education_qualification);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->staff_address);
	  
	  $branchid=$row->branch_id;
	   if($branchid=='' || $branchid=='0')
	   {
		   $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, @$row->branch_id);
	   }
	   else{
		   $branch_id		=	explode(',',$branchid);
		   
		   $getbranchname	=	'';
		   
		   foreach($branch_id as $branchid){
			   
			$getbranch		=   $this->staffenrolement_model->getbranchName($branchid);
			
		  	$getbranchname .=	",".$getbranch->branch_name;
		   }
			$staffbranch=ltrim($getbranchname,",");
			
	   		$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row,$staffbranch);
	
	   }
	   
	   if($row->class_name!='')
	   {
	  	$classid		=	$row->class_name;

		$class			=	explode(',',$classid);
		
		$getclassname	=	'';
		
		foreach($class as $class_name)
		{	
			$classname		=	$this->staffenrolement_model->getsingleClassName($class_name);
			
			$getclassname  .=	','.$classname->class_name;
		}
			$staffclass			=	ltrim($getclassname,",");
			
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $staffclass);
		
	   }
	   else
	   {
		   $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row,@$row->class_name);
	  	}
	   
		
	 $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->teaching_days);
	 
	 $salary 	= $row->salary_currency .' ' . $row->salary_amount.' '. $row->salary_mode;
	 
     $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $salary);
	 
	 $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row,$row->staff_application_date);
	 
	 $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row,$row->staff_postpone_date);
	 
     $excel_row++;
    
    }
     
    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Staff-Enrolement Data.xls"');
    $object_writer->save('php://output');
 }
 

}

