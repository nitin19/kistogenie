$db = new PDO("mysql:host=$host;dbname=$db_name", $username, $password);

$sql="SELECT * FROM users WHERE username = ?";
$STH = $db->prepare($sql);
$STH->execute(array($myusername));
$User = $STH->fetch();

if (!empty($User) && password_verify($mypassword, $User['hash']))
  echo "Logged in";
else
  echo "User/password incorrect";