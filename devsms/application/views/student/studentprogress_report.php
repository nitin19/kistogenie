
<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li><a href="#">Progress Reports</a></li>

        <li><a href="#">Term1</a></li>

        <li class="edit"><a href="#"> Modify Report Term 1: GCS - Aaliyah Ali</a></li>        

        </ul>

        </div>

        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>

        </div>
        
        
         <div style="clear:both"></div>
         
     <div id="progressreportError" style="display:none;"><div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			 Please fill all required fields field first.
		       </div></div>
        
         <div style="clear:both"></div>
        
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>


        <div class="col-sm-6 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

        <div class="col-sm-12 nopadding accdetailheading">

        <h1>Modify Report: GCS - Aaliyah Ali</h1>

        </div>

 <form action="<?php echo base_url(); ?>modifyprogressreport/update_progressreport" name="modifyprogressreportForm" id="modifyprogressreportForm" method="post">
 <input type="hidden" name="student_id" id="student_id" value="<?php echo $studentinfo->student_id;?>" />
 <input type="hidden" name="school_id" id="school_id" value="<?php echo $studentinfo->school_id;?>" />
 <input type="hidden" name="branch_id" id="branch_id" value="<?php echo $studentinfo->student_school_branch;?>" />
 <input type="hidden" name="class_id" id="class_id" value="<?php echo $studentinfo->student_class_group;?>" />

        <div class="profile-bg">

        <div class="col-sm-12 col-xs-12 nopadding">

        <h2>Report</h2>

        </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Student:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span></span>

    </div>

  </div>



  		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Branch:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span> </span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-4 col-xs-4 control-label nopadding">Class:</label>

    	<div class="col-sm-8 col-xs-8 inputinfo">

      	<span></span>

    	</div>

  		</div>

<div class="col-sm-12 col-xs-12 nopadding greyborder headingdiv">

        <h2>Statutory Attendance</h2>

        </div>

        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Present:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>   </span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Late</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>  </span>

    </div>

  </div>

  		

  		<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding"> Absences:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span></span>

    </div>

  </div>

  <div class="form-group detailbox reportselbox">

    <label class="col-sm-3 control-label nopadding">Term</label>

    <div class="col-sm-9 inputbox">

<select class="form-control" name="term" id="term">
  <option value="">Select term</option>
  <?php foreach($terms as $term) { ?>
 <option value="<?php echo $term->term_id;?>"><?php echo $term->term_name;?></option>
<?php }?>
</select>

    </div>

  </div>
  
  </div>
  
  
<div class="col-sm-12 nopadding dynamic_data_section" id="dynamicdatasection">
 </div>

 </form>

        </div>

        </div>

        <div class="col-sm-6 rightspace legendsec fullwidsec">

        </div>

        </div>
        

<script>
  jQuery(document).ready(function(){

	  jQuery("#term").on('change', function(e){ 
   		  var termid = jQuery(this).val();
		  if(termid!='') {
			var student_id = jQuery('#student_id').val();
			var school_id = jQuery('#school_id').val();
			var branch_id = jQuery('#branch_id').val();
			var class_id = jQuery('#class_id').val();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>studentdashboard/check_term',
            data:{ 'student_id': student_id,'school_id': school_id,'branch_id': branch_id,'class_id': class_id,'termid': termid },
            success :  function(data) {
					jQuery('#dynamicdatasection').html(data);
			          }
				   });
				 } else {
				 return false;
				 }
		 });
		 
	});
</script>

<script>
  jQuery(document).ready(function(){
 
   var validFrom=false;
	 jQuery("#modifyprogressreportForm").validate({
		ignore: [],
        rules: {
			'effort[]': {
    					required: true
   					},
			'behaviour[]': {
    					required: true
   					},
			'homework[]': {
    					required: true
   					},
			'proofreading[]': {
    					required: true
   					},

			'comment[]': {
    					required: true
   					}
            },
        messages: {
			 'effort[]': {
    						required: "Required field",
   					},
			 'behaviour[]': {
    						required: "Required field",
   					},
			'homework[]': {
    						required: "Required field",
   					},
			'proofreading[]': {
    						required: "Required field",
   					},
			'comment[]': {
    						required: "Required field",
   					}
					
               },
			   
	   submitHandler: submitForm
      });
	  
  	function submitForm() {
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>modifyprogressreport/check_progress_peport_status',
            data : jQuery("#modifyprogressreportForm").serialize(),
			dataType : "html",
           beforeSend: function()
            {
            },
                 success :  function(data) {
					// alert(data);
				      data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
					  validFrom=true;
	 jQuery('#modifyprogressreportForm').attr('action', '<?php echo base_url(); ?>modifyprogressreport/update_progressreport');
                       document.getElementById("modifyprogressreportForm").submit();
					} else if(data.Status == 'false') {
						jQuery('#progressreportError').show();
					} 
             }
        });
        return false;
      }
			 
	});
</script>

<style>
.titlediv > h1 {
    color: #1d2531;
    font-size: 16px;
    margin: 0;
    padding: 0 15px;
}
</style>