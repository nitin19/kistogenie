<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generateattendance extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		  }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','generate_attendance_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
    }

	public function index(){
		
		$config = array();
		$PerPage				   		= isset($_POST['perpage'])?$_POST['perpage']:NULL;
		$school_id		= $this->session->userdata('user_school_id');
		$data['branches'] = $this->generate_attendance_model->getbranches($school_id);
		
				$branch_search					= isset($_GET['branch'])?$_GET['branch']:'0';
                $sdate					= isset($_GET['sdate'])?$_GET['sdate']:date("F Y", strtotime("-1 months"));
	
		
		  $data["total_rows"] = $this->generate_attendance_model->record_count($school_id, $branch_search);
		  $data["base_url"] = base_url()."generateattendance/index";
		   
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 100;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'generateattendance/index';
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = "/?branch=$branch_search&sdate=$sdate";
		
		$data["per_page"] 						= $perpage;
        $data["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$start =  $this->uri->segment(3,0) ;
		$data['last_page']				= $start;
		$data["pagination"] = $this->pagination->create_links();
		$data['PerPage']				= $PerPage;
		$data['data_rows'] = $this->generate_attendance_model->get_staff($school_id, $branch_search, $data["per_page"], $start);
		$data['post_url']				= $config['postfix_string'];
		$data['branch_search']			= $branch_search;
		$data['sdate']			= $sdate;
		
		$this->load->view('header');
		$this->load->view('attendanceregister/attendancegenerate',$data);
		$this->load->view('footer');
	}  
	
        public function check_classes()

	     {     
		$school_id = $this->session->userdata('user_school_id');
		$schoolbranch = trim($_POST['schoolbranch']); 
	        $getclasses = $this->generate_attendance_model->getclasses($school_id,$schoolbranch);
		foreach($getclasses as $getclass) {
 	             echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
		}
	     } 
		 
	
	
	 public function generate_salary(){ 
	 		$month_year = $this->input->post('month_year');
			$sal_branch = $this->input->post('sal_branch');
	 		$madesalary = $this->generate_attendance_model->generated_salary($month_year, $sal_branch);
	 			if(count($madesalary) > 0){
					$this->session->set_flashdata('error', 'Salary of selected Month And Branch is already created');
					redirect(base_url()."generateattendance");exit;
				} else {
	 			 $inputdata		=  $this->input->post();
				 $ldata =  $this->generate_attendance_model->generatesalary($inputdata);
				 if($ldata) {
				$this->session->set_flashdata('success', 'Salary has been Generated Successfully.');
				 redirect(base_url()."generateattendance"); exit;
			     } else {
				$this->session->set_flashdata('error', 'Some problem exists. Salary has not been Generated.');
				redirect(base_url()."generateattendance");exit;
			     }	
				}
		 }
		 
	 public function view_salary(){
		 $config = array();
		  $data = array();
		  $search_condition =array();
		$PerPage				   		= isset($_POST['perpage'])?$_POST['perpage']:NULL;
		$school_id		= $this->session->userdata('user_school_id');
		$data['branches'] = $this->generate_attendance_model->getbranches($school_id);
		
				$branch_search					= isset($_GET['branch'])?$_GET['branch']:0;
                $sdate					= isset($_GET['sdate'])?$_GET['sdate']:NULL;
				
		   $data["total_rows"] = $this->generate_attendance_model->salary_record_count($branch_search,$sdate,$search_condition,$school_id);
		  $data["base_url"] = base_url()."generateattendance/index";
		   
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 100;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'generateattendance/view_salary';
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = "/?branch=$branch_search&sdate=$sdate";
		
		$data["per_page"] 						= $perpage;
        $data["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$start =  $this->uri->segment(3,0) ;
		$data['last_page']				= $start;
		$data["pagination"] = $this->pagination->create_links();
		$data['PerPage']				= $PerPage;
		$data['data_rows'] = $this->generate_attendance_model->get_salary_record($branch_search,$sdate,$search_condition,$school_id, $data["per_page"], $start);
		
		$data['post_url']				= $config['postfix_string'];
		$data['branch_search']			= $branch_search;
		$data['sdate']			= $sdate;
		
		$this->load->view('header');
		$this->load->view('attendanceregister/viewsalary', $data);
		$this->load->view('footer');
	 }
	 
	 	function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}
	
	function excel_action(){
  		$search_condition				=	array();
   		
		$school_id						= $this->session->userdata('user_school_id');
   
     	$month_year     					= $this->input->post('search_date');
		$sal_branch     					= $this->input->post('srch_branch');
     
  		$staffdata	 					= $this->generate_attendance_model->generated_salary($month_year, $sal_branch);
		
   		//$this->load->model("staff_model");
    
   		$this->load->library("Excel");
    
    	$object = new PHPExcel();
     
    	$object->setActiveSheetIndex(0);
     
    $table_columns = array("Firstname", "Lastname", "Branch", "Month Year", "Total Working Days", "Total Working Hours", "Present Working Days", "Present Working Hours", "Absent Working Days", "Absent working Hours", "Late Working Hours", "Actual Salary", "Calculated Salary", "Adjustment", "Grand Salary");
     
    $column = 0;
     
    foreach($table_columns as $field)
    {
     $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
     $column++;
    }
    
      
    $excel_row = 2;
     
    foreach($staffdata as $row)
    {
     $user_id = $row->user_id;
	 
	$userdetail = $this->generate_attendance_model->getuserinfo($user_id);
	$firstname = $userdetail->staff_fname;
    $lastname = $userdetail->staff_lname;
	
	$staff_branch_ids = $row->branch_id;
	$branchdetail = $this->generate_attendance_model->getstaff_branches($staff_branch_ids);
	$branchname = $branchdetail->branch_name;
	 
     $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $firstname);
     $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $lastname);
     $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $branchname);
     $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->month_year);
     $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->total_working_days);
     $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->total_working_hours);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->present_working_days);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->present_working_hours);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->absent_working_days);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->absent_working_hours);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->late_working_hours);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->actual_salary);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $userdetail->salary_currency.$row->calculated_salary);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $userdetail->salary_currency.$row->adjust_amount);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $userdetail->salary_currency.$row->grand_amount);
	  
     $excel_row++;
    
    }
     
    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Staff Salary.xls"');
    $object_writer->save('php://output');
 
  }

}