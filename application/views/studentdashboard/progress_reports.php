<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>

		      <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>


        <li class="edit">Progress Reports</li>        

        </ul>

        </div>

        <h1>Progress Reports</h1>

        <div class="col-sm-12 progressreports nopadding">
        <?php 
		$sr = 0; 

		foreach($terms as $tremdetail){ ?>
        
<?php $sr++; 
		$user_id		= $this->session->userdata('user_id');
		$this->load->model(array('Student_model_dashboard'));
		$studentinfo = $this->Student_model_dashboard->getStudentinfo($user_id);
		
		$this->load->model(array('Student_model_dashboard'));
		$studentterm = $this->Student_model_dashboard->getStudentterm($tremdetail->term_id,$studentinfo->student_id);

if($studentterm > 0){

?>

   
      <a href="<?php echo base_url().'viewprogressreport/index/'.$studentinfo->student_id.'/'.$tremdetail->term_id.'/'.date("Y");?>">
      
        <div class="reportsec progressreport">

        <div class="<?php if($sr % 5 =='1'){echo 'greenbox';} if($sr % 5 == '2'){echo 'bluebox';} if($sr % 5 =='3'){echo 'yellowbox';} if($sr % 5 =='4'){echo 'pinkbox';} if($sr % 5== '0'){echo 'purplebox';}?>  reportblk">

        <div class="reportblocks">

        <div class="col-sm-9 col-xs-9 reportname">

        <h1><?php echo $tremdetail->term_name; ?></h1>

        <h2 class="marks"></h2>

        </div>

        <div class="col-sm-3 col-xs-3 reportarrow">

        <i class="fa fa-arrow-circle-o-up"></i>

        </div>

        

        </div>

        <div class="progressindicator">

<!--        <span><?php /*$branch_id = $tremdetail->branch_id; 
		$this->load->model(array('Student_model_dashboard'));
$branchName = $this->Student_model_dashboard->getbranchName($branch_id);

echo $branchName->branch_name;*/
		?> </span>
-->
        </div>

        </div>

        </div>
        </a>
      
        <?php } }?>
        
    </div>

    </div>