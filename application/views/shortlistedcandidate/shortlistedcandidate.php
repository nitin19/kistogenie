<div class="editprofile-content">
        <div class="profilemenus">
        
    <div class="col-sm-10 menubaritems nopadding">
        <ul>
            <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>
        <li class="edit"> Shortlisted Candidate</a></li>        
        </ul>
     </div>
     
     <div class="col-sm-2">
     
<?php

function generate_random_username(){

        $characters = 'abcdefghijklmnopqrstuvwxyz';

        $string = '';

        for ($i = 0; $i < 6; $i++) {

            $string .= $characters[rand(0, strlen($characters) - 1)];

        }

        return $string;

    }
$inviteusername = generate_random_username();
    
function generate_random_password(){

        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';

        $string = '';

        for ($i = 0; $i < 8; $i++) {

            $string .= $characters[rand(0, strlen($characters) - 1)];

        }

        return $string;

    }
    
$invitepassword = generate_random_password();


?>

<!--<input type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" value="Invite Staff" style="padding: 4px 20px;">  -->


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:0px;">
          <div class="col-sm-12 col-xs-11 nopadding invitemember">
          <div class="col-sm-9"><h3 class="modal-title inviteheader">Invite staff member</h3></div>
           <div class="col-sm-3">
           <button type="button" class="close closenotify" data-dismiss="modal">&times;</button>
           </div>
          </div>
        </div>
        <!---------error message ------------------>
        <div style="clear:both"></div>
       <div class="alert alert-danger alert-dismissable" id="invitationError" style="display:none" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <b>Alert!</b> 
            Some problem exists. Invitation has not been sent. 
        </div>
        
         <div class="alert alert-danger alert-dismissable" id="invitationAlreadyError" style="display:none" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <b>Alert!</b> 
            You have already invited this staff member.
        </div>
    
<!---------error message end------------------>
<!---------success message ------------------>
         
      <div class="alert alert-success alert-dismissable" id="invitationSuccess" style="display:none">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
           <b>Alert!</b> 
            Invitation has been sent successfully. 
             </div>

 <div style="clear:both"></div>         
<!---------success message end------------------>
        <div class="modal-body">
        <form action="<?php echo base_url();?>schooltermreport/insert_archive_data" name="invitationForm" id="invitationForm">
            <input type="hidden" name="invited_user_name" id="invited_user_name" value="<?php echo $inviteusername;?>" >
            <input type="hidden" name="invited_user_pwd" id="invited_user_pwd" value="<?php echo  $invitepassword;?>">
          <div class="col-sm-12 popup_section_form">
          
          <div id="Loadingmsg"></div>
          
              <div class="col-sm-10 nopadding text_box_sec">
                 <input type="text" name="invited_user_email" id="invited_user_email" placeholder="Enter email address" maxlength="121" required />
             </div>
             <div class="col-sm-2 nopadding text_box_btn">
                <input value="Send" class="popup_save_btn" id="save" name="save" type="submit">
             </div>
          </div>
          </form>
        </div>
        <div class="modal-footer">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>
      
    </div>
  </div>
  
  
     </div>
     
        
        </div>
         <!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <b>Alert!</b> 
              <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
           <b>Alert!</b> 
           <?php echo $this->session->flashdata('success'); ?>
       </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>
        <div class="attendancesec"> 
    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">
        <form action="<?php echo base_url();?>shortlistedcandidate/index" name="stafffilterform" id="stafffilterform">
            <div class=" col-sm-12 search nopadding" id="srchh">
             <div class="col-sm-6 nopadding">Shortlisted Candidate Overview</div>
             <div class="col-sm-5 nopadding srchstaff">
            <div class="col-sm-6 col-xs-5 searching termsearch nopadding">

        <div class="form-group staffsrch">

      <input type="text" class="form-control searchbox" placeholder="Name, Email" name="user_srch" id="username"  value="">


     </div>

        </div>
    <div class="col-sm-6 col-xs-7 rightspace viewreport">

        <div class="form-group">

    <input type="submit" class="btn btn-danger" value="Filter">

    <a href="javascript:void(0)" class="cleanSearchFilter">Clear</a>

  </div>

</div>
   </div>           
</div>
</form>
    <div class="tablewrapper">

<table class="table-bordered table-striped">

              <thead>

                  <tr class="headings">

                      <th class="column1">No.</th>

                                <th class="column2">Name</th>

                                <th class="column3">School</th>
                                <!-- <th class="column3">Postion</th> -->
                                <th class="column3">Email</th>

                                <th class="column3">Contact</th>


                                <th class="column3">Options</th>

                       
                        <!-- <th class="column1">Edit</th> -->
                        <th class="column1">View</th>

                  </tr>

              </thead>

              <tbody>
                <?php
                if(count($shortlistedcandidatedata)>0)
                {
                  $sr=$last_page;
                  $comnCls = '';
                  foreach ( $shortlistedcandidatedata as $datastudent )
                  { 
                    $get_user = $this->applicationbank_model->get_user($datastudent->user_id);

                    $get_staffenrollment = $this->applicationbank_model->get_staffenrollment($get_user->id);

                    $get_branch = $this->applicationbank_model->get_branch($get_staffenrollment->branch_id);

                
                    ?>
                    <tr class="familydata staff">
                     
                      <td class="column1"><?php echo ++$sr; ?></td>

                      <td class="column2"><?php  if($datastudent->staff_fname != "" || $datastudent->staff_lname != ""){echo $datastudent->staff_fname." ".$datastudent->staff_lname;}else{echo ".";}; ?></td>
                      <td class="column3"><?php if($datastudent->school_name != ""){echo $datastudent->school_name;}else{".";} ?></td>
                          
                      <td class="column3"><?php if($datastudent->email!=""){echo $datastudent->email;}else{echo ".";} ?></td>

                      <td class="column3"><?php if($datastudent->staff_telephone!=""){echo $datastudent->staff_telephone;} else{echo ".";} ?></td>
			
                      <td class="column3"> 
                        <div class="btn-group">
                          <button type="button" class="btn btn-info btn-flat">Action</button>
                          <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <ul class="dropdown-menu" role="menu">
                            
                            <li>
                              <a href="<?php echo base_url(); ?>shortlistedcandidate/view_shortlistedcandidate/<?php echo $datastudent->user_id; ?>" target="_blank" style="cursor: pointer;"> View </a>
                              <a onclick="delConfirm(<?php echo $datastudent->slc_id; ?>)" style="cursor: pointer;"> Delete </a>
                            </li>
                            	<li> <a href="#" data-toggle="modal" data-target="#myModal_thankyou_<?php echo $datastudent->user_id;?>"> Selected Email </a> </li>
				<li> <a href="#" data-toggle="modal" data-target="#myModal_reject_<?php echo $datastudent->user_id;?>"> Reject Email </a> </li>
				<li> <a href="#" data-toggle="modal" data-target="#myModal_confirminterview_<?php echo $datastudent->user_id;?>"> Confirm Interview </a> </li>
				<li> <a href="#" data-toggle="modal" data-target="#myModal_accepted_<?php echo $datastudent->user_id;?>"> Accepted </a> </li>
				<li> <a href="#" data-toggle="modal" data-target="#myModal_notaccepted_<?php echo $datastudent->user_id;?>"> Not Accepted </a> </li>
	                        <li> 
	                            <a href="<?php echo base_url()?>shortlistedcandidate/move_to_application_bank/<?php echo $datastudent->user_id;?>/<?php echo $get_staffenrollment->school_id;?>/<?php echo $get_staffenrollment->branch_id;?>" > Move to application bank </a> 
	                        </li>
                          </ul>
                        </div>
                        <!-- <a href="#" class="btn btn-success excelbtn">Cv</a> -->
                      </td>
                    </tr>
		          <form action="<?php echo base_url()?>shortlistedcandidate/selected_message" method="post" id="notificationform" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
		                    <!-- The Modal -->
		                    <div id="myModal_thankyou_<?php echo $datastudent->user_id;?>" class="modal" style="z-index:99999999999">
		                    
		                      <!-- Modal content -->
		                      <div class="modal-content">
		                        <div class="modalbg">
		                          <div class="col-sm-12 compose nopadding" style="padding: 0 8px;">
		                            <div class="col-sm-8 nopadding composemsg">
		                              <h3>Congratulation Email</h3> 
		                            </div>
		                            <div class="col-sm-4 nopadding composeclose">
		                              <button type="button" class="close closebtn" data-dismiss="modal" aria-hidden="true">&times;</button>
		                            </div>
		                          </div>
		                          <div class="col-sm-12" style="margin-top: 10px;">
		                            <!-- <input type="hidden" name="notificationid" id="notificationid" class="form-control" value="<?php echo $notification->notification_id; ?>"/>  -->    
		                            <div class="col-sm-3 nopadding popup">
		                              From:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              
		                              <input type="hidden" name="from_id" id="from_id" class="form-control" value="<?php echo $this->session->userdata('user_id'); ?>"/>
		                              <input type="text" name="from" id="from" class="form-control" value="<?php echo $this->session->userdata('user_name'); ?>" readonly />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              To:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                              <input type="hidden" name="applied_by" id="applied_by" class="form-control" value="staff"/>                                     
		                             <!-- <input type="hidden" name="toemail" id="toemail" class="form-control" value="<?php echo $datastudent->email; ?>"/> -->
		                              <input type="hidden" name="toemail" id="toemail" class="form-control" value="<?php echo $datastudent->email; ?>"/>
		                              <input type="text" name="to" id="to" class="form-control" value="<?php echo $datastudent->staff_fname; ?> <?php echo $datastudent->staff_lname; ?>" readonly />                                        
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Subject:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              <input type="text" name="subject" id="subject" class="form-control" maxlength="251"  />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Message:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                                <textarea name="message" rows="3"  id="message" class="form-control"  maxlength="5001"></textarea>
		                            </div>                                            
		                                       
		                            <div class="col-sm-3  popupbtn" style="margin-left:20px; padding-bottom: 20px;">
		                              <input type="submit" name="send" class="compose" id="send" value="Send" />
		                            </div>
		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                  </form>
		                  
		                  
		                  <form action="<?php echo base_url()?>shortlistedcandidate/reject" method="post" id="notificationform" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
		                    <!-- The Modal -->
		                    <div id="myModal_reject_<?php echo $datastudent->user_id;?>" class="modal" style="z-index:99999999999">
		                    
		                      <!-- Modal content -->
		                      <div class="modal-content">
		                        <div class="modalbg">
		                          <div class="col-sm-12 compose nopadding" style="padding: 0 8px;">
		                            <div class="col-sm-8 nopadding composemsg">
		                              <h3>Reject Email</h3> 
		                            </div>
		                            <div class="col-sm-4 nopadding composeclose">
		                              <button type="button" class="close closebtn" data-dismiss="modal" aria-hidden="true">&times;</button>
		                            </div>
		                          </div>
		                          <div class="col-sm-12" style="margin-top: 10px;">
		                            
		                            <div class="col-sm-3 nopadding popup">
		                              From:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              
		                              <input type="hidden" name="from_id" id="from_id" class="form-control" value="<?php echo $this->session->userdata('user_id'); ?>"/>
		                              <input type="text" name="from" id="from" class="form-control" value="<?php echo $this->session->userdata('user_name'); ?>" readonly />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              To:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                              <input type="hidden" name="applied_by" id="applied_by" class="form-control" value="staff"/>                                     
		                              <input type="hidden" name="toemail" id="toemail" class="form-control" value="<?php echo $datastudent->email; ?>"/>
		                              <input type="text" name="to" id="to" class="form-control" value="<?php echo $datastudent->staff_fname; ?> <?php echo $datastudent->staff_lname; ?>" readonly />                                        
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Subject:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              <input type="text" name="subject" id="subject" class="form-control" maxlength="251"  />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Message:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                                <textarea name="message" rows="3"  id="message" class="form-control" maxlength="5001"></textarea>
		                            </div>                                            
		                                       
		                            <div class="col-sm-3  popupbtn" style="margin-left:20px; padding-bottom: 20px;">
		                              <input type="submit" name="send" class="compose" id="send" value="Send" />
		                            </div>
		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                  </form>
		                  
		                  <form action="<?php echo base_url()?>shortlistedcandidate/confirminterview" method="post" id="notificationform" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
		                    <!-- The Modal -->
		                    <div id="myModal_confirminterview_<?php echo $datastudent->user_id;?>" class="modal" style="z-index:99999999999">
		                    
		                      <!-- Modal content -->
		                      <div class="modal-content">
		                        <div class="modalbg">
		                          <div class="col-sm-12 compose nopadding" style="padding: 0 8px;">
		                            <div class="col-sm-8 nopadding composemsg">
		                              <h3>Confirm Interview</h3> 
		                            </div>
		                            <div class="col-sm-4 nopadding composeclose">
		                              <button type="button" class="close closebtn" data-dismiss="modal" aria-hidden="true">&times;</button>
		                            </div>
		                          </div>
		                          <div class="col-sm-12" style="margin-top: 10px;">
		                            
		                            <div class="col-sm-3 nopadding popup">
		                              From:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              
		                              <input type="hidden" name="from_id" id="from_id" class="form-control" value="<?php echo $this->session->userdata('user_id'); ?>"/>
		                              <input type="text" name="from" id="from" class="form-control" value="<?php echo $this->session->userdata('user_name'); ?>" readonly />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              To:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                              <input type="hidden" name="applied_by" id="applied_by" class="form-control" value="staff"/>                                     
		                              <input type="hidden" name="toemail" id="toemail" class="form-control" value="<?php echo $datastudent->email; ?>"/>
		                              <input type="text" name="to" id="to" class="form-control" value="<?php echo $datastudent->staff_fname; ?> <?php echo $datastudent->staff_lname; ?>" readonly />                                        
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Subject:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              <input type="text" name="subject" id="subject" class="form-control" maxlength="251"  />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Message:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                                <textarea name="message" rows="3"  id="message" class="form-control" maxlength="5001"></textarea>
		                            </div>                                            
		                                       
		                            <div class="col-sm-3  popupbtn" style="margin-left:20px; padding-bottom: 20px;">
		                              <input type="submit" name="send" class="compose" id="send" value="Send" />
		                            </div>
		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                  </form>
		                  
		                  <form action="<?php echo base_url()?>shortlistedcandidate/notaccepted" method="post" id="notificationform" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
		                    <!-- The Modal -->
		                    <div id="myModal_notaccepted_<?php echo $datastudent->user_id;?>" class="modal" style="z-index:99999999999">
		                    
		                      <!-- Modal content -->
		                      <div class="modal-content">
		                        <div class="modalbg">
		                          <div class="col-sm-12 compose nopadding" style="padding: 0 8px;">
		                            <div class="col-sm-8 nopadding composemsg">
		                              <h3>Not Accepted</h3> 
		                            </div>
		                            <div class="col-sm-4 nopadding composeclose">
		                              <button type="button" class="close closebtn" data-dismiss="modal" aria-hidden="true">&times;</button>
		                            </div>
		                          </div>
		                          <div class="col-sm-12" style="margin-top: 10px;">
		                            
		                            <div class="col-sm-3 nopadding popup">
		                              From:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              
		                              <input type="hidden" name="from_id" id="from_id" class="form-control" value="<?php echo $this->session->userdata('user_id'); ?>"/>
		                              <input type="text" name="from" id="from" class="form-control" value="<?php echo $this->session->userdata('user_name'); ?>" readonly />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              To:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                              <input type="hidden" name="applied_by" id="applied_by" class="form-control" value="staff"/>                                     
		                              <input type="hidden" name="toemail" id="toemail" class="form-control" value="<?php echo $datastudent->email; ?>"/>
		                              <input type="text" name="to" id="to" class="form-control" value="<?php echo $datastudent->staff_fname; ?> <?php echo $datastudent->staff_lname; ?>" readonly />                                        
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Subject:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              <input type="text" name="subject" id="subject" class="form-control" maxlength="251"  />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Message:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                                <textarea name="message" rows="3"  id="message" class="form-control" maxlength="5001"></textarea>
		                            </div>                                            
		                                       
		                            <div class="col-sm-3  popupbtn" style="margin-left:20px; padding-bottom: 20px;">
		                              <input type="submit" name="send" class="compose" id="send" value="Send" />
		                            </div>
		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                  </form>
		                  
		                  <form action="<?php echo base_url()?>shortlistedcandidate/accepted" method="post" id="notificationform" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
		                    <!-- The Modal -->
		                    <div id="myModal_accepted_<?php echo $datastudent->user_id;?>" class="modal" style="z-index:99999999999">
		                    
		                      <!-- Modal content -->
		                      <div class="modal-content">
		                        <div class="modalbg">
		                          <div class="col-sm-12 compose nopadding" style="padding: 0 8px;">
		                            <div class="col-sm-8 nopadding composemsg">
		                              <h3>Accepted</h3> 
		                            </div>
		                            <div class="col-sm-4 nopadding composeclose">
		                              <button type="button" class="close closebtn" data-dismiss="modal" aria-hidden="true">&times;</button>
		                            </div>
		                          </div>
		                          <div class="col-sm-12" style="margin-top: 10px;">
		                            
		                            <div class="col-sm-3 nopadding popup">
		                              From:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              
		                              <input type="hidden" name="from_id" id="from_id" class="form-control" value="<?php echo $this->session->userdata('user_id'); ?>"/>
		                              <input type="text" name="from" id="from" class="form-control" value="<?php echo $this->session->userdata('user_name'); ?>" readonly />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              To:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                              <input type="hidden" name="applied_by" id="applied_by" class="form-control" value="staff"/>                                     
		                              <input type="hidden" name="toemail" id="toemail" class="form-control" value="<?php echo $datastudent->email; ?>"/>
		                              <input type="text" name="to" id="to" class="form-control" value="<?php echo $datastudent->staff_fname; ?> <?php echo $datastudent->staff_lname; ?>" readonly />                                        
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Subject:
		                            </div>
		                            <div class="col-sm-9 nopadding popup">
		                              <input type="text" name="subject" id="subject" class="form-control" maxlength="251"  />
		                            </div>
		
		                            <div class="col-sm-3 nopadding popup">
		                              Message:
		                            </div>
		                            <div class="col-sm-9 nopadding popup"> 
		                                <textarea name="message" rows="3"  id="message" class="form-control" maxlength="5001"></textarea>
		                            </div>                                            
		                                       
		                            <div class="col-sm-3  popupbtn" style="margin-left:20px; padding-bottom: 20px;">
		                              <input type="submit" name="send" class="compose" id="send" value="Send" />
		                            </div>
		                          </div>
		                        </div>
		                      </div>
		                    </div>
		                  </form>
                    <?php 
                  } 
                                    
                } 
                else 
                { ?>
                   <tr><th colspan="7" style="text-align: center; width:1215px;height:100px; background:#FFF; color: #6a7a91;;font-size:25px;">No record to show.</th></tr>
                  <?php 
                } 
                ?>
              </tbody>

          </table>
          
          <div class="box-footer clearfix profile-bg">
            
          <div class="col-sm-4"  style="margin:20px 0px;">
              <?php 
              
              if(count($shortlistedcandidatedata)>0){

                $last_page1=$last_page;
                ?>
                Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
                <?php
              }
              ?>
                            </div>
                            <div class="col-sm-5 ">
              <ul class="pagination pagination-sm no-margin ">
                <?php echo $links;?>
              </ul>
                            </div>
                 <div class="col-sm-3 col-xs-6 totaldiv nopadding">           
        <div class="col-sm-12 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>
 <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>shortlistedcandidate/index" method="get">
         
        <input type="hidden" name="user_srch" id="user_srch" value="<?php echo $user_srch; ?>"  />
         
        <select class="form-control" name="perpage" id="perpage">
   
   <!--<option <?php//if($per_page == "10"){ echo 'selected'; } ?> value="10">10</option>-->
  
  <option value=''>Select</option>
  
  <option <?php if($per_page == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option <?php if($per_page == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option <?php if($per_page == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option <?php if($per_page == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>
 </form>
                            </div>
            </div>
             <!-- <div class="col-sm-12 exceldiv">
                     <form method="post"  name="export_form" action="<?php echo base_url();?>staff/excel_action">
    
              <input type="hidden" name="user_srch" id="user_srch" value="<?php echo $user_srch; ?>"  />
    
                  <input type="submit" name="export" class="btn btn-success excelbtn" value="Export to Excel" />
    
             </form>
                    </div>  -->
</div>
</div>

    </div>
    </div>

    </div>
    
</div> 
    </div>
    
<script type="text/javascript"> 
    jQuery("#perpage").on('change', function(e){ 
        jQuery('#perPageForm').submit();
}); 
       jQuery(".cleanSearchFilter").click(function() {
              jQuery("#stafffilterform")[0].reset();
               window.location.href='<?php echo base_url()?>shortlistedcandidate/index';
            }); 
</script> 
 <script type="text/javascript">
    function delConfirm(id){
        if( confirm("Are you sure want to Delete?") ){
            window.location.href ="<?php echo base_url() ;?>shortlistedcandidate/delete_shortlistedcandidate/"+id;
        }else{
            return false;
        }
}
</script>   
    <script>
  jQuery(document).ready(function(){
        
   jQuery("#invitationForm").validate({
        rules: {
              invited_user_email: {
                required: true,
                email: true,
                maxlength: 120
                 }
            },
        messages: {
              invited_user_email: {
                   required: "Please enter email address.",
                   email: "Please enter a valid email address.",
                   maxlength: "Maximum 120 characters are allowed."
                 }
             },
       
       /* submitHandler: function(form) {
            form.submit();
          }*/
         submitHandler: submitForm
        });
        
        function submitForm() {
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/Invitestaff',
            data : jQuery("#invitationForm").serialize(),
            dataType : "html",
           beforeSend: function()
            {
                jQuery("#Loadingmsg").html('<img src="<?php echo base_url();?>assets/images/ajax-loader.gif"> loading...');
            },
                 success :  function(data) {
                    data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
                      jQuery("#Loadingmsg").hide();
                      jQuery('#invitationError').hide();
                      jQuery('#invitationAlreadyError').hide();
                      jQuery("#invitationForm")[0].reset();
                       jQuery('#invitationSuccess').show();
                       
                        setTimeout(profilesteps, 6000);
                         function profilesteps() {
                             jQuery('#invitationSuccess').hide();
                             jQuery('#myModal').modal('hide');
                             window.location.href = '<?php echo base_url();?>staff';
                           }
                       
                    } else if(data.Status == 'false') {
                        jQuery("#Loadingmsg").hide();
                        jQuery('#invitationSuccess').hide();
                        jQuery('#invitationAlreadyError').hide();
                        jQuery('#invitationError').show();
                    } else if(data.Status == 'alreadyexist') {
                        jQuery("#Loadingmsg").hide();
                        jQuery('#invitationSuccess').hide();
                        jQuery('#invitationError').hide();
                        jQuery('#invitationAlreadyError').show();
                    } 
             }
        });
        return false;
      }
             
     });
         
</script>
    
    <style>
    #srchh{
     margin-bottom: 10px;
    padding: 5px;
    background-color:#fff;
    height:55px;
    }
    #search {
    background-color: #33ab44;
    color: #fff;
    margin: 0 0 5px;
    width: 100%;
}
    #srchh > form {
    float: right;
    padding: 5px;
}

*::after, *::before {
    box-sizing: border-box;
}
*::after, *::before {
    box-sizing: border-box;
}
#srchh > div {
    float: left;
    font-size: 25px;
    padding-left: 10px;
    padding-top:7px;
}
.btn-info {
    background-color: rgb(55, 177, 72);
    border-color:  rgb(55, 177, 72);
}
#perpage {
    float: right;
}
.attendancesec{width:100%;}
.select_box select {
    -moz-appearance: none;
    background-image: url("../images/dropdownicon.png"), linear-gradient(#ffffff, #f3f5f8);
    background-position: 90% center;
    background-repeat: no-repeat;
    border: 1px solid #dfe3e9;
    box-shadow: none;
    height: 30px;
    padding: 0 3px;
    width: 70px;
}

.form-group .staffsrch{
    width: 80% !important;
}
.srchstaff{
    margin:0 0 0 80px;
}
@media screen and (min-width:320px) and (max-width:383px){

#srchh {
    height: 140px !important;
}   
#username {
    border-radius: 4px;
    margin-bottom: 10px;
    width: 100% !important;
}
#srchh > form {
    float: left;
    width: 100%;
}
.input-group {
    width: 100%;
}
}
@media screen and (min-width:384px) and (max-width:480px){
#srchh {
    height: 140px !important;
}
#username {
    border-radius: 4px;
    margin-bottom: 10px;
    width: 100% !important;
}
#srchh > form {
    float: left;
    width: 100%;
}
.input-group {
    width: 100%;
}
}
    

#myModal { margin-top:140px; } 

.save_btn > input {
    background: #37b248 none repeat scroll 0 0;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #fff;
    font-size: 14px;
    padding: 7px 20px;
}
.save_btn {
    padding-top: 20px;
}
.popup_save_btn {
    background: #37b248 none repeat scroll 0 0;
    border: medium none;
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    color: #fff;
    padding: 12px 25px;
}
.modal-body {
    padding: 35px 0 70px;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-radius: 3px;
    min-height: 45px;
    width: 100%;
    padding: 10px;
}
.btn.btn-default {
    background: #37b248 none repeat scroll 0 0;
    color: #fff;
}
.text_box_sec > input {
    border: 1px solid #ccc;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
    min-height: 44px;
    padding: 10px;
    width: 100%;
}
.save_btn {
    text-align: right;
}
.text_box_btn {
    text-align: left;
}
.modal-header {
    text-align: center;
}

.red_line { color:#F00 !important; }
.yellow_line { color:#990 !important; }
.blank_trans_data {
    color: transparent;
}
.search {
    padding: 10px 0 60px !important;
}
.invitemember {
    background: #37b248 none repeat scroll 0 0;
    color: #fff;
    padding-bottom: 5px;
    padding-top: 5px;
}

.close.closenotify{
   background: #fff none repeat scroll 0 0 !important;
    border-radius: 40px;
    float: right;
    height: 40px;
    opacity: 1;
    width: 40px;
    color:#36b047;
}
.inviteheader{
    float:right;
    padding-right:35px;
}
#perpage{
    margin-right:35px;
}
.exceldiv{margin-bottom:10px;}
.blankcol{color:#fff !important;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index:2147483647 !important; /* Sit on top */
    padding-top: 50px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
	/*padding: 20px;
 	border: 10px solid #579b4a;*/
    width: 600px;
    top: 80px;
}

/* The Close Button */
/*.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}
*/
.closebtn{
    background: #fff none repeat scroll 0 0 !important;
    border-radius: 40px;
    float: right;
    height: 40px;
    opacity: 1;
    width: 40px;
	color:#36b047;
}
.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.col-sm-3.popup{
	padding:10px;
}
.col-sm-9.popup{
padding:10px;
}
.compose{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
    padding:5px 30px;
	float:right;
}

.composeclose{
padding:8px 0px 0px 0px;
}
.btncls{padding-top:5px;}
/*.viewmessage{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
	padding:5px 7px;
}*/

.viewmodal> h3 {
   background: #36b047 none repeat scroll 0 0;
   color: #fff;
   text-align: end;
   text-transform: uppercase;
}
.composemsg> h3 {
   background: #36b047 none repeat scroll 0 0;
   color: #fff;
   text-align: end;
   text-transform: uppercase;
}

.attachment_section {
    list-style: outside none none;
    padding-left: 0;
}
.attachment_section li h4 {
	font-size:14px;
}
.icon_section {
    float: right;
}

#myModal > h3 {
    background: #36B047;
    border-radius: 5px;
    color: #fff;
    padding: 13px 0;
    text-align: center;
}	
#myModal .modal-content {
    background-color: #fefefe;
/*    border: 5px solid #36B047;*/
  
    margin: auto;
    padding: 0px;
    top: 30px;
    width: 600px;
}
.ui-autocomplete-input, .ui-menu, .ui-menu-item { z-index: 2147483647; }
.col-sm-3.nopadding.popup {
    font-size: 14px;
    font-weight: bold;
}

.profilemenu_notification {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #dfe3e9;
    border-radius: 5px;
    float: left;
    margin: 10px 0;
    padding: 15px 10px;
    width: 100%;
} 

.bargraph{
    padding: 0 0 0 5px;
}

.graphclass{z-index:0px !important;} 

.toggleclass{padding:7px 0px 10px 0px !important;}
.option{text-align:center;}
.msgbox{
	padding:10px 0 0 20px;
}

.tablewrapper .familydata td i.icnclass{
    color: inherit !important;}
	
.viewmessage:hover{
	color:#09F;
}
.deletemsg:hover{
	color:#09F;
}
.attchmnttxt{font-size:11px;}



.checkboxdiv.profilecheckbox > label {
    width:80px !important;
}
.modalbg {
    background: #fff none repeat scroll 0 0;
    float: left;
    width: 100%;
	border-radius:5px;
	}

.icon_section > a {
    padding-right: 15px;
}
.icon_section > a:hover{
	color:#09F;
}
.icon_section > a:hover{
	color:#09F;
}
.prfltitle > h1 {
    padding-left: 12px;
}
 /***** 16-aug *****/

.ui-autocomplete {
	max-height: 270px;
	overflow-y: auto;
   
	overflow-x: hidden;
   
} 
.admin_msg::after {
    content: none !important;
}
</style>
