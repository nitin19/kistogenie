<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentenrolementreport extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
		
        $this->load->model(array('login_model','authorization_model','studentenrolementreport_model'));
		
		$this->load->database();
		
        $this->load->library('session');
		
		$this->load->library('form_validation');
		
		$this->load->library('image_lib');
		 
    }

	public function index()

	{
		$data 							= array();
		
		$search_condition				= array();
		
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		
		$sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;
		
		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		
		$perpage						= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		

		$start						    = $this->uri->segment(3,0);
		
		$ordrBY							= $this->uri->segment(4,0);
		 
		$dirc							= $this->uri->segment(5,"asc");
		
		$data["base_url"] 				= base_url() . "studentenrolementreport/index";
		
		$school_id						= $this->session->userdata('user_school_id');
		
		$data['total_rows'] 			= $this->studentenrolementreport_model->get_branches($school_id,$branch_search);
		
	
		
		$data['per_page'] 				= $perpage;
		
		$data['postfix_string'] 		= "/?branch=$branch_search&sdate=$sdate_search&edate=$edate_search"; 
		
		$this->pagination->initialize($data);
		
		
		
		if($perpage!='') {

			$data["per_page"] 	= $perpage;

		} else {

			 $data["per_page"]	= 10;

		}

		$data["uri_segment"] 	= 3;
		
		$page 					=  $this->uri->segment(3,0) ;
		
		$data['last_page']		= $page;
		
		$data['branch_search']	= $branch_search;
		
		$data['sdate_search']	= $sdate_search;
		
		$data['edate_search']	= $edate_search;
		
		$data["pagination"] 	= $this->pagination->create_links();
		
		$data['branches'] 		= $this->studentenrolementreport_model->total_branches($school_id,$data["per_page"],$page,$branch_search);
		
		$data['error']			= $this->session->flashdata('error');
		
		$data['success']		= $this->session->flashdata('success');
		
		$this->load->view('header');
		$this->load->view('enrolement/student_enrolement_report', $data);
		$this->load->view('footer');

	}
	
function excel_action()

 		 {
	 		$search_condition	=	array();
			
			$branch_search	   	= 	$this->input->post('branch_search');
    	
 		    $sdate_search     	= 	$this->input->post('sdate_search');
  
 			$edate_search     	= 	$this->input->post('edate_search');

			$school_id      	= 	$this->session->userdata('user_school_id');

			
			if($sdate_search!=''){
	
				$startdate	= date('Y-m-d', strtotime($sdate_search));
	
			} else {
	
				$startdate	= '';
	
			  }
	
			if($edate_search!=''){
	
				$enddate	= date('Y-m-d', strtotime($edate_search));
	
			} else {
	
				$enddate	= '';

		   }
		   
		$studentreportdata=$this->studentenrolementreport_model->getExcelData($school_id,$branch_search);
		
    
   		$this->load->library("Excel");
    
    	$object = new PHPExcel();
     
    	$object->setActiveSheetIndex(0);
     
    $table_columns = array("Branchname", "Applied Students", "Selected Students", "Rejected Students","Waiting Students");
     
    $column = 0;
     
    foreach($table_columns as $field)
    {
     $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
     $column++;
    }
    
      
    $excel_row = 2;
     
    foreach($studentreportdata as $row)
    {
     $branch_id=$row->branch_id;
     
     $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->branch_name);
	 
	 $applied_data=$this->studentenrolementreport_model->get_applied($school_id,$branch_id,$startdate,$enddate);
	 $selected_data=$this->studentenrolementreport_model->get_selected($school_id,$branch_id,$startdate,$enddate);
	 $rejected_data=$this->studentenrolementreport_model->get_rejected($school_id,$branch_id,$startdate,$enddate);
	 $waiting_data=$this->studentenrolementreport_model->get_waiting($school_id,$branch_id,$startdate,$enddate);
	 
     $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row,@$applied_data);
	 
     $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, @$selected_data);
     $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, @$rejected_data);
     $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, @$waiting_data);
 
     $excel_row++;
    
    }
     
    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Student-Enrolement-Report Data.xls"');
    $object_writer->save('php://output');
 }
 
}