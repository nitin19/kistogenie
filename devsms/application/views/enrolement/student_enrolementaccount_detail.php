 <?php 

 		$student_fname = $info->student_fname;

		$student_lname = $info->student_lname;

		$student_gender = $info->student_gender;

		$student_dob = $info->student_dob;
		
		$student_age = $info->student_age;

		$student_nationality = $info->student_nationality;

		$student_country_of_birth = $info->student_country_of_birth;

		$student_first_language = $info->student_first_language;

		$student_other_language = $info->student_other_language;

		$student_religion = $info->student_religion;

		$student_ethnic_origin = $info->student_ethnic_origin;

		$student_telephone = $info->student_telephone;

		$student_address = $info->student_address;

		$student_address_1 = $info->student_address_1;

		$student_father_name = $info->student_father_name;

		$student_father_occupation = $info->student_father_occupation;

		$student_father_mobile = $info->student_father_mobile;

		$student_father_email = $info->student_father_email;

		$student_mother_name = $info->student_mother_name;

		$student_mother_occupation = $info->student_mother_occupation;

		$student_mother_mobile = $info->student_mother_mobile;

		$student_mother_email = $info->student_mother_email;

		$student_emergency_name = $info->student_emergency_name;

		$student_emergency_relationship = $info->student_emergency_relationship;

		$student_emergency_mobile = $info->student_emergency_mobile;

		$student_doctor_name = $info->student_doctor_name;

		$student_doctor_mobile = $info->student_doctor_mobile;

		$student_helth_notes = $info->student_helth_notes;

		$student_allergies = $info->student_allergies;

		$student_other_comments = $info->student_other_comments;

		$student_application_date = $info->student_application_date;

		$student_application_comment = $info->student_application_comment;

		$student_enrolment_date = $info->student_enrolment_date;

		$student_leaving_date = $info->student_leaving_date;

		//$student_certificates = $info->student_certificates;

		$student_family_address = $info->student_family_address;

 		

	   // $student_school_branch = $info->student_school_branch;

		//$student_class_group = $info->student_class_group;

		//$student_fee_band = $info->student_fee_band;

		//$student_sibling = $info->student_sibling;

		

		$this->load->model(array('studentenrolement_model'));

		

		//$branchName = $this->studentenrolement_model->getbranchName($student_school_branch);

	//	$className = $this->studentenrolement_model->getclassName($student_class_group);

	//	$fee_band = $this->studentenrolement_model->getFeedetail($student_fee_band);

	//	$sibling_detail = $this->studentenrolement_model->getSiblingdetail($student_sibling);





?>

<div class="editprofile-content">



    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">
         <ul>
        <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li class="edit">Account Details </li>        

        </ul>

        </div>
       </div>

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-12 col-xs-7 nopadding profile-bg accsection">

        <h1>Account Details</h1>

        </div>

        </div>

        <div class="col-sm-8 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

        <form>

        <div class="profile-bg prfltitle profile_titlebg">

        <h1>General details</h1>

<div class=" col-sm-12 nopadding contentdiv">

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Username</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->username; ?></span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Password</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->password; ?></span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Avtar<br/><span class="tagline">JPG, max. 500KB</span></label>

    <div class="col-sm-9 col-xs-8 profile-picture inputbox">

	<?php if($info->profile_image!='') { ?> <img src="<?php echo base_url();?>uploads/student/<?php echo $info->profile_image; ?>"> 

     <?php } else { ?> 

     <img src="<?php echo base_url();?>assets/images/avtar.png">

     <?php } ?>

     <!-- <div class="profilepicbtns"><i class="fa fa-picture-o"></i><br/><hr class="borderline"><i class="fa fa-trash-o"></i></div>-->

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-3 col-xs-4 control-label nopadding">Full Name</label>

    	<div class="col-sm-9 col-xs-8 inputinfo">

      	<span><?php echo $info->student_fname. ' ' . $info->student_lname; ?></span>

    	</div>

 		</div>

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Age</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $student_age;?></span>

    </div>

  </div>
        <div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Gender</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_gender;?></span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Nationality</label>

  <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_nationality;?></span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Country of Birth</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

     <span><?php echo $info->student_country_of_birth;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">First Language</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

     <span><?php echo $info->student_first_language;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Other Language</label>

    <div class="col-sm-9 col-xs-8 inputbox">

    <span><?php echo $info->student_other_language;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Religion</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->student_religion;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Ethnic Origin</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->student_ethnic_origin;?></span>

    </div>

  </div>
  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Telephone</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->student_telephone;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Full Address</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->student_address;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Address(optional)</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->student_address_1;?></span>

    </div>

  </div>

</div>

</div>

        <div class="profile-bg prfltitle profile_titlebg">

        <h1>Parents details</h1>

		<div class=" col-sm-12 nopadding contentdiv">

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Father's Full Name</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_father_name;?></span>

  </div>

 </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Father's Occupation</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_father_occupation;?></span>

   </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-3 col-xs-4 control-label nopadding">Father's Mobile</label>

    	<div class="col-sm-9 col-xs-8 inputinfo">

      	<span><?php echo $info->student_father_mobile;?></span>

    	</div>

  		</div>


		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Father's Email</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_father_email;?></span>

    </div>

  </div>

        <div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Mother's Full Name</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_mother_name;?></span>

    </div>

  </div>

      <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Mother's Occupation</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_mother_occupation;?></span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Mother's Mobile</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_mother_mobile;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Mother's Email</label>

   <div class="col-sm-9 col-xs-8 inputinfo">

     <span><?php echo $info->student_mother_email;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Emergency Name</label>

    <div class="col-sm-9 col-xs-8 inputbox">

    <span><?php echo $info->student_emergency_name;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Emergency Relationship</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $info->student_emergency_relationship;?></span>

    </div>
 </div>
 		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Emergency Mobile</label>

   <div class="col-sm-9 col-xs-8 inputbox">

    <span><?php echo $info->student_emergency_mobile;?></span>

    </div>

  </div>

  		</div>

</div>
       <div class="profile-bg prfltitle profile_titlebg">

        <h1>Medical details</h1>

		<div class=" col-sm-12 nopadding contentdiv">

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Doctor's Name</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_doctor_name;?></span>

    </div>

</div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Doctor's Mobile</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_doctor_mobile;?></span>

    </div>

</div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-3 col-xs-4 control-label nopadding">Health Notes</label>

    	<div class="col-sm-9 col-xs-8 inputinfo">

      	<span><?php echo $info->student_helth_notes;?></span>

    	</div>

</div>

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Allergies</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_allergies;?></span>

    </div>

</div>

        <div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Other comments</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->student_other_comments;?></span>

    </div>

  </div>

		</div>
       

</div>

<!--        <div class="profile-bg prfltitle profile_titlebg">



        <h1>Branch</h1>

		<div class="col-sm-12 nopadding contentdiv">

        <div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding">Branch</label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span><?php echo $branchName->branch_name;?></span>



    </div>



  </div>

</div>

  		</div>





        <div class="profile-bg prfltitle profile_titlebg">



        <h1>Class Details</h1>

		<div class="col-sm-12 nopadding contentdiv">

  		<div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding">Class / Group</label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span>

	  <?php

	  if($info->student_class_group!=''){

	   echo $className->class_name;

	  }?>

       </span>



    </div>



  </div>



  <div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding">Induction Date</label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span><?php echo date('d/m/Y', strtotime($info->student_application_date));?></span>



    </div>



  </div>



  <div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding">Comments</label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span><?php echo $info->student_application_comment;?></span>



    </div>



  </div>







  <div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding">Enrolment Date</label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span><?php echo date('d/m/Y', strtotime($info->student_enrolment_date));?></span>



    </div>



  </div>



  <div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding">Leaving Date</label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span><?php echo date('d/m/Y', strtotime($info->student_leaving_date));?></span>



    </div>



  </div>



  		</div>

    </div>    

        

        <div class="profile-bg prfltitle profile_titlebg">



        <h1>Fee Status</h1>

		<div class=" col-sm-12 nopadding contentdiv">

  		<div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding">Fee Band</label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span>

	  <?php if($fee_band){

	  echo $fee_band->fee_band_price.' '.$fee_band->fee_band_description;

	  } else { echo "No Fee";}

	  ?>

      </span>



    </div>



  </div>

		</div>

  		</div>
-->
        



       <!-- <div class="profile-bg prfltitle profile_titlebg">



        <h1>Family's Address</h1>

		

        <div class=" col-sm-12 nopadding contentdiv">

  		<div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding">Family Address</label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span><?php echo $info-> student_family_address ;?></span>



    </div>



  </div>

</div>

  		</div>-->



        <!--<div class="profile-bg prfltitle profile_titlebg">



        <h1>Sibling</h1>

        

        <?php if( count($sibling_detail) > 0 ) { 

		$sb = 0;

		foreach($sibling_detail as $sibling) {

			$sb++;

		?>

		<div class=" col-sm-12 nopadding contentdiv">

  		<div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding">Sibling <?php echo $sb;?></label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span><a href="<?php echo base_url();?>students/view_student/<?php echo $sibling->user_id;?>" target="_blank"><?php echo $sibling->student_fname.' '.$sibling->student_lname;?></a></span>



    </div>



  </div>

  </div>

  

  <?php }

   }  else { ?>

		<div class="contentdiv">

  		<div class="form-group detailbox accdetail">



    <label class="col-sm-3 col-xs-4 control-label nopadding"></label>



    <div class="col-sm-9 col-xs-8 inputinfo">



      <span style="padding-left:72px;"><?php echo 'No sibling'; ?></span>



    </div>



    </div>

    </div>

  <?php } ?>



  		</div>-->



        </form>

    



        </div>



        </div>



        <!--<div class="col-sm-4 rightspace fullwidsec">



        <div class="profilecompletion">

        

        

        

        <div class="col-sm-12 prfltitlediv nopadding profiletitltbox">



        <div class="col-sm-12 col-xs-10 prfltitle profile_titlebg"> <h1> Documents </h1> </div>



         <div class="profilecompleted profile-bg docsec">

         <div class="col-sm-12 nopadding contentdiv">

         <ul class="cetificate_section">

         <?php 

		

		 if($info->student_certificates!='') {

			

			 $certificates =  explode(',', $info->student_certificates);

			// echo count($certificates);

			 

			 foreach($certificates as $certificate) { ?>

           <li>

                <h4 id="filename"><?php 

				$string = $certificate ;

                $string1 = (strlen($string) > 5) ? substr($string,0,30).'...' : $string;  

                echo $string1;

				//echo $certificate;?> 

                    <div class="icon_section">

                  <a href="<?php echo base_url();?>students/download_certificates/<?php echo $certificate;?>"><i class="fa fa-download" data-toggle="tooltip" title="Download" data-placement="top"></i></a>

        

                  <a href="<?php echo base_url();?>uploads/student_certificates/<?php echo $certificate;?>" target="_blank"><i class="fa fa-eye" data-toggle="tooltip" title="View" data-placement="top"></i></a>

                  </div>

                  </h4>

               </li> 

			<?php }

		   } else {

		

		      echo 'No Document(s) Attached';

			 

			  }

		 

		 ?>

         </ul>

           </div>

		</div>

        </div>



     

        </div>



        </div>-->



        </div>

        

        <div id="barchart_values" style="width: 900px; height: 300px;"></div>



<script>

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();   

});

</script> 



 <script>

function myFunction() {

    var str = "<?php echo $certificate;?>";

    var res = str.substring(0, 4);

    document.getElementById("filename").innerHTML = res;

}

</script>

<style>

.dashboard-attendance .statusdiv {

    width: 100%;

}

.icon_section {

    float: right;

    padding: 0 15px 0 0;

}

.cetificate_section {

    padding-left:0px;

	list-style:none;

	 font-size: 15px;

}

.cetificate_section > li {

    border: 1px solid #ccc;

    padding: 5px 10px 5px 15px;

}

.prfltitle {

    padding: 0 0px 15px;

}

.fa.fa-download + .tooltip > .tooltip-inner {background-color:#57B94A !important; color:#fff;}

.fa.fa-download + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}

.fa.fa-eye + .tooltip > .tooltip-inner {background-color: #57B94A !important; color:#fff;}

.fa.fa-eye + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}

.cetificate_section li h4 {

    border-bottom: 1px solid #ccc;

    padding-bottom: 10px;

}

.cetificate_section > li {

    border: medium none;



}

.profile-bg {

    border: medium none;

	padding: 0px;

}

.cetificate_section li h4 {

    color: #354052 !important;

    font-size: 14px;

}

.prfltitlediv {

    margin-bottom: 15px;

}

.profile_titlebg h1{background: -webkit-linear-gradient(#ffffff, #d1d1d1);

    background: -o-linear-gradient(#ffffff, #d1d1d1); 

    background: -moz-linear-gradient(#ffffff, #d1d1d1); 

    background: linear-gradient(#ffffff, #d1d1d1);  padding: 10px 15px; }

.profiletitltbox{padding-top:0px;padding-bottom:0px;}

.profiletitltbox .profile_titlebg {

    padding: 0;

}

.contentdiv{

	padding:15px 15px 0px 15px;



}

.accsection h1 {

    padding: 0 0 10px 15px;

}



.docsec{

	margin-bottom:0px;}

.grphdiv{

	margin-left:15px;

}

.prfilleft {

    padding: 0;

}

</style> 