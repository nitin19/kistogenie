<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<?php 
$branch_id = $this->uri->segment(4); // 1stsegment
$staff_id = $this->uri->segment(3); // 2ndsegment
?>
<div class="editprofile-content">
    <div class="profilemenus">
        <ul>
            <?php
                $user_type = $this->session->userdata('user_type');
                if ($user_type == 'teacher') {
                    ?>
                    <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
                <?php } ?>
            <li><a href="<?php echo base_url(); ?>staffenrolement">Staff Process</a></li>
        </li><li class="edit"><a href="#">Add DBS</a></li>
</ul>
</div>
<div style="clear:both"></div>
<?php if ($this->session->flashdata('error')): ?>
        <div class="alert alert-danger alert-dismissable" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?>
<?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissable" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b>
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
<div style="clear:both"></div>
<div class="col-sm-12 nopadding profile-bg accsection">
    <h1>Add Details</h1>
</div>
<form action="<?php echo base_url(); ?>staffenrolement/dbs" method="post" name="staffprofileForm" id="staffprofileForm" enctype="multipart/form-data">
    <div class="col-sm-6 formleft">
        <div class="editprofileform editprofile_left">

            <div class="profile-bg prfltitle profile_titlebg">
                <h2>DBS Details</h2>
                <div class="col-sm-12 nopadding contentdiv">
                    <div class="form-group">
                        <label class="col-sm-3 control-label nopadding">DBS email sent<span>*</span></label>
                        <div class = "col-sm-9 inputbox">
			        <select class = "form-control" name = "dbs_email" id = "dbs_email">
					<option value="yes">YES</option>
					<option value="no">NO</option>
			        </select>
			</div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label nopadding">DBS code received<span>*</span></label>
                        <div class="col-sm-9 inputbox">
                            <select class = "form-control" name = "dbs_code" id = "dbs_code">
					<option value="yes">YES</option>
					<option value="no">NO</option>
			    </select>
                        </div>
                    </div>
               	    <div class = "form-group">
			    <label class = "col-sm-3 control-label nopadding">DBS Completion</label>
			    <div class = "col-sm-9 inputbox">
			        <select class = "form-control" name = "dbs_completion" id = "dbs_completion">
					<option value="yes">YES</option>
					<option value="awaiting">AWAITING</option>
			        </select>
			    </div>
		    </div>
		    
                    <div class="col-sm-6 nopadding">
		        <div class="col-sm-12 confirmlink link pull-right">
		         <input type="hidden" name="branch_id" value="<?php echo $branch_id; ?>" />
		         <input type="hidden" name="staff_id" value="<?php echo $staff_id; ?>" />
		         <input name="Confirmbtn" class="btn btn-default" value="Submit" type="submit">
		        </div>
		    </div>		    
                </div>
            </div>
        </div>
    </div>
</form>
</div>

<style>
#ppp,#ppp1 {   
    background-position: center center;
    background-size: cover;
    -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
    display: inline-block;
}
.profilepicbtns {
  display: inline-flex;
}
i.fa-picture-o {
  cursor: pointer;
}
i:hover {
  opacity: 0.6;
}
#profile_image{
	border:0;
	padding:0;
	}
.cancellink > input {
   background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0 !important;
   color:#FFF;
}

.errspan {
    float: right;
    margin-right:-13px;
    margin-top: -80px;
    position: relative;
    /*z-index: 2;*/
    }
.errspan1{
	margin-top: -305px;
}
.fa-question-circle + .tooltip > .tooltip-inner {background-color:#090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.fa-question-circle{color:green;}
.editprofileform {
    border-radius: 4px;
    float: left;
    margin: 0 2% 0 0;
    width: 48%;
}
.profile_titlebg h2{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1); 
	padding: 10px 15px;
	margin:0px; }
.profiletitltbox{padding-top:0px;}
.profiletitltbox .profile_titlebg {
    padding: 0;
}
.contentdiv{
	padding-left:15px;
	padding-top:15px;

}
.profile-bg {
	padding: 0px;
}
.accsection h1 {
    padding: 0 0 10px 15px;
	color:#393C3E;
}

.formright{
	padding-right:0px;
}
.formleft{
	padding-left:0px;
}
.editprofile_left{width:100%;}
.editprofile_right{width:100%;}
.radio-btn{
    float: left;
    margin: 0 !important;
    width: 40px;
}
.radio-btn > input {
    float: left !important;
    height: auto !important;
}
.radio-btn > span {
    color: #000 !important;
    font-size: 13px !important;
    padding-left: 10px;
}
</style>
<style>
.profilecheckbox .checkbox.cls_day::before{
	left:-20px;
}
.profilecheckbox .checkbox.cls_day::after{
	left:-20px;
}
.checkboxdiv.checkbox_cls {
    padding: 0 0 0 18px;
}

.link input{
    font-size: 19px;
    margin: 0;
	width: 100%;
}
</style>