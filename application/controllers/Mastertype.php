<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mastertype extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','master_model_type'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){

		$id = $this->uri->segment(4);
		
		$search_condition = array();
		$school_id		= $this->session->userdata('user_school_id');
		$type_search	= isset($_GET['type_search'])?$_GET['type_search']:NULL;
		$branch_search		= isset($_GET['branch'])?$_GET['branch']:NULL;
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		if($type_search!=''){
            $search_condition['teacher_type']	= $type_search;
		  }
		$totrows = $this->master_model_type->totaltypes($search_condition, $school_id);
		$data                = array();
		$data["total_rows"]  = count($totrows);
		$data["base_url"]    = base_url() . "teacher_type/index";
		$data["per_page"]    = 5;
		$data["uri_segment"] = 3;
		$this->pagination->initialize($data);
		$page                = $this->uri->segment(3,0) ;
		$data['last_page']	 = $page;
        $data["links2"]       = $this->pagination->create_links();
		$typedata = $this->master_model_type->get_single_record($id);
		$data['info']					= $typedata;
		$data['branch']      = $this->master_model_type->get_branch3($school_id);
		/*$data['teachtype']  = $this->master_model_type->get_types($search_condition, $type_search, $data["per_page"], $page);*/
		
		$data['teachtype']  = $this->master_model_type->get_teach_type($search_condition, $school_id, $data["per_page"], $page);		
		$this->load->view('header');
		$this->load->view('master/teacher_type', $data);
		$this->load->view('footer');
	}
	
	public function add_teach_type() {
    
    	   $school_id		= $this->session->userdata('user_school_id');
		   
		   $typedata = array();
		   $typedata['school_id'] = $school_id;
		   
		   
		  
		   $typedata['branch_id'] = $this->input->post('branch');
		  
		
	       $typedata['teacher_type'] = $this->input->post('select_type');
		   
		
	       $typedata['type_description'] = $this->input->post('desc');
		  
		   
		   $typedata['is_active'] = 1 ;
		   $typedata['is_deleted'] = 0 ;
		   $typedata['created_by'] = $this->session->userdata('user_name');
		   $typedata['created_date'] = date('Y-m-d H:i:s');
		   $typedata['updated_date'] = date('Y-m-d H:i:s');
		   
		   $type = $this->input->post('select_type');
		   $branch = $this->input->post('branch');
		
					
	 	$checkteacher_type = $this->master_model_type->check_teach_type($branch, $type, $school_id);
		           $countteachertype = count($checkteacher_type);
				   
				   if($countteachertype > 0 ) {
					   
					   $this->session->set_flashdata('error', 'Teacher type already exists. Teacher type has not been added.');
				       redirect(base_url()."teacher_type/");exit;
				
				     } else {
					     if($this->master_model_type->insert_teach_type($typedata)) {
							$this->session->set_flashdata('success', 'Teacher type has been added successfuly.');
							redirect(base_url()."teacher_type/");exit; 
						 } else {
							$this->session->set_flashdata('error', 'Some problem exists. Teacher type has not been added.');
							redirect(base_url()."teacher_type/");exit; 
						 }
				   }
	}
	public function update_teachtype(){
			/* $id = $this->uri->segment(4);*/
			  $page = $this->uri->segment(3);
			 $school_id		= $this->session->userdata('user_school_id');
			 $data							= array();
			 $id = $this->input->post('id');
			 
			 $data = array(
               'branch_id' => $this->input->post('branch'),
               'teacher_type' => $this->input->post('select_type'),
               'type_description' => $this->input->post('desc')
); 
			$type = $_POST['select_type'];
			$branch = $_POST['branch'];
			$id = $_POST['id'];
			
			$checkteacher_type = $this->master_model_type->check_update_teach_type($branch, $type, $school_id, $id);
		     $countteachertype = count($checkteacher_type);
				   
				   if($countteachertype > 0 ) {
					    $this->session->set_flashdata('error', 'Teacher Type already exists. Teacher Type has not been Updated.');
				       redirect(base_url()."teacher_type/index/".$page."/".$id);exit;
				
				     } else {
					$updatedata = $this->master_model_type->update_teacher_type($id,$data);
					 if($updatedata = 1){
					 $this->session->set_flashdata('success', 'Data has been updated successfuly.');
				redirect(base_url()."teacher_type/index/".$page."/".$id);exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Type has not been updated.');
				redirect(base_url()."teacher_type/index/".$page."/".$id);exit;
			   }
			 }
		 }
		 public function type_action(){
			$id = $this->uri->segment(3);  
			$activity = $this->uri->segment(4);
			$data							= array();
			if($activity == 'active'){ 
			 $data = array(
				'is_active' => '1'
				);
		  } 
		  if($activity == 'inactive'){ 
			 $data = array(
				'is_active' => '0'
				);
		  } 
		  $actiondata = $this->master_model_type->teacher_action($id, $data);
		  if($actiondata = 1){	
				$this->session->set_flashdata('success', 'Teacher Type has been updated successfuly.');
				redirect(base_url()."teacher_type/");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Type has not been updated.');
				redirect(base_url()."teacher_type/");exit;
			   }
			  
		  }
		  
		  
		  public function checktype_staff(){
			$id = $this->uri->segment(3); 
			$school_id		= $this->session->userdata('user_school_id'); 
			$data = array(
				'is_deleted' => '1'
				);
		$typedata_rows = $this->master_model_type->get_deltype($school_id, $id);
		$teacher_type = $typedata_rows->teacher_type;		 
				
		  $actiondata = $this->master_model_type->check_staffassigntype($teacher_type, $school_id);
		  if($actiondata > 0 ) {					   
					   $this->session->set_flashdata('error', 'Teacher Level has Already Assign to staff. You Cannot Delete it');
				       redirect(base_url()."teacher_type/");exit;	
				     }	   else {
						 			$updatedata = $this->master_model_type->teacher_action($id, $data);
									if( $updatedata = 1){
					 $this->session->set_flashdata('success', 'Data has been Deleted successfuly.');
				redirect(base_url()."teacher_type/");exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem existss. Teacher Level has not been Deleted.');
				redirect(base_url()."teacher_type/");exit;
				}
						 }
		  
		  }
 

/******************** update data***************************/

                   public function display() {
                       $data = array();
                       $data['result'] = $this->master_model_type->get_contents();
				   print_r($data);
                       $this->load->view('teacher_type', $data);
                  }

                  public function edit() {
					   $id = $this->uri->segment(4);
                       $data = array();
                       $data['result'] = $this->master_model_type->entry_update($id);
                      $this->load->view('teacher_type', $data);
                       
                     $info =  $this->master_model_type->entry_update1($id);
            
        }
		
		

 
 
 
	
}