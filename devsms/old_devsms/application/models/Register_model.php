<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register_model extends CI_Model {
	var $tbl		= "school";
	var $users		= "users";
	var $username	= "username";
	var $password	= "password";
	
	
    function __construct() {
        parent::__construct();
    }

  function check_registred_schoolname($schoolname) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where('school_name', $schoolname);
        $query = $this->db->get();
        return  $query->result();
    }
	
   function check_registred_username($Uname) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where('username', $Uname);
        $query = $this->db->get();
        return  $query->result();
    }

   
   function insertSchool($schooldata){
		if($this->db->insert($this->tbl, $schooldata))
			return $this->db->insert_id();
		  else
			return false;
		 //echo $this->db->last_query();exit;
	   }

  function insertUser($userdata){
		if($this->db->insert($this->users, $userdata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}

}
