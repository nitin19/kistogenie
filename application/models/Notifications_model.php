<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notifications_model extends CI_Model {
	var $tbl	 = "notification";
	var $users   = "users";
	var $branch  = "branch";
	var $staff   = "staff";
	var $student = "student";
	var $school = "school";
	var $classes = "classes";
  var $student_enrolement = "student_enrolement";
  var $staff_enrolement = "staff_enrolement";
	

    function __construct() {
        parent::__construct();
    }
	
	public function getuserInfo($user_id,$school_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		    $this->db->where(array("id="=>$user_id,"school_id="=>$school_id,"is_active="=>'1'));
        $query = $this->db->get();
        return  $query->row();
     }
      public function getrecieverInfo($user_id,$school_id) {
        $this->db->select('*');
        $this->db->from($this->users);
        $this->db->where(array("id="=>$user_id,"school_id="=>$school_id,"is_active="=>'1'));
        $query = $this->db->get();
        return  $query->row();
     }
	 
   public function getStaffInfo($user_id,$school_id) {
        $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("user_id="=>$user_id,"school_id="=>$school_id));
        $query = $this->db->get();
        return  $query->row();
     }
   public function getStudentInfo($user_id,$school_id) {
        $this->db->select('*');
        $this->db->from($this->student);
       	$this->db->where(array("user_id="=>$user_id,"school_id="=>$school_id));
        $query = $this->db->get();
        //echo $this->db->last_query();
        return  $query->row();
     }

     public function getEnrolmentStudentInfo($user_id,$school_id) {
        $this->db->select('*');
        $this->db->from($this->student_enrolement);
        $this->db->where(array("user_id="=>$user_id,"school_id="=>$school_id));
        $query = $this->db->get();
        //echo $this->db->last_query();
        return  $query->row();
     }

     public function getEnrolmentStaffInfo($user_id,$school_id) {
        $this->db->select('*');
        $this->db->from($this->staff_enrolement);
        $this->db->where(array("user_id="=>$user_id,"school_id="=>$school_id));
        $query = $this->db->get();
        //echo $this->db->last_query();
        return  $query->row();
     }
   public function studentinfo($user_id,$school_id) {
        $this->db->select('*');
        $this->db->from($this->student);
        $this->db->join($this->users, $this->users.'.id = '.$this->student.'.user_id','INNER'); 
		$this->db->where(array($this->student.".user_id="=>$user_id,$this->student.".school_id="=>$school_id));
        $query = $this->db->get();
       // echo $this->db->last_query();
        return  $query->row();
     }
   public function staffInfo($user_id,$school_id) {
        $this->db->select('*');
        $this->db->from($this->staff);
     $this->db->join($this->users, $this->users.'.id = '.$this->staff.'.user_id','INNER'); 
    $this->db->where(array($this->staff.".user_id="=>$user_id,$this->staff.".school_id="=>$school_id));
        $query = $this->db->get();
        return  $query->row();
     }
  public function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id="=>$branch_id));
        $query = $this->db->get();
        return  $query->row();
    }

  public function getclassName($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("class_id="=>$class_id));
        $query = $this->db->get();
        return  $query->row();
    }
      public function getschoolName($school_id) {
        $this->db->select('*');
        $this->db->from($this->school);
		$this->db->where(array("school_id="=>$school_id));
        $query = $this->db->get();
      //  echo $this->db->last_query();
        return  $query->row();
    }

   public function getAdminInfo($user_id,$school_id) {
        $this->db->select('*');
        $this->db->from($this->school);
		$this->db->where(array("school_id="=>$school_id));
        $query = $this->db->get();
        return  $query->row();
     }
	 
   public function count_notifications($school_id,$user_id,$last_notification_checke_at) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"to="=>$user_id,"is_active="=>'1',"is_deleted="=>'0',"created_date >"=>$last_notification_checke_at,"read_by="=>''));
         $query = $this->db->get();
		 $this->db->last_query(); 
		 return $query->num_rows(); 
       }

	
	public function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->users, $data))
			return true;
		   else
			return false;
		   //echo $this->db->last_query();exit;
	     }
		 
  public function get_notifications($school_id,$user_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"to="=>$user_id,"is_active="=>'1',"is_deleted="=>'0'));
		$this->db->order_by("notification_id", "desc");
		$this->db->limit(10);
         $query = $this->db->get();
		 $this->db->last_query(); 
		 return  $query->result(); 
       }
	   
	   
/*	public function count_notifications($school_id,$user_id,$last_notification_checke_at = "0") {
        $notifications_table = $this->db->dbprefix('notification');

        $sql = "SELECT COUNT($notifications_table.notification_id) AS total_notifications
        FROM $notifications_table
        WHERE $notifications_table.is_deleted=0 AND FIND_IN_SET($user_id, $notifications_table.to) != 0 AND FIND_IN_SET($user_id, $notifications_table.read_by) = 0
        AND timestamp($notifications_table.created_date)>timestamp('$last_notification_checke_at')";

        $result = $this->db->query($sql);
		echo $this->db->last_query(); 
        if ($result->num_rows()) {
            return $result->row()->total_notifications;
        }
    }*/
	
	 public function getRows($where=NULL){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  if($where != NULL){
			$this->db->where($where);
		  }
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	  public function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  if($where != NULL){
			$this->db->where($where);
		  }
          $this->db->order_by($odr, $dirc); 		
		  $this->db->limit($limit, $start);
          $query = $this->db->get();
		 $this->db->last_query(); 
          return  $query->result();
	  }
        
      public function updateNotificationReadby($dataupdatenotification,$whereupnotification){
		$this->db->where($whereupnotification);
		if($this->db->update($this->tbl, $dataupdatenotification))
			return true;
		   else
			return false;
		  // echo $this->db->last_query();exit;
	   } 
	      function getsenderName($sender_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('id'=>$sender_id,'is_deleted='=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
}