<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student_model extends CI_Model {
	
	var $tbl	= "student";
	var $users = "users";
	var $nationality = "nationality";
	var $country = "country";
	var $religion = "religion";
	var $ethnic_origin = "ethnic_origin";
	var $branch = "branch";
	var $fee_band = "fee_band";
	var $classes = "classes";
	var $attendance = "attendance";
	var $staff= "staff";
	var $student_enrolement_email_messages="student_enrolement_email_messages";	

    function __construct() {
        parent::__construct();
    }
	
	
	 function get_payment_email_info($school_id) {
        $this->db->select('*');
        $this->db->from($this->student_enrolement_email_messages);
		$this->db->where(array("school_id="=>$school_id,"student_email_type="=>'payment',"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
	    //echo $this->db->last_query();exit();
        return  $query->row();
    }
     function get_request_email_info($school_id){
		 $this->db->select('*');
        $this->db->from($this->student_enrolement_email_messages);
		$this->db->where(array("school_id="=>$school_id,"student_email_type="=>'requestpayment',"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
	    //echo $this->db->last_query();exit();
        return  $query->row();
	 }
	 
		function getnationality() {
        $this->db->select('*');
        $this->db->from($this->nationality);
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
    }
	
	function getcountry() {
        $this->db->select('*');
        $this->db->from($this->country);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getreligion() {
        $this->db->select('*');
        $this->db->from($this->religion);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getethnicorigin() {
        $this->db->select('*');
        $this->db->from($this->ethnic_origin);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function check_student_username($Uname) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where('username', $Uname);
        $query = $this->db->get();
        return  $query->result();
    }
	function check_student_email($email) {
        $this->db->select('email');
        $this->db->from($this->users);
		$this->db->where('email', $email);
        $query = $this->db->get();
		$this->db->last_query(); 
        return $query->num_rows(); 
    }
	
	
	function getsibling($Favalue,$school_id) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.student_address'=>$Favalue,$this->tbl.'.school_id'=>$school_id,"users.is_active="=>'1',"users.is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
       function getstaff_branches($staff_branch_ids) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id"=>$staff_branch_ids,"is_active"=>'1',"is_deleted"=>'0'));
        $query = $this->db->get();
      //  echo $this->db->last_query();
        return  $query->row();
    }
	function getstaff_classes($staff_class_ids){
         $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("class_id"=>$staff_class_ids,"is_active"=>'1',"is_deleted"=>'0'));
        $query = $this->db->get();
        return  $query->row();

    }
	function getschoolclasses($school_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		
        return  $query->result();
    }
	
	function getclasses($school_id,$schoolbranch) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$schoolbranch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 //echo $this->db->last_query();
        return  $query->result();
    }
	
	function getfeebands($school_id,$BranchID,$schoolclass) {
        $this->db->select('*');
        $this->db->from($this->fee_band);
		$this->db->where(array("school_id="=>$school_id,"branch_id="=>$BranchID,"class_id"=>$schoolclass,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	function getbranchName($student_school_branch) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id="=>$student_school_branch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    	//echo $this->db->last_query();exit;
	}
	
	function getclassName($student_class_group) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("class_id="=>$student_class_group,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
		//echo $this->db->last_query();exit;
    }
	
	function getFeedetail($student_fee_band) {
        $this->db->select('*');
        $this->db->from($this->fee_band);
		$this->db->where(array("fee_band_id="=>$student_fee_band,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	
	function getSiblingdetail($student_sibling) {
		 $ids = explode(',', $student_sibling);
		  $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where_in($this->tbl.'.student_id', $ids);
		  $this->db->where(array("users.is_active"=>'1',"users.is_deleted"=>'0'));
          $query = $this->db->get();
		  $this->db->last_query(); 
          return  $query->result();
    }
	
	
	function insertUser($data){
		if($this->db->insert($this->users, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
	function insertStudent($data){
		if($this->db->insert($this->tbl, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
     
	  
	  function getRows($where=NULL,$school_id,$word_search,$sby){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
		   
		     if($sby=='email') {
		$this->db->where("(users.email LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='phone') {
	   $this->db->where("($this->tbl.student_telephone LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='address') {
	   $this->db->where("($this->tbl.student_address LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='fname') {
	   $this->db->where("($this->tbl.student_father_name LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='mname') {
	   $this->db->where("($this->tbl.student_mother_name LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='femail') {
	   $this->db->where("($this->tbl.student_father_email LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='memail') {
	   $this->db->where("($this->tbl.student_mother_email LIKE '%$word_search%')", NULL, FALSE);
			  } else {
		$this->db->where("($this->tbl.student_fname LIKE '%$word_search%' OR $this->tbl.student_lname LIKE '%$word_search%')", NULL, FALSE);
	    $this->db->or_where("CONCAT(student_fname, ' ', student_lname) LIKE '%".$word_search."%'", NULL, FALSE);
			  }
			// $this->db->where("($this->tbl.student_fname LIKE '%$word_search%' OR $this->tbl.student_lname LIKE '%$word_search%' OR $this->tbl.student_telephone LIKE '%$word_search%' OR users.email LIKE '%$word_search%')", NULL, FALSE);
			//  $this->db->or_where("CONCAT(student_fname, ' ', student_lname) LIKE '%".$word_search."%'", NULL, FALSE);
		  }
		  
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	   function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$word_search,$sby){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			  
			    if($sby=='email') {
		$this->db->where("(users.email LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='phone') {
	   $this->db->where("($this->tbl.student_telephone LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='address') {
	   $this->db->where("($this->tbl.student_address LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='fname') {
	   $this->db->where("($this->tbl.student_father_name LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='mname') {
	   $this->db->where("($this->tbl.student_mother_name LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='femail') {
	   $this->db->where("($this->tbl.student_father_email LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='memail') {
	   $this->db->where("($this->tbl.student_mother_email LIKE '%$word_search%')", NULL, FALSE);
			  } else {
		$this->db->where("($this->tbl.student_fname LIKE '%$word_search%' OR $this->tbl.student_lname LIKE '%$word_search%')", NULL, FALSE);
	    $this->db->or_where("CONCAT(student_fname, ' ', student_lname) LIKE '%".$word_search."%'", NULL, FALSE);
			  }
			// $this->db->where("($this->tbl.student_fname LIKE '%$word_search%' OR $this->tbl.student_lname LIKE '%$word_search%' OR $this->tbl.student_telephone LIKE '%$word_search%' OR users.email LIKE '%$word_search%')", NULL, FALSE);
			//  $this->db->or_where("CONCAT(student_fname, ' ', student_lname) LIKE '%".$word_search."%'", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("users.id", "desc"); 	
		   }
		
		$this->db->limit($limit, $start);
          $query = $this->db->get();
		   $this->db->last_query(); 
           return  $query->result();

	}

    function delete($id){
		$where		= array('id' => $id);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->users, $data))
			return true;
		 else
			return false;
	}

	function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->users, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	
	function get_single_student($sid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.user_id'=>$sid));
          $query = $this->db->get();
          return  $query->row();
    }


  function updateUser($userdata,$whereuser){
		$this->db->where($whereuser);
		if($this->db->update($this->users, $userdata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 function udateStudent($studentdata,$wherestudent){
		$this->db->where($wherestudent);
		if($this->db->update($this->tbl, $studentdata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 
	  function updateCertificates($data,$where){
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		 else
			return false;
		//echo $this->db->last_query();exit;
	 }

    function updateAvtar($data,$where){
		$this->db->where($where);
		if($this->db->update($this->users, $data))
			return true;
		 else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 function student_attendence($attendeceinterval, $id) {
        $this->db->select('*');
        $this->db->from($this->attendance);
		 $this->db->join($this->tbl, $this->tbl.'.student_id = '.$this->attendance.'.student_id','INNER');
		$this->db->where(array($this->tbl.".user_id="=>$id));
		if($attendeceinterval!=''){
			$this->db->where($attendeceinterval);
			}
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
    }
/**********************get students of branches assign to a staff ***********************************/

	function getstaffstudent($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$staff_branch_id,$word_search,$sby) {
		$staff=explode(',',$staff_branch_id);
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
		  $this->db->where_in('student_school_branch',$staff);
           if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			  
			    if($sby=='email') {
		$this->db->where("(users.email LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='phone') {
	   $this->db->where("($this->tbl.student_telephone LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='address') {
	   $this->db->where("($this->tbl.student_address LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='fname') {
	   $this->db->where("($this->tbl.student_father_name LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='mname') {
	   $this->db->where("($this->tbl.student_mother_name LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='femail') {
	   $this->db->where("($this->tbl.student_father_email LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='memail') {
	   $this->db->where("($this->tbl.student_mother_email LIKE '%$word_search%')", NULL, FALSE);
			  } else {
		$this->db->where("($this->tbl.student_fname LIKE '%$word_search%' OR $this->tbl.student_lname LIKE '%$word_search%')", NULL, FALSE);
	    $this->db->or_where("CONCAT(student_fname, ' ', student_lname) LIKE '%".$word_search."%'", NULL, FALSE);
			  }
			// $this->db->where("($this->tbl.student_fname LIKE '%$word_search%' OR $this->tbl.student_lname LIKE '%$word_search%' OR $this->tbl.student_telephone LIKE '%$word_search%' OR users.email LIKE '%$word_search%')", NULL, FALSE);
			//  $this->db->or_where("CONCAT(student_fname, ' ', student_lname) LIKE '%".$word_search."%'", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("users.id", "desc"); 	
		   }
		
		$this->db->limit($limit, $start);
          $query = $this->db->get();
		  $this->db->last_query(); 
           return  $query->result();

    }
	
		 function getstaffRows($where=NULL,$word_search,$staff_branch_id,$sby){
	     $staff=explode(',',$staff_branch_id);
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
		  $this->db->where_in('student_school_branch',$staff);
           if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			  
			   if($sby=='email') {
		$this->db->where("(users.email LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='phone') {
	   $this->db->where("($this->tbl.student_telephone LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='address') {
	   $this->db->where("($this->tbl.student_address LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='fname') {
	   $this->db->where("($this->tbl.student_father_name LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='mname') {
	   $this->db->where("($this->tbl.student_mother_name LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='femail') {
	   $this->db->where("($this->tbl.student_father_email LIKE '%$word_search%')", NULL, FALSE);
			  } elseif($sby=='memail') {
	   $this->db->where("($this->tbl.student_mother_email LIKE '%$word_search%')", NULL, FALSE);
			  } else {
		$this->db->where("($this->tbl.student_fname LIKE '%$word_search%' OR $this->tbl.student_lname LIKE '%$word_search%')", NULL, FALSE);
	    $this->db->or_where("CONCAT(student_fname, ' ', student_lname) LIKE '%".$word_search."%'", NULL, FALSE);
			  }
			// $this->db->where("($this->tbl.student_fname LIKE '%$word_search%' OR $this->tbl.student_lname LIKE '%$word_search%' OR $this->tbl.student_telephone LIKE '%$word_search%' OR users.email LIKE '%$word_search%')", NULL, FALSE);
			//  $this->db->or_where("CONCAT(student_fname, ' ', student_lname) LIKE '%".$word_search."%'", NULL, FALSE);
		  }
		  
          $query = $this->db->get();
		   $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  function getstaff_classid($user_id){
		$this->db->select('*');
		$this->db->from($this->staff);
		$this->db->where(array('user_id'=>$user_id));
		$query=$this->db->get();
		return   $query->row();
		}
		function getstaff_classname($cid){
		$this->db->select('*');
		$this->db->from($this->classes);
		$this->db->where('class_id',$cid);
		$query=$this->db->get();
		//echo $this->db->last_query();	exit();
		return   $query->row();
		}
		
/****************************************************** EXCEL FUNCTION *******************************************************************/
	 
 function getExcelData($where=NULL,$odr=NULL,$dirc=NULL,$school_id,$word_search,$sby){
       $this->db->select('*');
          $this->db->from($this->tbl);
    $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
    $this->db->where(array($this->tbl.'.school_id'=>$school_id));
    if($where != NULL){
   $this->db->where($where);
    }
    
    if($word_search != NULL){
     
       if($sby=='email') {
  $this->db->where("(users.email LIKE '%$word_search%')", NULL, FALSE);
     } elseif($sby=='phone') {
    $this->db->where("($this->tbl.student_telephone LIKE '%$word_search%')", NULL, FALSE);
     } elseif($sby=='address') {
    $this->db->where("($this->tbl.student_address LIKE '%$word_search%')", NULL, FALSE);
     } elseif($sby=='fname') {
    $this->db->where("($this->tbl.student_father_name LIKE '%$word_search%')", NULL, FALSE);
     } elseif($sby=='mname') {
    $this->db->where("($this->tbl.student_mother_name LIKE '%$word_search%')", NULL, FALSE);
     } elseif($sby=='femail') {
    $this->db->where("($this->tbl.student_father_email LIKE '%$word_search%')", NULL, FALSE);
     } elseif($sby=='memail') {
    $this->db->where("($this->tbl.student_mother_email LIKE '%$word_search%')", NULL, FALSE);
     } else {
  $this->db->where("($this->tbl.student_fname LIKE '%$word_search%' OR $this->tbl.student_lname LIKE '%$word_search%')", NULL, FALSE);
     $this->db->or_where("CONCAT(student_fname, ' ', student_lname) LIKE '%".$word_search."%'", NULL, FALSE);
     }
 
    }
    
    if($odr){
   $this->db->order_by($odr, $dirc);   
     } else {
   $this->db->order_by("users.id", "desc");  
     }
  

          $query = $this->db->get();
  //   echo $this->db->last_query(); exit();
           return  $query->result();

 }

	
}
