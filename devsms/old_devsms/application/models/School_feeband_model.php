<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class School_feeband_model extends CI_Model {
	var $tbl	= "fee_band";
	var $classes = "classes";
	var $users = "users";
	var $branch = "branch";
	var $student = "student";
	var $school = "school";
	var $currency="currency";
	
    function __construct() {
        parent::__construct();
    }
	
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
		function getcurrencies() {
        $this->db->select('*');
        $this->db->from($this->currency);
		$query = $this->db->get();
        return  $query->result();
    }
	
	
	function getschoolclasses($school_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		//$this->db->where('school_id', $school_id);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	function check_fee_price($school_id,$branch_id,$class_id,$feeprice) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"fee_band_price"=>$feeprice));
		  $query = $this->db->get();
		  $this->db->last_query(); 
          return $query->result();   
	}
	
	function check_edited_feeprice($school_id,$branch_id,$class_id,$feeprice,$editedFeebandid) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"fee_band_price"=>$feeprice));
		$this->db->where('fee_band_id !=', $editedFeebandid);
		  $query = $this->db->get();
		  $this->db->last_query();
          return $query->result();   
	}
	  
	  
	  function getRows($where=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			//$this->db->like(array($this->tbl.'.student_fname'=>$word_search));
			 $this->db->where("(fee_band_price LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	   function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			//$this->db->like(array($this->tbl.'.student_fname'=>$word_search));
			 $this->db->where("(fee_band_price LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("fee_band_id", "desc"); 	
		   }
		
		     $this->db->limit($limit, $start);
             $query = $this->db->get();
		     $this->db->last_query(); 
             return  $query->result();

	}
	
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	
	function getclassName($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where('class_id', $class_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	
	/*function getclasses($schoolbranch,$school_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		//$this->db->where('branch_id', $schoolbranch);
		$this->db->where(array("branch_id"=>$schoolbranch,"school_id="=>$school_id));
        $query = $this->db->get();
        return  $query->result();
    }*/
	
	   function insertFee($feebanddata){
		if($this->db->insert($this->tbl, $feebanddata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	   }
	   
	   function get_single_feeband($feebandid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('fee_band_id', $feebandid);
          $query = $this->db->get();
          return  $query->result();
    }
	
	 function updateFee($feebanddata,$where) {
		$this->db->where($where);
		if($this->db->update($this->tbl, $feebanddata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	  }
	 
	  function check_deletedfeeband($school_id,$feebandid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		 // $this->db->where('class_id', $classid);
		  $this->db->where(array("school_id"=>$school_id,"fee_band_id"=>$feebandid));
          $query = $this->db->get();
          return  $query->row();
      }
	  
	  /*function check_student_class($school_id,$branch_id,$classid) {
          $this->db->select('*');
          $this->db->from($this->student);
		  $this->db->where(array("school_id"=>$school_id,"student_school_branch"=>$branch_id,"student_class_group"=>$classid));
          $query = $this->db->get();
		  echo $this->db->last_query();
          return  $query->result();
       }*/
	   
	    function check_student_feeband($school_id,$branch_id,$class_id,$feebandid) {
          $this->db->select('*');
          $this->db->from($this->student);
		  $this->db->join('users', 'users.id = '.$this->student.'.user_id','INNER'); 
		 // $this->db->where('class_id', $classid);
		  $this->db->where(array($this->student.".school_id"=>$school_id,$this->student.".student_school_branch"=>$branch_id,$this->student.".student_class_group"=>$class_id,$this->student.".student_fee_band"=>$feebandid,"users.is_active"=>'1',"users.is_deleted"=>'0'));
          $query = $this->db->get();
		   $this->db->last_query();
          return  $query->result();
         }
	   
	  function delete($feebandid){
		$where		= array('fee_band_id' => $feebandid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		 else
			return false;
	   }
	   
	   
/******************************************************************************************************/
	   
	   function getclasses($schoolbranch,$school_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("branch_id"=>$schoolbranch,"school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
      }
	
}



 
