<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="#">Terms</a></li>        

        </ul>

        </div>

        </div>
 <!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>
           
  <div class="col-sm-12 addform nopadding"> 
 <?php if($info) { ?>
  <form name="addterm" id="addterm" method="post"  action="<?php echo base_url(); ?>terms/update_term/<?php echo $last_page ?>/<?php echo $info->term_id; ?>">
  <?php } else{		?>
  <form name="addterm" id="addterm" method="post"  action="<?php echo base_url(); ?>terms/add_new_term" autocomplete="off">
  <?php } ?>
  
  
  <div class="col-sm-7 datafie">
  <div class="col-sm-4 ">
      <label class="control-label nopadding filterlabel">Terms: </label>
    </div>
  
 <div class="col-sm-4 inputbox termselect ">
      <select class="form-control" name="branch" id="branch">

  <option value="">Select Branch</option>  
     <?php
   			foreach($branch as $branchdetail)
					{
						$name = $branchdetail->branch_name;
						$id = $branchdetail->branch_id;
						if($id == $info->branch_id)
						{ 
						echo "<option value='$id' selected>$name</option>";	
						}
						else
						{
						echo "<option value='$id'>$name</option>";	
						} 
						
					}
					?>	
             
    </select>

    </div>
    
     <div class="col-sm-4 inputbox termselect ">

       <?php if($info) {  ?>
 		<input name="id" id="id" class="form-control" type="hidden" value="<?php echo $info->term_id; ?>">
      <input name="termname" id="termname" class="form-control" type="text" value="<?php echo $info->term_name; ?>">
       <?php   } else { ?>
       <input name="termname" id="termname" class="form-control" type="text" placeholder="Enter Term Name" autocomplete="off" maxlenght="61">
<?php } ?>

    </div>
    </div>
     <div class="col-sm-7 datafie">
    <div class="col-sm-4 ">

        <label class="control-label nopadding filterlabel">Description:</label>

    </div>
    <div class="col-sm-8 inputbox termselect">

<?php if($info) { ?>
      <textarea  placeholder="Write Description" class="form-control" name="description" id="description"><?php echo $info->term_description; ?></textarea>
 <?php } else { ?>   
      <textarea  placeholder="Write Description" class="form-control" name="description" id="description" autocomplete="off" maxlenght="251"></textarea>
<?php } ?>

    </div>
  </div>
  <div class="col-sm-7 datafie">

    <div class="col-sm-12 nopadding rbtn">

<?php if($info) { ?>
     <input value="Update" class="btn btn-default" id="update" name="update" type="submit">
       <?php } else { ?>
      <input value="Add" class="btn btn-default" id="add" name="add" type="submit">
<?php } ?>

    </div>
  </div>
  </form>
    </div>	

        

        <div class="attend">	

     

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">
    <div id="srrch">

    <h1>Term Details</h1>
     
<form action="<?php echo base_url(); ?>terms/index" name="search_term" id="search_term">
<select class="form-control" name="branch" id="branch" style="height: 37px;">
  <option value="">Select Branch</option>  
     <?php
   		   	foreach($branch as $branchdetail)
					{
						$name = $branchdetail->branch_name;
						$id = $branchdetail->branch_id;
						echo "<option value='$id'>$name</option>";	
						}
		?>	
             
    </select>
<input name="term_srch" placeholder="Search by Term" id="term_srch" class="form-control" style="width: 180px; height:36px; border-radius:0px;" type="text">
<input value="Search" class="btn btn-default" id="search" name="search" type="submit">	
</form>
</div>	
</div>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2">S.No.</th>

					  <th class="column5"> School Branch</th>

					  <th class="column4"> Term</th>
                      
                      <th class="column5"> Description</th>
                        
					  <th class="column4">Action</th>

                     

				  </tr>

			  </thead>

				<tbody>
				<?php
		if($detail)	{
			$sr=$last_page;
	    	    foreach ( $detail as $termdetail )
        				{
			
            ?>
					<tr class="familydata staff">
						<td class="column2"><?php echo ++$sr; ?> </td>
						<td class="column5"> 
			  <?php 
                $branch_id = $termdetail->branch_id;
            	$this->load->model(array('term_model'));
            	$branchName = $this->term_model->getbranchName($branch_id);
   				echo $branchName->branch_name; 
    		?>
						</td>
                        <td class="column4"><?php echo $termdetail->term_name; ?></td>
                        
                      <td class="column5"><?php echo $termdetail->term_description; ?></td>  
                        
                        <td class="column4">
											<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
													<li>

<a href="<?php echo base_url(); ?>terms/index/<?php echo $last_page ?>/<?php echo $termdetail->term_id?>"> Edit </a>
													</li>
                                                    <li>
		 <?php
        $term_id = $termdetail->term_id;
        $this->load->model(array('term_model'));
        $singlerole = $this->term_model->getterm($term_id);
            if($singlerole->is_active == "1") { ?>
        <a href="<?php echo base_url(); ?>terms/role_action/<?php echo $termdetail->term_id?>/inactive" title="click to deactivate Term"> Active </a>
        <?php } else { ?>
        <a href="<?php echo base_url(); ?>terms/role_action/<?php echo $termdetail->term_id?>/active" title="click to activate Term">De-Activated </a>
        <?php } ?>
</li>
		
													<li class="divider"></li>
<li> <a onclick="delConfirm('<?php echo $termdetail->term_id ?>')" style="cursor: pointer;"> Delete </a> </li>
												</ul>
											</div>
										</td>
 		<?php	}		
				} else { ?>
 				<tr><th colspan="7" style="text-align: center; width:1215px;height:100px;Font-size:25px; background:#FFF;">No record to show.</th></tr>
				 <?php } ?>
				</tbody>

		  </table>



	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-9 col-xs-6 paginationblk">
     		<? 
				if($detail){
						$last_page1=$last_page;
			?>
							<span id="shortlist">	Showing <?=++$last_page;?>  to  <?=$sr++;?> of <?=$total_rows++;?>  entries </span>
								
			<?	} ?>
	<ul class="pagination">
		<?=$links;?>	
	</ul>

    </div>

    <div class="col-sm-3 col-xs-6 totaldiv nopadding">

    <div class="col-sm-12 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="" method="post">

  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option   value="20">20</option>

  <option  value="30">30</option>

  <option value="40">40</option>

  <option value="50">50</option>

  <option  value="100">100</option>

 </select>

</form>

        </div>

    </div>

	</div>  

</div>

	</div>

    

	</div>

    </div>
        
 <script>
  jQuery(document).ready(function(){
  jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });
    jQuery("#addterm").validate({
        rules: {
               branch: {
                required: true
               },
			  termname: {
                required: true,
				alphanumeric: true,
				maxlength: 60
               },
			   description: {
                required: true
               }
        },
        messages: {
               branch: {
                  required: "Please Select Branch",
                 },
				  termname: {
                  required: "Please enter Term",
				  alphanumeric: "Please Enter Character and Numberic value only",
				  maxlength: "Your term can not be more than 60 characters long"
                 },
				 description: {
                  required: "Please Enter Description",
                 }	  
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
	});
</script>
 
 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#message').delay(10000).fadeOut();
      });
	  
	   jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
	  </script>
  
 <script type="text/javascript">
	function delConfirm(term_id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>terms/del_term/"+term_id;
		}else{
			return false;
		}
	}
</script>


 <style>
	
	#srrch{background-color:#fff; height:60px; }

.btn-info {
    background-color: rgb(55, 177, 72);
	border-color:  rgb(55, 177, 72);
	
}
#srrch #branch {
    width: 100%;
}
#srrch > h1 {
    border: 0 none;
    float: left;
    padding: 18px 0 0 20px;
}
#srrch > form {
    float: right;
	padding:13px;
	display: flex;
}
.addform{height:200px; background-color:#fff; margin-bottom: 10px;  padding: 20px 0 0 10px;}
.datafie {
    margin-bottom: 10px;
}
.addform select, #srrch select {
    -moz-appearance: none;
    background-image: url ("../images/dropdownicon.png"), linear-gradient(rgb(255, 255, 255), rgb(243, 245, 248));
    background-repeat: no-repeat;
    border: 1px solid rgb(223, 227, 233);
    box-shadow: none;
    height: 35px;
    width: calc(100% - 50px);
	background-position: 90% center;
    padding: 0 4px;
    width: 100%;
}
 #add.btn.btn-default {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    color: rgb(255, 255, 255);
    border: 1px solid #249533;
    padding: 6px 20px;
	border-radius:3px;
      
}
 #update.btn.btn-default {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    color: rgb(255, 255, 255);
    border: 1px solid #249533;
    padding: 6px 20px;
	border-radius:3px;
      
}
.pagination{margin:0;}
#type_search {
    border-bottom-left-radius: 5px !important;
    border-top-left-radius: 5px !important;
}
#search.btn.btn-default {
    max-height: 36px;
    padding: 0 30px 1px;
	background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    color: rgb(255, 255, 255);
    border: 1px solid #249533;
   	border-radius:3px;
}
#branch {margin-right:2%;}
.col-sm-12.rbtn {
  text-align:right;
}
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px 0 0;
}
.col-sm-12.paginationdiv.nopadding {
    background: #fff none repeat scroll 0 0;
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    padding: 15px;
}
#term_srch {
    border-radius: 4px !important;
    margin-right: 2%;
}

@media screen and (min-width:320px) and (max-width:480px){
.addform {
    height: auto;
    margin-bottom: 10px;
    padding: 20px 0 10px 10px;
}
#branch {
    font-size: 12px;
}
#term_srch {
    font-size: 12px;
    width: 100% !important;
}
#search.btn.btn-default {
    padding: 0 5px 1px;
}
#srrch {
    height: 185px;
    margin-bottom: 10px;
}
#srrch > form {
    display: inline-block;
    float: right;
    padding: 13px;
    width: 100%;
}
#search_term #term_srch {
    margin-top: 10px;
    width: 100% !important;
}
#search.btn.btn-default {
 padding: 3px 25px;
margin-top:8px;
}
}
@media screen and (min-width:481px) and (max-width:767px){
.headerleft {
    float: left;
}
.counter{
	margin-top:55px;
}
.datafie .inputbox.termselect {
    padding-bottom: 10px;
}
.addform {
    height: auto;
    margin-bottom: 10px;
    padding: 20px 0 10px 10px;
}
#srrch {
    height: 185px;
    margin-bottom: 10px;
}
#srrch > form {
    display: inline-block;
    float: right;
    padding: 13px;
    width: 100%;
}
#term_srch {
    border-radius: 4px !important;
    margin-right: 2%;
    margin-bottom: 10px;
}

#search_term #term_srch {
    margin-top: 10px;
    width: 100% !important;
}
#search.btn.btn-default {
 padding: 3px 25px;
}
}
@media screen and (min-width:768px) and (max-width:991px){
.datafie {
    margin-bottom: 10px;
    width: 100% !important;
}
#srrch {
    background-color: #fff;
    height: 120px;
    margin-bottom: 10px;
}
}
	</style>
	