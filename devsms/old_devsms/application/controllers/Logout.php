<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {
	function __construct(){
        parent::__construct();
        
    }

	public function index()	{
			if( $this->authorize->do_user_logout() == true ){
				$this->session->set_flashdata('success', 'Successfuly logged out.');
				redirect(base_url());
			}else{
				$this->session->set_flashdata('error', 'Problem in Loggin out. Try again.');
				redirect(base_url().'dashboard');exit;
			}
	
	}
}




