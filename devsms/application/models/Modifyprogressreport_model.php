<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Modifyprogressreport_model extends CI_Model {
	var $tbl	= "progress_report";
	var $attendance	= "attendance";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $classes = "classes";
	var $staff = "staff";
	var $subjects = "subjects";
	var $term = "term";
	var $school= "school";
	var $report_template="student_report_template";
	var $student_report_levels="student_report_levels";
	var $student_report_sublevels="student_report_sublevels";
	var $report_dropdowns	= "report_dropdowns";
	
	
    function __construct() {
        parent::__construct();
    }
	
	
	function  getStudentinfo($student_id) {
        $this->db->select('*');
        $this->db->from($this->student);
		$this->db->where('student_id', $student_id);
        $query = $this->db->get();
        return  $query->row();
	   }
	
 function getbranchName($student_school_branch) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id="=>$student_school_branch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getclassName($student_class_group) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("class_id="=>$student_class_group,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }	
	
	function getclassNameArr($student_class_group) {
		$ids = explode(',', $student_class_group); 
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where_in('class_id', $ids);
		$this->db->where(array("is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	 function get_total_Attendancestatus($studentid) {
		     $this->db->select('*');
		     $this->db->from($this->attendance);
			 $this->db->where('student_id', $studentid);
			 $query = $this->db->get();
			  $this->db->last_query();
             return  $query->num_rows();
	         }
			 
	
	 function get_present_Attendancestatus($studentid) {
		     $this->db->select('*');
		     $this->db->from($this->attendance);
			 $this->db->where('student_id', $studentid);
			 $this->db->where('attendance_status', '1');
			 $query = $this->db->get();
			  $this->db->last_query();
             return  $query->num_rows();
	         }
	  
	   function get_absent_Attendancestatus($studentid) {
		     $this->db->select('*');
		     $this->db->from($this->attendance);
			 $this->db->where('student_id', $studentid);
			 $this->db->where('attendance_status', '2');
			 $query = $this->db->get();
			  $this->db->last_query();
             return  $query->num_rows();
	         } 
			 
	 function get_late_Attendancestatus($studentid) {
		     $this->db->select('*');
		     $this->db->from($this->attendance);
			 $this->db->where('student_id', $studentid);
			 $this->db->where('attendance_status', '3');
			 $query = $this->db->get();
			  $this->db->last_query();
             return  $query->num_rows();
	         } 
	
	function getSubjects($school_id,$branch_id,$class_id) {
        $this->db->select('*');
        $this->db->from($this->subjects);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }
	
	
	function get_subject_name($subjectid,$school_id,$branch_id,$class_id) {
        $this->db->select('*');
        $this->db->from($this->subjects);
		$this->db->where(array("subject_id="=>$subjectid,"school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
         return  $query->row();
    }
	
	
	function getTerms($school_id) {
        $this->db->select('*');
        $this->db->from($this->term);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }


 function check_progress_report($student_id,$school_id,$branch_id,$class_id,$term,$year) {
		$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("student_id="=>$student_id,"school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"term"=>$term,"progress_report_year"=>$year,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
	   }
	
	
		 
	 
function updateReport($input){
  	$student_id  	= 	$input['student_id'];
    $school_id  	=   $input['school_id'];;
	$branch_id   	= 	$input['branch_id'];
	$class_id    	= 	$input['class_id'];
	$term       	= 	$input['term'];
	$subject_id  	= 	$input['subject_id'];
	$staff_id		=	$input['teacher'];

	$effort  		= 	$input['effort'];
	$behaviour  	= 	$input['behaviour'];
	$homework  		= 	$input['homework'];
	//$proofreading  	= 	$input['proofreading'];
	$startlevel		=	$input['startlevel'];
	$currentlevel	=	$input['currentlevel'];
	$targetlevel	=	$input['targetlevel'];

	$positive_character_reference  		= 	$input['Positive_character_reference'];
	$current_surah  					= 	$input['Current_surah'];
	$tajweed_reference  				= 	$input['Tajweed_reference'];
	$previous_surahs  					= 	$input['Previous_surahs'];
	$aod_behaviour  					= 	$input['Behaviour'];
	$aod_progress						= 	$input['Progress'];
	$aod_homework  						= 	$input['Homework'];
	$fluency_in_reading  				= 	$input['Fluency_in_reading'];
	$dua  								= 	$input['Dua'];
	$level_of_understanding  			= 	$input['Level_of_Understanding'];
	$dua_progress  						= 	$input['Dua_Progress'];
	$vocabulary  						= 	$input['Vocabulary'];
	$speaking_writing  					= 	$input['Speaking/Writing'];


	$comment  				= 	$input['comment'];
	$account_of_achievement	=	$input['account_of_achievement'];
	$area_development		=	$input['area_development'];
	$topic_covered			=	$input['topic_covered'];
	
	$pgryear 		= date('Y');
	$created_by 	= $this->session->userdata('user_id');
	$created_date 	= date('Y-m-d H:i:s');;
	$updated_date 	= date('Y-m-d H:i:s');
	$is_active 		= '1';
	
	$data =array();
	for ($i = 0; $i < count($subject_id); $i++) {
		 $sv = $subject_id[$i];
		 
		 $data[$i] = array(
		   'student_id' 			=> $student_id, 
           'school_id' 				=> $school_id, 
           'branch_id' 				=> $branch_id,
           'class_id' 				=> $class_id,
		   'term' 					=> $term,
           'subject_id' 			=> $sv,

           'staff_id'				=> $staff_id[$sv],
		   'effort' 				=> $effort[$sv], 
           'behaviour' 				=> $behaviour[$sv],
           'home_work' 				=> $homework[$sv],
		   //'proof_reading' => $proofreading[$sv],
		   'start_level' 			=> $startlevel[$sv],
		   'current_level' 			=> $currentlevel[$sv],
		   'target_level' 			=> $targetlevel[$sv],
        
           'positive_character_reference' => $positive_character_reference[$sv],
           'current_surah' 			=> $current_surah[$sv],
           'tajweed_reference' 		=> $tajweed_reference[$sv],
           'previous_surahs' 		=> $previous_surahs[$sv],
           'aod_behaviour' 			=> $aod_behaviour[$sv],
           'aod_progress' 			=> $aod_progress[$sv],
           'aod_homework' 			=> $aod_homework[$sv],
           'fluency_in_reading' 	=> $fluency_in_reading[$sv],
           'dua' 					=> $dua[$sv],
           'level_of_understanding' => $level_of_understanding[$sv],
           'dua_progress' 			=> $dua_progress[$sv],
           'vocabulary' 			=> $vocabulary[$sv],
           'speaking_writing' 		=> $speaking_writing[$sv],

           'comment' 				=> $comment[$sv],
           'account_of_achievement' => $account_of_achievement[$sv],
		   'area_development' 		=> $area_development[$sv],
		   'topic_covered' 			=> $topic_covered[$sv],
		   'progress_report_year' 	=> $pgryear,
		   'is_active' 				=> $is_active,
		   'created_by' 			=> $created_by, 
           'created_date' 			=> $created_date,
           'updated_date' 			=> $updated_date,
		   
          );
	}
	
	
	$this->db->where(array('student_id' => $student_id,'school_id' => $school_id, 'branch_id' => $branch_id,'class_id' => $class_id, 'term' => $term,'progress_report_year' => $pgryear));
	
		if($this->db->update_batch($this->tbl, $data, 'subject_id')) {
		//echo $this->db->last_query();exit;
		return true;
		} else {
		//echo $this->db->last_query();exit;
		return false;
		     }
	  }
	  
	  
 function insertReport($input){
  	$student_id  	= 	$input['student_id'];
    $school_id  	=   $input['school_id'];;
	$branch_id   	= 	$input['branch_id'];
	$class_id    	= 	$input['class_id'];
	$term       	= 	$input['term'];
	$subject_id  	= 	$input['subject_id'];

	$teacher		=	$input['teacher'];
	$effort  		= 	$input['effort'];
	$behaviour  	= 	$input['behaviour'];
	$homework  		= 	$input['homework'];
	//$proofreading  	= 	$input['proofreading'];
	$startlevel		=	$input['startlevel'];
	$currentlevel	=	$input['currentlevel'];
	$targetlevel	=	$input['targetlevel'];

	$positive_character_reference  		= 	$input['Positive_character_reference'];
	$current_surah  					= 	$input['Current_surah'];
	$tajweed_reference  				= 	$input['Tajweed_reference'];
	$previous_surahs  					= 	$input['Previous_surahs'];
	$aod_behaviour  					= 	$input['Behaviour'];
	$aod_progress						= 	$input['Progress'];
	$aod_homework  						= 	$input['Homework'];
	$fluency_in_reading  				= 	$input['Fluency_in_reading'];
	$dua  								= 	$input['Dua'];
	$level_of_understanding  			= 	$input['Level_of_Understanding'];
	$dua_progress  						= 	$input['Dua_Progress'];
	$vocabulary  						= 	$input['Vocabulary'];
	$speaking_writing  					= 	$input['Speaking/Writing'];

	$comment  				= 	$input['comment'];
	$account_of_achievement	=	$input['account_of_achievement'];
	$area_development		=	$input['area_development'];
	$topic_covered			=	$input['topic_covered'];
	
	$pgryear 		= date('Y');
	$created_by 	= $this->session->userdata('user_id');
	$created_date 	= date('Y-m-d H:i:s');;
	$updated_date 	= date('Y-m-d H:i:s');
	$is_active 		= '1';
	
	$data =array();
	for ($i = 0; $i < count($subject_id); $i++) {
		 $sv = $subject_id[$i];
		 
		 $data[$i] = array(
		   'student_id' 	=> $student_id, 
           'school_id' 		=> $school_id, 
           'branch_id' 		=> $branch_id,
           'class_id' 		=> $class_id,
		   'term' 			=> $term,
           'subject_id' 	=> $sv,

           'staff_id'		=> $teacher[$sv],
		   'effort' 		=> $effort[$sv], 
           'behaviour' 		=> $behaviour[$sv],
           'home_work' 		=> $homework[$sv],
		   //'proof_reading' => $proofreading[$sv],
		   'start_level' 	=> $startlevel[$sv],
		   'current_level' 	=> $currentlevel[$sv],
		   'target_level' 	=> $targetlevel[$sv],

		   'positive_character_reference' => $positive_character_reference[$sv],
           'current_surah' 			=> $current_surah[$sv],
           'tajweed_reference' 		=> $tajweed_reference[$sv],
           'previous_surahs' 		=> $previous_surahs[$sv],
           'aod_behaviour' 			=> $aod_behaviour[$sv],
           'aod_progress' 			=> $aod_progress[$sv],
           'aod_homework' 			=> $aod_homework[$sv],
           'fluency_in_reading' 	=> $fluency_in_reading[$sv],
           'dua' 					=> $dua[$sv],
           'level_of_understanding' => $level_of_understanding[$sv],
           'dua_progress' 			=> $dua_progress[$sv],
           'vocabulary' 			=> $vocabulary[$sv],
           'speaking_writing' 		=> $speaking_writing[$sv],

           'comment' 				=> $comment[$sv],
		   'account_of_achievement' => $account_of_achievement[$sv],
		   'area_development' 		=> $area_development[$sv],
		   'topic_covered' 			=> $topic_covered[$sv],
		   'progress_report_year' 	=> $pgryear,
		   'is_active' 				=> $is_active,
		   'created_by' 			=> $created_by, 
           'created_date' 			=> $created_date,
           'updated_date' 			=> $updated_date,
          );
	}
	
		//$this->db->insert_batch($this->tbl, $data); 
		if($this->db->insert_batch($this->tbl, $data))
			return $this->db->insert_id();
		   else
			return false;
		 //echo $this->db->last_query();exit;
     }
   
   /************************************************* to fetch all record template (Cheshta)************************************************/
   
   function report_template(){
	    $this->db->select('*');
        $this->db->from($this->report_template);
		$this->db->where(array("is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
		   
	   }
	
	function get_template_decs($tmpltid,$catid){
	    $this->db->select('*');
        $this->db->from($this->report_template);
		$this->db->where(array("is_deleted="=>'0',"template_id="=>$tmpltid,"subcategory_id="=>$catid));
        $query = $this->db->get();
		//echo  $this->db->last_query(); exit();
        return  $query->result();
		   
	   }
	   
	function getSchoolinfo($school_id){
	    $this->db->select('*');
        $this->db->from($this->school);
		$this->db->where(array("is_deleted="=>'0',"school_id="=>$school_id));
        $query = $this->db->get();
		//echo  $this->db->last_query(); 
        return  $query->result();
		   
	   }
	function getBranchinfo($branch_id){
	    $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("is_deleted="=>'0',"branch_id="=>$branch_id));
        $query = $this->db->get();
		//echo  $this->db->last_query(); exit();
        return  $query->result();
		   
	   }

	function getClassinfo($class_id){
	    $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("is_deleted="=>'0',"class_id="=>$class_id));
        $query = $this->db->get();
		//echo  $this->db->last_query(); exit();
        return  $query->row();
		   
	   }

	function report_level($sub_name){
	    $this->db->select('*');
        $this->db->from($this->student_report_levels);
		$this->db->where(array("subject_name="=>$sub_name));
        $query = $this->db->get();
		//echo  $this->db->last_query(); exit();
        return  $query->result();
		   
	   }

	function get_subcategory($subject_name,$cat_name) {
          $this->db->select('*');
          $this->db->from($this->report_dropdowns);
		  $this->db->where(array('subject_name' => $subject_name,'cat_name' => $cat_name ));
          $query = $this->db->get();
          return  $query->result();
           //echo $this->db->last_query(); exit();
     }

	function under_subcategory($subcategory_id , $subject_name) {
          $this->db->select('*');
          $this->db->from($this->report_template);
		  $this->db->where(array('subcategory_id' => $subcategory_id,'subject_name' => $subject_name,"is_deleted="=>'0'));
          $query = $this->db->get();
          return  $query->result();
           //echo $this->db->last_query(); exit();
     }
	function get_curentlvl_sublevels($curnt_lvl_id) {
          $this->db->select('*');
          $this->db->from($this->student_report_sublevels);
		  $this->db->where(array('level_id' => $curnt_lvl_id));
          $query = $this->db->get();
		 // echo $this->db->last_query();
          return  $query->result();
     }
	 function  get_surah_name($tmpltid) {
        $this->db->select('*');
        $this->db->from($this->student_report_sublevels);
		$this->db->where('sublevel_id', $tmpltid);
        $query = $this->db->get();
        return  $query->row();
	   }

  /************************************************* to fetch all record template (Cheshta)************************************************/

/******************************** staff according to subjects 10-jan-2018 **************/

  	function get_staff($subject_id){
	    $this->db->select('*');
        $this->db->from($this->staff);
        $this->db->join('users', 'users.id = '.$this->staff.'.user_id','INNER'); 
	    $this->db->where(array('users.is_deleted=' => '0'));
		 if($subject_id != NULL){
		 	$where = "FIND_IN_SET('".$subject_id."', $this->staff.subject_name)";
			$this->db->where($where);
		  }	
        $query = $this->db->get();
		//echo  $this->db->last_query(); exit();
        return  $query->result();
		   
	   } 
   
}
