<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

          <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li class="edit">Enrolment Template</li>        

        </ul>

        </div>

        </div>

    	
<div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>

        

        <div class="attendancesec">	
        
        
        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">


             <?php 
		$editedtemplateid = $this->uri->segment(4); 
		$Action = $this->uri->segment(5);
    $emailusertype = $this->uri->segment(6); 
    if($editedtemplateid == ''){
      $editedtemplateid =  '1' ;
    }
		
		?>

<form action="<?php echo base_url();?>enrolmenttemplate/updatetemplate/<?php echo $last_page;?>/<?php echo $editedtemplateid;?>/<?php echo $emailusertype;?>" name="reporttemplate" id="reporttemplate" method="post">
<input type="hidden" name="templateid" value = "<?php echo $info->id ; ?>">
<input type="hidden" name="email_user" value = "<?php echo $info->email_user_type ; ?>">



<div class="col-sm-12 col-xs-12">
<div class="col-sm-12 col-xs-12 applycodediv reportdiv nopadding">

    <div class="form-group fullwidthinput">
      <div class="col-sm-6 nopadding">
        <div class="col-sm-12 nopadding">
        <label class=" control-label nopadding filterlabel">User Type:</label>
        </div>
  
      <div class="col-sm-12 inputbox termselect usersel">
       <select name="user_type" id="user_type" disabled>
        <option value="">Select user type </option>  
        <option <?php if ($info->email_user_type == 'student_enrolment') { echo 'selected';}?> value ='student_enrolment'>Student Enrolment</option>
        <option <?php if ($info->email_user_type == 'staff_enrolment') { echo 'selected';}?> value ='staff_enrolment'>Staff Enrolment</option>
        </select>
        </div>
        </div>

      <div class="col-sm-6 nopadding">
        <div class="col-sm-12 nopadding">
        <label class=" control-label nopadding filterlabel">Subject : </label>
        </div>
  
        <div class="col-sm-12 inputbox termselect">
          <?php if($info->email_user_type == 'student_enrolment'){ ?>

        <input type="text" name="msgtitle" id="msgtitle" value="<?php echo $info->student_email_subject;?>"  placeholder="Enter subject" class="form-control" autocomplete="off" >
        
        <?php }else{ ?> 
        
        <input type="text" name="msgtitle" id="msgtitle" value="<?php echo $info->staff_email_subject;?>"  placeholder="Enter subject" class="form-control" autocomplete="off" >

        <?php } ?>

      </div>

        </div>
      </div>
    </div>


<div class="col-sm-9 col-xs-9 applycodediv reportdiv nopadding">

    <div class="form-group fullwidthinput">
    
    	<div class="col-sm-12 nopadding">
        <div class="col-sm-12 nopadding">
        <label class=" control-label nopadding filterlabel">Description: </label>
        </div>
  
        <div class="col-sm-12 inputbox termselect">
    <?php if($info->email_user_type == 'student_enrolment'){ ?>

        <textarea  class="form-control" name="msgdesc" id="msgdesc" autocomplete="off" ><?php echo $info->student_email_message;?> </textarea>

      <?php }else{ ?> 

        <textarea  class="form-control" name="msgdesc" id="msgdesc" autocomplete="off" ><?php echo $info->staff_email_message;?> </textarea>

       <?php } ?>

           <script>
       CKEDITOR.replace( 'msgdesc' );
     </script>

        </div>
        </div>

         
	</div>
    
</div>

   <div class="col-sm-3 col-xs-3 nopadding">
        <div class="col-sm-12 nopadding">
        <label class=" control-label nopadding filterlabel">Variables:</label>
    </div>
    <div class="col-sm-12 col-xs-12 variablediv nopadding">
    <h4>Common Variables </h4>

    <p>(username) , (password) , (school name) , (name of branch) , (address of branch) , (branch contact number) , (gender), (email id) , (date of birth) , (his-her) , (him-her) </p><hr>

    <h4>Staff Variables </h4> 

    <p>(name of staff) , (qualification) , (salary) , (teaching days)</p><hr>

    <h4>Student Variables </h4>

    <p> (name of Student) , (father name) , (mother name) , (father mobile) , (mother mobile) , (father email) , (mother email) , (father address) , (mother address) , (child class) , (date of enrolment) </p>

</div>
</div>
<div class="col-sm-12 col-xs-12 applycodediv reportdiv nopadding">

    <div class="form-group fullwidthinput">
    <div class="col-sm-12 nopadding">
          <div class="col-sm-7 nopadding"></div>
        <div class="col-sm-5 nopadding">
        <div class="col-sm-4 nopadding"></div>
        <div class="col-sm-8 confirmlink nopadding">
          <input type="submit" class="open1 addtmplt" value="Update My Template">
        </div>
        </div>
       </div> 
        
	</div>
    
</div>
</div>
   

    </form>      
    
        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 headingdiv nopadding">

  <div class="col-sm-4">   <h1>Enrolment Templates<span><?php echo $Totalrec = $total_rows;?></span></h1></div>
  <div class="col-sm-3 filterdiv"> 
      <form action="<?php echo base_url();?>enrolmenttemplate/index" method="POST">
       <select name="user_type" id="user_type" onchange="this.form.submit()">
        <option <?php if($email_user_type == 'student_enrolment'){echo 'selected';}?> value='student_enrolement'>Student Enrolment</option>
        <option <?php if($email_user_type == 'staff_enrolment'){echo 'selected';}?> value='staff_enrolment'>Staff Enrolment</option>
       </select>
     </form>

  </div>

     <div class="col-sm-5 filterdiv">

  <form action="<?php echo base_url();?>enrolmenttemplate/index" name="templatefilterform" id="templatefilterform">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding">

  

<div class="col-sm-7 col-xs-5 searching termsearch nopadding">

        <div class="form-group">

      <input type="text" class="form-control searchbox" placeholder="Search Here" name="seachword" id="seachword" value="<?php echo $word_search;?>">


     </div>

        </div>

        <div class="col-sm-2 col-xs-7 rightspace viewreport">

<div class="form-group">

    <input type="submit" class="btn btn-danger srchbtn" value="Find Template">

   </div>

</div>

        </div>

        </form>

        </div>

</div>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

            <th class="column3">S.No.</th>

            <th class="column4">Subject</th>

            <th class="column10">Description</th>
                      
            <th class="column3">Action</th>

				  </tr>

			  </thead>

				<tbody>
                 <?php 
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $templates) { 
              $sr++;
            if($templates->email_user_type == 'student_enrolment'){
              $subject=$templates->student_email_subject;
              $description =$templates->student_email_message;
            }else{
              $subject=$templates->staff_email_subject;
              $description =$templates->staff_email_message;
            }
						
            
					?>

					<tr class="familydata students">
						<td class="column3"><?php echo $sr; ?></td>
                 <td class="column4"><?php echo $subject; ?></td>
                      <td class="column10"><?php 
                        $string = $description ;
                $string1 = (strlen($string) > 5) ? substr($string,0,100).'...' : $string;  
                echo $string1;

                      
                      ?></td>
                   
                      <td class="column3">
											<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
 	      <li><a href="<?php echo base_url();?>enrolmenttemplate/index/<?php echo $last_page;?>/<?php echo $templates->id;?>/edit/<?php echo $templates->email_user_type ;?><?php echo $post_url;?>" target="_blank"> Edit </a></li>
          
           
			<!--<li class="divider"></li>
				 <li><a onclick="delConfirm('<?php //echo $templates->id;?>')" style="cursor: pointer;"> Delete </a> </li> -->
									</ul>
								</div>
											
							</td>
                        </tr>
			         <?php	}		
					} else { ?>               
                                  
                    <tr><th colspan="7" style="text-align: center; width:1215px; height: 100px;font-size:25px ; background:#FFF;  color: #6a7a91;">No record to show.</th></tr>	
				   <?php } ?>

				</tbody>

		  </table>

     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv">

    <div class="col-sm-8 col-xs-6 paginationblk">

			<p class="showp">Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?>  entries</p>
			
	<ul class="pagination">

		<?php echo $pagination;?>

	</ul>

    </div>

    <div class="col-sm-4 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total:&nbsp;<?php echo $Totalrec ;?></h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>enrolmenttemplate/index" method="get">
       
          <input type="hidden" name="status" id="status" value="<?php echo $status_search; ?>"  />
       
          <input type="hidden" name="seachword" id="seachword" value="<?php echo $word_search; ?>"  />  

          <select class="form-control" name="perpage" id="perpage">
        
          <option value="">Select</option>
        
          <option   <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>
        
          <option   <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>
        
          <option   <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>
        
          <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>
        
          <option <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>
        
         </select>

</form>


        </div>

    </div>

	</div>

    </div>     

</div>


	</div>

    

	</div>

    </div>

   
  <script>
  jQuery(document).ready(function(){
	  
	  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
					
	 jQuery.validator.addMethod("lettersonly", function(value, element) {
		return this.optional(element) || /^[a-z\s]+$/i.test(value);
		}, "Only alphabetical characters");

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });
	    jQuery("#reporttemplate").validate({
        rules: {
		      subject_name:{
            required:true
          },
          subcat_name:{
            required:true
          },
          cat_name:{
            required:true
          },
         reporttitle: {
                   required: true
                   //maxlength: 61
          },

         reportdesc: {
                  required: true
          }
			  
        },
        messages: {
        subject_name: {
                  required: "Select Subject Name."
          },
           cat_name: {
                  required: "Select Your Category."
          },
           subcat_name: {
                  required: "Select Your SubCategory."
          },
        reporttitle: {
                  required: "Please Enter Subject."
                 // maxlength: "Maximum 60 characters allowed."
          },
        reportdesc: {
                  required: "Please Enter Description"
          }

			},
        submitHandler: function(form) {
            form.submit();
          }
        });	
					
 });
</script>					   
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>enrolmenttemplate/deletetemplate/"+id;
		}else{
			return false;
		}
	}
</script>	
	
<style>
.variablediv {
    border: 1px solid #ccc;
    height: 325px;
    margin-top: 7px;
    border-radius: 4px;
    box-shadow: 0 0 5px #ccc;

}
.variablediv > p, h4 {
   padding: 0 0 0 10px;
   margin :5px 0px;
 
}

.variablediv > hr{
margin: 10px;

}

.open1.addtmplt {
  font-size: 18px !important;
	margin:0px !important
}
.tmplt_btndiv{
	margin:0px;
}
#msgdesc {
    min-height: 200px;
}
.reportdiv {
    margin-bottom: 20px;
}
.btn-info { 
	background-color: #37B148;
  border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.fullwidthinput select {
    background-position: 95% center;
	padding:0;
}
.Inactive {
    background: #ff0000 none repeat scroll 0 0;
}
<!--changes-->
.showp {
    padding-top: 20px;
}
.pagination{
	margin:0px;
	padding-left:180px;}
	
.paginationblk {
    padding: 25px;
}
.srchbtn{
   padding: 6px 30px;
}
.open1{
	margin:0px !important;
	margin-bottom:20px !important;
}
.variablediv > p {
    font-size: 13px;
    line-height: 18px;
}
.headingdiv {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #dfe3e9;
    border-radius: 5px;
    margin: 0;
    padding: 0px 0px;
}
.tablediv .headingdiv h1 {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: medium none;
    border-radius: 0;
    color: #354052;
    font-size: 24px;
    margin: 0;
    padding: 20px 0;
}
.filterdiv {
    margin: 15px 0;
}
.confirmlink{
  margin-top: 10px;
}
.termselect #cke_1_contents{

  height:220px !important;  
}
.usersel select {

    background-image: none;
    padding: 0 0 0 5px;
    
}
.filterdiv select {
  
    padding: 0 0 0 5px;
    
}

</style>	
