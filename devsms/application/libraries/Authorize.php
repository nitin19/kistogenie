<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
class Authorize {
	function Authorize(){
		$this->CI =& get_instance();
		$this->CI->load->model('authorization_model');	
	}
	function validate_user_credentials($user, $pass)	{

		if($this->CI->authorization_model->validate_user_credentials($user, $pass) == true)	{

			return true;

		}	else	{

			return false;

		}

	}
	function do_user_login($info, $user_access, $staff_details)	{	
		
		$this->CI->session->set_userdata('user_id', $info->id);
		
		$this->CI->session->set_userdata('user_school_id', $info->school_id);
		
		$this->CI->session->set_userdata('user_name', $info->username);

		$this->CI->session->set_userdata('user_email', $info->email);
		
		$this->CI->session->set_userdata('user_image', $info->profile_image);
		
		$this->CI->session->set_userdata('user_type', $info->user_type);
		
		$this->CI->session->set_userdata('last_login_time', $info->last_login);

		$this->CI->session->set_userdata('user_loggedin', true);
		
		//--------------------------------------------------------
		if($user_access!=''){
			
        $this->CI->session->set_userdata('access_id', $user_access->id);
		
		$this->CI->session->set_userdata('access_school_id', $user_access->school_id);
		
		//$this->CI->session->set_userdata('access_user_id', $user_access->user_id);

		$this->CI->session->set_userdata('access_students', $user_access->students);
		
		$this->CI->session->set_userdata('access_student_attendance', $user_access->student_attendance);
		
		$this->CI->session->set_userdata('access_documents', $user_access->documents);
		
		$this->CI->session->set_userdata('access_progress_reports', $user_access->progress_reports);
		
		$this->CI->session->set_userdata('access_configurations', $user_access->configurations);
		
		$this->CI->session->set_userdata('access_student_enrolement', $user_access->student_enrolement);
		
		$this->CI->session->set_userdata('access_staff', $user_access->staff);
		
		$this->CI->session->set_userdata('access_staff_enrolement', $user_access->staff_enrolement);
		
		$this->CI->session->set_userdata('access_staff_attendence', $user_access->staff_attendence);
		
		$this->CI->session->set_userdata('access_staff_salary', $user_access->staff_salary);
		
		$this->CI->session->set_userdata('access_teaching_learing', $user_access->teaching_learing);
		
		$this->CI->session->set_userdata('access_basic_masters', $user_access->basic_masters);
		}
		//----------------------------------------------------------
		if($staff_details!=''){

			$this->CI->session->set_userdata('staff_branch_id', $staff_details->branch_id);

			$this->CI->session->set_userdata('staff_class_id', $staff_details->class_name);
			
			$this->CI->session->set_userdata('staff_subject_id', $staff_details->subject_name);
		}
		
		
		//---------------------------------------------------------
		

		if($info->user_type=="superadmin"){

			$this->CI->session->set_userdata('log_mode', "superadmin");

		} elseif($info->user_type=="admin") {
			
			$this->CI->session->set_userdata('log_mode', "admin");
			
		  } elseif($info->user_type=="teacher") {
			  
			$this->CI->session->set_userdata('log_mode', "teacher");
			
		  } elseif($info->user_type=="student") {
			  
			$this->CI->session->set_userdata('log_mode', "student");
			
		  } elseif($info->user_type=="enrolementstudent") {
			  
			$this->CI->session->set_userdata('log_mode', "enrolementstudent");
			
		  }

		return true;

	}
	
	/*function do_user_login_access($user_access)	{	
		
		$this->CI->session->set_userdata('access_id', $user_access->id);
		
		$this->CI->session->set_userdata('access_school_id', $user_access->school_id);
		
		$this->CI->session->set_userdata('access_user_id', $user_access->user_id);

		$this->CI->session->set_userdata('access_students', $user_access->students);
		
		$this->CI->session->set_userdata('access_student_attendance', $user_access->student_attendance);
		
		$this->CI->session->set_userdata('access_documents', $user_access->documents);
		
		$this->CI->session->set_userdata('access_progress_reports', $user_access->progress_reports);
		
		$this->CI->session->set_userdata('access_configurations', $user_access->configurations);
		
		$this->CI->session->set_userdata('access_student_enrolement', $user_access->student_enrolement);
		
		$this->CI->session->set_userdata('access_staff_enrolement', $user_access->staff_enrolement);
		
		$this->CI->session->set_userdata('access_staff_attendence', $user_access->staff_attendence);
		
		$this->CI->session->set_userdata('access_staff_salary', $user_access->staff_salary);
		
		$this->CI->session->set_userdata('access_teaching_learing', $user_access->teaching_learing);
		
		$this->CI->session->set_userdata('access_basic_masters', $user_access->basic_masters);

		return true;

	}*/

	

	function is_user_logged_in(){

		$user_name 					= $this->CI->session->userdata('user_name');

		$bool_user_loggedin 		= $this->CI->session->userdata('user_loggedin');

		if( (isset($user_name) && $user_name != '') && (isset($bool_user_loggedin) && $bool_user_loggedin == true) )

			return true;

		else

			return false;

		

	}


function do_user_logout()	{
	    $this->CI->session->unset_userdata('user_id');	
		$this->CI->session->unset_userdata('user_school_id');
		$this->CI->session->unset_userdata('user_name');
		$this->CI->session->unset_userdata('user_email');
		$this->CI->session->unset_userdata('user_image');
		$this->CI->session->unset_userdata('user_type');
		$this->CI->session->unset_userdata('last_login_time');
		$this->CI->session->unset_userdata('user_loggedin');
		$this->CI->session->unset_userdata('log_mode');
		
		//----------------------------------------------------------
		$this->CI->session->unset_userdata('access_id');
		$this->CI->session->unset_userdata('access_school_id');
		$this->CI->session->unset_userdata('access_students');
		$this->CI->session->unset_userdata('access_student_attendance');
		$this->CI->session->unset_userdata('access_documents');
		$this->CI->session->unset_userdata('access_progress_reports');
		$this->CI->session->unset_userdata('access_configurations');
		$this->CI->session->unset_userdata('access_student_enrolement');
		$this->CI->session->unset_userdata('access_staff_enrolement');
		$this->CI->session->unset_userdata('access_staff_attendence');
		$this->CI->session->unset_userdata('access_staff_salary');
		$this->CI->session->unset_userdata('access_teaching_learing');
		$this->CI->session->unset_userdata('access_basic_masters');
		//$this->CI->session->unset_userdata('log_mode');
		
		//----------------------------------------------------------
		$this->CI->session->unset_userdata('staff_branch_id');
		$this->CI->session->unset_userdata('staff_class_id');
		$this->CI->session->unset_userdata('staff_subject_id');
		
		//----------------------------------------------------------
		
		setcookie("username", "", time()-180,"/");
		setcookie("userpass", "", time()-180,"/");
		return true;
	}
	

function send_email($to,$message,$subject){

		
			$Olivetree = 'Olivetree';
			$from		 = "binarydata2015@gmail.com";

			$headers 	 = 'MIME-Version: 1.0' . "\r\n";

			$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			$headers 	.= 'To: <'.$to.'>' . "\r\n";

			$headers 	.= 'From: '.$Olivetree.'<'.$from.'>' . "\r\n";

			if($_SERVER['HTTP_HOST']!="localhost")

				mail($to, $subject, $message, $headers);

			return true;

	}



	

	function validate_user_email($email)	{

		

		if($this->CI->authorization_model->validate_user_email($email) == true)	{

			return true;

		}	else	{

			return false;

		}

	}


	function generate_password(){

		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';

		$string = '';

		for ($i = 0; $i < 8; $i++) {

			$string .= $characters[rand(0, strlen($characters) - 1)];

		}

		return $string;

	}
}
?>