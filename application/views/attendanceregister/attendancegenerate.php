<?php

$user_type		= $this->session->userdata('user_type');

$lastmonth =  date('Y-m-d', strtotime('last month'));
						$lastdate = explode('-',$lastmonth);
						$yy = $lastdate[0];
						
$tot_month_days = cal_days_in_month(CAL_GREGORIAN,date('m'),date("Y"));

function days_in_month($year,$month,$calday="5"){ 
//0 Sunday, 1 Monday, 2 Tue , 3 Wed , 4. Thu , 5. Fri , 6. Sat
    // calculate total number of occurance in this month
    $num = cal_days_in_month(CAL_GREGORIAN, $month, $year); // days in month
    $dayofweek = date( "w", mktime(0, 0, 0, $month, 1, $year));    
    $adddays=0;
    if($calday > $dayofweek )
            $adddays=1 + $calday - $dayofweek;
    else if($calday < $dayofweek )
            $adddays=1 + 7 + ($calday - $dayofweek );
               
    $remainingdays=$num-$adddays;
    $leavesnum=1+intval($remainingdays / 7);
    return     $leavesnum;
}

	

function hour_min($minutes){// Total
   if($minutes <= 0) return '00:00';
else    
   return sprintf("%02d",floor($minutes / 60)).':'.sprintf("%02d",str_pad(($minutes % 60), 2, "0", STR_PAD_LEFT));
}

/*$year=date("Y");
$month=date('m');
$day=date('w');

echo days_in_month($year,$month,$day);*/

//echo $sdate;


/*echo '<br>';
$attmonth =  date('Y-m-d', strtotime($sdate));
$attcalender = explode('-', $attmonth);
$calenderyear = $attcalender[0];
$calendermonth =  $attcalender[1];*/
?>

<div class="editprofile-content">
    	<div class="profilemenus">
        <ul>
		          <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

           <?php  } elseif($user_type == 'student'){?>

		          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>
    <?php }else {?>
          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
    <?php } ?>
        <li><a href="<?php echo base_url();?>generateattendance">Generate Salary</a>
        <li class="edit"> Generate Attendance</li>        
        </ul>
        </div>
        
         <!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>

        <div class="col-sm-12 profile-bg generatesalary-filter">
        <form name="attfilter" id="attfilter" method="get" action="<?php echo base_url(); ?>generateattendance/index/0/">
        <div class="col-sm-12 tablenavdiv">
    <div class="col-sm-5 tablenevright">
        <div class="col-sm-12 col-xs-12 applycodediv nopadding datepickerspacing">
        <div class="col-sm-2 branchlabel">
                <label>Branch:</label>
        </div>
        <div class="col-sm-6 col-xs-5 nopadding dateblk">
        <div class="form-group">
           <div class="inputbox datepickerdiv nopadding">
          <select class="form-control" name="branch" id="branch">
           <option value="">Select Branch</option>  
            <?php
			$staff_branch_id = $this->session->userdata('staff_branch_id');
  if($user_type=='teacher' && $staff_branch_id!=''){
 $staff_branch = explode(',',$staff_branch_id); 
	  foreach($staff_branch as $staff_branch_ids){
	  $this->load->model(array('generate_attendance_model'));
	 $branchdta = $this->generate_attendance_model->get_staff_branches($staff_branch_ids);

    ?>
     <option <?php if($branch_search == $branchdta->branch_id){ echo 'selected'; } ?> value="<?php echo $branchdta->branch_id; ?>"><?php echo $branchdta->branch_name;?></option>
<?php } } else if(count($branches) > 0 && $user_type=='admin'){
	   foreach($branches as $branch) { 
	 ?>
	  <option  <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
		<?php  
	   }
   }
	 ?>
     </select>
            </div>
        </div> 
  	    </div>
        </div>
        </div>
    <div class="col-sm-5 tablenevright">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding datepickerspacing">
                <div class="col-sm-2 datelabel">
                <label>Date:</label>
                </div>
                <div class="col-sm-6 col-xs-5 nopadding dateblk">
        		<div class="form-group">
    <div class="inputbox datepickerdiv nopadding">
    <?php $defaultdate = date("F Y", strtotime("-1 months")); ?>
      <input type="text" name="sdate" class="date-picker23" id="sdate" value="<?php if($sdate!='') { echo $sdate; } else { echo  $defaultdate; }   ?>">
     
    </div>
  </div> 
  	</div>
</div>

        </div>
   <div class="col-sm-2 tablenevright">
   <div class="confirmlink">
       <input type="submit" class="open1" value="Generate Salary">
   </div>
   </div>

    </div>
    </form>
    </div>
        <div class="attendancesec">	
    <div class="col-sm-12 tablediv nopadding">
    <div class="col-sm-12 nopadding">
    <div class="tabletitlediv generatetitlediv">
    <h1>Generate Salary<span></span></h1>
    <?php  if(count($data_rows) > 0) { ?>
  <button id="save_salary" onclick="document.getElementById('staff_salary').submit();">Save Salary</button>
	<?php } ?>
    </div>
    </div>
   
    <div class="tablewrapper">
<table class="table-bordered table-striped">
			  <thead>
				  <tr class="headings enrolment-headings">
					  <th class="column1">No.</th>
					  <th class="column3">Name</th>
					 <!-- <th class="column1">Total Working (Hrs) in Month</th>-->
                      <th class="column2 salcls">Total Days</th>
                      <!--<th class="column1">Working Hrs In Days</th>-->
                      <th class="column2 salcls">Present</th>
                      <th class="column2 salcls">Absent</th>
                       <th class="column2 salcls">Late</th>
                     <!-- <th class="column1">Present Working Hours In Month</th>-->
                     <!-- <th class="column2">Absent Working Hours In Month</th>-->
                      <th class="column2 salcls">Actual Salary(Monthly)</th>
                      <th class="column2 salcls">Calculated</th>
                      <th class="column2 salcls">Adjustment</th>
                      <th class="column2 salcls">Grand</th>
                      
				  </tr>
			  </thead>
               <form action="<?php echo base_url(); ?>generateattendance/generate_salary/"  method="post" id="staff_salary" name="staff_salary">
				<tbody>
                <?php 
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $staff) { 
					     $sr++;
						  $staff_school_branch = $staff->existing_branch;
						  $this->load->model(array('generate_attendance_model'));
						  $branchName = $this->generate_attendance_model->getbranchName($staff_school_branch);
					?>
					<tr class="familydata generate-salary enrolment-table">
						<td class="column1"><?php echo $sr; ?></td>
						<td class="column3"><?php echo $staff->staff_fname.' '. $staff->staff_lname,'('.$staff->user_id.')';?></td>
                        
                       <!-- <td class="column1"><?php
						/*-----------------------------------*/
						$lastmonth =  date('Y-m-d', strtotime($sdate));
						$lastdate = explode('-',$lastmonth);
						 
						$totalmonthday = '0';
						$day = '';
						$year=$lastdate[0];
						$month=$lastdate[1];
						if($staff->teaching_days!=''){
						$workdays = explode(',',$staff->teaching_days);
						foreach($workdays as $dayworks){
							if($dayworks=='Sun'){ $day='0'; }
							if($dayworks=='Mon'){ $day='1'; }
							if($dayworks=='Tue'){ $day='2'; }
							if($dayworks=='Wed'){ $day='3'; }
							if($dayworks=='Thu'){ $day='4'; }
							if($dayworks=='Fri'){ $day='5'; }
							if($dayworks=='Sat'){ $day='6'; }
							if($day!=''){
							$totalmonthday = $totalmonthday + days_in_month($year,$month,$day);
						}
						}
						}
						/*-----------------------------------*/
						if($branchName->branch_timing!='') {
						$timing = explode('-',$branchName->branch_timing);
						$StartTime= $timing[0];
						$EndTime = $timing[1];
						$datetime1 = new DateTime($StartTime);
						$datetime2 = new DateTime($EndTime);
						$interval = $datetime1->diff($datetime2);
						$br_tot_hrs = $interval->format('%h:%i');
						$hrsmin = explode(':',$br_tot_hrs);
						$staffhrs = $hrsmin[0] * $totalmonthday;
						$staffmin = ($hrsmin[1] * $totalmonthday) / 60;
						
						 $total_working_hours = $staffhrs + $staffmin;
						echo $total_working_hours.' Hrs';
					//echo "<br>";
						
					  $mth_wk_in_min = $total_working_hours * 60;
						}
						else { $total_working_hours = '0'; 
						$mth_wk_in_min = '0';
						} 
						/*---------------------------------------*/
						?></td>-->
                        <input type="hidden" name="total_working_hours[<?php echo $staff->user_id; ?>]" value="<?php echo $total_working_hours; ?>"/>
                        <?php $my_working_hours=$total_working_hours; ?>
                        <td class="column2 salcls"><?php
						 $lastmonth =  date('Y-m-d', strtotime($sdate));
						$lastdate = explode('-', $lastmonth);
						$workdays = explode(',', $staff->teaching_days); 
						$count_workdays = count($workdays);
						$perday_sal =$staff->salary_amount/$count_workdays;


						$totalmonthday = '0';
						$day = '';
						$year=$lastdate[0];
						$month=$lastdate[1];
						foreach($workdays as $dayworks){
							if($dayworks=='Sun'){ $day='0'; }
							if($dayworks=='Mon'){ $day='1'; }
							if($dayworks=='Tue'){ $day='2'; }
							if($dayworks=='Wed'){ $day='3'; }
							if($dayworks=='Thu'){ $day='4'; }
							if($dayworks=='Fri'){ $day='5'; }
							if($dayworks=='Sat'){ $day='6'; }
							if($day!=''){
							$totalmonthday = $totalmonthday + days_in_month($year,$month,$day);
						}
						}
						echo $totalmonthday.' Days';
						echo '<br>';
						
						$staff->salary_amount;

						if($staff->salary_mode == 'Hourly'){

							$my_actual_salary = $staff->salary_amount * $my_working_hours ;

						}elseif($staff->salary_mode == 'Weekly'){

						$my_actual_salary =	$perday_sal * $totalmonthday;

						}else{

							$my_actual_salary = $staff->salary_amount ;
						}


						if($staff->salary_amount!='0' && $mth_wk_in_min!='0'){
						//$permin_salary =  ($staff->salary_amount) / $mth_wk_in_min;
							$permin_salary =  $my_actual_salary / $mth_wk_in_min;
						if($permin_salary!=''){
							 round($permin_salary, 2);
						}
						} else { $permin_salary = '0';}
						//echo round($permin_salary, 2)
						
						 ?></td>
                         <input type="hidden" name="total_working_days[<?php echo $staff->user_id; ?>]" value="<?php echo $totalmonthday; ?>"  />
                         
                       <!-- <td class="column1"><?php 
						if($branchName->branch_timing!='') {
						$timing = explode('-',$branchName->branch_timing);
						$StartTime= $timing[0];
						$EndTime = $timing[1];
						$datetime1 = new DateTime($StartTime);
						$datetime2 = new DateTime($EndTime);
						$interval = $datetime1->diff($datetime2);
						$dailyworking_hours = $interval->format('%h:%i');
						echo $dailyworking_hours.' Hrs';
						echo '<br>';
						($interval->format('%h') * 60) + $interval->format('%i');
						} else {
							$dailyworking_hours = '0';
						}
						?></td>-->
                        <input type="hidden" name="working_hrs_in_days[<?php echo $staff->user_id; ?>]" value="<?php echo $dailyworking_hours; ?>"  />
                        
                        <td class="column2 salcls"><?php 
						$staffattendance = $this->generate_attendance_model->staff_attendance($staff->user_id, $sdate);
						echo count($staffattendance).' Days';
						?></td>
                        <input type="hidden" name="present_working_days[<?php echo $staff->user_id; ?>]" value="<?php echo count($staffattendance); ?>"  />
                        
                        <td class="column2 salcls"><?php echo $totalmonthday - count($staffattendance); ?></td>
                        <input type="hidden" name="absent_working_days[<?php echo $staff->user_id; ?>]" value="<?php echo $totalmonthday - count($staffattendance); ?>"  />
                        
                        <!--<td class="column1">--><?php
						if(count($staffattendance) > 0){
							$staffhrs = '0';
							$staffmins = '0';
							foreach($staffattendance as $attendstaff){

								$presentdate = $attendstaff->Date_Time;
							
								$staffdayhrs = $this->generate_attendance_model->staff_day_hrs($presentdate,$staff->user_id);
								$todaypunches = count($staffdayhrs);
								
								if($todaypunches > 1){
								$login    = new DateTime($staffdayhrs[0]->Date_Time);
								$logout   = new DateTime($staffdayhrs[1]->Date_Time);
								$interval1 = $logout->diff($login);
								/* $totaldayss_time = $interval1->format('%h:%i');
								 echo $totaldayss_time;
								 echo "<br>";*/
								 $totalday_time = explode(':', $interval1->format('%h:%i'));
								 $staffhrs = $staffhrs + $totalday_time[0];
								 $staffmins = $staffmins + $totalday_time[1];
								}
								 if($todaypunches > 3){
								 $login2    = new DateTime($staffdayhrs[2]->Date_Time);
								 $logout2   = new DateTime($staffdayhrs[3]->Date_Time);
								 $interval2 = $logout2->diff($login2);
								 $totalday_time2 = explode(':', $interval2->format('%h:%i'));
								  $staffhrs = $staffhrs + $totalday_time2[0];
								 $staffmins = $staffmins + $totalday_time2[1];
								 } 
								 
								 if($todaypunches > 5){
								 $login3    = new DateTime($staffdayhrs[4]->Date_Time);
								 $logout3   = new DateTime($staffdayhrs[5]->Date_Time);
								 $interval3 = $logout3->diff($login3);
								 $totalday_time3 = explode(':', $interval3->format('%h:%i'));
								 $staffhrs = $staffhrs + $totalday_time3[0];
								 $staffmins = $staffmins + $totalday_time3[1];
								 }
								 
							}

							$min_cnv_hrs = explode(':', hour_min($staffmins));
							
							$tottalstaffhrs = $staffhrs + $min_cnv_hrs[0];
							$tottalstaffmin = $min_cnv_hrs[1];
							echo $total_working_hours =  $tottalstaffhrs.':'.$tottalstaffmin;
							//echo $total_working_hours.' Hrs';
							//echo "<br>";
							  $staff_work_hrs = ($staffhrs * 60) + $staffmins;
							
						} else { $staff_work_hrs = '0';
						$total_working_hours = '0';
						}
						?><!--</td>-->
                         <input type="hidden" name="present_working_hours[<?php echo $staff->user_id; ?>]" value="<?php echo $total_working_hours; ?>"  />
                        
                        <!--<td class="column2"><?php 
						$absent_working_minutes = $mth_wk_in_min - $staff_work_hrs;
						$absent_working_hours = hour_min($absent_working_minutes);
						echo $absent_working_hours.' Hrs';
						//echo $absent_working_minutes;
						?></td>-->
                        <input type="hidden" name="absent_working_hours[<?php echo $staff->user_id; ?>]" value="<?php echo $absent_working_hours; ?>"  />
                        
                         <td class="column2 salcls"><?php
						 $present_daysInmonth = count($staffattendance);
						 $dailywork = explode(':', $dailyworking_hours);
						 $dailymin = ($dailywork[0] * 60) + $dailywork[1];
						  $actualpresentminutes = $dailymin * $present_daysInmonth;
						 //echo '<br>';
						 $total_working_hours;
						 $presenttime = explode(':', $total_working_hours);
						//print_r($presenttime);
						if($total_working_hours!='' && $total_working_hours!='0'){
							 $presentmin = ($presenttime[0] * 60 ) + $presenttime[1];
						//echo $presentmin;
						} else {$presentmin = '0'; }
						
						//echo '<br>';
						if($actualpresentminutes > $presentmin){
							$latemin = ($actualpresentminutes) - ($presentmin);
							$late_time = hour_min($latemin);
						} else { $late_time = '00:00'; }
						
						echo $late_time;
						
						  ?></td>
                        <input type="hidden" name="late_working_hours[<?php echo $staff->user_id; ?>]" value="<?php echo $late_time; ?>"  />
                        
                        <td class="column2 salcls"><?php
                        /**************** actual salary ***************************/
						if($staff->salary_mode == 'Hourly'){

							$my_actual_salary = $staff->salary_amount * $my_working_hours ;

						}elseif($staff->salary_mode == 'Weekly'){

						$my_actual_salary =	$perday_sal * $totalmonthday;

						}else{

							$my_actual_salary = $staff->salary_amount ;
						}

						echo $staff->salary_currency.$my_actual_salary ;
						///echo '<br>';
						//echo round($staff_work_hrs * $permin_salary , 2);

                        ?></td>
                         <input type="hidden" name="actual_salary[<?php echo $staff->user_id; ?>]" value="<?php echo $staff->salary_currency.$my_actual_salary; ?>"  />
                        <input type="hidden" value="<?php echo $permin_salary;?> " name="perminutesalary">
                        <td class="column2 salcls">

                        	<?php 
						/*if($staff->salary_mode == 'Weekly' && $staff->teaching_days!=''){
							$weekteach_days = explode(',', $staff->teaching_days);
							count($weekteach_days);
							
							if($branchName->branch_timing!='') {
						$timing = explode('-',$branchName->branch_timing);
						$StartTime= $timing[0];
						$EndTime = $timing[1];
						$datetime1 = new DateTime($StartTime);
						$datetime2 = new DateTime($EndTime);
						$interval = $datetime1->diff($datetime2);
						$branch_timing =  $interval->format('%h:%i');
						}
								$interval->format('%h');
								$interval->format('%i');
							$totlahrs_inweek = $interval->format('%h') * count($weekteach_days);
							$totlalmin_inweek = $interval->format('%i') * count($weekteach_days);
							$working_time_min = ($totlahrs_inweek * 60) + $totlalmin_inweek;
							echo $working_time_min;
						}*/

						$perminutesalary = $staff_work_hrs * $permin_salary;
						if($perminutesalary > 0){
						echo $staff->salary_currency.round($perminutesalary, 2);
						$workingsalary = round($perminutesalary, 2);
					} else {
						echo $staff->salary_currency.$perminutesalary;
						$workingsalary = $perminutesalary;
					}
					


						
						?></td>
                        <input type="hidden" name="received_salary[<?php echo $staff->user_id; ?>]" value="<?php echo round($perminutesalary, 2); ?>"  />
                        
                        <td class="column2 salcls">
                        <input class="form-control" name="actual[<?php echo $staff->user_id; ?>]" id="actual<?php echo $staff->user_id; ?>" type="hidden" value="<?php echo $workingsalary; ?>">
                        <input class="form-control" name="adjusted[<?php echo $staff->user_id; ?>]" id="adjusted<?php echo $staff->user_id; ?>" type="text" onkeyup="sum<?php echo $staff->user_id; ?>();" value="">
                      <input class="form-control" name="grand[<?php echo $staff->user_id;?>]" id="grand<?php echo $staff->user_id;?>" type="hidden" value=""></td>  
                       
                        
                        <td class="column2 salcls">
                        <input class="form-control" name="granddemo[<?php echo $staff->user_id;?>]" id="granddemo<?php echo $staff->user_id;?>" type="text" disabled value=""></td>
                        </tr>
                        
                        
                <input type="hidden" name="user_id_arr[]" value="<?php echo $staff->user_id;?>" />
                <input type="hidden" name="staff_salary[<?php echo $staff->user_id;?>]" value="<?php echo $staff->salary_currency.$staff->salary_amount;?>" />
                <input type="hidden" name="staff_present_day[<?php echo $staff->user_id;?>]" value="<?php echo count($staffattendance); ?>" />
                <input type="hidden" name="user_tot_wrk[<?php echo $staff->user_id;?>]" value="<?php echo $staff_work_hrs ;?>" />
                 <input type="hidden" name="branch_id[<?php echo $staff->user_id;?>]" value="<?php echo $staff->existing_branch;?>" />
                <input type="hidden" name="month_year" value="<?php echo $sdate;?>" />
                <input type="hidden" name="sal_branch" value="<?php echo $branch_search;?>" />
                  <?php	}		
					} else { ?>
                         <tr><th colspan="10" style="text-align: center; width:1215px;height:100px;font-size:25px; background:#FFF;  color: #6a7a91;">No record to show.</th></tr>
                        <?php }?>                   
				</tbody>
                </form>
		  </table>
     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-8 col-xs-6 paginationblk">
         <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries.
			<?php  } ?>
	<ul class="pagination">
		
		<?php echo $pagination;?>
        
	</ul>

    </div>

    <div class="col-sm-4 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total Staffs: <?php echo count($data_rows) ;?></h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>generateattendance" method="post">

  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>

  <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>

    </div>

	</div>

    </div>     
          
</div>



	</div>
    
	</div>
    </div>
    </div>
    </div>
    </div>
<script type="text/javascript"> 
      $(document).ready( function() {
        $('#message').delay(5000).fadeOut();
      });
	  
	  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	});
	  </script>
  
 
  
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>students/delete/"+id;
		}else{
			return false;
		}
	}
	
	<?php  if(count($data_rows) > 0){
					     
					  foreach($data_rows as $staff) { ?>
	function sum<?php echo $staff->user_id; ?>() {
            var txtFirstNumberValue = document.getElementById('actual<?php echo $staff->user_id; ?>').value;
            var txtSecondNumberValue = document.getElementById('adjusted<?php echo $staff->user_id; ?>').value;
            var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue);
			//alert(txtFirstNumberValue);
            if (!isNaN(result)) {
                document.getElementById('grand<?php echo $staff->user_id;?>').value = result;
				document.getElementById('granddemo<?php echo $staff->user_id;?>').value = result;
			} }
				<?php 
				} }
				?>
         
</script>	 
<script>
           $(function() {
    $( ".datepicker1" ).datepicker({ dateFormat: "dd MM yy" });

});



$(function() {
    $('.date-picker23').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
        onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });
});


$(document).ready(function () {

    $('#attfilter').validate({ // initialize the plugin
        rules: {
            branch: {
                required: true,
            }
        },
		messages :{
        branch : {
            required : 'Please select one Branch Name'
        }
    }
    });

});
      </script>
<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.branchlabel > label {
    padding-top: 5px;
}
.confirmlink input {
    margin: 10px 0;
}
.confirmlink {
    margin: 0 -15px 0 0px;
}
.paginationblk {
    padding-top: 25px;
}
#perpage{
	margin-right:35px;
}
.salcls{
	text-align: center;
}
.ui-datepicker-calendar {
    display: none;
    }
.tabletitlediv > h1{
		float:left; 
		}
		
		#save_salary {
    background-color: rgb(55, 178, 72);
    border: 0 none;
    border-radius: 5px;
    color: rgb(255, 255, 255);
    float: right;
    margin: 18px;
    padding: 10px 20px;
}
</style>
  </body>
</html>