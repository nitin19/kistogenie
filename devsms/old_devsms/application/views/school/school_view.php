 <?php 
	    $schoolid = $info->school_id;
		$this->load->model(array('main_schools_model'));
		$schoolInfo = $this->main_schools_model->getSchoolinfo($schoolid);
	  ?>
<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">
   

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="#">School Details</a></li>        

        </ul>

        </div>

        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>

        </div>

    	

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-8 col-xs-7 nopadding">

        <h1>School Details</h1>

        </div>

        <div class="col-sm-4 col-xs-5 nopadding activelink">

        <h2><img src="<?php echo base_url();?>assets/images/greendot.png">Online</h2>

        </div>

        </div>

        <div class="col-sm-6 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

        <form>

        <div class="profile-bg">

        <h2>School details</h2>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Username</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $schoolInfo->username; ?></span>

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Password</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $schoolInfo->password; ?></span>

    </div>

  </div>

  		

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-3 col-xs-4 control-label nopadding">School Name</label>

    	<div class="col-sm-9 col-xs-8 inputinfo">

        	<span><?php echo $info->school_name ?></span>

    	 </div>

  		</div>

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">School Email</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

       <span><?php echo $info->school_email ?></span>

    </div>

  </div>

        <div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">School Phone</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->school_phone;?></span>

    </div>

  </div>

        <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">School Address</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $info->school_address;?></span>

    </div>

  </div>
  
  
      
        <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Contact Person</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

        <span><?php echo $info->school_contact_person;?></span>

     </div>

  </div>
  
  		

  		</div>

        

        </form>
    

        </div>

        </div>

        <div class="col-sm-6 rightspace fullwidsec">

        </div>

        </div>