<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Staffattendance_model extends CI_Model {
	
	var $tbl	= "attendance_register";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $classes = "classes";
	var $staff = "staff";
	
	
    function __construct() {
        parent::__construct();
    }
	
	function getstaff_branches($staff_branch_ids) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id"=>$staff_branch_ids,"is_active"=>'1',"is_deleted"=>'0'));
        $query = $this->db->get();
      // echo $this->db->last_query();
        return  $query->row();
    }
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }  
 
	function getRows($where=NULL , $attendancedate){
		$this->db->select('*');
		if($where != NULL){
			$this->db->where($where);
		}
		if($attendancedate !=''){
		
		$this->db->where("DATE_FORMAT(Date_Time,'%Y-%m-%d')", $attendancedate);
		
		}
		$query = $this->db->get($this->tbl);
		//echo $this->db->last_query(); exit();
		return $query->num_rows();
	}
	  
	  function getPagedData($where,$attendancedate,$start,$limit,$odr=NULL,$dirc=NULL){
	
		$this->db->select('*');
		$this->db->from($this->tbl);
		
		
		if($where != NULL){
			$this->db->where($where );		}
	
		if($attendancedate !=''){
		
		$this->db->where("DATE_FORMAT(Date_Time,'%Y-%m-%d')", $attendancedate);
		
		}
		$this->db->order_by("attendancereg_id", "asc"); 	
		$this->db->group_by($this->tbl.'.user_id'); 	
		//$this->db->limit($limit, $start);
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $query->result();
	  }
	   
	 
	function getstaffs($school_id,$schoolbranch) {
        $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->join('users', 'users.id = '.$this->staff.'.user_id','INNER'); 

		$this->db->where(array($this->staff.".school_id="=>$school_id,$this->staff.".existing_branch="=>$schoolbranch,"users.user_type="=>'teacher',"users.is_active="=>'1',"users.is_deleted="=>'0'));

		/* if($schoolbranch != NULL){
		 	$where = "FIND_IN_SET('".$schoolbranch."', $this->staff.branch_id)";
			$this->db->where($where);
		  }	*/
		$this->db->order_by("users.id", "asc");
        $query = $this->db->get();
		//echo $this->db->last_query(); 
        return  $query->result();
      }
	 
	 
	   
	  function  getstaffinfo($staffid) {
        $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where('user_id', $staffid);
        $query = $this->db->get();
        //echo $this->db->last_query(); 
        return  $query->row();
	   }
	   
	   
	 function check_attendance($school_id,$branch_id,$attendance_date) {
		$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$branch_id,"is_active="=>'1',"is_deleted="=>'0'));
        if($attendance_date !=''){
		
		$this->db->where("DATE_FORMAT(Date_Time,'%Y-%m-%d')", $attendance_date);
		
		}
        $query = $this->db->get();
       // echo $this->db->last_query();exit;
        return  $query->result();
	   }
	   
	 
	 
	function updateAttendance($input){
 		
 		$school_id 			 = $this->session->userdata('user_school_id');
		$branchid  			 = $input['branchid'];
		$attendancedate 	 = date('Y-m-d', strtotime($input['attendancedate']));

		$staff_id  		 	 =  $input['staffid'];
		$attendance_status   = 	$input['attendance_status'];
		$attendance_note     = 	$input['attendance_note'];

		$created_date 		 = date('Y-m-d H:i:s');
	
		$is_active 			 = '1';
		$is_deleted 		 = '0';

		$branchtiming 		= $this->getstaff_branches($branchid);

	$timing  			= explode('-',$branchtiming->branch_timing);
	$start_time  		= strtotime($attendancedate.' '.date('H:i:s', strtotime($timing[0])));
	$end_time    		= strtotime ($attendancedate.' '.date('H:i:s', strtotime($timing[1])));

	$difference  		= $end_time - $start_time;

	$minutes     		= floor($difference / 60);
	 
	$half_minute 		= '+'.abs($minutes/2).'minutes';

	$mid_timing 		= date('H:i:s', strtotime($half_minute,strtotime($timing[0])));
	
	$data =array();
	for ($i = 0; $i < count($staff_id); $i++) {
	$sv = $staff_id[$i];


	$check_status = $this->getstaffstatus($branchid,$sv,$attendancedate);

	if($attendance_status[$sv] == '2'){
	$intime 	= 	$attendancedate.' '.date('H:i:s', strtotime($timing[0]));
	$outtime 	=	$attendancedate.' '.date('H:i:s', strtotime($timing[1]));
	} 
	if($attendance_status[$sv] == '3'){
	$intime 	= 	$attendancedate.' '.date('H:i:s', strtotime($timing[0]));
	$outtime 	=	$attendancedate.' '.$mid_timing;
	} 
	if($attendance_status[$sv] == '4'){
	$intime 	= 	$attendancedate.' '.$mid_timing;
	$outtime 	=	$attendancedate.' '.date('H:i:s', strtotime($timing[1]));
	}
		 $data = array(
           'school_id' => $school_id, 
           'branch_id' => $branchid,
           'user_id' => $sv,
           'in_out_status'=>'In',
           'Date_Time' => $intime,
           'is_active' => $is_active,
           'is_deleted' => $is_deleted,
           'created_date' => $created_date,
           'attendance_status' => $attendance_status[$sv], 
           'attendance_notes' => $attendance_note[$sv]
          );

		 $dataout = array(
           'school_id' => $school_id, 
           'branch_id' => $branchid,
           'user_id' => $sv,
           'in_out_status'=>'Out',
           'Date_Time' => $outtime,
           'is_active' => $is_active,
           'is_deleted' => $is_deleted,
           'created_date' => $created_date,
           'attendance_status' => $attendance_status[$sv], 
           'attendance_notes' => $attendance_note[$sv]
          );

	if(count($check_status)>0){
			if($attendance_status[$sv] == '1'){	
			/**************** Delete ***************************/
			$this ->db-> where(array('user_id='=>$sv,"DATE_FORMAT(Date_Time,'%Y-%m-%d')="=> $attendancedate));
			$this->db->delete($this->tbl); 
			
			}else{

			$this ->db-> where(array('user_id='=>$sv,"DATE_FORMAT(Date_Time,'%Y-%m-%d')="=> $attendancedate,'in_out_status='=>"In"));
			
			if($this->db->update($this->tbl, $data)){
			$this ->db-> where(array('user_id='=>$sv,"DATE_FORMAT(Date_Time,'%Y-%m-%d')="=> $attendancedate ,'in_out_status='=>"Out"));
			
			$this->db->update($this->tbl, $dataout); 
		}
			
		 }
		}else{

		if($attendance_status[$sv] != '1'){	
		if($this->db->insert($this->tbl, $data)){
			$this->db->insert($this->tbl, $dataout);
			$response .= $this->db->insert_id().',';

		}	}	}	 

}	
return true;
}
	  
	function insertAttendance($input){
		
    $school_id 			 = $this->session->userdata('user_school_id');
			
	$branchid  			 = $input['branchid'];
	
	$attendancedate 	 = date('Y-m-d', strtotime($input['attendancedate']));
	
	$staff_id  		 	 =  $input['staffid'];
	$attendance_status   = 	$input['attendance_status'];
	$attendance_note     = 	$input['attendance_note'];

	$created_date 		 = date('Y-m-d H:i:s');
	
	$is_active 			 = '1';
	$is_deleted 		 = '0';


	$branchtiming 		= $this->getstaff_branches($branchid);

	$timing  			= explode('-',$branchtiming->branch_timing);
	$start_time  		= strtotime($attendancedate.' '.date('H:i:s', strtotime($timing[0])));
	$end_time    		= strtotime ($attendancedate.' '.date('H:i:s', strtotime($timing[1])));

	$difference  		= $end_time - $start_time;

	$minutes     		= floor($difference / 60);
	 
	$half_minute 		= '+'.abs($minutes/2).'minutes';

	$mid_timing 		= date('H:i:s', strtotime($half_minute,strtotime($timing[0])));
	
	$data =array();
	for ($i = 0; $i < count($staff_id); $i++) {
	$sv = $staff_id[$i];

	if($attendance_status[$sv] == '2'){
	$intime 	= 	$attendancedate.' '.date('H:i:s', strtotime($timing[0]));
	$outtime 	=	$attendancedate.' '.date('H:i:s', strtotime($timing[1]));
	} 
	if($attendance_status[$sv] == '3'){
	$intime 	= 	$attendancedate.' '.date('H:i:s', strtotime($timing[0]));
	$outtime 	=	$attendancedate.' '.$mid_timing;
	} 
	if($attendance_status[$sv] == '4'){
	$intime 	= 	$attendancedate.' '.$mid_timing;
	$outtime 	=	$attendancedate.' '.date('H:i:s', strtotime($timing[1]));
	}
		 $data = array(
           'school_id' => $school_id, 
           'branch_id' => $branchid,
           'user_id' => $sv,
           'in_out_status'=>'In',
           'Date_Time' => $intime,
           'is_active' => $is_active,
           'is_deleted' => $is_deleted,
           'created_date' => $created_date,
           'attendance_status' => $attendance_status[$sv], 
           'attendance_notes' => $attendance_note[$sv]
          );

		 $dataout = array(
           'school_id' => $school_id, 
           'branch_id' => $branchid,
           'user_id' => $sv,
           'in_out_status'=>'Out',
           'Date_Time' => $outtime,
           'is_active' => $is_active,
           'is_deleted' => $is_deleted,
           'created_date' => $created_date,
           'attendance_status' => $attendance_status[$sv], 
           'attendance_notes' => $attendance_note[$sv]
          );
	if($attendance_status[$sv] != '1'){	
		if($this->db->insert($this->tbl, $data)){
			$this->db->insert($this->tbl, $dataout);
			$response .= $this->db->insert_id().',';
		}
	}

	}
	if ($response ){
	return true;
		}
		else{
			return false;
		}
		//$this->db->insert_batch($this->tbl, $data); 
		/*if($this->db->insert_batch($this->tbl, $data)){
			$this->db->insert_batch($this->tbl, $dataout);
			return $this->db->insert_id();
		}
		   else
			return false; */
	 //echo $this->db->last_query();exit;
   }
   
  	  function getstaffstatus($branchid , $staffid , $attendancedate ){

  	  	$attendance_date 	 = date('Y-m-d', strtotime($attendancedate));
	
		$this->db->select('*');
		$this->db->from($this->tbl);

		$this->db->where(array('branch_id'=>$branchid,'user_id'=>$staffid) );

		if($attendancedate !=''){
		
		$this->db->where("DATE_FORMAT(Date_Time,'%Y-%m-%d')", $attendance_date);
		
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $query->row();
	  } 


   
  	 
}



 
