<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Modifyprogressreport extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','modifyprogressreport_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{
		$data							= array();
		$student_id = 	 $this->uri->segment(3); 
		$data['studentinfo'] = $this->modifyprogressreport_model->getStudentinfo($student_id);
		$school_id = $data['studentinfo']->school_id;
	
	    $data['terms'] = $this->modifyprogressreport_model->getTerms($school_id);
		
		$data['p_term_id']	  	    = $this->uri->segment(4);
		$data['p_year']	      	    = $this->uri->segment(5);
		
		$this->load->view('header');
		$this->load->view('progressreport/modify_progress_report', $data);
		$this->load->view('footer');

	}
	
		 public function check_term()

	{     
			$student_id = $_POST['student_id'];
			$school_id = $_POST['school_id'];
			$branch_id = $_POST['branch_id'];
			$class_id = $_POST['class_id'];
			$term = $_POST['termid'];
			$year = $_POST['year'];
	 $getreports = $this->modifyprogressreport_model->check_progress_report($student_id,$school_id,$branch_id,$class_id,$term,$year);
		if( count($getreports) > 0 ) { 
			 $HTML = ''; 
		   foreach($getreports as $report) {
			  $subjectid =  $report->subject_id;
			  $subjectname =  $this->modifyprogressreport_model->get_subject_name($subjectid,$school_id,$branch_id,$class_id);
			  $subjectname->subject_name;
			   
			  
$HTML.= '<input type="hidden" name="subject_id[]" value="'.$report->subject_id.'" />';

$HTML.= '<div class="col-sm-12 nopadding">';
$HTML.= '<div class="col-sm-12 prfltitlediv titlediv">';
$HTML.= '<h1>'.$subjectname->subject_name.'</h1>';
$HTML.= '</div>';
        
$HTML.= '<div class="profile-bg report_cls">';
  
$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Effort</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="effort['.$report->subject_id.']" id="effort_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->effort == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->effort == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->effort == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->effort == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';
  
$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Behaviour</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="behaviour['.$report->subject_id.']" id="behaviour_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->behaviour == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->behaviour == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->behaviour == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->behaviour == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Homework</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="homework['.$report->subject_id.']" id="homework_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->home_work == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->home_work == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->home_work == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->home_work == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Proof reading</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="proofreading['.$report->subject_id.']" id="proofreading_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Incomplete"';
 if($report->proof_reading == 'Incomplete'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Incomplete</option>';
 
$HTML.= '<option value="Amendments Required"';
if($report->proof_reading == 'Amendments Required'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Amendments Required</option>';

$HTML.= '<option value="Approved by Teacher"';
if($report->proof_reading == 'Approved by Teacher'){
$HTML.= ' selected ';
}
$HTML.= '>Approved by Teacher</option>';


$HTML.= '<option value="Complete"';
if($report->proof_reading == 'Complete'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Complete</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Comments</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<textarea class="form-control" rows="3" name="comment['.$report->subject_id.']" id="comment_'.$report->subject_id.'">'.$report->comment.'</textarea>';

$HTML.= '</div>';

$HTML.= '</div>';
 
$HTML.= '</div>';
  
$HTML.= '</div>';
			 
		 }	
		 
		 $HTML.= '<div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-6 nopadding">

        <div class="cancellink">

        <!--<input type="button" value="Cancel">-->

        </div>

        </div>

        <div class="col-sm-6 nopadding">

        <div class="confirmlink">

        <input type="submit" name="modifyprogressreportBtn" id="modifyprogressreportBtn" value="Confirm Modification">

        </div>

        </div>

        </div>';
			
			echo $HTML; 
		
		 }  else {
			 
	$subjects_Info = $this->modifyprogressreport_model->getSubjects($school_id,$branch_id,$class_id);	
	if(count($subjects_Info) > 0) { 
	
	$HTML = ''; 
	
	 foreach($subjects_Info as $subjectinfo) {
		 
$HTML.= '<input type="hidden" name="subject_id[]" value="'.$subjectinfo->subject_id.'" />';

$HTML.= '<div class="col-sm-12 nopadding">';
$HTML.= '<div class="col-sm-12 prfltitlediv titlediv">';
$HTML.= '<h1>'.$subjectinfo->subject_name.'</h1>';
$HTML.= '</div>';
        
$HTML.= '<div class="profile-bg report_cls">';
  
$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Effort</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="effort['.$subjectinfo->subject_id.']" id="effort_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory">Satisfactory</option>';
 
$HTML.= '<option value="Good">Good</option>';

$HTML.= '<option value="Very Good">Very Good</option>';

$HTML.= '<option value="Excellent">Excellent</option>';
  
$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Behaviour</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="behaviour['.$subjectinfo->subject_id.']" id="behaviour_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory">Satisfactory</option>';
 
$HTML.= '<option value="Good">Good</option>';

$HTML.= '<option value="Very Good">Very Good</option>';

$HTML.= '<option value="Excellent">Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Homework</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="homework['.$subjectinfo->subject_id.']" id="homework_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory">Satisfactory</option>';
 
$HTML.= '<option value="Good">Good</option>';

$HTML.= '<option value="Very Good">Very Good</option>';

$HTML.= '<option value="Excellent">Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Proof reading</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="proofreading['.$subjectinfo->subject_id.']" id="proofreading_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Incomplete">Incomplete</option>';
 
$HTML.= '<option value="Amendments Required">Amendments Required</option>';

$HTML.= '<option value="Approved by Teacher">Approved by Teacher</option>';

$HTML.= '<option value="Complete">Complete</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Comments</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<textarea class="form-control" rows="3" name="comment['.$subjectinfo->subject_id.']" id="comment_'.$subjectinfo->subject_id.'"></textarea>';

$HTML.= '</div>';

$HTML.= '</div>';
 
$HTML.= '</div>';
  
$HTML.= '</div>';
		 
	    }

$HTML.= '<div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-6 nopadding">

        <div class="cancellink">

       <!-- <input type="button" value="Cancel">-->

        </div>

        </div>

        <div class="col-sm-6 nopadding">

        <div class="confirmlink">

        <input type="submit" name="modifyprogressreportBtn" id="modifyprogressreportBtn" value="Confirm Modification">

        </div>

        </div>

        </div>';
		
	 echo $HTML; 
	 
	 } else {
		 $HTML = ''; 
		 $HTML.= '<div class="col-sm-12 nopadding"><div class="profile-bg report_cls"> <h2 style="color:#000"> No subjects found </h2></div></div>';
		 echo $HTML; 
	         }
			 
		 }
		
		 
	  }
	  
	  
	  	  function check_progress_peport_status() {
	
			$subjectCount =  count(array_filter($this->input->post('subject_id')));
			$effortCount = count(array_filter($this->input->post('effort')));
			$behaviourCount =  count(array_filter($this->input->post('behaviour')));
			$homeworkCount = count(array_filter($this->input->post('homework')));
			$proofreadingCount =  count(array_filter($this->input->post('proofreading')));
			$commentCount = count(array_filter($this->input->post('comment')));
	
 if($subjectCount == $effortCount && $subjectCount == $behaviourCount && $subjectCount == $homeworkCount && $subjectCount == $proofreadingCount){
		           echo json_encode(array('Status'=>"true"));
			      } else {
			        echo json_encode(array('Status'=>"false"));
				  }
			
	  }
	  
	  
	  function update_progressreport() {
		  
		 /* $input	=  $this->input->post();
		  echo '<pre>';
		  print_r($input); exit;*/
		  
		  if ($this->input->post('student_id') != '') {
                 $student_id = trim($this->input->post('student_id'));
            }
			if ($this->input->post('school_id') != '') {
                $school_id = trim($this->input->post('school_id'));
            }
			if ($this->input->post('branch_id') != '') {
                  $branch_id = trim($this->input->post('branch_id'));
            }
			if ($this->input->post('class_id') != '') {
                 $class_id = trim($this->input->post('class_id'));
            }
			
			if ($this->input->post('term') != '') {
                 $term = trim($this->input->post('term'));
            }
			
			$year = date('Y');
			
$chkReport = $this->modifyprogressreport_model->check_progress_report($student_id,$school_id,$branch_id,$class_id,$term,$year);
			$countReport = count($chkReport);
		  
		  			if( $countReport > 0 ) {
			           $input	=  $this->input->post();
		        if($this->modifyprogressreport_model->updateReport($input)) {
		                  $this->session->set_flashdata('success', 'Progress report has been updated successfuly.');
	   redirect(base_url()."modifyprogressreport/index/$student_id");exit;
				       } else {
					      $this->session->set_flashdata('error', 'Some problem exists. Progress report has not been updated.');
       redirect(base_url()."modifyprogressreport/index/$student_id");exit;
				        }
						
			       }  else  {
					   
					    $input		=  $this->input->post();
		            if($this->modifyprogressreport_model->insertReport($input)) {
		                  $this->session->set_flashdata('success', 'Progress report has been added successfuly.');
	  redirect(base_url()."modifyprogressreport/index/$student_id");exit;
				       } else {
					      $this->session->set_flashdata('error', 'Some problem exists. Progress report has not been added.');
	  redirect(base_url()."modifyprogressreport/index/$student_id");exit;
				           }
				       
					   }
		  
		  }
		  
		  
	
	 public function get_autofill_term()

	{     
			$student_id = $_POST['student_id'];
			$school_id = $_POST['school_id'];
			$branch_id = $_POST['branch_id'];
			$class_id = $_POST['class_id'];
			$term = $_POST['termid'];
			$year = $_POST['year'];
	 $getreports = $this->modifyprogressreport_model->check_progress_report($student_id,$school_id,$branch_id,$class_id,$term,$year);
		if( count($getreports) > 0 ) { 
			 $HTML = ''; 
		   foreach($getreports as $report) {
			  $subjectid =  $report->subject_id;
			  $subjectname =  $this->modifyprogressreport_model->get_subject_name($subjectid,$school_id,$branch_id,$class_id);
			  $subjectname->subject_name;
			   
			  
$HTML.= '<input type="hidden" name="subject_id[]" value="'.$report->subject_id.'" />';

$HTML.= '<div class="col-sm-12 nopadding">';
$HTML.= '<div class="col-sm-12 prfltitlediv titlediv">';
$HTML.= '<h1>'.$subjectname->subject_name.'</h1>';
$HTML.= '</div>';
        
$HTML.= '<div class="profile-bg report_cls">';
  
$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Effort</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="effort['.$report->subject_id.']" id="effort_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->effort == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->effort == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->effort == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->effort == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';
  
$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Behaviour</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="behaviour['.$report->subject_id.']" id="behaviour_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->behaviour == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->behaviour == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->behaviour == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->behaviour == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Homework</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="homework['.$report->subject_id.']" id="homework_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->home_work == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->home_work == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->home_work == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->home_work == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Proof reading</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="proofreading['.$report->subject_id.']" id="proofreading_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Incomplete"';
 if($report->proof_reading == 'Incomplete'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Incomplete</option>';
 
$HTML.= '<option value="Amendments Required"';
if($report->proof_reading == 'Amendments Required'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Amendments Required</option>';

$HTML.= '<option value="Approved by Teacher"';
if($report->proof_reading == 'Approved by Teacher'){
$HTML.= ' selected ';
}
$HTML.= '>Approved by Teacher</option>';


$HTML.= '<option value="Complete"';
if($report->proof_reading == 'Complete'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Complete</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Comments</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<textarea class="form-control" rows="3" name="comment['.$report->subject_id.']" id="comment_'.$report->subject_id.'">'.$report->comment.'</textarea>';

$HTML.= '</div>';

$HTML.= '</div>';
 
$HTML.= '</div>';
  
$HTML.= '</div>';
			 
		 }	
		 
		 $HTML.= '<div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-6 nopadding">

        <div class="cancellink">

        <!--<input type="button" value="Cancel">-->

        </div>

        </div>

        <div class="col-sm-6 nopadding">

        <div class="confirmlink">

        <input type="submit" name="modifyprogressreportBtn" id="modifyprogressreportBtn" value="Confirm Modification">

        </div>

        </div>

        </div>';
			
			echo $HTML; 
		
		 }
	  
	}

}
