<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 class Useraccess_model extends CI_Model {
	var $tbl	= "staff";
	var $users = "users";
	var $staff_access = "staff_access";
	var $branch = "branch";

    function __construct() {
        parent::__construct();
    } 
	
	   function getRows($where=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			 $this->db->where("($this->tbl.staff_fname LIKE '%$word_search%' OR $this->tbl.staff_lname LIKE '%$word_search%' OR $this->tbl.staff_telephone LIKE '%$word_search%' OR users.email LIKE '%$word_search%')", NULL, FALSE);
			 $this->db->where("CONCAT(staff_fname, ' ', staff_lname) LIKE '%".$word_search."%'", NULL, FALSE);
		  }
		  $this->db->order_by($this->tbl.".staff_id", "desc");
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	   function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.school_id'=>$school_id));
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			 $this->db->where("($this->tbl.staff_fname LIKE '%$word_search%' OR $this->tbl.staff_lname LIKE '%$word_search%' OR $this->tbl.staff_telephone LIKE '%$word_search%' OR users.email LIKE '%$word_search%')", NULL, FALSE);
			 $this->db->or_where("CONCAT(staff_fname, ' ', staff_lname) LIKE '%".$word_search."%'", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("users.id", "desc"); 	
		   }
		
		$this->db->limit($limit, $start);
          $query = $this->db->get();
		  $this->db->last_query(); 
           return  $query->result();

	 }
	 
	 function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
     }
	 
	 function get_staff_access($school_id,$staff_id) {
        $this->db->select('*');
        $this->db->from($this->staff_access);
		$this->db->where(array("school_id="=>$school_id,"user_id="=>$staff_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->row();
     }
	 
	 function check_user_access($school_id,$accessuserId) {
        $this->db->select('*');
        $this->db->from($this->staff_access);
		$this->db->where(array("school_id="=>$school_id,"user_id="=>$accessuserId,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
     }
	 
	 function updateAccessmenu($data,$where){
		$this->db->where($where);
		if($this->db->update($this->staff_access, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	  }
	  
	  function insertAccessmenu($data){
		if($this->db->insert($this->staff_access, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	     }
		 
	 function get_staff_info($school_id,$userid) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id="=>$school_id,"user_id="=>$userid));
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->row();
     }
	 
	function updateStaff($data,$where){
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	  }
}
