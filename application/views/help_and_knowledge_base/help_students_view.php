<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>

<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>

    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

 <li><a href="<?php echo base_url(); ?>help">Help and Knowledgebase</a></li>


        <li class="edit">Students</li>        

        </ul>

        </div>

 

   <div class="block-content">	

      <div class="col-sm-12 tablediv nopadding">
      
      
      
      
      

    <div class="col-sm-3">
	<div class="list-block">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#add_new" aria-controls="home" role="tab" data-toggle="tab">Add New Student</a></li>
      <li role="presentation"><a href="#view_student" aria-controls="profile" role="tab" data-toggle="tab">View Students</a></li>
    </ul>
	</div>
    </div>
    <div class="col-sm-9">
      <div class="tab-content adm-dsh">
		<div role="tabpanel" class="tab-pane active" id="add_new">
			<h1 class="tab-title">What is Student Module?</h1>
			<p class="main-heading-cntnt"> This module will introduce the number of students  in an organization , how to add the new students in an organization or also view, update the records of the students  and delete etc.</p>
			
			<div class="admn-content">
					<h3 class="para-heading">Student module consist of two sections:</h3>
					<p class="heading-cntnt"> <strong>1) Add Student</strong></p>
					<p class="heading-cntnt"><strong>2) View Students</strong></p>
					
					<div class="compse-msg">
						<h3 class="para-heading">HOW TO ADD A NEW STUDENT</h2>
						<p class="heading-cntnt"> In this section user can add a new student for a particular branch. New student section consist of six steps .These are :</p>
					</div>
					
					<div class="compse-msg">
						<h3 class="para-heading">Step 1- Add child detail form will be open</h3>
						<p class="heading-cntnt"> Now below there is two buttons Reset and Next step. Reset button is used in case if user want to  change their details and want to fill it again but if user wants  to proceed with the further step so they can click on next step <br>
						<b>NOTE</b> - fields with asterisk(*) is mandatory to move on next step.</p>
						<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/add-new-student.jpg" class="stt">
					
						
						<h3 class="para-heading">Step 2- Add student to the branch.</h3>
						<p class="heading-cntnt"> Radio buttons with branch name and branch address will display.select the branch to move further.</p>
						<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/step-2.jpg" class="stt">
						
						
						<h3 class="para-heading">Step 3- Add student to the branch.</h3>
						<p class="heading-cntnt"> Add student to the class.</p>
						<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/step-3.jpg" class="stt">
						
						
						<h3 class="para-heading">Step 4- Select  fee band.</h3>
						<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/step-4.jpg" class="stt">
						
											
						<h3 class="para-heading">Step 5- Upload documents</h3>
						<p class="heading-cntnt"> Upload documents for particular student For multiple selection,click ctrl + left click(in Windows) & command + right click (in Macbook). </p>
						<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/step-5.jpg" class="stt">
						
						
						<h3 class="para-heading">Step 6- Add sibling</h3>
						<p class="heading-cntnt"> Add sibling with same address. After that submit your application using submit button. So by filling all the details user can successfully added.</p>
						<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/step-6.jpg" class="stt">
					</div>	
			</div>
	 </div>
	 <!-- Teacher_dashboard-->
      <div role="tabpanel" class="tab-pane" id="view_student">
	  
			<h1 class="tab-title">View Students</h1>
			<p class="main-heading-cntnt">This section display the listing of all the students with action button .</p>
			<div class="admn-content">
					<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/view_student_report.jpg" class="stt"><div class="compse-msg">
						<h3 class="para-heading">SEARCH:</h3>
						<p class="heading-cntnt">  User can search the student  by branch, class, status, sort by(a-z,z-a), name, email etc . User can also search by first name,last name and full name using search textbox.</p>
						<p><strong>Action button provides following functionalities such as:</strong>
						<p>user can  also view, update the records of the students  and delete etc.</p>
						
						<h3 class="para-heading">RELATION PERSON :</h3>
						<p class="heading-cntnt"> User can also add relation person of that student because in case of any emergency if parents are unable to come so in that  case guardian takes the responsibility to pick the student.</p>
						
						<h3 class="para-heading">MODIFY PROGRESS REPORT :</h3>
						<p class="heading-cntnt"> User can also modify progress report of that student.</p>
						
						<h3 class="para-heading">ACTIVE:</h3>
						<p class="heading-cntnt"> User can also deactivate the student by click on active of action button.</p>
						<p class="heading-cntnt"> Pagination is also there to select the number of records.</p>
					</div>	
			</div>
			

	  
	  
	  </div>
		
			
	</div><!-- STUDENT view Report -->	
	  
	  
	  
	  </div>
    </div>
    </div>
      
      

         

	   </div>
    
	 </div>

    </div>

<style>
.tabs-left, .tabs-right {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}

.tab-content.adm-dsh {
    background: #fff;
    width: 100%;
    display: inline-block;
    padding: 10px 20px;
    border: 1px solid #eee;
}
h1.tab-title {
    margin-top: 0 !important;
    margin-left: 0 !important;
    border: none !important;
    font-weight: 900;
    font-size: 32px !important;
}
.adm-dsh p {
    margin-left: 0;
}
p.main-heading-cntnt {
    margin-top: -44px;
    border-bottom: 1px solid #eeeeee;
    padding: 10px 0;
	margin-left:20px;
}
.admn-content {
    margin-top: -2px;
    padding-top: 10px;
    padding-left: 20px;
}
h3.para-heading {
        margin-top: 0px;
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
.admn-content p {
    margin-top: -7px;
}
img.stt {
    width: 100%;
    padding: 30px 0;
	margin-top:-10px;
}
.compse-msg {
    padding: 4px 0 0 0px;
}
h4.txt-frmt {
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
ul.msg-point {
    padding-left: 0;
    margin: 0 0 0 14px;
    line-height: 30px;
    font-size: 15px;
}
.compse-msg {
    padding: 20px 0 16px 0px;
    line-height: 28px;
}
h3.para-heading.mgn {
    margin-top: 20px;
}
.list-block {
    height: 634px;
    width: 100% !important;
    background: #fff;
}
p.spacing {
    line-height: 15px;
    margin-top: -17px;
}
</style>