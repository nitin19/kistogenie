<?php
function multi_attach_mail($to, $subject, $message, $senderMail, $senderName, $files){

    $from = $senderName." <".$senderMail.">"; 
    $headers = "From: $from";

    // boundary 
    $semi_rand = md5(time()); 
    $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 

    // headers for attachment 
    $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\""; 

    // multipart boundary 
    $message .= "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
    "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n"; 

// preparing attachments
for($x=0;$x<count($files);$x++){
	
	 $tmp_name ='./uploads/notification/'.$files[$x]; 
	 
    $file = fopen( $tmp_name,"rb");
    $data = fread($file,filesize($tmp_name));
    fclose($file);
    $data = chunk_split(base64_encode($data));
    $message .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$files[$x]\"\n" . 
    "Content-Disposition: attachment;\n" . " filename=\"$files[$x]\"\n" . 
    "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
    $message .= "--{$mime_boundary}\n";
}
   
    $message .= "--{$mime_boundary}--";
    $returnpath = "-f" . $senderMail;

    //send email
    $mail = @mail($to, $subject, $message, $headers, $returnpath); 

    //function return true, if email sent, otherwise return fasle
    if($mail){ return TRUE; } else { return FALSE; }

}

//email variables
$to = 'binarydata2016@gmail.com';
$from = 'binarydata2017@gmail.com';
$from_name = 'binarydata';

//attachment files path array
$files = array('confidence.jpg','Jellyfish.jpg','1496294786Sample-Portfolio-Review-Analysis1.pdf');
$subject = 'PHP Email with multiple attachments by CodexWorld'; 
$html_content = '<h1>PHP Email with multiple attachments by Binarydata</h1>
            <p><b>Total Attachments : </b>'.count($files).' attachments</p>';

//call multi_attach_mail() function and pass the required arguments
$send_email = multi_attach_mail($to,$subject,$html_content,$from,$from_name,$files);

//print message after email sent
echo $send_email?"<h1>Mail Sent</h1>":"<h1>Mail sending failed.</h1>";

?>