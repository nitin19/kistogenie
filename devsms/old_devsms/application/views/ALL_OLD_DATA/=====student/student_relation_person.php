


<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="#">Student Relation Form</a></li><span class="fa fa-question-circle"  tool-tip-toggle="tooltip-demo" data-original-title="Add student guardian here.If parents are unable to pick the student,Guardian takes the responsibility for that."></span>        

        </ul>

        </div>

       </div>

    	
                <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>

        

        <div class="attendancesec">	
        	<div class="col-sm-12 profile-bg filterbox generatefilter">
        
     
               <?php 
		$editedRelationid = $this->uri->segment(4); 
		$Action = $this->uri->segment(5); 
		if($Action == 'edit') {
		?>
<form action="<?php echo base_url()?>studentsrelationperson/update_relation/<?php echo $info->student_id;?>/<?php echo $info->student_relation_id;?>" name="studentrelationform" id="studentrelationform" method="post">

<div class="col-sm-12 col-xs-12 applycodediv nopadding">
	<div class="col-sm-8 nopadding selectoption">

    	<div class="form-group fullwidthinput">

    	<label class="col-sm-1 control-label nopadding filterlabel"> Relation: </label>

    	<div class="col-sm-11 nopadding selectfilter" style="margin-bottom:35px">

   		 <div class="col-sm-3 inputbox termselect">
   	
     	 <input type="text" name="relation_full_name" id="relation_full_name"  placeholder="Enter Full Name" class="form-control"  value="<?php echo $info->student_relation_fullname; ?>">

    </div>
    
    <div class="col-sm-3 inputbox termselect">

      <input type="text" name="relation" id="relation" class="form-control" value="<?php echo $info->student_relation; ?>" >
      </div>
    
    <div class="col-sm-3 inputbox termselect">
    <input type="text" name="mobile_no" id="mobile_no" class="form-control" value="<?php echo $info->student_relation_mobile_number ; ?>" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
        </div>
         <div class="col-sm-3 inputbox termselect">
              <div class="confirmlink">
        <input type="submit" class="open1" value="Update">
        </div>
        	</div>
     			</div>
     
    			</div>
	        </div>
 
    </div>

    </form>
    
     <?php } else { ?>           
<form action="<?php echo base_url()?>studentsrelationperson/add_relation" name="studentrelationform" id="studentrelationform" method="post" autocomplete="off">


<div class="col-sm-12 col-xs-12 applycodediv nopadding">

 <div class="col-sm-8 nopadding selectoption">

    <div class="form-group fullwidthinput">

    <label class="col-sm-1 control-label nopadding filterlabel"> Relation: </label>

    <div class="col-sm-11 nopadding selectfilter" style="margin-bottom:35px">

    <div class="col-sm-3 inputbox termselect">
    <input type="hidden" name= "studentid" value="<?php echo $student_id;?>" />

      <input type="text" name="relation_full_name" id="relation_full_name"  placeholder="Enter Full Name" class="form-control" >

    </div>
    
    <div class="col-sm-3 inputbox termselect">

      <input type="text" name="relation" id="relation"  placeholder="Enter Relation" class="form-control" >
      </div>
    
    <div class="col-sm-3 inputbox termselect">
    <input type="text" name="mobile_no" id="mobile_no" placeholder="Enter Mobile No." class="form-control"  maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
        </div>
         <div class="col-sm-3 inputbox termselect">
              <div class="confirmlink">
        <input type="submit" class="open1" value="Add">
        </div>
        	</div>
     			</div>
     
    			</div>
	        </div>
       </div>
     </form>
     
         <?php } ?>
  
        </div>
        </div>


    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <h1>Relations<span></span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2"> No.</th>
                      
                      <th class="column5">Student Relation Fullname </th>
                        
					  <th class="column5">Student Relation </th>
                      
					 <th class="column5"> Student Relation Mobile No.</th>
                     
                     <!--<th class="column3">Desc</th>-->
                    
                     <!--<th class="column4">Status</th>-->
                     
                      <th class="column3">Action</th>

				  </tr>

			  </thead>

				<tbody>

					<?php if(count($relation)>0)
							{	$sr=0;
								foreach($relation as $student_relation)
								{	
								$sr++;	
					?>
					<tr class="familydata students">
                    
						<td class="column2"><?php echo $sr; ?></td>
                        <td class="column5"><?php echo $student_relation->student_relation_fullname ;?></td>
                        <td class="column5"><?php echo $student_relation->student_relation ;?></td>
                        <td class="column5"><?php echo $student_relation->student_relation_mobile_number ;?></td>
                        
                       
                        
                         <td class="column3">
                         
                         <div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
                                                
	      <li><a href="<?=base_url();?>studentsrelationperson/index/<?php echo $student_relation->student_id;?>/<?php echo $student_relation->student_relation_id;?>/edit"> Edit </a></li>
          
<!--				<li>
					<?php if($student_relation->is_active == '1'){ ?>
	<a href="<?php echo base_url();?>studentsrelationperson/deactivaterelation/<?php echo $student_relation->student_id;?>/<?php echo $student_relation->student_relation_id;?>" title="click to de-activate relation">Active</a>
				
					<?php
					 } else {
					?>
	<a href="<?php echo base_url();?>studentsrelationperson/deactivaterelation/<?php echo $student_relation->student_id;?>/<?php echo $student_relation->student_relation_id;?>" title="click to activate relation">De-Activated </a>
				<?php } ?>
				</li>-->
                
			<li class="divider"></li>
				 <li><a onclick="delConfirm(this)" data-name="<?php echo $student_relation->student_relation_id; ?>" data-id="<?php echo $student_relation->student_id; ?>" style="cursor: pointer;"> Delete </a> </li>
									</ul>
								</div>
                         
                         </td>
                                        
                                        
                        </tr>
                        <?php } }
							else{
						?>
				
                    <tr><th colspan="7" style="text-align: center; width:1215px; height:100px; font-size:25px;background:#FFF;color: #6a7a91;">No record to show.</th></tr>	
				  
					<?php } ?> 
				</tbody>

		  </table>

	</div>

</div>
	
    </div>

    </div>

    </div>
</div>
 <script>
  jQuery(document).ready(function(){
	  
	  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
					
  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });
	
    jQuery("#studentrelationform").validate({
        rules: {
                relation_full_name: {
                 required: true,
				 lettersonly: true
                },
				relation: {
                required: true,
				lettersonly: true
                },
			   mobile_no: {
			  required: true,	
			  number: true,
			  minlength: 11,
			  maxlength: 11
        },

        },
        messages: {
              relation_full_name: {
                  required: "Please Enter Relation Fullname.",
				   lettersonly:"Please enter letters only"
                 },
			 relation: {
                  required: "Please Enter Student Relation.",
				  lettersonly:"Please enter letters only"
                 },
			  mobile_no: {
                  required: "This field is required.",
				  number: "Numbers only please.",
				  minlength: "Minimum 11 characters required.",
				  maxlength: "Maximum 11 characters allowed."
                 },

            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		 jQuery.validator.addMethod("only_number", function(value, element) {
  return this.optional(element) || /^[0-9]+$/i.test(value);
}, "Digits(0-9) only please"); 
	});
</script>

 <script type="text/javascript">
	function delConfirm(obj){
		var stid=$(obj).attr('data-id');
		var relid=$(obj).attr('data-name');
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>studentsrelationperson/deleterelation/?relid="+relid+"&studid="+stid;
		}else{
			return false;
		}
	}
</script>		
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>

<script type="text/javascript">
 
$(document).ready(function(){
 
    $('[tool-tip-toggle="tooltip-demo"]').tooltip({
 
        placement : 'top'
 
    });
 
});
 
</script>
<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.fullwidthinput select {
    background-position: 95% center;
}
.Inactive {
    background: #ff0000 none repeat scroll 0 0;
}
.cleanSearchFilter {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533 !important;
    border-radius: 3px;
    color: #fff !important;
    padding: 9px;
    text-decoration: none !important;
}
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px 0 0;
}
.feeband{
font-size:14px;
}
@media screen and (min-width:320px) and (max-width:480px){
.col-sm-9.col-xs-6.paginationblk {
	text-align:center;
}
.cleanSearchFilter {
    padding: 8px 5px !important;
}
.termsearch {
    padding-bottom: 10px !important;
}
.viewreport {
    padding: 0 4px;
    width: 100%;
}
.searching {
    width: 100% !important;
}
.feeband{
font-size:14px !important;
}
}
@media screen and (min-width:481px) and (max-width:767px){
.col-sm-4.inputbox.termselect {
    padding-bottom: 15px;
}
.selectoption {
    width: 100%;
	padding-bottom: 10px;
}
.selectfilter .col-sm-3.col-xs-3.inputbox.termselect {
    width: 33% !important;
}
.feeband{
font-size:14px !important;
}
}
@media screen and (min-width:768px) and (max-width:991px){
.fullwidthinput label {
	width:auto !important;
}
.col-sm-3.col-xs-3.inputbox.termselect {
    padding-bottom: 10px;
    width: 33%;
}
.rightspace a {
    font-size: 14px;
}
.totaldiv h3 {
    font-size: 14px;
    text-align: center;
}
}
@media screen and (min-width:992px) and (max-width:1199px){
.selectfilter .col-sm-3.col-xs-3.inputbox.termselect {
    width: 33%;
}
.cleanSearchFilter {
    padding: 11px 3px;
}
.applycodediv .form-group input {
    font-size: 10px;
}
.rightspace a {
    font-size: 11px;
}
.btn {
    padding: 9px 5px;
}
.searchbox {
	padding:7px;
}
.statusbtn {
    padding: 6px 10px;
}
.totaldiv h3 {
    text-align: left;
}
}
.confirmlink input {
    width: 100px !important;
	margin: 0px;
}
.confirmlink {
    float: left;
}
.attendancesec {
    float: left;
    width: 100%;
}
.fa-question-circle + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.fa.fa-question-circle{color:#090;}
</style>	