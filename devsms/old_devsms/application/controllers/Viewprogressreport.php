<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Viewprogressreport extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','viewprogressreport_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{   
	  $data 			  =  array();     
		
		  $student_id = 	 $this->uri->segment(3); 
		  $data['studentInfo'] = $this->viewprogressreport_model->getStudentinfo($student_id);
		  $school_id = $data['studentInfo']->school_id;
		  $branch_id = $data['studentInfo']->student_school_branch;
		  $class_id = $data['studentInfo']->student_class_group;
		 
		  $term_id		  = $this->uri->segment(4);
		  $year		      = $this->uri->segment(5);
		 
		$data['student_id']	  = $this->uri->segment(3);
		$data['branch_id']	  = $data['studentInfo']->student_school_branch;
		$data['class_id']	  = $data['studentInfo']->student_class_group;
		$data['term_id']	  = $this->uri->segment(4);
		$data['year']	  	  = $this->uri->segment(5);
		$data['school_id']    = $data['studentInfo']->school_id;

  $data['termInfo'] = $this->viewprogressreport_model->getTerminfo($term_id);	
  $data['branchInfo'] = $this->viewprogressreport_model->getBranchinfo($branch_id);	
  $data['classInfo'] = $this->viewprogressreport_model->getClassinfo($class_id);		
  $data['termReport'] = $this->viewprogressreport_model->get_term_report($student_id,$school_id,$branch_id,$class_id,$term_id,$year);
 
		$this->load->view('header');
		$this->load->view('progressreport/view_progress_report', $data);
		$this->load->view('footer');

	}

}

