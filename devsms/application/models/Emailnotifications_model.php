<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Emailnotifications_model extends CI_Model {

	    var $tbl = "notification";
		var $student	= "student";
	    var $users = "users";
		var $branch = "branch";
		var $classes = "classes";
		var $notification_email_cron = "notification_email_cron";
		var $staff = "staff";
		var $message_template= "message_template";

    function __construct() {
        parent::__construct();
    }
	
   function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	
 function get_staff($school_id, $branch_id) {
	$this->db->select('*');
    $this->db->from($this->staff);
	$this->db->where(array('school_id'=>$school_id));
	$this->db->where_in('branch_id', $branch_id); 
	$query = $this->db->get();
	$this->db->last_query(); 
    return $query->result();
   }
	
  function get_classes($school_id, $branch_id) {
	$this->db->select('*');
    $this->db->from($this->classes);
	$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
	$this->db->where_in('branch_id', $branch_id); 
	$query = $this->db->get();
	$this->db->last_query();
    return $query->result();
   }
   
   function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
  function getClassName($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where('class_id', $class_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
  function getStudents($school_id, $branch_id, $class_id) {
	$this->db->select('*');
    $this->db->from($this->student);
	$this->db->where(array('school_id'=>$school_id));
	$this->db->where_in('student_school_branch', $branch_id); 
	$this->db->where_in('student_class_group', $class_id); 
	$query = $this->db->get();
	$this->db->last_query(); 
    return $query->result();
   }
   
  function insertnotificationEmialcron($data){
		 if($this->db->insert($this->notification_email_cron, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
	
   function add_notification_staff($input){
	
	$user_id		= $this->session->userdata('user_id');
	$school_id  = $this->session->userdata('user_school_id');	
	
	$staffid  = 	$input['staff_name'];
	$subject  = 	$input['staff_email_subject'];
	$staff_email_message  = $input['staff_email_message'];
	
	$branch = '';
	$attachments = '';
	$created_date = date('Y-m-d H:i:s');;
	$is_active = '1';
	$is_deleted = '0';
	$read_by = '';
	
	$data =array();
	for ($i = 0; $i < count($staffid); $i++) {
		 $sv = $staffid[$i];
		 $data[$i] = array(
           'school_id' => $school_id, 
           'branch_id' => $branch,
           'from' => $user_id,
           'to' => $sv,
		   'subject' => $subject, 
           'message' => $staff_email_message,
           'attachments' => $attachments,
           'is_active' => $is_active,
		   'is_deleted' => $is_deleted, 
           'created_date' => $created_date,
           'read_by' => $read_by
          );
	}
		if($this->db->insert_batch($this->tbl, $data))
			return $this->db->insert_id();
		   else
			return false;
		 // $this->db->last_query();exit;
   }
	
  function add_notification($input){
	
	$user_id		= $this->session->userdata('user_id');
	$school_id  = $this->session->userdata('user_school_id');	
	
	$studentid  = 	$input['studentsname'];
	$subject  = 	$input['student_email_subject'];
	$student_email_message  = 	$input['student_email_message'];
	
	$branch = '';
	$attachments = '';
	$created_date = date('Y-m-d H:i:s');;
	$is_active = '1';
	$is_deleted = '0';
	$read_by = '';
	
	$data =array();
	for ($i = 0; $i < count($studentid); $i++) {
		 $sv = $studentid[$i];
		 $data[$i] = array(
           'school_id' => $school_id, 
           'branch_id' => $branch,
           'from' => $user_id,
           'to' => $sv,
		   'subject' => $subject, 
           'message' => $student_email_message,
           'attachments' => $attachments,
           'is_active' => $is_active,
		   'is_deleted' => $is_deleted, 
           'created_date' => $created_date,
           'read_by' => $read_by
          );
	}
		if($this->db->insert_batch($this->tbl, $data))
			return $this->db->insert_id();
		   else
			return false;
		 // $this->db->last_query();exit;
   }

      function getmessages($user_type) {
        $this->db->select('*');
        $this->db->from($this->message_template);
		$this->db->where(array("user_type="=>$user_type,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
    
        return  $query->result();
    }
      function get_singlemsg($message_id) {
        $this->db->select('*');
        $this->db->from($this->message_template);
		$this->db->where(array("message_id="=>$message_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
   // $this->db->last_query();exit;
        return  $query->row();
    }
   

}
