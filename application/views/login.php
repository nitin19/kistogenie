<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>School Management</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">

  </head>

  <body>



   <div class="loginpage">

   		<div class="container">

        	<div class="col-sm-12">

            	<div class="loginsection">

                	<div class="logodiv">

                    	<a href="https://www.kistogenie.com"><img class="logo" src="<?php echo base_url();?>assets/images/logo.PNG"></a>

                        </div>

                	<div class="loginheader">

                        <h1>Olive Tree Study Support</h1>

                    </div>

                    <div class="loginform-content">

                    

                    <form class="loginform" name="loginForm" id="loginForm" action="" method="post">

                   

                    <div class="col-sm-12 nopadding revealinput">

                    	<!--<div class="col-sm-7 col-xs-7 revealinputbox">

  					<div class="form-group">

    				<label>Enter your realm</label>

    				<input type="text" class="form-control" id="enteryourrealm" placeholder="2 - 5 - 4 - 7">

  					</div>

                    </div>

                    	<div class="col-sm-5 col-xs-5 revealbtn">

  					<input type="button" class="btn btn-default" value="Take me there">

                    </div>-->

                    </div>

                    <div class="loginformarea">

                    	<div class="col-sm-12 logintitle nopadding">

                        <img src="<?php echo base_url();?>assets/images/dividers.png">

                        <h1>Log In to Dashboard</h1>

                        </div>

                        <div class="loginformblk">

                       

                        <?php if($error!='') {  

                         echo '<div class="alert alert-danger" id="flashMessage">'.$error.'</div>';

                          } ?>

                          

                        <div class="form-group">

    					<label>Log In with your username</label>

    					<input type="text" class="form-control" name="username" id="username" value="<?php if( isset($_COOKIE['username'])){echo $_COOKIE['username'];} ?>" placeholder="Username">

  						</div>

  						<div class="form-group">

    					<label for="exampleInputPassword1">Password</label>

    					<input type="password" class="form-control" name="userpass" id="userpass" value="<?php if( isset($_COOKIE['userpass'])){echo $_COOKIE['userpass'];} ?>" placeholder="Password">

  						</div>

  						<div class="checkbox checkboxarea">

    					<input type="checkbox" id="checkbox-1" name="group2" <?php if( isset($_COOKIE['username']) && isset($_COOKIE['userpass'])){ echo 'checked';}?>>

      					<label for="checkbox-1"><span class="checkbox">Remember Me</span></label>

  						</div>

                        <div class="login-button">

  						<input type="submit" class="btn btn-default" value="Log In to Dashboard">

                        </div>

                        <div class="logininfo">

                         <p>If you would like more information regarding the School Companion, or need to get in touch, please visit our corporate website <a href="http://www.olivetreestudy.co.uk/" target="_blank">here</a> or send us an <a href="mailto:info@olivetreestudy.co.uk" >email </a>
                         <p>
							Please click here for <a href="<?php echo base_url();?>attendanceregister">Staff Attendance</a>
                        </p>

                        </div>

                        </div>

                       

                    </div>

                    

					</form>

                    </div>

                    <div class="login-links">

                    	<div class="col-sm-8 col-xs-8 linkleft loginlinks nopadding">

                        <span style="color:#7f8ea3">Don't have an account yet? <a href="<?php echo base_url();?>register">Get Started</a></span>

                        </div>

                        <div class="col-sm-4 col-xs-4 linkright loginlinks nopadding">

                        <a href="<?php echo base_url();?>forgotpassword">Forgot Password?</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

   </div>

   <footer>

   <div class="container">

   <div class="copyright">

  <p>© <?php echo date("Y");?> Olive Tree Study Support. All Rights Reserved. | Powered by : <span><a href="http://www.binarydata.in/" target="_blank">Binary Data</a></span></p>

   </div>

   </div>

   </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->

     <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

     <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

      <script>

   jQuery(document).ready(function(){

	setTimeout(function() {

            jQuery('#flashMessage').slideUp('slow');

            }, 3000);

			

    jQuery("#loginForm").validate({

        rules: {

            username: "required",

            userpass: {

            required: true,

             }

        },

        messages: {

            username: "Please enter your username",

            userpass: {

                required: "Please enter your password",

              }

        },

        

        submitHandler: function(form) {

            form.submit();

        }

    });



  });

  

  </script>

  

 <style>

 #loginForm .error { color:#F00; }
 .loginlinks a:hover{text-decoration: underline;}
 .loginlinks a {
    color: #00aeef;
    font-size: 14px;
    text-align: right;
}
 </style> 

  </body>

</html>