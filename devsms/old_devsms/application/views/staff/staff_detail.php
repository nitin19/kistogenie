<?php
/*echo "<pre>";
print_r($result);
echo "</pre>";
exit();*/
?>



<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">
<?php foreach ($result as $data): ?>
    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

       

        <li class="edit"><a href="#">Details</a></li>        

        </ul>

        </div>

<!--        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>
-->
        </div>
		<div class="col-sm-12 nopadding profile-bg accsection">
          <h1>Details : <?php echo $data->staff_fname. ' ' . $data->staff_lname; ?></h1>
		</div>
        <div class="editprofileform editdetails">

        <form action="<?php echo base_url() . "staff/edit_staff/". $data->user_id; ?>"  method="post" name="editstaffpro" id="editstaffpro" enctype="multipart/form-data">

        <div class="profile-bg prfltitle profile_titlebg">

        <h2>Account details</h2>
<div class="col-sm-12 nopadding contentdiv">
  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Username<span>*</span></label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $data->username; ?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Password<span>*</span></label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $data->password; ?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Avtar<br/><!--<span class="tagline">JPG, max. 500KB</span>--></label>

    <div class="col-sm-9 col-xs-8 profile-picture inputbox">
	<?php if($data->profile_image !=''){?>
      <img src="<?php echo base_url();?>uploads/staff/<?php echo $data->profile_image ; ?>">
	<?php } else { ?>
        <img src="<?php echo base_url();?>assets/images/avtar.png">
        <?php } ?>
     <!-- <div class="profilepicbtns"><i class="fa fa-picture-o"></i><br/><hr class="borderline"><i class="fa fa-trash-o"></i></div>-->

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Full Name<span>*</span></label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $data->staff_fname. ' ' . $data->staff_lname ; ?></span>

    </div>

  </div>

   <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Title</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $data->staff_title ; ?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Date of Birth</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $data->staff_dob ; ?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Email</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

     <span><?php echo $data->email ; ?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">School Branch</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php
	  if( $data->branch_id!='' && $data->branch_id!='0') {
		   $branchid= explode(',', $data->branch_id);

			    $this->load->model(array('staff_model'));
		
			foreach($branchid as $branch_id) {
				
			$teacherbranch =$this->staff_model->getbranchName($branch_id);
			echo $teacherbranch->branch_name. ',&nbsp;'.'&nbsp;';
		  }
	  }
	    ?></span>

    </div>

  </div>
  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Classes</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php 
  if( $data->class_name!='') {
		  $class = explode(',', $data->class_name);
		  $this->load->model(array('staff_model'));
		  $teacherclass = $this->staff_model->getclassName($class);
		   foreach($teacherclass as $teachclass) {
			   echo $teachclass->class_name. ',&nbsp;'.'&nbsp;';
		   }
          }
	    ?></span>

    </div>

  </div>
  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Subjects</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php 
  if( $data->subject_name!='') {
		  $subject = explode(',', $data->subject_name);
		  $this->load->model(array('staff_model'));
		  $teachersubject = $this->staff_model->getsubjectName($subject);
		   foreach($teachersubject as $teachsubject) {
			   echo $teachsubject->subject_name. ',&nbsp;'.'&nbsp;';
		   }
          }
	    ?></span>

    </div>

  </div>
  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Teacher Role</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php 
  if($data->staff_role_at_school!='') {
		  $role = $data->staff_role_at_school;
		  $this->load->model(array('staff_model'));
		  $teacherrole = $this->staff_model->getteacherrole($role);
		   echo $teacherrole->teacher_role;
          }
	    ?></span>

    </div>

  </div>
  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Teacher level</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php 
  if($data->staff_teacher_level!='') {
		  $level = $data->staff_teacher_level;
		  $this->load->model(array('staff_model'));
		  $teacherlevel = $this->staff_model->getteacherlevel($level);
		   echo $teacherlevel->teacher_level;
          }
	    ?>
	  </span>

    </div>

  </div>
  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Teacher Type</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php 
  if($data->staff_teacher_type!='') {
		  $type = $data->staff_teacher_type;
		  $this->load->model(array('staff_model'));
		  $teachertype = $this->staff_model->getteachertype($type);
		   echo $teachertype->teacher_type;
          }
	    ?>
	  </span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Education / Qualification</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $data->staff_education_qualification ; ?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Personal Summary</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

     <span><?php echo $data->staff_personal_summery ; ?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Telephone</label>

    <div class="col-sm-9 col-xs-8 inputbox">

    <span><?php echo $data->staff_telephone ; ?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Address</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $data->staff_address ; ?></span>

    </div>

  </div>

  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Address(Optional)</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $data->staff_address_1 ; ?></span>

    </div>

  </div>
</div>
  </div>
   <?php if($access !=''){
	  ?>

 
<div class="profile-bg prfltitle profile_titlebg">
 

<h2>Access details</h2>

<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding accesscls">Access</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $access; ?></span>

    </div>

  
</div>

</div>
        
<?php } ?>

        

        <div class="cancelconfirm">

        <div class="col-sm-12 nopadding">

        <div class="confirmlink">
        <?php $logmode		= $this->session->userdata('log_mode'); 
				if($logmode == 'admin'){
				?>
        <input type="submit" value="Edit information" class="btn btn-default"></a>
<?php } ?>
        </div>

        </div>

        </div>

        </form>
<?php endforeach; ?>
        </div>

    </div>
    
 <style>
    .profile_titlebg h2{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1); 
	padding: 10px 15px;
	margin:0px; }
.profiletitltbox{padding-top:0px;}
.profiletitltbox .profile_titlebg {
    padding: 0;
}
.contentdiv{
	padding-left:15px;
	padding-top:15px;

}
.profile-bg {
	padding: 0px;
}

.accsection h1 {
    padding: 0 0 10px 15px;
	color:#393C3E;
}
.detailbox .control-label.accesscls {
    padding-left: 15px !important;
}
</style>  
   