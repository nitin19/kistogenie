<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teacherdashboard extends CI_Controller {
	
	var $logmode;
	var $tbl="users";
function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','Teacherdashboard_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	

	public function index()

	{	
		
		$data = array();
		$user_id =   $this->session->userdata('user_id');
                //$access	=   $this->session->userdata('access_student_attendance');
                //$staff_branch_id = $this->session->userdata('staff_branch_id')

		$where = 'student_leaving_date BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)';
		$data["total_rows"] 			=	$this->Teacherdashboard_model->totalnotification($user_id);
		$data["base_url"] = base_url() . "teacherdashboard/index";
		$data["uri_segment"] = 3;
		$data["per_page"]						= 10;
		$this->pagination->initialize($data);
		$page =  $this->uri->segment(3,0) ;
		 $data['last_page']				= $page;
		 $data["links"] = $this->pagination->create_links();
		
		$data['show']   =	$this->Teacherdashboard_model->show_notification($user_id, $data["per_page"], $page);
		
		$school			=	$this->Teacherdashboard_model->attendance_schoolid($user_id);
		$school_id		=   $school->school_id;
		$totalclass 	= 	$this->Teacherdashboard_model->attendance_classid($user_id);
		$data['ids'] 	= 	$totalclass->class_name;
		$data['recentstudent']	=	$this->Teacherdashboard_model->recentaddstudent($school_id);
		$data['leavingstudent'] = 	$this->Teacherdashboard_model->leavingstudent($school_id, $where);
		$this->load->view('header');
		$this->load->view('dashboard/teacherdashboard',$data);
		$this->load->view('footer');

	}
	
	public function insertmessage(){
		$message = array();
		$sendmessege = array();
		$user_id		=   $this->session->userdata('user_id');
		$userdata			=	$this->Teacherdashboard_model->attendance_schoolid($user_id);
		$school_id		=   $userdata->school_id;
		$branch_id		=   $userdata->branch_id;
		$message['from'] = $user_id;
		$message['school_id'] = $school_id;
		$message['branch_id'] = $branch_id;
		$message['is_active'] = '1';
		$message['is_deleted'] = '0';
		$message['created_date']		=	date('Y-m-d H:i:s');
		
		
		
		if($this->input->post('subject')!=''){
			$message['subject'] = $this->input->post('subject');
		}
		
		if($this->input->post('message')!=''){
			$message['message'] = $this->input->post('message');
		}
		$files 			= 	$_FILES;
        $count			= 	count($_FILES['attachments']['name']);
            for($i=0; $i<$count; $i++)
                {
                $_FILES['attachments']['name']		=	$files['attachments']['name'][$i];
                $_FILES['attachments']['type']		= 	$files['attachments']['type'][$i];
                $_FILES['attachments']['tmp_name']	= 	$files['attachments']['tmp_name'][$i];
                $_FILES['attachments']['error']		= 	$files['attachments']['error'][$i];
                $_FILES['attachments']['size']		= 	$files['attachments']['size'][$i];
                $config['upload_path'] 				= 	'./uploads/notification';
                $config['allowed_types'] 			= 	'gif|jpg|png|docx|jpeg|xls|pdf|txt|xlsx';
	  			$config['max_size']             	= 	2048;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload("attachments");
                $fileName = $_FILES['attachments']['name'];
                $images[] = $fileName;
                }
         $fileName = implode(',',$images);
		 $message['attachments'] = $fileName;
		 
		 if($this->input->post('toemail')!=''){
			$toemail = $this->input->post('toemail');
		
		 if (in_array("All", $toemail)){
			 $school_users		=	$this->Teacherdashboard_model->getall_id($school_id,$user_id);
		 
		 foreach($school_users as $users){
				 $to = $users->id;
				 $message['to'] = $to;
					$alluser = $this->Teacherdashboard_model->insert_notification($message);
				 }
		 
		if($alluser){
			   $this->session->set_flashdata('success', 'Message has been successfully send.');
			   redirect(base_url()."teacherdashboard/"); exit;
				}
		 }
		
		if (in_array("Admin", $toemail)){
			 $school_admin		=	$this->Teacherdashboard_model->admin($school_id);
			 
		 		$message['to'] = $school_admin->id;
		 
				 /*$message['to'] = $to;*/
				$sendmessege1 = 	$this->Teacherdashboard_model->insert_notification($message);
				 $sendmessege = $sendmessege1;
		}
			
			if (in_array("Staff", $toemail)){
			 $school_staff		=	$this->Teacherdashboard_model->getall_staff($school_id,$user_id);
		 
		 foreach($school_staff as $staff){
				 $to = $staff->id;
				 $message['to'] = $to;
				$sendmessege1 = 	$this->Teacherdashboard_model->insert_notification($message);
				 }
				 $sendmessege = $sendmessege1;
		}	 
		
		if (in_array("Student", $toemail)){
			 $school_student		=	$this->Teacherdashboard_model->getall_student($school_id);
		 
		 foreach($school_student as $student){
				 $to = $student->id;
				 $message['to'] = $to;
				$sendmessege1 = 	$this->Teacherdashboard_model->insert_notification($message);
				 }
				 $sendmessege = $sendmessege1;
		}	
		 }
		 
		if($this->input->post('touser')!=''){
			$touser = $this->input->post('touser');
		
		$emails =  substr(trim($touser), 0, -1); 
        $emailuser	= $this->Teacherdashboard_model->getuser_id($emails);
	
		if($emailuser){
			foreach($emailuser as $user){
			  $to = $user->id;
			 $message['to'] =  $to;
			 $sendmessege1 = 	$this->Teacherdashboard_model->insert_notification($message);
			}
			$sendmessege = $sendmessege1;
		}}
		
		if($sendmessege){
			   $this->session->set_flashdata('success', 'Message has been successfully send.');
			   redirect(base_url()."teacherdashboard/"); exit;
				}
				else{
			   $this->session->set_flashdata('error', 'Some problem exists. Messsage can not be send.');
				redirect(base_url()."teacherdashboard/");exit;
		      }
		
	}
	
			public function deletemsg($notificationid){
				if($this->Teacherdashboard_model->delete($notificationid))
					{
					$this->session->set_flashdata('success', 'Message has been deleted successfully.');
					redirect(base_url()."Teacherdashboard/");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."Teacherdashboard/");exit;
					}
				}
			 
	public function download_attachments($fileName = NULL) { 
	
       if ($fileName) {
        $file		=	realpath ( FCPATH.'uploads/notification/'.$fileName );
        if (file_exists ( $file )) {
         $data		=	file_get_contents ( $file );
         $this->load->helper('download');
         force_download ( $fileName, $data );
		 echo"doen";
        } else {
		 $this->session->set_flashdata('error', 'Requested file does not exists.');
         redirect(base_url()."Teacherdashboard/");exit;
        }
       }
      }
	
	public function getimage(){
		
		$user_id		=   $this->session->userdata('user_id');
		$img			=	$this->Teacherdashboard_model->getimage($user_id);
		$image			=	$img->profile_image;
		return $image;
		}
		
	public function searchemail(){
			$user_id		=   $this->session->userdata('user_id');
			$school			=	$this->Teacherdashboard_model->attendance_schoolid($user_id);
			$school_id		=   $school->school_id;
		 if (isset($_REQUEST['term'])){
		   $term = $_REQUEST['term'];
		 }
		 $data = array();
		$emaildata = $this->Teacherdashboard_model->autocomplete($term, $school_id);
		foreach($emaildata as $dataemail){
			$data[] = $dataemail->email;
					}
		echo json_encode($data);
}





}

