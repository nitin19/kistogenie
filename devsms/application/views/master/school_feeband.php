<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li class="edit">Feeband</li>        

        </ul>

        </div>

        </div>

    	
                <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>

        

        <div class="attendancesec">	
        
        
        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">
             
             
          <?php 
		$editedFeebandid = $this->uri->segment(4); 
		$Action = $this->uri->segment(5); 
		if($Action == 'edit') {
		?>
        
<form action="<?php echo base_url();?>schoolfeeband/updatefeeband/<?php echo $last_page;?>/<?php echo $editedFeebandid;?>" name="schoolfeebandForm" id="schoolfeebandForm" method="post">

<div class="col-sm-12 col-xs-12 applycodediv nopadding">

 <div class="col-sm-12 nopadding selectoption maindiv">

    <div class="form-group fullwidthinput">

    <label class="col-sm-2 control-label nopadding filterlabel">Branch:</label>

    <div class="col-sm-10 nopadding selectfilter" style="margin-bottom:35px">

    <div class="col-sm-2 inputbox termselect">

      <select class="form-control" name="schoolbranch" id="schoolbranch">
      <option value="">Select Branch</option>   
      <?php  foreach($branches as $branch) { ?> 
       <option <?php if($branch->branch_id == $info->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>  
         <?php } ?>
     </select>
    </div>
    
    <div class="col-sm-2 inputbox termselect">

      <select class="form-control" name="classname" id="classname">
      <option value="">Select Class</option>  
      <?php 
  if( $info->branch_id != '') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = $info->branch_id;
		  $this->load->model(array('school_feeband_model'));
		  $sclasses =$this->school_feeband_model->getclasses($schoolbranch,$school_id);
     foreach($sclasses as $sclass) { ?>
          <option <?php if($sclass->class_id == $info->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>
       
     </select>

      </div>
       <div class="col-sm-2 inputbox termselect">
             <?php 
			$fee=$info->fee_band_price;
	   		$mode=explode(' ',$fee);
			$Feecurrency = $mode[0];
			$Feeprice = $mode[1];
			$Feemode =  $mode[2];
					
	  ?>
       <select class="form-control" name="mode" id="mode" onchange="generatefee()">
       
       <?php
       $arr = array("weekly","monthly","half-yearly","yearly");
     foreach($arr as $value)
     {
      if($Feemode == $value)
      { 
      echo "<option value='$value' selected>$value</option>"; 
      }
      else
      {
      echo "<option value='$value'>$value</option>"; 
      }
     }
     ?>
      
     </select>

      </div>
          <div class="col-sm-2 inputbox termselect">
       <select class="form-control" name="currency" id="currency" onchange="generatefee()">
      <option value="">Select currency</option>   
      <?php  foreach($currencies as $currency) { ?> 
       <option <?php $symbol=  $currency->symbol;
	   				if($Feecurrency==$symbol){ echo'selected';}?> value="<?php echo $currency->symbol;?>"><?php echo $currency->symbol;?></option>  
         <?php } ?>
     </select>

      </div>
   
    <div class="col-sm-2 inputbox termselect">
     <input type="text" name="classfee" id="classfee" value="<?php echo $Feeprice?>"  placeholder="Enter fee" class="form-control" maxlength="20" autocomplete="off" onfocusout="generatefee()" >
      <input type="hidden" name="mainclassfee" id="mainclassfee" value=""  placeholder="" class="form-control" maxlength="20" autocomplete="off">
        </div>
   
     </div>
     
    </div>
    
   
 <!--<div class="form-group fullwidthinput">
    <div class="col-sm-10 nopadding selectfilter">
    <div class="col-sm-6 inputbox termselect">
    <input type="text" name="classfee" id="classfee" value="<?php //echo $info->fee_band_price;?>"  placeholder="Enter fee" class="form-control" >
        </div>
      </div>
    </div>-->
    
    
    <div class="form-group fullwidthinput">
    <label class="col-sm-2 control-label nopadding filterlabel">Description:</label>
    <div class="col-sm-10 nopadding selectfilter" id="selectfilter">
    <div class="col-sm-8 inputbox termselect">
    <textarea  class="form-control" name="classfeedescription" id="classfeedescription" placeholder="Write Description"  style="resize:none" cols="num" rows="num"> <?php echo $info->fee_band_description;?></textarea>
    </div>
   

      <div class="confirmlink col-sm-2 addbtn">
        <input type="submit" class="open1" value="Update">
        </div>
        
    
          </div>
       </div>
    
      </div>
    </div>

    </form>
        
        

    
     <?php } else { ?>
     

<form action="<?php echo base_url();?>schoolfeeband/addfeeband" name="schoolfeebandForm" id="schoolfeebandForm" method="post" autocomplete="off">


<div class="col-sm-12 col-xs-12 applycodediv nopadding">

 <div class="col-sm-12 nopadding selectoption maindiv">

    <div class="form-group fullwidthinput">

    <label class="col-sm-2 control-label nopadding filterlabel">Branch:</label>

    <div class="col-sm-10 nopadding selectfilter" style="margin-bottom:35px">

    <div class="col-sm-2 inputbox termselect">

      <select class="form-control" name="schoolbranch" id="schoolbranch">
      <option value="">Select Branch</option>   
      <?php  foreach($branches as $branch) { ?> 
       <option value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>  
         <?php } ?>
     </select>

    </div>
    
    <div class="col-sm-2 inputbox termselect">

      <select class="form-control" name="classname" id="classname">
      <option value="">Select Class</option>   
     </select>

      </div>
      <div class="col-sm-2 inputbox termselect">
       <select class="form-control" name="mode" id="mode" >
      <option value="">Select Mode</option>   
		<option value="weekly">Weekly</option>
        <option value="monthly">Monthly</option>
        <option value="half-yearly">Half-Yearly</option>
        <option value="yearly">yearly</option>
     </select>

      </div>
      <div class="col-sm-2 inputbox termselect">
       <select class="form-control" name="currency" id="currency">
      <option value="">Select currency</option>   
      <?php  foreach($currencies as $currency) { ?> 
       <option value="<?php echo $currency->symbol;?>"><?php echo $currency->symbol;?></option>  
         <?php } ?>
     </select>

      </div>
    
    <div class="col-sm-2 inputbox termselect">

    <input type="text" name="classfee" id="classfee" value=""  placeholder="Enter fee" class="form-control" maxlength="20" autocomplete="off" >
<!--     <input type="hidden" name="mainclassfee" id="mainclassfee" value=""  placeholder="" class="form-control" maxlength="20" autocomplete="off">
-->      
   </div>
     </div>
     
    </div>
    
    <div class="form-group fullwidthinput">
    <label class="col-sm-2 control-label nopadding filterlabel">Description:</label>
    <div class="col-sm-10 nopadding selectfilter" id="selectfilter">
    <div class="col-sm-8 inputbox termselect">
    <textarea  class="form-control" name="classfeedescription" id="classfeedescription" placeholder="Write Description" maxlength="251" autocomplete="off"> </textarea>
    </div>
   

      <div class="confirmlink col-sm-2 addbtn">
        <input type="submit" class="open1" value="Add">
        </div>
        
    
          </div>
       </div>
    
      </div>
    </div>

    </form>
        
    <?php } ?>
  

        </div>

    </div>
        

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

  <form action="<?php echo base_url();?>schoolfeeband/index" name="classfeefilterform" id="classfeefilterform">

                <div class="col-sm-12 col-xs-12 applycodediv nopadding">

				<div class="col-sm-7 nopadding selectoption">

<div class="form-group fullwidthinput">

    <label class="col-sm-1 col-xs-1 control-label nopadding filterlabel">Show</label>

    <div class="col-sm-11 col-xs-11 nopadding selectfilter">

    <div class="col-sm-3 col-xs-3 inputbox termselect">

      <select class="form-control" name="branch" id="branch">
  <option value="">Select Branch</option>    
  <?php foreach($branches as $branch) { ?>
     <option <?php if($branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
    <?php  } ?>
</select>

    </div>
    
    
    <div class="col-sm-3 col-xs-3 inputbox termselect">

      <select class="form-control" name="class" id="class">
  <option value="">Select Class</option>    
  <?php 
  if( @$_GET['branch']!='') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch =  @$_GET['branch'];
		  $this->load->model(array('school_feeband_model'));
		  $sclasses =$this->school_feeband_model->getclasses($schoolbranch,$school_id);
     foreach($sclasses as $sclass) { ?>
          <option <?php if($class_search == $sclass->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>
</select>

    </div>

    <div class="col-sm-3 col-xs-3 inputbox termselect">

<select class="form-control" name="status" id="status">
  <option value="">Select Status </option>    
   <option <?php if($status_search == "1"){ echo 'selected'; } ?> value="1">Active</option>
   <option <?php if($status_search == "0"){ echo 'selected'; } ?> value="0">Inactive</option>
</select>

    </div>

    </div>

  </div>

</div>		

<div class="col-sm-3 col-xs-5 searching termsearch nopadding">

        <div class="form-group">

      <input type="text" class="form-control searchbox" placeholder="Search By Fee" name="seachword" id="seachword" value="<?php echo $word_search;?>">

   <!--<input type="submit" value="" class="searchbtn">-->

     </div>

        </div>

        <div class="col-sm-2 col-xs-7 rightspace viewreport">

<div class="form-group">

    <input type="submit" class="btn btn-danger feeband" value="Find Fee Band">


  </div>

</div>

        </div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <h1>Feebands<span><?php echo $Totalrec = $total_rows;?></span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2"> No.</th>
                      
                      <th class="column3">Fee</th>
                        
					  <th class="column4"> Class </th>
                      
					 <th class="column4"> Branch </th>
                     
                     <!--<th class="column3">Desc</th>-->
                    
                     <th class="column4">Status</th>
                     
                      <th class="column3">Action</th>

				  </tr>

			  </thead>

				<tbody>

					<?php 
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $feeband) { 
					     $sr++;
						  $branch_id = $feeband->branch_id;
						  $class_id = $feeband->class_id;
						  $this->load->model(array('school_feeband_model'));
						  $branchName = $this->school_feeband_model->getbranchName($branch_id);
						  $ClassName = $this->school_feeband_model->getclassName($class_id);
					?>
					<tr class="familydata students">
						<td class="column2"><?php echo $sr; ?></td>
                        <td class="column3">
						<?php $feebandss = explode(' ',$feeband->fee_band_price); 
					echo $feebandss[0].' '.$feebandss[1].' '.$feebandss[2];
						?> 
                        </td>
                        <td class="column4"><?php 
							if($ClassName->class_name!=''){
								echo @$ClassName->class_name;
								}else{
								echo '<span style="color:transparent;">-</span>';}
						
						?></td>
                        <td class="column4"><?php 
							if($branchName->branch_name!=''){
								echo @$branchName->branch_name;
								}else{
								echo '<span style="color:transparent;">-</span>'; }
						
						?> </td>
                        <!--<td class="column3"><?php //echo $feeband->fee_band_description;?></td>-->
                        
                        <td class="column4 status_sec"><span class="acceptbtn statusbtn <?php if($feeband->is_active==1) { echo 'Active'; } else { echo 'Inactive'; } ?>"><?php if($feeband->is_active==1) { echo 'Active'; } else { echo 'Inactive'; } ?></span></td>
                        
                        
                         <td class="column3">
                         
                         <div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
                                                
	      <li><a href="<?=base_url();?>schoolfeeband/index/<?php echo $last_page;?>/<?php echo $feeband->fee_band_id;?>/edit<?php echo $post_url;?>"> Edit </a></li>
          
				<li>
				<?php if($feeband->is_active == '1'){ ?>
	<a href="<?php echo base_url();?>schoolfeeband/deactivatefeeband/<?php echo $last_page;?>/<?php echo $feeband->fee_band_id;?><?php echo $post_url;?>" title="click to deactivate fee band"> Active </a>
					<?php
					 } else {
					?>
	<a href="<?=base_url();?>schoolfeeband/activatefeeband/<?php echo $last_page;?>/<?php echo $feeband->fee_band_id;?><?php echo $post_url;?>" title="click to activate fee band">De-Activated </a>
				<?php } ?>
				</li>
                
			<li class="divider"></li>
				 <li><a onclick="delConfirm('<?php echo $feeband->fee_band_id;?>')" style="cursor: pointer;"> Delete </a> </li>
									</ul>
								</div>
                         
                         </td>
                                        
                                        
                        </tr>
				<?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px; height:100px; font-size:25px;background:#FFF;  color: #6a7a91;">No record to show.</th></tr>	
				   <?php } ?>

				</tbody>

		  </table>

     <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-9 col-xs-6 paginationblk">
         <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
			<?php  } ?>
	<ul class="pagination">
		
		<?php echo $pagination;?>
        

	</ul>

    </div>

    <div class="col-sm-3 col-xs-6 totaldiv nopadding">

    <div class="col-sm-6 col-xs-6 nopadding totalstudent">

    <h3>Total:&nbsp;<?php echo $Totalrec ;?> </h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>schoolfeeband/index" method="get">
       
          <input type="hidden" name="status" id="status" value="<?php echo $status_search; ?>"  />
       
          <input type="hidden" name="seachword" id="seachword" value="<?php echo $word_search; ?>"  />  
          
          <input type="hidden" name="branch" id="branch" value="<?php echo $branch_search; ?>"  />
       
          <input type="hidden" name="class" id="class" value="<?php echo $class_search; ?>"  />  

          <select class="form-control" name="perpage" id="perpage">
        
          <option value="">Select</option>
        
          <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>
        
          <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>
        
          <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>
        
          <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>
        
          <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>
        
         </select>

</form>



        </div>

    </div>

	</div>

    </div>     

</div>





	</div>

    

	</div>

    </div>

    </div>
    
        
	
 
 <script>
  jQuery(document).ready(function(){
	  
	  jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
					
	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#classfeefilterform")[0].reset();
			   window.location.href='<?php echo base_url()?>schoolfeeband/index';
			}); 
			
			
	  jQuery("#schoolbranch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			// var BranchID =  jQuery('input[name=student_school_branch]:checked').val();
			jQuery("#classname").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>schoolfeeband/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#classname').append(resp);
			          }
				   });
				 } else {
				 jQuery("#classname").find('option[value!=""]').remove();
				 return false;
				 }
			 });
			 
	 jQuery("#branch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			// var BranchID =  jQuery('input[name=student_school_branch]:checked').val();
			jQuery("#class").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>students/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#class').append(resp);
			          }
				   });
				 } else {
				jQuery("#class").find('option[value!=""]').remove();
				 return false;
				 }
			 });	 
					
  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });
	
    jQuery("#schoolfeebandForm").validate({
        rules: {
                schoolbranch: {
                 required: true,
                },
				classname: {
                required: true,
                },
			   classfee: {
                  required: true,
                 number: true,
				   maxlength: 20
               },
			   mode: {
                required: true,
                },
			 currency: {
                required: true,
                },

        },
        messages: {
              schoolbranch: {
                  required: "Please select branch."
                 },
			 classname: {
                  required: "Please select class."
                 },
			  classfee: {
                  required: "Please enter fee.",
				  number: "Digits(0-9) only please",
				  maxlength: "Maximum 20 characters allowed."
                 },
			  mode: {
                  required: "Please select mode."
                 },
		 	  currency: {
                  required: "Please select currency."
                 },

            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		 jQuery.validator.addMethod("lettersonly_number", function(value, element) {
  return this.optional(element) || /^[0-9!,%,&,@,#,$,^,*,?,_,~]+$/i.test(value);
}, "Digits(0-9) only please"); 
	});
</script>

<!--<script type="text/javascript">
function generatefee() {
   var txtFirstNumberValue = document.getElementById('mode').value;
   var txtSecondNumberValue = document.getElementById('currency').value;
   var txtThirdNumberValue = document.getElementById('classfee').value;
   var result = txtSecondNumberValue+' '+txtThirdNumberValue+' '+txtFirstNumberValue;
   if (isNaN(result)) {
      document.getElementById('mainclassfee').value = result;       
   }                        
}
</script>-->

 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>schoolfeeband/deletefeeband/"+id;
		}else{
			return false;
		}
	}
</script>	

<style>
.btn-info { 
	background-color: #37B148;
    border-color: #37B148; 
	}
.btn-info:hover {
    background-color: #37B148;
    border-color: #37B148;
    color: #fff;
}
.fullwidthinput select {
    background-position: 95% center;
}
.Inactive {
    background: #ff0000 none repeat scroll 0 0;
}
/*.cleanSearchFilter {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533 !important;
    border-radius: 3px;
    color: #fff !important;
    padding: 9px;
    text-decoration: none !important;
}*/
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px 20px 0;
}
.feeband{
font-size:14px;
}
.confirmlink.addbtn{
	float:left !important;
}
.selectoption.maindiv {
    padding-bottom: 20px;
}
#classfeedescription{
	max-height:40px;
}
.open1{
	margin:0px !important;
}
.feeband{
   padding: 6px 30px;
}
@media screen and (min-width:320px) and (max-width:480px){
.col-sm-9.col-xs-6.paginationblk {
	text-align:center;
}
.cleanSearchFilter {
    padding: 8px 5px !important;
}
.termsearch {
    padding-bottom: 10px !important;
}
.viewreport {
    padding: 0 4px;
    width: 100%;
}
.searching {
    width: 100% !important;
}
.feeband{
font-size:14px !important;
}
}
@media screen and (min-width:481px) and (max-width:767px){
.col-sm-4.inputbox.termselect {
    padding-bottom: 15px;
}
.selectoption {
    width: 100%;
	padding-bottom: 10px;
}
.selectfilter .col-sm-3.col-xs-3.inputbox.termselect {
    width: 33% !important;
}
.feeband{
font-size:14px !important;
}
}
@media screen and (min-width:768px) and (max-width:991px){
.fullwidthinput label {
	width:auto !important;
}
.col-sm-3.col-xs-3.inputbox.termselect {
    padding-bottom: 10px;
    width: 33%;
}
.rightspace a {
    font-size: 14px;
}
.totaldiv h3 {
    font-size: 14px;
    text-align: center;
}
}
@media screen and (min-width:992px) and (max-width:1199px){
.selectfilter .col-sm-3.col-xs-3.inputbox.termselect {
    width: 33%;
}
.cleanSearchFilter {
    padding: 11px 3px;
}
.applycodediv .form-group input {
    font-size: 10px;
}
.rightspace a {
    font-size: 11px;
}
.btn {
    padding: 9px 5px;
}
.searchbox {
	padding:7px;
}
.statusbtn {
    padding: 6px 10px;
}
.totaldiv h3 {
    text-align: left;
}

}
</style>	