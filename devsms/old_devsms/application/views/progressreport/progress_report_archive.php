<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li><a href="#"> Progress Reports</a></li>

        <li class="edit"><a href="#">Report Archives</a></li>        

        </ul>

        </div>

       <!-- <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>-->

        </div>
        
         <div style="clear:both"></div>
        
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>
 

        <div class="attendancesec" style="width:100%">	

        <div class="col-sm-12 profile-bg filterbox generatefilter">

        <div class="filterdiv">

<form action="<?php echo base_url();?>progressreportarchive/index" name="progressreportarchiveForm" id="progressreportarchiveForm">

    <div class="col-sm-12 col-xs-12 applycodediv nopadding">

<div class="col-sm-2 nopadding selectoption onethirdfilter">

<div class="col-sm-12 form-group fullwidthinput nopadding">

    <label class="col-sm-4 control-label nopadding">Branch:</label>

    <div class="col-sm-8 inputbox nopadding">

 <select class="form-control" name="branch" id="branch">
      <option value="">Select Branch</option>   
    <?php foreach($branches as $branch) { ?>
     <option <?php if(@$branch_search == $branch->branch_id){ echo 'selected'; } ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch_name;?></option>
    <?php  } ?>
      </select>
      

</div>
  </div>

</div>

<div class="col-sm-2 nopadding selectoption onethirdfilter">

<div class=" col-sm-12  form-group fullwidthinput nopadding class1">

    <label class="col-sm-4 control-label nopadding">Class:</label>

    <div class="col-sm-8 inputbox nopadding">

      <select class="form-control" name="class" id="class">
         <option value="">Select Class</option>
         <?php 
  if( @$_GET['branch']!='') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $schoolbranch = @$_GET['branch'];
		  $this->load->model(array('progressreportarchive_model'));
		  $sclasses =$this->progressreportarchive_model->getclasses($school_id,$schoolbranch);
     foreach($sclasses as $sclass) { ?>
          <option <?php if(@$class_search == $sclass->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>   
        </select>

    </div>

  </div>

</div>

<div class="col-sm-2 nopadding selectoption onethirdfilter">

<div class="col-sm-12 form-group fullwidthinput nopadding class2">

    <label class="col-sm-4 control-label nopadding">Term:</label>

    <div class="col-sm-8 inputbox nopadding">

<select class="form-control" name="term" id="term">
  <option value="">Select Term</option>  
  <?php 
     foreach($sterms as $sterm) { ?>
          <option <?php if(@$term_search == $sterm->term_id){ echo 'selected'; } ?> value="<?php echo $sterm->term_id;?>"><?php echo $sterm->term_name;?></option>
        <?php  } 
	    ?>     
</select>

    </div>

  </div>

</div>

				
<div class="col-sm-2 nopadding selectoption onethirdfilter">

<div class="col-sm-12 form-group fullwidthinput nopadding class3">

    <label class="col-sm-4 control-label nopadding">Year:</label>

    <div class="col-sm-8 inputbox nopadding">

 <select class="form-control" name="year" id="year">
  <option value="">Select Year</option> 
   <?php 
     foreach($syears as $syear) { ?>
          <option <?php if(@$year_search == $syear->progress_report_year){ echo 'selected'; } ?> value="<?php echo $syear->progress_report_year;?>"><?php echo $syear->progress_report_year;?></option>
        <?php  } 
	    ?>    

</select>

    </div>

  </div>

</div>


<div class="col-sm-4 rightspace selectoption viewreport topspacing tabfilterhalf">

<div class="form-group button">

    <input type="submit" class="btn btn-danger" value="View Report">

  <a href="javascript:void(0)" class="cleanSearchFilter">Clean the filter</a>

  </div>

</div>


</div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

<?php  $Totalrec = $total_rows; ?>

    <h1>Report Archives</h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">No.</th>

					  <th class="column16">Report Archive</th>

					  <th class="column2 actiondiv">Actions</th>

				  </tr>

			  </thead>

				<tbody>
                
                   <?php 
					  if(count(@$data_rows) > 0){
					      $sr=$last_page;
			 foreach($data_rows as $reports) { 
					     $sr++;
			     ?>

					<tr class="familydata reportarchives">

						<td class="column1"><?php echo $sr; ?></td>

						<td class="column16"><?php echo $reports->archive_filename; ?> </td>

<td class="column2">
<a href="<?php echo base_url();?>schooltermreport/index?branch=<?php echo $reports->branch_id;?>&class=<?php echo $reports->class_id;?>&term=<?php echo $reports->term;?>&year=<?php echo $reports->progress_report_year;?>" style="cursor: pointer;">  <i class="fa fa-eye"></i></a>
<!--<i class="glyphicon glyphicon-hdd"></i>-->
<a onclick="delConfirm(<?php echo $reports->archive_id;?>)" style="cursor: pointer;">  <i class="fa fa-trash-o"></i></a>
</td>

                        </tr>

									  <?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px; height:100px; background:#FFF;font-size:25px;color: #6a7a91;">No record found.</th></tr>	
				   <?php } ?> 

				</tbody>

		  </table>
          

<div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-8  footer">
    
    <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries
			<?php  } ?>

	<ul class="pagination">
    
<?php echo $pagination;?>

	</ul>

    </div>

    <div class="col-sm-4 totaldiv nopadding">

    <div class="col-sm-8">

    <h3>Total Report Archives:<?php echo $Totalrec ;?>  </h3>

    </div>


    <div class="col-sm-4 col-xs-5 selectfilter paginationselbox nopadding">

      <span>Showing:</span>

 <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>progressreportarchive">
 
<input type="hidden" name="branch" id="branch" value="<?php echo @$_GET['branch'];?>" />
<input type="hidden" name="class" id="class" value="<?php echo @$_GET['class'];?>" />
<input type="hidden" name="term" id="term" value="<?php echo @$_GET['term'];?>" />
<input type="hidden" name="year" id="year" value="<?php echo @$_GET['year'];?>" />

  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option  <?php if($PerPage == "20"){ echo 'selected'; } ?> value="20">20</option>

  <option  <?php if($PerPage == "30"){ echo 'selected'; } ?> value="30">30</option>

  <option  <?php if($PerPage == "40"){ echo 'selected'; } ?> value="40">40</option>

  <option  <?php if($PerPage == "50"){ echo 'selected'; } ?> value="50">50</option>

  <option  <?php if($PerPage == "100"){ echo 'selected'; } ?> value="100">100</option>

 </select>

</form>



        </div>
        
        

    </div>

	</div>

    </div>
          

          

  </div>





	</div>

    

	</div>

    </div>

    </div>
    <style>
    .footer{padding:25px;}
	

    .form-group.button {
    float: right;
   
}
.fullwidthinput select {
     width: 120% !important;
}
.class1{
	margin:0 0 0 35px;
}
.class2 {
    margin: 0 0 0 70px;
}
.class3 {
     margin: 0 0 0 100px;
    
}
.selectfilter{
	padding-right:15px;
}

   </style>
     <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>progressreportarchive/delete/"+id;
		}else{
			return false;
		}
	}
</script>
    
     <script>
  jQuery(document).ready(function(){
	  
	    jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
	  
	  	   jQuery(".cleanSearchFilter").click(function() {
	          jQuery("#progressreportarchiveForm")[0].reset();
			   window.location.href='<?php echo base_url()?>progressreportarchive/index';
			}); 
			
	jQuery("#branch").on('change', function(e){ 
   		  var schoolbranch = jQuery(this).val();
		  if(schoolbranch!='') {
			jQuery("#class").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>progressreportarchive/check_classes',
            data:{ 'schoolbranch': schoolbranch },
            success :  function(resp) {
					    jQuery('#class').append(resp);
			          }
				   });
				 } else {
				 jQuery("#class").find('option[value!=""]').remove();
				 return false;
				 }
			 });
			 
	 });
		 
</script>

