<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>



<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>

        <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>
   <li><a href="<?php echo base_url(); ?>help">Help and Knowledgebase</a></li>

        <li class="edit"> Documents</li>        

        </ul>

        </div>

   <div class="block-content">	

      <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-3">
	<div class="list-block">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#documents" aria-controls="home" role="tab" data-toggle="tab">Documents</a></li>
    </ul>
	</div>
    </div>
    <div class="col-sm-9">
      <div class="tab-content adm-dsh">
		<div role="tabpanel" class="tab-pane active" id="admin-dashboard">
			<h1 class="tab-title">What is Document?</h1>
            <div class="admn-content">
             <p><b>In this section, user can save the students documents in a particular folder.</b></p>
			<h2 class="main_heading_txt text_font">HOW TO ADD A FOLDER?</h2>
             <img src="<?php echo base_url();?>uploads/documents/document.jpg" class="stt">
             <img src="<?php echo base_url();?>uploads/documents/document-folder.jpg" class="stt">
             <ul>
             <li>On clicking add folder button,the popup will show up in which user write the name of the folder and clicked on save button.</li>
             <li>After that folder is successfully saved and will view on main screen .</li>
           	</ul>
            <p class="heading-cntnt">After single click on the folder , new page will open . </p>
            <p><b>In the next page, there is two buttons  on header:</b></p>
             <img src="<?php echo base_url();?>uploads/documents/document_file.jpg" class="stt">
			<ul class="msg-point">
					<li><b>ADD FOLDER-</b> to create a subfolder in the present folder.</li>
					<li><b>ADD FILE- </b>to upload files in the present folder. </li>
					</ul>
                    	<ul class="msg-point">
					<li>User can also upload the multiple  files by click ctrl + left click(in Windows) & command + right click (in Macbook). </li>
					<li>Clicked on save button to save the files in a folder.</li>
					</ul>
			</div>	
			</div>
	
	 <!-- Teacher_dashboard-->
      
		
			
	</div><!-- STUDENT view Report -->	
	  
	  
	  
	  </div>
    </div>
    </div>

    </div>

<style>
.tabs-left, .tabs-right {
  border-bottom: none;

  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}

.tab-content.adm-dsh {
    background: #fff;
    width: 100%;
    display: inline-block;
    padding: 10px 20px;
    border: 1px solid #eee;
}
h1.tab-title {
    margin-top: 0 !important;
    margin-left: 0 !important;
    border: none !important;
    font-weight: 900;
    font-size: 32px !important;
}
.adm-dsh p {
    margin-left: 0;
}
p.main-heading-cntnt {
    border-bottom: 1px solid #eeeeee;
    padding: 10px 0;
	margin-left:20px;
}
.admn-content {
    margin-top: -2px;
    padding-top: 10px;
    padding-left: 20px;
}
h3.para-heading {
        margin-top: 0px;
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
.admn-content p {
    margin-top: -7px;
}
img.stt {
    width: 100%;
    padding: 30px 0;
	margin-top:-10px;
}
.compse-msg {
    padding: 4px 0 0 0px;
}
h4.txt-frmt {
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
ul.msg-point {
    padding-left: 0;
    margin: 0 0 0 14px;
    line-height: 30px;
    font-size: 15px;
}
.compse-msg {
    padding: 20px 0 16px 0px;
    line-height: 28px;
}
h3.para-heading.mgn {
    margin-top: 20px;
}
.list-block {
    height: 634px;
    width: 100% !important;
    background: #fff;
}
p.spacing {
    line-height: 15px;
    margin-top: -17px;
}
.textspacing{margin-bottom:0px;}
.msg-point > ul {
    margin-bottom: 10px;
}
.editprofile-content .tab-content h1{padding: 15px 22px 0;margin-bottom:0px;}
.text_font{font-weight:bold;}
</style>