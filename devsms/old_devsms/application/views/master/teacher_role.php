<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="#">Teacher Role</a></li>        

        </ul>

        </div>

        </div>
 <!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>
           
  <div class="col-sm-12 addform nopadding"> 
  <?php if($info) { ?>
  <form name="addrole" id="addrole" method="post"  action="<?php echo base_url(); ?>teacher_role/update_teachrole/<?php echo $last_page ?>/<?php echo $info[0]['id']; ?>">
  <?php } else { ?>
  <form name="addrole" id="addrole" method="post"  action="<?php echo base_url(); ?>teacher_role/add_new_teachrole" autocomplete="off">
  <?php } ?>
  
  
  <div class="col-sm-12 datafie">
 <div class="col-sm-5 nopadding">
  <div class="col-sm-3 nopadding ">
      <label class="control-label nopadding filterlabel" style="padding-top:5px;">Teacher Role: </label>
    </div>
  
<!-- <div class="col-sm-4 inputbox termselect ">
      <select class="form-control" name="branch" id="branch">

  <option value="">Select Branch</option>  
     <?php
   			/*foreach($branch as $branchdetail)
					{
						$name = $branchdetail['branch_name'];
						$id = $branchdetail['branch_id'];
						if($id == $info[0]['branch_id'])
						{ 
						echo "<option value='$id' selected>$name</option>";	
						}
						else
						{
						echo "<option value='$id'>$name</option>";	
						} 
					}*/?>	
             
    </select>

    </div>-->
    
     <div class="col-sm-8 inputbox termselect ">
 <?php if($info) {  ?>
 		<input name="id" id="id" class="form-control" type="hidden" value="<?php echo $info[0]['id']; ?>">
      <input name="role" id="role" class="form-control" type="text" value="<?php echo $info[0]['teacher_role']; ?>">
       <?php   } else { ?>
       <input name="role" id="role" class="form-control" type="text" placeholder="Enter Teacher Role" autocomplete="off" maxlenght="61">
<?php } ?>
    </div>
   </div>
<div class="col-sm-6 nopadding">
    <div class="col-sm-2 nopadding">

        <label class="control-label nopadding filterlabel" style="padding-top:5px;">Description:</label>

    </div>
    <div class="col-sm-9 inputbox termselect">
<?php if($info) { ?>
      <textarea  placeholder="Write Description" class="form-control" name="description" id="description" style="resize:none;max-height:35px" cols="num" rows="num"><?php echo $info[0]['role_description']; ?></textarea>
 <?php } else { ?>   
      <textarea  placeholder="Write Description" class="form-control" name="description" id="description" autocomplete="off" maxlenght="251" style="resize:none;max-height:35px" cols="num" rows="num"></textarea>
<?php } ?>
    </div>
  </div>
  <div class="col-sm-1 datafie nopadding">

    <div class="col-sm-12 nopadding rbtn">
 <?php if($info) { ?>
     <input value="Update" class="btn btn-default" id="update" name="update" type="submit">
       <?php } else { ?>
      <input value="Add" class="btn btn-default" id="add" name="add" type="submit">
<?php } ?>
    </div>
  </div>
   </div>
  </form>
    </div>	

        

        <div class="attend">	

     

    <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">
    <div id="srrch">

    <h1>Role Details</h1>
     
<form action="<?php echo base_url(); ?>teacher_role/index" name="search_role" id="search_role">
<!--<select class="form-control" name="branch" id="branch" style="height: 37px;">
  <option value="">Select Branch</option>  
     <?php
   			/*foreach($branch as $branchdetail)
					{
						$name = $branchdetail['branch_name'];
						$id = $branchdetail['branch_id'];
						echo "<option value='$name'>$name</option>";	
						}*/
					?>	
             
    </select>-->
<input name="role_srch" placeholder="Search by Role" id="role_srch" class="form-control" style="width: 180px; height:36px; border-radius:0px;" type="text">
<input value="Search" class="btn btn-default" id="search" name="search" type="submit">	
</form>
</div>	
</div>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2">S.No.</th>
                      
                      <th class="column7"> Role</th>

					  <th class="column7"> School</th>

					  <th class="column4">Action</th>

                     

				  </tr>

			  </thead>

				<tbody>
<?php
if($detail){
	$sr=$last_page;
        foreach ( $detail as $teacherrole )
        {
			
            ?>
					
					<tr class="familydata staff">
						<td class="column2"><?php echo ++$sr; ?></td>
                        <td class="column7"><?php 
						if($teacherrole['teacher_role']!='')
						{
						echo $teacherrole['teacher_role'];  
						}else{
							echo '<span style="color:transparent;">-</span>';
							}
						?></td>
						<td class="column7"> 
						<?php 
						/*$branch_id = $teacherrole['branch_id'];
        $this->load->model(array('teacherrole_model'));
        $branchName = $this->teacherrole_model->getbranchName($branch_id);
		echo @$branchName->branch_name;*/ 
		
						$school_id = $teacherrole['school_id'];
						$this->load->model(array('teacherrole_model'));
						$schoolName = $this->teacherrole_model->getschoolName($school_id);
						if(@$schoolName->school_name!='')
						{
						echo @$schoolName->school_name; 
						}else{
							echo '<span style="color:transparent;">-</span>';
							}
						?></td>
                        
                        
                        
                        
                        <td class="column4">
											<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
													<li>

<a href="<?php echo base_url(); ?>teacher_role/index/<?php echo $last_page ?>/<?php echo $teacherrole['id']?>"> Edit </a>
													</li>
                                                    <li>
 <?php
$techerrole_id = $teacherrole['id'];
$this->load->model(array('teacherrole_model'));
$singlerole = $this->teacherrole_model->getteacherrole($techerrole_id);
	if($singlerole->is_active == "1") { ?>
<a href="<?php echo base_url(); ?>teacher_role/role_action/<?php echo $teacherrole['id']?>/inactive" title="click to deactivate Teacher Role"> Active </a>
<?php } else { ?>
<a href="<?php echo base_url(); ?>teacher_role/role_action/<?php echo $teacherrole['id']?>/active" title="click to activate Teacher Role">De-Activated </a>
<?php } ?>
</li>
													<li class="divider"></li>
<li> <a onclick="delConfirm('<?php echo $teacherrole['id'] ?>')" style="cursor: pointer;"> Delete </a> </li>
												</ul>
											</div>
										</td>
 <?php	}		
			} else { ?>
 <tr><th colspan="7" style="text-align: center; width:1215px;height:100px;Font-size:25px; background:#FFF;color: #6a7a91">No record to show.</th></tr>
				   <?php } ?>
				</tbody>

		  </table>



	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-9 col-xs-6 paginationblk">
    <?php 
							if(count($detail) > 0 ){
								$last_page1=$last_page;
								?>
							<span id="shortlist">	Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries </span>
								<?php
							}
							?>   
			
	<ul class="pagination">
		<?php echo $links;?>
	</ul>

    </div>

    <div class="col-sm-3 col-xs-6 totaldiv nopadding">

    <div class="col-sm-12 col-xs-6 selectfilter paginationselbox nopadding">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>teacher_role" method="get">
       
               <input type="hidden" name="role_srch" id="role_srch" value="<?php echo $role_search;?>"  />  

  <select class="form-control" name="perpage" id="perpage">

  <option value="">Select</option>

  <option   value="20">20</option>

  <option  value="30">30</option>

  <option value="40">40</option>

  <option value="50">50</option>

  <option  value="100">100</option>

 </select>

</form>

        </div>

    </div>

	</div>  

</div>

	</div>

    

	</div>

    </div>
        
 <script>
  jQuery(document).ready(function(){
	  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");
    jQuery("#addrole").validate({
        rules: {
               branch: {
                required: true
               },
			  role: {
                required: true,
				lettersonly: true,
				maxlength: 60
               }
			
        },
        messages: {
               branch: {
                  required: "Please Select Branch",
                 },
				  role: {
                  required: "Please enter Teacher Role",
				  lettersonly: "Please Enter Character Value",
				  maxlength: "your level not more than 60 characters long"
                 },
					  
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
	});
</script>
 
 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#message').delay(10000).fadeOut();
      });
	  
	   jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	}); 
	  </script>
  
 <script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>teacher_role/checkrole_staff/"+id;
		}else{
			return false;
		}
	}
</script>


 <style>
	
	#srrch{background-color:#fff; height:60px; }

.btn-info {
    background-color: rgb(55, 177, 72);
	border-color:  rgb(55, 177, 72);
	
}
#srrch #branch {
    width: 100%;
}
#srrch > h1 {
    border: 0 none;
    float: left;
    padding: 18px 0 0 20px;
}
#srrch > form {
    float: right;
	padding:13px;
	display: flex;
}
.addform{background-color:#fff; margin-bottom: 10px;  padding: 20px 10px 0px;}
.datafie {
    margin-bottom: 15px;
}
.addform select, #srrch select {
    -moz-appearance: none;
	background-image: url("<?php echo base_url();?>assets/images/dropdownicon.png"), linear-gradient(rgb(255, 255, 255), rgb(243, 245, 248)) !important;
    background-repeat: no-repeat;
    border: 1px solid rgb(223, 227, 233);
    box-shadow: none;
    height: 35px;
    width: calc(100% - 50px);
	background-position: 90% center;
    padding: 0 4px;
    width: 100%;
}
 #add.btn.btn-default {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    color: rgb(255, 255, 255);
    border: 1px solid #249533;
 	width:100%;
	border-radius:3px;
      
}

#update.btn.btn-default {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    color: rgb(255, 255, 255);
    border: 1px solid #249533;
    width:100%;
	border-radius:3px;
      
}
.pagination{margin:0;}
#type_search {
    border-bottom-left-radius: 5px !important;
    border-top-left-radius: 5px !important;
}
#search.btn.btn-default {
    max-height: 36px;
    padding: 0 30px 1px;
	background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    color: rgb(255, 255, 255);
    border: 1px solid #249533;
   	border-radius:3px;
}
#branch {margin-right:2%;}
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px 0 0;
}
.col-sm-12.paginationdiv.nopadding {
    background: #fff none repeat scroll 0 0;
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    padding: 15px;
}
#role_srch {
    border-radius: 4px !important;
    margin-right: 2%;
}

@media screen and (min-width:320px) and (max-width:480px){
.addform {
    height: auto;
    margin-bottom: 10px;
    padding: 20px 0 10px 10px;
}
#branch {
    font-size: 12px;
}
#role_srch {
    font-size: 12px;
    width: 100% !important;
}
#search.btn.btn-default {
    padding: 0 5px 1px;
}
#srrch {
    height: 185px;
    margin-bottom: 10px;
}
#srrch > form {
    display: inline-block;
    float: right;
    padding: 13px;
    width: 100%;
}
#search_role #role_srch {
    margin-top: 10px;
    width: 100% !important;
}
#search.btn.btn-default {
 padding: 3px 25px;
margin-top:8px;
}
}
@media screen and (min-width:481px) and (max-width:767px){
.headerleft {
    float: left;
}
.counter{
	margin-top:55px;
}
.datafie .inputbox.termselect {
    padding-bottom: 10px;
}
.addform {
    height: auto;
    margin-bottom: 10px;
    padding: 20px 0 10px 10px;
}
#srrch {
    height: 185px;
    margin-bottom: 10px;
}
#srrch > form {
    display: inline-block;
    float: right;
    padding: 13px;
    width: 100%;
}
#role_srch {
    border-radius: 4px !important;
    margin-right: 2%;
    margin-bottom: 10px;
}

#search_role #role_srch {
    margin-top: 10px;
    width: 100% !important;
}
#search.btn.btn-default {
 padding: 3px 25px;
}
}
@media screen and (min-width:768px) and (max-width:991px){
.datafie {
    margin-bottom: 10px;
    width: 100% !important;
}
#srrch {
    background-color: #fff;
    height: 120px;
    margin-bottom: 10px;
}
}
	</style>
	