<?php
$studentid = $studentinfo->student_id; 
$school_id	= $studentinfo->school_id;
$student_school_branch = $studentinfo->student_school_branch;
$student_class_group = $studentinfo->student_class_group;
$this->load->model(array('modifyprogressreport_model'));
$branchName = $this->modifyprogressreport_model->getbranchName($student_school_branch);

//$className = $this->modifyprogressreport_model->getclassName($student_class_group);

$totalAttendancestatus = $this->modifyprogressreport_model->get_total_Attendancestatus($studentid);
$presentAttendancestatus = $this->modifyprogressreport_model->get_present_Attendancestatus($studentid);
$absentAttendancestatus = $this->modifyprogressreport_model->get_absent_Attendancestatus($studentid);
$lateAttendancestatus = $this->modifyprogressreport_model->get_late_Attendancestatus($studentid);
?>
<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li><a href="<?php echo base_url(); ?>progressreports">Progress Reports</a></li>

        <li class="edit"><?php echo $studentinfo->student_fname.' '.$studentinfo->student_lname;?></li>        

        </ul>

        </div>

        </div>
        
        
         <div style="clear:both"></div>
         
     <div id="progressreportError" style="display:none;"><div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			 Please fill all required fields field first.
		       </div></div>
        
         <div style="clear:both"></div>
        
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>


        <div class="col-sm-12 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

        <div class="col-sm-12 nopadding accdetailheading">

        <h1>Modify Report: <?php echo $studentinfo->student_fname.' '.$studentinfo->student_lname;?></h1>

        </div>

 <form action="<?php echo base_url(); ?>modifyprogressreport/update_progressreport" name="modifyprogressreportForm" id="modifyprogressreportForm" method="post">
 
 <input type="hidden" name="student_id" id="student_id" value="<?php echo $studentinfo->student_id;?>" />
 <input type="hidden" name="school_id" id="school_id" value="<?php echo $studentinfo->school_id;?>" />
 <input type="hidden" name="branch_id" id="branch_id" value="<?php echo $studentinfo->student_school_branch;?>" />
<!-- <input type="hidden" name="class_id" id="class_id" value="<?php //echo $studentinfo->student_class_group;?>" />-->
 <input type="hidden" name="year" id="year" value="<?php echo date('Y');?>" /> 
 <input type="hidden" name="p_year" id="p_year" value="<?php echo @$p_year;?>" />

    
<div class="col-sm-12 prfltitlediv titlediv report_cls">

        <h2>Report</h2>

        </div>
        
    <div class="profile-bg report_cls">
  		<div class="form-group detailbox accdetail">
			<div class="col-sm-6 col-xs-6">
    <label class="col-sm-4 col-xs-4 control-label nopadding">Student:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo $studentinfo->student_fname.' '.$studentinfo->student_lname;?></span>

    </div>
</div>
<div class="col-sm-6 col-xs-6">
   <label class="col-sm-4 col-xs-4 control-label nopadding">Branch:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span><?php echo @$branchName->branch_name;?> </span>

    </div>
</div>
  </div>



<!--  		<div class="form-group detailbox accdetail">

 
  </div>
-->
  		<div class="form-group detailbox accdetail">
        <div class="col-sm-6 col-xs-6">

    	<label class="col-sm-4 col-xs-4 control-label nopadding">Class:</label>

    	<div class="col-sm-8 col-xs-8 inputinfo">

      	<span><?php if($studentinfo->student_class_group!='') { 
		 			  $classNames_Arr = $this->modifyprogressreport_model->getclassNameArr($studentinfo->student_class_group);
					  if( count($classNames_Arr) > 0 ) {
						  foreach($classNames_Arr as $className) {
							   echo @$className->class_name.'<br>';
							}
						  }
		          
		         } ?></span>

    	</div>
</div>
  		</div>
        
   
<!--
 		<div class="form-group detailbox accdetail">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Subject:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>Arabic Language</span>

    </div>

  </div>
-->

<div class="col-sm-12 col-xs-12 nopadding greyborder headingdiv">

        <h2>Statutory Attendance</h2>

        </div>

        <div class="form-group detailbox">
<div class="col-sm-6 col-xs-6">
    <label class="col-sm-4 col-xs-4 control-label nopadding">Present:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span>  <?php if( $presentAttendancestatus > 0 ) {		  
$present_percentage =  ($presentAttendancestatus * 100) / $totalAttendancestatus;
echo  ceil($present_percentage).'%';
} ?> </span>

    </div>
    </div>
        <div class="col-sm-6 col-xs-6">

    <label class="col-sm-4 col-xs-4 control-label nopadding"> Absent:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

    <span><?php if( $absentAttendancestatus > 0 ) {
$absent_percentage =  ($absentAttendancestatus * 100) / $totalAttendancestatus;
 echo  ceil($absent_percentage).'%';
}?></span>

    </div>
</div>
  </div>

        <div class="form-group detailbox">
 <div class="col-sm-6 col-xs-6">
    <label class="col-sm-4 col-xs-4 control-label nopadding">Late:</label>

    <div class="col-sm-8 col-xs-8 inputinfo">

      <span> <?php if( $lateAttendancestatus > 0 ) {
$late_percentage =  ($lateAttendancestatus * 100) / $totalAttendancestatus;
echo  ceil($late_percentage).'%';
}?> </span>

    </div>

  </div>
</div>
  		

<!--  		<div class="form-group detailbox">

  </div>-->

  		<!--<div class="form-group detailbox">

    <label class="col-sm-4 col-xs-4 control-label nopadding">Unauthorised Absences:</label>

    <div class="col-sm-8 col-xs-8 inputbox">

     <span>4.76%</span>

    </div>

  </div>-->
  
  
  <div class="form-group detailbox reportselbox">
   <div class="col-sm-6 col-xs-6">
     <label class="col-sm-3 control-label nopadding">Class / Group</label>
    <div class="col-sm-9 inputbox">
    
<select class="form-control" name="class_id" id="class_id" >
  <option value="">Select</option>
   <?php 
      if($studentinfo->student_class_group!='') { 
		   $classNames_Arr = $this->modifyprogressreport_model->getclassNameArr($studentinfo->student_class_group);
		   if( count($classNames_Arr) > 0 ) {
             foreach($classNames_Arr as $sclass) { 
	      ?>
          <option <?php if(in_array($sclass->class_id, explode(',',$info->student_class_group))) { echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
		    }
          }
	    ?>
</select>

    </div>
  </div>
     <div class="col-sm-6 col-xs-6">
  
    <label class="col-sm-3 control-label nopadding">Term</label>

    <div class="col-sm-9 inputbox">

<select class="form-control" name="term" id="term">
  <option value="">Select term</option>
<?php foreach($terms as $term) { ?>
 <option <?php //if($term->term_id == @$p_term_id){ echo 'selected'; } ?> value="<?php echo $term->term_id;?>"><?php echo $term->term_name;?></option>
<?php }?>
</select>

    </div>

  </div>
   </div>
  
</div>
 <div class="form-group">
 <label class="col-sm-3 control-label nopadding" ></label>  
 <div class="col-sm-9 inputbox" id="clasError" style="display:none; color:#F00;">Please select class first</div>
 </div>

<!--  <div class="form-group detailbox reportselbox">

  
  </div>-->
  
  
<div class="col-sm-12 nopadding dynamic_data_section" id="dynamicdatasection">
 </div>

 </form>

        </div>

        </div>

<!--        <div class="col-sm-6 rightspace legendsec fullwidsec">


        </div>-->

        </div>
        

<script>

  jQuery(document).ready(function(){
	  
	   jQuery("#class_id").on('change', function(e){ 
   		       // var termid = jQuery('#term').val();
			     jQuery('#clasError').hide();
				 jQuery('#term').prop('selectedIndex',0);
				 jQuery('#dynamicdatasection').hide();
			    /*if(termid!='') {
			     jQuery("#term").find('option[value!=""]').remove();
				}*/
		   });

	  jQuery("#term").on('change', function(e){ 
   		 
		    var termid = jQuery(this).val();
		    var student_id = jQuery('#student_id').val();
			var school_id = jQuery('#school_id').val();
			var branch_id = jQuery('#branch_id').val();
			var class_id = jQuery('#class_id').val();
			var year = jQuery('#year').val();
			
		  if(termid!='' && class_id!='') {
       
	    jQuery('#dynamicdatasection').show();			  
			
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>modifyprogressreport/check_term',
     		data : { 'student_id': student_id,'school_id': school_id,'branch_id': branch_id,'class_id': class_id,'termid': termid,'year': year },
            success :  function(data) {
					jQuery('#dynamicdatasection').html(data);
			          }
					  
				   });
				 } else {
				//return false;
				 jQuery('#term').prop('selectedIndex',0);
				 jQuery('#clasError').show();
				 }
		 });
		  
		    var student_id = jQuery('#student_id').val();
			var school_id = jQuery('#school_id').val();
			var branch_id = jQuery('#branch_id').val();
			var class_id = jQuery('#class_id').val();
			var termid = jQuery('#term').val();
		    var year = jQuery('#p_year').val();
   		  
		  if(student_id!='' && school_id!='' && branch_id!='' && class_id!='' && termid!='' && year!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>modifyprogressreport/get_autofill_term',
      data:{ 'student_id': student_id,'school_id': school_id,'branch_id': branch_id,'class_id': class_id,'termid': termid,'year': year },
            success :  function(data) {
					jQuery('#dynamicdatasection').html(data);
			          }
				   });
				 } else {
				 return false;
				 }

    jQuery(".sel_category").live("click", function(){

       var ab = jQuery(this).val();
    alert(ab);
});

			 
	});
	
</script>

<script>
  jQuery(document).ready(function(){
 
   var validFrom=false;
	 jQuery("#modifyprogressreportForm").validate({
		ignore: [],
        rules: {
			
			 term: {
    				required: true
   				},
					
			'effort[]': {
    					required: true
   					},
			'behaviour[]': {
    					required: true
   					},
			'homework[]': {
    					required: true
   					},
      'area_development[]':{
              maxlength: 250
            } , 
       'account_of_achievement[]':{
              maxlength: 250
            },
			'topic_covered[]': {
    		      maxlength: 250
   					}

            },
        messages: {
			
			  term: {
    						required: "Please select term",
   					},
					
			 'effort[]': {
    						required: "Required field",
   					},
			 'behaviour[]': {
    						required: "Required field",
   					},
			'homework[]': {
    						required: "Required field",
   					},
			'area_development[]': {

    						maxlength: "Maximum characters upto 250.",
   					},
      'account_of_achievement[]': {
        
                maxlength: "Maximum characters upto 250.",
            },
      'topic_covered[]': {
        
                maxlength: "Maximum characters upto 250.",
            }
					
               },
			   
	   submitHandler: submitForm
      });
	  
  	function submitForm() {
         jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>modifyprogressreport/check_progress_peport_status',
            data : jQuery("#modifyprogressreportForm").serialize(),
			dataType : "html",
           beforeSend: function()
            {
            },
                 success :  function(data) {
					//alert(data);
				      data = jQuery.parseJSON(data);
                  if(data.Status == 'true') {
					  validFrom=true;
	 				jQuery('#modifyprogressreportForm').attr('action', '<?php echo base_url(); ?>modifyprogressreport/update_progressreport');
                     document.getElementById("modifyprogressreportForm").submit();
					} else if(data.Status == 'false') {
						jQuery('#progressreportError').show();
					} 
             }
        });
        return false;
      }
			 
	});
</script>

<style>
.titlediv > h1 {
    color: #1d2531;
    font-size: 16px;
    margin: 0;
    padding: 0 15px;
}

.greyborder {
    padding-top: 15px;
	border-bottom:0px !important;
}
.report_cls{
  padding:0 0 0 15px;
}
 .aoa_selectbox , .aod_selectbox {
    margin-bottom: 10px;
}
.aoa_selectbox select {
    margin-right: 8px;
    width: 24%;
}
.aod_selectbox select {
    margin-right: 10px;
    width: 32%;
} 

.chosen-container-single .chosen-single {
    background-image: url("../images/dropdownicon.png"), linear-gradient(#ffffff, #f3f5f8);
    background-position: 96% center;
    background-repeat: no-repeat;
    border: 1px solid #dfe3e9 !important;
    border-radius: 0px !important;
    box-shadow: none !important;
    height: 35px !important;
    margin-right: 8px !important;
    width: 190px !important;
}

.chosen-container-single{
  width: 200px !important;
}
.chosen-single > span {
    padding-top: 3px;
}

.chosen-drop {
    width: 350px !important;
}
</style>