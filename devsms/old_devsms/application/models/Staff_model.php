<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Staff_model extends CI_Model {
	var $tbl	= "staff";
	var $users = "users";
	var $branch = "branch";
	var $classes = "classes";
	var $subjects = "subjects";
	var $teacher_role = "teacher_role";
	var $teacher_level = "teacher_level";
	var $teacher_type = "teacher_type";
	var $school	= "school";
	var $currency="currency";
	var $staff_access	= "staff_access";
	//var $password	= "password";

    function __construct() {
        parent::__construct();
    }
	
		function count_staffaccess($id) {
        $this->db->select('*');
        $this->db->from($this->staff_access);
		$this->db->where('user_id', $id);
        $query = $this->db->get();
        return  $query->num_rows();
    }
	
		  function insertstaffAccess_update($staffaccess){
		if($this->db->insert($this->staff_access, $staffaccess))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
	function checkstaffname($Uname) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where('username', $Uname);
        $query = $this->db->get();
        return  $query->result();
    }
	
	function checkstaffemail($email, $school_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('email'=>$email,'school_id'=>$school_id));
        $query = $this->db->get();
		return $query->num_rows();
    }
	
	function checkeditstaffemail($email, $id, $school_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('email'=>$email,'id !='=>$id,'school_id'=>$school_id));
        $query = $this->db->get();
		return $query->num_rows();
    }
	
	function insertUser($data){
		if($this->db->insert($this->users, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
	function insertStaff($data){
		if($this->db->insert($this->tbl, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
	function insertStaffAccess($data){
		if($this->db->insert($this->staff_access, $data))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}


/*function found($user_srch)
{
	$this->db->select("CONCAT_WS(' ', staff.staff_fname, staff.staff_lname) AS name");
	$this->db->from($this->tbl);
	$this->db->where('name', $user_srch);
	$query = $this->db->get();
	$rr= $this->db->last_query(); 
	echo $rr; exit();
    $result = $query->result();
    return $result;
}
*/
function found($user_srch){
	//$fullname = "CONCAT(staff.staff_fname, ' ', staff.staff_lname) as 'fullname'";
	$this->db->select ("*");
	$this->db->from($this->tbl);
	$this->db->where("CONCAT(staff_fname, ' ', staff_lname) LIKE '%".$user_srch."%'", NULL, FALSE);
	//$this->db->where("$fullname LIKE '$rr'");
	//$this->db->where('staff_lname',$user_srch);
	$query = $this->db->get();
	echo $this->db->last_query(); 
    //$result = $query->result();
    return $query->result();
	//print_r ($result);
}	

function get_records($where, $school_id, $limit=NULL, $offset=NULL,$gg,$user_srch) {
	 $this->db->limit($limit, $offset);
	 $this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
		$this->db->where(array($this->tbl.'.school_id'=>$school_id,"users.user_type="=>'teacher',"users.is_deleted="=>'0'));
		if($where!=''){
		$this->db->where($gg, null, false);
		$this->db->or_where("CONCAT(staff_fname, ' ', staff_lname) LIKE '%".$user_srch."%'", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".staff_id", "desc"); 
		 $query = $this->db->get();
		$rr= $this->db->last_query(); 
		//echo $rr; exit();
        $result = $query->result();
    return $result;
	
}

function get_single($id) {
$this->db->select('*');
$this->db->from($this->tbl);
$this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
$this->db->where('users.id', $id);
$query = $this->db->get();
$result = $query->result();
return $result;
}

function get_singlerecord($id) {
	$this->db->select('*');
$this->db->from($this->tbl);
 $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
$this->db->where('users.id', $id);
$query = $this->db->get();
return $query->row(); 
}	

function get_single_staff_access($id) {
        $this->db->select('*');
        $this->db->from($this->staff_access);
		$this->db->where(array("user_id="=>$id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
  function updatestaff($id,$staffdata){
$this->db->where('user_id', $id);
$this->db->update($this->tbl, $staffdata);
}

function updateuser($id,$userdata){
$this->db->where('id', $id);
$this->db->update($this->users, $userdata);
}

function updatestaffAccess($id,$staffaccess){
$this->db->where('user_id', $id);
$this->db->update($this->staff_access, $staffaccess);
//echo $this->db->last_query();
}

function act_inact($id,$data1){
$this->db->where('id', $id);
$this->db->update('users', $data1);
}	

function del_img($id,$data5){
$this->db->where('id', $id);
$this->db->update('users', $data5);
}

public function record_count($where, $school_id, $gg,$user_srch) {
         $this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
		$this->db->where(array($this->tbl.'.school_id'=>$school_id,"users.user_type="=>'teacher',"users.is_deleted="=>'0') );
		if($where!=''){
		$this->db->where($gg, null, false );
		$this->db->where("CONCAT(staff_fname, ' ', staff_lname) LIKE '%".$user_srch."%'", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".staff_id", "desc");
        $query = $this->db->get();
		return $query->num_rows();
    }
	
function get_branches($school_id) {
	$this->db->select('*');
	$this->db->from($this->branch);
	$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
	$query = $this->db->get();
	$this->db->last_query(); 
	return $query->result();
	}
	
function get_classes($school_id, $branch_id) {
	$this->db->select('*');
    $this->db->from($this->classes);
	$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
	$this->db->where_in('branch_id', $branch_id); 
	$query = $this->db->get();
	$this->db->last_query();
    return $query->result();
}
	
function getsubjects($school_id, $branch_id, $class_id) {
	$this->db->select('*');
    $this->db->from($this->subjects);
	$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
	$this->db->where_in('branch_id', $branch_id); 
	$this->db->where_in('class_id', $class_id); 
	$query = $this->db->get();
	$this->db->last_query(); 
    return $query->result();
}
	
/*function teacher_role($school_id, $branch_id) {
	 $this->db->select('*');
        $this->db->from($this->teacher_role);
		$this->db->where(array('school_id'=>$school_id,'branch_id'=>$branch_id,"is_deleted="=>'0', "is_active="=>'1'));
		 $query = $this->db->get();
		$this->db->last_query(); 
      return $query->result();
	}
	
	function teacher_type($school_id, $branch_id) {
	 $this->db->select('*');
        $this->db->from($this->teacher_type);
		$this->db->where(array('school_id'=>$school_id,'branch_id'=>$branch_id,"is_deleted="=>'0', "is_active="=>'1'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
         return $query->result();
	}
	
	function teacher_level($school_id, $branch_id) {
	 $this->db->select('*');
        $this->db->from($this->teacher_level);
		$this->db->where(array('school_id'=>$school_id,'branch_id'=>$branch_id,"is_deleted="=>'0', "is_active="=>'1'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
       return $query->result();
	}*/

function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getsingleClassName($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where('class_id', $class_id);
        $query = $this->db->get();
        return  $query->row();
    }

function getclassName($class) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where_in('class_id', $class);
        $query = $this->db->get();
       return $query->result();
    }

function getsubjectName($subject) {
        $this->db->select('*');
        $this->db->from($this->subjects);
        $this->db->where_in('subject_id', $subject);
        $query = $this->db->get();
       return $query->result();
    }

function getteacherrole($role) {
        $this->db->select('*');
        $this->db->from($this->teacher_role);
        $this->db->where('id', $role);
        $query = $this->db->get();
        return  $query->row();
    }
	
function getteacherlevel($level) {
        $this->db->select('*');
        $this->db->from($this->teacher_level);
        $this->db->where('id', $level);
        $query = $this->db->get();
        return  $query->row();
    }

function getteachertype($type) {
        $this->db->select('*');
        $this->db->from($this->teacher_type);
        $this->db->where('id', $type);
        $query = $this->db->get();
        return  $query->row();
    }
	
   function chk_invited_user_email($school_id,$invited_user_email) {
        $this->db->select('*');
        $this->db->from($this->users);
	$this->db->where(array('school_id'=>$school_id,'email'=>$invited_user_email));
        $query = $this->db->get();
        return  $query->result();
     }
	 
	 
	  function insertInviteduser($Inviteduserdata){
		if($this->db->insert($this->users, $Inviteduserdata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
	function insertInvitedstaff($Invitedstaffdata){
		if($this->db->insert($this->tbl, $Invitedstaffdata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	}
	
	/******************** chnages ********************************/
	function teacher_role($school_id) {
		$this->db->select('*');
        $this->db->from($this->teacher_role);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0',"is_active="=>'1'));
		$query = $this->db->get();
	//	echo $this->db->last_query(); exit();
        return $query->result();
	}
	
	function teacher_type($school_id) {
	 $this->db->select('*');
        $this->db->from($this->teacher_type);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0',"is_active="=>'1'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
         return $query->result();
	}
	
	function teacher_level($school_id) {
	 $this->db->select('*');
        $this->db->from($this->teacher_level);
		$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0',"is_active="=>'1'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
       return $query->result();
	}
		function getdatastaff( $staff_id) {
        $this->db->select('*');
        $this->db->from($this->users);
  		$this->db->where('id', $staff_id);
        $query = $this->db->get();
        return  $query->row();
	}
	
	function get_school_name($school_id) {
          $this->db->select('*');
          $this->db->from($this->school);
		  $this->db->where('school_id', $school_id);
          $query = $this->db->get();
          return  $query->row();
    }
	
	function getcurrencies() {
        $this->db->select('*');
        $this->db->from($this->currency);
		$query = $this->db->get();
        return  $query->result();
    }
	
/***************************************** EXCEL FUNCTION **************************************************/

	
	
	function getexcel_className($class_name) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where('class_id', $class_name);
        $query = $this->db->get();
       return $query->row();
    }
	
	function getexceldata($where, $school_id,$gg,$user_srch) {
		
	 	$this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
		$this->db->join('staff_access', 'staff_access.user_id = '.$this->tbl.'.user_id','LEFT' );
		$this->db->where(array($this->tbl.'.school_id'=>$school_id,"users.user_type="=>'teacher',"users.is_deleted="=>'0'));
		if($where!=''){
		$this->db->where($gg, null, false);
		$this->db->or_where("CONCAT(staff_fname, ' ', staff_lname) LIKE '%".$user_srch."%'", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".staff_id", "desc"); 
		 $query = $this->db->get();
		$rr= $this->db->last_query(); 
//	echo $rr; exit();
        $result = $query->result();
    	return $result;
	
}
		
	
}
