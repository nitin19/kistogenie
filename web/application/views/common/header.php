<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    
   
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Oliv Tree Study</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">     
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">  
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>

    <!-- top_header -->
    <section class="top_header">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-3">
            <ul class="list-inline list-unstyled list_media">
              <li><a href="#"><i class="fa fa-facebook fb" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter twit" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram inst" aria-hidden="true"></i></a></li>
            </ul>
          </div>
          <div class="col-md-8 col-sm-9">
            <ul class="list-inline list-unstyled list_login">
              <li><a href="#"><i class="fa fa-user-plus"  aria-hidden="true"></i>login</a></li>
              <li><a href="#"><i class="fa fa-mobile" aria-hidden="true"></i>Get in touch 020 89148422</a></li>
              <li>
                <div class="dropdown">
                    <div class="form-group">
                      <select class="form-control" id="sel1">
                        <option value="">enrol here</option>
                        <option value="student">student</option>
                        <option value="staff">staff</option>
                      </select>
                    </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </section>
    </section>
    <!-- end  -->

    <section class="announce_sec">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-3">
            <h2 class="announce">Announcements</h2>
          </div>
          <div class="col-md-9 col-sm-9">
             <marquee><p class="letter_txt">Dear Parents/Carers, please find the March edition of our Newsletter here. <span><a href="#"> Click Here to view OR Download. </a></span></p> </marquee>
          </div>
        </div>
      </div>
    </section>

    <header>
      <nav class="navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="brand" href="http://www.olivetreestudy.co.uk"><img src="<?php echo base_url();?>assets/img/logo.png" class="logo"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="http://www.olivetreestudy.co.uk"><i class="fa fa-home" aria-hidden="true"></i> <br> home </a></li>
             <!--  <li><a href="#"><i class="fa fa-university" aria-hidden="true"></i> <br> Weekend School</a></li> -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-university" aria-hidden="true"></i> <br> Weekend School</a></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li>
              <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> <br> Mum Dad</a></li>
              <li><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <br> Shop</a></li>
              <li><a href="#"><i class="fa fa-map" aria-hidden="true"></i> <br>Contact Us</a></li>
              <li><a href="#"><i class="fa fa-shopping-cart cart" aria-hidden="true"></i></a></li>
             
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>