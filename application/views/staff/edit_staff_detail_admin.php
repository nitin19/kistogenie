<?php
			 /* echo "<pre>";
			  print_r($branch);
			  echo"</pre>";*/
	?>		
<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>
    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

 <li><a href="<?php echo base_url(); ?>staff">Staff</a></li>

        <li><a href="<?php echo base_url(); ?>staff">Edit</a></li> 

        <li class="edit"><a href="#">Details <?php echo $staffdata->staff_fname. ' ' . $staffdata->staff_lname; ?></a></li>

        </ul>

        </div>

        </div>	
        <!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>

<div class="col-sm-12 profile-bg nopadding accsection">
 <h1>Edit Details</h1>
 </div>
<div class="col-sm-12 nopadding edit_profile">


 <form action="<?php echo base_url(); ?>staff/update_staff" method="post" name="editstaffprofileForm" id="editstaffprofileForm" enctype="multipart/form-data">
 
  <div class="col-sm-6 formleft">
<div class="editprofileform editprofile_left">
      <div class="profile-bg prfltitle profile_titlebg">

       <h2>Personal details</h2>
           
       <div class="col-sm-9 inputbox">
      <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $staffdata->user_id; ?> ">
        </div>
        <div class="col-sm-12 nopadding contentdiv">
        
  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">First Name</label>

    <div class="col-sm-9 inputbox">

      <input type="text" class="form-control" name="firstname" id="firstname" placeholder="" value="<?php echo $staffdata->staff_fname ; ?>">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Last Name</label>

    <div class="col-sm-9 inputbox">

      <input type="text" class="form-control" name="lastname" id="lastname" placeholder="" value="<?php echo $staffdata->staff_lname ; ?>">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Username<span>*</span></label>

    <div class="col-sm-9 inputbox">
 <input type="text" class="form-control" name="username" id="username" placeholder="" value="<?php echo $staffdata->username; ?>" maxlength="61">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Password<span>*</span></label>

    <div class="col-sm-9 inputbox">

       <input type="text" class="form-control" name="password" id="password" placeholder="" value="<?php echo $staffdata->password; ?>"  maxlength="16">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Avtar<br/><span class="tagline">JPG, max. 500KB</span></label>

    <div class="col-sm-9 profile-picture inputbox">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"/>
<?php 
if($staffdata->profile_image == ''){
?>
<img src="<?php echo base_url();?>assets/images/avtar.png" id="ppp">
<?php } else { ?>
      <img src="<?php echo base_url();?>uploads/staff/<?php echo $staffdata->profile_image ; ?>" id="ppp">
      <?php } ?>
<img src="" id="ppp1" >

      <div class="profilepicbtns"><i class="fa fa-picture-o"></i><span class="name" style="display:none">No file selected</span><br />
      <input type="file" name="profile_image" id="profile_image" style="display:none;"/>
      <hr class="borderline"><a href="<?php echo base_url() . "staff/delete_img/". $staffdata->user_id."/del" ;?>"><i class="fa fa-trash-o" ></i></a></div>
    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Title</label>

    <div class="col-sm-9 inputbox">

      <select class="form-control" name="title" id="title">
  <option value="">Select Title  </option>

  <option <? if($staffdata->staff_title == "ms."){ echo 'selected'; } ?> value="ms.">Ms.</option>

  <option <? if($staffdata->staff_title == "mr."){ echo 'selected'; } ?> value="mr.">Mr.</option>

  <option <? if($staffdata->staff_title == "mrs."){ echo 'selected'; } ?> value="mrs.">Mrs.</option>

</select>

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Date of Birth</label>

    <div class="col-sm-9 inputbox">

      <input type="text" name="MyDate1" id="MyDate1" class="datepicker" placeholder="DD-MM-YYYY" value=<?php if( $staffdata->staff_dob=='0000-00-00' || $staffdata->staff_dob=='1970-01-01' ) { echo ''; } else { echo date('d-m-Y', strtotime($staffdata->staff_dob)) ; } ?>>

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Email<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="email" class="form-control" name="email" id="email" placeholder="" value="<?php echo $staffdata->email ; ?>" maxlength="121">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Telephone</label>

    <div class="col-sm-9 inputbox">

     <input type="text" id="phone" name="phone" class="form-control" value="<?php echo $staffdata->staff_telephone ; ?>" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Address</label>

    <div class="col-sm-9 inputbox">

     <input type="text" id="address" name="address" class="form-control" value="<?php echo $staffdata->staff_address ; ?>">

    </div>

  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Address(Optional)</label>

    <div class="col-sm-9 inputbox">

     <input type="text" id="optionaladdress" name="optionaladdress" class="form-control" value="<?php echo $staffdata-> staff_address_1 ; ?>">

    </div>

  </div>
    
  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Qualification</label>

    <div class="col-sm-9 inputbox">
    
    <input type="text" name="quali" id="quali" class="form-control" value="<?php echo $staffdata->staff_education_qualification; ?>">

    </div>

  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Existing Branch</label>
    <div class="col-sm-9 inputbox">
    <select class="form-control" name="exist_branch" id="exist_branch">
              
 <option value="" disabled="" selected="">Select Branch </option>
   
      <?php
   			foreach($branch as $branchdetail) {
						$name = $branchdetail['branch_name'];
						$id = $branchdetail['branch_id'];
						?>
		 <option <?php if($staffdata->existing_branch == $id){ echo 'selected'; } ?> value="<?php echo $id;?>"><?php echo $name;?></option>				
					<?php } ?>	   
    </select>
    </div>
    </div>

     <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Account number</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_acc_no" id="staff_acc_no" class="form-control" value="<?php echo $staffdata->account_no; ?>" onKeyUp="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Sort code</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_sort_code" id="staff_sort_code" class="form-control" value="<?php echo $staffdata->sort_code; ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">National insurance</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_insurance" id="staff_insurance" class="form-control" value="<?php echo $staffdata->staff_insurance; ?>">
    </div>
  </div>

<!--  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Hourly rate</label>
    <div class="col-sm-9 inputbox">
    <input type="text" name="staff_hourly_rate" id="staff_hourly_rate" class="form-control" onkeypress="return isNumberKey(event)" value="<?php echo $staffdata->hourly_rate; ?>">
    </div>
  </div> -->
  

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Personal Summary</label>

    <div class="col-sm-9 inputbox">

     <textarea class="form-control" rows="2" id="persummary" name="persummary"><?php echo $staffdata->staff_personal_summery ; ?></textarea><span class="fa fa-question-circle errspan errspan1"  tool-tip-toggle="tooltip-demo" data-original-title="Enter your previous experience/s or achievements,if any." ></span>

    </div>

  </div>
    
</div>
 </div>
 </div>
 </div>
 
  <div class="col-sm-6 formright">
  
 <div class="editprofileform editprofile_right">
 
           <div class="profile-bg prfltitle profile_titlebg">

        <h2>Professional details</h2>
    <div class="col-sm-12 nopadding contentdiv">
  
  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Teacher Type</label>

    <div class="col-sm-9 inputbox">

     <select class="form-control" name="staff_teacher_type"  id="teachtype" >    
     <option value=""></option> 
   <?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = $staffdata->branch_id;
		  $this->load->model(array('staff_model'));
		  $teachertype =$this->staff_model->teacher_type($school_id, $branch_id);
     foreach($teachertype as $teachtype) { ?>
          <option <?php if($staffdata->staff_teacher_type == $teachtype->id){ echo 'selected'; } ?> value="<?php echo $teachtype->id;?>"><?php echo $teachtype->teacher_type;?></option>
        <?php  } 
          }else{
			  foreach($getteachertype as $teach_type){
	    ?>
        <option value="<?php echo $teach_type->id;?>"><?php echo $teach_type->teacher_type;?></option>
        
        <?php }}?>
       </select> 	
    </div>

  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Teacher Level</label>
    <div class="col-sm-9 inputbox">
      	<select class="form-control" name="staff_teacher_level" id="teachlevel" >     
     <option value="" ></option>
   <?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = $staffdata->branch_id;
		  $this->load->model(array('staff_model'));
		  $teacherlevel =$this->staff_model->teacher_level($school_id, $branch_id);

     foreach($teacherlevel as $teachlevel) { ?>
          <option <?php if($staffdata->staff_teacher_level == $teachlevel->id){ echo 'selected'; } ?> value="<?php echo $teachlevel->id;?>"><?php echo $teachlevel->teacher_level;?></option>
        <?php  } 
          }else{
			  	foreach($getteacherlevel as $teach_level)
		{
			  ?>
			  
			  <option value="<?php echo $teach_level->id;?>"><?php echo $teach_level->teacher_level;?></option>
			  
			  
	<?	}	  }
	    ?>
      </select>
    </div>
  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Role at the School</label>

    <div class="col-sm-9 inputbox">
    
    <select class="form-control" name="staff_teacher_role" id="teachrole">
    
    <option value=""></option>     
     <?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = $staffdata->branch_id;
		  $this->load->model(array('staff_model'));
		  $teacherrole =$this->staff_model->teacher_role($school_id, $branch_id);
     foreach($teacherrole as $teachrole) { ?>
          <option <?php if($staffdata->staff_role_at_school == $teachrole->id){ echo 'selected'; } ?> value="<?php echo $teachrole->id;?>"><?php echo $teachrole->teacher_role;?></option>
        <?php  } 
          }else{
			  foreach($getteacherrole as $teach_role){
	    ?>	
        <option value="<?php echo $teach_role->id;?>"><?php echo $teach_role->teacher_role;?></option>
        
   		<?php }  }?>		
     </select>
    </div>

  </div>
  </div>
 </div>
          
          
           <div class="profile-bg prfltitle profile_titlebg">

    <h2>Access system</h2>
    <div class="col-sm-12 nopadding contentdiv">
    
     <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Branch Name</label>
    <div class="col-sm-9 inputbox">
    <select class="form-control" name="branch[]" id="branch" multiple="multiple" style="height:80px; !important;">          
	<!--<option value=""></option> -->
      <?php
	          $branch_id_Arr = explode(',', $staffdata->branch_id);

   			foreach($branch as $branchdetail) {
						$name = $branchdetail['branch_name'];
						$id = $branchdetail['branch_id'];
						?>
		 <option <?php if(in_array($id, $branch_id_Arr)){ echo 'selected'; } ?> value="<?php echo $id;?>"><?php echo $name;?></option>				
					<?php } ?>	   
    </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>
    </div>
    </div>
    
     <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Class Name</label>
    <div class="col-sm-9 inputbox" >
<select class="form-control" name="class_name[]" id="class_name" multiple="multiple" style="height:80px; !important;">  
<!--<option value="">Select Class</option>  -->
<?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = explode(',', $staffdata->branch_id);
		  $class = explode(',', $staffdata->class_name);
		  $this->load->model(array('staff_model'));
		  $teacherclass =$this->staff_model->get_classes($school_id, $branch_id);
		  
     foreach($teacherclass as $teachclass) {
		$branchInfo = $this->staff_model->getbranchName($teachclass->branch_id); 
		  ?>
          <option <?php if(in_array($teachclass->class_id, $class)){ echo 'selected'; } ?> value="<?php echo $teachclass->class_id;?>"><?php echo $teachclass->class_name.' ('.$branchInfo->branch_name.')'; ?></option>
        <?php  } 
          }
	    ?>
 </select><span class="fa fa-question-circle errspan" tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)." ></span>    
 
    </div>
    </div>
       
     <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Subject Name</label>
    <div class="col-sm-9 inputbox" id="subject_name_old">
 <select class="form-control" name="subjectsname[]" id="subject_name" multiple="multiple" style="height:80px; !important;"> 
   	<?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = explode(',', $staffdata->branch_id);
		  $class_id  = explode(',', $staffdata->class_name);
		  $subjects  = explode(',', $staffdata->subject_name);
		  $this->load->model(array('staff_model'));
		  $teachersubject =$this->staff_model->getsubjects($school_id, $branch_id, $class_id);
		  
		 foreach($teachersubject as $getsubject) { 
		   $branchInfo = $this->staff_model->getbranchName($getsubject->branch_id);
		   $classesInfo = $this->staff_model->getsingleClassName($getsubject->class_id);
		 ?>
		 <option <?php if(in_array($getsubject->subject_id, $subjects)){ echo 'selected'; } ?> value="<?php echo $getsubject->subject_id; ?>"><?php echo $getsubject->subject_name.' ('.$classesInfo->class_name.') ('.$branchInfo->branch_name.')'; ?></option>
             <?php  
			}
        }
    ?>
    </select><span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>
    
	<?php /*?><?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = explode(',', $staffdata->branch_id);
		  $class_id  = explode(',', $staffdata->class_name);
		  $subjects  = explode(',', $staffdata->subject_name);
		  $this->load->model(array('staff_model'));
		  $teachersubject =$this->staff_model->getsubjects($school_id, $branch_id, $class_id);
		  
		 foreach($teachersubject as $getsubject) { 
		   $branchInfo = $this->staff_model->getbranchName($getsubject->branch_id);
		   $classesInfo = $this->staff_model->getsingleClassName($getsubject->class_id);
		 ?>
		<div class="checkboxdiv profilecheckbox">
					<input type="checkbox" name="subjectsname[]" id="<?php echo $getsubject->subject_id; ?>" value="<?php echo $getsubject->subject_id; ?>" <?php if(in_array($getsubject->subject_id, $subjects)){ echo 'checked'; } ?>>
    			<label for="<?php echo $getsubject->subject_id; ?>"><span class="checkbox"><?php echo $getsubject->subject_name.' ('.$classesInfo->class_name.') ('.$branchInfo->branch_name.')'; ?></span></label></div>
             <?php  
			}
        }
    ?><?php */?>
    
    </div>
  </div>
  
      <div class="col-sm-12 nopadding">
     	<div class="col-sm-6 nopadding">

     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Students</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="students_menu" id="students_menu" value="Yes" <?php if(@$staffAccessdata->students == "Yes"){ echo 'checked'; } ?> >
    			<label for="students_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Student attendance</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="student_attendance_menu" id="student_attendance_menu" value="Yes" <?php if(@$staffAccessdata->student_attendance == "Yes"){ echo 'checked'; } ?> >
    			<label for="student_attendance_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     </div>
     
      <div class="col-sm-12 nopadding">
     	<div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Documents</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="documents_menu" id="documents_menu" value="Yes" <?php if(@$staffAccessdata->documents == "Yes"){ echo 'checked'; } ?> >
    			<label for="documents_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     
     	<div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Progress reports</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="progress_reports_menu" id="progress_reports_menu" value="Yes" <?php if(@$staffAccessdata->progress_reports == "Yes"){ echo 'checked'; } ?> >
    			<label for="progress_reports_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     </div>
     
      <div class="col-sm-12 nopadding">
     	<div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Configurations</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="configurations_menu" id="configurations_menu" value="Yes" <?php if(@$staffAccessdata->configurations == "Yes"){ echo 'checked'; } ?> >
    			<label for="configurations_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     	<div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Student enrolement</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="student_enrolement_menu" id="student_enrolement_menu" value="Yes" <?php if(@$staffAccessdata->student_enrolement == "Yes"){ echo 'checked'; } ?> >
    			<label for="student_enrolement_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
</div>

      <div class="col-sm-12 nopadding">
    <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Staff </label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="staff_menu" id="staff_menu" value="Yes" <?php if(@$staffAccessdata->staff == "Yes"){ echo 'checked'; } ?> >
    			<label for="staff_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     
     	<div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Staff enrolement</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="staff_enrolement_menu" id="staff_enrolement_menu" value="Yes" <?php if(@$staffAccessdata->staff_enrolement == "Yes"){ echo 'checked'; } ?> >
    			<label for="staff_enrolement_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
   
     	
   </div>
   
      <div class="col-sm-12 nopadding">
       
       <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Staff attendance</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="staff_attendence_menu" id="staff_attendence_menu" value="Yes" <?php if(@$staffAccessdata->staff_attendence == "Yes"){ echo 'checked'; } ?> >
    			<label for="staff_attendence_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
   </div>
   
     	<div class="col-sm-6 nopadding">  
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Staff salary</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="staff_salary_menu" id="staff_salary_menu" value="Yes" <?php if(@$staffAccessdata->staff_salary == "Yes"){ echo 'checked'; } ?> >
    			<label for="staff_salary_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
        
     	
     </div>
     
      <div class="col-sm-12 nopadding">
      <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Teaching & learning</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="teaching_learing_menu" id="teaching_learing_menu" value="Yes" <?php if(@$staffAccessdata->teaching_learing == "Yes"){ echo 'checked'; } ?> >
    			<label for="teaching_learing_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
     <div class="col-sm-6 nopadding">
     <div class="form-group">
    <label class="col-sm-8 control-label nopadding">Basic Setup</label>
    <div class="col-sm-4 inputbox">
		  <div class="checkboxdiv profilecheckbox profilecheckbox1">
			    <input type="checkbox" name="basic_masters_menu" id="basic_masters_menu" value="Yes" <?php if(@$staffAccessdata->basic_masters == "Yes"){ echo 'checked'; } ?> >
    			<label for="basic_masters_menu"><span class="checkbox"></span></label>
                </div>
        </div>
     </div>
     </div>
  </div>
     
  
  </div>
 </div>
 
 
           <div class="profile-bg prfltitle profile_titlebg">
        <h2>Salary</h2>
		<div class="col-sm-12 nopadding contentdiv">
  
      <div class="form-group">
    <label class="col-sm-2 control-label nopadding">Salary</label>
    <div class="col-sm-10 inputbox">
         <div class="col-sm-5 inputbox">
      <select class="form-control" name="salary_currency"  id="salary_currency">  
       <option value="">Select currency</option>   
      <?php  foreach($currencies as $currency) { ?> 
       <option <?php if($currency->symbol == $staffdata->salary_currency){ echo 'selected'; } ?> value="<?php echo $currency->symbol;?>"><?php echo $currency->symbol;?></option>  
         <?php } ?>
        </select>
        </div>
        
         <div class="col-sm-3 inputbox">
    <input type="text" name="salary_amount" id="salary_amount" value="<?php echo $staffdata->salary_amount;?>" placeholder="Salary" class="form-control" maxlength="10">
        </div>
        
         <div class="col-sm-4 inputbox">
      <select class="form-control" name="salary_mode"  id="salary_mode"> 
        <option value="">Select Mode</option>  
         <option <?php if($staffdata->salary_mode == "Hourly"){ echo 'selected'; } ?> value="Hourly">Hourly</option> 
		<option <?php if($staffdata->salary_mode == "Weekly"){ echo 'selected'; } ?> value="Weekly">Weekly</option>
        <option <?php if($staffdata->salary_mode == "Monthly"){ echo 'selected'; } ?> value="Monthly">Monthly</option>

    <!--    <option <?php if($staffdata->salary_mode == "Half-Yearly"){ echo 'selected'; } ?> value="Half-Yearly">Half-Yearly</option>
        <option <?php if($staffdata->salary_mode == "Yearly"){ echo 'selected'; } ?> value="Yearly">Yearly</option>-->        
        </select>
        </div>
   
    
       </div>
  </div>
  
      <div class="form-group">
 <?php 
  $weekdaysArr = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat"); 
  $teaching_days_Arr  = explode(',', $staffdata->teaching_days);
 ?>
    <label class="col-sm-3 control-label nopadding">Teaching Days</label>
    <div class="col-sm-9 inputbox">
         <?php foreach($weekdaysArr as $weekday) { ?>
		  <div class="checkboxdiv profilecheckbox checkbox_cls">
			    <input type="checkbox" name="teaching_days[]" id="week_day_<?php echo $weekday;?>" value="<?php echo $weekday;?>" <?php if(in_array($weekday, $teaching_days_Arr)){ echo 'checked'; } ?> >
    			<label for="week_day_<?php echo $weekday;?>"><span class="checkbox cls_day"><?php echo $weekday;?></span></label>
                </div>
           <?php } ?>     
        </div>
     </div>
  
  </div>
  </div>
  
 
  <?php /*?><div class="profile-bg prfltitle profile_titlebg">

        <h2>Professional details</h2>
    <div class="col-sm-12 nopadding contentdiv">
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Branch Name</label>
    <div class="col-sm-9 inputbox">
    <select class="form-control" name="branch[]" id="branch" multiple="multiple" required style="height:80px; !important;">          
	<!--<option value=""></option> -->
      <?php
	          $branch_id_Arr = explode(',', $staffdata->branch_id);
   			foreach($branch as $branchdetail) {
						$name = $branchdetail['branch_name'];
						$id = $branchdetail['branch_id'];
						?>
		 <option <?php if(in_array($id, $branch_id_Arr)){ echo 'selected'; } ?> value="<?php echo $id;?>"><?php echo $name;?></option>				
					<?php } ?>	   
    </select>
    </div>
    </div>
    
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Class Name</label>
    <div class="col-sm-9 inputbox" >
<select class="form-control" name="class_name[]" id="class_name" multiple="multiple" required style="height:80px; !important;">  
<!--<option value="">Select Class</option>  -->
<?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = explode(',', $staffdata->branch_id);
		  $class = explode(',', $staffdata->class_name);
		  $this->load->model(array('staff_model'));
		  $teacherclass =$this->staff_model->get_classes($school_id, $branch_id);
		  
     foreach($teacherclass as $teachclass) {
		$branchInfo = $this->staff_model->getbranchName($teachclass->branch_id); 
		  ?>
          <option <?php if(in_array($teachclass->class_id, $class)){ echo 'selected'; } ?> value="<?php echo $teachclass->class_id;?>"><?php echo $teachclass->class_name.' ('.$branchInfo->branch_name.')'; ?></option>
        <?php  } 
          }
	    ?>
 </select><span class="fa fa-question-circle errspan" tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)." ></span>    
 
    </div>
    </div>
    
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Subject Name</label>
    <div class="col-sm-9 inputbox" id="subject_name">
	<?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = explode(',', $staffdata->branch_id);
		  $class_id  = explode(',', $staffdata->class_name);
		  $subjects  = explode(',', $staffdata->subject_name);
		  $this->load->model(array('staff_model'));
		  $teachersubject =$this->staff_model->getsubjects($school_id, $branch_id, $class_id);
		  
		 foreach($teachersubject as $getsubject) { 
		   $branchInfo = $this->staff_model->getbranchName($getsubject->branch_id);
		   $classesInfo = $this->staff_model->getsingleClassName($getsubject->class_id);
		 ?>
		<div class="checkboxdiv profilecheckbox">
					<input type="checkbox" name="subjectsname[]" id="<?php echo $getsubject->subject_id; ?>" value="<?php echo $getsubject->subject_id; ?>" <?php if(in_array($getsubject->subject_id, $subjects)){ echo 'checked'; } ?>>
    			<label for="<?php echo $getsubject->subject_id; ?>"><span class="checkbox"><?php echo $getsubject->subject_name.' ('.$classesInfo->class_name.') ('.$branchInfo->branch_name.')'; ?></span></label></div>
             <?php  
			}
        }
    ?>
    </div>
  </div>
  
  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Teacher Type</label>

    <div class="col-sm-9 inputbox">

     <select class="form-control" name="staff_teacher_type"  id="teachtype" required >    
     <option value=""></option> 
   <?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = $staffdata->branch_id;
		  $this->load->model(array('staff_model'));
		  $teachertype =$this->staff_model->teacher_type($school_id, $branch_id);
     foreach($teachertype as $teachtype) { ?>
          <option <?php if($staffdata->staff_teacher_type == $teachtype->id){ echo 'selected'; } ?> value="<?php echo $teachtype->id;?>"><?php echo $teachtype->teacher_type;?></option>
        <?php  } 
          }else{
			  foreach($getteachertype as $teach_type){
	    ?>
        <option value="<?php echo $teach_type->id;?>"><?php echo $teach_type->teacher_type;?></option>
        
        <?php }}?>
       </select> 	
    </div>

  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Teacher Level</label>
    <div class="col-sm-9 inputbox">
      	<select class="form-control" name="staff_teacher_level" id="teachlevel" required >     
     <option value="" ></option>
   <?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = $staffdata->branch_id;
		  $this->load->model(array('staff_model'));
		  $teacherlevel =$this->staff_model->teacher_level($school_id, $branch_id);

     foreach($teacherlevel as $teachlevel) { ?>
          <option <?php if($staffdata->staff_teacher_level == $teachlevel->id){ echo 'selected'; } ?> value="<?php echo $teachlevel->id;?>"><?php echo $teachlevel->teacher_level;?></option>
        <?php  } 
          }else{
			  	foreach($getteacherlevel as $teach_level)
		{
			  ?>
			  
			  <option value="<?php echo $teach_level->id;?>"><?php echo $teach_level->teacher_level;?></option>
			  
			  
	<?	}	  }
	    ?>
      </select>
    </div>
  </div>

  <div class="form-group">

    <label class="col-sm-3 control-label nopadding">Role at the School</label>

    <div class="col-sm-9 inputbox">
    
    <select class="form-control" name="staff_teacher_role" id="teachrole" required >
    
    <option value=""></option>     
     <?php 
  if( $staffdata->branch_id!='' && $staffdata->branch_id!='0') {
		  $school_id = $this->session->userdata('user_school_id');
		  $branch_id = $staffdata->branch_id;
		  $this->load->model(array('staff_model'));
		  $teacherrole =$this->staff_model->teacher_role($school_id, $branch_id);
     foreach($teacherrole as $teachrole) { ?>
          <option <?php if($staffdata->staff_role_at_school == $teachrole->id){ echo 'selected'; } ?> value="<?php echo $teachrole->id;?>"><?php echo $teachrole->teacher_role;?></option>
        <?php  } 
          }else{
			  foreach($getteacherrole as $teach_role){
	    ?>	
        <option value="<?php echo $teach_role->id;?>"><?php echo $teach_role->teacher_role;?></option>
        
   		<?php }  }?>		
     </select>
    </div>

  </div>
</div>
 </div><?php */?>
 
 
 </div>
         <div class="col-sm-6 nopadding">
        <div class="cancellink">
        </div>
        </div>
        <div class="col-sm-6 nopadding">
        <div class="col-sm-12 confirmlink link">
 <input type="submit" class="btn btn-default" id="updateBtn" name="update" value="Update Staff Detail">
        </div>
        </div>
 </div>
 
        
        <!--<div class="col-sm-12 cancelconfirm updatestaffbtn">

        <div class="col-sm-12 nopadding">

        <div class="confirmlink">
 <input type="submit" class="btn btn-default" id="updateBtn" name="update" value="Update Staff Detail">
        </div>

        </div>

        </div>-->

		</form>


    </div>    
    </div>

        
        <script>
  jQuery(document).ready(function(){
	  
	  $("#MyDate1").keydown(false);
	  	  
   var v = jQuery("#editstaffprofileForm").validate({
		ignore: [],
        rules: {
              username: {
                required: true,
				maxlength: 60
               },
			  password: {
                required: true,
				minlength: 5,
         		maxlength: 15
               },
			  email: {
                required: true,
		  		email: true,
		  		maxlength: 120,
				remote:'<?php echo base_url(); ?>staff/edit_check_email/<?php echo $staffdata->user_id; ?>'
               },
			   firstname: {
				  alphaLetter: true,
				  maxlength: 60
                 },
			  lastname: {
				   alphaLetter: true,
				   maxlength: 60

                 },
			   phone: {
				 number: true,
                 minlength: 11
               }
			/*  'branch[]':{
                required: true
                },
			 'class_name[]':{
                required: true
               },
			  staff_teacher_type: {
                required: true
               },
			   staff_teacher_level: {
                required: true
               },
			   staff_teacher_role: {
                required: true
               },
		    salary_currency: {
                required: true
               },
			 salary_amount: {
                required: true,
				number: true,
				maxlength: 9
               }, 
			 salary_mode: {
                required: true
               }*/ 
			  
          },
        messages: {
                username: {
                  required: "Please enter username",
				  maxlength: "Maximum 60 characters allowed."
                 },
				password: {
                  required: "Please enter password",
				  minlength: "Minimum 5 characters required.",
                  maxlength: "Maximum 15 characters allowed."
                 },
				email: {
                  required: "Please enter email address.",
				  email: "Please enter a valid email address.",
				  maxlength: "Maximum 120 characters allowed.",
				  remote: "Email already in use."
                 },
				  firstname: {
				   alphaLetter: "Letters only please",
				   maxlength: "Maximum 60 characters allowed."
                 },
			   lastname: {
				   alphaLetter: "Letters only please",
				   maxlength: "Maximum 60 characters allowed."
                 },	
				  phone: {
             		 minlength: "Enter 11 digit number ",
					 Numericonly: "enter only numeric"
                 }			 
				/*'branch[]': {
                  required: "Please select branch."
                 },
				'class_name[]': {
                  required: "Please select class."
                 },
				staff_teacher_type: {
                  required: "Please select teacher type."
                 },
				staff_teacher_level: {
                  required: "Please select teacher level."
                 },
				staff_teacher_role: {
                  required: "Please select teacher role."
                 },
				salary_currency:  {
                  required: "Please select currency"
                  },
				salary_amount:  {
                   required: "Please enter salary",
				   number: "Numbers only please.",
				   maxlength: "Maximum 9 characters allowed."
                  },
			   salary_mode:  {
                  required: "Please select salary mode"
                  }*/
			  
            },
       
        /*submitHandler: function(form) {
            form.submit();
          }*/
        });
	  
	  
	jQuery("#updateBtn").click(function() {
      if (v.form()) {
		  jQuery( "#editstaffprofileForm" ).submit();
          return false;
       }
     });
	  
	  
	  /*jQuery("#branch").on('change', function(e){ */
	  jQuery("#branch").on('click', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/getclass',
            data:{ 'branch_id': branch_id },
            success :  function(response) {
				        response=jQuery.parseJSON(response);
						//jQuery('.profilecheckbox').empty();
						jQuery('#class_name').empty();
					    jQuery('#class_name').html(response.datacls);
						jQuery('#subject_name').empty();
					    jQuery('#subject_name').html(response.datasub);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
	 jQuery("#class_name").on('click', function(e){ 
   		   var class_id = jQuery(this).val();
		   var branch_id = jQuery('#branch').val();
		  if(branch_id!='' && class_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/get_subject',
            data:{ 'branch_id': branch_id, 'class_id':class_id},
            success :  function(resp) {
						//jQuery('.profilecheckbox').empty();
						jQuery('#subject_name').empty();
					    jQuery('#subject_name').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
	 /*  jQuery("#branch").on('change', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/get_teacher_role',
            data:{ 'branch_id': branch_id },
            success :  function(resp) {
						jQuery('#teachrole').empty();
					    jQuery('#teachrole').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
		jQuery("#branch").on('change', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/get_teacher_level',
            data:{ 'branch_id': branch_id },
            success :  function(resp) {
						jQuery('#teachlevel').empty();
					    jQuery('#teachlevel').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
		jQuery("#branch").on('change', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>staff/get_teacher_type',
            data:{ 'branch_id': branch_id },
            success :  function(resp) {
						jQuery('#teachtype').empty();
					    jQuery('#teachtype').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 */

	});
</script>
         <script>
          function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57)) {
             return false;
      } else {
          return true;
      }
       }

	$(".fa-picture-o").click(function () {
  $("input[type='file']").trigger('click');
  $("#ppp").hide();
  $("#ppp1").show();
});

 $("#ppp1").hide();
 
$('input[type="file"]').on('change', function() {
  var val = $(this).val();
  $(this).siblings('span').text(val);
})

$('input[type="file"]').on('change', function() {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#ppp1").css("background-image", "url("+this.result+")");
            }
        }
        });
		</script> 
        <script type="text/javascript">
 
$(document).ready(function(){
 
    $('[tool-tip-toggle="tooltip-demo"]').tooltip({
 
        placement : 'top'
 
    });
 
});
 
</script> 
  <style>
#ppp,#ppp1 {
   
    background-position: center center;
    background-size: cover;
    -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
    display: inline-block;
}
#cancel{
	background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
	border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
    float: left;
    font-size: 14px;
    margin: 20px 0;
    padding: 6px 20px;

}
.editprofileform {
    border-radius: 4px;
    float: left;
    margin-right: 2%;
    width: 48%;
}
.fa-question-circle + .tooltip > .tooltip-inner {background-color:#090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.errspan {
    float: right;
    margin-right: -13px;
    margin-top: -80px;
    position: relative;
}

.errspan1{
	margin-top:-305px;
}
.padd {
    max-height: 722px;
    min-height: 722px;
}
.updatestaffbtn {
    width: 100%;
}
.updatestaffbtn .confirmlink{padding-right:10px;}
.editprofile_left{width:100%;}
.editprofile_right{width:100%;}
.formleft{padding-left:0px;}
.formright{padding-right:0px;}
.fa.fa-question-circle.errspan{color:#090;}

.profile_titlebg h2{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1); 
	padding: 10px 15px;
	margin:0px; }
.profiletitltbox{padding-top:0px;}
.profiletitltbox .profile_titlebg {
    padding: 0;
}
.contentdiv{
	padding-left:15px;
	padding-top:15px;

}
.profile-bg {
	padding: 0px;
}
.accsection h1 {
    padding: 0 0 10px 15px;
	color:#393C3E;
}
</style>
<style>
.profilecheckbox .checkbox.cls_day::before{
	left:-20px;
}
.profilecheckbox .checkbox.cls_day::after{
	left:-20px;
}
.checkboxdiv.checkbox_cls {
    padding: 0 0 0 18px;
}
.link input{
    font-size: 19px;
    margin: 0;
	width: 100%;
}

</style>  
   
       
