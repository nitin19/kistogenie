
<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

        <li class="edit">Subjects</li>        

        </ul>

        </div>

        </div>
<!---------error message ------------------>
        <div style="clear:both"></div>
         <?php if($this->session->flashdata('error')): ?>  
       <div class="alert alert-danger alert-dismissable" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
        <?php endif; ?>
<!---------error message end------------------>
<!---------success message ------------------>
         <?php if($this->session->flashdata('success')): ?>
      <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>         
<!---------success message end------------------>
           
  <div class="col-sm-12 addform nopadding"> 
 <?php 
		$subjectid = $this->uri->segment(4); 
		$Action = $this->uri->segment(5); 
		if($Action == 'edit') {
		?>
          <form name="addsubject" id="addsubject" method="post"  action="<?php echo base_url();?>subjects/update_subject/<?php echo $last_page ?>/<?php echo $info->subject_id; ?>/edit">  
  <div class="col-sm-9 datafie">
  <div class="col-sm-3 ">
      <label class="control-label nopadding filterlabel">Subjects: </label>
    </div>  
 <div class="col-sm-3 ">
      <select class="form-control" name="branch" id="branch">
  <option value="">Select Branch</option>  
     <?php
   			foreach($branch as $branchdetail)
					{
						$name = $branchdetail['branch_name'];
						$id = $branchdetail['branch_id'];
						if($id == $info->branch_id)
						{ 
						echo "<option value='$id' selected>$name</option>";	
						}
						else
						{
						echo "<option value='$id'>$name</option>";	
						} 
					}?>	          
    </select>
    </div>    
     <div class="col-sm-3 ">
      <select class="form-control" name="subclass" id="subclass">  
         <option value="">Select Class</option>
          <?php 
  if( $info->branch_id != '') {
		  $school_id		= $this->session->userdata('user_school_id');
		  $branch_id = $info->branch_id;
		  $this->load->model(array('subjects_model'));
		  $sclasses =$this->subjects_model->get_classes($branch_id, $school_id);
     foreach($sclasses as $sclass) { ?>
          <option <?php if($sclass->class_id == $info->class_id){ echo 'selected'; } ?> value="<?php echo $sclass->class_id;?>"><?php echo $sclass->class_name;?></option>
        <?php  } 
          }
	    ?>            
    </select>
    </div>   
     <div class="col-sm-3 ">
      <input name="subject_id" id="subject_id" class="form-control" type="hidden"  value="<?php echo $info->subject_id; ?>">
       <input name="subject" id="subject" class="form-control" type="text"  value="<?php echo $info->subject_name; ?>">
    </div>
    </div>
     <div class="col-sm-9 datafie">
    <div class="col-sm-3 ">
       <label class="control-label nopadding filterlabel">Description:</label>
    </div>
    <div class="col-sm-9"> 
      <textarea  placeholder="Write Description" class="form-control" name="description" id="description" style="resize:none" cols="num" rows="num"><?php echo $info->subject_description; ?></textarea>
    </div>
  </div>
  <div class="col-sm-9 datafie">
     <div class="col-sm-12 rbtn">
      <input value="Update" class="btn btn-default" id="add" name="add" type="submit">
    </div>
  </div>
  </form>
  <?php } else { ?>
  <form name="addsubject" id="addsubject" method="post"  action="<?php echo base_url();?>subjects/add_new_subject/">  
  <div class="col-sm-9 datafie">
  <div class="col-sm-3 ">
      <label class="control-label nopadding filterlabel">Subjects: </label>
    </div>  
 <div class="col-sm-3 ">
      <select class="form-control" name="branch" id="branch">
  <option value="">Select Branch</option>  
     <?php
   			foreach($branch as $branchdetail)
					{
						$name = $branchdetail['branch_name'];
						$id = $branchdetail['branch_id'];
						echo "<option value='$id'>$name</option>";	
					}
					?>	           
    </select>
    </div>    
     <div class="col-sm-3 ">
      <select class="form-control" name="subclass" id="subclass">  
         <option value="">Select Class</option>            
    </select>
    </div>   
     <div class="col-sm-3 ">
       <input name="subject" id="subject" class="form-control" type="text" placeholder="Enter Subject Name" autocomplete="off" maxlenght="61">
    </div>
    </div>
     <div class="col-sm-9 datafie">
    <div class="col-sm-3 ">
       <label class="control-label nopadding filterlabel">Description:</label>
    </div>
    <div class="col-sm-9"> 
      <textarea  placeholder="Write Description" class="form-control" name="description" id="description" autocomplete="off" maxlenght="251" style="resize:none" cols="num" rows="num"></textarea>
    </div>
  </div>
  <div class="col-sm-9 datafie">
       <div class="col-sm-12 rbtn">
      <input value="Add" class="btn btn-default" id="add" name="add" type="submit">
    </div>
  </div>
  </form>
<?php } ?>  
    </div>	        
        <div class="attend">	    
    <div class="col-sm-12 tablediv nopadding">
    <div class="col-sm-12 nopadding">
    <div id="srrch">
    <h1>Subject Details</h1>     
<form action="" name="search_subject" id="search_subject">
<select class="form-control" name="srch_branch" id="srch_branch" >
  <option value="">Select Branch</option>  
     <?php
   			foreach($branch as $branchdetail)
					{
						
						$name = $branchdetail['branch_name'];
						$id = $branchdetail['branch_id'];
						echo "<option value='$id'>$name</option>";	
						}
					?>	
             
    </select>
    <select class="form-control" name="srch_class" id="srch_class">  
     <option value="">Select Class</option>           
    </select>
<input name="srch_subject" id="srch_subject" class="form-control" style="width: 180px; height:36px; border-radius:3px;margin:2%;" type="text" placeholder="Search by Subject"  >
<input value="Search" class="btn btn-default" id="search" name="search" type="submit">	
</form>
</div>	
</div>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2">S. No.</th>
                      
                      <th class="column5">Subject</th>
                      
                      <th class="column4"> Class</th>
                      
					  <th class="column6"> School Branch</th>

					  <th class="column3">Action</th>

                     

				  </tr>

			  </thead>

				<tbody>
<?php
if($detail){
	$sr=0;
        foreach ( $detail as $subject )
        {
			
            ?>					
					<tr class="familydata staff">
						<td class="column2"><?php echo ++$sr; ?></td>
                        <td class="column5"><?php 
							if($subject['subject_name']!='')
							{ echo $subject['subject_name'];
								}else{
									echo '<span style="color:transparent;">-</span>';
									}
						
						?></td>
                        <td class="column4"><?php 
						$class_id = $subject['class_id'];
						$this->load->model(array('subjects_model'));
						$className = $this->subjects_model->getclassName($class_id);
						if(@$className->class_name!=''){ 
							echo @$className->class_name; 
						}else{
							echo '<span style="color:transparent;">-</span>';
							}
						?></td>
						<td class="column6"> <?php 
						$branch_id = $subject['branch_id'];
						$this->load->model(array('subjects_model'));
						$branchName = $this->subjects_model->getbranchName($branch_id);
						if(@$branchName->branch_name!='')
						{
						echo @$branchName->branch_name; 
						}else{
							echo '<span style="color:transparent;">-</span>';
							}
						?></td>
                     <td class="column3">
											<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
													<li>

<a href="<?php echo base_url(); ?>subjects/index/<?php echo $last_page ?>/<?php echo $subject['subject_id']?>/edit"> Edit </a>
													</li>
                                                    <li>
 <?php 
$subject_id = $subject['subject_id'];
 $this->load->model(array('subjects_model'));
$singlesubject = $this->subjects_model->getsubject($subject_id);
 $singlesubject->is_active; 
 		if($singlesubject->is_active == "1") {  ?>                 
<a href="<?php echo base_url(); ?>subjects/subject_action/<?php echo $subject['subject_id']?>/inactive" title="click to deactivate Subject"> Active </a>
<?php } else { ?>
<a href="<?php echo base_url(); ?>subjects/subject_action/<?php echo $subject['subject_id']?>/active" title="click to activate Subject">De-Activated </a>
<?php } ?>
</li>
													<li class="divider"></li>
<li> <a onclick="delConfirm('<?php echo $subject['subject_id']?>')" style="cursor: pointer;"> Delete </a> </li>
												</ul>
											</div>
                    </td>
                        </tr>
  <?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px;height:100px; background:#FFF;  color: #6a7a91; font-size:25px;">No record to show.</th></tr>	
                    <?php } ?>
                  </tbody>
		  </table>
<div class="profile-bg">
	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-9 col-xs-6 paginationblk">
  <? 
							if($detail){
								$last_page1=$last_page;
								?>
							<span id="shortlist">	Showing <?=++$last_page;?> to <?=$sr++;?> of <?=$total_rows++;?> entries  </span>
								<?
							}
							?>   
			
	<ul class="pagination">
		<?php echo $links;?>
	</ul>

    </div>

    <div class="col-sm-3 col-xs-6 totaldiv nopadding">

    <div class="col-sm-12 col-xs-6 selectfilter paginationselbox ">

                <span>Showing:</span>

       <form name="perPageForm" id="perPageForm" action="<?php echo base_url();?>subjects" method="get">
       
          <input type="hidden" name="srch_branch" id="seachword" value="<?php echo $branch_search; ?>"  />  
          
          <input type="hidden" name="srch_class" id="srch_class" value="<?php echo $class_search; ?>"  />
       
          <input type="hidden" name="srch_subject" id="srch_subject" value="<?php echo $subject_search; ?>"  />  

          <select class="form-control" name="perpage" id="perpage">
        
          <option value="">Select</option>
        
          <option   value="20">20</option>
        
          <option  value="30">30</option>
        
          <option value="40">40</option>
        
          <option value="50">50</option>
        
          <option  value="100">100</option>
        
         </select>

</form>

        </div>

    </div>

	</div>  

</div>

	</div>

  </div>  

	</div>

    </div>
        
 <script>
  jQuery(document).ready(function(){
	  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");
    jQuery("#addsubject").validate({
        rules: {
               branch: {
                required: true
               },
			  subclass: {
                required: true
               },
			   subject: {
                required: true,
				lettersonly: true,
				maxlength: 30
               },
			
        },
        messages: {
               branch: {
                  required: "Please Select Branch",
                 },
				  subclass: {
                  required: "Please Select Class",
                 },
				 subject: {
                  required: "Please Enter Subject Name",
				  lettersonly: "Please Enter Character Value",
				  maxlength: "your level not more than 30 characters long"
                 },
			  
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
	});
</script>
<script>
  jQuery(document).ready(function(){
jQuery("#branch").on('change', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if(branch_id!='') {
			jQuery("#subclass").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>subjects/get_class',
            data:{ 'branch_id': branch_id },
            success :  function(resp) {
					    jQuery('#subclass').append(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
 
     
          $("#srch_branch").change(function(){
 jQuery("#srch_class").find('option[value!=""]').remove();
    $.ajax({
        url: "<?php echo base_url();?>subjects/get_class",
        data: {branch_id : $(this).val()},
        type: "post",
        success: function(msg){
			jQuery('#srch_class').append(msg);
			/*$('#srch_class').empty();
			$(msg).appendTo('#srch_class');*/
		}
    });
});
});

jQuery("#perpage").on('change', function(e){ 
			          jQuery('#perPageForm').submit();
			    	});
     </script>
  
<script type="text/javascript">
	function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>subjects/checkstaff_subject/"+id;
		}else{
			return false;
		}
	}
</script>

 <style>
	
	#srrch{background-color:#fff; height:60px; }

.btn-info {
    background-color: rgb(55, 177, 72);
	border-color:  rgb(55, 177, 72);
	
}
#srrch > h1 {
    border: 0 none;
    float: left;
    padding: 18px 0 0 20px;
}
#srrch > form {
    float: right;
	padding:13px;
	display: flex;
	width:70%;
}
.addform{height:200px; background-color:#fff; margin-bottom: 10px;  padding: 20px 0 0 10px;}
.datafie {
    margin-bottom: 15px;
}
.addform select, #srrch select {
    -moz-appearance: none;
    background-image: url("<?php echo base_url();?>assets/images/dropdownicon.png"), linear-gradient(rgb(255, 255, 255), rgb(243, 245, 248));
    background-repeat: no-repeat;
    border: 1px solid rgb(223, 227, 233);
    box-shadow: none;
    height: 35px;
    width: calc(100% - 50px);
	background-position: 90% center;
    padding: 0 4px;
    width: 100%;
}
 .btn.btn-default{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    color: rgb(255, 255, 255);
    border: 1px solid #249533;
    padding: 5px 20px;
	border-radius:3px;
}
.pagination{padding-left:40%;}
.col-sm-9.col-xs-6.paginationblk {
    padding: 25px;
}
.rbtn {
  text-align:right;
}
#search_subject select{
	margin:2%;
}
@media screen and (min-width:320px) and (max-width:480px){
.addform {
    height: auto;
    margin-bottom: 10px;
    padding: 20px 0 10px 10px;
}
#branch {
    font-size: 12px;
}
#search_subject #srch_subject {
    width: 100% !important;
}
#search.btn.btn-default {
    padding: 0 5px 1px;
}
#srrch {
    background-color: #fff;
    height: 235px;
    margin-bottom: 15px;
}
#srrch > form {
    display: inline-block;
    width: 100%;
}
#search.btn.btn-default {
    padding: 8px 15px;
    margin-left: 7px;
}
.col-sm-9.col-xs-6.paginationblk {
    text-align: center;
}
.datafie .col-sm-3 {
    padding-bottom: 10px;
}
.col-sm-9.col-xs-6.paginationblk > span {
    float: left;
    margin: 0;
    padding: 0 0 12px;
}
.paginationselbox {
	padding: 0 27px;
}
}
@media screen and (min-width:481px) and (max-width:767px){
.addform {
    height: auto;
    margin-bottom: 10px;
    padding: 20px 0 10px 10px;
}
#branch {
    font-size: 12px;
}
#search_subject #srch_subject {
    width: 100% !important;
}
.datafie .col-sm-3 {
    padding-bottom: 10px;
}
#srrch > form {
    display: inline-block;
    width: 100%;
}
#srrch {
    background-color: #fff;
    height: 280px;
    margin-bottom: 10px;
}
#search {
    margin-left: 15px;
}
}
@media screen and (min-width:768px) and (max-width:991px){
.col-sm-9.datafie {
    width: 100%;
}
#srrch {
    height: 250px;
    margin-bottom: 10px;
}
#srrch > form {
    display: inline-block;
    width: 100%;
}
#search_subject #srch_subject {
    width: 100% !important;
}
#search {
    margin-left: 10px;
}
.datafie .col-sm-3 select {
    font-size: 11px;
}
#subject {
    font-size: 12px;
}
}
@media screen and (min-width:992px) and (max-width:1200px){
.datafie .col-sm-3 select {
    font-size: 12px;
}
#subject {
    font-size: 11px;
}
#search_subject select {
    font-size: 11px;
}
#srrch > form {
    width: 74%;
}
}
	</style>
	