<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	var $logmode;
	function __construct(){
		
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','Admindashboard_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }

	

	public function index(){
		
		$school_id		= $this->session->userdata('user_school_id');
		$data['recentstudent'] = $this->Admindashboard_model->recentaddstudent($school_id);
		$this->load->view('header');
		$this->load->view('dashboard/dashboard', $data);
		$this->load->view('footer');

	}

}

