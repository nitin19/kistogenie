<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>School Management</title>
    
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">
    
    <link href="<?php echo base_url();?>assets/css/register.css" rel="stylesheet">
    
  </head>
  <body>

   <div class="loginpage registerpage">
   		<div class="container">
        	<div class="col-sm-12">
            	<div class="registersection">
                	<div class="logodiv">
                    	<a href="https://www.kistogenie.com"><img class="logo" src="<?php echo base_url();?>assets/images/logo.PNG"></a>
                        </div>
                	<div class="loginheader registerheader">
                        <h1>Olive Tree Study Support</h1>
                    </div>
                    <div class="loginform-content registerform-content">
                    
                    
                     <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>
                    
<form action="<?php echo base_url();?>/register/addschool" class="loginform" name="registerForm" id="registerForm" method="post">
                    
                    <div class="loginformarea registerformarea">
                    	<div class="col-sm-12 logintitle nopadding">
                        <img src="<?php echo base_url();?>assets/images/register-dividers.PNG">
                        <h1>Register Your School</h1>
                        </div>
                        <div class="registerformblk">
                        <div class="col-sm-12">
                        <div class="form-group">
    					<label>Enter School Name<span>*</span></label>
    					<input type="text" class="form-control" name="school_name" id="school_name" placeholder="School Name">
  						</div>
                        </div>
                        <div class="col-sm-6">
  						<div class="form-group">
    					<label>Email Address<span>*</span></label>
    					<input type="emali" class="form-control" name="school_email"  id="school_email" placeholder="lorem@lipsum.com">
  						</div>
                        </div>
                        <div class="col-sm-6">
  						<div class="form-group">
    					<label>Phone Number<span>*</span></label>
    					<input type="text" class="form-control" name="school_phone" id="school_phone" placeholder="1234567890">
  						</div>
                        </div>
                        <div class="col-sm-12">
                        <div class="form-group">
                        <label>School Address<span>*</span></label>
                        <textarea class="form-control" rows="3" name="school_address" id="school_address"></textarea>
                        </div>
                        </div>
                        <div class="col-sm-12">
  						<div class="form-group">
    					<label>Contact Person Name<span>*</span></label>
    					<input type="text" class="form-control" name="school_contact_person" id="school_contact_person" placeholder="Enter Your Name">
  						</div>
                        </div>
                        <div class="col-sm-6">
  						<div class="form-group">
    					<label>Username Name<span>*</span></label>
    					<input type="text" class="form-control" name="username" id="username" placeholder="Create User Name">
  						</div>
                        </div>
                        <div class="col-sm-6">
  						<div class="form-group">
    					<label>Password<span>*</span></label>
    					<input type="password" class="form-control" name="password" id="password" placeholder="..................">
  						</div>
                        </div>
                        <div class="col-sm-12">
  						<div class="checkbox checkboxarea">
    					<input type="checkbox" name="term_condition" id="term_condition">
<label for="term_condition"><span class="checkbox">I have read and agreed to <span style="color:#37b047">Terms and Conditions.</span></span></label>
  						</div>
                        </div>
                        <div class="col-sm-12">
                        <div class="login-button register-button">
  						<input type="submit" class="btn btn-default" value="Register">
                        </div>
                        </div>
                        
                        </div>
                       
                    </div>
                    
					</form>
                    </div>
                    <div class="login-links register-links">
                    	<div class="col-sm-12 col-xs-12 linkleft loginlinks nopadding">
            <!-- <a  class="loginpagelink" href="">If Already Have an Account. Please <a href="#" class="login-link">login</a></a>-->
                       If Already Have an Account. Please <a href="<?php echo base_url();?>" class="login-link">login</a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
   </div>
   <footer>
   <div class="container">
   <div class="copyright">
     <p>© <?php echo date("Y");?> Olive Tree Study Support. All Rights Reserved. | Powered by : <span><a href="http://www.binarydata.in/" target="_blank">Binary Data</a></span></p>
   </div>
   </div>
   </footer>
   
   
    
     <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
     
     <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
     
     <script src="<?php echo base_url();?>assets/js/jquery-validate.bootstrap-tooltip.min.js"></script>

     <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

     

         <script>

   jQuery(document).ready(function(){

/*	setTimeout(function() {

            jQuery('#flashMessage').slideUp('slow');

            }, 3000);*/

	
	jQuery.validator.addMethod("alphaUname", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    });
	
	jQuery.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaLnumber", function(value, element) {
      return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaUpwd", function(value, element) {
    return this.optional(element) || value == value.match(/^(?=.*[a-zA-Z0-9])[a-zA-Z0-9!@#$%&*.]{7,}$/);
    });		
	
		
/*	  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });*/

    jQuery("#registerForm").validate({

        rules: {
			school_name: {
                   required: true,
				   alphaLetter: true,
				   maxlength: 120,
				   remote: {
					 url: "<?php echo base_url(); ?>register/check_schoolname",
					 type: "POST"
					 }
                 },
			school_email: {
                   required: true,
				   email: true,
				   maxlength: 120
                 },
			school_phone: {
                   required: true,
				   number: true,
				   maxlength: 14
                 },
			school_address: {
                   required: true,
				   maxlength: 250
                 },
			school_contact_person: {
                   required: true,
				   alphaLetter: true,
				   maxlength: 60
                 },
				 	 	
			 username: {
				  required: true,
				  alphaUname: true,
				  minlength: 4,
				  maxlength: 40,
				  remote: {
					 url: "<?php echo base_url(); ?>register/check_username",
					 type: "POST"
					 }
       			  },
			 password: {
				  required: true,
				  alphaUpwd: true,
				  minlength: 6,
				  maxlength: 15
			   },
			   
			  term_condition: {
                   required: true,
                 }
	
            },

        messages: {
			
			school_name: {
                  required: "This field is required.",
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 120 characters allowed.",
				  remote: "School name already exists."
                 },
				 
			school_email: {
                    required: "This field is required.",
					email: "Please enter a valid email address.",
					maxlength: "Maximum 120 characters allowed."
                   },
		  
		   school_phone: {
				  required: "This field is required.",
				  number: "Numbers only please.",
				  minlength: "Minimum 10 characters required.",
				  maxlength: "Maximum 14 characters allowed."
				},
				
			school_address: {
				   required: "This field is required.",
				   maxlength: "Maximum 250 characters allowed."
				},
				
			school_contact_person: {
                  required: "This field is required.",
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
                 },

            username: {
                  required: "This field is required.",
				  alphaUname: "Letters only please.",
				  minlength: "Minimum 4 characters required.",
				  maxlength: "Maximum 40 characters allowed.",
				  remote: "User name already in use."
                 },
			    password: {
                  required: "This field is required.",
				  alphaUpwd: "Letters, numbers and special characters allowed",
				  minlength: "Minimum 6 characters required.",
				  maxlength: "Maximum 15 characters allowed."
                 },
				 
				 term_condition: {
                   required: "This field is required."
                 }
          },
        
		submitHandler: function(form) {

            form.submit();

        }

     });

  });

  </script>
  
  <script>
	jQuery(document).ready(function(){
		$('body').click(function(){
			$('.alert-dismissable').fadeOut('slow');
		})
	})
</script>
    
    
  </body>
</html>