<?php 
$this->load->view('common/header');
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">


  <section class="banner">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="banner_head">Staff Enrolment</h2>
            <ol class="breadcrumb">
              <li><a href="http://www.olivetreestudy.co.uk">Home</a></li>
              <li><a href="#">Enrolment</a></li>
            </ol>
          </div>
        </div>
      </div>
    </section>

       <!-- start reg -->
    <section class="reg">
     <div class="container">
       <div class="row ">
          <?php
          if($this->session->flashdata('success'))
          {
            ?>
            <div class="col-md-offset-3 col-md-5 col-sm-offset-3 col-sm-6 col-xs-12 ">
              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
              </div>
            </div>
            <?php
            
          }

          if($this->session->flashdata('error'))
          {
            ?>
            <div class="col-md-offset-3 col-md-5 col-sm-offset-3 col-sm-6 col-xs-12 ">
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> <?php echo $this->session->flashdata('error');?>
              </div>
            </div>
            <?php
            
          }
          ?>
         <div class="col-md-12">
           <h2>Registration form for Staff</h2>
            <span class="mapline"><i class="fa fa-map map"></i></span>
            <div class="space60"></div>
            <div class="row">
              <div class="col-md-offset-3  col-md-5 col-sm-offset-3 col-sm-6 col-xs-12">
                <form role="form" class="form-horizontal" id="staffForm" name="staffForm" method="POST" action="<?php echo site_url("Staff_enrol") ?>/insert_staff" method="post" novalidate="novalidate" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">Branch</label>
                    <div class="col-sm-8">
                     
                      <select class="form-control" name="branch" id="branch">
                        <option value="" disabled="" selected="">Select one </option>
                        <?php 
                          $branches = $this->Registration_model->get_all_branch();
                          if(sizeof($branches) > 0)
                          {
                            foreach ($branches as $branch) {
                              ?>
                              <option value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>
                              <?php
                            }
                            
                          }
                        ?>
                        
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">Title</label>
                    <div class="col-sm-8">
                      
                      <select name="title" id="title" class="form-control">
                        <option value="" disabled="" selected="">Select one </option>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Ms">Ms</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="staff_first" class="col-sm-4 control-label">Staff's First Name</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" class="form-control" name="staff_first" id="staff_first" value="" maxlength="61">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="staff_last" class="col-sm-4 control-label">Staff's Last Name</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" class="form-control" name="staff_last" id="staff_last" value="" maxlength="61">
                    </div>
                  </div>
                 
                  <div class="form-group">
                    <label for="dob" class="col-sm-4 control-label">Date of Birth</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                     <!-- <input type="text" class="form-control datepicker" name="dob" id="inputPassword3" maxlength="10" placeholder="YYYY-MM-DD">-->
                     <div class="input-group date" data-date-format="dd.mm.yyyy">
                        <input  type="text" class="form-control" name="dob" id="dob" placeholder="dd.mm.yyyy">
                        <div class="input-group-addon" >
                          <span class="fa fa-calendar"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="education" class="col-sm-4 control-label">Education Qualification</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" name="education" id="education" class="form-control" value="" maxlength="61">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-sm-4 control-label">E-mail Address</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="email" class="form-control" id="email" name="email" value="" maxlength="121">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="phone" class="col-sm-4 control-label">Phone</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="text" name="phone" id="phone" class="form-control" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="cv" class="col-sm-4 control-label">Upload CV</label>
                    <div class="col-sm-8">
                      <!-- <input type="password" class="form-control" id="inputPassword3" placeholder=""> -->
                      <input type="file" name="cv" id="cv" >
                    </div>
                  </div>

                  <!-- <button class="btn btn_enrole">Enrole Me </button> -->
                  <input type="submit" class="btn btn_enrole" name="staffbtn" id="staffbtn" value="Enrole Me">
                </form>
              </div>

           
            </div>
         </div>
       </div>
     </div>
   </section>
   <!-- end reg -->


<?php $this->load->view('common/footer'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<style type="text/css">
  .error {
    color: red !important;
}
form#studentForm .col-sm-8 {
    margin-bottom: 20px;
}

</style>

<script>
    $(document).ready(function(){
     $('.input-group.date').datepicker({format: "dd/mm/yyyy"}); 
    });
</script>

       <script>
     
            (function ($, W, D)
            {
                var JQUERY4U = {};
                JQUERY4U.UTIL =
                        {
                            setupFormValidation: function ()
                            {
                                //form validation rules
                                $("#staffForm").validate({
                                    rules: {
                                         
                                        branch:
                                        {
                                            required: true
                                        },
                                        title :
          					                    {
          					                        required : true
          					                    },
                                        staff_first :
                                        {
                                            required : true
                                        },
                                        staff_last :
                                        {
                                            required : true
                                        },
                                        dob :
                                        {
                                            required : true
                                        },
                                        education :
                                        {
                                            required : true
                                        },
                                        email :
                                        {
                                            required : true,
                                            email : true
                                        },
                                        phone :
                                        {
                                            required : true
                                        },
                                        cv :
                                        {
                                            required : true
                                        }  
					                  
					                         },


                                    messages: {
                                        branch :
                                        {
                                        required: "Please select branch"
                                        },
                                        title : 
                                        {
                                        required:"Please select title"
                                        },
                                        staff_first : 
                                        {
                                        required:"Please enter staff_first"
                                        },
                                        staff_last : 
                                        {
                                        required:"Please enter staff_last"
                                        },
                                        dob : 
                                        {
                                        required:"Please enter dob"
                                        },
                                        education : 
                                        {
                                        required:"Please enter education"
                                        },
                                        email : 
                                        {
                                        required:"Please enter email"
                                        },
                                        phone :
                                        {
                                          required:"Please enter phone number"  
                                        },
                                        cv :
                                        {
                                          required:"Please upload cv"  
                                        }
                                    },
                  
                                    submitHandler: function (form) {
                                        form.submit();
                                    }
                                });
                            }
                        }

                //when the dom has loaded setup form validation rules
                $(D).ready(function ($) {
                    JQUERY4U.UTIL.setupFormValidation();
                });
            })(jQuery, window, document);
        </script>
        
 
</body>
</html>