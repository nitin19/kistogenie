<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentaccountdetail extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->model('student_model');
    }

	public function index()

	{
		$id=34;
		$this->load->view('header');
$data['result']=$this->student_model->get_stud_acc_dtl($id);
		$this->load->view('student/student_account_detail', $data);

		$this->load->view('footer');

	}
	
	public function idd() {
		$id=34;
	    $this->load->view('header');
	    $id = $this->uri->segment(3);
	    $data['result']=$this->student_model->get_stud_acc_dtl($id);
        $this->load->view('staff/student_account_detail',$data);
        $this->load->view('footer');
    }

}

