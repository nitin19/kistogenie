<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schoolbranch extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','school_branch_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){
		
		$status_search					= isset($_GET['status'])?$_GET['status']:NULL; 
		$word_search				    = isset($_GET['seachword'])?$_GET['seachword']:NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$search_condition['is_deleted']	= '0';
		if($status_search!=''){
            $search_condition['is_active']	= $status_search;
		  }
		 
		$data							= array();
		
		$school_id		= $this->session->userdata('user_school_id');
		
		$data['title']	      			= "Branch";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'schoolbranch/index';
	    $config['total_rows'] 			= $this->school_branch_model->getRows($search_condition,$school_id,$word_search);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = "/?status=$status_search&seachword=$word_search&perpage=$perpage"; 
		
		$this->pagination->initialize($config);

		$odr =   "branch_id";
		$dirc	= "desc";
		
		$data_rows= $this->school_branch_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$word_search);
		
		$branchid = $this->uri->segment(4);
		if($branchid!='') {
		  $branch_data_rows				= $this->school_branch_model->get_single_branch($branchid);
		  $data['info']				    = $branch_data_rows[0];
		 }
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['status_search']			= $status_search;
		$data['word_search']		    = $word_search;
		$data['PerPage']				= $PerPage;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
						
		$this->load->view('header');
		$this->load->view('master/school_branch', $data);
		$this->load->view('footer');
     }
	 
	 public function addbranch(){
		 
		    $branchdata 		= array();
			
			$school_id = $this->session->userdata('user_school_id');
			
		    $branchdata['school_id'] = $this->session->userdata('user_school_id');
			$branchdata['created_by'] = $this->session->userdata('user_name');
			$branchdata['is_active'] = '1';
			$branchdata['created_date'] = date('Y-m-d H:i:s');;
			$branchdata['updated_date'] = date('Y-m-d H:i:s');
			
			if (trim($this->input->post('branchname')) != '') {
                $branchdata['branch_name'] = trim($this->input->post('branchname'));
				$branchname = trim($this->input->post('branchname'));
            }
			
			if (trim($this->input->post('branchaddress')) != '') {
                $branchdata['branch_address'] = trim($this->input->post('branchaddress'));
            }
			if (trim($this->input->post('start_timing')) && trim($this->input->post('end_timing')) != '') {
                $branchdata['branch_timing'] = trim($this->input->post('start_timing')) .'-'. trim($this->input->post('end_timing'));
            }
			if ($this->input->post('branch_days') != '') {
				$branch_days = $this->input->post('branch_days');
                $branchdata['branch_days'] =implode(',', $branch_days);
            }
			 $checkbranchname = $this->school_branch_model->check_branch_name($branchname, $school_id);
			 $countBranches = count($checkbranchname);
			 
			 if( $countBranches > 0 ) {
				  $this->session->set_flashdata('error', 'Branch name already exists. Branch has not been added.');
				  redirect(base_url()."schoolbranch/");exit;
			 } else {
			
		   if($this->school_branch_model->insertBranch($branchdata)) {
			   $this->session->set_flashdata('success', 'Branch has been added successfuly.');
			   redirect(base_url()."schoolbranch/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Branch has not been added.');
				redirect(base_url()."schoolbranch/");exit;
		      }
			}
			
        }
		
		
		public function updatebranch(){
			
			  $start						    = $this->uri->segment(3);
		      $editedBranchid					= $this->uri->segment(4);
			  $Action							= $this->uri->segment(5);  
		 
		  $postURL	= $this->createPostURL($_GET);
		if($editedBranchid==NULL){
			$this->session->set_flashdata('error', 'Select student first.');
			redirect(base_url()."schoolbranch");exit;
		   }
		   
		 $branchdata 		= array();
         $school_id = $this->session->userdata('user_school_id');
		 $where							= array('branch_id'=>$editedBranchid);
		 $data['title']	      			= "Edit Branch";
		 $data['mode']					= "Edit";
			
			if (trim($this->input->post('branchname')) != '') {
                $branchdata['branch_name'] = trim($this->input->post('branchname'));
				$branchname = trim($this->input->post('branchname'));
            }
			
			if (trim($this->input->post('branchaddress')) != '') {
                $branchdata['branch_address'] = trim($this->input->post('branchaddress'));
            }
	      if (trim($this->input->post('start_timing')) && trim($this->input->post('end_timing')) != '') {
                $branchdata['branch_timing'] = trim($this->input->post('start_timing')) .'-'. trim($this->input->post('end_timing'));
            }
          if ($this->input->post('branch_days') != '') {
				$branch_days = $this->input->post('branch_days');
				$branchdata['branch_days'] =implode(',', $branch_days);
            }
			else {
				
				$branchdata['branch_days'] ='';
				}
			$branchdata['updated_date'] = date('Y-m-d H:i:s');
			
			 $checkbranchname = $this->school_branch_model->check_editedbranch_name($branchname, $school_id, $editedBranchid);
			 $countBranches = count($checkbranchname);
			 
			 if( $countBranches > 0 ) {
				  $this->session->set_flashdata('error', 'Branch name already exists. Branch has not been updated.');
				  redirect(base_url()."schoolbranch/index/$start/$editedBranchid/edit/");exit;
			 } else {
			
		   if($this->school_branch_model->updateBranches($branchdata,$where)) {
			   $this->session->set_flashdata('success', 'Branch has been updated successfuly.');
			   redirect(base_url()."schoolbranch/index/$start/$editedBranchid/edit/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Branch has not been updated.');
				redirect(base_url()."schoolbranch/index/$start/$editedBranchid/edit/");exit;
		      }
			}
		

	   }
	   
	   
	   	function deactivatebranch(){
		
		  $start						    = $this->uri->segment(3);
		  $branchid							= $this->uri->segment(4); 
		$where		= array('branch_id'=>$branchid);
		$data		= array('is_active'=>'0');
		if($this->school_branch_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Branch has been updated successfuly.');
			redirect(base_url()."schoolbranch/index/$start/$branchid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Branch has not been updated.');
			redirect(base_url()."schoolbranch/index/$start/$branchid");exit;
		}
		
	}
	function activatebranch(){
		 
		  $start						    = $this->uri->segment(3);
		  $branchid							= $this->uri->segment(4); 
			  
		$where		= array('branch_id'=>$branchid);
		$data		= array('is_active'=>'1');
		if($this->school_branch_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Branch has been updated successfuly.');
			redirect(base_url()."schoolbranch/index/$start/$branchid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Branch has not been updated.');
			redirect(base_url()."schoolbranch/index/$start/$branchid");exit;
		}
		
	}
	
	
		public function deletebranch($branchid){
			
			 $school_id = $this->session->userdata('user_school_id');
			 
			 $chk_student_branch = $this->school_branch_model->check_student_branch($school_id,$branchid);
			 $countStudentbranch = count($chk_student_branch); 
			 
			  $chk_staff_branch = $this->school_branch_model->check_staff_branch($school_id,$branchid);
			  $countStaffbranch = count($chk_staff_branch);
			 
			   if( $countStudentbranch > 0 || $countStaffbranch > 0 ) {
				  $this->session->set_flashdata('error', 'Branch has not been deleted. As this Branch is assigned to students or staff');
				  redirect(base_url()."schoolbranch/index/");exit;
				 } else {
			
		 		 if($this->school_branch_model->delete($branchid)){
					$this->session->set_flashdata('success', 'Branch has been deleted successfuly.');
					redirect(base_url()."schoolbranch/index");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."schoolbranch/index");exit;
		          }
			  }

	    }
	
		function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}
	
	
}