<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Attendancereportarchive extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','attendancereportarchive_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{
		$data							= array();
		$school_id		= $this->session->userdata('user_school_id');
	   
	    $data['branches'] = $this->attendancereportarchive_model->getbranches($school_id); 
		
		$sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;
		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		if($sdate_search!=''){
			$startdate	= date('Y-m-d', strtotime($sdate_search));
		}

		if($edate_search!=''){
			$enddate	= date('Y-m-d', strtotime($edate_search));
		}

		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}

		if($class_search!=''){
			$search_condition['class_id']	= $class_search;
		}
		
		$search_condition['school_id']	= $this->session->userdata('user_school_id');
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		$data['title']	      			= "Attendance archive";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'attendancereportarchive/index';

	    $config['total_rows'] 			= $this->attendancereportarchive_model->getRows($search_condition,@$startdate,@$enddate);
	   
		$config['per_page'] 			= $perpage;
 		$config['postfix_string'] = "/?sdate=$sdate_search&edate=$edate_search&branch=$branch_search&class=$class_search"; 
		$this->pagination->initialize($config);
		$odr =   "archive_id";
		$dirc	= "desc";

		$data_rows  = $this->attendancereportarchive_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,@$startdate,@$enddate);
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
   
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['class_search']			= $class_search;
		$data['sdate_search']			= $sdate_search;
		$data['edate_search']			= $edate_search;
		$data['PerPage']				= $PerPage;
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		$this->load->view('header');
		$this->load->view('attendance/attendance_report_archive', $data);
		$this->load->view('footer');

	}
	
	
	  public function check_classes()

	{     
		    $school_id = $this->session->userdata('user_school_id');
			$schoolbranch = trim($_POST['schoolbranch']); 
	        $getclasses = $this->attendancereportarchive_model->getclasses($school_id,$schoolbranch);
			
			foreach($getclasses as $getclass) {
 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
			}
	  }
	  
	  
	  public function delete($id){
		  $filename = $this->attendancereportarchive_model->filename($id);
		$fileName=$filename->archive_filename;

			 if ($fileName) {

				 $file = realpath ( FCPATH.'uploads/generate_report_archive/'.$fileName.'.xls' );
	
        		if (file_exists ( $file )) {
	
				$this->load->helper("file");
			 	$this->load->helper("url");
			 	$url = FCPATH.'uploads/generate_report_archive/'.$fileName.'.xls';
			 	unlink($url);
				
				$this->attendancereportarchive_model->delete($id);
				
				$this->session->set_flashdata('success', 'Attendance archive has been deleted successfuly.');
				redirect(base_url()."attendancereportarchive");exit;
				}
				}
		else{
			$this->session->set_flashdata('error', 'Some problem exists. Try again.');
			redirect(base_url()."attendancereportarchive");exit;
		}
 
		
	 }
	 
	 
	 		 
	 public function download_excel_backup($id) { 
	   	 $filename = $this->attendancereportarchive_model->filename($id);
		$fileName=$filename->archive_filename;

       if ($fileName) {
         $file = realpath ( FCPATH.'uploads/generate_report_archive/'.$fileName.'.xls' );
		
        if (file_exists ( $file )) {
         $data = file_get_contents ( $file );
         $this->load->helper('download');
         force_download ( $fileName.'.xls', $data );
        } else {
		 $this->session->set_flashdata('error', 'Requested file does not exists.');
         redirect(base_url()."schoolconfig/index/");exit;
        }
       }
      }
	  


}

