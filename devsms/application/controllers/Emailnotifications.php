<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Emailnotifications extends CI_Controller {



	 var $logmode;

	function __construct(){

       

        parent::__construct();

        if( $this->authorize->is_user_logged_in() == false ){

			$this->session->set_flashdata('error', 'Please login first.');

			redirect(base_url());

		   }

		$this->logmode	= $this->session->userdata('log_mode');

        $this->load->model(array('login_model','authorization_model','emailnotifications_model'));

		$this->load->database();

        $this->load->library('session');

		$this->load->library('form_validation');

		$this->load->library('image_lib');

		 

    }



	public function index() {   

	    $data					= array(); 

		$school_id				= $this->session->userdata('user_school_id');

		$data['branches']		= $this->emailnotifications_model->getbranches($school_id);

		$this->load->view('header');

		$this->load->view('bulkemials/email_notification_view', $data);

		$this->load->view('footer');

	

	 }

	 

     public function getStaff() {

			$branch_id  = $this->input->post('branch_id');

		    if( $branch_id == '' ) { 

			   } else {

			$school_id	= $this->session->userdata('user_school_id');

			$getstaffdata = $this->emailnotifications_model->get_staff($school_id, $branch_id);	

			$HTML5='';

			foreach($getstaffdata as $getstaff)

				{

				$branchInfo = $this->emailnotifications_model->getbranchName($getstaff->branch_id);

				$HTML5.='<option value="'.$getstaff->user_id.'"';

				 if($getstaff->user_id == $getstaff->user_id){ 

				   $HTML5.= 'selected';

				  }

				 $HTML5.= '>'.$getstaff->staff_fname.' '.$getstudent->staff_lname.' ('.$branchInfo->branch_name.') </option>';

					}

			   echo $HTML5;

           }

	   }

	 

   public function getclass() {

			$branch_id  = $this->input->post('branch_id');

		    if( $branch_id == '' ) { 

			   } else {

			$school_id	= $this->session->userdata('user_school_id');

			$getclasses = $this->emailnotifications_model->get_classes($school_id, $branch_id);	

			$HTML1='';

			$cls_id_str = '';

			foreach($getclasses as $getclass)

				{

				 $cls_id_str.= $getclass->class_id.',';

				$branchInfo = $this->emailnotifications_model->getbranchName($getclass->branch_id);

				$HTML1.='<option value="'.$getclass->class_id.'"';

				 if($getclass->class_id == $getclass->class_id){ 

				   $HTML1.= 'selected';

				  }

				 $HTML1.= '>'.$getclass->class_name.' ('.$branchInfo->branch_name.') </option>';

					}

			

			$classIDSstring = rtrim($cls_id_str,',');

			$class_id  =  explode(',', $classIDSstring); 

			$getStudents = $this->emailnotifications_model->getStudents($school_id, $branch_id, $class_id);

		    $HTML2 = '';	

			foreach($getStudents as $getstudent) {

				$branchInfo = $this->emailnotifications_model->getbranchName($getstudent->student_school_branch);

				$classesInfo = $this->emailnotifications_model->getClassName($getstudent->student_class_group);

				

				 $HTML2.='<option value="'.$getstudent->user_id.'"';

				 if($getstudent->user_id == $getstudent->user_id){ 

				   $HTML2.= 'selected';

				  }

				  $HTML2.= '>'.$getstudent->student_fname.' '.$getstudent->student_lname.' ('.$classesInfo->class_name.')  ('.$branchInfo->branch_name.') </option>';

					}

			  echo json_encode(array('datacls'=>$HTML1,'datastu'=>$HTML2));

           }

	   }

		

 	public function get_students(){

			 $class_id  = $this->input->post('class_id');

			 if( $class_id == '' ) {

			 } else {

			/*$class_id = implode(',', $class_ids);*/

			$branch_id  = $this->input->post('branch_id');

			$school_id	= $this->session->userdata('user_school_id');

			$getStudents = $this->emailnotifications_model->getStudents($school_id, $branch_id, $class_id);	

			foreach($getStudents as $getstudent) {

				$branchInfo = $this->emailnotifications_model->getbranchName($getstudent->student_school_branch);

				$classesInfo = $this->emailnotifications_model->getClassName($getstudent->student_class_group);

				

				  echo '<option value="'.$getstudent->user_id.'"';

				 if($getstudent->user_id == $getstudent->user_id){ 

				    echo 'selected';

				  }

				 echo '>'.$getstudent->student_fname.' '.$getstudent->student_lname.' ('.$classesInfo->class_name.')  ('.$branchInfo->branch_name.') </option>';

						 

			    }		   		  

	       }

	 }

        public function sendnotificationStaff(){

			$data		=	 array();

			$user_id	= $this->session->userdata('user_id');

			$school_id  = $this->session->userdata('user_school_id');

			$data['school_id'] = $this->session->userdata('user_school_id');

			$data['user_type'] = 'staff';

			if ($this->input->post('notification_type_staff') != '') {

				  $data['notification_type'] = $this->input->post('notification_type_staff');

				  $notification_type = $this->input->post('notification_type_staff');

				  }

			if ($this->input->post('branch_staff') != '') {

			    $branchids = $this->input->post('branch_staff');

			    $data['branches'] = implode(',', $branchids);

				 }	

			$data['from_user'] = $this->session->userdata('user_id');
	

			if ($this->input->post('staff_name') != '') {

				$staffnames = $this->input->post('staff_name');

				$data['to_user'] = implode(',', $staffnames);

				}

			if ($this->input->post('staff_email_subject') != '') {

				  $data['email_subject'] = $this->input->post('staff_email_subject');

				  $staff_email_subject = $this->input->post('staff_email_subject');

				  }

			if ($this->input->post('staff_email_message') != '') {

				  $data['email_message'] = $this->input->post('staff_email_message');

				  $staff_email_message = $this->input->post('staff_email_message');

				  }

			$data['status'] 		= 'Pending';

		 	$data['created_by'] 	= $this->session->userdata('user_id');

			$data['created_date'] 	= date('Y-m-d H:i:s');  

			$data['is_active'] 		= '1';  

			$data['is_deleted'] 	= '0';  

	        $files 					= $_FILES;
        //	$count			= 	count($_FILES['attachments']['name']);

                $_FILES['attachments']['name']		=	$files['attachments']['name'];
                $_FILES['attachments']['type']		= 	$files['attachments']['type'];
                $_FILES['attachments']['tmp_name']	= 	$files['attachments']['tmp_name'];
                $_FILES['attachments']['error']		= 	$files['attachments']['error'];
                $_FILES['attachments']['size']		= 	$files['attachments']['size'];
                $config['upload_path'] 				= 	'./uploads/notification';
                $config['allowed_types'] 			= 	'gif|jpg|png|docx|jpeg|xls|pdf|txt';
	  			//$config['max_size']             	= 	500;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload("attachments");
                $fileName = $_FILES['attachments']['name'];
                //$images = $fileName;
        
        // $fileName = implode(',',$images);
		 $data['attachments'] = $fileName;

			if($notification_type == 'email') {

				 if($this->emailnotifications_model->insertnotificationEmialcron($data)) {

				 	require("https://www.kistogenie.com/emailtesting.php");

			        $this->session->set_flashdata('success', 'Message will received by users after some interval.');

	 				redirect(base_url()."emailnotifications/index");exit;

			      } else {

	                $this->session->set_flashdata('error', 'Some problem exists. Message has not been sent.');

	 				redirect(base_url()."emailnotifications/index");exit;

				     }

		  	} elseif($notification_type == 'both') {

			  if($this->emailnotifications_model->insertnotificationEmialcron($data)) {

			        $input	=  $this->input->post();

			     if($this->emailnotifications_model->add_notification_staff($input)) {

					 $this->session->set_flashdata('success', 'Message will received by users after some interval.');

	 				 redirect(base_url()."emailnotifications/index");exit;

				      } else {

					  $this->session->set_flashdata('error', 'Some problem exists. Message has not been sent.');

	 				  redirect(base_url()."emailnotifications/index");exit;

				       }

			      } else {

	                 $this->session->set_flashdata('error', 'Some problem exists. Message has not been sent.');

	 				 redirect(base_url()."emailnotifications/index");exit;

				     }

			} else {

			       $input	=  $this->input->post();

			    if($this->emailnotifications_model->add_notification_staff($input)) {

					 $this->session->set_flashdata('success', 'Message will received by users after some interval.');

	  				 redirect(base_url()."emailnotifications/index");exit;

				      } else {

					  $this->session->set_flashdata('error', 'Some problem exists. Message has not been sent.');

	                  redirect(base_url()."emailnotifications/index");exit;

				       }

			  }

  	}


        public function sendnotification(){

			   $data	=	 array();

			   $user_id		= $this->session->userdata('user_id');

			   $school_id  = $this->session->userdata('user_school_id');

			   

			 $data['school_id'] = $this->session->userdata('user_school_id');

			 

			 $data['user_type'] = 'student';

				  

			 if ($this->input->post('notification_type') != '') {

				  $data['notification_type'] = $this->input->post('notification_type');

				  $notification_type = $this->input->post('notification_type');

				  }

				  

			 if ($this->input->post('branch') != '') {

			    $branchids = $this->input->post('branch');

			    $data['branches'] = implode(',', $branchids);

				 }	

				 

			if ($this->input->post('class_name') != '') {

				$classes = $this->input->post('class_name');

				$data['classes'] = implode(',', $classes);

				}

				

			$data['from_user'] = $this->session->userdata('user_id');

			 	

			if ($this->input->post('studentsname') != '') {

				$studentsnames = $this->input->post('studentsname');

				$data['to_user'] = implode(',', $studentsnames);

				}

				

			if ($this->input->post('student_email_subject') != '') {

				  $data['email_subject'] = $this->input->post('student_email_subject');

				  $student_email_subject = $this->input->post('student_email_subject');

				  }

				  	

			if ($this->input->post('student_email_message') != '') {

				  $data['email_message'] = $this->input->post('student_email_message');

				  $student_email_message = $this->input->post('student_email_message');

				  }


				 $data['status'] = 'Pending';  

				 $data['created_by'] = $this->session->userdata('user_id');

				 $data['created_date'] = date('Y-m-d H:i:s');  

				 $data['is_active'] = '1';  

				 $data['is_deleted'] = '0';  

	        	        $files 			= 	$_FILES;
        //	$count			= 	count($_FILES['attachments']['name']);

                $_FILES['attachments']['name']		=	$files['attachments']['name'];
                $_FILES['attachments']['type']		= 	$files['attachments']['type'];
                $_FILES['attachments']['tmp_name']	= 	$files['attachments']['tmp_name'];
                $_FILES['attachments']['error']		= 	$files['attachments']['error'];
                $_FILES['attachments']['size']		= 	$files['attachments']['size'];
                $config['upload_path'] 				= 	'./uploads/notification';
                $config['allowed_types'] 			= 	'gif|jpg|png|docx|jpeg|xls|pdf|txt';
	  			//$config['max_size']             	= 	500;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload("attachments");
                $fileName = $_FILES['attachments']['name'];
                //$images = $fileName;
        
        // $fileName = implode(',',$images);
		 $data['attachments'] = $fileName;

			if($notification_type == 'email') {

				 if($this->emailnotifications_model->insertnotificationEmialcron($data)) {

				 	require("https://www.kistogenie.com/emailtesting.php");

			         $this->session->set_flashdata('success', 'Message will received by users after some interval.');

	 				 redirect(base_url()."emailnotifications/index");exit;

			      } else {

	                 $this->session->set_flashdata('error', 'Some problem exists. Message has not been sent.');

	 				 redirect(base_url()."emailnotifications/index");exit;

				     }

		  	} elseif($notification_type == 'both') {

			  if($this->emailnotifications_model->insertnotificationEmialcron($data)) {

			        $input	=  $this->input->post();

			     if($this->emailnotifications_model->add_notification($input)) {

					 $this->session->set_flashdata('success', 'Message will received by users after some interval.');

	 				 redirect(base_url()."emailnotifications/index");exit;

				      } else {

					  $this->session->set_flashdata('error', 'Some problem exists. Message has not been sent.');

	 				  redirect(base_url()."emailnotifications/index");exit;

				       }

			      } else {

	                 $this->session->set_flashdata('error', 'Some problem exists. Message has not been sent.');

	 				 redirect(base_url()."emailnotifications/index");exit;

				     }

			} else {

			       $input	=  $this->input->post();

			    if($this->emailnotifications_model->add_notification($input)) {

					 $this->session->set_flashdata('success', 'Message will received by users after some interval.');

	  				 redirect(base_url()."emailnotifications/index");exit;

				      } else {

					  $this->session->set_flashdata('error', 'Some problem exists. Message has not been sent.');

	                  redirect(base_url()."emailnotifications/index");exit;

				       }

			  }

  	}


public function get_messagedesc($message_id){ 

	$message_id= $this->input->post('message_id');

	$message = $this->emailnotifications_model->get_singlemsg($message_id);

	echo $message->description.',,'.$message->title;



}




}



