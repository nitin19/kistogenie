<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendanceregister extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        //if( $this->authorize->is_user_logged_in() == false ){
			//$this->session->set_flashdata('error', 'Please login first.');
			//redirect(base_url());
		   //}
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','attendanceregister_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
    }

	public function index(){
		//$this->load->view('header');
		$this->load->view('attendanceregister/attendance_register');
		$this->load->view('footer');
	}
	
	public function attendance_add() 
	{
		
		  		$userdata = array();
				$staffData = array();	
				$user	= $this->input->post('username');
				$pass	= $this->input->post('password'); 
				$status	= $this->input->post('in_out_status'); 
				$checkUser = $this->attendanceregister_model->check_staff_username($user,$pass);
						if($checkUser) {							
						$userid = $checkUser->id;
						$schoolId = $checkUser->school_id;
						$staffData['school_id'] = $schoolId;
						$staffData['user_id'] = $userid;
						$staffData['Date_Time'] = date('Y-m-d H:i:s');
						$staffData['is_active'] = '1';
						$staffData['is_deleted'] = '0';
						$staffData['in_out_status'] = $status;
						
						$user_last_att = $this->attendanceregister_model->check_last_status($userid);
						if(count($user_last_att) > 0 && $user_last_att->in_out_status==$status){
							$this->session->set_flashdata('error', 'You are already Log '.$status.', Attendance has not Saved');
							redirect(base_url()."attendanceregister"); exit;
						}
						 else if(count($user_last_att)==0 && $status=='Out'){
							$this->session->set_flashdata('error', 'You are not already Login. Attendance status log out is not correct.');
							redirect(base_url()."attendanceregister"); exit; 
						 }
						 else{
							// if($user_last_att){ $staffData['in_out_status'] = $status; }
							 
									if ($this->attendanceregister_model->add_attendance($staffData))
									{
								        $this->session->set_flashdata('success', 'Attedance saved successfully.');
								        redirect(base_url()."attendanceregister"); exit;
										} else {
											$this->session->set_flashdata('error', 'Attedance Not saved successfully.'); 
											redirect(base_url()."attendanceregister"); exit;
										}
							
						}
						
				} else {
						            $this->session->set_flashdata('error', 'Invalid Username and Password. Please enter correct Username or Password.');
							    redirect(base_url()."attendanceregister"); exit;
						    }
					 
	  }
	
}



