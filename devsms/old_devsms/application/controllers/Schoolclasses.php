<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schoolclasses extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','school_classes_model','student_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){
		
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$status_search					= isset($_GET['status'])?$_GET['status']:NULL; 
		$word_search				    = isset($_GET['seachword'])?$_GET['seachword']:NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$search_condition['is_deleted']	= '0';
		 
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		if($status_search!=''){
            $search_condition['is_active']	= $status_search;
		  }
		 
		$data							= array();
		
		$school_id		= $this->session->userdata('user_school_id');
		$data['branches'] = $this->school_classes_model->getbranches($school_id);
		$data['sclasses'] = $this->school_classes_model->getschoolclasses($school_id);
		
		$data['title']	      			= "Classes";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'schoolclasses/index';
	    $config['total_rows'] 			= $this->school_classes_model->getRows($search_condition,$school_id,$word_search);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = "/?branch=$branch_search&status=$status_search&seachword=$word_search&perpage=$perpage"; 
		
		$this->pagination->initialize($config);

		$odr =   "class_id";
		$dirc	= "desc";
		
		$data_rows= $this->school_classes_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$word_search);
		
		$classid = $this->uri->segment(4);
		if($classid!='') {
		  $class_data_rows				= $this->school_classes_model->get_single_class($classid);
		  $data['info']				    = $class_data_rows[0];
		 }
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['status_search']			= $status_search;
		$data['word_search']		    = $word_search;
		$data['PerPage']				= $PerPage;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
						
		$this->load->view('header');
		$this->load->view('master/school_classes', $data);
		$this->load->view('footer');
     }
	 
	 public function addclass(){
		 
		    $classdata 		= array();
			
			$school_id = $this->session->userdata('user_school_id');
			
		    $classdata['school_id'] = $this->session->userdata('user_school_id');
			$classdata['created_by'] = $this->session->userdata('user_name');
			$classdata['is_active'] = '1';
			$classdata['created_date'] = date('Y-m-d H:i:s');;
			$classdata['updated_date'] = date('Y-m-d H:i:s');
			 
			if (trim($this->input->post('schoolbranch')) != '') {
                $classdata['branch_id'] = trim($this->input->post('schoolbranch'));
				 $branch_id = trim($this->input->post('schoolbranch'));
            }
			
			if (trim($this->input->post('classname')) != '') {
                $classdata['class_name'] = trim($this->input->post('classname'));
				$class_name = trim($this->input->post('classname'));
            }
			
			if (trim($this->input->post('classdescription')) != '') {
                 $classdata['class_description'] = trim($this->input->post('classdescription'));
            }
		
			$checkclassname = $this->school_classes_model->check_class_name($branch_id, $class_name, $school_id);
			 $countClasses = count($checkclassname);
			 
			 if( $countClasses > 0 ) {
				  $this->session->set_flashdata('error', 'Class name already exists. Class has not been added.');
				  redirect(base_url()."schoolclasses/");exit;
			 } else {
			
		   if($this->school_classes_model->insertClasses($classdata)) {
			   $this->session->set_flashdata('success', 'Class has been added successfuly.');
			   redirect(base_url()."schoolclasses/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Class has not been added.');
				redirect(base_url()."schoolclasses/");exit;
		      }
			}
			
        }
		
		
		public function updateclass(){
			
			  $start						    = $this->uri->segment(3);
		      $editedClassid					= $this->uri->segment(4); 
			  $Action					= $this->uri->segment(5); 
		 
		  $postURL	= $this->createPostURL($_GET);
		if($editedClassid==NULL){
			$this->session->set_flashdata('error', 'Select student first.');
			redirect(base_url()."schoolclasses");exit;
		   }
		   
		
		   
		 $classdata 		= array();
         $school_id = $this->session->userdata('user_school_id');
		 $where							= array('class_id'=>$editedClassid);
		 $data['title']	      			= "Edit Class";
		 $data['mode']					= "Edit";
			
			if (trim($this->input->post('schoolbranch')) != '') {
                 $classdata['branch_id'] = trim($this->input->post('schoolbranch'));
				 $branch_id = trim($this->input->post('schoolbranch'));
            }
			
			if (trim($this->input->post('classname')) != '') {
                $classdata['class_name'] = trim($this->input->post('classname'));
				$class_name = trim($this->input->post('classname'));
            }
			
			if (trim($this->input->post('classdescription')) != '') {
                $classdata['class_description'] = trim($this->input->post('classdescription'));
            }
			
			$classdata['updated_date'] = date('Y-m-d H:i:s');
			
			 $checkclassname = $this->school_classes_model->check_editedclass_name($branch_id, $class_name, $school_id, $editedClassid);
			 $countClasses = count($checkclassname);
			 
			 if( $countClasses > 0 ) {
				  $this->session->set_flashdata('error', 'Class name already exists. Class has not been updated.');
				  redirect(base_url()."schoolclasses/index/$start/$editedClassid/edit/");exit;
			 } else {
			
		   if($this->school_classes_model->updateClasses($classdata,$where)) {
			   $this->session->set_flashdata('success', 'Class has been updated successfuly.');
			   redirect(base_url()."schoolclasses/index/$start/$editedClassid/edit/"); exit;
		   } else {
			   $this->session->set_flashdata('error', 'Some problem exists. Class has not been updated.');
				redirect(base_url()."schoolclasses/index/$start/$editedClassid/edit/");exit;
		      }
			}
		

	   }
	   
	   
	   	function deactivateclass(){
		
		  $start						    = $this->uri->segment(3);
		  $classid							= $this->uri->segment(4); 
		$where		= array('class_id'=>$classid);
		$data		= array('is_active'=>'0');
		if($this->school_classes_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Class has been updated successfuly.');
			redirect(base_url()."schoolclasses/index/$start/$classid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Class has not been updated.');
			redirect(base_url()."schoolclasses/index/$start/$classid");exit;
		}
		
	}
	function activateclass(){
		 
		  $start						    = $this->uri->segment(3);
		  $classid							= $this->uri->segment(4); 
			  
		$where		= array('class_id'=>$classid);
		$data		= array('is_active'=>'1');
		if($this->school_classes_model->update($data,$where)){
			$this->session->set_flashdata('success', 'Class has been updated successfuly.');
			redirect(base_url()."schoolclasses/index/$start/$classid");exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Class has not been updated.');
			redirect(base_url()."schoolclasses/index/$start/$classid");exit;
		}
		
	}
	
	
		public function deleteclass($classid){
			
			 $school_id = $this->session->userdata('user_school_id');
			 
			 $class_data_rows = $this->school_classes_model->check_deletedclass($school_id,$classid);
			 
			 $branch_id = $class_data_rows->branch_id;
			 
			 $chk_student_cls = $this->school_classes_model->check_student_class($school_id,$branch_id,$classid);
			
			 $countStudentclass = count($chk_student_cls); 
			 			
			   if( $countStudentclass > 0 ) {
				  $this->session->set_flashdata('error', 'Class has not been deleted. As this class is assigned to students');
				  redirect(base_url()."schoolclasses/index/");exit;
				 } else {
			
		 		 if($this->school_classes_model->delete($classid)){
					$this->session->set_flashdata('success', 'Class has been deleted successfuly.');
					redirect(base_url()."schoolclasses/index");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."schoolclasses/index");exit;
		          }
			  }

	    }
	
		function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}
	
	
}