<?php

if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Attendancereport_model extends CI_Model {

	

	var $tbl	= "attendance";

	var $users = "users";

	var $student = "student";

	var $branch = "branch";

	var $classes = "classes";

	var $staff = "staff";
    
	var $attendance_archive = 'attendance_archive';
	

	

    function __construct() {

        parent::__construct();

    }

	
	function getstaff_branches($staff_branch_ids) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id"=>$staff_branch_ids,"is_active"=>'1',"is_deleted"=>'0'));
        $query = $this->db->get();
      //  echo $this->db->last_query();
        return  $query->row();
    }
	
	function getbranches($school_id) {

        $this->db->select('*');

        $this->db->from($this->branch);

		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->result();

    }  

	 

	 

 	function getclasses($school_id,$schoolbranch) {

        $this->db->select('*');

        $this->db->from($this->classes);

		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$schoolbranch,"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->result();

    }

	  

	function getRows($where=NULL,$startdate,$enddate){

		$this->db->select('*');

		$this->db->group_by('student_id');

		if($where != NULL){

			$this->db->where($where);

		}

	 if($startdate != NULL && $enddate!= NULL){

			$this->db->where('attendance_date >=', $startdate);

			$this->db->where('attendance_date <=', $enddate);

		  }

		$query = $this->db->get($this->tbl);

		 $this->db->last_query();

		return $query->num_rows();

	}

	  

	  function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$startdate,$enddate){

		$this->db->select('*');

		$this->db->group_by('student_id');

		if($where != NULL){

			$this->db->where($where);

		}

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where('attendance_date >=', $startdate);

			$this->db->where('attendance_date <=', $enddate);

		  }

		$this->db->order_by("attendance_id", "asc"); 	

		$this->db->limit($limit, $start);

		$query = $this->db->get($this->tbl);

		$this->db->last_query();

		return $query->result();

	  }

	   

	   

	   function get_total_Attendancestatus($studentid,$startdate,$enddate) {

		     $this->db->select('*');

		     $this->db->from($this->tbl);

			 $this->db->where('student_id', $studentid);

			 $this->db->where('attendance_date >=', $startdate);

			 $this->db->where('attendance_date <=', $enddate);

			 $query = $this->db->get();

			  $this->db->last_query();

             return  $query->num_rows();

	         }

			 

	

	 function get_present_Attendancestatus($studentid,$startdate,$enddate) {

		     $this->db->select('*');

		     $this->db->from($this->tbl);

			 $this->db->where('student_id', $studentid);

			 $this->db->where('attendance_status', '1');

			 $this->db->where('attendance_date >=', $startdate);

			 $this->db->where('attendance_date <=', $enddate);

			 $query = $this->db->get();

			  $this->db->last_query();

             return  $query->num_rows();

	         }

	  

	   function get_absent_Attendancestatus($studentid,$startdate,$enddate) {

		     $this->db->select('*');

		     $this->db->from($this->tbl);

			 $this->db->where('student_id', $studentid);

			 $this->db->where('attendance_status', '2');

			 $this->db->where('attendance_date >=', $startdate);

			 $this->db->where('attendance_date <=', $enddate);

			 $query = $this->db->get();

			  $this->db->last_query();

             return  $query->num_rows();

	         } 

			 

	 function get_late_Attendancestatus($studentid,$startdate,$enddate) {

		     $this->db->select('*');

		     $this->db->from($this->tbl);

			 $this->db->where('student_id', $studentid);

			 $this->db->where('attendance_status', '3');

			 $this->db->where('attendance_date >=', $startdate);

			 $this->db->where('attendance_date <=', $enddate);

			 $query = $this->db->get();

			  $this->db->last_query();

             return  $query->num_rows();

	         } 

	   

	   function  getstudentinfo($studentid) {

        $this->db->select('*');

        $this->db->from($this->student);

		$this->db->where('student_id', $studentid);

        $query = $this->db->get();

        return  $query->row();

	   }

	   
   	 function chk_archive_filename($filename) {
        $this->db->select('*');
        $this->db->from($this->attendance_archive);
		$this->db->where('archive_filename', $filename);
        $query = $this->db->get();
        return  $query->result();
     }
	 
	 	function insertarchiveReport($archivedata){
		if($this->db->insert($this->attendance_archive, $archivedata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	 }
  	 /************************************************************EXCEL FUNCTION**************************************************************************************/


	  function getExcelData($where=NULL,$odr=NULL,$dirc=NULL,$startdate,$enddate){

		$this->db->select('*');

		$this->db->group_by('student_id');

		if($where != NULL){

			$this->db->where($where);

		}

		if($startdate != NULL && $enddate!= NULL){

			$this->db->where('attendance_date >=', $startdate);

			$this->db->where('attendance_date <=', $enddate);

		  }

		$this->db->order_by("attendance_id", "asc"); 	

		$query = $this->db->get($this->tbl);

		$this->db->last_query();

		return $query->result();

	  }


}







 

