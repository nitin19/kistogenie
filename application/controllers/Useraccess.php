<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Useraccess extends CI_Controller {
	 var $logmode;
	function __construct(){
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','useraccess_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
    }

	public function index() {
	     
		$word_search_val	 = isset($_GET['seachword'])?$_GET['seachword']:NULL;
		$search_condition['users.user_type']	= 'teacher';
		$search_condition['users.is_active']	= '1';
		$search_condition['users.is_deleted']	= '0';
		$word_search=trim($word_search_val);
		$data							= array();
		
		$school_id		= $this->session->userdata('user_school_id');
		$data['branches'] = $this->useraccess_model->getbranches($school_id);
		
		$data['title']	      			= "User access";
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		$perpage						= 10;		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'useraccess/index';
	    $config['total_rows'] 			= $this->useraccess_model->getRows($search_condition,$school_id,$word_search);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = "/?seachword=$word_search"; 
		
		$this->pagination->initialize($config);
		
		 $odr =   "staff.user_id";
		 $dirc	= "desc";
		
		$data_rows= $this->useraccess_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$word_search);
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['word_search']		    = $word_search;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		 
	   
		 $this->load->view('header');
		 $this->load->view('useraccess/user_access_view', $data);
		 $this->load->view('footer');
	}
	
		  
	  public function add_user_access() {
		  	 $accessdata = array();
			 $school_id =  $_POST['school_id'];
			 $accessuserId =  $_POST['accessuserId'];
			 $accessMenuName =  $_POST['accessMenuName'];
			 $accessMenuvalue =  $_POST['accessMenuvalue'];
			
			$accessdata['school_id'] = $_POST['school_id'];
			$accessdata['user_id'] = $_POST['accessuserId'];
			$accessdata[$accessMenuName] = $_POST['accessMenuvalue'];
			$accessdata['is_active'] = '1';	
	        $accessdata['created_date'] = date('Y-m-d H:i:s');	
			$updated_date = date('Y-m-d H:i:s');		
			
			$chackaccess = $this->useraccess_model->check_user_access($school_id,$accessuserId);
			$countuseraccess = count($chackaccess);
			 if( $countuseraccess > 0 ) {
		         $where		= array('school_id'=>$school_id,'user_id'=>$accessuserId);
		         $data		= array($accessMenuName=>$accessMenuvalue,'updated_date'=>$updated_date);
		       	 if($this->useraccess_model->updateAccessmenu($data,$where)){
					echo 'Success';
		        	  } else {
					echo 'Fail';
		             }
				 
			  } else { 
			 		 if($this->useraccess_model->insertAccessmenu($accessdata)) {
					   	 echo 'Success';
			    	 } else {
						 echo 'Fail';
			     		 }
			 	 }
			
			}
			
	  	 public function update_staff_branch() {
		  	 $staffdata = array();
			 $school_id =  $_POST['school_id'];
			 $userid =  $_POST['userid'];
			 $branch_id =  $_POST['branch_id'];
			 $actionValue =  $_POST['actionValue'];
			
			$staffInfo = $this->useraccess_model->get_staff_info($school_id,$userid);
			  
			  if( $actionValue=='add' ) {
			      if($staffInfo->branch_id!='') {
				        $branchValue = $staffInfo->branch_id.','.$branch_id;
			           } else {
						  $branchValue = $branch_id;
			             }
			        } else {
					 if($staffInfo->branch_id!='') {
				           $branchValueArr = explode(',',$staffInfo->branch_id);
						  $key = array_search($branch_id, $branchValueArr);
							if (false !== $key) {
								unset($branchValueArr[$key]);
							}
							
							$branchValue = implode(',',$branchValueArr);
			           } 
			       }
				   
		         $where		= array('school_id'=>$school_id,'user_id'=>$userid);
		         $data		= array('branch_id'=>$branchValue);
		       	 if($this->useraccess_model->updateStaff($data,$where)){
					echo 'Success';
		        	  } else {
					echo 'Fail';
		             }

			}
       
}

