<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Forgotpassword extends CI_Controller {



	   function __construct() {

        parent::__construct();

		$this->load->model(array('login_model','authorization_model'));

    }

	

	public function index()	{

		$data = array();

		$this->form_validation->set_rules('useremail', 'E-mail', 'trim|required|valid_email|xss_clean|callback_email_check');

        if ($this->form_validation->run() == TRUE){

			$useremail				= $this->input->post('useremail');

			$where					= array("email"=>$useremail);				

			$user_info				= $this->login_model->getData($where,NULL);

			$user_info				= $user_info[0];

			$this->updateAdminPassword($user_info);

		}

		

		$data['error']		= $this->session->flashdata('error');

		$data['success']	= $this->session->flashdata('success');

		$this->load->view('forgotpassword',$data);

	}

	 

	public function email_check($str)	{

		if( $this->authorize->validate_user_email($str) == FALSE ){

			$this->form_validation->set_message('email_check', 'The given E-mail doesn\'t exsits.');

			return FALSE;

		} else {

			return TRUE;

		}

	}

	

	private function updateAdminPassword($user_info){

			$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';

			$string = '';

			for ($i = 0; $i < 8; $i++) {

				$string .= $characters[rand(0, strlen($characters) - 1)];

			}

			$pass			= ($string);

			$data			= array('password'=>$pass);

			$where		= array('email'=>$user_info->email);

			$this->login_model->update($data,$where);

			

			$to 					 = $user_info->email;

			$subject				 = "Reset Password";

			$message				 = "<font face='arial'>";

			//$message				.= "Hello ".$user_info->fname." ".$user_info->lname.",<br />";
			
			$message				.= "Hello ".$user_info->username.",<br />";

			$message				.= "We have recieved your password rest request and generated a random password.<br />";

			$message				.= "Please use this password to login. You can change your password after login.<br />";

			$message				.= "<b>".$string."</b><br />";

			

			$message				.= "</font>";

			

			$this->authorize->send_email($to,$message,$subject);

			

			$this->session->set_flashdata('success', 'New Password has sent to your email. Please check your email.');

			redirect(base_url().'forgotpassword');

	}

	

}

