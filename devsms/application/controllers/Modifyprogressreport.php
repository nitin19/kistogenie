<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Modifyprogressreport extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','modifyprogressreport_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{
		$data							= array();
		$student_id = 	 $this->uri->segment(3); 
		$data['studentinfo'] = $this->modifyprogressreport_model->getStudentinfo($student_id);
		$school_id = $data['studentinfo']->school_id;
	
	    $data['terms'] = $this->modifyprogressreport_model->getTerms($school_id);
		
		$data['p_term_id']	  	    = $this->uri->segment(4);
		$data['p_year']	      	    = $this->uri->segment(5);
		
		$this->load->view('header');
		$this->load->view('progressreport/modify_progress_report', $data);
		$this->load->view('footer');

	}
	
	function get_crntlvl_sublevels(){
	  $curnt_lvl_id		=	trim($this->input->post('curnt_lvl_id'));
	  $subLevels 		= 	$this->modifyprogressreport_model->get_curentlvl_sublevels($curnt_lvl_id);
	  $HTML = '';
       if( count(array_filter($subLevels)) > 0 ) {
		   foreach($subLevels as $sublevel) {
				$HTML.= '<option value="'.$sublevel->sublevel_id.'">'.$sublevel->sublevel_name.'</option>';
		   }
	    }
	  echo $HTML;	
	}
	
	function get_templatedesc(){
		
	$student_id		=	$this->input->post('student_id');	
	
	$studentinfo	= 	$this->modifyprogressreport_model->getStudentinfo($student_id);

	$tmpltid		=	$this->input->post('template_id');

	$catid			=	$this->input->post('cat_id');
		
	$result 		= 	$this->modifyprogressreport_model->get_template_decs($tmpltid,$catid);
	
	$description	=	$result[0]->description;

		
	$description 	= 	str_replace ("{first_name}", $studentinfo->student_fname,$description);
	$description 	= 	str_replace ("{last_name}", $studentinfo->student_lname,$description);
	
	$school_id		=	$studentinfo->school_id;
	$school_name	= 	$this->modifyprogressreport_model->getSchoolinfo($school_id);

	$description 	= 	str_replace ("{school_name}", $school_name[0]->school_name,$description);
	
	$branch_id		=	$studentinfo->student_school_branch;
	$branch_name	= 	$this->modifyprogressreport_model->getBranchinfo($branch_id);

	$class_id		=	$studentinfo->student_class_group;
	$class_name		= 	$this->modifyprogressreport_model->getClassinfo($class_id);


	$description 	= 	str_replace ("{branch_name}", $branch_name[0]->branch_name,$description);
	 if($studentinfo->student_gender == 'Female'){

	if (strpos($description,'{his/her}') > 0) {
	 		$pronoun = 'her';
		}else {
			$pronoun='Her';
		}

	if (strpos($description,'{gender}') > 0 ) {

		$var= strpos($description,'{gender}');

	 		$gender='she';
		}else {
			$gender='She';
		}
	if (strpos($description,'{Gender}') > 0 ) {
$var1= strpos($description,'{gender}');
	 		$gender1='she';
		}else {
			$gender1='She';
		}
	$pronoun1 = 'her';
	 
	 }else{

	 	if (strpos($description,'{his/her}') > 0) {
	 		 $pronoun = 'his';
		}else {
			 $pronoun = 'His';
		}
	 	if (strpos($description,'{gender}') > 0) {
	 		$gender='he';
		}else {
			$gender='He';
		}
		if (strpos($description,'{Gender}') > 0 ) {

	 		$gender1='he';
		}else {
			$gender1='He';
		}
		$pronoun1 = 'him';

	 }
	$description 	= 	str_replace ("{gender}", $gender,$description);
	$description 	= 	str_replace ("{Gender}", $gender1,$description);
	$description 	= 	str_replace ("{his/her}", $pronoun,$description);
	$description 	= 	str_replace ("{him/her}", $pronoun1,$description);
	$description 	= 	str_replace ("{father_name}", $studentinfo->student_father_name,$description);
	$description 	= 	str_replace ("{mother_name}", $studentinfo->student_mother_name,$description);
	$description 	= 	str_replace ("{main_telephone}", $studentinfo->student_telephone,$description);
	$description 	= 	str_replace ("{email_id}", $studentinfo->user_id,$description);
	$description 	= 	str_replace ("{date_of_birth}", $studentinfo->student_dob,$description);
	$description 	= 	str_replace ("{father_mobile}", $studentinfo->student_father_mobile,$description);
	$description 	= 	str_replace ("{mother_mobile}", $studentinfo->student_mother_mobile,$description);
	$description 	= 	str_replace ("{father_email}", $studentinfo->student_father_email,$description);
	$description 	= 	str_replace ("{mother_email}", $studentinfo->student_mother_email,$description);
	$description 	= 	str_replace ("{father_address}", $studentinfo->student_father_address,$description);
	$description 	= 	str_replace ("{mother_address}", $studentinfo->student_mother_address,$description);
	$description 	= 	str_replace ("{child_class}",$class_name->class_name ,$description);
	$description 	= 	str_replace ("{enrolment_date}", $studentinfo->student_enrolment_date,$description);
	
    $reponse = $description;

	   if($catid=='2') {
		   $surahInfo	= 	$this->modifyprogressreport_model->get_surah_name($tmpltid);
		   echo $studentinfo->student_fname.' '.$studentinfo->student_lname.' is currently learning the surah '.$surahInfo->sublevel_name.'.';
	     }else if($catid=='9'){
		   $surahInfo	= 	$this->modifyprogressreport_model->get_surah_name($tmpltid);
		   echo $studentinfo->student_fname.' '.$studentinfo->student_lname.' is currently learning the concept of '.$surahInfo->sublevel_name.'.';
	     }else {
	   
    $reponse = preg_replace_callback('/[.!?].*?\w/', create_function('$matches', 'return strtoupper($matches[0]);'),$reponse);

    echo $reponse;	
	    }
	}
	
	public function check_term()
	{     
			$student_id = $_POST['student_id'];
			$school_id = $_POST['school_id'];
			$branch_id = $_POST['branch_id'];
			$class_id = $_POST['class_id'];
			$term = $_POST['termid'];
			$year = $_POST['year'];
			$getreports = $this->modifyprogressreport_model->check_progress_report($student_id,$school_id,$branch_id,$class_id,$term,$year);
	 	 	$report_tmplt = $this->modifyprogressreport_model->report_template(); 


		if( count($getreports) > 0 ) { 
			 $HTML = ''; 
		   foreach($getreports as $report) {
			  $subjectid =  $report->subject_id;
			  $subjectname =  $this->modifyprogressreport_model->get_subject_name($subjectid,$school_id,$branch_id,$class_id);
			  $subjectname->subject_name;
			   
			  
$HTML.= '<input type="hidden" name="subject_id[]" value="'.$report->subject_id.'" />';

$HTML.= '<div class="col-sm-12 nopadding">';
$HTML.= '<div class="col-sm-12 prfltitlediv titlediv">';
$HTML.= '<h1>'.$subjectname->subject_name.'</h1>';
$HTML.= '</div>';
   
$HTML.= '<div class="profile-bg report_cls">';

 /*******************************Teacher dropdown for subjects 10-jan-2018 *************************/

$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Teacher for ' .$subjectname->subject_name.'</label>';

$HTML.= '<div class="col-sm-9 inputbox ">';


$subject_id = $report->subject_id;

$staffs = $this->modifyprogressreport_model->get_staff($subject_id);

if(count(array_filter($staffs)) > 0 ) {

$HTML.= '<select class="form-control" name="teacher['.$report->subject_id.']" id="teacher_'.$report->subject_id.'">';
foreach ($staffs as $staff) {

$HTML.= '<option value="'.$staff->staff_id.'"';
if($report->staff_id == $staff->staff_id){
	
	$HTML.= 'selected';

}

$HTML.= '>'.$staff->staff_fname.' '.$staff->staff_lname.'</option>';

  }

$HTML.= '</select>';
}else{

$HTML.= '<span> No teacher assigned for this subject.</span>';

}


$HTML.= '</div>';
$HTML.= '</div>';

 /*******************************Teacher dropdown for subjects 10-jan-2018 *************************/
  
$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Progress</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="effort['.$report->subject_id.']" id="effort_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Exceeding Target"';
 if($report->effort == 'Exceeding Target'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Exceeding Target</option>';
 
$HTML.= '<option value="On Target"';
if($report->effort == 'On Target'){ 
$HTML.= ' selected ';
 }
$HTML.= '>On Target</option>';

$HTML.= '<option value="Below Target"';
if($report->effort == 'Below Target'){
$HTML.= ' selected ';
}
$HTML.= '>Below Target</option>';


$HTML.= '<option value="Significantly Below Target"';
if($report->effort == 'Significantly Below Target'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Significantly Below Target</option>';
  
$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';

$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Behaviour</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="behaviour['.$report->subject_id.']" id="behaviour_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->behaviour == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->behaviour == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->behaviour == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->behaviour == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Homework</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="homework['.$report->subject_id.']" id="homework_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->home_work == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->home_work == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->home_work == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->home_work == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';

/*$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Proof reading</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="proofreading['.$report->subject_id.']" id="proofreading_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Incomplete"';
 if($report->proof_reading == 'Incomplete'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Incomplete</option>';
 
$HTML.= '<option value="Amendments Required"';
if($report->proof_reading == 'Amendments Required'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Amendments Required</option>';

$HTML.= '<option value="Approved by Teacher"';
if($report->proof_reading == 'Approved by Teacher'){
$HTML.= ' selected ';
}
$HTML.= '>Approved by Teacher</option>';


$HTML.= '<option value="Complete"';
if($report->proof_reading == 'Complete'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Complete</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';
*/
/***************** New level for reports 26-dec-2017*****************************************/

if($subjectname->subject_name =="Memorisation" || $subjectname->subject_name =="Reading"){

$sub_name = $subjectname->subject_name ;

$reading_level = $this->modifyprogressreport_model->report_level($sub_name);


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<div class="col-sm-4 nopadding">';

$HTML.= '<label class=" col-sm-3 control-label nopadding">Start level</label>';

$HTML.= '<div class=" col-sm-8 inputbox">';

$HTML.= '<select class="form-control" name="startlevel['.$report->subject_id.']" id="startlevel_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

foreach($reading_level as $readinglevel){

$HTML.= '<option value="'.$readinglevel->level_name.'"';

if($report->start_level == $readinglevel->level_name){ 

$HTML.= 'selected'; 

}


$HTML.= '>'.$readinglevel->level_name.'</option>';

}

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="col-sm-4 nopadding">';

$HTML.= '<label class="col-sm-4 control-label nopadding">Current level</label>';

$HTML.= '<div class="col-sm-8 inputbox">';

$HTML.= '<select class="form-control mycustomLevels" name="currentlevel['.$report->subject_id.']" id="currentlevel_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

foreach($reading_level as $readinglevel){

$HTML.= '<option value="'.$readinglevel->level_id.'"';

if($report->current_level == $readinglevel->level_id){ 

$HTML.= 'selected'; 

}


$HTML.= '>'.$readinglevel->level_name.'</option>';

}

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="col-sm-4 nopadding">';

$HTML.= '<label class=" col-sm-4 control-label nopadding">Target level</label>';

$HTML.= '<div class="col-sm-8 inputbox">';

$HTML.= '<select class="form-control" name="targetlevel['.$report->subject_id.']" id="targetlevel_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

foreach($reading_level as $readinglevel){

$HTML.= '<option value="'.$readinglevel->level_name.'"';

if($report->target_level == $readinglevel->level_name){ 

$HTML.= 'selected'; 

}


$HTML.= '>'.$readinglevel->level_name.'</option>';

}

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';

$HTML.= '</div>';
}


/******************************* End of new levels ****************************************/

$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-2 control-label nopadding">Account of Achievement</label>';

$HTML.= '<div class="col-sm-10 inputbox termselect">';
$HTML.= '<div class="col-sm-12 inputbox termselect aoa_selectbox">';

$cat_subject_name = $subjectname->subject_name;

$cat_name ='AoA';

$categorys = $this->modifyprogressreport_model->get_subcategory($cat_subject_name,$cat_name);


if(count(array_filter($categorys)) > 0 ) {
foreach ($categorys as $category) {

$HTML.= '<select class="list_AoA';
if($category->dropdown_name=='Current surah') {
  } else {
	$HTML.= ' livesearch';
  }
if($category->dropdown_name=='Current surah') {
	$HTML.= ' currentlevel_'.$report->subject_id;
	}
$HTML.= '" id="'.$category->dropdown_id.'" name="'.$category->dropdown_name.'['.$report->subject_id.']" subject-value="'.$report->subject_id.'"';
if($category->dropdown_name=='Current surah') {
	$HTML.= ' disabled="true"';
	}
$HTML.= '>';
$HTML.= '<option value="">'.$category->dropdown_name.'</option>';
$subcategorys = $this->modifyprogressreport_model->under_subcategory($category->dropdown_id,$subjectname->subject_name);
if(count($subcategorys)>0){
foreach ($subcategorys as $subcategory ) {
	$HTML.= '<option value="'.$subcategory->template_id.'">'.$subcategory->title.'</option>';
 }
}
$HTML.= '</select>';

$HTML.= '<input type="hidden" name="field'.$category->dropdown_id.'_'.$report->subject_id.'" id="field'.$category->dropdown_id.'_'.$report->subject_id.'" value="">';

   }
}


$HTML.= '</div>';

$HTML.= '<div class="col-sm-12 inputbox nopadding">';

$HTML.= '<textarea class="form-control temp_desc" rows="3" name="account_of_achievement['.$report->subject_id.']" id="list1_'.$report->subject_id.'" maxlength="450" >'.$report->account_of_achievement.'</textarea>';


$HTML.= '</div>';

$HTML.= '</div>';
$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-2 control-label nopadding">Area of Development</label>';

$HTML.= '<div class="col-sm-10 inputbox termselect">';
$HTML.= '<div class="col-sm-12 inputbox termselect aod_selectbox">';

$cat_subject_name = $subjectname->subject_name;

$cat_name ='AoD';

$categorys = $this->modifyprogressreport_model->get_subcategory($cat_subject_name,$cat_name);

if(count(array_filter($categorys)) > 0 ) {
foreach ($categorys as $category ) {

$HTML.= '<select class="list_AoD" id="'.$category->dropdown_id.'" name="'.$category->dropdown_name.'['.$report->subject_id.']" subject-value="'.$report->subject_id.'">';
$HTML.= '<option value="">'.$category->dropdown_name.'</option>';
$subcategorys = $this->modifyprogressreport_model->under_subcategory($category->dropdown_id,$subjectname->subject_name);
if(count($subcategorys)>0){
foreach ($subcategorys as $subcategory ) {
	$HTML.= '<option value="'.$subcategory->template_id.'">'.$subcategory->title.'</option>';
 }
}
$HTML.= '</select>';

$HTML.= '<input type="hidden" name="field'.$category->dropdown_id.'_'.$report->subject_id.'" id="field'.$category->dropdown_id.'_'.$report->subject_id.'" value="">';

   }
}


$HTML.= '</div>';

$HTML.= '<div class="col-sm-12 inputbox nopadding">';

$HTML.= '<textarea class="form-control" rows="3" name="area_development['.$report->subject_id.']" id="list2_'.$report->subject_id.'" maxlength="450">'.$report->area_development.'</textarea>';

$HTML.= '</div>';

$HTML.= '</div>';
$HTML.= '</div>';

/***************** Topic covered only for Islamic Studies 28-dec-2017*****************************************/

if($subjectname->subject_name =="Islamic Studies" || $subjectname->subject_name =="Islamic studies"){

$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-2 control-label nopadding">Topic Covered</label>';

$HTML.= '<div class="col-sm-10 inputbox termselect">';

$HTML.= '<div class="col-sm-12 inputbox termselect aoa_selectbox">';

$cat_subject_name = $subjectname->subject_name;

$cat_name ='ToC';

$categorys = $this->modifyprogressreport_model->get_subcategory($cat_subject_name,$cat_name);

if(count(array_filter($categorys)) > 0 ) {
foreach ($categorys as $category ) {

$HTML.= '<select class="list_ToC" id="'.$category->dropdown_id.'"  name="'.$category->dropdown_name.'['.$report->subject_id.']" subject-value="'.$report->subject_id.'">';
$HTML.= '<option value="">'.$category->dropdown_name.'</option>';
$subcategorys = $this->modifyprogressreport_model->under_subcategory($category->dropdown_id,$subjectname->subject_name);
if(count($subcategorys)>0){
foreach ($subcategorys as $subcategory ) {
	$HTML.= '<option value="'.$subcategory->template_id.'">'.$subcategory->title.'</option>';
 }
}
$HTML.= '</select>';

$HTML.= '<input type="hidden" name="field'.$category->dropdown_id.'_'.$report->subject_id.'" id="field'.$category->dropdown_id.'_'.$report->subject_id.'" value="">';

   }
}

$HTML.= '</div>';

$HTML.= '<div class="col-sm-12 inputbox nopadding">';

$HTML.= '<textarea class="form-control" rows="3" name="topic_covered['.$report->subject_id.']" id="list3_'.$report->subject_id.'" maxlength="450">'.$report->topic_covered.'</textarea>';


$HTML.= '</div>';

$HTML.= '</div>';

/*$HTML.= '<label class="col-sm-3 control-label nopadding">Topic Covered</label>';

$HTML.= '<div class="col-sm-9 inputbox nopadding">';

$HTML.= '<textarea class="form-control" rows="3" name="topic_covered['.$report->subject_id.']" id="list3_'.$report->subject_id.'">'.$report->topic_covered.'</textarea>'.'';

$HTML.= '</div>';*/

$HTML.= '</div>';


}

/******************************* Topic covered only for Arabic language****************************************/



$HTML.= '</div>';
  
$HTML.= '</div>';

			 
		 }	
		 
		 $HTML.= '<div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-6 nopadding">

        <div class="cancellink">

       <!-- <input type="button" value="Cancel">-->

        </div>

        </div>

        <div class="col-sm-6 nopadding">

        <div class="confirmlink">

        <input type="submit" name="modifyprogressreportBtn" id="modifyprogressreportBtn" value="Confirm Modification">

        </div>

        </div>

        </div>';
			
			echo $HTML; 
		
		 }  else {
			 
	$subjects_Info = $this->modifyprogressreport_model->getSubjects($school_id,$branch_id,$class_id);	
	if(count($subjects_Info) > 0) { 
	
	$HTML = ''; 
	
foreach($subjects_Info as $subjectinfo) {
		 
$HTML.= '<input type="hidden" name="subject_id[]" value="'.$subjectinfo->subject_id.'" />';

$HTML.= '<div class="col-sm-12 nopadding">';
$HTML.= '<div class="col-sm-12 prfltitlediv titlediv">';
$HTML.= '<h1>'.$subjectinfo->subject_name.'</h1>';
$HTML.= '</div>';
        
$HTML.= '<div class="profile-bg report_cls">';


 /*******************************Teacher dropdown for subjects 10-jan-2018 *************************/

$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Teacher for ' .$subjectinfo->subject_name.'</label>';

$HTML.= '<div class="col-sm-9 inputbox ">';


$subject_id = $subjectinfo->subject_id;

$staffs = $this->modifyprogressreport_model->get_staff($subject_id);

if(count(array_filter($staffs)) > 0 ) {

$HTML.= '<select class="form-control" name="teacher['.$subjectinfo->subject_id.']" id="teacher_'.$subjectinfo->subject_id.'">';
foreach ($staffs as $staff) {

$HTML.= '<option value="'.$staff->staff_id.'">'.$staff->staff_fname.' '.$staff->staff_lname.'</option>';

  }

$HTML.= '</select>';
}else{

$HTML.= '<span> No teacher assigned for this subject.</span>';

}

$HTML.= '</div>';
$HTML.= '</div>';

 /*******************************Teacher dropdown for subjects 10-jan-2018 *************************/
  
$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Progress</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="effort['.$subjectinfo->subject_id.']" id="effort_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Exceeding Target">Exceeding Target</option>';
 
$HTML.= '<option value="On Target">On Target</option>';

$HTML.= '<option value="Below Target">Below Target</option>';

$HTML.= '<option value="Significantly Below Target">Significantly Below Target</option>';
  
$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Behaviour</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="behaviour['.$subjectinfo->subject_id.']" id="behaviour_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory">Satisfactory</option>';
 
$HTML.= '<option value="Good">Good</option>';

$HTML.= '<option value="Very Good">Very Good</option>';

$HTML.= '<option value="Excellent">Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Homework</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="homework['.$subjectinfo->subject_id.']" id="homework_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory">Satisfactory</option>';
 
$HTML.= '<option value="Good">Good</option>';

$HTML.= '<option value="Very Good">Very Good</option>';

$HTML.= '<option value="Excellent">Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


/*$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Proof reading</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="proofreading['.$subjectinfo->subject_id.']" id="proofreading_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Incomplete">Incomplete</option>';
 
$HTML.= '<option value="Amendments Required">Amendments Required</option>';

$HTML.= '<option value="Approved by Teacher">Approved by Teacher</option>';

$HTML.= '<option value="Complete">Complete</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';

*/
/***************** New level for reports 26-dec-2017*****************************************/

if($subjectinfo->subject_name =="Memorisation" || $subjectinfo->subject_name =="Reading"){

$sub_name = $subjectinfo->subject_name ;

$reading_level = $this->modifyprogressreport_model->report_level($sub_name);


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-2 control-label nopadding">Start level</label>';

$HTML.= '<div class="col-sm-2 inputbox">';

$HTML.= '<select class="form-control" name="startlevel['.$subjectinfo->subject_id.']" id="startlevel_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

foreach($reading_level as $readinglevel){

$HTML.= '<option value="'.$readinglevel->level_name.'">';

$HTML.= $readinglevel->level_name;

$HTML.= '</option>';

}

$HTML.= '</select>';

$HTML.= '</div>';



$HTML.= '<label class="col-sm-2 control-label nopadding">Current level</label>';

$HTML.= '<div class="col-sm-2 inputbox">';

$HTML.= '<select class="form-control mycustomLevels" name="currentlevel['.$subjectinfo->subject_id.']" id="currentlevel_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

foreach($reading_level as $readinglevel){

$HTML.= '<option value="'.$readinglevel->level_id.'">';

$HTML.= $readinglevel->level_name;

$HTML.= '</option>';

}

$HTML.= '</select>';

$HTML.= '</div>';


$HTML.= '<label class="col-sm-2 control-label nopadding">Target level</label>';

$HTML.= '<div class="col-sm-2 inputbox">';

$HTML.= '<select class="form-control" name="targetlevel['.$subjectinfo->subject_id.']" id="targetlevel_'.$subjectinfo->subject_id.'">';

$HTML.= '<option value="">Select</option>';

foreach($reading_level as $readinglevel){

$HTML.= '<option value="'.$readinglevel->level_name.'">';

$HTML.= $readinglevel->level_name;

$HTML.= '</option>';

}

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';
}


/******************************* End of new levels ****************************************/

 
$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-2 control-label nopadding">Account of Achievement</label>';

$HTML.= '<div class="col-sm-10 inputbox termselect">';

$HTML.= '<div class="col-sm-12 inputbox termselect aoa_selectbox">';

$cat_subject_name = $subjectinfo->subject_name;

$cat_name ='AoA';

$categorys = $this->modifyprogressreport_model->get_subcategory($cat_subject_name,$cat_name);

if(count(array_filter($categorys)) > 0 ) {
foreach ($categorys as $category ) {

$HTML.= '<select class="list_AoA';
if($category->dropdown_name=='Current surah') {
  } else {
	$HTML.= ' livesearch';
  }
if($category->dropdown_name=='Current surah') {
	$HTML.= ' currentlevel_'.$subjectinfo->subject_id;
	}

$HTML .='" id="'.$category->dropdown_id.'"  name="'.$category->dropdown_name.'['.$subjectinfo->subject_id.']" subject-value="'.$subjectinfo->subject_id.'"';
if($category->dropdown_name=='Current surah') {
	$HTML.= ' disabled="true"';
	}
$HTML.= '>';

$HTML.= '<option value="">'.$category->dropdown_name.'</option>';
$subcategorys = $this->modifyprogressreport_model->under_subcategory($category->dropdown_id,$subjectinfo->subject_name);
if(count($subcategorys)>0){
foreach ($subcategorys as $subcategory ) {
	$HTML.= '<option value="'.$subcategory->template_id.'">'.$subcategory->title.'</option>';
 }
}
$HTML.= '</select>';

$HTML.= '<input type="hidden" name="field'.$category->dropdown_id.'_'.$subjectinfo->subject_id.'" id="field'.$category->dropdown_id.'_'.$subjectinfo->subject_id.'" value="">';

   }
}


$HTML.= '</div>';

$HTML.= '<div class="col-sm-12 inputbox nopadding">';

$HTML.= '<textarea class="form-control" name="account_of_achievement['.$subjectinfo->subject_id.']" id="list1_'.$subjectinfo->subject_id.'" maxlength="450"></textarea>';


$HTML.= '</div>';

$HTML.= '</div>';

$HTML.= '</div>';

$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-2 control-label nopadding">Area of Development</label>';

$HTML.= '<div class="col-sm-10 inputbox termselect">';

$HTML.= '<div class="col-sm-12 inputbox termselect aod_selectbox">';

$cat_subject_name = $subjectinfo->subject_name;

$cat_name ='AoD';

$categorys = $this->modifyprogressreport_model->get_subcategory($cat_subject_name,$cat_name);

if(count(array_filter($categorys)) > 0 ) {
foreach ($categorys as $category ) {

$HTML.= '<select class="list_AoD" id="'.$category->dropdown_id.'"  name="'.$category->dropdown_name.'['.$subjectinfo->subject_id.']" subject-value="'.$subjectinfo->subject_id.'">';
$HTML.= '<option value="">'.$category->dropdown_name.'</option>';
$subcategorys = $this->modifyprogressreport_model->under_subcategory($category->dropdown_id,$subjectinfo->subject_name);
if(count($subcategorys)>0){
foreach ($subcategorys as $subcategory ) {
	$HTML.= '<option value="'.$subcategory->template_id.'">'.$subcategory->title.'</option>';
 }
}
$HTML.= '</select>';

$HTML.= '<input type="hidden" name="field'.$category->dropdown_id.'_'.$subjectinfo->subject_id.'" id="field'.$category->dropdown_id.'_'.$subjectinfo->subject_id.'" value="">';

   }
}

$HTML.= '</div>';

$HTML.= '<div class="col-sm-12 inputbox nopadding">';

$HTML.= '<textarea class="form-control" rows="3" name="area_development['.$subjectinfo->subject_id.']" id="list2_'.$subjectinfo->subject_id.'" maxlength="450"></textarea>';

$HTML.= '</div>';

$HTML.= '</div>';

$HTML.= '</div>';

/***************** Topic covered only for Islamic Studies 28-dec-2017*****************************************/

if($subjectinfo->subject_name == "Islamic Studies" || $subjectinfo->subject_name == "Islamic studies"){

$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-2 control-label nopadding">Topic Covered</label>';

$HTML.= '<div class="col-sm-10 inputbox termselect">';

$HTML.= '<div class="col-sm-12 inputbox termselect aoa_selectbox">';

$cat_subject_name = $subjectinfo->subject_name;

$cat_name ='ToC';

$categorys = $this->modifyprogressreport_model->get_subcategory($cat_subject_name,$cat_name);

if(count(array_filter($categorys)) > 0 ) {
foreach ($categorys as $category ) {

$HTML.= '<select class="list_ToC" id="'.$category->dropdown_id.'"  name="'.$category->dropdown_name.'['.$subjectinfo->subject_id.']" subject-value="'.$subjectinfo->subject_id.'">';
$HTML.= '<option value="">'.$category->dropdown_name.'</option>';
$subcategorys = $this->modifyprogressreport_model->under_subcategory($category->dropdown_id,$subjectinfo->subject_name);
if(count($subcategorys)>0){
foreach ($subcategorys as $subcategory ) {
	$HTML.= '<option value="'.$subcategory->template_id.'">'.$subcategory->title.'</option>';
 }
}
$HTML.= '</select>';

$HTML.= '<input type="hidden" name="field'.$category->dropdown_id.'_'.$subjectinfo->subject_id.'" id="field'.$category->dropdown_id.'_'.$subjectinfo->subject_id.'" value="">';

   }
}

$HTML.= '</div>';

$HTML.= '<div class="col-sm-12 inputbox nopadding">';

$HTML.= '<textarea class="form-control" rows="3" name="topic_covered['.$subjectinfo->subject_id.']" id="list3_'.$subjectinfo->subject_id.'" maxlength="450">Wuduh, Salah, Seerah of the Prophet Muhammad (pbuh) .</textarea>';

$HTML.= '</div>';

$HTML.= '</div>';


/* $HTML.= '<label class="col-sm-3 control-label nopadding">Topic Covered</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<textarea class="form-control" rows="3" name="topic_covered['.$subjectinfo->subject_id.']" id="list3_'.$subjectinfo->subject_id.'"></textarea>';

$HTML.= '</div>'; */

$HTML.= '</div>';

}


/******************************* Topic covered only for Arabic language****************************************/	 
 
$HTML.= '</div>';
  
$HTML.= '</div>';
		 
	    }
    

$HTML.= '<div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-6 nopadding">

        <div class="cancellink">

        <!--<input type="button" value="Cancel">-->

        </div>

        </div>

        <div class="col-sm-6 nopadding">

        <div class="confirmlink">

        <input type="submit" name="modifyprogressreportBtn" id="modifyprogressreportBtn" value="Confirm Modification">

        </div>

        </div>

        </div>';
		
	 echo $HTML; 
	 
	 } else {
		 $HTML = ''; 
		 $HTML.= '<div class="col-sm-12 nopadding"><div class="profile-bg report_cls"> <h2 style="color:#000"> No subjects found </h2></div></div>';
		 echo $HTML; 
	         }
			 
		 }
		
?>		
	    	 <script>



			$(".livesearch").chosen();
 /************************************************Current Levels Work START**********************************************************************/	
			jQuery(".mycustomLevels").on('change',function(e){
					 e.preventDefault();
				    var curnt_lvl_cl = $(this).attr('id');
					var curnt_lvl_id = $(this).val();
					$('.'+curnt_lvl_cl).prop("disabled", false); 
					$('.'+curnt_lvl_cl).children('option:not(:first)').remove();
				if(curnt_lvl_id!='') {	
					jQuery.ajax({
					type : 'POST',
					url  : '<?php echo base_url(); ?>modifyprogressreport/get_crntlvl_sublevels',
					data:{ 'curnt_lvl_id': curnt_lvl_id },
					success :  function(respdata) {
					           //alert(respdata);
					         jQuery('.'+curnt_lvl_cl).append(respdata);
						    }
				        });
				     } else {
						 $('.'+curnt_lvl_cl).prop("disabled", true); 
					     $('.'+curnt_lvl_cl).children('option:not(:first)').remove();
						}
				}); 

/************************************************Current Levels Work END**********************************************************************/		 
					 
					 
	   			jQuery(".list_AoA").on('change',function(e){
					 e.preventDefault();
				    var cat_id = $(this).attr('id');
					var template_id = $(this).val();
					var sub_id = $(this).attr('subject-value');
					var student_id = <?php echo $student_id; ?>;
					var textarea_value = $('#list1_'+sub_id).val();
					var F1 =  jQuery('#field'+cat_id+'_'+sub_id).val();
														
				if(template_id!='') {	
					jQuery.ajax({
					type : 'POST',
					url  : '<?php echo base_url(); ?>modifyprogressreport/get_templatedesc',
					data:{ 'student_id': student_id,'template_id': template_id,'cat_id': cat_id,'sub_id': sub_id },
					success :  function(data) {
						var dnM_get_Value = '';
						if(textarea_value!='') {
							   if(F1!='') {
								   if(F1 == data) {
								   	    dnM_get_Value = textarea_value;
								      } else {
								      	 if (textarea_value.toLowerCase().includes(F1.toLowerCase())) {
											   dnM_get_Value = textarea_value.replace(F1, data);
											  } else {
												dnM_get_Value =	textarea_value+' '+data;
												
										}
								   }
							  } else {
						        dnM_get_Value =	textarea_value+' '+data;	 
							 }
						  } else {
							  dnM_get_Value =	textarea_value+' '+data;	
						  }
					 
					    jQuery('#field'+cat_id+'_'+sub_id).val(data);
					  
					  jQuery('#list1_'+sub_id).replaceWith('<textarea class="form-control temp_desc " rows="3" name="account_of_achievement['+sub_id+']" id="list1_'+sub_id+'" maxlength="450">'+dnM_get_Value+'</textarea>');
					  
						}
				   });
				   
				     } else {
					return false;
						}
				
				});
				
/*******************************************************************************************************/

				jQuery(".list_AoD").on('change',function(e){
						e.preventDefault();
				    var cat_id = $(this).attr('id');
					var template_id = $(this).val();
					var sub_id = $(this).attr('subject-value');
					var student_id = <?php echo $student_id; ?>;
					var textarea_value = $('#list2_'+sub_id).val();
					var F1 =  jQuery('#field'+cat_id+'_'+sub_id).val();

				if(template_id!='') {	
					jQuery.ajax({
					type : 'POST',
					url  : '<?php echo base_url(); ?>modifyprogressreport/get_templatedesc',
					data:{ 'student_id': student_id,'template_id': template_id,'cat_id': cat_id,'sub_id': sub_id },
					success :  function(data) {
						var dnM_get_Value = '';
						if(textarea_value!='') {
							   if(F1!='') {
								   if(F1 == data) {
								   	    dnM_get_Value = textarea_value;
								      } else {
								      	 if (textarea_value.toLowerCase().includes(F1.toLowerCase())) {
											   dnM_get_Value = textarea_value.replace(F1, data);
											  } else {
												dnM_get_Value =	textarea_value+'• '+data;
												dnM_get_Value +='\n';
												
										}
								   }
							  } else {
						        dnM_get_Value =	textarea_value+'• '+data;
						        dnM_get_Value +='\n';	 
							 }
						  } else {
							  dnM_get_Value =	textarea_value+'• '+data;
							  dnM_get_Value +='\n';	
						  }
					 
					    jQuery('#field'+cat_id+'_'+sub_id).val(data);
					  
					  jQuery('#list2_'+sub_id).replaceWith('<textarea class="form-control temp_desc " rows="3" name="area_development['+sub_id+']" id="list2_'+sub_id+'" maxlength="450">'+dnM_get_Value+'</textarea>');
					  
						}
				   });
				   
				     } else {
					return false;
						}
				   
				});		
			
/**********************************************************************************************/		 
					 
					 
	   			jQuery(".list_ToC").on('change',function(e){
					 e.preventDefault();
				    var cat_id = $(this).attr('id');
					var template_id = $(this).val();
					var sub_id = $(this).attr('subject-value');
					var student_id = <?php echo $student_id; ?>;
					var textarea_value = $('#list3_'+sub_id).val();
					var F1 =  jQuery('#field'+cat_id+'_'+sub_id).val();
									
				if(template_id!='') {	
					jQuery.ajax({
					type : 'POST',
					url  : '<?php echo base_url(); ?>modifyprogressreport/get_templatedesc',
					data:{ 'student_id': student_id,'template_id': template_id,'cat_id': cat_id,'sub_id': sub_id },
					success :  function(data) {
						var dnM_get_Value = '';
						if(textarea_value!='') {
							   if(F1!='') {
								   if(F1 == data) {
								   	    dnM_get_Value = textarea_value;
								      } else {
								      	 if (textarea_value.toLowerCase().includes(F1.toLowerCase())) {
											   dnM_get_Value = textarea_value.replace(F1, data);
											  } else {
												dnM_get_Value =	textarea_value+' '+data;
												
										}
								   }
							  } else {
						        dnM_get_Value =	textarea_value+' '+data;	 
							 }
						  } else {
							  dnM_get_Value =	textarea_value+' '+data;	
						  }
					 
					    jQuery('#field'+cat_id+'_'+sub_id).val(data);
					  
					  jQuery('#list3_'+sub_id).replaceWith('<textarea class="form-control temp_desc " rows="3" name="topic_covered['+sub_id+']" id="list3_'+sub_id+'" maxlength="450">'+dnM_get_Value+'</textarea>');
					  
						}
				   });
				   
				     } else {
					return false;
						}
				});

		
				</script>		
		
<?php		 
	  }
	  	  function check_progress_peport_status() {
	  	  	
	  	  	echo json_encode(array('Status'=>"true"));
	
		/*	$subjectCount =  count(array_filter($this->input->post('subject_id')));
			$effortCount = count(array_filter($this->input->post('effort')));
			$behaviourCount =  count(array_filter($this->input->post('behaviour')));
			$homeworkCount = count(array_filter($this->input->post('homework')));
			//$area_developmentCount =  count(array_filter($this->input->post('area_development')));
			//$account_of_achievementCount = count(array_filter($this->input->post('account_of_achievement')));
			//$topic_coveredCount = count(array_filter($this->input->post('topic_covered')));


	
// if($subjectCount == $effortCount && $subjectCount == $behaviourCount && $subjectCount == $homeworkCount && $subjectCount == $proofreadingCount){
 	if($subjectCount == $effortCount && $subjectCount == $behaviourCount && $subjectCount == $homeworkCount){
		           echo json_encode(array('Status'=>"true"));
			      } else {
			        echo json_encode(array('Status'=>"false"));
				  }*/
			
	  }
	  
	  
	  function update_progressreport() {
		  
		  /*$input	=  $this->input->post();
		  echo '<pre>';
		  print_r($input);
		  echo '</pre>';
		  exit; */ 
		  
		  if ($this->input->post('student_id') != '') {
                 $student_id = trim($this->input->post('student_id'));
            }
			if ($this->input->post('school_id') != '') {
                $school_id = trim($this->input->post('school_id'));
            }
			if ($this->input->post('branch_id') != '') {
                  $branch_id = trim($this->input->post('branch_id'));
            }
			if ($this->input->post('class_id') != '') {
                 $class_id = trim($this->input->post('class_id'));
            }
			
			if ($this->input->post('term') != '') {
                 $term = trim($this->input->post('term'));
            }
			
			$year 		 = date('Y');
			
			$chkReport 	 = $this->modifyprogressreport_model->check_progress_report($student_id,$school_id,$branch_id,$class_id,$term,$year);

			$countReport = count($chkReport);
		  
		  			if( $countReport > 0 ) {
			           $input	=  $this->input->post();
		        if($this->modifyprogressreport_model->updateReport($input)) {
		                  $this->session->set_flashdata('success', 'Progress report has been updated successfuly.');
	   redirect(base_url()."modifyprogressreport/index/$student_id");exit;
				       } else {
					      $this->session->set_flashdata('error', 'Some problem exists. Progress report has not been updated.');
       redirect(base_url()."modifyprogressreport/index/$student_id");exit;
				        }
						
			       }  else  {
					   
					    $input		=  $this->input->post();
		            if($this->modifyprogressreport_model->insertReport($input)) {
				
		                  $this->session->set_flashdata('success', 'Progress report has been added successfuly.');
	  redirect(base_url()."modifyprogressreport/index/$student_id");exit;
				       } else {
					      $this->session->set_flashdata('error', 'Some problem exists. Progress report has not been added.');
	  redirect(base_url()."modifyprogressreport/index/$student_id");exit;
				           }
				       
					   }
		  
		  }
		  
		  
	
	 public function get_autofill_term()

	{     
			$student_id = $_POST['student_id'];
			$school_id = $_POST['school_id'];
			$branch_id = $_POST['branch_id'];
			$class_id = $_POST['class_id'];
			$term = $_POST['termid'];
			$year = $_POST['year'];
	 $getreports = $this->modifyprogressreport_model->check_progress_report($student_id,$school_id,$branch_id,$class_id,$term,$year);
		if( count($getreports) > 0 ) { 
			 $HTML = ''; 
		   foreach($getreports as $report) {
			  $subjectid =  $report->subject_id;
			  $subjectname =  $this->modifyprogressreport_model->get_subject_name($subjectid,$school_id,$branch_id,$class_id);
			  $subjectname->subject_name;
			   
			  
$HTML.= '<input type="hidden" name="subject_id[]" value="'.$report->subject_id.'" />';

$HTML.= '<div class="col-sm-12 nopadding">';
$HTML.= '<div class="col-sm-12 prfltitlediv titlediv">';
$HTML.= '<h1>'.$subjectname->subject_name.'</h1>';
$HTML.= '</div>';
        
$HTML.= '<div class="profile-bg report_cls">';
  
$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Effort</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="effort['.$report->subject_id.']" id="effort_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->effort == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->effort == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->effort == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->effort == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';
  
$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Behaviour</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="behaviour['.$report->subject_id.']" id="behaviour_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->behaviour == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->behaviour == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->behaviour == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->behaviour == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Homework</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="homework['.$report->subject_id.']" id="homework_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Satisfactory"';
 if($report->home_work == 'Satisfactory'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Satisfactory</option>';
 
$HTML.= '<option value="Good"';
if($report->home_work == 'Good'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Good</option>';

$HTML.= '<option value="Very Good"';
if($report->home_work == 'Very Good'){
$HTML.= ' selected ';
}
$HTML.= '>Very Good</option>';


$HTML.= '<option value="Excellent"';
if($report->home_work == 'Excellent'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Excellent</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox reportselbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Proof reading</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<select class="form-control" name="proofreading['.$report->subject_id.']" id="proofreading_'.$report->subject_id.'">';

$HTML.= '<option value="">Select</option>';

$HTML.= '<option value="Incomplete"';
 if($report->proof_reading == 'Incomplete'){
$HTML.= ' selected ';
	  } 
$HTML.= '>Incomplete</option>';
 
$HTML.= '<option value="Amendments Required"';
if($report->proof_reading == 'Amendments Required'){ 
$HTML.= ' selected ';
 }
$HTML.= '>Amendments Required</option>';

$HTML.= '<option value="Approved by Teacher"';
if($report->proof_reading == 'Approved by Teacher'){
$HTML.= ' selected ';
}
$HTML.= '>Approved by Teacher</option>';


$HTML.= '<option value="Complete"';
if($report->proof_reading == 'Complete'){ 
$HTML.= ' selected '; 
}
$HTML.= '>Complete</option>';

$HTML.= '</select>';

$HTML.= '</div>';

$HTML.= '</div>';


$HTML.= '<div class="form-group detailbox">';

$HTML.= '<label class="col-sm-3 control-label nopadding">Comments</label>';

$HTML.= '<div class="col-sm-9 inputbox">';

$HTML.= '<textarea class="form-control" rows="3" name="comment['.$report->subject_id.']" id="comment_'.$report->subject_id.'">'.$report->comment.'</textarea>';

$HTML.= '</div>';

$HTML.= '</div>';
 
$HTML.= '</div>';
  
$HTML.= '</div>';
			 
		 }	
		 
		 $HTML.= '<div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-6 nopadding">

        <div class="cancellink">

       <!-- <input type="button" value="Cancel">-->

        </div>

        </div>

        <div class="col-sm-6 nopadding">

        <div class="confirmlink">

        <input type="submit" name="modifyprogressreportBtn" id="modifyprogressreportBtn" value="Confirm Modification">

        </div>

        </div>

        </div>';
			
			echo $HTML; 
		
		 }
	  
	}
	
	
/************************************************ textarea description for template (Cheshta)******************************************/

	
		

}
