<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">
   

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

	       <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>


        <li class="edit">ShortListed Candidate </li>        

        </ul>

        </div>

       
        </div>

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-12 col-xs-7 nopadding profile-bg accsection">

        <h1>ShortListed Candidate </h1>

        </div>

        </div>

        <div class="col-sm-8 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

        <div class="profile-bg prfltitle profile_titlebg">

        <h1>General details</h1>
<div class=" col-sm-12 nopadding contentdiv">
	<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Avtar<br/><span class="tagline">JPG, max. 500KB</span></label>

    <div class="col-sm-9 col-xs-8 profile-picture inputbox">
    
	<?php if($info->profile_image!='') { ?> <img src="<?php echo base_url();?>uploads/staff/<?php echo $info->profile_image; ?>"> 
     <?php } else { ?> 
     <img src="<?php echo base_url();?>assets/images/avtar.png">
     <?php } ?>

     <!-- <div class="profilepicbtns"><i class="fa fa-picture-o"></i><br/><hr class="borderline"><i class="fa fa-trash-o"></i></div>-->

    </div>

  </div>

  		<div class="form-group detailbox accdetail">

    	<label class="col-sm-3 col-xs-4 control-label nopadding">Full Name</label>

    	<div class="col-sm-9 col-xs-8 inputinfo">

      	<span><?php echo $candidate->staff_fname. ' ' . $candidate->staff_lname; ?></span>

    	</div>

  		</div>

 		<div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Date of Birth</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo date('d M Y', strtotime($candidate->staff_dob));?></span>

    </div>

  </div>

        <div class="form-group detailbox accdetail">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Title</label>

    <div class="col-sm-9 col-xs-8 inputinfo">

      <span><?php echo $candidate->staff_title;?></span>

    </div>

  </div>
  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Telephone</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $candidate->staff_telephone;?></span>

    </div>

  </div>

  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Full Address</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $candidate->staff_address;?></span>

    </div>

  </div>
  
  		<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Address(optional)</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $candidate->staff_address_1;?></span>

    </div>

  </div>
  
<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Education/Qualification</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $candidate->staff_education_qualification;?></span>

    </div>

  </div>
  
  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Personal Summary</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $candidate->staff_personal_summery?></span>

    </div>

  </div>
  <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Applied date</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo date('d M Y', strtotime($candidate->staff_application_date));?></span>

    </div>

  </div>
    <div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">Application Status</label>

    <div class="col-sm-9 col-xs-8 inputbox">

     <span><?php echo $candidate->staff_enrolement_status; ?></span>

    </div>

  </div>
<div class="form-group detailbox">

    <label class="col-sm-3 col-xs-4 control-label nopadding">CV</label>

    <div class="col-sm-9 col-xs-8 inputbox">
	<a href="<?php echo base_url(); ?>uploads/staff/<?php echo $candidate->cv; ?>" target="_blank"> View CV</a>
    <!-- <span><?php echo base_url(); ?>uploads/staff/<?php echo $candidate->cv; ?></span> -->

    </div>

  </div>


</div>
</div>
        

        </div>

        </div>

        
        
       

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script> 


<style>
.dashboard-attendance .statusdiv {
    width: 100%;
}
.icon_section {
    float: right;
    padding: 0 15px 0 0;
}
.cetificate_section {
    padding-left:0px;
	list-style:none;
	 font-size: 15px;
}
.cetificate_section > li {
    border: 1px solid #ccc;
    padding: 5px 10px 5px 15px;
}
.prfltitle {
    padding: 0 0px 15px;
}
.fa.fa-download + .tooltip > .tooltip-inner {background-color:#57B94A !important; color:#fff;}
.fa.fa-download + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}
.fa.fa-eye + .tooltip > .tooltip-inner {background-color: #57B94A !important; color:#fff;}
.fa.fa-eye + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}
.cetificate_section li h4 {
    border-bottom: 1px solid #ccc;
    padding-bottom: 10px;
}
.cetificate_section > li {
    border: medium none;

}
.profile-bg {
    border: medium none;
	padding: 0px;
}
.cetificate_section li h4 {
    color: #354052 !important;
    font-size: 14px;
}
.prfltitlediv {
    margin-bottom: 15px;
}
.profile_titlebg h1{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1);  padding: 10px 15px; }
.profiletitltbox{padding-top:0px;padding-bottom:0px;}
.profiletitltbox .profile_titlebg {
    padding: 0;
}
.contentdiv{
	padding:15px 15px 0px 15px;

}
.accsection h1 {
    padding: 0 0 10px 15px;
}

.docsec{
	margin-bottom:0px;}
.grphdiv{
	margin-left:15px;
}
.prfilleft {
    padding: 0;
}
</style> 