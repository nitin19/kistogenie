<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schoolconfig extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','school_model'));
		$this->load->database();
        $this->load->library('session');
			$this->load->library('form_validation');
		$this->load->library('image_lib');
    }

	public function index()	{
		
	    $data = array();
			
        $sid = $this->session->userdata('user_id');
		
		$data_rows						= $this->school_model->get_records($sid);
		$data['info']					= $data_rows[0];
		
		$data['title']	      			= "School config";
		$data['error']					= $this->session->flashdata('error');
		$data['logmode']				= $this->logmode;
		$data['success']				= $this->session->flashdata('success');
		
		
		$this->load->view('header');
           
		$this->load->view('schoolconfig/school_config' , $data);
      
		$this->load->view('footer');

	}
	public function update($uid){
		
		$postURL	= $this->createPostURL($_GET);
		 if($uid==NULL){
			$this->session->set_flashdata('error', 'Select school first.');
			redirect(base_url()."schoolconfig/index");exit;
		   }
		  
		  $userdata 						= array();
		  $schooldata 					    = array();						
		  $data							    = array();
	  
	    $school_id = $this->session->userdata('user_school_id'); 
	 
	    $whereuser						= array('id'=>$uid);
		$whereschool					= array('school_id'=>$school_id);
			
			if ($this->input->post('schoolname') != '') {
                $schooldata['school_name'] = trim($this->input->post('schoolname'));
				$schoolname = trim($this->input->post('schoolname'));
            }
			
			if ($this->input->post('telephone') != '') {
                $schooldata['school_phone'] = trim($this->input->post('telephone'));
            }
			
			if ($this->input->post('address') != '') {
                $schooldata['school_address'] = trim($this->input->post('address'));
            }
			
			if ($this->input->post('contact_person') != '') {
                $schooldata['school_contact_person'] = trim($this->input->post('contact_person'));
            }
			
			if ($this->input->post('email') != '') {
                $schooldata['school_email'] = trim($this->input->post('email'));
            }
			
			if ($this->input->post('password') != '') {
                $userdata['password'] = trim($this->input->post('password'));
            }
			
			if ($this->input->post('email') != '') {
                $userdata['email'] = trim($this->input->post('email'));
            }
	 
	      $checkSchoolname = $this->school_model->check_edited_schoolname($school_id,$schoolname);
		  $countSchoolname = count($checkSchoolname);
	 
	 if( $countSchoolname > 0 ) {
				  $this->session->set_flashdata('error', 'School name already exists. School has not been updated.');
				  redirect(base_url()."schoolconfig/index");exit;
			 } else {
	 
	     if($this->school_model->udateSchool($schooldata,$whereschool)){
			$this->school_model->updateUser($userdata,$whereuser);
			$this->session->set_flashdata('success', 'School has been updated successfuly.');
			redirect(base_url()."schoolconfig/index");exit;
		   } else {
			$this->session->set_flashdata('error', 'Some problem exists. School has not been updated.');
			redirect(base_url()."schoolconfig/index");exit;
		    }
		}
	 }   
 
   public function backup_database(){
	//$fileName='db_backup.zip';
	$fileName = 's0_'.date('Y-m-d').'_'.time('H-i-s').'.sql.zip';
    $this->load->dbutil();
    $backup = $this->dbutil->backup();
    $this->load->helper('file');
    if(write_file(FCPATH.'downloads/database_backup/'.$fileName, $backup)) {
	        $this->session->set_flashdata('success', 'Database backup '.$fileName.' has been created successfuly.');
			redirect(base_url()."schoolconfig/index/");exit;
	        } else {
			$this->session->set_flashdata('error', 'Some problem exists. Database backup has not been created.');
			redirect(base_url()."schoolconfig/index/");exit;
	            }
            }
   
		 
	 public function download_database_backup($fileName = NULL) { 
	   
       if ($fileName) {
        $file = realpath ( FCPATH.'downloads/database_backup/'.$fileName );
        if (file_exists ( $file )) {
         $data = file_get_contents ( $file );
         $this->load->helper('download');
         force_download ( $fileName, $data );
        } else {
		 $this->session->set_flashdata('error', 'Requested file does not exists.');
         redirect(base_url()."schoolconfig/index/");exit;
        }
       }
      }
	  
	  function delete_database_backup($fileName){
		  
	  if ($fileName) {
        $file = realpath ( FCPATH.'downloads/database_backup/'.$fileName );
        if (file_exists ( $file )) {
        
		         $this->load->helper("file");
				 $this->load->helper("url");
				 $url = FCPATH.'downloads/database_backup/'.$fileName;
				 unlink($url);
				 
			 $this->session->set_flashdata('success', 'Database backup has been deleted successfuly.');
			 redirect(base_url()."schoolconfig/index/");exit;
		
          } else {
		 $this->session->set_flashdata('error', 'Requested file does not exists.');
         redirect(base_url()."schoolconfig/index/");exit;
                }
            }
	     }
		 
 
    function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}

}  

