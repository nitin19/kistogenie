<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>

<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>
		<?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>


 <li><a href="<?php echo base_url(); ?>help">Help and Knowledgebase</a></li>


        <li class="edit">Student Enrolment</li>        

        </ul>

        </div>

 

   <div class="block-content">	

      <div class="col-sm-12 tablediv nopadding">
      
      
      
      
      

    <div class="col-sm-3">
	<div class="list-block">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#student-enrolment-section" aria-controls="home" role="tab" data-toggle="tab">Student Enrolment</a></li>
      <li role="presentation"><a href="#student_enrol-repo" aria-controls="profile" role="tab" data-toggle="tab">Student Enrolement Report</a></li>
    </ul>
	</div>
    </div>
    <div class="col-sm-9">
      <div class="tab-content adm-dsh">
      <div role="tabpanel" class="tab-pane active" id="student-enrolment-section">
			
			<h1 class="tab-title">Student Enrolment</h1>
			<p class="main-heading-cntnt">In student Enrolment, students registered  from the website link: <a href="http://aerodeck.temp.domains/~olive3/student-enrolement/">http://aerodeck.temp.domains/~olive3/student-enrolement/</a> </p>
			<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/student_enrollment.jpg" class="stt">
			<div class="compse-msg">
			
				<div class="admn-content">
					<p class="heading-cntnt">Auto thank you email will be receive by registered student. Listing of all registered students  displayed on the student enrolment section. In student enrolment section, there are 4 tabs:</p>
					<ul class="msg-point">
					<li><strong>APPLIED STUDENTS</strong></li>
					<li><strong>SELECTED STUDENTS</strong></li>
					<li><strong>REJECTED STUDENTS</strong></li>
					<li><strong>WAITING STUDENTS</strong></li>
					</ul>
				</div>
	</div>
			<div class="admn-content">
				<h3 class="para-heading">APPLIED STUDENT:</h3>
				<p class="spacing">Listing of all the registered student  in applied section consists of  action button with 9 functionalities:</p>
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/applied-student.jpg" class="stt">
				
				<p class="spacing" ><strong>ENROLED:</strong> A mail sent to the particular  student informing that he/she has been selected in the organisation  for  attend the classes  and after that list will automatically  updated.
</p>
				<p class="spacing"><strong>REJECTED:</strong> In this ,An auto “sorry mail”  sent to the applicant that his/her application is rejected for reason or no vacant  space in classes .please apply for the next time.
</p>			
				<p class="spacing"><strong>POSTPONE:</strong> This functionality will generate a popup  for admin or user to postpone the application for some interval.</p>
				<p class="spacing"><strong>REQUEST DETAIL:</strong> This functionality will generate a popup  in which, a mail sent to the applicant with username and password and with the link to login in sms to fill the further details of the student.<br><br>
				When student will fill the information then details will be saved into the temporary table.
				<br>
				<br>
				When student will fill the details than auto mail will revert back to the admin informing that the user/student  fill their some details using the above link.
				</p>
				<p class="spacing"><strong>SCHEDULE  EMAIL:</strong> This functionality will generate a popup  in which, student will get an email notification with date & time including the  address of the school  about schedule of free class.</p>
				<p class="spacing"><strong>RESCHEDULE  EMAIL:</strong> This functionality will generate a popup  in which, if student will not able to come  by certain reasons  so that a student will get an email notification  with date& time including the  address about reschedule their free class.
</p>
				<p class="spacing"><strong>NOTES:</strong> This popup act like a sticky notes, admin can save any information regarding student .</p>
				<p class="spacing"><strong>DELETE:</strong> User can delete the particular student from the list.</p>
				
</div>
<div class="admn-content">
				<h3 class="para-heading">SELECTED STUDENT: </h3>
				<p>All the enrolled student will display here with action button  of functionalities:</p>
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/selected_student.jpg" class="stt">
				
				<ul class="msg-point">
					<li><strong>VIEW </strong>- User can view the general details of the student.</li>
					<li><strong>NOTES</strong>- This popup act like a sticky notes, admin can save any information regarding student .</li>
					</ul>
</div>
				<div class="admn-content">
				<h3 class="para-heading">REJECTED  STUDENT</h3>
				<p>All the rejected student will display here with action button of functionalities:</p>
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/rejected_student.jpg" class="stt">
				<div class="compse-msg">
					<ul class="msg-point">
					<li><strong>APPLIED: </strong>- This functionality will generate a popup  in which all the registered students will display  in the list.<br>A rejected student can also apply again.
</li>
					<li><strong>ENROLED</strong>- A mail sent to the particular  student informing that he/she has been selected in the organisation  for  attend the classes  and after that list will automatically  updated.<br> Selected student now in the list of “Selected student tab”.
</li>
					<li><strong>POSTPONE</strong> - This functionality will generate a popup  for admin or user to postpone the application for some interval.</li>
					<li><strong>REQUEST DETAIL</strong>- This functionality will generate a popup  in which, a mail sent to the applicant with username and password and with the link to login in sms to fill the further details of the student.
					<br>
					<br>When student will fill the information then details will be saved into the temporary table.
<br><br>When student will fill the details than auto mail will revert back to the admin informing that the user/student  fill their some details using the above link.
</li>
					<li><strong>SCHEDULE  EMAIL</strong> - This functionality will generate a popup  in which, student will get an email notification with date & time including the  address of the school  about schedule of free class.
	</li>
	<li><strong>RESCHEDULE  EMAIL</strong> - This functionality will generate a popup  in which, if student will not be  able to come  by certain reasons so that a student will get an email notification  with date & time including and address to  about reschedule their free class.
	<ul>
	<li>NOTES - This popup act like a sticky notes, admin can save any information regarding student .</li>
	<li>DELETE - User can delete the particular student from the list.</li>
	</ul>
	</li>
	</ul>
	
	</div>
	<div class="admn-content">
				<h3 class="para-heading">Waiting List</h3>
				<p>All the rejected student will display here with action button of functionalities:</p>
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/waiting_student.jpg" class="stt">
				<div class="compse-msg">
					<ul class="msg-point">
					<li><strong>APPLIED: </strong>- This functionality will generate a popup  in which all the registered students will display  in the list.<br>A rejected student can also apply again.
</li>
					<li><strong>ENROLED</strong>- A mail sent to the particular  student informing that he/she has been selected in the organisation  for  attend the classes  and after that list will automatically  updated.<br> Selected student now in the list of “Selected student tab”.
</li>
					<li><strong>POSTPONE</strong> - This functionality will generate a popup  for admin or user to postpone the application for some interval.</li>
					<li><strong>REQUEST DETAIL</strong>- This functionality will generate a popup  in which, a mail sent to the applicant with username and password and with the link to login in sms to fill the further details of the student.
					<br>
					<br>When student will fill the information then details will be saved into the temporary table.
<br><br>When student will fill the details than auto mail will revert back to the admin informing that the user/student  fill their some details using the above link.
</li>
					<li><strong>SCHEDULE  EMAIL</strong> - This functionality will generate a popup  in which, student will get an email notification with date & time including the  address of the school  about schedule of free class.
	</li>
	<li><strong>RESCHEDULE  EMAIL</strong> - This functionality will generate a popup  in which, if student will not be  able to come  by certain reasons so that a student will get an email notification  with date & time including and address to  about reschedule their free class.
	<ul>
	<li>NOTES - This popup act like a sticky notes, admin can save any information regarding student .</li>
	<li>DELETE - User can delete the particular student from the list.</li>
	</ul>
	</li>
	</ul>
	
	</div>
			</div>
	  
	  </div>
	 </div>
	 <!-- Teacher_dashboard-->
      <div role="tabpanel" class="tab-pane" id="student_enrol-repo">
	  
	 <h1 class="tab-title">Student Enrolement Report</h1>
			<p class="main-heading-cntnt">This section provide the listing of user which are applied from the website.</p>
			
		<div class="admn-content">
			<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/student-view-report.jpg" class="stt">
			<ul class="msg-point">
				<li>It also provide the number of those applied students whose status are modify or updated(selected, rejected and waiting)  from Student Enrolment Section</li>
				<li>Listing of users are displayed with action button of functionality.</li>
				<li><strong>VIEW -</strong> By this user can view the student registration detail.</li>
				<li>User can also search the number of  students by selecting branch with starting and ending date then clicked on view student button to view the number of students in a particular section.</li>
				<li><strong>In showing dropdown ,user can select the numbers of records to view  per page.</strong></li>
				<li><strong>EXPORT TO EXCEL</strong> button is used to save the records in excel file on their personal computer.</li>
			</ul>
		</div>
	  
	  
	  </div>
		
			
	</div><!-- STUDENT view Report -->	
	  
	  
	  
	  </div>
    </div>
    </div>
      
      

         

	   </div>
    
	 </div>

    </div>

<style>
.tabs-left, .tabs-right {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}

.tab-content.adm-dsh {
    background: #fff;
    width: 100%;
    display: inline-block;
    padding: 10px 20px;
    border: 1px solid #eee;
}
h1.tab-title {
    margin-top: 0 !important;
    margin-left: 0 !important;
    border: none !important;
    font-weight: 900;
    font-size: 32px !important;
}
.adm-dsh p {
    margin-left: 0;
}
p.main-heading-cntnt {
    margin-top: -44px;
    border-bottom: 1px solid #eeeeee;
    padding: 10px 0;
	margin-left:20px;
}
.admn-content {
    margin-top: -2px;
    padding-top: 10px;
    padding-left: 20px;
}
h3.para-heading {
        margin-top: 0px;
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
.admn-content p {
    margin-top: -7px;
}
img.stt {
    width: 100%;
    padding: 30px 0;
}
.compse-msg {
    padding: 4px 0 0 0px;
}
h4.txt-frmt {
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
ul.msg-point {
    padding-left: 0;
    margin: 0 0 0 14px;
    line-height: 30px;
    font-size: 15px;
}
.compse-msg {
    padding: 20px 0 16px 0px;
    line-height: 28px;
}
h3.para-heading.mgn {
    margin-top: 20px;
}
.list-block {
    height: 634px;
    width: 100% !important;
    background: #fff;
}
p.spacing {
    line-height: 15px;
    margin-top: -17px;
}
</style>