<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Progressreport_model extends CI_Model {
	var $tbl	= "term";
	var $users = "users";
	var $progress_report = "progress_report";
	var $branch = "branch";
	var $classes = "classes";
	var $subjects = "subjects";
	
    function __construct() {
        parent::__construct();
    }
	
	function get_terms($school_id) {
	$this->db->select('*');
	$this->db->from($this->tbl);
	$this->db->where(array('school_id'=>$school_id,"is_deleted="=>'0', "is_active="=>'1'));
	$query = $this->db->get();
	$this->db->last_query(); 
	return $query->result();
	}
	
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
  function getprogressreportcount($school_id,$term_id,$year) {
        $this->db->select('*');
		$this->db->group_by('student_id');
        $this->db->from($this->progress_report);
		$this->db->where(array("school_id="=>$school_id,"term"=>$term_id,"progress_report_year"=>$year,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->num_rows();
    }
	
}
