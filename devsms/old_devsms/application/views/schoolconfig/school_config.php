  <?php 
	   //  $schoolid = $info->school_id;
	   //  $this->load->model(array('school_model'));
		// $schoolInfo = $this->school_model->getSchoolinfo($schoolid);
	  ?>
<div class="editprofile-content">
<div class="col-sm-12 profilemenus nopadding">


    <div class="col-sm-9 col-xs-12 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li class="edit"><a href="#">Configuration</a><!--<i class="fa fa-question-circle" id="changeprofilepic" data-toggle="tooltip" title="Some data" data-placement="top"></i>--></li>        

        </ul>

        </div>

       <!-- <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>
-->
        </div>



 <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div> 
 
		 <div class="col-sm-12 col-xs-7 nopadding profile-bg" style="padding: 0px 0px 5px 15px !important;">
		
         <h1>School Configuration</h1>
		
        </div>
 
        <div class="col-sm-6 leftspace fullwidsec">

        <div class="editprofileform accdetailinfo">
		
        <form action="<?php echo base_url(); ?>schoolconfig/update/<?php echo $info->id; ?>" method="post" name="editschoolinfoForm" id="editschoolinfoForm" enctype="multipart/form-data">
       
        <div class="profile-bg">
		<div class="col-sm-12  profiletitltbox profile_titlebg nopadding">
        <h4>School details</h4>
        </div>
        
         <div class="content-div">
          <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Username<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="text" class="form-control" name="username" id="username" value="<?php echo $info->username; ?>"  placeholder="" required="required" autocomplete="off" maxlength="41" disabled="disabled">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Password<span>*</span></label>
    <div class="col-sm-9 inputbox">
      <input type="password" class="form-control" name="password" id="password" value="<?php echo $info->password; ?>" placeholder="" required="required" autocomplete="off" maxlength="16">
    </div>
  </div>

  		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">School Name<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="text" placeholder="" name="schoolname" id="schoolname" class="form-control" value="<?php echo $info->school_name;?>">

    </div>

  </div>

  		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">Email<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="text" placeholder="" name="email" id="email" class="form-control"  value="<?php echo $info->school_email ; ?>">

    </div>

  </div>

  		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">Telephone<span>*</span></label>

    <div class="col-sm-9 inputbox">

      <input type="text" placeholder="" name="telephone" id="telephone" class="form-control" value="<?php echo $info->school_phone; ?>" maxlength="11">

    </div>

  </div>

  		

 		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">Address<span>*</span> </label>

    <div class="col-sm-9 inputbox">

      <input type="text" placeholder="" name="address" id="address" class="form-control" value="<?php echo $info->school_address;?>">

    </div>

  </div>

  		<div class="form-group">

    <label class="col-sm-3 control-label nopadding">Contact person</label>

    <div class="col-sm-9 inputbox">

 <input type="text" placeholder="" name="contact_person" id="contact_person" class="form-control" value="<?php echo $info->school_contact_person;?>">

    </div>

  </div>
</div>
  		</div>

<div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-12 nopadding">

        <div class="confirmlink configbtn">

         <input type="submit" value="Update" name="updateschoolBtn" id="updateschoolBtn" class="btn btn-default">

        </div>

        </div>

        </div>

        </form>

        </div>

        </div>

        <div class="col-sm-6 rightspace fullwidsec">

        <div class="editprofileform accdetailinfo miscchanges">

        <form>

        <div class="profile-bg">
		<div class="col-sm-12 profiletitltbox profile_titlebg nopadding">
        <h4>Miscellaneous Site Changes</h4>
		</div>
        <div class="content-div">
  		<div class="form-group">

    <label class="col-sm-5 control-label nopadding">Current Academic Year: 2016</label>

    <div class="col-sm-7 inputbox">

      <select class="form-control">

      <option>Select Year</option>

  <option>1</option>

  <option>2</option>

  <option>3</option>

  <option>4</option>

  <option>5</option>

</select>

    </div>

  </div>

		<div class="imptext">

        <h1>Important</h1>

        <p>Amend the Academic Year by selecting the year from the drop down on the left, and clicking the 'Set Academic Year' button. This setting determines which pages are displayed for the Fees and Accounts sections. 

The academic year should be the year that the school year has started in. E.g. if the year starts in September 2008 and ends in July 2009, the Academic year should be set to 2008 for the duration of this year.

The academic year should be the year that the school year has started in. E.g. if the year starts in September 2008 and ends in July 2009, the Academic year should be set to 2008 for the duration of this year.

The academic year should be the year that the school year has started in. E.g. if the year starts in September 2008 and ends in July 2009, the Academic year should be set to 2008 for the duration of this year.

The academic year should be the year that the school year has started in. E.g. if the year starts in September 2008 and ends in July 2009, the Academic year should be set to 2008 for the duration of this year.

</p>

        </div>
</div>
  		</div>

<div class="cancelconfirm cancelconfirmdiv">

        <div class="col-sm-12 nopadding">

        <div class="confirmlink configbtn">

         <input type="button" id="updateschoolBtn" value="Set Academic Year" class="btn btn-default">

        </div>

        </div>

        </div>

        </form>
         


        </div>

        </div>

        <div class="col-sm-12 nopadding">

        <div class="col-sm-6 leftspace fullwidsec">

        <div class="col-sm-12 tablediv nopadding">

    <div class="col-sm-12 nopadding">

    <h1>Lessons Overview<span>147</span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2">No.</th>

					  <th class="column16">Lessons</th>    

				  </tr>

			  </thead>

				<tbody class="tableheight">

					<tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

					<tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

                        <tr class="familydata configtable1">

						<td class="column2">01</td>

						<td class="column16">OAK - SAT - Class 1</td>

                        </tr>

				</tbody>

		  </table>

          

          

</div>





	</div>

    

	</div>

        

        </div>

        <div class="col-sm-6 rightspace fullwidsec">

        <div class="col-sm-12 tablediv nopadding">

          <div class="col-sm-12 nopadding">

            <h1>Backups</h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column2">No.</th>

					  <th class="column15">Local Backups</th>  

                      <th class="column3"></th>   

				  </tr>

			  </thead>

				<tbody class="tableheight">
                
		<?php 
		
	$log_directory = $_SERVER['DOCUMENT_ROOT'].'/devsms/downloads/database_backup/';

	$results_array = array();
     

		if (is_dir($log_directory))
		{
				if ($handle = opendir($log_directory))
				{
						while(($file = readdir($handle)) !== FALSE)
						{
								$results_array[] = $file;
						}
						closedir($handle);
				}
		}
		
		$filecounter=1;
		$countfiles = count(array_filter($results_array));
		if($countfiles > 2) {
 	 foreach(array_reverse($results_array) as $filename) {
		if($filename=="." || $filename==".." || $filename=="download.php" || $filename=="index.php") {
             } else { ?>
		         <tr class="familydata configtable2">

				<td class="column2"><?php echo $filecounter;?></td>

				<td class="column15"><?php echo $filename;?></td>

   <!-- <td class="column1"> <a href="<?php //echo base_url();?>schoolconfig/download_database_backup/<?php //echo $filename;?>"> <i class="fa fa-download"></i></a>  &nbsp;&nbsp;&nbsp; <a href="<?php //echo base_url();?>schoolconfig/delete_database_backup/<?php //echo $filename;?>"> <i class="fa fa-trash-o"></i></a></td>-->
   
     <td class="column3"> <a href="<?php echo base_url();?>schoolconfig/download_database_backup/<?php echo $filename;?>" class="change"> <i class="fa fa-download" data-toggle="tooltip" title="Download" data-placement="top"></i></a>   <a onclick="delConfirm('<?php echo $filename;?>')" style="cursor: pointer;"  class="change"> <i class="fa fa-trash-o" data-toggle="tooltip" title="Delete" data-placement="top"></i></a></td>

                        </tr>
				<?php		
				$filecounter++;
        		 }
      		 }
			 
		} else {
			 echo '<tr class="familydata configtable2"> No backup found </tr>';
		   }
			?>				

				</tbody>

		  </table>

          
  <form action="<?php echo base_url(); ?>schoolconfig/backup_database" name="dbbkupForm" id="dbbkupForm" method="post">
    <div class="confirmlink">
       <input type="submit" class="btn btn-default" value="Create database bkup">
       </div>
    </form>
          

</div>





	</div>

    

	    </div>

        

        </div>
        </div>

    </div>
   <style>


.change .fa-download:hover {
  color: #57cdf7;
}
.change .fa-trash-o:hover{
  color: #57cdf7;
}
.fa-trash-o + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-trash-o+ .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

.fa-download + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-download+ .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
   </style> 
 <script type="text/javascript">
	function delConfirm(fileName){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>schoolconfig/delete_database_backup/"+fileName;
		}else{
			return false;
		}
	}
</script>

<script>
  jQuery(document).ready(function(){
	  
  jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Only alphabetical characters");

jQuery.validator.addMethod("alphaUname", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    });
	
	jQuery.validator.addMethod("alphaUpwd", function(value, element) {
    return this.optional(element) || value == value.match(/^(?=.*[a-zA-Z0-9])[a-zA-Z0-9!@#$%&*.]{7,}$/);
    });
	
    jQuery("#editschoolinfoForm").validate({
        rules: {
			
				username: {
				  required: true,
				  alphaUname: true,
				  minlength: 4,
				  maxlength: 40
				},
				
				password: {
				  required: true,
				  alphaUpwd: true,
				  minlength: 6,
				  maxlength: 15
				},
				
                schoolname: {
                required: true,
				lettersonly: true,
				maxlength: 120
               },
			   email: {
                  required: true,
                  email: true,
				  maxlength: 120
               },
			   telephone: {
                  required: true,
				  number:true,
                  minlength: 11,
				  maxlength: 11
               },
			   
			   address: {
                required: true
               }
        },
        messages: {
			
			   username: {
                  required: "This field is required.",
				  alphaUname: "Letters only please.",
				  minlength: "Minimum 4 characters required.",
				  maxlength: "Maximum 40 characters allowed."
                 },
			    password: {
                  required: "This field is required.",
				  alphaUpwd: "Letters, numbers and special characters allowed",
				  minlength: "Minimum 6 characters required.",
				  maxlength: "Maximum 15 characters allowed."
                 },
				 
                schoolname: {
                  required: "Please Enter Schoolname",
				  lettersonly: "Please enter only character value",
				  maxlength: "Maximum 120 characters allowed."
                 },
				  email: {
                  required: "Please Enter Email",
				  email: "Please enter a valid email address",
				  maxlength: "Maximum 120 characters allowed."
                 },
				  telephone: {
				  required: "Please Enter Telephone",
                  number: "Please Enter Number",
				  minlength: "Minimum 11 characters required.",
				  maxlength: "Maximum 11 characters allowed."
                 },
				  address: {
                  required: "Please Enter Address"
                 }
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
	});
</script>
    <script type="text/javascript"> 
      $(document).ready( function() {
		$('body').click(function(){
			$('.alert-dismissable').fadeOut('slow');
		})
	  });
    </script>   

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>


<style>
.detailright h1 {
	color:#354052 !important;
}
.imptext > p {
    padding-top: 25px;
}

.imptext > h1 {
    font-size: 18px !important;
}

.imptext{
	padding-bottom:5px;
}
.confirmlink.configbtn {
    margin: 0 0 20px;
}

#updateschoolBtn{margin:0px;}

.profile_titlebg h4{background: -webkit-linear-gradient(#ffffff, #d1d1d1);
    background: -o-linear-gradient(#ffffff, #d1d1d1); 
    background: -moz-linear-gradient(#ffffff, #d1d1d1); 
    background: linear-gradient(#ffffff, #d1d1d1); margin-top: 0;
    padding: 10px 0 10px 15px; }
	
.profile-bg{padding:0px !important;}

.content-div{
	padding:15px 15px 0px 15px;

}

.fa.fa-question-circle + .tooltip > .tooltip-inner {background-color:#57B94A !important; color:#fff;}
.fa.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}
.fa.fa-question-circle+ .tooltip > .tooltip-inner {background-color: #57B94A !important; color:#fff;}
.fa.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#57B94A !important;}
.fa.fa-question-circle{color:#090;}
</style>