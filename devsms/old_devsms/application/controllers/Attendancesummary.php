<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Attendancesummary extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','attendancesummary_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{
		 $data							= array();
		$school_id		= $this->session->userdata('user_school_id');
		$data['branches'] = $this->attendancesummary_model->getbranches($school_id);
		
		$sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;
		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		//$type_search					= isset($_GET['type'])?$_GET['type']:NULL;
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$class_search					= isset($_GET['class'])?$_GET['class']:NULL;
		
		 
		if($sdate_search!=''){
			//$search_condition['attendance_date']	= date('Y-m-d', strtotime($sdate_search));
			$startdate	= date('Y-m-d', strtotime($sdate_search));
		}
		
		if($edate_search!=''){
			//$search_condition['attendance_date']	= date('Y-m-d', strtotime($edate_search));
			$enddate	= date('Y-m-d', strtotime($edate_search));
		}
		
		
		
		/*if($type_search!=''){
			$search_condition['attendance_status']	= $type_search;
		}*/
		
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		if($class_search!=''){
			$search_condition['class_id']	= $class_search;
		}
		
		$search_condition['school_id']	= $this->session->userdata('user_school_id');
		
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		
		$data['title']	      			= "Attendance report";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		$perpage						= 50;
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'attendancesummary/index';
		
if($sdate_search!='' && $edate_search!='') {
	    $config['total_rows'] 			= $this->attendancesummary_model->getRows($search_condition,$startdate,$enddate);
	   }
	   
		$config['per_page'] 			= $perpage;
 		$config['postfix_string'] = "/?sdate=$sdate_search&edate=$edate_search&branch=$branch_search&class=$class_search"; 
		$this->pagination->initialize($config);
		$odr =   "attendance_id";
		$dirc	= "asc";
		
if($sdate_search!='' && $edate_search!='') {
		    $data_rows  = $this->attendancesummary_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,$startdate,$enddate);
	   }
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
if($sdate_search!='' && $edate_search!='') {
		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
      }
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['class_search']			= $class_search;
		$data['sdate_search']			= $sdate_search;
		$data['edate_search']			= $edate_search;
		//$data['type_search']			= $type_search;
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		
		$this->load->view('header');

		$this->load->view('attendance/attendance_summary', $data);

		$this->load->view('footer');

	}
	
	 public function check_classes()

	{     
		    $school_id = $this->session->userdata('user_school_id');
			$schoolbranch = trim($_POST['schoolbranch']); 
	        $getclasses = $this->attendancesummary_model->getclasses($school_id,$schoolbranch);
			
			foreach($getclasses as $getclass) {
 			 echo '<option value="'.$getclass->class_id.'">'.$getclass->class_name.'</option>';
			}
	  }
	  
	  	   public function excel_action(){
	
		$branch_search		=	$this->input->post('branch_search');
		
		$class_search		=	$this->input->post('class_search');
		
		$sdate_search		=	$this->input->post('sdate_search');
		
		$edate_search		=	$this->input->post('edate_search');	
		
 
	 	$search_condition	= 	array();
		
		if($sdate_search!=''){
			
			$startdate	= date('Y-m-d', strtotime($sdate_search));
		}
		
		if($edate_search!=''){
			
			$enddate	= date('Y-m-d', strtotime($edate_search));
		}
			
		if($branch_search!=''){
			
			$search_condition['branch_id']	= $branch_search;
		}
		if($class_search!=''){
			
			$search_condition['class_id']	= $class_search;
		}
		
		$search_condition['school_id']	= $this->session->userdata('user_school_id');
		
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		$odr =   "attendance_id";
		$dirc	= "asc";
		
		if($sdate_search!='' && $edate_search!='') {
			
		  $summarydata		 = $this->attendancesummary_model->getExcelData($search_condition,$odr,$dirc,$startdate,$enddate);
		}
		
		
		$this->load->library("Excel");
    
    	$object 		 = new PHPExcel();
     
    	$object->setActiveSheetIndex(0);
		 
		$table_columns 	 = array("Student Firstname","Student Lastname", "Present","Absent","Late","Total Session");
		 
		$column = 0;
		 
		foreach($table_columns as $field)
		{
		 $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		 $column++;
		}
		
		  
		$excel_row = 2;
		 
		foreach($summarydata as $row)
		{
	  	 $studentid = $row->student_id;
		  
		 $studentinfo =$this->attendancesummary_model->getstudentinfo($studentid);
		 
		 
	     $startdate	= date('Y-m-d', strtotime($sdate_search));
		 $enddate	= date('Y-m-d', strtotime($edate_search));
			   
		 $totalAttendancestatus = $this->attendancesummary_model->get_total_Attendancestatus($studentid,$startdate,$enddate);
			
		 $presentAttendancestatus = $this->attendancesummary_model->get_present_Attendancestatus($studentid,$startdate,$enddate);
				
		 $absentAttendancestatus = $this->attendancesummary_model->get_absent_Attendancestatus($studentid,$startdate,$enddate);
				
		 $lateAttendancestatus = $this->attendancesummary_model->get_late_Attendancestatus($studentid,$startdate,$enddate);
			  
		 $present_percentage =  ($presentAttendancestatus * 100) / $totalAttendancestatus;
		 $absent_percentage =  ($absentAttendancestatus * 100) / $totalAttendancestatus;
		 $late_percentage =  ($lateAttendancestatus * 100) / $totalAttendancestatus;
			   
			     
		$present_attendance_Arry[] = $present_percentage;
		$absent_attendance_Arry[] = $absent_percentage;
		$late_attendance_Arry[] = $late_percentage;
		$total_session_Arry[]  = $totalAttendancestatus;
				   
		 
		 $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row,$studentinfo->student_fname);
		
		 $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row,$studentinfo->student_lname);
		 
		 $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row,$present_percentage.''.'%');
			
		 $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row,$late_percentage.''.'%');
		
		 $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row,$absent_percentage.''.'%');
		
		 $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row,$totalAttendancestatus);
		 
		 $excel_row++;
		
		}
		
/*	   $avg_present_attendance = array_sum($present_attendance_Arry) / count($present_attendance_Arry);
	   $avg_late_attendance = array_sum($late_attendance_Arry) / count($late_attendance_Arry);
	   $avg_absent_attendance = array_sum($absent_attendance_Arry) / count($absent_attendance_Arry);
	   $avg_session_attendance = array_sum($total_session_Arry) / count($total_session_Arry);
		
	   $avg_column=array("Average total",'',$avg_present_attendance,$avg_late_attendance,$avg_absent_attendance,$avg_session_attendance);
	   
	   foreach($avg_column as $lastcol)
		{
		 $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $lastcol);
		 $column++;
		}*/
		 
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Attendance Summary Data.xls"');
		$object_writer->save('php://output');
	
	}		


}

