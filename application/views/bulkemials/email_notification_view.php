<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>
<div class="editprofile-content">
    <div class="col-sm-12 profilemenus nopadding">
     <div class="col-sm-9 col-xs-8 nopadding menubaritems">
         <ul>
		          <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

           <?php  } elseif($user_type == 'student'){?>

              <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>
    <?php }else {?>
          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
    <?php } ?>
        <li class="edit">Send Notification</li>        
        </ul>
        </div>
        </div>

          <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div> 	

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-12 col-xs-7 nopadding profile-bg accsection">

 <div class="col-sm-6 nopadding">       <h1>Send Notification</h1> </div>
<div class="col-sm-6 nopadding msgtmp">
  <a href="<?php echo base_url();?>messagetemplate" class="btn btn-info btn-flat">Create Message Template </a>

</div>
        </div>

        </div>

        <div class="col-sm-12 leftspace fullwidsec">

        <div class="notifyprofileform editdetails accdetailinfo notification_section">
       <div class="profile-bg prfltitle profile_titlebg select_radio"> 
         <div class="radio_buttons col-sm-6 nopadding">
         <label class="radio-inline">
          <input type="radio" class="userType" name="user_type" id="inlineRadio1" value="staff" checked="checked"> Staff
        </label>
        <label class="radio-inline">
          <input type="radio" class="userType" name="user_type" id="inlineRadio2" value="student"> Student
        </label>
        </div>

       </div>
         <div class="staff_div" id="staffdiv">
         
             <form name="SendNotificationStaffForm" id="SendNotificationStaffForm" method="post" action="<?php echo base_url(); ?>emailnotifications/sendnotificationStaff" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
     
       <div class="notifyprofileform">
    
         <div class="profile-bg prfltitle profile_titlebg select_div">
         <div class="notification_type">
        <label class="radio-inline">
          <input type="radio" name="notification_type_staff" id="inlineRadio13" value="both" checked="checked"> Both
        </label>
         
        <label class="radio-inline">
          <input type="radio" name="notification_type_staff" id="inlineRadio11" value="email"> Only Email
        </label>
        <label class="radio-inline">
          <input type="radio" name="notification_type_staff" id="inlineRadio12" value="sms"> Only SMS Notification
        </label>
         <br />
         </div>
            
        <div class="col-sm-6 form-group">
        <label class="col-sm-12 control-label nopadding">Branch</label>
        <div class="col-sm-12 inputbox nopadding">
       <select class="form-control" name="branch_staff[]" id="branch_staff" multiple="multiple" required style="height:80px; !important;" title="Please select branch">  
        <?php foreach($branches as $branch) { ?>		
            <option value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>		
        <?php } ?>				                  
        </select>
<i class="fa fa-question-circle errspan" data-toggle="tooltip" title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook) " data-placement="top"></i>

<!-- <span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>  -->
                        
        </div>
        </div>
        
        
        <div class="col-sm-6 form-group multipleselbox">
        <label class="col-sm-12 control-label nopadding">Staff</label>
        <div class="col-sm-12 inputbox nopadding" >
    <select class="form-control" name="staff_name[]" id="staff_name" multiple="multiple" required style="height:80px; !important;" title="Please select staff">  
     </select>
<i class="fa fa-question-circle errspan" data-toggle="tooltip" title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook) " data-placement="top"></i>
   <!--  <span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>   -->
        </div>
        </div>

       <div class="col-sm-6 form-group filterdiv ">
        <label class="col-sm-12 control-label nopadding">Predefined Messages</label>
        <div class="col-sm-12 inputbox nopadding" >
      <?php  

          $user_type ='staff';
    
          $this->load->model('emailnotifications_model');
          $predefined_msg  = $this->emailnotifications_model->getmessages($user_type);
            if(count($predefined_msg) > 0){ ?>

        <select class="form-control premsg" name="premsg" id="premsg" >
          <option value=''>Select title</option>
        <?php 
          
  
          foreach($predefined_msg as $predefinedmsg){ ?>

           <option value ='<?php echo $predefinedmsg->message_id;?>'><?php echo $predefinedmsg->title;?></option>


          <?php } }?>

       </select>

       <i class="fa fa-question-circle errspan" data-toggle="tooltip" title="Select Predefined template. To create new template click 'Create Message Template' infront of 'Send Notification'." data-placement="top"></i> 
        </div>
      </div>

     <div class="col-sm-6 form-group">
    <label class="col-sm-12 control-label nopadding">Subject</label>
    <div class="col-sm-12 inputbox nopadding">
      <input type="hidden" name="field_staff_email_subject" id="field_staff_email_subject" value="">
      <input type="text" class="form-control" name="staff_email_subject" id="staff_email_subject" value="">
    </div>
  </div>

     <div class="col-sm-9 form-group nopadding">
    <label class="col-sm-12 control-label">Message</label>
    <div class="col-sm-12 inputbox">
     
      <textarea class="form-control ckeditor" name="staff_email_message" id="staff_email_message" required>  </textarea>

     <script>
       CKEDITOR.replace( 'staff_email_message' );
     </script> 
    </div>
  </div>

          <div class="col-sm-3 form-group vardiv nopadding">
        <label class="col-sm-12 control-label nopadding">Variables</label>
        <div class="col-sm-12 inputbox nopadding" >
    <div class="variablediv nopadding">
    <div class="col-sm-3 inputbox nopadding" ><h4>Common Variables : </h4></div>

    <div class="col-sm-9 inputbox nopadding" ><p>{first_name} , {last_name} , {username} , {password} , {gender} , {email_id} , {school_name} , {branch_name} , {date_of_birth} , {main_telephone} , {his/her} , {him/her} </p></div>

   <div class="col-sm-3 inputbox nopadding" > <h4>Staff Variables : </h4> </div>

   <div class="col-sm-9 inputbox nopadding" > <p>{qualification} , {salary} , {teaching_days}</p></div>

   <div class="col-sm-3 inputbox nopadding" > <h4>Student Variables : </h4></div>

   <div class="col-sm-9 inputbox nopadding" > <p> {father_name} , {mother_name} , {father_mobile} , {mother_mobile} , {father_email} , {mother_email} , {father_address} , {mother_address} , {child_class} , {enrolment_date} </p></div>


</div>
        </div>
        </div>

   <div class="col-sm-12 form-group vardiv">
    <label class="col-sm-2 control-label nopadding">Attachments</label>
    <div class="col-sm-10">
	<input type="file" name="attachments[]" multiple class="attach_btn"/>
                                <span class="attchmnttxt">only .jpg, .jpeg, .pdf, .docx, .txt, .png, .xls
                                 files can be uploaded(max. size:2mb).</span>
    </div>
  </div>
   
           </div>        
       </div>     
        
        
 <div class="notifyprofileform">
        <div class="cancelconfirm prevnextbtns">
        <div class="col-sm-6 nopadding">
        </div>
        <div class="col-sm-6 nopadding">
        <div class="confirmlink">
           <input type="submit" class="send_notfication" value="Send Notification">
        </div>
        </div>
        </div>

        </div>
        
         </form>
         
           </div>

        <div class="student_div" id="studentdiv" style="display:none"> 
        
              <form name="SendNotificationForm" id="SendNotificationForm" method="post" action="<?php echo base_url(); ?>emailnotifications/sendnotification" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
     
       <div class="notifyprofileform">
         <div class="profile-bg prfltitle profile_titlebg select_div">


         <div class="radio_buttons">
          <label class="radio-inline">
          <input type="radio" name="notification_type" id="inlineRadio5" value="both" checked="checked"> Both
        </label>

        <label class="radio-inline">
          <input type="radio" name="notification_type" id="inlineRadio3" value="email"> Only Email
        </label>
        <label class="radio-inline">
          <input type="radio" name="notification_type" id="inlineRadio4" value="sms"> Only SMS Notification
        </label>
       </div>
         <br />
            
        <div class="col-sm-6 form-group">
        <label class="col-sm-12 control-label nopadding">Branch</label>
        <div class="col-sm-12 inputbox nopadding">
          <select class="form-control" name="branch[]"  id="branch" multiple="multiple" required style="height:80px; !important;" title="Please select branch">          
       <?php foreach($branches as $branch) { ?>		
            <option value="<?php echo $branch->branch_id;?>"><?php echo $branch->branch_name;?></option>		
        <?php } ?>				                  
        </select>
<i class="fa fa-question-circle errspan" data-toggle="tooltip" title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook) " data-placement="top"></i>

       <!-- <span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>  -->
                        
        </div>
        </div>
        
        
        <div class="col-sm-6 form-group multipleselbox">
        <label class="col-sm-12 control-label nopadding">Class</label>
        <div class="col-sm-12 inputbox nopadding" >
    <select class="form-control" name="class_name[]" id="class_name" multiple="multiple" required style="height:80px; !important;" title="Please select class">  
     </select>
<i class="fa fa-question-circle errspan" data-toggle="tooltip" title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook) " data-placement="top"></i>

<!--<span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>   -->
        </div>
        </div>
        
            <div class="col-sm-12 nopadding">
         <div class="col-sm-6 form-group">
        <label class="col-sm-12 control-label nopadding">Students</label>
        <div class="col-sm-12 inputbox nopadding" id="student_name_old">
        <select class="form-control" name="studentsname[]" id="student_name" multiple="multiple" required style="height:105px; !important;" title="Please select student">
        </select>
<i class="fa fa-question-circle errspan" data-toggle="tooltip" title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook) " data-placement="top"></i>

      <!--  <span class="fa fa-question-circle errspan"  tool-tip-toggle="tooltip-demo" data-original-title="For multiple selections use ctrl+ mouse left click (in windows) & command + mouse right click (in macbook)" ></span>-->
        </div>
      </div>

        <div class="col-sm-6 form-group filterdiv ">
        <label class="col-sm-12 control-label nopadding">Predefined Messages</label>
        <div class="col-sm-12 inputbox nopadding" >
      <?php  

          $user_type ='student';
    
          $this->load->model('emailnotifications_model');
          $predefined_msg  = $this->emailnotifications_model->getmessages($user_type);
            if(count($predefined_msg) > 0){ ?>

        <select class="form-control studentpremsg" name="studentpremsg" id="studentpremsg" >
             <option value=''>Select title</option>
        <?php 
          
  
          foreach($predefined_msg as $predefinedmsg){ ?>

           <option value ='<?php echo $predefinedmsg->message_id;?>'><?php echo $predefinedmsg->title;?></option>


          <?php } }?>

       </select>
<i class="fa fa-question-circle errspan" data-toggle="tooltip" title="Select Predefined template. To create new template click 'Create Message Template' infront of 'Send Notification' " data-placement="top"></i>
        
        </div>
             <div class="form-group subectdiv">
    <label class="col-sm-12 control-label nopadding">Subject</label>
    <div class="col-sm-12 inputbox nopadding">
        <input type="hidden" name="field_student_email_subject" id="field_student_email_subject" value="">
      <input type="text" class="form-control" name="student_email_subject" id="student_email_subject" value="">
    </div>
  </div>
      </div>
</div>


     <div class="col-sm-9 form-group nopadding">

    <label class="col-sm-12 control-label">Message</label>
    <div class="col-sm-12 inputbox">
 
      <textarea class="form-control ckeditor" name="student_email_message" id="student_email_message">  </textarea>
     <script>
       CKEDITOR.replace( 'student_email_message' );
     </script> 
    </div>
  </div>

        <div class="col-sm-3 form-group vardiv nopadding">
        <label class="col-sm-12 control-label">Variables</label>
        <div class="col-sm-12 inputbox nopadding " >
    <div class="variablediv">
    <div class="col-sm-3 inputbox nopadding" ><h4>Common Variables : </h4></div>

    <div class="col-sm-9 inputbox nopadding" ><p>{first_name} , {last_name} , {username} , {password} , {gender} , {email_id} , {school_name} , {branch_name} , {date_of_birth} , {main_telephone} , {his/her} , {him/her} </p></div>

   <div class="col-sm-3 inputbox nopadding" > <h4>Staff Variables : </h4> </div>

   <div class="col-sm-9 inputbox nopadding" > <p>{qualification} , {salary} , {teaching_days}</p></div>

   <div class="col-sm-3 inputbox nopadding" > <h4>Student Variables : </h4></div>

   <div class="col-sm-9 inputbox nopadding" > <p> {father_name} , {mother_name} , {father_mobile} , {mother_mobile} , {father_email} , {mother_email} , {father_address} , {mother_address} , {child_class} , {enrolment_date} </p></div>


</div>
        </div>
        </div>

  <div class="col-sm-12 form-group">
    <label class="col-sm-12 control-label nopadding">Attachments</label>
    <div class="col-sm-12 nopadding">
  <input class="attach_btn" type="file" name="attachments[]" multiple />
                                <span class="attchmnttxt">only .jpg, .jpeg, .pdf, .docx, .txt, .png, .xls
                                 files can be uploaded(max. size:2mb).</span>
    </div>
  </div>

   
           </div>        
       </div>     
        
        
 <div class="notifyprofileform">
        <div class="cancelconfirm prevnextbtns">
        <div class="col-sm-6 nopadding">
        </div>
        <div class="col-sm-6 nopadding">
        <div class="confirmlink">
           <input type="submit" class="send_notfication" value="Send Notification">
        </div>
        </div>
        </div>

        </div>
        
        
         </form>
         
         </div>

             </div>

           </div>

        </div>
   
  
<script>
  jQuery(document).ready(function(){

        if( jQuery(".toggle .toggle-title").hasClass('active') ){
    jQuery(".toggle .toggle-title.active").closest('.toggle').find('.toggle-inner').show();
  }
  jQuery(".toggle .toggle-title").click(function(){
    if( jQuery(this).hasClass('active') ){
      jQuery(this).removeClass("active").closest('.toggle').find('.toggle-inner').slideUp(200);
    }
    else{ jQuery(this).addClass("active").closest('.toggle').find('.toggle-inner').slideDown(200);
    }
  });
	 
	   jQuery('.userType').click(function(){
          var inputValue = jQuery(this).attr("value");
          if(inputValue == 'staff') {
			  jQuery('#staffdiv').show();

			  jQuery('#studentdiv').hide();
		   } else {
			  jQuery('#staffdiv').hide();

			  jQuery('#studentdiv').show();
		
		   }
       });
	   
	   jQuery("#branch_staff").on('click', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if( branch_id!='' && branch_id!='null' ) {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/getStaff',
            data:{ 'branch_id': branch_id },
            success :  function(responsestaff) {
            //  alert(responsestaff);
						jQuery('#staff_name').empty();
					    jQuery('#staff_name').html(responsestaff);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
	   jQuery("#branch").on('click', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if( branch_id!='' && branch_id!='null' ) {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/getclass',
            data:{ 'branch_id': branch_id },
            success :  function(response) {
				        response=jQuery.parseJSON(response);
						jQuery('#class_name').empty();
					    jQuery('#class_name').html(response.datacls);
						jQuery('#student_name').empty();
					    jQuery('#student_name').html(response.datastu);
			          }
				   });
				 } else {
				 return false;
				 }
			 }); 
			 
	 jQuery("#class_name").on('click', function(e){ 
   		  var class_id = jQuery(this).val();
		   var branch_id = jQuery('#branch').val();
		  if(branch_id!='' && class_id!='' && branch_id!='null' && class_id!='null') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/get_students',
            data:{ 'branch_id': branch_id, 'class_id':class_id},
            success :  function(resp) {
              //alert(resp);
						jQuery('#student_name').empty();
					    jQuery('#student_name').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 });
			 
	  
	 jQuery('[data-toggle="tooltip"]').tooltip();   

  jQuery("#SendNotificationStaffForm").validate({
		ignore: [],
        rules: {
			  'branch_staff[]':{
                required: true
                },
			 'staff_name[]':{
                required: true
               },
			  staff_email_subject: {
                required: true
               },
  
			  staff_email_message: {
              // required: true
               } 

          },
        messages: {
				'branch_staff[]': {
                  required: "Please select branch."
                 },
				'staff_name[]': {
                  required: "Please select staff."
                 },
				staff_email_subject: {
                  required: "Please enter subject."
                 },

				staff_email_message: {
                  //required: "Please enter message."
                 }
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });
		
		jQuery("#SendNotificationForm").validate({
		ignore: [],
        rules: {
			  'branch[]':{
                required: true
                },
			 'class_name[]':{
                required: true
               },
			  'studentsname[]':{
                required: true
               },
			  student_email_subject: {
                required: true
               },
			   student_email_message: {
               // required: true
               }
          },
        messages: {
				'branch[]': {
                  required: "Please select branch."
                 },
				'class_name[]': {
                  required: "Please select class."
                 },
			    'studentsname[]': {
                  required: "Please select student."
                 },
				student_email_subject: {
                  required: "Please enter subject."
                 },
				student_email_message: {
                 // required: "Please enter message."
                 }
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });			 

 
   });

jQuery(".premsg").on('change',function(e){
           
        var message_id    = $(this).val();

        var subject_value = $('#staff_email_subject').val();
        var F2            = jQuery('#field_staff_email_subject').val();

                     
        if(message_id!='') { 
          jQuery.ajax({
          type : 'POST',
          url  : '<?php echo base_url(); ?>emailnotifications/get_messagedesc',
          data:{ 'message_id': message_id },
          success :  function(data) {

            var arr = data.split(',,');

            CKEDITOR.instances['staff_email_message'].setData(arr[0]);

            var dnM_sub_Value = '';

            if(subject_value!='') {
                 if(F2!='') {

                   if(F2 == arr[1]) {
                        dnM_sub_Value = subject_value;
                      } else {
                         if (subject_value.toLowerCase().includes(F2.toLowerCase())) {

                         dnM_sub_Value = subject_value.replace(F2, arr[1]);

                        } else {

                        dnM_sub_Value = subject_value+' '+arr[1];
                                }
                   }
                } else {
                    dnM_sub_Value = subject_value+' '+arr[1];   
               }
              } else {
                dnM_sub_Value = subject_value+' '+arr[1];  
              }
                 
            jQuery('#field_staff_email_subject').val(arr[1]);
            
            jQuery('#staff_email_subject').replaceWith('<input type="text" class="form-control" name="staff_email_subject" id="staff_email_subject" value="'+dnM_sub_Value+'">');

            }
           });
           
             } else {
          return false;
            }
        });

jQuery(".studentpremsg").on('change',function(e){
           
        var message_id    = $(this).val();

        var subject_value = $('#student_email_subject').val();
        var F2            = jQuery('#field_student_email_subject').val();

                       
        if(message_id!='') { 
          jQuery.ajax({
          type : 'POST',
          url  : '<?php echo base_url(); ?>emailnotifications/get_messagedesc',
          data:{ 'message_id': message_id },
          success :  function(data) {
            var arr = data.split(',,');

          CKEDITOR.instances['student_email_message'].setData(arr[0]);
            var dnM_sub_Value = '';

            if(subject_value!='') {
                 if(F2!='') {

                   if(F2 == arr[1]) {
                        dnM_sub_Value = subject_value;
                      } else {
                         if (subject_value.toLowerCase().includes(F2.toLowerCase())) {

                         dnM_sub_Value = subject_value.replace(F2, arr[1]);

                        } else {

                        dnM_sub_Value = subject_value+' '+arr[1];
                                }
                   }
                } else {
                    dnM_sub_Value = subject_value+' '+arr[1];   
               }
              } else {
                dnM_sub_Value = subject_value+' '+arr[1];  
              }

            jQuery('#field_student_email_subject').val(arr[1]);
            
            jQuery('#student_email_subject').replaceWith('<input type="text" class="form-control" name="student_email_subject" id="student_email_subject" value="'+dnM_sub_Value+'">');

               }
           });
           
             } else {
          return false;
            }
        });


</script>  
<style>

.notifyprofileform .form-group .attach_btn {

    height: 100%;

}
.fa-question-circle + .tooltip > .tooltip-inner {background-color:#090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}
.notification_section .notifyprofileform {
    width: 100%;
}
.notification_section .profile_titlebg {
    padding: 30px 15px;
}
.radio_buttons {
    padding-bottom: 20px;
}
.notification_type {
    padding-bottom: 20px;
}
.fa.fa-question-circle.errspan {
    color: #57b94a;
    float: left;
    text-align: center;
    width: 20px;
}
.inputbox select {
    float: left;
    width: calc(100% - 20px);
}
.notifyprofileform .form-group .inputbox input {
    float: left;
    width: calc(100% - 20px);
}
#cke_staff_email_message{ width: calc(100% - 20px);}
#cke_student_email_message{width: calc(100% - 20px);}
/* 15-nov-2017 */

.prfltitle.profile_titlebg.select_radio{
	margin-bottom:0px;
	padding-bottom:0px;
}
.profile-bg.prfltitle.profile_titlebg.select_div {
    padding-top: 0;
}

/****************** 24-January-2018 ***********************************/
.variablediv {
    border: 1px solid #ccc;
    height: 305px;
    border-radius: 4px;
    box-shadow: 0 0 5px #ccc;
    background-color: #eff3f6;

}
.variablediv  p, h4 {
   padding: 5px 0px 0px 5px;
   margin :5px 0px;
   font-size: 11px;
   line-height: 20px;
 
}
.msgtmp a {
    float: right;
    margin-right: 35px;
    margin-top: 20px;
}

.btn-info {
    background-color: #37b148;
    border-color: #37b148;
}

/****************** 25-January-2018 ***********************************/
.vardiv {
    float: left;
 }
 .subectdiv {
    margin-top: 70px;
}
</style>