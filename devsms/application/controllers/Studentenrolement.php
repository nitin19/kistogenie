<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Studentenrolement extends CI_Controller {



	 var $logmode;

	function __construct(){

        parent::__construct();

        if( $this->authorize->is_user_logged_in() == false ){

			$this->session->set_flashdata('error', 'Please login first.');

			redirect(base_url());

		   }

		$this->logmode	= $this->session->userdata('log_mode');

        $this->load->model(array('login_model','authorization_model','studentenrolement_model'));

		$this->load->database();

        $this->load->library('session');

		$this->load->library('form_validation');

		$this->load->library('image_lib');

		$this->load->library('pagination');

		$this->load->library('authorize');

    }



	public function index()
{   
		$branchid 						= $this->uri->segment(4);

	    $sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;

		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		
		$branch_id						= isset($_GET['branchidsrch'])?$_GET['branchidsrch']:NULL;


		$search_condition['users.user_type']	= 'enrolementstudent';

		$search_condition['users.is_active']	= '1';

		$search_condition['users.is_deleted']	= '0';

		$search_condition['student_enrolement.student_enrolement_status']	= 'applied';

		$currentdate = date('Y-m-d');
		
		if($branch_id!=''){
			
			$search_condition['student_enrolement.student_school_branch']	= $branch_id;
			$rprtbranchid = $branch_id;
			
		}
		if($branchid!='' ){
		
		$search_condition['student_enrolement.student_school_branch']	= $branchid;
		$rprtbranchid = $branchid;
		}

		if($sdate_search!=''){

			$startdate	= date('Y-m-d', strtotime($sdate_search));

		} else {

			$startdate	= '';

		  }

		if($edate_search!=''){

			$enddate	= date('Y-m-d', strtotime($edate_search));

		} else {

			$enddate	= '';

		   }

		$data							= array();

		$school_id						= $this->session->userdata('user_school_id');
		
		$staff_branch_id				= $this->session->userdata('staff_branch_id');
		
		$user_type						= $this->session->userdata('user_type');

		$data['branches'] 				= $this->studentenrolement_model->getbranches($school_id);

		
		$data['selectedInfo'] 			= $this->studentenrolement_model->get_selected_info($school_id);

		$data['rejectedInfo'] 			= $this->studentenrolement_model->get_rejected_info($school_id);
		
		$data['waitingInfo'] 			= $this->studentenrolement_model->get_waiting_info($school_id);

		$data['requestdetailsInfo'] 	= $this->studentenrolement_model->get_requestdetail_info($school_id);

		$data['scheduleInfo'] 			= $this->studentenrolement_model->get_schedule_info($school_id);

		$data['rescheduleInfo']			= $this->studentenrolement_model->get_reschedule_info($school_id);
		
		$data['sessionresponseInfo']	= $this->studentenrolement_model->get_sessionresponse_info($school_id);
		
		$data['sessionwaitingInfo']		= $this->studentenrolement_model->get_sessionwaiting_info($school_id);
		
		$data['after_sessionresponseInfo']	= $this->studentenrolement_model->get_after_sessionresponse_info($school_id);
		
		$data['leaving_aftersessionInfo']	= $this->studentenrolement_model->get_leaving_aftersession_info($school_id);
		
		$data['leaving_beforesessionInfo']	= $this->studentenrolement_model->get_leaving_beforesession_info($school_id);
		
		$data['title']	      			= "Students enrolement";

		$data['name']      				= $this->session->userdata('user_name');

		$data['school_id']				= $this->session->userdata('user_school_id');

		$start						    = $this->uri->segment(3,0);

		$ordrBY							= $this->uri->segment(4,0); 

		$dirc							= $this->uri->segment(5,"asc");


		$perpage						= 10;
	

		$config['uri_segment']			= 3;

		$config['base_url'] 			= base_url().'studentenrolement/index';

	 //   $config['total_rows'] 			= $this->studentenrolement_model->getAppliedRows($search_condition,$school_id,$startdate,$enddate,$currentdate);

		$config['per_page'] 			= $perpage;

        $config['postfix_string'] 		= "/?branchidsrch=$rprtbranchid&sdate=$sdate_search&edate=$edate_search"; 

		 $odr =   "student_enrolement.student_application_date";

		 $dirc	= "desc";


		//$data_rows= $this->studentenrolement_model->getAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$startdate,$enddate,$currentdate);
		
		 /************** Changes for user access system **************************/
		 
		 if($user_type=='teacher'){
			 if($staff_branch_id!=''){
					$config['total_rows'] 		= $this->studentenrolement_model->getStaffAppliedRows($search_condition,$staff_branch_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getStaffAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$staff_branch_id,$startdate,$enddate,$currentdate);
			 }
			 else{
					$config['total_rows'] 		= $this->studentenrolement_model->getStaffAppliedRows($search_condition,$staff_branch_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getStaffAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$staff_branch_id,$startdate,$enddate,$currentdate);
			 }
		 }
		 else if($user_type=='admin'){
				$config['total_rows'] 			= $this->studentenrolement_model->getAppliedRows($search_condition,$school_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$startdate,$enddate,$currentdate);
		 }
		
		$this->pagination->initialize($config);

		$data['dirc']					= $dirc;

		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;

		$data['total_rows']				= $config['total_rows'];

		$data['page_name']				= $this->uri->segment(1);

		$data['last_page']				= $start;

		$data['pagination']				= $this->pagination->create_links();

		$data['sdate_search']			= $sdate_search;

		$data['edate_search']			= $edate_search;
		
		$data['select_last_page']		= '0';
		$data['reject_last_page']		= '0';
		$data['waiting_last_page']		= '0';
		
		//$data['branch_data']			= $branchid;	
		$data['branch_data']			= $rprtbranchid;		
		
		$data['branchsrch_id']			= $branch_id;		

		$data['post_url']				= $config['postfix_string'];

		$data['logmode']				= $this->logmode;

		$data['error']					= $this->session->flashdata('error');

		$data['success']				= $this->session->flashdata('success');


		$this->load->view('header');

		$this->load->view('enrolement/student_enrolement_view', $data);

		$this->load->view('footer');

	}

/* ******************************************* Represent the selected view *********************************** */	

	public function selectedstudents()

	{  
		$branchid = $this->uri->segment(4);
		 
	    $sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;

		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		
		$branch_id						= isset($_GET['branchidsrch'])?$_GET['branchidsrch']:NULL;

		$search_condition['users.user_type']	= 'student';

		$search_condition['users.is_active']	= '1';

		$search_condition['users.is_deleted']	= '0';

		$search_condition['student_enrolement.student_enrolement_status']	= 'selected';
	
		if($branchid!=''){
		
		$search_condition['student_enrolement.student_school_branch']	= $branchid;
		$rprtbranchid = $branchid;
		
		}
		if($branch_id!=''){
			
			$search_condition['student_enrolement.student_school_branch']	= $branch_id;
			$rprtbranchid = $branch_id;
			
		}
		if($sdate_search!=''){

			$startdate	= date('Y-m-d', strtotime($sdate_search));

		} else {

			$startdate	= '';

		  }

		if($edate_search!=''){

			$enddate	= date('Y-m-d', strtotime($edate_search));

		} else {

			$enddate	= '';

		   }

		$currentdate 					= date('Y-m-d');
		
		$data							= array();

		$school_id						= $this->session->userdata('user_school_id');
		
		$staff_branch_id				= $this->session->userdata('staff_branch_id');
		
		$user_type						= $this->session->userdata('user_type');

		$data['branches'] 				= $this->studentenrolement_model->getbranches($school_id);
		
		$data['appliedInfo'] 			= $this->studentenrolement_model->get_applied_info($school_id);

		$data['rejectedInfo'] 			= $this->studentenrolement_model->get_rejected_info($school_id);

		$data['waitingInfo'] 			= $this->studentenrolement_model->get_waiting_info($school_id);

		$data['requestdetailsInfo'] 	= $this->studentenrolement_model->get_requestdetail_info($school_id);

		$data['scheduleInfo'] 			= $this->studentenrolement_model->get_schedule_info($school_id);

		$data['rescheduleInfo'] 		= $this->studentenrolement_model->get_reschedule_info($school_id);
		
		$data['schoolContactNo'] 		= $this->studentenrolement_model->getschoolPhone($school_id);

		$data['title']	      			= "Students enrolement";

		$data['name']      				= $this->session->userdata('user_name');

		$data['school_id']				= $this->session->userdata('user_school_id');

		$start						    = $this->uri->segment(3,0);

		$ordrBY							= $this->uri->segment(4,0); 

		$dirc							= $this->uri->segment(5,"asc");

		$perpage						= 10;

		$config['uri_segment']			= 3;

		$config['base_url'] 			= base_url().'studentenrolement/selectedstudents';

	//    $config['total_rows'] 			= $this->studentenrolement_model->getSelectedRows($search_condition,$school_id,$startdate,$enddate);

		$config['per_page'] 			= $perpage;

        $config['postfix_string'] 		= "/?branchidsrch=$rprtbranchid&sdate=$sdate_search&edate=$edate_search"; 

		$odr 	= "student_enrolement.student_application_date";

		$dirc	= "desc";

	//	$data_rows= $this->studentenrolement_model->getSelectedPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$startdate,$enddate);
		
		 /************** Changes for user access system **************************/
		 
		 if($user_type=='teacher'){
			 if($staff_branch_id!=''){
				$config['total_rows'] 			= $this->studentenrolement_model->getStaffAppliedRows($search_condition,$staff_branch_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getStaffAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$staff_branch_id,$startdate,$enddate,$currentdate);
			 }
			 else{
				$config['total_rows'] 			= $this->studentenrolement_model->getStaffAppliedRows($search_condition,$staff_branch_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getStaffAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$staff_branch_id,$startdate,$enddate,$currentdate);
			 }
		 }
		 else if($user_type=='admin'){
				$config['total_rows'] 			= $this->studentenrolement_model->getSelectedRows($search_condition,$school_id,$startdate,$enddate);
				$data_rows						= $this->studentenrolement_model->getSelectedPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$startdate,$enddate);
		 }
		

		$this->pagination->initialize($config);

		$data['dirc']					= $dirc;

		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;

		$data['total_rows']				= $config['total_rows'];

		$data['page_name']				= $this->uri->segment(1);

		$data['last_page']				= $start;

		$data['pagination']				= $this->pagination->create_links();

		$data['sdate_search']			= $sdate_search;

		$data['edate_search']			= $edate_search;
		
		$data['applied_last_page']		= '0';
		$data['reject_last_page']		= '0';
		$data['waiting_last_page']		= '0';
		
		//$data['branch_data']			= $branchid;	
		$data['branch_data']			= $rprtbranchid;		
		
		$data['branchsrch_id']			= $branch_id;

		$data['post_url']				= $config['postfix_string'];

		$data['logmode']				= $this->logmode;

		$data['error']					= $this->session->flashdata('error');

		$data['success']				= $this->session->flashdata('success');


		$this->load->view('header');

		$this->load->view('enrolement/selected_students_view', $data);

		$this->load->view('footer');

	}

	
/* ******************************************* Represent the REJECTED view *********************************** */
	

	public function rejectedstudents()

	{   
		$branchid = $this->uri->segment(4);
		
	    $sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;

		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;

		$search_condition['users.user_type']	= 'enrolementstudent';

		$search_condition['users.is_active']	= '0';

		$search_condition['users.is_deleted']	= '0';

		$search_condition['student_enrolement.student_enrolement_status']	= 'rejected';
		
		$branch_id			= isset($_GET['branchidsrch'])?$_GET['branchidsrch']:NULL;

		if($branchid!=''){
		
		$search_condition['student_enrolement.student_school_branch']	= $branchid;
		$rprtbranchid = $branchid;
		
		}
				
		if($branch_id!=''){
			
			$search_condition['student_enrolement.student_school_branch']	= $branch_id;
			$rprtbranchid = $branch_id;
			
		}

		if($sdate_search!=''){

			$startdate	= date('Y-m-d', strtotime($sdate_search));

		} else {

			$startdate	= '';

		  }

		if($edate_search!=''){

			$enddate	= date('Y-m-d', strtotime($edate_search));

		} else {

			$enddate	= '';

		   }
		 
		$currentdate 					= date('Y-m-d');
		
		$data							= array();

		$school_id						= $this->session->userdata('user_school_id');
		
		$staff_branch_id				= $this->session->userdata('staff_branch_id');
		
		$user_type						= $this->session->userdata('user_type');

		$data['branches'] 				= $this->studentenrolement_model->getbranches($school_id);

		$data['appliedInfo'] 			= $this->studentenrolement_model->get_applied_info($school_id);

		$data['selectedInfo'] 			= $this->studentenrolement_model->get_selected_info($school_id);

		$data['waitingInfo'] 			= $this->studentenrolement_model->get_waiting_info($school_id);

		$data['requestdetailsInfo'] 	= $this->studentenrolement_model->get_requestdetail_info($school_id);

		$data['scheduleInfo'] 			= $this->studentenrolement_model->get_schedule_info($school_id);

		$data['rescheduleInfo'] 		= $this->studentenrolement_model->get_reschedule_info($school_id);
		
		$data['sessionresponseInfo']	= $this->studentenrolement_model->get_sessionresponse_info($school_id);
		
		$data['sessionwaitingInfo']		= $this->studentenrolement_model->get_sessionwaiting_info($school_id);
		
		$data['after_sessionresponseInfo']	= $this->studentenrolement_model->get_after_sessionresponse_info($school_id);
		
		$data['leaving_aftersessionInfo']	= $this->studentenrolement_model->get_leaving_aftersession_info($school_id);
		
		$data['leaving_beforesessionInfo']	= $this->studentenrolement_model->get_leaving_beforesession_info($school_id);

		$data['schoolContactNo'] 		= $this->studentenrolement_model->getschoolPhone($school_id);

		

		$data['title']	      			= "Students enrolement";

		$data['name']      				= $this->session->userdata('user_name');

		$data['school_id']				= $this->session->userdata('user_school_id');

		$start						    = $this->uri->segment(3,0);

		$ordrBY							= $this->uri->segment(4,0); 

		$dirc							= $this->uri->segment(5,"asc");

		$perpage						= 10;

		$config['uri_segment']			= 3;

		$config['base_url'] 			= base_url().'studentenrolement/rejectedstudents';

//	    $config['total_rows'] 			= $this->studentenrolement_model->getRejectedRows($search_condition,$school_id,$startdate,$enddate);

		$config['per_page'] 			= $perpage;

        $config['postfix_string'] 		= "/?branchidsrch=$rprtbranchid&sdate=$sdate_search&edate=$edate_search"; 

		$odr 							= "student_enrolement.student_application_date";

		$dirc							= "desc";

		//$data_rows= $this->studentenrolement_model->getRejectedPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$startdate,$enddate);
		 /************** Changes for user access system **************************/
		 
		 if($user_type=='teacher'){
			 if($staff_branch_id!=''){
				$config['total_rows'] 			= $this->studentenrolement_model->getStaffAppliedRows($search_condition,$staff_branch_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getStaffAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$staff_branch_id,$startdate,$enddate,$currentdate);
			 }
			 else{
				$config['total_rows'] 			= $this->studentenrolement_model->getStaffAppliedRows($search_condition,$staff_branch_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getStaffAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$staff_branch_id,$startdate,$enddate,$currentdate);
			 }
		 }
		 else if($user_type=='admin'){
				$config['total_rows'] 			= $this->studentenrolement_model->getRejectedRows($search_condition,$school_id,$startdate,$enddate);
				$data_rows						= $this->studentenrolement_model->getRejectedPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$startdate,$enddate);
		 }
		
		$this->pagination->initialize($config);

		$data['dirc']					= $dirc;

		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;

		$data['total_rows']				= $config['total_rows'];

		$data['page_name']				= $this->uri->segment(1);

		$data['last_page']				= $start;

		$data['pagination']				= $this->pagination->create_links();

		$data['sdate_search']			= $sdate_search;

		$data['edate_search']			= $edate_search;
		
		$data['select_last_page']		= '0';
		$data['applied_last_page']		= '0';
		$data['waiting_last_page']		= '0';
		
		//$data['branch_data']			= $branchid;	
		$data['branch_data']			= $rprtbranchid;		
		
		$data['branchsrch_id']			= $branch_id;
		

		$data['post_url']				= $config['postfix_string'];

		$data['logmode']				= $this->logmode;

		$data['error']					= $this->session->flashdata('error');

		$data['success']				= $this->session->flashdata('success');

		$this->load->view('header');

		$this->load->view('enrolement/rejected_students_view', $data);

		$this->load->view('footer');

	}

/* ******************************************* Represent the WAITING view *********************************** */	

	public function waitingstudents()

	{   
		$branchid = $this->uri->segment(4);
		
	    $sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;

		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		
		$branch_id						= isset($_GET['branchidsrch'])?$_GET['branchidsrch']:NULL;
		

		$search_condition['users.user_type']	= 'enrolementstudent';

		$search_condition['users.is_active']	= '1';

		$search_condition['users.is_deleted']	= '0';

		$search_condition['student_enrolement.student_enrolement_status']	= 'waiting';

		$currentdate = date('Y-m-d');
		
		if($branchid!=''){
		
		$search_condition['student_enrolement.student_school_branch']	= $branchid;
		$rprtbranchid = $branchid;
		
		}
				
		if($branch_id!=''){
			
			$search_condition['student_enrolement.student_school_branch']	= $branch_id;
			$rprtbranchid = $branch_id;
			
		}

		if($sdate_search!=''){

			$startdate	= date('Y-m-d', strtotime($sdate_search));

		} else {

			$startdate	= '';

		  }

		if($edate_search!=''){

			$enddate	= date('Y-m-d', strtotime($edate_search));

		} else {

			$enddate	= '';

		   }

		$data							= array();
		
		$school_id						= $this->session->userdata('user_school_id');
		
		$staff_branch_id				= $this->session->userdata('staff_branch_id');
		
		$user_type						= $this->session->userdata('user_type');

		$data['branches'] 				= $this->studentenrolement_model->getbranches($school_id);

		$data['appliedInfo'] 			= $this->studentenrolement_model->get_applied_info($school_id);

		$data['selectedInfo'] 			= $this->studentenrolement_model->get_selected_info($school_id);

		$data['rejectedInfo'] 			= $this->studentenrolement_model->get_rejected_info($school_id);

		$data['requestdetailsInfo'] 	= $this->studentenrolement_model->get_requestdetail_info($school_id);

		$data['scheduleInfo'] 			= $this->studentenrolement_model->get_schedule_info($school_id);

		$data['rescheduleInfo'] 		= $this->studentenrolement_model->get_reschedule_info($school_id);
		
		$data['sessionresponseInfo']	= $this->studentenrolement_model->get_sessionresponse_info($school_id);
		
		$data['sessionwaitingInfo']		= $this->studentenrolement_model->get_sessionwaiting_info($school_id);
		
		$data['after_sessionresponseInfo']	= $this->studentenrolement_model->get_after_sessionresponse_info($school_id);
		
		$data['leaving_aftersessionInfo']	= $this->studentenrolement_model->get_leaving_aftersession_info($school_id);
		
		$data['leaving_beforesessionInfo']	= $this->studentenrolement_model->get_leaving_beforesession_info($school_id);

		$data['schoolContactNo'] 		= $this->studentenrolement_model->getschoolPhone($school_id);

		$data['title']	      			= "Students enrolement";

		$data['name']      				= $this->session->userdata('user_name');

		$data['school_id']				= $this->session->userdata('user_school_id');

		$start						    = $this->uri->segment(3,0);

		$ordrBY							= $this->uri->segment(4,0); 

		$dirc							= $this->uri->segment(5,"asc");

		$perpage						= 10;

		$config['uri_segment']			= 3;

		$config['base_url'] 			= base_url().'studentenrolement/waitingstudents';

	   // $config['total_rows'] 		= $this->studentenrolement_model->getWaitingRows($search_condition,$school_id,$startdate,$enddate,$currentdate);

		$config['per_page'] 			= $perpage;

        $config['postfix_string'] 		= "/?branchidsrch=$rprtbranchid&sdate=$sdate_search&edate=$edate_search"; 

		 $odr 							= "student_enrolement.student_application_date";

		 $dirc							= "asc";

		//$data_rows= $this->studentenrolement_model->getWaitingPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$startdate,$enddate,$currentdate);
		
		/************** Changes for user access system **************************/
		 
		 if($user_type=='teacher'){
			 if($staff_branch_id!=''){
				$config['total_rows'] 			= $this->studentenrolement_model->getStaffAppliedRows($search_condition,$staff_branch_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getStaffAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$staff_branch_id,$startdate,$enddate,$currentdate);
			 }
			 else{
				$config['total_rows'] 			= $this->studentenrolement_model->getStaffAppliedRows($search_condition,$staff_branch_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getStaffAppliedPagedData($search_condition,$start,$perpage,$odr,$dirc,$staff_branch_id,$startdate,$enddate,$currentdate);
			 }
		 }
		 else if($user_type=='admin'){
				$config['total_rows'] 			= $this->studentenrolement_model->getWaitingRows($search_condition,$school_id,$startdate,$enddate,$currentdate);
				$data_rows						= $this->studentenrolement_model->getWaitingPagedData($search_condition,$start,$perpage,$odr,$dirc,$school_id,$startdate,$enddate,$currentdate);
		 }
		
		$this->pagination->initialize($config);
		
		$data['dirc']					= $dirc;

		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;

		$data['total_rows']				= $config['total_rows'];

		$data['page_name']				= $this->uri->segment(1);

		$data['last_page']				= $start;

		$data['pagination']				= $this->pagination->create_links();

		$data['sdate_search']			= $sdate_search;

		$data['edate_search']			= $edate_search;

		$data['select_last_page']		= '0';
		$data['reject_last_page']		= '0';
		$data['applied_last_page']		= '0';
		
		//$data['branch_data']			= $branchid;	
		$data['branch_data']			= $rprtbranchid;		
		
		$data['branchsrch_id']			= $branch_id;

		$data['post_url']				= $config['postfix_string'];

		$data['logmode']				= $this->logmode;

		$data['error']					= $this->session->flashdata('error');

		$data['success']				= $this->session->flashdata('success');


		$this->load->view('header');

		$this->load->view('enrolement/waiting_students_view', $data);

		$this->load->view('footer');



	}

/************************************************* student enrolement view start ************************************************/


	function wating_enrolement_student(){

		$sid				= $_POST['user_id'];
		
		$where				= array('user_id'=>$sid);

		$postponedate		= $_POST['pdate'];
		
		$date				= date('Y-m-d', strtotime($postponedate));
		
		$data				= array('student_enrolement_status'=>'waiting','student_postpone_date'=>$date);
		
		$result				= $this->studentenrolement_model->update($data,$where);
		
		if($result){

			echo $result;
					
			return 'true';

		}else{

			return 'false';
		}
   	}



	public function deleteenrolement($id){

		$del_student=$this->studentenrolement_model->delete($id);

		if($del_student=='1'){

			$this->session->set_flashdata('success', 'Student has been deleted successfully.');

			redirect(base_url()."studentenrolement/index");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."studentenrolement/index");exit;

		}

	}
	
	
/********************************************* student enrolement view end ********************************************/	



/********************************************* student enrolement selected view start ********************************************/


 public function wating_selected_student(){
		
		$sid				= $_POST['user_id'];
		
		$where				= array('user_id'=>$sid);

		$postponedate		= $_POST['pdate'];
		
		$userwhere			= array('id'=>$sid);
		 
		$userdata			= array('user_type'=>'enrolementstudent');
		
		$date				= date('Y-m-d', strtotime($postponedate));
		
		$data				= array('student_enrolement_status'=>'waiting','student_postpone_date'=>$date);
		
		$result				= $this->studentenrolement_model->update($data,$where);
		
		if($result){
			
			$result1= $this->studentenrolement_model->updateUser($userdata,$userwhere);
			
			echo $result1;
					
			return 'true';

		}else{

			return 'false';
		}
	}


public function deleteselected($id){

		$del_student=$this->studentenrolement_model->delete($id);

		if($del_student=='1'){

			$this->session->set_flashdata('success', 'Student has been deleted successfully.');

			redirect(base_url()."studentenrolement/selectedstudents/index");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."studentenrolement/selectedstudents/index");exit;

		}

	}

/********************************************* student enrolement selected view end ********************************************/	



/********************************************* student enrolement rejected view start ********************************************/

	function wating_rejected_student(){

		$sid				= $_POST['user_id'];
		
		$where				= array('user_id'=>$sid);

		$postponedate		= $_POST['pdate'];
		
		$userwhere			= array('id'=>$sid);
		 
		$userdata			= array('is_deleted'=>'0','is_active'=>'1','user_type'=>'enrolementstudent');
		
		$date				= date('Y-m-d', strtotime($postponedate));
		
		$data				= array('student_enrolement_status'=>'waiting','student_postpone_date'=>$date);
		
		$result				= $this->studentenrolement_model->update($data,$where);
		
		if($result){
			
			$result1= $this->studentenrolement_model->updateUser($userdata,$userwhere);
			
			echo $result1;
					
			return 'true';

		}else{

			return 'false';
		}
	}
	

public function deleterejected($id){

		$del_student=$this->studentenrolement_model->delete($id);

		if($del_student=='1'){

			$this->session->set_flashdata('success', 'Student has been deleted successfully.');

			redirect(base_url()."studentenrolement/rejectedstudents/index");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."studentenrolement/rejectedstudents/index");exit;

		}

	}




/*********************************************student enrolement rejected view end ********************************************/



/*********************************************student enrolement waiting view start ********************************************/


public function deletewaiting($id){

		$del_student=$this->studentenrolement_model->delete($id);

		if($del_student=='1'){

			$this->session->set_flashdata('success', 'Student has been deleted successfully.');

			redirect(base_url()."studentenrolement/waitingstudents/index");exit;

		}else{

			$this->session->set_flashdata('error', 'Some problem exists. Try again.');

			redirect(base_url()."studentenrolement/waitingstudents/index");exit;

		}

	}

/*********************************************student enrolement waiting view end ********************************************/


/************************************************** NEW FUNCTIONS **************************************************************/


public function view_studentaccount_detail($sid) {

		$user_id = $this->uri->segment(3);

		$data = array();

		if($sid==NULL){

			$this->session->set_flashdata('error', 'Select student first.');

			redirect(base_url()."studentenrolement/index");exit;

		   }
		

		$data['info']					= $this->studentenrolement_model->get_single_student($user_id);

		$data['title']	      			= "Account detail";

		$data['error']					= $this->session->flashdata('error');

		$data['logmode']				= $this->logmode;

		$data['success']				= $this->session->flashdata('success');

	     

		$this->load->view('header');

        $this->load->view('enrolement/student_enrolementaccount_detail',$data);

	    $this->load->view('footer');

    }




public function add_user_enrolement(){
			
		  $tab_value  = $_POST['tab_value'];
	
		  $school_id = $_POST['school_id'];

		  $user_id = $_POST['user_id'];

		  $student_email = $_POST['student_email'];

		  $student_email_subject = $_POST['student_email_subject'];

		  $student_email_message = $_POST['student_email_message'];

		  $where	= array('user_id'=>$user_id);

	  	  $data		= array('student_enrolement_status'=>'selected','student_postpone_date'=>'0000-00-00');

		  $userwhere	= array('id'=>$user_id);

	  	  $userdata		= array('user_type'=>'student','is_deleted'=>'0','is_active'=>'1');

		  $enroleinfo	= $this->studentenrolement_model->get_single_student($user_id);

		  $studentdata = array();	

		  $studentdata['school_id'] = trim($enroleinfo->school_id);

		  $studentdata['user_id'] = trim($enroleinfo->user_id);

		  $studentdata['student_fname'] = trim($enroleinfo->student_fname);

		  $studentdata['student_lname'] = trim($enroleinfo->student_lname);

		  $studentdata['student_gender'] = trim($enroleinfo->student_gender);

		  $studentdata['student_dob'] = trim($enroleinfo->student_dob);

          $studentdata['student_nationality'] = trim($enroleinfo->student_nationality);

		  $studentdata['student_country_of_birth'] = trim($enroleinfo->student_country_of_birth);

		  $studentdata['student_first_language'] = trim($enroleinfo->student_first_language);

		  $studentdata['student_other_language'] = trim($enroleinfo->student_other_language);

		  $studentdata['student_religion'] = trim($enroleinfo->student_religion);

		  $studentdata['student_ethnic_origin'] = trim($enroleinfo->student_ethnic_origin);

		  $studentdata['student_telephone'] = trim($enroleinfo->student_telephone);

		  $studentdata['student_address'] = trim($enroleinfo->student_address);

		  $studentdata['student_address_1'] = trim($enroleinfo->student_address_1);

		  $studentdata['student_father_name'] = trim($enroleinfo->student_father_name);

		  $studentdata['student_father_occupation'] = trim($enroleinfo->student_father_occupation);

		  $studentdata['student_father_mobile'] = trim($enroleinfo->student_father_mobile);

		  $studentdata['student_father_email'] = trim($enroleinfo->student_father_email);

		  $studentdata['student_father_address'] = trim($enroleinfo->student_father_address);

		  $studentdata['student_mother_name'] = trim($enroleinfo->student_mother_name);

		  $studentdata['student_mother_occupation'] = trim($enroleinfo->student_mother_occupation);

		  $studentdata['student_mother_mobile'] = trim($enroleinfo->student_mother_mobile);

		  $studentdata['student_mother_email'] = trim($enroleinfo->student_mother_email);

		  $studentdata['student_mother_address'] = trim($enroleinfo->student_mother_address);

		  $studentdata['student_emergency_name'] = trim($enroleinfo->student_emergency_name);

		  $studentdata['student_emergency_relationship'] = trim($enroleinfo->student_emergency_relationship);

		  $studentdata['student_emergency_mobile'] = trim($enroleinfo->student_emergency_mobile);

		  $studentdata['student_doctor_name'] = trim($enroleinfo->student_doctor_name);

		  $studentdata['student_doctor_mobile'] = trim($enroleinfo->student_doctor_mobile);

		  $studentdata['student_doctor_surgery_address'] = trim($enroleinfo->student_doctor_surgery_address);

		  $studentdata['student_helth_notes'] = trim($enroleinfo->student_helth_notes);

		  $studentdata['student_allergies'] = trim($enroleinfo->student_allergies);

		  $studentdata['student_other_comments'] = trim($enroleinfo->student_other_comments);

		  $studentdata['student_school_branch'] = trim($enroleinfo->student_school_branch);

		  $studentdata['student_class_group'] = trim($enroleinfo->student_class_group);

		  $studentdata['student_application_date'] = trim($enroleinfo->student_application_date);
		   
		  $studentdata['student_application_comment'] = trim($enroleinfo->student_application_comment);

		  if($enroleinfo->student_enrolment_date=='0000-00-00') {

			    $studentdata['student_enrolment_date'] = date('Y-m-d');

			   } else {

			    $studentdata['student_enrolment_date'] = trim($enroleinfo->student_enrolment_date);

		      }

		  $studentdata['student_leaving_date'] = trim($enroleinfo->student_leaving_date);

		  $studentdata['student_fee_band'] = trim($enroleinfo->student_fee_band);

		  $studentdata['student_certificates'] = trim($enroleinfo->student_certificates);

		  $studentdata['student_family_address'] = trim($enroleinfo->student_family_address);

		  $studentdata['student_sibling'] = trim($enroleinfo->student_sibling);

		  $studentdata['card_no'] = trim($enroleinfo->card_no);

		  $studentdata['card_exp'] = trim($enroleinfo->card_exp);
		  

		if($this->studentenrolement_model->update($data,$where)){

	
			 $this->studentenrolement_model->updateUser($userdata,$userwhere);

			 $this->studentenrolement_model->insertStudent($studentdata);


			 $to		= $student_email;

			 $subject	= $student_email;

			 $message	= $student_email_message;

			 $this->authorize->send_email($to,$message,$subject);

			  $this->session->set_flashdata('success', 'Message has been send successfully.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		     }else{

			$this->session->set_flashdata('error', 'Message has not been send.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		    }
			
	    }
		
public function applied_user_enrolement(){
	
		  $tab_value  = $_POST['tab_value'];
	
		  $school_id = $_POST['school_id'];

		  $user_id = $_POST['user_id'];

		  $student_email = $_POST['student_email'];

		  $student_email_subject = $_POST['student_email_subject'];

		  $student_email_message = $_POST['student_email_message'];

		  
 		  $rejectedInfo	= $this->studentenrolement_model->get_single_student($user_id);
		  
		  $userwhere	= array('id'=>$user_id);
		 
		  $userdata		= array('is_deleted'=>'0','is_active'=>'1','user_type'=>'enrolementstudent');
 
		  $where		= array('user_id'=>$user_id);

	  	  $data			= array('student_enrolement_status'=>'applied','student_postpone_date'=>'0000-00-00');

	
			if($this->studentenrolement_model->update($data,$where)){
				
			 $this->studentenrolement_model->updateUser($userdata,$userwhere);
			 
			 $to		= $student_email;

			 $subject	= $student_email_subject;

			 $message	= $student_email_message;

			 $this->authorize->send_email($to,$message,$subject);
  $this->session->set_flashdata('success', 'Message has been send successfully.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		     }else{

			$this->session->set_flashdata('error', 'Message has not been send.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		    }

	}
	
	
public function reject_user_enrolement(){
	
		  $tab_value  = $_POST['tab_value'];
	
		  $school_id = $_POST['school_id'];

		  $user_id = $_POST['user_id'];
		  
		  $student_email = $_POST['student_email'];

		  $student_email_subject = $_POST['student_email_subject'];

		  $student_email_message = $_POST['student_email_message'];

		  
 		  $rejectedInfo	= $this->studentenrolement_model->get_single_student($user_id);
		  
		  $userwhere	= array('id'=>$user_id);
		 
		  $userdata		= array('is_deleted'=>'0','is_active'=>'0','user_type'=>'enrolementstudent');
 
		  $where		= array('user_id'=>$user_id);

	  	  $data			= array('student_enrolement_status'=>'rejected','student_postpone_date'=>'0000-00-00');

	
			if($this->studentenrolement_model->update($data,$where)){
				
			 $this->studentenrolement_model->updateUser($userdata,$userwhere);
			 
			 $to		= $student_email;

			 $subject	= $student_email_subject;

			 $message	= $student_email_message;

			 $this->authorize->send_email($to,$message,$subject);

  $this->session->set_flashdata('success', 'Message has been send successfully.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		     }else{

			$this->session->set_flashdata('error', 'Message has not been send.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		    }
				}
	

public function request_user_detail(){

		  $tab_value  = $_POST['tab_value'];
	
		  $school_id = $_POST['school_id'];

		  $user_id = $_POST['user_id'];

		  $student_email = $_POST['student_email'];

		  $student_email_subject = $_POST['student_email_subject'];

		  $student_email_message = $_POST['student_email_message'];
	
			 $to		= $student_email;

			 $subject	= $student_email_subject;

			 $message	= $student_email_message;

			$result		= $this->authorize->send_email($to,$message,$subject);
			
				if($result){ 
	$this->session->set_flashdata('success', 'Message has been send successfully.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		     }else{

			$this->session->set_flashdata('error', 'Message has not been send.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		    }
	}
	
	
public function schedule_user_detail(){

	
		  $tab_value  = $_POST['tab_value'];
	
		  $school_id = $_POST['school_id'];

		  $user_id = $_POST['user_id'];

		  $student_email = $_POST['student_email'];

		  $student_email_subject = $_POST['student_email_subject'];

		  $student_email_message = $_POST['student_email_message'];

		
 		
			$to			= $student_email;

			$subject	= $student_email_subject;

			$message	= $student_email_message;

			$result		= $this->authorize->send_email($to,$message,$subject);
			
			if($result){ 
				
			$this->session->set_flashdata('success', 'Message has been send successfully.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		     }else{

			$this->session->set_flashdata('error', 'Message has not been send.');

			if($tab_value=='applied')
					{
					redirect(base_url()."studentenrolement/index");exit;	
					}
					
				else if($tab_value=='rejected'){
					redirect(base_url()."studentenrolement/rejectedstudents");exit;	
				}
				else if($tab_value=='waiting'){
					redirect(base_url()."studentenrolement/waitingstudents");exit;	
				}
			  
		    }
	}
	
	

/************************************************** NEW FUNCTIONS STUDENT NOTES **************************************************************/

public function notesfor_user_enrolement(){
		
		$tab_value=$_POST['tab_value'];
		
		  $school_id 	= $_POST['school_id'];

		  $user_id 		= $_POST['user_id'];

		  $student_notes 	= $_POST['student_notes'];
		  
		  $where		= array('user_id'=>$user_id);

	  	  $data			= array('student_notes'=>$student_notes);
	
		if($this->studentenrolement_model->update($data,$where)){
			
			$this->session->set_flashdata('success', 'Notes has been saved successfully.');
			if($tab_value=="applied"){
			
				redirect(base_url()."studentenrolement/index");exit;
			
				}
			if($tab_value=="selected"){
			
				redirect(base_url()."studentenrolement/selectedstudents");exit;

			}
			if($tab_value=="rejected"){
			
				redirect(base_url()."studentenrolement/rejectedstudents");exit;

			}
			if($tab_value=="waiting"){
			
				redirect(base_url()."studentenrolement/waitingstudents");exit;

			}
		}
		else{ 
	
			$this->session->set_flashdata('error', 'Notes has not been saved successfully.');

			redirect(base_url()."studentenrolement/index");exit;

				
				}
	}
	
/**************** to edit student enrolment details ************************************/

public function edit($sid){


		if($sid==NULL){

			$this->session->set_flashdata('error', 'Select student first.');

			redirect(base_url()."students");exit;

		   }

		$data = array();

		$school_id 				= $this->session->userdata('user_school_id');

		$data['nationalities'] 	= $this->studentenrolement_model->getnationality();

	    $data['countries'] 		= $this->studentenrolement_model->getcountry();

		$data['religions'] 		= $this->studentenrolement_model->getreligion();

		$data['ethnicorigins'] 	= $this->studentenrolement_model->getethnicorigin();

		$data['branches'] 		= $this->studentenrolement_model->getbranches($school_id);
		 
		//$data['payment_receive_email'] 	= $this->studentenrolement_model->get_payment_email_info($school_id);
		 
		//$data['request_payment_email'] 	= $this->studentenrolement_model->get_request_email_info($school_id);
	
		$data['info']					= $this->studentenrolement_model->get_single_student($sid);

		$data['title']	      			= "Account detail";

		$data['error']					= $this->session->flashdata('error');

		$data['logmode']				= $this->logmode;

		$data['success']				= $this->session->flashdata('success');

		$this->load->view('header');

        $this->load->view('enrolement/editenrol_account_detail',$data);

	    $this->load->view('footer');

	}
	
	 public function edit_student($sid){

		// $postURL	= $this->createPostURL($_GET);

		 if($sid==NULL){

			$this->session->set_flashdata('error', 'Select student first.');

			redirect(base_url()."studentenrolement");exit;

		   }

		$whereuser						= array('id'=>$sid);

		$wherestudent					= array('user_id'=>$sid);

		$userdata 						= array();

		$studentdata 					= array();						

		$data							= array();

		$data['title']	      			= "Edit Account Details";

		$data['mode']					= "Edit";


			if (trim($this->input->post('password')) != '') {

                $userdata['password'] = trim($this->input->post('password'));

            }

			if (trim($this->input->post('email')) != '') {

                $userdata['email'] = trim($this->input->post('email'));

            }
			
			$userdata['updated_date'] = date('Y-m-d H:i:s');

			if (trim($this->input->post('student_fname')) != '') {
                $fname=trim($this->input->post('student_fname'));
                $studentdata['student_fname'] = trim($this->input->post('student_fname'));

            }


			if (trim($this->input->post('student_lname')) != '') {
                $lname=trim($this->input->post('student_lname'));
                $studentdata['student_lname'] = trim($this->input->post('student_lname'));

            }

			
			if (trim($this->input->post('student_gender')) != '') {

                $studentdata['student_gender'] = trim($this->input->post('student_gender'));

            }

			if (trim($this->input->post('student_dob')) != '') {

                  $dob = trim($this->input->post('student_dob'));

				  $studentdata['student_dob'] = date('Y-m-d', strtotime($dob));

            }


			if (trim($this->input->post('student_nationality')) !='') {

                $studentdata['student_nationality'] = trim($this->input->post('student_nationality'));

            }
		

			if (trim($this->input->post('student_country_of_birth')) != '') {

                $studentdata['student_country_of_birth'] = trim($this->input->post('student_country_of_birth'));

            }

			if (trim($this->input->post('student_first_language')) != '') {

                $studentdata['student_first_language'] = trim($this->input->post('student_first_language'));

            }

			if (trim($this->input->post('student_other_language')) != '') {

                $studentdata['student_other_language'] = trim($this->input->post('student_other_language'));

            }

			if (trim($this->input->post('student_religion')) != '') {

                $studentdata['student_religion'] = trim($this->input->post('student_religion'));

            }

			if (trim($this->input->post('student_ethnic_origin')) != '') {

                $studentdata['student_ethnic_origin'] = trim($this->input->post('student_ethnic_origin'));

            }
		
			if (trim($this->input->post('student_telephone')) != '') {

                $studentdata['student_telephone'] = trim($this->input->post('student_telephone'));

            }

			if (trim($this->input->post('student_address')) != '') {

                $studentdata['student_address'] = trim($this->input->post('student_address'));

            }

			if (trim($this->input->post('student_address_1')) != '') {

                $studentdata['student_address_1'] = trim($this->input->post('student_address_1'));

            }

			if (trim($this->input->post('student_father_name')) != '') {

                $studentdata['student_father_name'] = trim($this->input->post('student_father_name'));

            }

			if (trim($this->input->post('student_father_occupation')) != '') {

                $studentdata['student_father_occupation'] = trim($this->input->post('student_father_occupation'));

            }

			if (trim($this->input->post('student_father_mobile')) !='' ) {

                $studentdata['student_father_mobile'] = trim($this->input->post('student_father_mobile'));

            }

			if (trim($this->input->post('student_father_email')) != '') {

                $studentdata['student_father_email'] = trim($this->input->post('student_father_email'));

            }

			if (trim($this->input->post('student_mother_name')) != '') {

                $studentdata['student_mother_name'] = trim($this->input->post('student_mother_name'));

            }

			if (trim($this->input->post('student_mother_occupation')) != '') {

                $studentdata['student_mother_occupation'] = trim($this->input->post('student_mother_occupation'));

            }

			if (trim($this->input->post('student_mother_mobile')) != '') {

                $studentdata['student_mother_mobile'] = trim($this->input->post('student_mother_mobile'));

            }

			if (trim($this->input->post('student_mother_email')) != '') {

                $studentdata['student_mother_email'] = trim($this->input->post('student_mother_email'));

            }

			if (trim($this->input->post('student_emergency_name')) != '') {

                $studentdata['student_emergency_name'] = trim($this->input->post('student_emergency_name'));

            }

			if (trim($this->input->post('student_emergency_relationship')) != '') {

                $studentdata['student_emergency_relationship'] = trim($this->input->post('student_emergency_relationship'));

            }

			if (trim($this->input->post('student_emergency_mobile')) != '') {

                $studentdata['student_emergency_mobile'] = trim($this->input->post('student_emergency_mobile'));

            }

			if (trim($this->input->post('student_doctor_name')) != '') {

                $studentdata['student_doctor_name'] = trim($this->input->post('student_doctor_name'));

            }

			if (trim($this->input->post('student_doctor_mobile')) != '') {

                $studentdata['student_doctor_mobile'] = trim($this->input->post('student_doctor_mobile'));

            }

			if (trim($this->input->post('student_helth_notes')) != '') {

                $studentdata['student_helth_notes'] = trim($this->input->post('student_helth_notes'));

            }

			if (trim($this->input->post('student_allergies')) != '') {

                $studentdata['student_allergies'] = trim($this->input->post('student_allergies'));

            }

			if (trim($this->input->post('student_other_comments')) != '') {

                $studentdata['student_other_comments'] = trim($this->input->post('student_other_comments'));

            }

			if (trim($this->input->post('student_family_address')) != '') {

                $studentdata['student_family_address'] = trim($this->input->post('student_family_address'));

            }

			if ( count($this->input->post('student_sibling')) > 0 ) {

                $studentdata['student_sibling'] = implode(',',$this->input->post('student_sibling'));

            }


		if($this->studentenrolement_model->update($studentdata,$wherestudent)){

			$this->studentenrolement_model->updateUser($userdata,$whereuser);
	
			$this->session->set_flashdata('success', 'Enroled Student has been updated successfully.');

			redirect(base_url()."studentenrolement/edit/$sid");exit;

		 } else {

			$this->session->set_flashdata('error', 'Some problem exists. Enroled Student has not been updated.');

			redirect(base_url()."studentenrolement/edit/$sid");exit;

		   }


		$data['logmode']			= $this->logmode;

		$data['error']				= $this->session->flashdata('error');

		$data['success']			= $this->session->flashdata('success');

		$this->load->view('header');

		$this->load->view('studentenrolement/edit',$data);

		$this->load->view('footer');

	  }


	public function excel_action(){

	  		$branchid 			= $this->input->post('branch_data');
	
   			$search_condition	=	array();
			
			$exceltab	     	= 	$this->input->post('exceltab');
    
 		    $sdate_search     	= 	$this->input->post('sdate_search');
  
 			$edate_search     	= 	$this->input->post('edate_search');

			$branch_id			= 	$this->input->post('branchsrch_id');
			
			$school_id      	= 	$this->session->userdata('user_school_id');
			
			$currentdate 		= date('Y-m-d');
			
			if($branch_id!=''){
			
			$search_condition['student_enrolement.student_school_branch']	= $branch_id;
			
				}
			
			if($branchid!='' ){
		
			$search_condition['student_enrolement.student_school_branch']	= $branchid;
				}
	
	
			$search_condition['users.is_active']	= '1';
	
			$search_condition['users.is_deleted']	= '0';
			
			if($exceltab=="applied")
			{
				$search_condition['student_enrolement.student_enrolement_status']	= 'applied';
				
				$search_condition['users.user_type']	= 'enrolementstudent';
				
				$tabvalue='applied';
	
	
			}
			else if($exceltab=="selected"){
				
				$search_condition['student_enrolement.student_enrolement_status']	= 'selected';
				
				$search_condition['users.user_type']	= 'student';
				
				$tabvalue='selected';
				
				}
			else if($exceltab=="rejected"){
				
				$search_condition['student_enrolement.student_enrolement_status']	= 'rejected';
				
				$search_condition['users.user_type']	= 'enrolementstudent';
				
				$search_condition['users.is_active']	= '0';
				
				$tabvalue='rejected';
				
				}
			else if($exceltab=="waiting"){
				
				$search_condition['student_enrolement.student_enrolement_status']	= 'waiting';
				
				$search_condition['users.user_type']	= 'enrolementstudent';
				
				$tabvalue='waiting';
				
				}
				
			
			if($sdate_search!=''){
	
				$startdate	= date('Y-m-d', strtotime($sdate_search));
	
			} else {
	
				$startdate	= '';
	
			  }
	
			if($edate_search!=''){
	
				$enddate	= date('Y-m-d', strtotime($edate_search));
	
			} else {
	
				$enddate	= '';

		   }
		   
		 	$odr =   "student_enrolement.student_application_date";

		 	$dirc	= "desc";
    
		  	$student_data 		= $this->studentenrolement_model->getExcelData($search_condition,$odr,$dirc,$school_id,$startdate,$enddate,$currentdate,$tabvalue);
		  
			$this->load->library("Excel");
			
			$object 			= new PHPExcel();
			 
			$object->setActiveSheetIndex(0);
			 
			$table_columns = array("Firstname", "Lastname", "Gender","Date of Birth", "Contact details", "Email-id","Address","Fathername","Father's contact details","Father's Email","Mothername","Mother's contact details","Mother's Email","Branch","Class","Fee Band","Application date","Enrollement date","Leaving date","Postpone date");
			 
    		$column = 0;
     
    		foreach($table_columns as $field)
		    {
		     $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		     $column++;
		    }
    
      
		    $excel_row = 2;
		     
		    foreach($student_data as $row)
		    {
		     $id=$row->user_id;
		     
		     $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->student_fname);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->student_lname);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->student_gender);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->student_dob);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->student_telephone);
		      $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->email);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->student_address);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->student_father_name);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->student_father_mobile);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->student_father_email);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->student_mother_name);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->student_mother_mobile);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->student_mother_email);
		   /*     $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->student_school_branch);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->student_class_group);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row,$row->student_fee_band);*/
		    
		    if($row->student_school_branch==''){
		     $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, @$row->student_school_branch);
		     }else{
		     $student_school_branch=$row->student_school_branch;
		     $branch= $this->studentenrolement_model->getbranchName($student_school_branch);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row,@$branch->branch_name);
		     }
		    
		     if($row->student_class_group==''){
		     $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, @$row->student_class_group); 
		     }
		     else{
		     $student_class_group=$row->student_class_group;
		     $class = $this->studentenrolement_model->getclassName($student_class_group);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, @$class->class_name);
		     }
		     if($row->student_fee_band==''){
		     $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row,@$row->student_fee_band);
		     }
		     else{
		     $student_fee_band=$row->student_fee_band;
		     $fee = $this->studentenrolement_model->getFeedetail($student_fee_band);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, @$fee->fee_band_price);
		     }
		     
		     $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row->student_application_date);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->student_enrolment_date);
		     $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $row->student_leaving_date);
			 $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row->student_postpone_date);
		     $excel_row++;
		    
		    }
		     
		    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		    header('Content-Type: application/vnd.ms-excel');
		    header('Content-Disposition: attachment;filename="Student Data.xls"');
		    $object_writer->save('php://output');

		}

	function createPostURL($get){

		$start	= $this->uri->segment(4,0);

		$odrby	= $this->uri->segment(5,0);

		$dirc	= $this->uri->segment(6,"asc");

		$postURL	= "$start/$odrby/$dirc/?";

		foreach($get as $key=>$val){

			$postURL	.="$key=$val&";

		}

		return  $postURL;exit;

	}

}
