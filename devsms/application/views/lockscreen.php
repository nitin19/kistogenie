<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>School Management</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">

  </head>

  <body>



   <div class="loginpage">

   		<div class="container">

        	<div class="col-sm-12">

            	<div class="loginsection">

                	<div class="logodiv">

                    	<a href="http://kistogenie.com/sms/"><img class="logo" src="<?php echo base_url();?>assets/images/logo.PNG"></a>

                        </div>

                	<div class="loginheader">

                        <h1>Olive Tree Study Support</h1>

                    </div>

                    <div class="loginform-content">

                    

                    <form class="loginform" name="loginForm" id="loginForm" action="<?php echo base_url();?>/login" method="post">

                   

                    <div class="col-sm-12 nopadding revealinput">

                    	<!--<div class="col-sm-7 col-xs-7 revealinputbox">

  					<div class="form-group">

    				<label>Enter your realm</label>

    				<input type="text" class="form-control" id="enteryourrealm" placeholder="2 - 5 - 4 - 7">

  					</div>

                    </div>

                    	<div class="col-sm-5 col-xs-5 revealbtn">

  					<input type="button" class="btn btn-default" value="Take me there">

                    </div>-->

                    </div>

                    <div class="loginformarea">

                    	<div class="col-sm-12 logintitle nopadding">

                        <img src="<?php echo base_url();?>assets/images/dividers.png">

                        <h1>Log In to Dashboard</h1>

                        </div>

                        <div class="loginformblk">

                        <div class="form-group">

    					<label>Log In with your username</label>

    					<input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username;?>" readonly>

  						</div>

  						<div class="form-group">

    					<label for="exampleInputPassword1">Password</label>

    					<input type="password" class="form-control" name="userpass" id="userpass" placeholder="Password" value="">

  						</div>

  						

                        <div class="login-button">

  						<input type="submit" class="btn btn-default" value="Log In to Dashboard">

                        </div>

                        

                        </div>

                       

                    </div>

                    

					</form>

                    </div>

                    <div class="login-links">

                    	<div class="col-sm-8 col-xs-8 linkleft loginlinks nopadding">

                     	<span style="color:#7f8ea3">Sign-in with:<a href="http://kistogenie.com/devsms/"> Another User</a></span>

                        </div>

                    </div>

                </div>

            </div>

        </div>

   </div>

   <footer>

   <div class="container">

   <div class="copyright">

   <p>© <?php echo date("Y");?> Olive Tree Study Support. All Rights Reserved. | Powered by : <span><a href="http://www.binarydata.in/" target="_blank">Binary Data</a></span></p>

   </div>

   </div>

   </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->

     <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

     <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

      <script>

   jQuery(document).ready(function(){

	setTimeout(function() {

            jQuery('#flashMessage').slideUp('slow');

            }, 3000);

			

    jQuery("#loginForm").validate({

        rules: {

            username: "required",

            userpass: {

            required: true,

             }

        },

        messages: {

            username: "Please enter your username",

            userpass: {

                required: "Please enter your password",

              }

        },

        

        submitHandler: function(form) {

            form.submit();

        }

    });



  });

  

  </script>

  

 <style>

 #loginForm .error { color:#F00; }
 .loginform-content{min-height:300px;}
.loginlinks a:hover{color:#1FB4F0;}
 </style> 

  </body>

</html>