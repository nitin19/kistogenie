    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6  col-xs-12 foot_box">
            <a href="http://www.olivetreestudy.co.uk"><img src="<?php echo base_url();?>assets/img/logo.png" class="img-responsive"></a>
            <ul class="list-unstyled list_foot">
              <li><a href="tel:020 8914 8422" class="phone"><i class="fa fa-phone" aria-hidden="true"></i>020 8914 8422</a></li>
              <li><a href="mailto: info@olivetreestudy.co.uk" class="email"><i class="fa fa-envelope" aria-hidden="true"></i>info@olivetreestudy.co.uk</a></li>
            </ul>
            <ul class="list-inline list-unstyled list_media list_media1">
              <li><a href="#"><i class="fa fa-facebook fb" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter twit" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram inst" aria-hidden="true"></i></a></li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12 foot_box">
            <h3 class="menu_head">menu</h3>
            <ul class="list-unstyled list_home">
              <li><a href="http://www.olivetreestudy.co.uk">Home Page</a></li>
              <li><a href="#">Weekend School</a></li>
              <li><a href="#">Mum & Dad</a></li>
              <li><a href="#">Shop</a></li>
              <li><a href="#">Contact us</a></li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-6  col-xs-12">
            <div class="media">
                <div class="media-left">
                  <img src="<?php echo base_url();?>assets/img/img1.png" class="media-object">
                </div>
                <div class="media-body">
                  <a href="#">Tips to Present Ramadan To Your Childs’ Class</a>
                </div>
            </div>
            <div class="media media1">
                <div class="media-left">
                  <img src="<?php echo base_url();?>assets/img/img2.png" class="media-object">
                </div>
                <div class="media-body">
                  <a href="#">The Ultimate Ramadan Dua’ List – Shaykh Navaid Aziz</a>
                </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6  col-xs-12 ">
             <h3 class="menu_head sub_haed">Subscribe To Our Mailing List</h3>
             <input type="text" class="form-control form_name" name="" placeholder="Your Email">
             <button type="button" class="btn btn_submit">submit</button>
          </div>
        </div>
      </div>
    </footer>

    <section class="footer_last">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p class="foot_txt">Copyright © 2017,<span><a href="#">Olive Tree Study Support</a></span>, All Rights Reserved.</p>
          </div>
        </div>
      </div>
      <button type="button" class="btn btn_help"><i class="fa fa-comment" aria-hidden="true"></i> need help?</button>
    </section>



   <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

  <script type="text/javascript">
    $("#sel1").change(function(){
      var user = $(this).val();
      if(user == "student")
      {
        window.location.href = '<?php echo base_url();?>'+'Student_enrol';
      }
      else if(user == "staff")
      {
        window.location.href = '<?php echo base_url();?>'+'Staff_enrol';
      }
    });
  </script>
  </body>
  </html>
