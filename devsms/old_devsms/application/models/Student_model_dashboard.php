<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student_model_dashboard extends CI_Model {
	var $tbl	= "student";
	var $users = "users";
	var $country = "country";
	var $religion = "religion";
	var $ethnic_origin = "ethnic_origin";
	var $branch = "branch";
	var $fee_band = "fee_band";
	var $classes = "classes";
	var $information_schema = "INFORMATION_SCHEMA.COLUMNS";
	var $attendance = "attendance";
	var $term = "term";
	var $progressreport	= "progress_report";
	var $subjects	= "subjects";
	
	var $notification	= "notification";
	
	

    function __construct() {
        parent::__construct();
    }
	
		function get_single_student($user_id) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->join($this->users, $this->users.'.id = '.$this->tbl.'.user_id','INNER'); 
		  $this->db->where(array($this->tbl.'.user_id'=>$user_id));
          $query = $this->db->get();
          return  $query->row();
    }
	
	 function student_attendence($attendeceinterval, $user_id) {
        $this->db->select('*');
        $this->db->from($this->attendance);
		 $this->db->join($this->tbl, $this->tbl.'.student_id = '.$this->attendance.'.student_id','INNER');
		$this->db->where(array($this->tbl.".user_id="=>$user_id));
		if($attendeceinterval!=''){
			$this->db->where($attendeceinterval);
			}
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
    }
	
	function get_attendence($startingdate=null, $endingdate=null, $type=null, $user_id) {
	$this->db->select('*');
	$this->db->from($this->attendance);
	$this->db->join($this->tbl, $this->attendance.'.student_id = '.$this->tbl.'.student_id','INNER' );
	$this->db->where($this->tbl.'.user_id = ', $user_id);
	 if($type!=''){
	$this->db->where($this->attendance.'.attendance_status = ', $type);
	 }
	  if($startingdate!=''){
	$this->db->where('attendance_date >=', $startingdate);
	  }
	   if($endingdate!=''){
	$this->db->where('attendance_date <=', $endingdate);
	   }
	$query = $this->db->get();
	 $this->db->last_query(); 
	return $query->result();
}
	
	function  getStudentinfo($user_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return  $query->row();
	   }
	   
	   function  getprogressreport($school_id,$branch_id,$class_id,$user_id) {
        $this->db->select('*');
        $this->db->from($this->progressreport);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"user_id", $user_id));
        $query = $this->db->get();
        return  $query->result();
	   }
	   
	/*   function getTerms($school_id,$branch_id) {
        $this->db->select('*');
        $this->db->from($this->term);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$branch_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }*/
	
	function check_progress_report($student_id,$school_id,$branch_id,$class_id,$term,$year) {
		$this->db->select('*');
        $this->db->from($this->progressreport);
		$this->db->where(array("student_id="=>$student_id,"school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"term"=>$term,"progress_report_year"=>$year,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
	   }
	
	function getSubjects($school_id,$branch_id,$class_id) {
        $this->db->select('*');
        $this->db->from($this->subjects);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }
	
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id="=>$branch_id));
        $query = $this->db->get();
        return  $query->row();
    }
	
	function gettodayattendence($user_id) {
          $this->db->select('*');
          $this->db->from($this->attendance);
		  $this->db->join($this->tbl, $this->attendance.'.student_id = '.$this->tbl.'.student_id','INNER'); 
		  $this->db->where(array($this->tbl.'.user_id'=>$user_id));
		  $this->db->where('attendance_date', date("Y-m-d"));
          $query = $this->db->get();
		  $this->db->last_query(); 
          return  $query->row();
    }
	
	 function  getStudentterm($term_id,$student_id) {
        $this->db->select('*');
        $this->db->from($this->progressreport);
		$this->db->where(array("term="=>$term_id,"student_id"=>$student_id));
        $query = $this->db->get();
        return  $query->num_rows();
	   }
	   
	   
	   function totalnotification($user_id){
		$this->db->select('*');
		$this->db->from($this->notification);
		$this->db->where(array('to'=>$user_id,'is_active'=>'1','is_deleted='=>'0'));
		$query=$this->db->get();
		return  $query->num_rows();
		}

   	function show_notification($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL){
		$this->db->select('*');
		$this->db->from($this->notification);
		$this->db->where($where);
		$this->db->order_by("notification_id", "desc"); 
		$this->db->limit($limit, $start);	
		$query=$this->db->get();
		return  $query->result();
		}
		
	function getsenderName($sender_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('id'=>$sender_id,'is_deleted='=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getsenderimage($sender_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('id'=>$sender_id,'is_deleted='=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	function delete($notificationid){
		$where		= array('notification_id' => $notificationid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->notification, $data))
			return true;
		 else
			return false;
	   }
		
/*********************** changes after branch_id removed ********************************/
   function getTerms($school_id) {
        $this->db->select('*');
        $this->db->from($this->term);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }
		
}
