<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Master_model extends CI_Model {
	
	//var $username	= "username";
	//var $password	= "password";

    function __construct() {
        parent::__construct();
    }
	
	function get_branch($school_id){
	 $this->db->select('*');
        $this->db->from('branch');
		$this->db->where(array("school_id"=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
		 $query = $this->db->get();
		$rr= $this->db->last_query(); 
        $result = $query->result_array();
    return $result;
	}
 
 function get_role($school_id){
	 $this->db->select('*');
        $this->db->from('teacher_role');
		$this->db->join('branch', 'branch.branch_id = teacher_role.branch_id','INNER' );
		$this->db->where(array("teacher_role.school_id"=>$school_id,"teacher_role.is_deleted="=>'0', ));
		 $query = $this->db->get();
		$rr= $this->db->last_query(); 
        $result1 = $query->result_array();
    return $result1;
	}
	
		function insert_teach_role($roledata){
		if($this->db->insert('teacher_role', $roledata))
			return $this->db->insert_id();
		  else
			return false;		
	}
    function get_teachtype_branch($school_id){
	     $this->db->select('*');
        $this->db->from('branch');
		$this->db->where(array("school_id"=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
		 $query = $this->db->get();
		$rr= $this->db->last_query(); 
        $result = $query->result_array();
    return $result;
	}
	function insert_teach_type($typedata){
		$this->db->insert('teacher_type', $typedata);
				
	}
	 function get_teachtype_role($school_id){
	 $this->db->select('*');
        $this->db->from('teacher_type');
		$this->db->join('branch', 'branch.branch_id = teacher_type.branch_id','INNER' );
		$this->db->where(array("teacher_type.school_id"=>$school_id,"teacher_type.is_deleted="=>'0', ));
		 $query = $this->db->get();
		$rr= $this->db->last_query(); 
        $result1 = $query->result_array();
    return $result1;
	}
}
