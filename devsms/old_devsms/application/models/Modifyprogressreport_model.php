<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Modifyprogressreport_model extends CI_Model {
	var $tbl	= "progress_report";
	var $attendance	= "attendance";
	var $users = "users";
	var $student = "student";
	var $branch = "branch";
	var $classes = "classes";
	var $staff = "staff";
	var $subjects = "subjects";
	var $term = "term";
	
	
    function __construct() {
        parent::__construct();
    }
	
	
	function  getStudentinfo($student_id) {
        $this->db->select('*');
        $this->db->from($this->student);
		$this->db->where('student_id', $student_id);
        $query = $this->db->get();
        return  $query->row();
	   }
	
 function getbranchName($student_school_branch) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("branch_id="=>$student_school_branch,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getclassName($student_class_group) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("class_id="=>$student_class_group,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }	
	
	
	
	 function get_total_Attendancestatus($studentid) {
		     $this->db->select('*');
		     $this->db->from($this->attendance);
			 $this->db->where('student_id', $studentid);
			 $query = $this->db->get();
			  $this->db->last_query();
             return  $query->num_rows();
	         }
			 
	
	 function get_present_Attendancestatus($studentid) {
		     $this->db->select('*');
		     $this->db->from($this->attendance);
			 $this->db->where('student_id', $studentid);
			 $this->db->where('attendance_status', '1');
			 $query = $this->db->get();
			  $this->db->last_query();
             return  $query->num_rows();
	         }
	  
	   function get_absent_Attendancestatus($studentid) {
		     $this->db->select('*');
		     $this->db->from($this->attendance);
			 $this->db->where('student_id', $studentid);
			 $this->db->where('attendance_status', '2');
			 $query = $this->db->get();
			  $this->db->last_query();
             return  $query->num_rows();
	         } 
			 
	 function get_late_Attendancestatus($studentid) {
		     $this->db->select('*');
		     $this->db->from($this->attendance);
			 $this->db->where('student_id', $studentid);
			 $this->db->where('attendance_status', '3');
			 $query = $this->db->get();
			  $this->db->last_query();
             return  $query->num_rows();
	         } 
	
	function getSubjects($school_id,$branch_id,$class_id) {
        $this->db->select('*');
        $this->db->from($this->subjects);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }
	
	
	function get_subject_name($subjectid,$school_id,$branch_id,$class_id) {
        $this->db->select('*');
        $this->db->from($this->subjects);
		$this->db->where(array("subject_id="=>$subjectid,"school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
         return  $query->row();
    }
	
	
	function getTerms($school_id) {
        $this->db->select('*');
        $this->db->from($this->term);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query();
        return  $query->result();
    }


 function check_progress_report($student_id,$school_id,$branch_id,$class_id,$term,$year) {
		$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("student_id="=>$student_id,"school_id="=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"term"=>$term,"progress_report_year"=>$year,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query(); 
        return  $query->result();
	   }
	
	
		 
	 
function updateReport($input){
  	$student_id  = 	$input['student_id'];
    $school_id  =    $input['school_id'];;
	$branch_id   = 	$input['branch_id'];
	$class_id    = 	$input['class_id'];
	$term       = 	$input['term'];
	$subject_id  = 	$input['subject_id'];
	$effort  = 	$input['effort'];
	$behaviour  = 	$input['behaviour'];
	$homework  = 	$input['homework'];
	$proofreading  = 	$input['proofreading'];
	$comment  = 	$input['comment'];
	
	$pgryear = date('Y');
	$created_by = $this->session->userdata('user_id');
	$created_date = date('Y-m-d H:i:s');;
	$updated_date = date('Y-m-d H:i:s');
	$is_active = '1';
	
	$data =array();
	for ($i = 0; $i < count($subject_id); $i++) {
		 $sv = $subject_id[$i];
		 
		 $data[$i] = array(
		   'student_id' => $student_id, 
           'school_id' => $school_id, 
           'branch_id' => $branch_id,
           'class_id' => $class_id,
		   'term' => $term,
           'subject_id' => $sv,
		   'effort' => $effort[$sv], 
           'behaviour' => $behaviour[$sv],
           'home_work' => $homework[$sv],
		   'proof_reading' => $proofreading[$sv],
           'comment' => $comment[$sv],
		   'progress_report_year' => $pgryear,
		   'is_active' => $is_active,
		   'created_by' => $created_by, 
           'created_date' => $created_date,
           'updated_date' => $updated_date,
          );
	}
	
	
	$this->db->where(array('student_id' => $student_id,'school_id' => $school_id, 'branch_id' => $branch_id,'class_id' => $class_id, 'term' => $term,'progress_report_year' => $pgryear));
	
		if($this->db->update_batch($this->tbl, $data, 'subject_id')) {
		//echo $this->db->last_query();exit;
		return true;
		} else {
		//echo $this->db->last_query();exit;
		return false;
		     }
	  }
	  
	  
 function insertReport($input){
  	$student_id  = 	$input['student_id'];
    $school_id  =    $input['school_id'];;
	$branch_id   = 	$input['branch_id'];
	$class_id    = 	$input['class_id'];
	$term       = 	$input['term'];
	$subject_id  = 	$input['subject_id'];
	$effort  = 	$input['effort'];
	$behaviour  = 	$input['behaviour'];
	$homework  = 	$input['homework'];
	$proofreading  = 	$input['proofreading'];
	$comment  = 	$input['comment'];
	
	$pgryear = date('Y');
	$created_by = $this->session->userdata('user_id');
	$created_date = date('Y-m-d H:i:s');;
	$updated_date = date('Y-m-d H:i:s');
	$is_active = '1';
	
	$data =array();
	for ($i = 0; $i < count($subject_id); $i++) {
		 $sv = $subject_id[$i];
		 
		 $data[$i] = array(
		   'student_id' => $student_id, 
           'school_id' => $school_id, 
           'branch_id' => $branch_id,
           'class_id' => $class_id,
		   'term' => $term,
           'subject_id' => $sv,
		   'effort' => $effort[$sv], 
           'behaviour' => $behaviour[$sv],
           'home_work' => $homework[$sv],
		   'proof_reading' => $proofreading[$sv],
           'comment' => $comment[$sv],
		   'progress_report_year' => $pgryear,
		   'is_active' => $is_active,
		   'created_by' => $created_by, 
           'created_date' => $created_date,
           'updated_date' => $updated_date,
          );
	}
	
		//$this->db->insert_batch($this->tbl, $data); 
		if($this->db->insert_batch($this->tbl, $data))
			return $this->db->insert_id();
		   else
			return false;
		 //echo $this->db->last_query();exit;
     }
   
   


  	 
}



 
