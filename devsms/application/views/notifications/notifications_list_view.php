 <div class="editprofile-content">
      <div class="col-sm-12 profilemenus nopadding">
     <div class="col-sm-9 col-xs-8 nopadding menubaritems">
         <ul>
		    <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>
        <li class="edit">Notifications</li>        
        </ul>
        </div>
        </div>

          <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div> 	

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-12 col-xs-7 nopadding profile-bg accsection">

        <h1>Notifications</h1>
        
        <div class="tablewrapper">
  
     <table class="table-bordered table-striped">
				<thead>
				  <tr class="headings">

					  <th class="column3">Sender</th>

                      <th class="column5">Sender Name & Subject</th>
                      
                       <th class="column7">Message</th>
                   
					  <th class="column3">Date</th>

					  <th class="column2">Actions</th>

				  </tr>

			  </thead>

				<tbody>
                      <?php 
					  if(count($data_rows) > 0){
					      $sr=$last_page;
					  foreach($data_rows as $notification) { 
					     $sr++;
						 $this->load->model(array('notifications_model')); 
						 $userInfo = $this->notifications_model->getuserInfo($notification->from,$notification->school_id);
					     if($userInfo->user_type=='teacher') {
						       $path = 'staff';
							   $Info = $this->notifications_model->getStaffInfo($notification->from,$notification->school_id);
							   $Name = $Info->staff_fname.' '.$Info->staff_lname;
					         } elseif($userInfo->user_type=='student') {
								$path = 'student';
							   $Info = $this->notifications_model->getStudentInfo($notification->from,$notification->school_id);
							   $Name = $Info->student_fname.' '.$Info->student_lname;
							  } else {
								$path = 'admin';
							    $Info = $this->notifications_model->getAdminInfo($notification->from,$notification->school_id);
								$Name = $Info->school_contact_person;
							    }
					if($notification->read_by!='') {
						 $readStatus = 'readed_notification';
					   } else {
						 $readStatus = 'unreaded_notification';
					    }
						
						 ?>
                         
                        <tr class="familydata communication">

						<td class="column3">
                        <div class="imgsec">
                        <?php  if($userInfo->profile_image!='') { ?>
                 <img src="<?php echo base_url();?>uploads/<?php echo $path;?>/<?php echo $userInfo->profile_image;?>">
                        <?php } else { ?>
				 <img src="<?php echo base_url();?>assets/images/avtar.png">
                         <?php } ?>
                        </div>
                        </td>
                        
                        <td class="column5">
                        <div class="contentsec">
                        <p><?php echo $Name;?></p>
                           <?php if($notification->subject!='') { ?>
                        <span><?php echo substr($notification->subject, 0,70); if(strlen($notification->subject) > 70 ) { echo '...'; }?></span>
                             <?php } else { ?>
                        <span style="color:transparent;">-</span>
                            <?php } ?>
                        </div>
                        </td>
                        
                        <td class="column7">
                        <div class="contentsec">
                         <?php if($notification->message!='') { ?>
                         <span><?php echo substr($notification->message, 0,140); if(strlen($notification->message) > 140 ) { echo '...'; }?></span>
                         <?php } else { ?>
                          <span style="color:transparent;">-</span>
                            <?php } ?>
                        </div>
                        </td>

						<td class="column3">
		<?php echo  date('d M Y', strtotime($notification->created_date)).' at '.date('H:i A', strtotime($notification->created_date));?></td>
                        <td class="column2 spacing">
                        
        						<a class="viewmessage" id="viewmsg" data-value="<?php echo $notification->notification_id;?>" data-toggle="modal" data-target="#viewModal<?php 																				 											echo $notification->notification_id;?>"><i class="fa fa-eye icnclass"></i></a>
                        
             <!--<a class="deletemsg" onclick="delConfirm('')"><i class="fa fa-trash-o icnclass"></i></a>-->
                     
                        </td>

                        </tr>        


<!--*********************************POPUP TO VIEW NOTIFICATION ***************************-->
     <div id="viewModal<?php echo $notification->notification_id;?>" class="modal" style="z-index:99999999999">
        
          <!-- Modal content -->
          <div class="modal-content">
          <div class="modalbg">
                <div class="col-sm-12 nopadding" style=" background:#36b047 none repeat scroll 0 0;">
                	<div class="col-sm-8 nopadding viewmodal"><h3>View Message</h3></div>
                    <div class="col-sm-4 btncls"><button type="button" class="close closebtn" data-dismiss="modal" aria-hidden="true">&times;</button></div>
                 </div>

                    	<div class="col-sm-12 msgbox">
                        	
                        	<div class="col-sm-12 nopadding">
                            <div class="col-sm-3 nopadding popup">
                            <div class="imgsec modalimg" >
                        <?php  if($userInfo->profile_image!='') { ?>
                 <img src="<?php echo base_url();?>uploads/<?php echo $path;?>/<?php echo $userInfo->profile_image;?>">
                        <?php } else { ?>
				 <img src="<?php echo base_url();?>assets/images/avtar.png">
                         <?php } ?>
                        </div>
                              <!--<label for="sender">From:</label>-->
                            </div>
                            <div class="col-sm-9 nopadding popup">
                           <?php  $sender_id = $notification->from;
						$this->load->model(array('notifications_model'));
						$senderName = $this->notifications_model->getsenderName($sender_id);?>
							<?php
							
							 if($userInfo->user_type=='teacher') {
						       $path = 'staff';
							   $Info = $this->notifications_model->getStaffInfo($notification->from,$notification->school_id);
							   $Name = $Info->staff_fname.' '.$Info->staff_lname;
					         } elseif($userInfo->user_type=='student') {
								$path = 'student';
							   $Info = $this->notifications_model->getStudentInfo($notification->from,$notification->school_id);
							   $Name = $Info->student_fname.' '.$Info->student_lname;
							  } else {
								$path = 'admin';
							    $Info = $this->notifications_model->getAdminInfo($notification->from,$notification->school_id);
								$Name = $Info->school_contact_person;
							    }
							
								echo $Name;
							 	echo"<br>";						
	    						echo $senderName->email;
							?>
                             </div>
                             </div>
                             <div class="col-sm-12 nopadding">
                                 <div class="col-sm-3 nopadding popup">
                              <label for="subject">Date:</label>
                            </div>
                            <div class="col-sm-9 nopadding popup">
                            	
								<?php 							
                            $senddate=$notification->created_date;
							
							echo $senddate=date('d M Y', strtotime($senddate));	?>
							
                                
                             </div>
                             </div>
                             
                             <div class="col-sm-12 nopadding">
                                 <div class="col-sm-3 nopadding popup">
                              <label for="subject">Subject:</label>
                            </div>
                            <div class="col-sm-9 nopadding popup">
                            	
								<?php echo $notification->subject;?>
							
                                
                             </div>
                             </div>
                             <div class="col-sm-12 nopadding">
                             <div class="col-sm-3 nopadding popup">
                        		<label for="message">Message:</label>
                              </div>
                              <div class="col-sm-9 nopadding popup"> 
                        		<?php echo $notification->message;?>
                        	  </div>
                              </div>
                              <div class="col-sm-12 nopadding">
                                  <div class="col-sm-3 nopadding popup" style="padding:10px !important;">
                              <label for="attachment">Attachments:</label>
                            </div>
                <div class="col-sm-9 nopadding popup">
                            	
         <ul class="attachment_section">
         <?php 
		
		 if($notification->attachments!='') {
			
			 $attachments =  explode(',',$notification->attachments);
			
			 
			 foreach($attachments as $attachment) { ?>
           <li>
                <h4><span class="attachmentheading"><?php 
                  $string = $attachment;
				  echo substr($string,0,10); if(strlen($string) > 10 ) { echo '...'; }
                
	              ?></span>
                    <div class="icon_section">
                  <a href="<?php echo base_url();?>notifications/download_attachments/<?php echo $attachment;?>" data-toggle="tooltip" title="Download" data-placement="top"><i class="fa fa-download"></i></a>
        
                  <a href="<?php echo base_url();?>uploads/notification/<?php echo $attachment;?>" target="_blank" data-toggle="tooltip" title="View" data-placement="top"><i class="fa fa-eye"></i></a>
                  </div>
                  </h4>
               </li> 
			<?php }
		   } else {
		      echo 'No attachments';
		    }
		 ?>
         </ul>
       
                             </div>
                             </div>
							</div>
</div>
              </div>  
	</div>
                 
				      <?php	}		
					} else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px;height:60px;font-size:18px; background:#FFF;  color: #6a7a91;">You have no notifications.</th></tr>	
				   <?php } ?>
                  
             	</tbody>
		     </table> 
             
   <div class="profile-bg">
	<div class="col-sm-12 paginationdiv nopadding">
    <div class="col-sm-8 col-xs-6 paginationblk">
         <?php  if(count($data_rows) > 0){
			       $last_page1=$last_page;
			   ?>
			Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries.
			<?php  } ?>
	<ul class="pagination">
		<?php echo $pagination;?>
	    </ul>
    </div>
	  </div>
    </div>
 	
               </div>

             </div>

          </div>

      </div>




 
 <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>  

<script>
  jQuery(document).ready(function(){
	  
	   jQuery("#branch").on('click', function(e){ 
   		  var branch_id = jQuery(this).val();
		  if( branch_id!='' && branch_id!='null' ) {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/getclass',
            data:{ 'branch_id': branch_id },
            success :  function(response) {
				        response=jQuery.parseJSON(response);
						jQuery('#class_name').empty();
					    jQuery('#class_name').html(response.datacls);
						jQuery('#student_name').empty();
					    jQuery('#student_name').html(response.datastu);
			          }
				   });
				 } else {
				 return false;
				 }
			 }); 
			 
	 jQuery("#class_name").on('click', function(e){ 
   		  var class_id = jQuery(this).val();
		   var branch_id = jQuery('#branch').val();
		  if(branch_id!='' && class_id!='' && branch_id!='null' && class_id!='null') {
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/get_students',
            data:{ 'branch_id': branch_id, 'class_id':class_id},
            success :  function(resp) {
						jQuery('#student_name').empty();
					    jQuery('#student_name').html(resp);
			          }
				   });
				 } else {
				 return false;
				 }
			 }); 
   });
</script>  
<style>
.fa-question-circle + .tooltip > .tooltip-inner {background-color:#090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

</style>

<style>
.spacing {
    padding: 0;
}
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index:2147483647 !important; /* Sit on top */
    padding-top: 50px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
	/*padding: 20px;
 	border: 10px solid #579b4a;*/
    width: 600px;
    top: 80px;
}


/*.closebtn{
    background: #fff none repeat scroll 0 0 !important;
    border-radius: 40px;
    float: right;
    height: 40px;
    opacity: 1;
    width: 40px;
	color:#36b047;
}*/
.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.popup{
	padding:10px;
}

.compose{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
    padding:5px 30px;
	float:right;
}

.composeclose{
padding:8px 0px 0px 0px;
}
.btncls{padding-top:5px;}

.modalbg {
    background: #fff none repeat scroll 0 0;
    float: left;
    width: 100%;
	border-radius:5px;
	}
.imgsec.modalimg > img {
    border-radius: 70px;
}
.viewmodal> h3 {
 	padding:0 0 0 20px;
   color: #fff;
   text-transform: uppercase;
}
.close.closebtn{
	font-size:30px;
}
.attachmentheading{float:left; line-height:0px;}
.icon_section{float:right;}
.attachment_section{list-style:none; padding:0px;}

</style>