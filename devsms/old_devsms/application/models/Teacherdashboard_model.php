<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
	
class Teacherdashboard_model extends CI_Model {
	
		var $tbl	= "notification";
		var $users	= "users";
		var $student="student";
		var $classes= "classes";
		var $staff 	="staff";
		var $attendance="attendance";
	   function __construct() {
        parent::__construct();
    }
	
	function getall_id($school_id,$user_id){
		$this->db->select('*');
        $this->db->from($this->users);
        $this->db->where(array('school_id'=>$school_id,'is_deleted='=>'0','id!='=>$user_id, 'is_active'=>'1'));
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
	    return  $query->result();
	}
	
	function admin($school_id){
		$this->db->select('*');
        $this->db->from($this->users);
        $this->db->where(array('school_id'=>$school_id,'is_deleted='=>'0','user_type='=>'admin'));
        $query = $this->db->get();
		//$this->db->last_query();exit();
	    return  $query->row();
	}
	
	function getall_staff($school_id,$user_id){
		$this->db->select('*');
        $this->db->from($this->users);
        $this->db->where(array('school_id'=>$school_id,'is_deleted='=>'0','user_type='=>'teacher','id!='=>$user_id, 'is_active'=>'1'));
        $query = $this->db->get();
		//echo $this->db->last_query();exit();
	   return  $query->result();
	}
	function getall_student($school_id){
		$this->db->select('*');
        $this->db->from($this->users);
        $this->db->where(array('school_id'=>$school_id,'is_deleted='=>'0','user_type='=>'student', 'is_active'=>'1'));
        $query = $this->db->get();
		//echo $this->db->last_query();exit();
	   return  $query->result();
	}
	
	function getuser_id($emails) {
		$emailsarray = explode(',',$emails);
		$array = array_filter(array_map('trim', $emailsarray));
        $this->db->select('*');
        $this->db->from($this->users);
        $this->db->where_in('email', $array);
        $query = $this->db->get();
	    $this->db->last_query(); 
	    return  $query->result();
    }
	
	function getsenderName($sender_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('id'=>$sender_id,'is_deleted='=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	function getsenderimage($sender_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('id'=>$sender_id,'is_deleted='=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
		function insert_notification($notification){
		if($this->db->insert($this->tbl, $notification))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit();
	}
	
	function show_notification($user_id,$limit=NULL, $offset=NULL){
		$this->db->limit($limit, $offset);
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where(array('to'=>$user_id,'is_deleted='=>'0'));
		$query=$this->db->get();
		return  $query->result();
		}
		
	function totalnotification($user_id){
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where(array('to'=>$user_id,'is_deleted='=>'0'));
		$query=$this->db->get();
		return  $query->num_rows();
		}
		


	function attendance_schoolid($user_id){
		$this->db->select('*');
		$this->db->from($this->staff);
		$this->db->where(array('user_id'=>$user_id));
		$query = $this->db->get();
		return   $query->row();
		}
		
	
	function attendance_classid($user_id){
		$this->db->select('*');
		$this->db->from($this->staff);
		$this->db->where(array('user_id'=>$user_id));
		$query=$this->db->get();
		return   $query->row();
		}
		
	function leaving_getclassName($class) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where_in('class_id', $class);
        $query = $this->db->get();
       return  $query->row();
    }
		
	function get_classname($cid){
		$this->db->select('*');
		$this->db->from($this->classes);
		$this->db->where('class_id',$cid);
		$query=$this->db->get();
		//echo $this->db->last_query();	exit();
		return   $query->row();
		}

	 	function attendance_getstudent($cid){
		$this->db->select('*');
		$this->db->from($this->student);
		$this->db->where('student_class_group',$cid);
		$query=$this->db->get();
//		echo $this->db->last_query();	exit();
		return $query->num_rows();
	
	
		}
	
	
	 function attendance_present($user_id,$cid){
		
		 $this->db->select('attendance_status');
		 $this->db->from($this->attendance); 
		 $this->db->where(array('created_by'=>$user_id,'attendance_status'=>1,'class_id'=>$cid));
		 $this->db->where('attendance_date', date("Y-m-d"));
		 $query=$this->db->get();
		//echo $this->db->last_query();exit();
		return $query->num_rows();
	 }
	 
	function attendance_absent($user_id,$cid){
		 $this->db->select('attendance_status');
		 $this->db->from($this->attendance); 
		 $this->db->where(array('created_by'=>$user_id,'attendance_status'=>2,'class_id'=>$cid));
		  $this->db->where('attendance_date',date("Y-m-d"));
		 $query=$this->db->get();
		//echo $this->db->last_query();exit();
		 return $query->num_rows();
		 }
		 
	function attendance_late($user_id,$cid){
		 $this->db->select('attendance_status');
		 $this->db->from($this->attendance); 
		 $this->db->where(array('created_by'=>$user_id,'attendance_status'=>3,'class_id'=>$cid));
		 $this->db->where('attendance_date',date("Y-m-d"));
		 $query=$this->db->get();
		//echo $this->db->last_query(); exit();
		return $query->num_rows();
		 
		 }  
		 
	function recentaddstudent($school_id) {
	 	$this->db->select('*');
        $this->db->from($this->student);
        $this->db->join('users', 'users.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id,"users.is_deleted="=>'0'));
		$this->db->order_by($this->student.".student_id", "desc"); 
		$this->db->limit('5');
		$query = $this->db->get();
		//echo $this->db->last_query();exit();
        return $query->result();
}

	function leavingstudent($school_id, $where) {
	 $this->db->select('*');
        $this->db->from($this->student);
        $this->db->join('users', 'users.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id,"users.is_deleted="=>'0'));
		if($where!=''){
		$this->db->where($where, null, false);
		}
		$this->db->order_by($this->student.".student_id", "desc"); 
		$this->db->limit('5');
		 $query = $this->db->get();
		 $this->db->last_query(); 
        return $query->result();
}

	function getclassName($class) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where_in('class_id', $class);
        $query = $this->db->get();
       return  $query->row();
    }
	function delete($notificationid){
		$where		= array('notification_id' => $notificationid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		 else
			return false;
	   }
	   
	function classstudents($cid) {
        $this->db->from($this->student);
        $this->db->join($this->users, $this->users.'.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.student_class_group'=>$cid, $this->users.".is_deleted="=>'0'));
        $query = $this->db->get();
		return $query->num_rows();
	 
    }
	 public function autocomplete($term, $school_id){
		 $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('school_id'=>$school_id, 'is_deleted'=>'0', 'is_active'=>'1'));
		$this->db->like('email', $term);
		 $this->db->last_query(); 
        $query = $this->db->get();
		return $query->result();
	 }
	 
	 
	 
}
	