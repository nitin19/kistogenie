<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentenrolementdashboard extends CI_Controller {
	
	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model', 'studentenrolementdashboard_model'));
		$this->load->database();
        $this->load->library('session');
    }

	

	public function index(){
		
		 $data = array();
		 $school_id = $this->session->userdata('user_school_id');
		 $user_id		=   $this->session->userdata('user_id');
		 $data['nationalities'] = $this->studentenrolementdashboard_model->getnationality();
	     $data['countries'] = $this->studentenrolementdashboard_model->getcountry();
		 $data['religions'] = $this->studentenrolementdashboard_model->getreligion();
		 $data['ethnicorigins'] = $this->studentenrolementdashboard_model->getethnicorigin();
		 $data['branches'] = $this->studentenrolementdashboard_model->getbranches($school_id);
		
		$data['info']				    = $this->studentenrolementdashboard_model->get_single_student($user_id);
		$data['title']	      			= "Student enrolement dashboard";
		$data['error']					= $this->session->flashdata('error');
		$data['logmode']				= $this->logmode;
		$data['success']				= $this->session->flashdata('success');
		
		$this->load->view('header_enrollment');
		$this->load->view('enrolement/student_enrolement_dashboard', $data);
		$this->load->view('footer');

	}  
	
	
		
	 public function edit_student($sid){
		 $postURL	= $this->createPostURL($_GET);
		 if($sid==NULL){
			$this->session->set_flashdata('error', 'Select student first.');
			redirect(base_url()."studentenrolementdashboard");exit;
		   }
		
		$whereuser						= array('id'=>$sid);
		$wherestudent					= array('user_id'=>$sid);
		
		$userdata 						= array();
		$studentdata 					= array();						
		$data							= array();
		$data['title']	      			= "Edit Account Details";
		$data['mode']					= "Edit";
           
			
			/*if (trim($this->input->post('password')) != '') {
                $userdata['password'] = trim($this->input->post('password'));
            }*/
			
			if (trim($this->input->post('email')) != '') {
                $userdata['email'] = trim($this->input->post('email'));
            }
			
			if($_FILES['profile_image']!='') {
				$config['upload_path']   = './uploads/student/'; 
				$config['allowed_types'] =  'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload("profile_image")){
				$data = array('upload_data' => $this->upload->data());
				$image_name = $data['upload_data']['file_name'];
				if($_FILES['profile_image']['error'] == 0 ){
					$file_name					= $data['upload_data']['file_name'];
					$userdata['profile_image']		= $file_name;
					 }
				  }
			   }
			
			 $userdata['updated_date'] = date('Y-m-d H:i:s');
			
			 
			if (trim($this->input->post('student_fname')) != '') {
                $studentdata['student_fname'] = trim($this->input->post('student_fname'));
            }
			
			if (trim($this->input->post('student_lname')) != '') {
                $studentdata['student_lname'] = trim($this->input->post('student_lname'));
            }
			
			if (trim($this->input->post('student_gender')) != '') {
                $studentdata['student_gender'] = trim($this->input->post('student_gender'));
            }
			
			if (trim($this->input->post('student_dob')) != '') {
                  $dob = trim($this->input->post('student_dob'));
				  $studentdata['student_dob'] = date('Y-m-d', strtotime($dob));
            }
			
			if (trim($this->input->post('student_nationality')) !='') {
                $studentdata['student_nationality'] = trim($this->input->post('student_nationality'));
            }
				
			if (trim($this->input->post('student_country_of_birth')) != '') {
                $studentdata['student_country_of_birth'] = trim($this->input->post('student_country_of_birth'));
            }
			
			if (trim($this->input->post('student_first_language')) != '') {
                $studentdata['student_first_language'] = trim($this->input->post('student_first_language'));
            }
			
			if (trim($this->input->post('student_other_language')) != '') {
                $studentdata['student_other_language'] = trim($this->input->post('student_other_language'));
            }
			
			if (trim($this->input->post('student_religion')) != '') {
                $studentdata['student_religion'] = trim($this->input->post('student_religion'));
            }
			
			if (trim($this->input->post('student_ethnic_origin')) != '') {
                $studentdata['student_ethnic_origin'] = trim($this->input->post('student_ethnic_origin'));
            }
			
			if (trim($this->input->post('student_telephone')) != '') {
                $studentdata['student_telephone'] = trim($this->input->post('student_telephone'));
            }
			
			if (trim($this->input->post('student_address')) != '') {
                $studentdata['student_address'] = trim($this->input->post('student_address'));
            }
			
			if (trim($this->input->post('student_address_1')) != '') {
                $studentdata['student_address_1'] = trim($this->input->post('student_address_1'));
            }
			
			if (trim($this->input->post('student_father_name')) != '') {
                $studentdata['student_father_name'] = trim($this->input->post('student_father_name'));
            }
			
			if (trim($this->input->post('student_father_occupation')) != '') {
                $studentdata['student_father_occupation'] = trim($this->input->post('student_father_occupation'));
            }
			
			if (trim($this->input->post('student_father_mobile')) !='' ) {
                $studentdata['student_father_mobile'] = trim($this->input->post('student_father_mobile'));
            }
				
			if (trim($this->input->post('student_father_email')) != '') {
                $studentdata['student_father_email'] = trim($this->input->post('student_father_email'));
            }
			
			if (trim($this->input->post('student_mother_name')) != '') {
                $studentdata['student_mother_name'] = trim($this->input->post('student_mother_name'));
            }
			
			if (trim($this->input->post('student_mother_occupation')) != '') {
                $studentdata['student_mother_occupation'] = trim($this->input->post('student_mother_occupation'));
            }
			
			if (trim($this->input->post('student_mother_mobile')) != '') {
                $studentdata['student_mother_mobile'] = trim($this->input->post('student_mother_mobile'));
            }
			
			if (trim($this->input->post('student_mother_email')) != '') {
                $studentdata['student_mother_email'] = trim($this->input->post('student_mother_email'));
            }
			
			if (trim($this->input->post('student_emergency_name')) != '') {
                $studentdata['student_emergency_name'] = trim($this->input->post('student_emergency_name'));
            }
			
			
			if (trim($this->input->post('student_emergency_relationship')) != '') {
                $studentdata['student_emergency_relationship'] = trim($this->input->post('student_emergency_relationship'));
            }
			
			if (trim($this->input->post('student_emergency_mobile')) != '') {
                $studentdata['student_emergency_mobile'] = trim($this->input->post('student_emergency_mobile'));
            }
			
			if (trim($this->input->post('student_doctor_name')) != '') {
                $studentdata['student_doctor_name'] = trim($this->input->post('student_doctor_name'));
            }
			
			if (trim($this->input->post('student_doctor_mobile')) != '') {
                $studentdata['student_doctor_mobile'] = trim($this->input->post('student_doctor_mobile'));
            }
			
			if (trim($this->input->post('student_helth_notes')) != '') {
                $studentdata['student_helth_notes'] = trim($this->input->post('student_helth_notes'));
            }
			
			if (trim($this->input->post('student_allergies')) != '') {
                $studentdata['student_allergies'] = trim($this->input->post('student_allergies'));
            }
			
			
			if (trim($this->input->post('student_other_comments')) != '') {
                $studentdata['student_other_comments'] = trim($this->input->post('student_other_comments'));
            }
			   
		
		if($this->studentenrolementdashboard_model->udateStudent($studentdata,$wherestudent)){
			      
			$this->studentenrolementdashboard_model->updateUser($userdata,$whereuser);
			$this->session->set_flashdata('success', 'Data has been updated successfully.');
			redirect(base_url()."studentenrolementdashboard");exit;
		 } else {
			$this->session->set_flashdata('error', 'Some problem exists. Data has not been updated.');
			redirect(base_url()."studentenrolementdashboard");exit;
		   }
	  }
	  
	  function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}
	
}

