 <div class="editprofile-content">
      <div class="col-sm-12 profilemenus nopadding">
     <div class="col-sm-9 col-xs-8 nopadding menubaritems">
         <ul>
        <?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>
        <li class="edit">Notifications</li>        
        </ul>
        </div>
        </div>

          <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <b>Alert!</b> 
        <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       <b>Alert!</b> 
       <?php echo $this->session->flashdata('success'); ?>
     </div>
<?php endif; ?>
 <div style="clear:both"></div>   

        <div class="col-sm-12 nopadding accdetailheading">

        <div class="col-sm-12 col-xs-7 nopadding profile-bg accsection">

        <h1>Notifications</h1>
        
        <div class="tablewrapper">
  
     <table class="table-bordered table-striped">
        <thead>
          <tr class="headings">

            <th class="column3">Sender</th>

                      <th class="column5">Sender Name & Subject</th>
                      
                       <th class="column7">Message</th>
                   
            <th class="column3">Date</th>

            <th class="column2">Actions</th>

          </tr>

        </thead>

        <tbody>
                      <?php 
            if(count($data_rows) > 0){
                $sr=$last_page;
            foreach($data_rows as $notification) { 
               $sr++;
             $this->load->model(array('notifications_model')); 
             $userInfo = $this->notifications_model->getuserInfo($notification->from,$notification->school_id);
               if($userInfo->user_type=='teacher') {
                 $path = 'staff';
                 $Info = $this->notifications_model->getStaffInfo($notification->from,$notification->school_id);
                 $Name = $Info->staff_fname.' '.$Info->staff_lname;
                   } elseif($userInfo->user_type=='student') {
                $path = 'student';
                 $Info = $this->notifications_model->getStudentInfo($notification->from,$notification->school_id);
                 $Name = $Info->student_fname.' '.$Info->student_lname;
                } 
                else if($userInfo->user_type=='enrolementstudent') {
                                $path = 'student';
                                $Info = $this->notifications_model->getEnrolmentStudentInfo($notification->from,$notification->school_id);
                                $Name = $Info->student_fname.' '.$Info->student_lname;
                              } 
                              else if($userInfo->user_type=='enrolementstaff') {
                                $path = 'staff';
                                $Info = $this->notifications_model->getEnrolmentStaffInfo($notification->from,$notification->school_id);
                                $Name = $Info->staff_fname.' '.$Info->staff_lname;
                                
                              }  else {
                $path = 'admin';
                $Info = $this->notifications_model->getAdminInfo($notification->from,$notification->school_id);
                $Name = $Info->school_contact_person;
                  }
          if($notification->read_by!='') {
             $readStatus = 'readed_notification';
             } else {
             $readStatus = 'unreaded_notification';
              }
            
             ?>
                         
                        <tr class="familydata communication">

            <td class="column3">
                        <div class="imgsec">
                        <?php  if($userInfo->profile_image!='') { ?>
                 <img src="<?php echo base_url();?>uploads/<?php echo $path;?>/<?php echo $userInfo->profile_image;?>">
                        <?php } else { ?>
         <img src="<?php echo base_url();?>assets/images/avtar.png">
                         <?php } ?>
                        </div>
                        </td>
                        
                        <td class="column5">
                        <div class="contentsec">
                        <p><?php echo $Name;?></p>
                           <?php if($notification->subject!='') { ?>
                        <span><?php echo substr($notification->subject, 0,70); if(strlen($notification->subject) > 70 ) { echo '...'; }?></span>
                             <?php } else { ?>
                        <span style="color:transparent;">-</span>
                            <?php } ?>
                        </div>
                        </td>
                        
                        <td class="column7">
                        <div class="contentsec">
             <?php if($notification->message!='') {

              $recieverInfo = $this->notifications_model->getrecieverInfo($notification->to,$notification->school_id);

              if($recieverInfo->user_type=='student') {

                $notify_msg           = $notification->message;

                $Info                 = $this->notifications_model->getStudentInfo($notification->from,$notification->school_id);
                $info                 = $this->notifications_model->studentinfo($notification->to,$notification->school_id);
                
                $username             = $info->username;
                $password             = $info->password;
                $user_email           = $info->email;
                $student_fname        = $info->student_fname;
                $student_lname        = $info->student_lname;
                $student_gender       = $info->student_gender;
                $student_dob          = $info->student_dob;
                $student_nationality        = $info->student_nationality;
                $student_country_of_birth   = $info->student_country_of_birth;
                $student_first_language     = $info->student_first_language;
                $student_other_language     = $info->student_other_language;
                $student_religion           = $info->student_religion;
                $student_ethnic_origin      = $info->student_ethnic_origin;
                $student_telephone          = $info->student_telephone;
                $student_address            = $info->student_address;
                $student_address_1          = $info->student_address_1;
                $student_father_name        = $info->student_father_name;
                $student_father_occupation  = $info->student_father_occupation;
                $student_father_mobile      = $info->student_father_mobile;
                $student_father_email       = $info->student_father_email;
                $student_mother_name        = $info->student_mother_name;
                $student_mother_occupation  = $info->student_mother_occupation;
                $student_mother_mobile      = $info->student_mother_mobile;
                $student_mother_email       = $info->student_mother_email;
                $student_emergency_name     = $info->student_emergency_name;
                $student_emergency_relationship = $info->student_emergency_relationship;
                $student_emergency_mobile       = $info->student_emergency_mobile;
                $student_doctor_name            = $info->student_doctor_name;
                $student_doctor_mobile          = $info->student_doctor_mobile;
                $student_helth_notes            = $info->student_helth_notes;
                $student_allergies              = $info->student_allergies;
                $student_other_comments         = $info->student_other_comments;
                $student_application_date       = $info->student_application_date;
                $student_application_comment    = $info->student_application_comment;
                $student_enrolment_date         = $info->student_enrolment_date;
                $student_leaving_date           = $info->student_leaving_date;
                $student_certificates           = $info->student_certificates;
                $student_family_address         = $info->student_family_address;
                $student_school_branch          = $info->student_school_branch;
                $student_class_group            = $info->student_class_group;
                $student_fee_band               = $info->student_fee_band;
                $student_sibling                = $info->student_sibling; 
                $school_id                      = $notification->school_id;

                $branchName                     = $this->notifications_model->getbranchName($student_school_branch);
                $className                      = $this->notifications_model->getclassName($student_class_group);
                $schoolName                     = $this->notifications_model->getschoolName($school_id);

               if($student_gender == 'Female'){

               if (strpos($notify_msg,'{his/her}') > 0) {
                $pronoun = 'her';
                  } else {
                $pronoun='Her';
              }

              if (strpos($notify_msg,'{gender}') > 0 ) {

                $var= strpos($notify_msg,'{gender}');

                  $gender='she';
                }else {
                  $gender='She';
                }
              if (strpos($notify_msg,'{Gender}') > 0 ) {
                $var1= strpos($notify_msg,'{gender}');
                  $gender1='she';
                }else {
                  $gender1='She';
                }
                $pronoun1 = 'her';
       
            } else {

              if (strpos($notify_msg,'{his/her}') > 0) {
                 $pronoun = 'his';
              } else {
                 $pronoun = 'His';
              }
              if (strpos($notify_msg,'{gender}') > 0) {
                $gender='he';
              } else {
                $gender='He';
              }
              if (strpos($notify_msg,'{Gender}') > 0 ) {

                $gender1='he';
              } else {
                $gender1='He';
              }
              $pronoun1 = 'him';

            }

            $email_message1   =   str_replace ("{username}", $username,$notify_msg);
            $email_message2   =   str_replace ("{password}", $password,$email_message1);
            $email_message3   =   str_replace ("{gender}", $gender,$email_message2);
            $email_message4   =   str_replace ("{Gender}", $gender1,$email_message3);
            $email_message5   =   str_replace ("{his/her}", $pronoun,$email_message4);
            $email_message6   =   str_replace ("{him/her}", $pronoun1,$email_message5);
            $email_message7   =   str_replace ("{first_name}", $student_fname,$email_message6);
            $email_message8   =   str_replace ("{last_name}", $student_lname,$email_message7);
            $email_message9   =   str_replace ("{father_name}", $student_father_name,$email_message8);
            $email_message10  =   str_replace ("{mother_name}", $student_mother_name,$email_message9);
            $email_message11  =   str_replace ("{main_telephone}", $student_telephone,$email_message10);
            $email_message12  =   str_replace ("{email_id}", $user_email ,$email_message11);
            $email_message13  =   str_replace ("{date_of_birth}", $student_dob,$email_message12);
            $email_message14  =   str_replace ("{father_mobile}", $student_father_mobile,$email_message13);
            $email_message15  =   str_replace ("{mother_mobile}", $student_mother_mobile,$email_message14);
            $email_message16  =   str_replace ("{father_email}", $student_father_email ,$email_message15);
            $email_message17  =   str_replace ("{mother_email}", $student_mother_email,$email_message16);
            $email_message18  =   str_replace ("{father_address}", $student_family_address,$email_message17);
            $email_message19  =   str_replace ("{mother_address}", $student_family_address,$email_message18);
            $email_message20  =   str_replace ("{child_class}",$className->class_name,$email_message19);
            $email_message21  =   str_replace ("{enrolment_date}", $student_enrolment_date,$email_message20);

            $email_message22  =   str_replace ("{branch_name}", $branchName->branch_name,$email_message21);
            $email_message_final  =   str_replace ("{school_name}", $schoolName->school_name,$email_message22);
          
          }elseif($recieverInfo->user_type == 'teacher'){

            $notify_msg           = $notification->message;

            $info    = $this->notifications_model->staffInfo($notification->to,$notification->school_id);

            $branchName                     = $this->notifications_model->getbranchName($info->existing_branch);
            $schoolName                     = $this->notifications_model->getschoolName($info->school_id);


            if($info->staff_title  == 'mrs.' || $info->staff_title  == 'ms.' ){

            if (strpos($notify_msg,'{his/her}') > 0) {
                $pronoun = 'her';
              } else {
                $pronoun='Her';
              }

            if (strpos($notify_msg,'{gender}') > 0 ) {

              $var= strpos($notify_msg,'{gender}');

                $gender='she';
              } else {
                $gender='She';
              }
            if (strpos($notify_msg,'{Gender}') > 0 ) {
                $var1= strpos($notify_msg,'{gender}');
                $gender1='she';
              } else {
                $gender1='She';
              }
                $pronoun1 = 'her';
     
          } else {

            if (strpos($notify_msg,'{his/her}') > 0) {
               $pronoun = 'his';
            } else {
               $pronoun = 'His';
            }
            if (strpos($notify_msg,'{gender}') > 0) {
              $gender='he';
            } else {
              $gender='He';
            }
            if (strpos($notify_msg,'{Gender}') > 0 ) {

              $gender1='he';
            } else {
              $gender1='He';
            }
            $pronoun1 = 'him';

          } 
            $email_message1   =   str_replace ("{username}", $info->username,$notify_msg);
            $email_message2   =   str_replace ("{password}", $info->password,$email_message1);
            $email_message3   =   str_replace ("{gender}", $gender,$email_message2);
            $email_message4   =   str_replace ("{Gender}", $gender1,$email_message3);
            $email_message5   =   str_replace ("{his/her}", $pronoun,$email_message4);
            $email_message6   =   str_replace ("{him/her}", $pronoun1,$email_message5);
            $email_message7   =   str_replace ("{first_name}", $info->staff_fname,$email_message6);
            $email_message8   =   str_replace ("{last_name}", $info->staff_lname,$email_message7);
            $email_message9   =   str_replace ("{main_telephone}", $info->staff_telephone,$email_message8);
            $email_message10  =   str_replace ("{email_id}", $info->email ,$email_message9);
            $email_message11  =   str_replace ("{date_of_birth}", $info->staff_dob,$email_message10);
            $email_message12  =   str_replace ("{qualification}", $info->staff_education_qualification,$email_message11);
            $email_message13  =   str_replace ("{teaching_days}", $info->teaching_days,$email_message12);
            $email_message14  =   str_replace ("{salary}", $info->salary_currency.''.$info->salary_amount,$email_message13);

            $email_message15  =   str_replace ("{branch_name}", $branchName->branch_name,$email_message14);
            $email_message_final  =   str_replace ("{school_name}", $schoolName->school_name,$email_message15);


            
          }else{
            $email_message_final=$notification->message;
          }
          ?>



         <span><?php echo substr($email_message_final, 0,140); if(strlen($email_message_final) > 140 ) { echo '...'; }?></span>
         <?php } else { ?>
          <span style="color:transparent;">-</span>
            <?php } ?>
        </div>
        </td>

            <td class="column3">
    <?php echo  date('d M Y', strtotime($notification->created_date)).' at '.date('H:i A', strtotime($notification->created_date));?></td>
                        <td class="column2 spacing">
                        
                    <a class="viewmessage" id="viewmsg" data-value="<?php echo $notification->notification_id;?>" data-toggle="modal" data-target="#viewModal<?php                                                              echo $notification->notification_id;?>"><i class="fa fa-eye icnclass"></i></a>
                        
             <!--<a class="deletemsg" onclick="delConfirm('')"><i class="fa fa-trash-o icnclass"></i></a>-->
                     
                        </td>

                        </tr>        


<!--*********************************POPUP TO VIEW NOTIFICATION ***************************-->
     <div id="viewModal<?php echo $notification->notification_id;?>" class="modal notify" style="z-index:99999999999">
        
          <!-- Modal content -->
          <div class="modal-content">
          <div class="modalbg">
                <div class="col-sm-12 nopadding" style=" background:#36b047 none repeat scroll 0 0;">
                  <div class="col-sm-8 nopadding viewmodal"><h3>View Message</h3></div>
                    <div class="col-sm-4 btncls"><button type="button" class="close closebtn" data-dismiss="modal" aria-hidden="true">&times;</button></div>
                 </div>

                      <div class="col-sm-12 msgbox">
                          
                          <div class="col-sm-12 nopadding">
                            <div class="col-sm-3 nopadding popup">
                            <div class="imgsec modalimg" >
                        <?php  if($userInfo->profile_image!='') { ?>
                 <img src="<?php echo base_url();?>uploads/<?php echo $path;?>/<?php echo $userInfo->profile_image;?>">
                        <?php } else { ?>
         <img src="<?php echo base_url();?>assets/images/avtar.png">
                         <?php } ?>
                        </div>
                              <!--<label for="sender">From:</label>-->
                            </div>
                            <div class="col-sm-9 nopadding popup">
                           <?php  $sender_id = $notification->from;
                          $this->load->model(array('notifications_model'));
                          $senderName = $this->notifications_model->getsenderName($sender_id);?>
                            <?php
                            
                              if($userInfo->user_type=='teacher') 
                              {
                                $path = 'staff';
                                $Info = $this->notifications_model->getStaffInfo($notification->from,$notification->school_id);
                                $Name = $Info->staff_fname.' '.$Info->staff_lname;
                              }
                              else if($userInfo->user_type=='enrolementstudent') {
                                $path = 'student';
                                $Info = $this->notifications_model->getEnrolmentStudentInfo($notification->from,$notification->school_id);
                                $Name = $Info->student_fname.' '.$Info->student_lname;
                              } 
                              else if($userInfo->user_type=='enrolementstaff') {
                                $path = 'staff';
                                $Info = $this->notifications_model->getEnrolmentStaffInfo($notification->from,$notification->school_id);
                                $Name = $Info->staff_fname.' '.$Info->staff_lname;
                                
                              }  
                              elseif($userInfo->user_type=='student') 
                              {
                                $path = 'student';
                                $Name = $Info->student_fname.' '.$Info->student_lname;
                              } 
                              else 
                              {
                                $path = 'admin';
                                  $Info = $this->notifications_model->getAdminInfo($notification->from,$notification->school_id);
                                $Name = $Info->school_contact_person;
                              }
                            
                              echo $Name;
                              echo"<br>";           
                                echo $senderName->email;
                            ?>
                             </div>
                             </div>
                             <div class="col-sm-12 nopadding">
                                 <div class="col-sm-3 nopadding popup">
                              <label for="subject">Date:</label>
                            </div>
                            <div class="col-sm-9 nopadding popup">
                              
                <?php               
                            $senddate=$notification->created_date;
              
              echo $senddate=date('d M Y', strtotime($senddate)); ?>
              
                                
                             </div>
                             </div>
                             
                             <div class="col-sm-12 nopadding">
                                 <div class="col-sm-3 nopadding popup">
                              <label for="subject">Subject:</label>
                            </div>
                            <div class="col-sm-9 nopadding popup">
                              
                <?php echo $notification->subject;?>
              
                                
                             </div>
                             </div>
                             <div class="col-sm-12 nopadding">
                             <div class="col-sm-3 nopadding popup">
                            <label for="message">Message:</label>
                              </div>
                              <div class="col-sm-9 nopadding popup"> 
                            <?php echo $email_message_final; /*$notification->message;*/?>
                            </div>
                              </div>
                              <div class="col-sm-12 nopadding">
                                  <div class="col-sm-3 nopadding popup" style="padding:10px !important;">
                              <label for="attachment">Attachments:</label>
                            </div>
                <div class="col-sm-9 nopadding popup">
                              
         <ul class="attachment_section">
         <?php 
    
     if($notification->attachments!='') {
      
       $attachments =  explode(',',$notification->attachments);
      
       
       foreach($attachments as $attachment) { ?>
           <li>
                <h4><span class="attachmentheading"><?php 
                  $string = $attachment;
          echo substr($string,0,10); if(strlen($string) > 10 ) { echo '...'; }
                
                ?></span>
                    <div class="icon_section">
                  <a href="<?php echo base_url();?>notifications/download_attachments/<?php echo $attachment;?>" data-toggle="tooltip" title="Download" data-placement="top"><i class="fa fa-download"></i></a>
        
                  <a href="<?php echo base_url();?>uploads/notification/<?php echo $attachment;?>" target="_blank" data-toggle="tooltip" title="View" data-placement="top"><i class="fa fa-eye"></i></a>
                  </div>
                  </h4>
               </li> 
      <?php }
       } else {
          echo 'No attachments';
        }
     ?>
         </ul>
       
                             </div>
                             </div>
              </div>
</div>
              </div>  
  </div>
                 
              <?php }   
          } else { ?>
                    <tr><th colspan="7" style="text-align: center; width:1215px;height:60px;font-size:18px; background:#FFF;  color: #6a7a91;">You have no notifications.</th></tr> 
           <?php } ?>
                  
              </tbody>
         </table> 
             
   <div class="profile-bg">
  <div class="col-sm-12 paginationdiv nopadding">
    <div class="col-sm-8 col-xs-6 paginationblk">
         <?php  if(count($data_rows) > 0){
             $last_page1=$last_page;
         ?>
      Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries.
      <?php  } ?>
  <ul class="pagination">
    <?php echo $pagination;?>
      </ul>
    </div>
    </div>
    </div>
  
               </div>

             </div>

          </div>

      </div>




 
 <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>  

<script>
  jQuery(document).ready(function(){
    
     jQuery("#branch").on('click', function(e){ 
        var branch_id = jQuery(this).val();
      if( branch_id!='' && branch_id!='null' ) {
      jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/getclass',
            data:{ 'branch_id': branch_id },
            success :  function(response) {
                response=jQuery.parseJSON(response);
            jQuery('#class_name').empty();
              jQuery('#class_name').html(response.datacls);
            jQuery('#student_name').empty();
              jQuery('#student_name').html(response.datastu);
                }
           });
         } else {
         return false;
         }
       }); 
       
   jQuery("#class_name").on('click', function(e){ 
        var class_id = jQuery(this).val();
       var branch_id = jQuery('#branch').val();
      if(branch_id!='' && class_id!='' && branch_id!='null' && class_id!='null') {
      jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>emailnotifications/get_students',
            data:{ 'branch_id': branch_id, 'class_id':class_id},
            success :  function(resp) {
            jQuery('#student_name').empty();
              jQuery('#student_name').html(resp);
                }
           });
         } else {
         return false;
         }
       }); 
   });
</script>  
<style>
.fa-question-circle + .tooltip > .tooltip-inner {background-color:#090 !important; color:#fff;}
.fa-question-circle + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

</style>

<style>
.spacing {
    padding: 0;
}
.notify {
    margin: auto;
    width: 70% !important;
}
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index:2147483647 !important; /* Sit on top */
    padding-top: 50px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    /*background-color: rgb(0,0,0);*/ /* Fallback color */
   /* background-color: rgba(0,0,0,0.4);*/ /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
  /*padding: 20px;
  border: 10px solid #579b4a;*/
    width: 100%;
 /*   top: 80px;*/
}


/*.closebtn{
    background: #fff none repeat scroll 0 0 !important;
    border-radius: 40px;
    float: right;
    height: 40px;
    opacity: 1;
    width: 40px;
  color:#36b047;
}*/
.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.popup{
  padding:10px;
}

.compose{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
    padding:5px 30px;
  float:right;
}

.composeclose{
padding:8px 0px 0px 0px;
}
.btncls{padding-top:5px;}

.modalbg {
    background: #fff none repeat scroll 0 0;
    float: left;
    width: 100%;
  border-radius:5px;
  }
.imgsec.modalimg > img {
    border-radius: 70px;
}
.viewmodal> h3 {
  padding:0 0 0 20px;
   color: #fff;
   text-transform: uppercase;
}
.close.closebtn{
  font-size:30px;
}
.attachmentheading{float:left; line-height:0px;}
.icon_section{float:right;}
.attachment_section{list-style:none; padding:0px;}

</style>
