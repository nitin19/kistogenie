<div class="editprofile-content enrollmentcontent">

          <div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div> 	
        <div class="col-sm-12 leftspace fullwidsec">

        <div class="editprofileform editdetails accdetailinfo">

        
<form name="basicform" id="basicform" method="post" action="<?php echo base_url(); ?>studentenrolementdashboard/edit_student/<?php echo $info->id; ?>" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">

   <div id="sf1" class="tab-pane active frm">
    <fieldset>
        
       <div class="editprofileform enrollmentprofileform">

  <div class="profile-bg prfltitle profile_titlebg enrollment_titlebg">
  <h1>General details</h1>


  <div class=" col-sm-12 nopadding contentdiv">
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Username<span>*</span></label>
    <div class="col-sm-7 inputbox">
      <input type="text" class="form-control" name="username" id="username" value="<?php echo $info->username; ?>"  placeholder="" required="required" autocomplete="off" maxlength="41" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Password<span>*</span></label>
    <div class="col-sm-7 inputbox">
      <input class="form-control" name="password" id="password" value="<?php echo $info->password; ?>" placeholder="" required="required" autocomplete="off" maxlength="16" disabled="disabled">
    </div>
  </div>
  </div>
    
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Avtar<br/><span class="tagline">JPG, PNG</span></label>
    <div class="col-sm-7 profile-picture inputbox">
  
    <input type="hidden" name="avtaruserid" id="avtaruserid" value="<?php echo $info->id; ?>" />
    
    <div class="upload-pimg-msg"></div>
    
 <div class="pimg-preview" id="dynamicavtar"> 
<?php if($info->profile_image!='') { ?> <img src="<?php echo base_url();?>uploads/student/<?php echo $info->profile_image; ?>"> 
 <?php } else { ?> 
 <img src="<?php echo base_url();?>assets/images/avtar.png">
 <?php } ?>
 </div>
   <div class="profilepicbtns"> <input type="file" name="profile_image" id="file" accept="image/*" style="padding: 0 12px; display:none;"/><i class="fa fa-picture-o" id="changeprofilepic" data-toggle="tooltip" title="Select your image" data-placement="right"></i><br/><hr class="borderline"><i class="fa fa-trash-o deleteprofilepic" id="<?php echo $info->profile_image; ?>" data-toggle="tooltip" title="Delete your image" data-placement="right"></i></div>
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec"> 
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Child's FirstName<span>*</span></label>
    <div class="col-sm-7 inputbox">
      <input type="text" class="form-control" name="student_fname" id="student_fname" value="<?php echo $info->student_fname;?>" placeholder="" required="required" autocomplete="off" maxlength="61">
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Child's LastName<span>*</span></label>
    <div class="col-sm-7 inputbox">
      <input type="text" class="form-control" name="student_lname" id="student_lname" value="<?php echo $info->student_lname;?>" placeholder="" required="required" autocomplete="off" maxlength="61">
    </div>
  </div>
</div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Date of Birth<span>*</span></label>
    <div class="col-sm-7 inputbox datepickerdiv">
      <input type="text" name="student_dob" id="student_dob" class="datepicker1" value="<?php if($info->student_dob=='0000-00-00') { echo ''; } else { echo date('d-m-Y', strtotime($info->student_dob)); } ?>" required="required" maxlength="10">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Telephone <span>*</span></label>
    <div class="col-sm-7 inputbox">
     <input type="text" name="student_telephone" id="student_telephone" value="<?php echo $info->student_telephone;?>" class="form-control" required="required" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Payment Email Address<span>*</span></label>
    <div class="col-sm-7 inputbox">
      <input type="email" class="form-control" id="email" name="email" value="<?php echo $info->email;?>" placeholder="" required="required" autocomplete="off" maxlength="121">
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Gender<span>*</span></label>
    <div class="col-sm-7 inputbox">
      <select name="student_gender" id="student_gender" class="form-control" required="required">
          <option value="" disabled="disabled" selected="selected">Select one </option>
 		 <option <?php if($info->student_gender == "Male"){ echo 'selected'; } ?> value="Male">Male</option>
  		 <option <?php if($info->student_gender == "Female"){ echo 'selected'; } ?> value="Female">Female</option>
		</select>
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Nationality <span>*</span></label>
    <div class="col-sm-7 inputbox">
  <select name="student_nationality" id="student_nationality" class="form-control" required="required">
  
       <?php 
 if($info->student_nationality!=""){
	 foreach($nationalities as $nationality) { ?>
	 <option <?php if($nationality->nationality_name == $info->student_nationality){ echo 'selected'; } ?> value="<?php echo $nationality->nationality_name;?>"><?php echo $nationality->nationality_name;?></option>
    <?php } 
	} else {
		
		$countryname = 'United Kingdom';
	foreach($nationalities as $nationality) { ?>
	 <option <?php if($nationality->nationality_name==$countryname) { echo 'selected'; } ?> value="<?php echo $nationality->nationality_name;?>"><?php echo $nationality->nationality_name; ?></option>
  <?php   } 
  }?>
    
</select>
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Country of Birth </label>
    <div class="col-sm-7 inputbox">
<select name="student_country_of_birth" id="student_country_of_birth" class="form-control">

     <?php 
		if($info->student_country_of_birth!=""){
	 foreach($countries as $country) { ?>
	 <option <?php if($country->country_name == $info->student_country_of_birth){ echo 'selected'; } ?> value="<?php echo $country->country_name;?>"><?php echo $country->country_name;?></option>
    <?php } 
	} else {
		
		$countryname = 'United Kingdom';
	foreach($countries as $country) { ?>
	 <option <?php if($country->country_name==$countryname) { echo 'selected'; } ?> value="<?php echo $country->country_name;?>"><?php echo $country->country_name; ?></option>
  <?php   } 
  }?>
		
</select>
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">First Language</label>
    <div class="col-sm-7 inputbox">
      <input type="text" class="form-control" name="student_first_language" id="student_first_language" value="<?php echo $info->student_first_language;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Other Language</label>
    <div class="col-sm-7 inputbox">
      <input type="text" class="form-control" name="student_other_language" id="student_other_language" value="<?php echo $info->student_other_language;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Religion</label>
    <div class="col-sm-7 inputbox">
      <select name="student_religion" id="student_religion" class="form-control">
<option value=""> Select one</option>

  <?php 
 if($info->student_religion!=""){
	 foreach($religions as $religion) { ?>
	 <option <?php if($religion->religion_name == $info->student_religion){ echo 'selected'; } ?> value="<?php echo $religion->religion_name;?>"><?php echo $religion->religion_name;?></option>
    <?php } 
	} else {
		
		$religionname = 'Islam';
	foreach($religions as $religion) { ?>
	 <option <?php if($religion->religion_name==$religionname) { echo 'selected'; } ?> value="<?php echo $religion->religion_name;?>"><?php echo $religion->religion_name; ?></option>
  <?php   } 
  }?>
   
</select>
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Ethnic Origin</label>
    <div class="col-sm-7 inputbox">
<select name="student_ethnic_origin" id="student_ethnic_origin" class="form-control">
   <option>Select One</option>
        <?php foreach($ethnicorigins as $ethnicorigin) { ?>
	    <option <?php if($ethnicorigin->ethnic_origin_name == $info->student_ethnic_origin){ echo 'selected'; } ?> value="<?php echo $ethnicorigin->ethnic_origin_name;?>"><?php echo $ethnicorigin->ethnic_origin_name;?></option>
       <?php } ?>
</select>
    </div>
  </div>
  </div>
   
   <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Full Address <span>*</span></label>
    <div class="col-sm-7 inputbox">
     <input type="text" name="student_address" id="student_address" value="<?php echo $info->student_address;?>" class="form-control" required="required" autocomplete="off" maxlength="251">
    </div>
  </div>
  </div>
   <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Full Address(Optional)</label>
    <div class="col-sm-7 inputbox">
     <input type="text" name="student_address_1" id="student_address_1" value="<?php echo $info->student_address_1;?>" class="form-control" autocomplete="off" maxlength="251">
    </div>
  </div>
  </div>
  </div>
  </div>
  <div class="profile-bg prfltitle profile_titlebg enrollment_titlebg">
        <h1>Parents details</h1>
        <div class=" col-sm-12 nopadding contentdiv">
        <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Father's Name</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_father_name" id="student_father_name" value="<?php echo $info->student_father_name;?>" class="form-control" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Father's Occupation</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_father_occupation" id="student_father_occupation" value="<?php echo $info->student_father_occupation;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="121">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Father's Mobile</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_father_mobile" id="student_father_mobile" value="<?php echo $info->student_father_mobile;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Father's Email</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_father_email" id="student_father_email" value="<?php echo $info->student_father_email;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="121">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
 <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Mother's Name</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_mother_name" id="student_mother_name" value="<?php echo $info->student_mother_name;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
   <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Mother's Occupation</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_mother_occupation" id="student_mother_occupation" value="<?php echo $info->student_mother_occupation;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="121">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Mother's Mobile</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_mother_mobile" id="student_mother_mobile" value="<?php echo $info->student_mother_mobile;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Mother's Email</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_mother_email" id="student_mother_email" value="<?php echo $info->student_mother_email;?>" class="form-control"  placeholder="" autocomplete="off" maxlength="121">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Emergency Name</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_emergency_name" class="form-control" id="student_emergency_name" value="<?php echo $info->student_emergency_name;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Emergency Relationship</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_emergency_relationship" class="form-control" id="student_emergency_relationship" value="<?php echo $info->student_emergency_relationship;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  </div>
  <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Emergency Mobile</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_emergency_mobile" class="form-control" id="student_emergency_mobile" value="<?php echo $info->student_emergency_mobile;?>" placeholder="" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  </div>
  </div>
  </div>
  <div class="profile-bg prfltitle profile_titlebg enrollment_titlebg">
        <h1>Medical details</h1>
        <div class=" col-sm-12 nopadding contentdiv">
          <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Doctor's Name</label>
    <div class="col-sm-7 inputbox">
    <input type="text" name="student_doctor_name" class="form-control" id="student_doctor_name" value="<?php echo $info->student_doctor_name;?>" placeholder="" autocomplete="off" maxlength="61">
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Doctor's Mobile</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_doctor_mobile" class="form-control" id="student_doctor_mobile" value="<?php echo $info->student_doctor_mobile;?>" placeholder="" autocomplete="off" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');">
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Health Notes</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_helth_notes" class="form-control" id="student_helth_notes" value="<?php echo $info->student_helth_notes;?>" placeholder="" autocomplete="off" maxlength="251">
    </div>
  </div>
  </div>
    <div class="col-sm-6 enrollmentfield forminputsec">
  <div class="form-group">
    <label class="col-sm-5 control-label nopadding">Allergies</label>
    <div class="col-sm-7 inputbox">
      <input type="text" name="student_allergies" class="form-control" id="student_allergies" value="<?php echo $info->student_allergies;?>" placeholder="" autocomplete="off" maxlength="251">
    </div>
  </div>
</div>
 <div class="col-sm-12 enrollmentfield forminputsec fullinputdiv">
  <div class="form-group">
    <label class="col-sm-3 control-label nopadding">Other Comments</label>
    <div class="col-sm-9 inputbox">
     <textarea rows="3" class="form-control" name="student_other_comments" id="student_other_comments" autocomplete="off" maxlength="251"><?php echo $info->student_other_comments;?></textarea>
    </div>
  </div>
  </div>
  </div>
  
   
</div>        
   </div>     
     </fieldset> 
       
        </div>
        
   <div id="sf6" class="tab-pane frm" style="display: block;">
        <fieldset>
 <div class="editprofileform">
  <div class="cancelconfirm prevnextbtns">
        <div class="col-sm-6 nopadding">
        <div class="cancellink">
       <!-- <input class="cancelbtn" id="cancelbtn" onclick="cancelupdate('766')" value="Cancel" type="button">-->
        </div>
        </div>
        <div class="col-sm-6 nopadding">
        <div class="confirmlink">
        <input class="open6" value="Submit" type="button">
        </div>
        </div>
        </div>

        </div>
        
        </fieldset>
        
        </div>
   
  </form>
    
   

        </div>

        </div>


        </div>

 <script type="text/javascript">
	function cancelupdate(id){
		window.location.href ="<?php echo base_url();?>students/edit/"+id;
	}
</script>
 <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>    
        
 <script type="text/javascript">
  
  jQuery(document).ready(function(){
	  
	     $("#student_dob").keydown(false);
	     $("#student_application_date").keydown(false);
		 $("#student_enrolment_date").keydown(false);
		 $("#student_leaving_date").keydown(false);
	  
	  	jQuery('body').click(function(){
			jQuery('.alert-dismissable').fadeOut('slow');
		})
		
  jQuery("#file").change(function() {
	jQuery(".upload-pimg-msg").empty(); 
	var file = this.files[0];
	var imagefile = file.type;
	var imageTypes= ["image/jpeg","image/png","image/jpg"];
		if(imageTypes.indexOf(imagefile) == -1)
		{
			jQuery(".upload-pimg-msg").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.onload = function(e){
				jQuery(".pimg-preview").html('<img src="' + e.target.result + '" width="120px" height="100px" />');				
			};
			reader.readAsDataURL(this.files[0]);
		}
	});	
	  
	  jQuery('#changeprofilepic').on('click', function() {
             jQuery('#file').click();
       });
	   
	  jQuery('.deleteprofilepic').on('click', function() {
            var avtarName =  jQuery(this).attr('id');
			
			 var avtarUid = jQuery('#avtaruserid').val();
			 
			 if(avtarName!='' && avtarUid!='') {
			 jQuery.ajax({
         		          type : 'POST',
                          url  : '<?php echo base_url(); ?>students/deleteAvtar',
                          data:{ 'avtarName': avtarName,'avtarUid': avtarUid },
                          success :  function(resp) {
							  if(resp==1) {
					            jQuery('#dynamicavtar').html('<img src="<?php echo base_url();?>assets/images/avtar.png">');
								jQuery('.deleteprofilepic').attr('id','');
							  } 
			            }
				   });
			 } else {
				// return false;
				jQuery("#file").val(null);
				jQuery('#dynamicavtar').html('<img src="<?php echo base_url();?>assets/images/avtar.png">');
			   }
			 
       }); 
	   

		
	 	 jQuery("#student_address").focusout(function(){
   			     var Favalue = jQuery(this).val();
				 if(Favalue!='') {
			         jQuery('#student_family_address').val(Favalue);
					 jQuery("#student_sibling").find('option[value!=""]').remove();
					 jQuery.ajax({
         		          type : 'POST',
                          url  : '<?php echo base_url(); ?>students/check_sibling',
                          data:{ 'Favalue': Favalue },
                          success :  function(resp) {
					     jQuery('#student_sibling').append(resp);
			           }
				   });
				   
				 } else {
				   return false;
				  }
			});
			
	 jQuery("#student_family_address").focusout(function(){
   		 var Favalue = jQuery(this).val();
		  if(Favalue!='') {
			  jQuery("#student_sibling").find('option[value!=""]').remove();
			jQuery.ajax({
            type : 'POST',
            url  : '<?php echo base_url(); ?>students/check_sibling',
            data:{ 'Favalue': Favalue },
            success :  function(resp) {
					 jQuery('#student_sibling').append(resp);
			          }
				   });
				 } else {
				 return false;
				 }
				 
			});
	  
	   });
</script>

<script type="text/javascript">
  
  jQuery().ready(function() {

	jQuery.validator.addMethod("alphaUname", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    });
	
	jQuery.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaLnumber", function(value, element) {
      return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z]/);
    });
	
	jQuery.validator.addMethod("alphaUpwd", function(value, element) {
    return this.optional(element) || value == value.match(/^(?=.*[a-zA-Z0-9])[a-zA-Z0-9!@#$%&*.]{7,}$/);
    });

    // validate form on keyup and submit
    var v = jQuery("#basicform").validate({

      rules: {
         username: {
          required: true,
		  alphaUname: true,
          minlength: 4,
          maxlength: 40
		  /*remote: {
             url: "<?php //echo base_url(); ?>students/check_username",
             type: "POST"
             }*/
        },
        password: {
          required: true,
		  alphaUpwd: true,
          minlength: 6,
          maxlength: 15
        },
		email: {
          required: true,
		  email: true,
		  maxlength: 120
        },
       student_fname: {
          required: true,
		  alphaLetter: true,
		  maxlength: 60
        },
		student_lname: {
          required: true,
		  alphaLetter: true,
		  maxlength: 60
        },
		student_dob: {
          required: true
        },
		student_gender: {
          required: true
        },
		student_first_language: {
		 /* required: true,*/	
          alphaLetter: true,
		  maxlength: 60
        },
		student_other_language: {
          alphaLetter: true,
		  maxlength: 60
        },
		student_telephone: {
		  required: true,	
          number: true,
		  minlength: 11,
          maxlength: 11
        },
		student_address: {
		   required: true,
           maxlength: 250
        },
		student_address_1: {
           maxlength: 250
        },
		student_father_name: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_father_occupation: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_father_mobile: {
         /* required: true,*/	
          number: true,
		  minlength: 10,
          maxlength: 14
        },
		student_father_email: {
		  email: true,
		  maxlength: 120
        },
		student_mother_name: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_mother_occupation: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_mother_mobile: {
         /* required: true,*/	
          number: true,
		  minlength: 11,
          maxlength: 11
        },
		student_mother_email: {
		  email: true,
		  maxlength: 120
        },
		student_emergency_name: {
		  /*required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_emergency_relationship: {
		 /* required: true,*/
		  alphaLetter: true,
		  maxlength: 60
        },
		student_emergency_mobile: {
         /* required: true,*/	
          number: true,
		  minlength: 10,
          maxlength: 14
        },
		student_doctor_name: {
		  alphaLetter: true,
		  maxlength: 60
        },
		student_doctor_mobile: {
		  number: true,
		  minlength: 11,
          maxlength: 11
        },
		student_helth_notes: {
		  maxlength: 250
        },
		student_allergies: {
		  maxlength: 250
        },
		student_other_comments: {
		  maxlength: 250
        }
		
      },
     
	  errorElement: "span",
      errorClass: "help-inline-error",
	  
	  messages: {
                 username: {
                  required: "This field is required.",
				  alphaUname: "Letters only please.",
				  minlength: "Minimum 4 characters required.",
				  maxlength: "Maximum 40 characters allowed."
				 // remote: "User name already in use."
                 },
			    password: {
                  required: "This field is required.",
				  alphaUpwd: "Letters, numbers and special characters allowed",
				  minlength: "Minimum 6 characters required.",
				  maxlength: "Maximum 15 characters allowed."
                 },
				email: {
                    required: "This field is required.",
					email: "Please enter a valid email address.",
					maxlength: "Maximum 120 characters allowed."
                   },
				student_fname: {
                  required: "This field is required.",
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
                 },
				student_lname: {
                  required: "This field is required.",
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
                 },
				student_dob: {
                  required: "This field is required."
                 },
			    student_gender: {
                  required: "This field is required."
                 },
				 student_first_language: {
				 /* required: "This field is required.",	*/
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
				},
				student_other_language: {
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
				},
				student_telephone: {
				  required: "This field is required.",
				  number: "Numbers only please.",
				  minlength: "Minimum 11 characters required.",
				  maxlength: "Maximum 11 characters allowed."
				},
				student_address: {
				   required: "This field is required.",
				   maxlength: "Maximum 250 characters allowed."
				},
				student_address_1: {
				   maxlength: "Maximum 250 characters allowed."
				},
				student_father_name: {
				/*  required: "This field is required.",*/
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
				},
				student_father_occupation: {
				  /*required: "This field is required.",*/
				  alphaLetter: "Letters only please.",
				  maxlength: "Maximum 60 characters allowed."
				},
				student_father_mobile: {
				 /* required: "This field is required.",*/
				  number: "Numbers only please.",
				  minlength: "Minimum 10 characters required.",
				  maxlength: "Maximum 14 characters allowed."
				},
				student_father_email: {
				    email: "Please enter a valid email address.",
					maxlength: "Maximum 120 characters allowed."
				},
				student_mother_name: {
				 /*  required: "This field is required.",*/
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_mother_occupation: {
				  /* required: "This field is required.",*/
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_mother_mobile: {
				 /* required: "This field is required.",*/
				  number: "Numbers only please.",
				  minlength: "Minimum 11 characters required.",
				  maxlength: "Maximum 14 characters allowed."
				},
				student_mother_email: {
				    email: "Please enter a valid email address.",
					maxlength: "Maximum 120 characters allowed."
				},
				student_emergency_name: {
				 /*  required: "This field is required.",*/
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_emergency_relationship: {
				  /* required: "This field is required.",*/
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_emergency_mobile: {
				/*  required: "This field is required.",*/
				  number: "Numbers only please.",
				  minlength: "Minimum 10 characters required.",
				  maxlength: "Maximum 14 characters allowed."
				},
				student_doctor_name: {
				   alphaLetter: "Letters only please.",
				   maxlength: "Maximum 60 characters allowed."
				},
				student_doctor_mobile: {
				  number: "Numbers only please.",
				  minlength: "Minimum 10 characters required.",
				  maxlength: "Maximum 14 characters allowed."
				},
				student_helth_notes: {
				  maxlength: "Maximum 250 characters allowed."
				},
				student_allergies: {
				 maxlength: "Maximum 250 characters allowed."
				},
				student_other_comments: {
				  maxlength: "Maximum 250 characters allowed."
				}
            },
			
	focusInvalid: false,
    invalidHandler: function(form, validator) {
        if (!validator.numberOfInvalids())
            return;
        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top
        }, 1000);
      },		
		
    });

	 $(".open6").click(function() {
      if (v.form()) {
		  $( "#basicform" ).submit();
         return false;
      }
    });

  });
</script>

