<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Editstaffdetail extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->model('staff_model');
    }

	public function index()

	{

		$this->load->view('header');

		$this->load->view('staff/edit_staff_detail');

		$this->load->view('footer');

	}

function show_user_id() {
	/*$msg = $this->uri->segment(4);*/
	$id = $this->uri->segment(3);
	/*if($msg=='msg'){echo "updated Successfully";}*/ 
	$data['result']=$this->staff_model->get_singlerecord($id); 
	$this->load->view('header');
 $this->load->view('staff/edit_staff_detail',$data);
	
$this->load->view('footer');
}

function delete_img() {	
$img = $this->uri->segment(4);
$id = $this->uri->segment(3);

if($img='del') {$data5 = array('profile_image' => $this->input->post(''), ); }
	

	$data['result']=$this->staff_model->get_singlerecord($id);	
	$dtt=$this->staff_model->del_img($id,$data5);
	$this->load->view('header');
 $this->load->view('staff/edit_staff_detail',$data);
	$this->load->view('footer');
	
	if($dtt=1){	
$this->session->set_flashdata('success', '');
				redirect(base_url()."editstaffdetail/show_user_id/".$id);exit;
			}else{
				$this->session->set_flashdata('error', '');
				redirect(base_url()."editstaffdetail/show_user_id/" .$id);exit;
			   }
}


function update() {
	$data							= array();
	$id= trim($this->input->post('id'));
	$arr= trim($this->input->post('group2'));	
	
$data1 = array(
'staff_fname' => trim($this->input->post('firstname')),
'staff_lname' => trim($this->input->post('lastname')),
'staff_title' => trim($this->input->post('title')),
'staff_dob' => trim($this->input->post('MyDate1')),
'staff_role_at_school' =>trim($this->input->post('role')),
'staff_education_qualification' => trim($this->input->post('quali')),
'staff_personal_summery' => trim($this->input->post('persummary')),
'staff_telephone' => trim($this->input->post('phone')),
'staff_address' => trim($this->input->post('address')),
'staff_address_1' => trim($this->input->post('optionaladdress'))
);
$data1['staff_teacher_type']= implode(",",$arr);

$data2 = array(
'username' => trim($this->input->post('username')),
'password' => trim($this->input->post('password')),
'email' => trim($this->input->post('email'))
);


$config['upload_path']   = './uploads/staff/'; 
			$config['allowed_types'] =  'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload("profile_image")){
		    $data = array('upload_data' => $this->upload->data());
	        $image_name = $data['upload_data']['file_name'];
		    if($_FILES['profile_image']['error'] == 0 ){
				$file_name					= $data['upload_data']['file_name'];
				$data2['profile_image']		= $file_name;
			}
			}


$dttt=$this->staff_model->update1($id,$data1);
$dttt1=$this->staff_model->update2($id,$data2);	
if($dttt=1 && $dttt1=1){	
$this->session->set_flashdata('success', 'Data has been updated successfuly.');
				redirect(base_url()."editstaffdetail/show_user_id/".$id);exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Staff has not been added.');
				redirect(base_url()."editstaffdetail/show_user_id/".$id);exit;
			   }
	         
}

function act_inact() {
	$this->load->view('header');
	$id = $this->uri->segment(3);	
 $this->load->view('staff/staff');
	
$this->load->view('footer');
}

}

