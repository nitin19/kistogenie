<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>School Management</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">

  </head>

  <body>



   <div class="loginpage">

   		<div class="container">

        	<div class="col-sm-12">

            	<div class="loginsection">

                	<div class="logodiv">

                    	<a href="#"><img class="logo" src="<?php echo base_url();?>assets/images/logo.PNG"></a>

                        </div>

                	<div class="loginheader">

                        <h1>Olive Tree Study Support</h1>

                    </div>

                    <div class="loginform-content">

                    <form class="loginform" name="forgotpwdForm" id="forgotpwdForm" action="" method="post">

                    <div class="col-sm-12 nopadding revealinput"> </div>

                    <div class="loginformarea">

                    	<div class="col-sm-12 logintitle nopadding">

                        <img src="<?php echo base_url();?>assets/images/dividers.png">

                        <h1>Forgot Password</h1>

                        </div>

                        <div class="loginformblk">

                        <?php if($error!='') {  
                         echo '<div class="alert alert-danger" id="flasherrorMessage">'.$error.'</div>';
                          }
						  
						  if($success!='') {  
                           echo  '<div class="alert alert-success" id="flashsuccessMessage">'.$success.'</div>';
                          }
						  
						  $validationerror = validation_errors();
						  if($validationerror!='') {  
                             echo '<div class="alert alert-danger" id="flashvalidationerrorMessage">'.$validationerror.'</div>';
                             }
						   ?>

                        <div class="form-group">

    				<!--	<label>Log In with your username</label>-->

    					<input type="text" class="form-control" name="useremail" id="useremail" placeholder="Email">

  						</div>

  						

                        <div class="login-button">

  						<input type="submit" class="btn btn-default" value="Send">

                        </div>

                        <div class="logininfo">

                         

                        </div>

                        </div>

                       

                    </div>

                    

					</form>

                    </div>

                    <div class="login-links">

                    	<div class="col-sm-8 col-xs-8 linkleft loginlinks nopadding">

                        <span style="color:#7f8ea3">Don't have an account yet? <a href="<?php echo base_url();?>register">Get Started</a></span>

                        </div>

                        <div class="col-sm-4 col-xs-4 linkright loginlinks nopadding">

                        <a href="<?php echo base_url();?>">Login</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

   </div>

   <footer>

   <div class="container">

   <div class="copyright">

   <p>© <?php echo date("Y");?> Olive Tree Study Support. All Rights Reserved. | Powered by : <span><a href="http://www.binarydata.in/" target="_blank">Binary Data</a></span></p>

   </div>

   </div>

   </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->

     <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

     <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

      <script>

   jQuery(document).ready(function(){

	setTimeout(function() {
              jQuery('#flasherrorMessage').slideUp('slow');
			  jQuery('#flashsuccessMessage').slideUp('slow');
			  jQuery('#flashvalidationerrorMessage').slideUp('slow');
            }, 3000);
			

    jQuery("#forgotpwdForm").validate({

        rules: {

            useremail: {

            required: true,

			email: true

             }

        },

        messages: {

            useremail: {

                required: "Please enter your email address",

				required: "Please enter a valid email address",

              }

        },

        

        submitHandler: function(form) {

            form.submit();

        }

    });



  });

  

  </script>

  

 <style>

 #forgotpwdForm .error { color:#F00; }
 .loginlinks a:hover{color:#1FB4F0;}
 </style> 

  </body>

</html>