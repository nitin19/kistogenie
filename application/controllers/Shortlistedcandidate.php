<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shortlistedcandidate extends CI_Controller {

	var $logmode;
	function __construct()
	{
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false )
        {
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','staff_model', 'applicationbank_model', 'Shortlistedcandidate_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
		$this->load->library('authorize');
    }
    public function index()

	{
		$PerPage				= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$user_srch_val			= isset($_GET['user_srch'])?$_GET['user_srch']:NULL;
		
		$school_id				= $this->session->userdata('user_school_id');
		
		$staff_branch_id		= $this->session->userdata('staff_branch_id');
		
		$user_type				= $this->session->userdata('user_type');
		
		//echo $staff_branch_id;
			
		$user_srch				= trim($user_srch_val);
		
		$rr 					= "'%".$user_srch."%'";
		if($rr!="")
		{
			$gg= 'AND (  u.username LIKE '. $rr . ' OR s.school_name LIKE '. $rr . ' OR se.staff_telephone LIKE '. $rr . ' OR u.email LIKE ' . $rr .')';
		}
		else
		{
			$gg = '';
		}
	    


		$data = array();
		 
		$config = array();
			
        $config['base_url'] 		= base_url() . "shortlistedcandidate/index";

        if($PerPage!='') {
			
			$config["per_page"] 	= $PerPage;
			$perpage				= $config["per_page"] ;
			$data["per_page"]		= $config["per_page"];
		} else {
			 $config["per_page"]	= 10;
             $perpage				= $config["per_page"] ;
		}
		$page 					=  $this->uri->segment(3,0) ;
		$data["per_page"]		= $config["per_page"];
			
        //$config['total_rows']  		= $this->staff_model->record_count($user_srch, $school_id, $gg,$user_srch);
		
		//$data['staffdata'] 		= $this->staff_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);
		
		/************** Changes for user access system **************************/
		 
		 if($user_type=='teacher')
		 {
			/*if($staff_branch_id!='')
			{
				$config["total_rows"] 			= $this->applicationbank_model->getStaff_record_count($user_srch, $staff_branch_id, $gg,$user_srch);
				$data['applicationbankdata'] 				= $this->applicationbank_model->getStaff_records($user_srch, $staff_branch_id, $data["per_page"], $page, $gg,$user_srch);

			}
			else
			{
				$config["total_rows"] 			= $this->applicationbank_model->getStaff_record_count($user_srch, $staff_branch_id, $gg,$user_srch);
				$data['applicationbankdata'] 				= $this->applicationbank_model->getStaff_records($user_srch, $staff_branch_id, $data["per_page"], $page, $gg,$user_srch);
			}*/
			if($staff_branch_id!='')
			{
				$config["total_rows"] 			= $this->Shortlistedcandidate_model->record_count($user_srch, $school_id, $gg,$user_srch);
				$data['shortlistedcandidatedata'] 				= $this->Shortlistedcandidate_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);

			}
			else
			{
				$config["total_rows"] 			= $this->Shortlistedcandidate_model->record_count($user_srch, $school_id, $gg,$user_srch);
				$data['shortlistedcandidatedata'] 				= $this->Shortlistedcandidate_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);
			}
		 }
		 else if($user_type=='admin')
		 {
			$config["total_rows"] 			= $this->Shortlistedcandidate_model->record_count($user_srch, $school_id, $gg,$user_srch);
			$data['shortlistedcandidatedata'] 				= $this->Shortlistedcandidate_model->get_records($user_srch, $school_id, $data["per_page"], $page, $gg,$user_srch);
			
		 }
		

        $config["uri_segment"] 		= 3;
			
		$this->pagination->initialize($config);
		
		$config['postfix_string'] 	= "/?user_search=$rr&perpage=$perpage"; 

        $page 					=  $this->uri->segment(3,0) ;
		
		$data["uri_segment"]	= $config["uri_segment"];
		
		$data['total_rows']		= $config['total_rows']; 
		
		$data["per_page"]		= $config["per_page"];
		
		$data['post_url']		= $config['postfix_string'];
	
		$data['user_srch']		= $user_srch;
		
		$data['last_page']		= $page;
       
        $data["links"] 			= $this->pagination->create_links();
		
		
		
		
		$this->load->view('header');
 		$this->load->view('shortlistedcandidate/shortlistedcandidate', $data);
		$this->load->view('footer');
		
	}

	public function delete_shortlistedcandidate($id){
		if($this->Shortlistedcandidate_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Candidate has been removed from shortlist successfully.');
			redirect(base_url()."shortlistedcandidate/");exit;
		}
		else{
			$this->session->set_flashdata('error', 'Some problem exists. Try again.');
			redirect(base_url()."shortlistedcandidate/");exit;
		}
	}
	
	public function view_shortlistedcandidate(){
		$user_id = $this->uri->segment(3);
		$data['candidate'] = $this->Shortlistedcandidate_model->get_staffenrollment($user_id);
		//echo "<pre>";
		//print_r($data); exit;
		$this->load->view('header');
 		$this->load->view('shortlistedcandidate/viewcandidate', $data);
		$this->load->view('footer');
	}
	
	public function selected_message(){
		
		$message = array();
		$sendmessege = array();
		$user_id		=   $this->session->userdata('user_id');
		//$userdata		=   $this->Shortlistedcandidate_model->get_School_ID($user_id);
		$school_id		=   $this->session->userdata('user_school_id');
		//$branch_id		=   $userdata->branch_id;
		$message['from'] = $user_id;
		$message['school_id'] = $school_id;
		//$message['branch_id'] = $branch_id;
		$message['is_active'] = '1';
		$message['is_deleted'] = '0';
		$message['created_date']		=	date('Y-m-d H:i:s');
		
		if($this->input->post('subject')!=''){
			$subject = $this->input->post('subject');
		}
		
		if($this->input->post('message')!=''){
			$email_message = $this->input->post('message');
		}
		if($this->input->post('notificationid')!=''){
			$notificationid = $this->input->post('notificationid');
		}	
		 
		if($this->input->post('toemail')!=''){
			$toemail = $this->input->post('toemail');
			$applied_by = $this->input->post('applied_by');

			if($applied_by == 'student')
			{
				
				$get_student = $this->Shortlistedcandidate_model->getUser($toemail);

				$to = $get_student->email;
			}
			else
			{
				$to = $toemail;
			}
		}
		
		$mail= $this->authorize->send_email($to,$email_message,$subject);
		//print_r($mail); exit;
		
		if($mail){
			//$this->Shortlistedcandidate_model->delete($notificationid);
			$this->session->set_flashdata('success', 'Message has been successfully send.');
			redirect(base_url()."shortlistedcandidate/"); exit;
		}else{
			$this->session->set_flashdata('error', 'Some problem exists. Messsage can not be send.');
			redirect(base_url()."shortlistedcandidate/");exit;
		}
		
	}
	
	public function reject(){
		$message = array();
		$sendmessege = array();
		$user_id		=   $this->session->userdata('user_id');
		//$userdata		=   $this->Admindashboard_model->get_School_ID($user_id);
		$school_id		=   $this->session->userdata('user_school_id');
		//$branch_id		=   $userdata->branch_id;
		$message['from'] = $user_id;
		$message['school_id'] = $school_id;
		//$message['branch_id'] = $branch_id;
		$message['is_active'] = '1';
		$message['is_deleted'] = '0';
		$message['created_date'] = date('Y-m-d H:i:s');
		
		if($this->input->post('subject')!=''){
			$subject = $this->input->post('subject');
		}
		
		if($this->input->post('message')!=''){
			$email_message = $this->input->post('message');
		}
		if($this->input->post('notificationid')!=''){
			$notificationid = $this->input->post('notificationid');
		}	
		 
		if($this->input->post('toemail')!=''){
			$toemail = $this->input->post('toemail');
			$applied_by = $this->input->post('applied_by');

			if($applied_by == 'student')
			{
				$to = $toemail;
				//$get_student = $this->Admindashboard_model->getUser($toemail);
				//$to = $get_student->email;
			}
			else
			{
				$to = $toemail;
			}
		}
		$mail= $this->authorize->send_email($to,$email_message,$subject);
		
		if($mail){
			//$this->Admindashboard_model->delete($notificationid);
			$this->session->set_flashdata('success', 'Message has been successfully send.');
			redirect(base_url()."shortlistedcandidate/"); exit;
		}
		else{
			$this->session->set_flashdata('error', 'Some problem exists. Messsage can not be send.');
			redirect(base_url()."shortlistedcandidate/");exit;
		}
	}
	
	public function confirminterview(){
		$message = array();
		$sendmessege = array();
		$user_id		=   $this->session->userdata('user_id');
		//$userdata		=   $this->Admindashboard_model->get_School_ID($user_id);
		$school_id		=   $this->session->userdata('user_school_id');
		//$branch_id		=   $userdata->branch_id;
		$message['from'] = $user_id;
		$message['school_id'] = $school_id;
		//$message['branch_id'] = $branch_id;
		$message['is_active'] = '1';
		$message['is_deleted'] = '0';
		$message['created_date'] = date('Y-m-d H:i:s');
		
		if($this->input->post('subject')!=''){
			$subject = $this->input->post('subject');
		}
		
		if($this->input->post('message')!=''){
			$email_message = $this->input->post('message');
		}
		if($this->input->post('notificationid')!=''){
			$notificationid = $this->input->post('notificationid');
		}	
		 
		if($this->input->post('toemail')!=''){
			$toemail = $this->input->post('toemail');
			$applied_by = $this->input->post('applied_by');

			if($applied_by == 'student')
			{
				$to = $toemail;
				//$get_student = $this->Admindashboard_model->getUser($toemail);
				//$to = $get_student->email;
			}
			else
			{
				$to = $toemail;
			}
		}
		$mail= $this->authorize->send_email($to,$email_message,$subject);
		
		if($mail){
			//$this->Admindashboard_model->delete($notificationid);
			$this->session->set_flashdata('success', 'Message has been successfully send.');
			redirect(base_url()."shortlistedcandidate/"); exit;
		}
		else{
			$this->session->set_flashdata('error', 'Some problem exists. Messsage can not be send.');
			redirect(base_url()."shortlistedcandidate/");exit;
		}
	}
	
	public function notaccepted(){
		$message = array();
		$sendmessege = array();
		$user_id		=   $this->session->userdata('user_id');
		//$userdata		=   $this->Admindashboard_model->get_School_ID($user_id);
		$school_id		=   $this->session->userdata('user_school_id');
		//$branch_id		=   $userdata->branch_id;
		$message['from'] = $user_id;
		$message['school_id'] = $school_id;
		//$message['branch_id'] = $branch_id;
		$message['is_active'] = '1';
		$message['is_deleted'] = '0';
		$message['created_date'] = date('Y-m-d H:i:s');
		
		if($this->input->post('subject')!=''){
			$subject = $this->input->post('subject');
		}
		
		if($this->input->post('message')!=''){
			$email_message = $this->input->post('message');
		}
		if($this->input->post('notificationid')!=''){
			$notificationid = $this->input->post('notificationid');
		}	
		 
		if($this->input->post('toemail')!=''){
			$toemail = $this->input->post('toemail');
			$applied_by = $this->input->post('applied_by');

			if($applied_by == 'student')
			{
				$to = $toemail;
				//$get_student = $this->Admindashboard_model->getUser($toemail);
				//$to = $get_student->email;
			}
			else
			{
				$to = $toemail;
			}
		}
		$mail= $this->authorize->send_email($to,$email_message,$subject);
		
		if($mail){
			//$this->Admindashboard_model->delete($notificationid);
			$this->session->set_flashdata('success', 'Message has been successfully send.');
			redirect(base_url()."shortlistedcandidate/"); exit;
		}
		else{
			$this->session->set_flashdata('error', 'Some problem exists. Messsage can not be send.');
			redirect(base_url()."shortlistedcandidate/");exit;
		}
	}
	
	
	public function accepted(){
		$message = array();
		$sendmessege = array();
		$user_id		=   $this->session->userdata('user_id');
		//$userdata		=   $this->Admindashboard_model->get_School_ID($user_id);
		$school_id		=   $this->session->userdata('user_school_id');
		//$branch_id		=   $userdata->branch_id;
		$message['from'] = $user_id;
		$message['school_id'] = $school_id;
		//$message['branch_id'] = $branch_id;
		$message['is_active'] = '1';
		$message['is_deleted'] = '0';
		$message['created_date'] = date('Y-m-d H:i:s');
		
		if($this->input->post('subject')!=''){
			$subject = $this->input->post('subject');
		}
		
		if($this->input->post('message')!=''){
			$email_message = $this->input->post('message');
		}
		if($this->input->post('notificationid')!=''){
			$notificationid = $this->input->post('notificationid');
		}	
		 
		if($this->input->post('toemail')!=''){
			$toemail = $this->input->post('toemail');
			$applied_by = $this->input->post('applied_by');

			if($applied_by == 'student')
			{
				$to = $toemail;
				//$get_student = $this->Admindashboard_model->getUser($toemail);
				//$to = $get_student->email;
			}
			else
			{
				$to = $toemail;
			}
		}
		$mail= $this->authorize->send_email($to,$email_message,$subject);
		
		if($mail){
			//$this->Admindashboard_model->delete($notificationid);
			$this->session->set_flashdata('success', 'Message has been successfully send.');
			redirect(base_url()."shortlistedcandidate/"); exit;
		}
		else{
			$this->session->set_flashdata('error', 'Some problem exists. Messsage can not be send.');
			redirect(base_url()."shortlistedcandidate/");exit;
		}
	}
	
	public function move_to_application_bank($sender_id, $school_id, $branch_id){

		$application_data = array();
		$user_id		=   $this->session->userdata('user_id');
		
		$application_data['moved_by'] = $user_id;
		$application_data['user_id'] = $sender_id;
		$application_data['school_id'] = $school_id;
		$application_data['branch_id'] = $branch_id;
		$application_data['created']		=	date('Y-m-d H:i:s');

		$move = $this->Shortlistedcandidate_model->move_to_application_bank($application_data);

		if($move)
		{
			$this->Shortlistedcandidate_model->deleteenroll($sender_id);
			$this->session->set_flashdata('success', 'Data has been successfully moved to application bank.');
			redirect(base_url()."shortlistedcandidate/"); exit;
		}
		else{
			echo "in else"; exit;
			$this->session->set_flashdata('error', 'Some problem exists. Data has not been moved to application bank.');
			redirect(base_url()."shortlistedcandidate/");exit;
		}

	}
	
}

