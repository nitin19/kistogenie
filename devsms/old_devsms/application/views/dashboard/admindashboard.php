<?php error_reporting(0);?>
  <div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">
    
 

    <div class="col-sm-10 col-xs-8 nopadding menubaritems">

        <ul>

		<li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>

        <li class="edit"><a href="<?php echo base_url(); ?>admindashboard">Admin Dashboard</a></li>        

        </ul>

        </div>
        
    <div class="col-sm-2 col-xs-4 nopadding">

<input type="button" class="btn btn-success sndmsg" data-toggle="modal" data-target="#myModal" value="Send Message" style="padding: 4px 20px;">


        <form action="<?php echo base_url()?>admindashboard/insertmessage" method="post" id="notificationform" enctype="multipart/form-data"  accept-charset="utf-8" autocomplete="off">
        <!-- The Modal -->
        <div id="myModal" class="modal" style="z-index:99999999999">
        
          <!-- Modal content -->
          <div class="modal-content">
          	<div class="modalbg">
           <div class="col-sm-12 compose nopadding" style="padding: 0 8px;"><div class="col-sm-8 nopadding composemsg"><h3>Compose Message</h3> </div><div class="col-sm-4 nopadding composeclose"><button type="button" class="close closebtn" data-dismiss="modal" aria-hidden="true">&times;</button></div></div>
                    	<div class="col-sm-12" style="margin-top: 10px;">
                        	
                        	<div class="col-sm-3 nopadding popup">
	                    		From:
                            </div>
                            <div class="col-sm-9 nopadding popup">
                            	<input type="hidden" name="from_id" id="from_id" class="form-control" value="<?php echo $this->session->userdata('user_id'); ?>"/>
    	          				<input type="text" name="from" id="from" class="form-control" value="<?php echo $this->session->userdata('user_name'); ?>" readonly />
          					</div>
                            <div class="col-sm-3 nopadding popup">
                              To:
                            </div>
                            <div class="col-sm-9 nopadding popup">
                            
                    <div class="checkboxdiv profilecheckbox">
					   <input name="toemail[]" id="All" value="All" type="checkbox" class="subcat_al" >
    			            <label for="All"><span class="checkbox">All</span></label>
                            </div>
                            
                	<div class="checkboxdiv profilecheckbox">
						<input name="toemail[]" id="Staff" value="Staff" type="checkbox" class="subcats">
    						<label for="Staff"><span class="checkbox">Staff</span></label></div>
                    <div class="checkboxdiv profilecheckbox" style=" margin-bottom: 10px;">
						<input name="toemail[]" id="Student" value="Student" type="checkbox" class="subcats">
    						<label for="Student"><span class="checkbox">Student</span></label></div>
                                
                                <!--<input type="text"  name="to" id="to" class="form-control" placeholder="Enter Name"/>-->
                       <input id="touser" size="50"  name="touser" class="form-control" type="text">
                              
                             </div>
                                 <div class="col-sm-3 nopadding popup">
                              Subject:
                            </div>
                            <div class="col-sm-9 nopadding popup">
                            	<input type="text" name="subject" id="subject" class="form-control" maxlength="251"  />
                             </div>
                             <div class="col-sm-3 nopadding popup">
                        		Message:
                              </div>
                              <div class="col-sm-9 nopadding popup"> 
                        		<textarea name="message" rows="3"  id="message" class="form-control"  maxlength="5001"></textarea></br>
                        	  </div>
                                  <div class="col-sm-3 nopadding popup" style="padding: 0 12px;">
                              Attachments:
                            </div>
                           	<div class="col-sm-5 nopadding popup" style="margin-left:10px">
                            	<input type="file" name="attachments[]" multiple />
                                <span class="attchmnttxt">only .jpg, .jpeg, .pdf, .docx, .txt, .png, .xls
                                 files can be uploaded(max. size:2mb).</span>
                             </div>
                              <div class="col-sm-3 nopadding popupbtn" style="margin-left:20px">
                        		<input type="submit" name="send" class="compose" id="send" value="Send" />
                               </div>
                        </div>
        		</div>
              </div>
              </div>
     	</form>
     	

        </div>

<!--        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="quickaction" value="Quick actions">

        </div>-->

        </div>
        
    <!--    <div class="col-sm-12">
    <div id="donutchart"  style="padding:15px 0px;"></div>
</div>-->

<div style="clear:both"></div>
 <?php if($this->session->flashdata('error')): ?>
   <div class="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<b>Alert!</b> 
			  <?php echo $this->session->flashdata('error'); ?>
		</div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
     <div class="alert alert-success alert-dismissable" >
		   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		   <b>Alert!</b> 
		   <?php echo $this->session->flashdata('success'); ?>
	   </div>
<?php endif; ?>
 <div style="clear:both"></div>



<div class="col-sm-12 profilemenu_notification nopadding">
    <h4>Notifications</h4>
    
    <div class="tablewrapper">

    <?php if(count($data_rows) > 0 ){ 
	     $sr=$last_page;
	 ?>
     <table class="table-bordered table-striped">
				<thead>
				  <tr class="headings">

					  <th class="column3">Sender</th>

                      <th class="column9">Sender Name & Message</th>
                   
					  <th class="column5">Date</th>

					  <th class="column3">Actions</th>

				  </tr>

			  </thead>

				<tbody>
<?php
        foreach ( $show as $notification ) {
			      $sr++;
              ?>
					<tr class="familydata communication">

						<td class="column3">
                        <div class="imgsec">
						<?php 
						$sender_id = $notification->from;
						$this->load->model(array('Admindashboard_model'));
						$senderName = $this->Admindashboard_model->getsenderName($sender_id);
						$senderimage = $this->Admindashboard_model->getsenderimage($sender_id);
						$img_name=$senderimage->profile_image;
					?>
                   <?php   if( $senderimage->profile_image!='' ) { ?>
                   <img src="<?php echo base_url().'uploads/staff/'.$senderimage->profile_image; ?>" alt="no image">
    				 <?php   } else { ?>
		            <img src="<?php echo base_url();?>assets/images/avtar.png">
				   <?php }	?>
					  					
                        </div>
                        </td>
                        <td class="column9">
                        <div class="contentsec">
                        
                        <p><?php 
						$sender_id = $notification->from;
						$this->load->model(array('Admindashboard_model'));
						$senderName = $this->Admindashboard_model->getsenderName($sender_id);
						echo $senderName->username; 
						?></p>
                        <span><?php echo substr($notification->message, 0,60); if(strlen($notification->message) > 60 ) { echo '...'; }?></span>
                        </div>
                        </td>

						<td class="column5">
						<?php $date= $notification->created_date;
							echo date('d F y',strtotime($date));
						?></td>
                        <td class="column3 spacing">
                        
                        <a class="viewmessage"  id="viewmsg" data-value="<?php echo $notification->notification_id;?>"       										    		data-toggle="modal" data-target="#viewModal<?php echo $notification->notification_id;?>"><i class="fa fa-eye icnclass"></i></a>
                        
                        <a class="deletemsg" onclick="delConfirm('<?php echo $notification->notification_id;?>')"><i class="fa fa-trash-o icnclass"></i></a>
                     
                        </td>

                        </tr>
     <div id="viewModal<?php echo $notification->notification_id;?>" class="modal" style="z-index:99999999999">
        
          <!-- Modal content -->
          <div class="modal-content">
          <div class="modalbg">
                <div class="col-sm-12 nopadding" style=" background:#36b047 none repeat scroll 0 0;">
                	<div class="col-sm-8 nopadding viewmodal"><h3>View Message</h3></div>
                    <div class="col-sm-4 btncls"><button type="button" class="close closebtn" data-dismiss="modal" aria-hidden="true">&times;</button></div>
                 </div>

                    	<div class="col-sm-12 msgbox">
                        	
                        	<div class="col-sm-12 nopadding">
                            <div class="col-sm-3 nopadding popup">
                              <label for="sender">From:</label>
                            </div>
                            <div class="col-sm-9 nopadding popup">
                            <?php $sender_id = $notification->from;
						$this->load->model(array('Admindashboard_model'));
						$senderName = $this->Admindashboard_model->getsenderName($sender_id);?>
							<?php echo $senderName->username; ?>
                             </div>
                             </div>
                             <div class="col-sm-12 nopadding">
                                 <div class="col-sm-3 nopadding popup">
                              <label for="subject">Subject:</label>
                            </div>
                            <div class="col-sm-9 nopadding popup">
                            	
								<?php echo $notification->subject;?>
							
                                
                             </div>
                             </div>
                             <div class="col-sm-12 nopadding">
                             <div class="col-sm-3 nopadding popup">
                        		<label for="message">Message:</label>
                              </div>
                              <div class="col-sm-9 nopadding popup"> 
                        		<?php echo $notification->message;?>
                        	  </div>
                              </div>
                              <div class="col-sm-12 nopadding">
                                  <div class="col-sm-3 nopadding popup" style="padding:10px !important;">
                              <label for="attachment">Attachments:</label>
                            </div>
                <div class="col-sm-9 nopadding popup">
                            	
         <ul class="attachment_section">
         <?php 
		
		 if($notification->attachments!='') {
			
			 $attachments =  explode(',',$notification->attachments);
			
			 
			 foreach($attachments as $attachment) { ?>
           <li>
                <h4><?php 
                  $string = $attachment;
				  echo substr($string,0,10); if(strlen($string) > 10 ) { echo '...'; }
                 // $string1 = (strlen($string) > 5) ? substr($string,0,30).'...' : $string;  
                 // echo $string1; 
	              ?>
                    <div class="icon_section">
                  <a href="<?php echo base_url();?>admindashboard/download_attachments/<?php echo $attachment;?>" data-toggle="tooltip" title="Download" data-placement="top"><i class="fa fa-download"></i></a>
        
                  <a href="<?php echo base_url();?>uploads/notification/<?php echo $attachment;?>" target="_blank" data-toggle="tooltip" title="View" data-placement="top"><i class="fa fa-eye"></i></a>
                  </div>
                  </h4>
               </li> 
			<?php }
		   } else {
		      echo 'No attachments';
		    }
		 ?>
         </ul>
       
                             </div>
                             </div>
							</div>
</div>
              </div>  
	</div>
                        
                  <?php	} ?>
                  
             	</tbody>
		     </table> 
              
		<?php 	}  else { ?>
     <table class="table-bordered table-striped">
				<thead> </thead>
				<tbody>         
 <tr><th colspan="7" style="text-align: center; width:1215px;height:70px;Font-size:22px; background:#FFF;">No Message to show.</th></tr>
          	</tbody>
		  </table>  
	
	<?php } ?>

			 
 	<div class="col-sm-12 paginationdiv nopadding">
     <div class="col-sm-9 col-xs-6 paginationblk">
    <?php 
							if(count($data_rows) > 0){
			                 $last_page1=$last_page;
								?>
		<span id="shortlist"> Showing <?php echo ++$last_page;?> to <?php echo $sr++;?> of <?php echo $total_rows++;?> entries. </span>
								<?php
							    }
							?>   
			
	<ul class="pagination">
		<?php echo $pagination;?>
	</ul>

    </div>
    </div>
</div>
    
    
    
    </div>



    <div class="col-sm-12 nopadding">
    
<div class="col-sm-6 leftspace ">
<div class="col-sm-12 prfltitlediv nopadding">

        <div class="col-sm-11 col-xs-10 prfltitle">

        <h1>School and Branch Details </h1>

        </div>

        </div>
<div class="col-sm-6 todayattendancediv dashboard-attendance">
        <div class="col-sm-12 nopadding attendanceicon atticon">
<!--        <img src="<?php echo base_url(); ?>assets/images/dottedicon.png"  title="this will be displayed as a tooltip">

-->        <i class="fa fa-question-circle" data-toggle="tooltip" title="Displays the  Total Number of Branches, Staff, Students in a School" data-placement="top"></i>

        </div>
        
        <div class="col-sm-12 nopadding titlediv">
        <div class="col-sm-8">
        <h1>School Details</h1>
        </div>
        <div class="col-sm-4 stuatten">
        <h1><br><span></span></h1>
        </div>
        </div>
        
        <div class="col-sm-12 nopadding" style="padding: 30px 0px;">
        
        <div id="chartContainer1" class="chartdiv" style="width: 150px;height:200px; margin:auto;"><div class="canvasjs-chart-container" style="position: relative; text-align: left; cursor: auto;"><canvas class="canvasjs-chart-canvas" width="150" height="140" style="position: absolute;"></canvas><canvas class="canvasjs-chart-canvas" width="150" height="140" style="position: absolute; cursor: default;"></canvas><div class="canvasjs-chart-toolbar" style="position: absolute; right: 1px; top: 1px;"></div><div class="canvasjs-chart-tooltip" style="position: absolute; height: auto; box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1); z-index: 1000; display: none; border-radius: 5px; left: 59px; bottom: -39px;"><div style="width: auto; height: auto; min-width: 50px; margin: 0px; padding: 5px; font-family: Calibri,Arial,Georgia,serif; font-weight: normal; font-style: italic; font-size: 14px; color: rgb(0, 0, 0); text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1); text-align: left; border: 2px solid rgb(87, 185, 74); background: rgba(255, 255, 255, 0.9) none repeat scroll 0% 0%; text-indent: 0px; white-space: nowrap; border-radius: 5px; -moz-user-select: none;">95 %</div></div><a class="canvasjs-chart-credit" style="outline:none;margin:0px;position:absolute;right:3px;top:126px;color:dimgrey;text-decoration:none;font-size:10px;font-family:Lucida Grande, Lucida Sans Unicode, Arial, sans-serif" tabindex="-1" target="_blank" href="http://canvasjs.com/">CanvasJS.com</a></div></div>
        <div class="attendance-status" style="top:0;">
        <div class="present statusdiv">
        <span><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/greenicon.png">Branch</span>
        </div>
        <div class="present statusdiv">
        <span><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/blueicon.png">Staff</span>
        </div>
        <div class="present statusdiv">
        <span><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/yellowicon.png">Student</span>
        </div>
        </div>
        </div>
        </div>
 <div class="col-sm-6 todayattendancediv dashboard-attendance">
        <div class="col-sm-12 nopadding attendanceicon atticon">
        <!--<img src="<?php echo base_url(); ?>assets/images/dottedicon.png">-->
        
          <ul style="margin-bottom:0px;">
    <li class="dropdown userdropdown" style="list-style: outside none none;">

          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> 
           <i class="fa fa-ellipsis-h fa-lg" data-toggle="tooltip" title="Select Your Branch " data-placement="top"></i> </a>

          <ul class="dropdown-menu" style="z-index:9999999999999999;">
          <?php 
		  $pageno = $this->uri->segment(3);
		  if($pageno == ''){
			  $pageno = '0';
		  }
		  foreach ( $schoolbranches as $branchschool )
        { ?>
			<li data-value=""><a href="<?php echo base_url(); ?>admindashboard/index/<?php echo $pageno; ?>/<?php echo $branchschool->branch_id; ?>"><?php echo $branchschool->branch_name; ?></a></li>
<?php } ?>
          </ul>

        </li>
  </ul>
        
        </div>
        <div class="col-sm-12 nopadding titlediv">
        <div class="col-sm-8">
        <h1>Branch Details</h1>
        </div>
        <div class="col-sm-4 stuatten">
        <h1><br><span></span></h1>
        </div>
        </div>
        
        
        <div class="col-sm-12 nopadding" style="padding: 30px 0px;">
      
        <div id="chartContainer2" class="chartdiv" style="width: 150px;height:200px; margin:auto;">
       
        <div class="canvasjs-chart-container" style="position: relative; text-align: left; cursor: auto;"><canvas class="canvasjs-chart-canvas" width="150" height="140" style="position: absolute;"></canvas><canvas class="canvasjs-chart-canvas" width="150" height="140" style="position: absolute; cursor: default;"></canvas><div class="canvasjs-chart-toolbar" style="position: absolute; right: 1px; top: 1px;"></div><div class="canvasjs-chart-tooltip" style="position: absolute; height: auto; box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1); z-index: 1000; display: none; border-radius: 5px; left: 59px; bottom: -39px;"><div style="width: auto; height: auto; min-width: 50px; margin: 0px; padding: 5px; font-family: Calibri,Arial,Georgia,serif; font-weight: normal; font-style: italic; font-size: 14px; color: rgb(0, 0, 0); text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1); text-align: left; border: 2px solid rgb(87, 185, 74); background: rgba(255, 255, 255, 0.9) none repeat scroll 0% 0%; text-indent: 0px; white-space: nowrap; border-radius: 5px; -moz-user-select: none;">95 %</div></div><a class="canvasjs-chart-credit" style="outline:none;margin:0px;position:absolute;right:3px;top:126px;color:dimgrey;text-decoration:none;font-size:10px;font-family:Lucida Grande, Lucida Sans Unicode, Arial, sans-serif" tabindex="-1" target="_blank" href="http://canvasjs.com/">CanvasJS.com</a></div>
       
        </div>
        
        
        <div class="attendance-status" style="top:0;">
        <div class="present statusdiv">
        <span><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/greenicon.png">Staff</span>
        </div>
        <div class="present statusdiv">
        <span><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/blueicon.png">Student</span>
        </div>
        <div class="present statusdiv">
        <span><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/yellowicon.png">Classes</span>
        </div>
        </div>
        </div>
       
        </div>
        
</div>
    

    <div class="col-sm-6 rightspace studentsec fullwidsec">

        <div class="profilecompletion">

        <div class="col-sm-12 prfltitlediv nopadding">

        <div class="col-sm-11 col-xs-10 prfltitle">

        <h1>The number of new students</h1>

        </div>


        </div>

        <div class="col-sm-12 processcompleted nopadding">

        <div class="loadingbars nopadding profile-bg bargraph">
        
        <div class="col-sm-12 nopadding attendanceicon">
        <!--<img src="http://www.royalenfieldhimachal.com/demo/olivetree/assets/images/dottedicon.png">-->
        
          <ul style="margin-bottom:0px;">
    <li class="dropdown userdropdown" style="list-style: outside none none;">

          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> 
           <i class="fa fa-ellipsis-h fa-lg" data-toggle="tooltip" title="Select Your Branch " data-placement="top"></i> </a>

          <ul class="dropdown-menu selectbranch" style="z-index:9999999999999999;margin-left:50%">
          <?php foreach ( $schoolbranches as $branchschool )
        { ?>
            <li data-value="<?php echo $branchschool->branch_id; ?>"><a href="#"><?php echo $branchschool->branch_name; ?></a></li>
            <?php } ?>
          </ul>

        </li>
  </ul>
        
        </div>

        <div class="col-sm-9 border-right graph">
        
        

        <!--<img src="<?php echo base_url();?>assets/images/loadingbars.jpg" class="img-responsive">-->
        
        
        <div class="css_bar_graph">
   
   <!-- y_axis labels -->
   <ul class="y_axis">
    <li>$12K</li><li>$8K</li><li>$6K</li><li>$4K</li><li>$2K</li><li>$1K</li><li>$0</li>
   </ul>
   
   <!-- x_axis labels -->
   <ul class="x_axis">
   <?php foreach($totalbranch as $branchname){
	   $string = $branchname->branch_name;
        $string1 = (strlen($string) > 1) ? substr($string,0,3): $string;  
      
      //echo '<li style="overflow-wrap: break-word; white-space: pre-wrap; text-align: right;">'.$string1.'</li>';
	  echo '<li>'.$string1.'</li>';
   }
	   ?>
<!--    <li>2</li><li>4</li><li>6</li><li>8</li><li>10</li><li>12</li><li>14</li><li>16</li><li>18</li><li>20</li><li>22</li><li>24</li><li>26</li><li>28</li><li>30</li>
-->   </ul>
   
   <!-- graph -->
   <div class="graph graphbar">
    <!-- grid -->
    <ul class="grid">
                 <li><!-- 70 --></li>
     <li><!-- 60 --></li>
     <li><!-- 50 --></li>
     <li><!-- 40 --></li>
     <li><!-- 30 --></li>
     <li><!-- 20 --></li>
     <li><!-- 10 --></li>
     <li class="bottom"><span id="right_arrow"><img src="<?php echo base_url();?>assets/images/right_arrow-branch.png"></span><!-- 0 --></li>
    </ul>
    
    <!-- bars -->
    <!-- 250px = 100% -->
    <ul class="graph_bars">
    <span class="greybar bar imgstud" id="top_arrow"><img src="<?php echo base_url();?>assets/images/TOP_arrow1600.png"></span>
     <?php 
	 $school_id		= $this->session->userdata('user_school_id');
	 $sr = 0;
	 foreach($totalbranch as $branchname){
		 $sr =$sr + 1;
		 $branch_id = $branchname->branch_id;
		 $this->load->model(array('Admindashboard_model'));
        $branchName = $this->Admindashboard_model->branchsudents($school_id, $branch_id);
		$title="Students";
	
     echo '<span class="greybar bar'.$sr.'"><li class="bar nr_'.$sr.' graphclass" style="height:'.(count($branchName)*1/2).'%;"><span>'.$title.':'.count($branchName).'</span></li></span>';
      } ?>
    
    </ul> 
   </div>
   
   <!-- graph label -->
   
  
  </div>
        

        </div>

        <div class="col-sm-3 nopadding">

        <div class="monthdate borderbottom" >

        <h1 id="weekstudent"><?php echo $recentweekstudent;?></h1>

        <span>For This Week</span>

        </div>

        <div class="monthdate">

        <h1 id="monthstudent"><?php echo $recentmonthkstudent;?></h1>

        <span>For This Month</span>

        </div>

        </div>

        </div>

        </div>

        </div>

        </div>
        

        </div>

    </div>


<div class="container"> 
		
		
        <div class="toggle">
			<div class="toggle-title  ">
				<h3>
				<i></i>
				<span class="title-name">Recently Added Student</span>
				</h3>
			</div>
			
			<div class="toggle-inner toggleclass">
				<p>
                <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">No.</th>
					  <th class="column4">Name</th>
                      <th class="column4">Branch</th>
<!--                      <th class="column3">Class</th>-->
	 				  <th class="column6">Email</th>
                      <th class="column3">Contact</th>
                      <th class="column2 option">Options</th>
                       
				  </tr>

			  </thead>

				<tbody>
<?php
if($recentstudent){
	$sr=1;
        foreach ( $recentstudent as $studentrecent )
        {
            ?>                
	 					<tr class="familydata staff">
					 
                   		 <td class="column1"><?php echo $sr++; ?></td>

						<td class="column4"><?php echo $studentrecent->student_fname.' '.$studentrecent->student_lname;?></td>
                        
                        <td class="column4"><?php 
						$branch_id = $studentrecent->student_school_branch;
        				$this->load->model(array('Admindashboard_model'));
        				$branchName = $this->Admindashboard_model->getbranchName($branch_id);
						 
						 if($branchName->branch_name!='') {
							  echo @$branchName->branch_name;
							  } else { 
							  echo '<span style="color:transparent;">-</span>';
							  } 
						?></td>

						<!--<td class="column3"><?php /*?><?php
						 $class = $studentrecent->student_class_group;
       					 $this->load->model(array('Admindashboard_model'));
        				 $className = $this->Admindashboard_model->getclassName($class);
						 echo @$className->class_name;?><?php */?></td>-->

						<td class="column6"><?php 
							 	if($studentrecent->email!='') {
							  		echo $studentrecent->email; 
							  		} else { 
							  			echo '<span style="color:transparent;">-</span>';
							   	} ?>
						</td>

                        <td class="column3"><?php 
							 if($studentrecent->student_telephone!='') {
							  echo $studentrecent->student_telephone;
							  } else { 
							  echo '<span style="color:transparent;">-</span>';
							   } ?>
						</td>
         
                        <td class="column2 recentstud"><a href="<?php echo base_url();?>students/view_student/<?php echo $studentrecent->user_id;?>"> View  </a>
											<!--<div class="btn-group">
												<button type="button" class="btn btn-info btn-flat">Action</button>
												<button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
												    <span class="caret"></span>
												    <span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
													<li>
                                                         <a href=""> Edit </a>
													</li>-->
													<!--<li>
					                                     <a href=""> View  </a>
													</li>-->
													<!--<li>
													     <a href="" title="click to deactivate student"> Active </a>
													</li>
													<li class="divider"></li>
									                <li>
                                                        <a onclick="" style="cursor: pointer;"> Delete </a> 
                                                    </li>
												</ul>
											</div>-->
										</td>
 </tr>
 <?php 	}	 } 	?>
	 		</tbody>

		  </table>
          

</div></p>
			</div>
			
			</div><!-- END OF TOGGLE -->    
        <div class="toggle">
			<div class="toggle-title  ">
				<h3>
				<i></i>
				<span class="title-name">Leaving Student</span>
				</h3>
			</div>
			
			<div class="toggle-inner toggleclass">
				<p>
  
                <div class="tablewrapper">

		<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">No.</th>

					  <th class="column4">Name</th>
                      
                      <th class="column4">Branch</th>

<!--					  <th class="column3">Class</th>
-->
                      <th class="column6">Email</th>

                      <th class="column3">Leaving Date</th>
                      
                       <th class="column2 option">Options</th>
                       
                        <!--<th class="column1">Edit</th>
                        <th class="column1">View</th>-->

				  </tr>

			  </thead>

				<tbody>
 <?php
if($leavingstudent){
	$sr=1;
        foreach ($leavingstudent as $studentleaving )
        {
            ?>               
	 					<tr class="familydata staff">
 					 
                  		 <td class="column1"><?php echo $sr++; ?></td>

						<td class="column4"><?php echo $studentleaving->student_fname.' '.$studentleaving->student_lname;?></td>
                        
                        <td class="column4"><?php 
						$branch_id = $studentleaving->student_school_branch;
        				$this->load->model(array('Admindashboard_model'));
        				$branchName = $this->Admindashboard_model->getbranchName($branch_id);

						 if($branchName->branch_name!='') {
							  echo @$branchName->branch_name;
							  } else { 
							  echo '<span style="color:transparent;">-</span>';
							  } 
						
						?></td>

						<!--<td class="column2"><?php /*<?php
						 $class = $studentleaving->student_class_group;
						$this->load->model(array('Admindashboard_model'));
						$className = $this->Admindashboard_model->getclassName($class);
						echo @$className->class_name;?>*/?></td>-->

						<td class="column6"><?php 
						 if($studentleaving->email!='') {
							  echo $studentleaving->email; 
							  } else { 
							  echo '<span style="color:transparent;">-</span>';
							   } ?>
						
						</td>

                        <td class="column3"><?php 
							 if($studentleaving->student_leaving_date!='') {
							  echo $studentleaving->student_leaving_date;
							  } else { 
							  echo '<span style="color:transparent;">-</span>';
							  } ?>
						</td>
         
                        <td class="column2 recentstud"><a href="<?php echo base_url();?>students/view_student/<?php echo $studentleaving->user_id;?>"> View  </a>
										</td>
           </tr>
  <?php } } ?>        
	 		</tbody>

		  </table>
          

</div></p>
			</div>
			
			</div><!-- END OF TOGGLE --> 
		<div class="toggle">
			<div class="toggle-title  ">
				<h3>
				<i></i>
				<span class="title-name">Recently Added Staff</span>
				</h3>
			</div>
			
			<div class="toggle-inner toggleclass">
				<p>
                <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">No.</th>

					  <th class="column4">Name</th>
                      
                      <th class="column4">Branch</th>

					  <th class="column6">Email</th>

                      <th class="column3">Contact</th>

<!--                      <th class="column3">Rank</th>
-->                      
                       <th class="column2 option">Options</th>
                       

				  </tr>

			  </thead>

				<tbody>
                <?php
if($recentstaff){
	$sr=1;
        foreach ($recentstaff as $stafftrecent )
        {
            ?> 
	 					<tr class="familydata staff">
					 
                    <td class="column1"><?php echo $sr++;?></td>
						<td class="column4"><?php if($stafftrecent->staff_fname!='')
												{	echo $stafftrecent->staff_fname.' '.$stafftrecent->staff_lname;
												}else { 
							  				echo '<span style="color:transparent;">-</span>';
							  } 
						?></td>
                        
                        <td class="column4"><?php 
						$branch_id = $stafftrecent->branch_id;
  						$this->load->model(array('Admindashboard_model'));
       					$branchName = $this->Admindashboard_model->getbranchName($branch_id);
						
						 if($branchName->branch_name!='' && $branchName->branch_name!='0') {
							  echo @$branchName->branch_name;
							  } else { 
							  echo '<span style="color:transparent;">-</span>';
							  } 
						?></td>

						<td class="column6"><?php
						if($stafftrecent->email!='') {
							   echo $stafftrecent->email;
							  } else { 
							  echo '<span style="color:transparent;">-</span>';
							  } 
						?></td>
                        
                        <td class="column3"><?php 
						 if($stafftrecent->staff_telephone!='') {
							  echo $stafftrecent->staff_telephone;
							  } else { 
							  echo '<span style="color:transparent;">-</span>';
							  } 
						?></td>

						<!--<td class="column3"><?php echo $stafftrecent->user_type;?></td>-->

         
                        <td class="column2 recentstud"><a href="<?php echo base_url();?>staff/view_user/<?php echo $stafftrecent->user_id;?>"> View  </a>
										</td>
           </tr>
           <?php } } ?>
	 		</tbody>

		  </table>
          

</div></p>
			</div>
			
			</div><!-- END OF TOGGLE -->
  
		</div>
<script src="<?php echo base_url();?>assets/js/canvasjs.min.js"></script>

<script type="text/javascript">
window.onload = function () {
			var chart = new CanvasJS.Chart("chartContainer1", {

				animationEnabled: true,
				theme: "theme2",
				data: [
				{
					
					type: "doughnut",
					indexLabelFontFamily: "Garamond",
					indexLabelFontSize: 20,
					startAngle: 80,
					indexLabelFontColor: "dimgrey",
					indexLabelLineColor: "darkgrey",
					 toolTipContent: "{legendText}:{y}",
					

					dataPoints: [
					{ y: <?php echo count($totalbranch); ?>, legendText: "Branch" , color: "#57b94a" , },
					{ y: <?php echo $totalstaff; ?>, legendText: "Staff" , color: "#3472b2"},
					{ y: <?php echo $totalstudents; ?>, legendText: "Student" , color: "#f5b73c" },

					]
				}
				]
			});

			chart.render();
					var chart = new CanvasJS.Chart("chartContainer2", {

				animationEnabled: true,
				theme: "theme2",
				data: [
				{
					
					type: "doughnut",
					indexLabelFontFamily: "Garamond",
					indexLabelFontSize: 20,
					startAngle: 80,
					indexLabelFontColor: "dimgrey",
					indexLabelLineColor: "darkgrey",
					 toolTipContent: "{legendText}:{y}",
					

					dataPoints: [
					{ y: <?php if($branchstaff){echo count($branchstaff); } else { echo $firstbranchstaff ; } ?>, legendText: "staff" , color: "#57b94a" , },
					{ y:<?php if($branchstudents){echo count($branchstudents); } else { echo $firstbranchstudent ; } ?>, legendText: "student" , color: "#3472b2"},
					{ y:<?php if($branchclasses){echo count($branchclasses); } else { echo $firstbranchclasses ; } ?>, legendText: "classes" , color: "#f5b73c" },

					]
				}
				]
			});

			chart.render();
			
	}
	</script>
        <script>
		$(document).ready(function(){
		if( jQuery(".toggle .toggle-title").hasClass('active') ){
		jQuery(".toggle .toggle-title.active").closest('.toggle').find('.toggle-inner').show();
	}
	jQuery(".toggle .toggle-title").click(function(){
		if( jQuery(this).hasClass('active') ){
			jQuery(this).removeClass("active").closest('.toggle').find('.toggle-inner').slideUp(200);
		}
		else{	jQuery(this).addClass("active").closest('.toggle').find('.toggle-inner').slideDown(200);
		}
	});
	
	
	$('.selectbranch li').click(function(){
		 var branch_id = $(this).attr('data-value');
		 if(branch_id!='') {
 $.ajax({ 
     type: 'POST',
     url: '<?php echo base_url();?>admindashboard/getbranchdata',
     data: {'postdata': branch_id},
     success: function(data){
		 data = jQuery.parseJSON(data);
		 jQuery('.x_axis').empty();
		 jQuery('.x_axis').append(data.Class);
		 jQuery('.graph_bars').empty();
		 jQuery('.graph_bars').append(data.student);
		 jQuery('#weekstudent').empty();
		 jQuery('#weekstudent').append(data.week);
		 jQuery('#monthstudent').empty();
		 jQuery('#monthstudent').append(data.month);
		 jQuery('#right_arrow').empty();
		 jQuery('#right_arrow').append(data.bottom);
      }
 });
  } else {
				 return false;
				 }
});
		});
		
	</script>
    
    
    <script type="text/javascript">

function delConfirm(id){
		if( confirm("Are you sure want to Delete?") ){
			window.location.href ="<?php echo base_url();?>admindashboard/deletemsg/"+id;
		}else{
			return false;
		}
	}
	
	// Validate "Other" textbox
var isOther = document.getElementById("All");
var other = document.getElementById("touser");
var staffcheck=document.getElementById("Staff");
var student=document.getElementById("Student");
isOther.addEventListener("click", function () {
    console.log("All Clicked!");
     if (isOther.checked) {
          other.readOnly = true;
		  other.value='';
		  staffcheck.checked=true;
		  staffcheck.disabled=true;
		  student.checked=true;
		  student.disabled=true;
		  
     } else {
          	  other.readOnly = false;
		  staffcheck.disabled=false;
		  student.disabled=false;
		  staffcheck.checked=false;
		  student.checked=false;
     }
});
other.addEventListener("focus", function (evt) {
     // Checkbox must be checked before data can be entered into textbox
     if (All.checked) {
          this.readOnly = true;
		  staffcheck.disabled=true;
		  student.disabled=true;
     } else {
          	  this.readOnly = false;
  		  staffcheck.disabled=false;
		  student.disabled=false;
		  
     }
});
</script>
  
<script>
/*$("#All").change(function () {
  	$("input:checkbox").prop('checked', $(this).prop("checked"));
});*/
  
$(function() {
    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }
    
    $( "#touser" ).bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
            event.preventDefault();
        }
    })
	
    .autocomplete({
        minLength: 1,
        source: function( request, response ) {
            // delegate back to autocomplete, but extract the last term
            $.getJSON("<?php echo base_url();?>admindashboard/searchemail", { term : extractLast( request.term )},response);
        },
        focus: function() {
            // prevent value inserted on focus
            return false;
        },
        select: function( event, ui ) {
            var terms = split( this.value );
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push( ui.item.value );
            // add placeholder to get the comma-and-space at the end
            terms.push( "" );
            this.value = terms.join( ", " );
            return false;
        }
    });
});
</script>

<script>
  jQuery(document).ready(function(){
	  
/*	jQuery('.subcats').click(function(){
	    jQuery(".subcat_al").removeAttr('checked');
	  });

 jQuery('.subcat_al').click(function(){
	    jQuery(".subcats").removeAttr('checked'); 
		 jQuery(".subcat_al").prop( 'checked', true );
	  });
	  
	  
jQuery(".subcats").click(function () {
    if (jQuery(".subcats:checked").length == 0) {  
		 jQuery(".subcat_al").prop( 'checked', true );
	}
});*/
	  
jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });
	
    jQuery("#notificationform").validate({
        rules: {
                subject: {
                   required: true,
				   maxlength: 250
               },
			   message: {
                   required: true,
     			   maxlength: 5000
               }
        },
        messages: {
              subject: {
                  required: "This field is required.",
				  maxlength: "Maximum 250 characters allowed."
                 },
			  message: {
                  required: "This field is required.",
				  maxlength: "Maximum 5000 characters allowed."
                 }
            },
       
        submitHandler: function(form) {
            form.submit();
          }
        });

	});
</script>
        
        <style>
		/* Styles for page */
.container {
  width: 100%;
  margin: 10px auto;
  float:left;
  padding: 0 20px;
}

body {
  color: #4B4B4B;
  font-family: sans-serif;
}
body a {
  cursor: pointer;
  color: #4B4B4B;
  text-decoration: none;
}
body section {
  margin-bottom: 90px;
}
body section h1 {
  text-transform: uppercase;
  text-align: center;
  font-weight: normal;
  letter-spacing: 10px;
  font-size: 25px;
  line-height: 1.5;
}
body section p, body section a {
  text-align: center;
  letter-spacing: 3px;
}

span {
  letter-spacing: 0px;
}

p {
  font-weight: 200;
  line-height: 1.5;
  font-size: 14px;
}

/* Styles for Accordion */
.toggle:last-child {
  border-bottom: 1px solid #dddddd;
}
.toggle .toggle-title {
  position: relative;
  display: block;
  border-top: 1px solid #dddddd;
  margin-bottom: 6px;
}
.toggle .toggle-title h3 {
  font-size: 20px;
  margin: 0px;
  line-height: 1;
  cursor: pointer;
  font-weight: 200;
}
.toggle .toggle-inner {
  padding: 7px 25px 10px 25px;
  display: none;
  margin: -7px 0 6px;
}
.toggle .toggle-inner div {
  max-width: 100%;
}
.toggle .toggle-title .title-name {
  display: block;
  padding: 25px 25px 14px;
}
.toggle .toggle-title a i {
  font-size: 22px;
  margin-right: 5px;
}
.toggle .toggle-title i {
  position: absolute;
  background: url("<?php echo base_url();?>assets/images/plus_minus.png") 0px -24px no-repeat;
  width: 24px;
  height: 24px;
  -webkit-transition: all 0.3s ease;
  transition: all 0.3s ease;
  margin: 20px;
  right: 0;
}
.toggle .toggle-title.active i {
  background: url("<?php echo base_url();?>assets/images/plus_minus.png") 0px 0px no-repeat;
}
.toggle{background-color:#fff;}

.fa-ellipsis-h  { cursor:pointer; color:#4b4b4b;}

.fa-ellipsis-h  + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa-ellipsis-h  + .tooltip > .tooltip-arrow {border-top-color:#090 !important;}

.fa.fa-question-circle  { cursor:pointer; }
.fa.fa-question-circle + .tooltip > .tooltip-inner {background-color: #090 !important; color:#fff;}
.fa.fa-question-circle + .tooltip > .tooltip-arrow{border-top-color:#090 !important;}

.css_bar_graph {
    float: left;
    width: 360px;
	overflow-y:hidden;
}

div.css_bar_graph ul.x_axis{
left:13px !important;
width:440px !important;
}
div.css_bar_graph ul.x_axis li{
   font-size: 10px !important; 
   padding: 0 12px 0 18px !important;
}
div.css_bar_graph div.graph li.bar span
{top:0px !important;}

.atticon{padding:0px;}
@media screen and (min-width:320px) and (max-width:480px){
	.todayattendancediv img {
width:auto !important;
}
.sndmsg {
    font-size: 11px;
    width: 80px;
	padding:5px !important;
}
	}
	
@media screen and (min-width:767px) and (max-width:1024px){

.sndmsg {
    font-size: 11px;
    width: 80px;
	padding:5px !important;
}

.leftspace {
    width: 100%;
}
	}

@media screen and (min-width:981px){
.toggle .img-responsive {
    display: inline;
    width: 24%;
} }

.editprofile-content {
    min-height: 200px;
}
.profile-bg{padding: 0 20px;}
.staff .recentstud > a {
    background: #57b94a none repeat scroll 0 0;
    border-radius: 3px;
    color: #fff;
    padding: 5px 25px 6px;
	text-decoration:none;
	float:right;
}
.todayattendancediv {
    min-height: 345px;
}
.greybar.bar.imgstud {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    left: -10px !important;
}
.greybar.bar.imgstud > img {
    height: 200px !important;
    width: 35px;
}
.bottom img {
    height: 25px;
    width: 155px;
}
div.css_bar_graph div.graph li {
    height: 30px;
}
		</style>
        
<style>
.spacing {
    padding: 0;
}
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index:2147483647 !important; /* Sit on top */
    padding-top: 50px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
	/*padding: 20px;
 	border: 10px solid #579b4a;*/
    width: 600px;
    top: 80px;
}

/* The Close Button */
/*.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}
*/
.closebtn{
    background: #fff none repeat scroll 0 0 !important;
    border-radius: 40px;
    float: right;
    height: 40px;
    opacity: 1;
    width: 40px;
	color:#36b047;
}
.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.col-sm-3.popup{
	padding:10px;
}
.col-sm-9.popup{
padding:10px;
}
.compose{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
    padding:5px 30px;
	float:right;
}

.composeclose{
padding:8px 0px 0px 0px;
}
.btncls{padding-top:5px;}
/*.viewmessage{
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533;
    border-radius: 3px;
    color: #fff;
	padding:5px 7px;
}*/
</style>
<style>

.viewmodal> h3 {
   background: #36b047 none repeat scroll 0 0;
   color: #fff;
   text-align: end;
   text-transform: uppercase;
}
.composemsg> h3 {
   background: #36b047 none repeat scroll 0 0;
   color: #fff;
   text-align: end;
   text-transform: uppercase;
}

.attachment_section {
    list-style: outside none none;
    padding-left: 0;
}
.attachment_section li h4 {
	font-size:14px;
}
.icon_section {
    float: right;
}

#myModal > h3 {
    background: #36B047;
    border-radius: 5px;
    color: #fff;
    padding: 13px 0;
    text-align: center;
}	
#myModal .modal-content {
    background-color: #fefefe;
/*    border: 5px solid #36B047;*/
  
    margin: auto;
    padding: 0px;
    top: 30px;
    width: 600px;
}
.ui-autocomplete-input, .ui-menu, .ui-menu-item { z-index: 2147483647; }
.col-sm-3.nopadding.popup {
    font-size: 14px;
    font-weight: bold;
}

.profilemenu_notification {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #dfe3e9;
    border-radius: 5px;
    float: left;
    margin: 10px 0;
    padding: 15px 10px;
    width: 100%;
} 

.bargraph{
    padding: 0 0 0 5px;
}

.graphclass{z-index:0px !important;} 

.toggleclass{padding:7px 0px 10px 0px !important;}
.option{text-align:center;}
.msgbox{
	padding:10px 0 0 20px;
}

.tablewrapper .familydata td i.icnclass{
    color: inherit !important;}
	
.viewmessage:hover{
	color:#09F;
}
.deletemsg:hover{
	color:#09F;
}
.attchmnttxt{font-size:11px;}



.checkboxdiv.profilecheckbox > label {
    width:80px !important;
}
.modalbg {
    background: #fff none repeat scroll 0 0;
    float: left;
    width: 100%;
	border-radius:5px;
	}

.icon_section > a {
    padding-right: 15px;
}
.icon_section > a:hover{
	color:#09F;
}
.icon_section > a:hover{
	color:#09F;
}
.prfltitle > h1 {
    padding-left: 12px;
}
 /***** 16-aug *****/
 
       .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
           
            overflow-x: hidden;
           
        } 
</style>