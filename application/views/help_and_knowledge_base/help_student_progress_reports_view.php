<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js" type="text/javascript"></script>

<div class="editprofile-content">

    	<div class="profilemenus">

        <ul>

		<?php 

         $user_type = $this->session->userdata('user_type');

          if ($user_type == 'teacher'){?>
          
          <li><a href="<?php echo base_url(); ?>teacherdashboard">Home</a></li>

        <?php  } elseif($user_type == 'student'){?>

          <li><a href="<?php echo base_url(); ?>studentdashboard">Home</a></li>

        <?php }else {?>

          <li><a href="<?php echo base_url(); ?>admindashboard">Home</a></li>
          
        <?php } ?>

 <li><a href="<?php echo base_url(); ?>help">Help and Knowledgebase</a></li>

        <li class="edit">Student Progress Report</li>    

        </ul>

        </div>

   <div class="block-content">	

      <div class="col-sm-12 tablediv nopadding">
      

    <div class="col-sm-3">
	<div class="list-block">
      <ul class="nav nav-tabs tabs-left" role="tablist">
      <li role="presentation" class="active"><a href="#progress_report" aria-controls="home" role="tab" data-toggle="tab">Progress Reports</a></li>
	  <li role="presentation"><a href="#term_section" aria-controls="profile" role="tab" data-toggle="tab">Terms</a></li>
      <li role="presentation"><a href="#report_archive" aria-controls="profile" role="tab" data-toggle="tab">Report Archives</a></li>
    </ul>
	</div>
    </div>
    <div class="col-sm-9">
      <div class="tab-content adm-dsh">
		<div role="tabpanel" class="tab-pane active" id="progress_report">
			<h1 class="tab-title">Progress Reports</h1>
			<p class="main-heading-cntnt"> It is a ready to use form for assessment of student’s class work ,homework and participation is a quick and easy way to keep student’s evaluation  organized.</p>
			<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/student-progress-report.jpg" class="stt">
			<div class="admn-content">
					<h3 class="para-heading">This module consist in two sections:</h3>
					<p class="heading-cntnt"> <strong>1) Term</strong></p>
					<p class="heading-cntnt"><strong>2) Report archive</strong></p>	
			</div>
	 </div>
	 
	  
<!-- Modify-student--->
	 
	  <div role="tabpanel" class="tab-pane" id="term_section">
			<h1 class="tab-title">Term</h1>
			<p class="main-heading-cntnt">In this there are two sections.</p>
			<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/Terms_section_student_progress_report.jpg" class="stt">
			
			<div class="admn-content">
				
				<div class="compse-msg">
					<h3 class="para-heading">HOW TO VIEW THE STUDENT REPORT?</h3>
					<p class="heading-cntnt">User can search the student  by branch, class, term and year then clicked on view report button to view the student report.</p>
				</div>
				
				<div class="compse-msg">	
					<p class="heading-cntnt"><strong>VIEW:</strong><br>
					It is used to view the report of the student
					</p>
					<p class="heading-cntnt"><strong>MODIFY REPORT:</strong><br>
					This button is used in case if user want to update the report of the student.
					</p>
					<P><strong>User can use save archive button in case if they want to save the progress report of the particular student.</strong></p>
					
				</div>
		
			</div>
	
	
	 </div>	
	<!-- Modify-student---> 
	
	
<!--summary-->
	 <div role="tabpanel" class="tab-pane" id="">	 
		<h1 class="tab-title">Report Archives</h1>
			<p class="main-heading-cntnt"></p>
			<div class="admn-content">
				progress_archive_box.jpg
				<div class="compse-msg">
					<h3 class="para-heading">HOW TO VIEW THE STUDENT REPORT?</h3>
					<p class="heading-cntnt">User can search the student  by branch, class, term and year then clicked on view report button to view the student report.</p>
				</div>	
				<div class="compse-msg">
					<p class="heading-cntnt"><strong> ATTENDANCE BY STUDENT: </strong> It display attendance data for specific student or by student on selecting date in terms of range, branch, class and clicked on view student button user can view the student attendance summary.
						<br>
						In student attendance summary, user can view the student attendance in terms of percentage and also user can view the session i.e how many session of the student attendance is  updated.
						<br>
						User can also view the average total of the student’s attendance.</p>
				</div>
			
			</div>
			
			
	  </div>
	  
	<!--summary-->  
	
	
	<!--summary-->
	 <div role="tabpanel" class="tab-pane" id="report_archive">	 
		<h1 class="tab-title">Report Archive</h1>
			<p class="main-heading-cntnt"></p>
			<div class="admn-content">
				<img src="<?php echo base_url();?>uploads/help_and_knowledge_base/helpdashboard/report_archives.jpg" class="stt">
					<ul class="msg-point">
					<li>In this section, User can search the student  by branch, class, term and year then clicked on view report button to view the student report.</li>
					<li>In report archive, saved record student will be displayed in the list.</li>
					</ul>
					<div class="compse-msg">
						<h3 class="para-heading">Action button provides following  functionality such as:</h3>
						<ul class="msg-point">
							<li><strong>VIEW</strong> - User can view the report archive of the particular student.</li>
							<li>DELETE - User can also delete the report archive of the particular student.</li>
						</ul>
						<p class="heading-cntnt">In total report archive, number of saved archive record is displayed.
<br>
In showing dropdown ,user can select the numbers of records to view  per page.</p>
					</div>		
			</div>
			
			
	  </div>
	  
	<!--summary-->   
	  
    </div>
    </div>
      
      

         

	   </div>
    
	 </div>

    </div>

<style>
.tabs-left, .tabs-right {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-left>li {
  margin-right: -1px;
}
.tabs-right>li {
  margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.sideways {
  margin-top:50px;
  border: none;
  position: relative;
}
.sideways>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.sideways>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.sideways>li.active>a,
.sideways>li.active>a:hover,
.sideways>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.sideways.tabs-left {
  left: -50px;
}
.sideways.tabs-right {
  right: -50px;
}
.sideways.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.sideways.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}

.tab-content.adm-dsh {
    background: #fff;
    width: 100%;
    display: inline-block;
    padding: 10px 20px;
    border: 1px solid #eee;
}
h1.tab-title {
    margin-top: 0 !important;
    margin-left: 0 !important;
    border: none !important;
    font-weight: 900;
    font-size: 32px !important;
}
.adm-dsh p {
    margin-left: 0;
}
p.main-heading-cntnt {
    margin-top: -44px;
    border-bottom: 1px solid #eeeeee;
    padding: 10px 0;
	margin-left:20px;
}
.admn-content {
    margin-top: -2px;
    padding-top: 10px;
    padding-left: 20px;
}
h3.para-heading {
        margin-top: 0px;
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
.admn-content p {
    margin-top: -7px;
}
img.stt {
    width: 100%;
    padding: 30px 0;
	margin-top:-10px;
}
.compse-msg {
    padding: 4px 0 0 0px;
}
h4.txt-frmt {
    font-size: 20px;
    font-weight: 600;
	color: #37b148;
}
ul.msg-point {
    padding-left: 0;
    margin: 0 0 0 14px;
    line-height: 30px;
    font-size: 15px;
}
.compse-msg {
    padding: 20px 0 16px 0px;
    line-height: 28px;
}
h3.para-heading.mgn {
    margin-top: 20px;
}
.list-block {
    height: 634px;
    width: 100% !important;
    background: #fff;
}
p.spacing {
    line-height: 15px;
    margin-top: -17px;
}
</style>