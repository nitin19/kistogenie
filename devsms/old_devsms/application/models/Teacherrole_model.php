<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teacherrole_model extends CI_Model {
	
	 var $tbl = 'teacher_role';
	
	 var $branch = 'branch';
	
	 var $staff = 'staff';
	 
	 var $school='school';
	//var $username	= "username";
	//var $password	= "password";

    function __construct() {
        parent::__construct();
    }
	
	function get_branch($school_id){
	 $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
         return $query->result_array();
	}
 
 /*	**********************	Changes in teacher_role****************************************   */

/* function get_roles($where=NULL, $school_id, $limit=NULL, $offset=NULL, $role_srch){
	 $this->db->limit($limit, $offset);
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', $this->branch.".is_deleted"=>'0', $this->branch.".is_active"=>'1'));
		if($where != NULL){
		$this->db->where($where);
		}
		if($role_srch != NULL){
			$this->db->where("($this->tbl.teacher_role LIKE '%$role_srch%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		 $query = $this->db->get(); 
		 $this->db->last_query(); 
        return $query->result_array();
	}
	
	
function check_teach_role($branch, $role, $school_id){
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"teacher_role"=>$role ));
		  $query = $this->db->get();
		 $this->db->last_query(); 
        return $query->result();   
	}*/
	
		function insert_teach_role($roledata){
		if($this->db->insert($this->tbl, $roledata))
			return $this->db->insert_id();
		  else
			return false;
	}


/*	**********************	Changes in teacher_role****************************************   */

	 /*function totalroles($where=NULL, $school_id, $role_srch){
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0',$this->branch.".is_deleted"=>'0', $this->branch.".is_active"=>'1' ));
		if($where != NULL){
		$this->db->where($where);
		}
		if($role_srch != NULL){
			$this->db->where("($this->tbl.teacher_role LIKE '%$role_srch%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		$query = $this->db->get(); 
		 $this->db->last_query(); 
        return $query->result_array();
	}*/
	
	function get_single_record($id) {
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where('id', $id);
		$query = $this->db->get();
		$this->db->last_query(); 
		return $query->result_array();
	}
	
/*	**********************	Changes in teacher_role****************************************   */
	
	
/*	function check_update_teach_role($branch, $role, $school_id, $id){
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"teacher_role"=>$role));
		$this->db->where('id !=', $id);
		$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();   
	}
*/
	  function update_teacher_role($id,$data){
		$this->db->where('id', $id);
		$this->db->update($this->tbl, $data);
	}

	  function teacher_action($id,$data){
		$this->db->where('id', $id);
		$this->db->update($this->tbl, $data);
	}

/*	**********************	Changes in teacher_role****************************************   */

	/*function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }*/
	
	 function get_delrole($school_id, $id) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where(array("school_id"=>$school_id, "id"=>$id));
          $query = $this->db->get();
		  $this->db->last_query();
          return  $query->row();
      }
	
/*	function check_staffassignrole($teacherrole_id, $school_id, $branch_id){
	 $this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("school_id"=>$school_id,"staff_role_at_school"=>$teacherrole_id,"branch_id"=>$branch_id));
		  $query = $this->db->get();
         $this->db->last_query();  
		 return $query->num_rows(); 
       // return $query->result();
	}*/
	
		function getteacherrole($techerrole_id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
  		$this->db->where('id', $techerrole_id);
        $query = $this->db->get();
        return  $query->row();
	}
	
	
	
/*	**********************	Changes in teacher_role****************************************   */

	function check_teach_role( $role, $school_id){
		 $this->db->select('*');
         $this->db->from($this->tbl);
		 $this->db->where(array("school_id"=>$school_id,"teacher_role"=>$role,"is_deleted"=>'0'));
		 $query = $this->db->get();
		 $this->db->last_query(); 
        return $query->result();   
	}
    function totalroles($where=NULL, $school_id, $role_srch){
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0'));
		if($where != NULL){
		$this->db->where($where);
		}
		if($role_srch != NULL){
			$this->db->where("($this->tbl.teacher_role LIKE '%$role_srch%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		$query = $this->db->get(); 
		 $this->db->last_query(); 
        return $query->result_array();
	}
	
	function getschoolName($school_id) {
        $this->db->select('*');
        $this->db->from($this->school);
        $this->db->where('school_id', $school_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	 function get_roles($where=NULL, $school_id, $limit=NULL, $offset=NULL, $role_srch){
	 $this->db->limit($limit, $offset);
	 $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0'));
		if($where != NULL){
		$this->db->where($where);
		}
		if($role_srch != NULL){
			$this->db->where("($this->tbl.teacher_role LIKE '%$role_srch%')", NULL, FALSE);
		}
		$this->db->order_by($this->tbl.".id", "desc"); 
		 $query = $this->db->get(); 
		 $this->db->last_query(); 
        return $query->result_array();
	}
	
	
	function check_update_teach_role($role, $school_id, $id){
		$this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"teacher_role"=>$role));
		$this->db->where('id !=', $id);
		$query = $this->db->get();
		$this->db->last_query(); 
        return $query->result();   
	}
	
	function check_staffassignrole($teacherrole_id, $school_id){
	 	$this->db->select('*');
        $this->db->from($this->staff);
		$this->db->where(array("school_id"=>$school_id,"staff_role_at_school"=>$teacherrole_id));
		  $query = $this->db->get();
         $this->db->last_query();  
		 return $query->num_rows(); 
       /* return $query->result();*/   
	}
	

}
