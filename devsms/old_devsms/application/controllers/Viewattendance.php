<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Viewattendance extends CI_Controller {



	var $logmode;

	function __construct(){

       

        parent::__construct();

        if( $this->authorize->is_user_logged_in() == false ){

			$this->session->set_flashdata('error', 'Please login first.');

			redirect(base_url());

		   }

		$this->logmode	= $this->session->userdata('log_mode');

        $this->load->model(array('login_model','authorization_model','viewattendance_model'));

		$this->load->database();

        $this->load->library('session');

    }



	public function index()

	{    
		
		$data							= array();	
		$student_id						= isset($_GET['id'])?$_GET['id']:NULL;
		$sdate_search					= isset($_GET['sdate'])?$_GET['sdate']:NULL;
		$edate_search					= isset($_GET['edate'])?$_GET['edate']:NULL;
		$PerPage				   		= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		
		$data['studentinfo'] = $this->viewattendance_model->getStudentinfo($student_id);
		$student_school_branch = $data['studentinfo']->student_school_branch;
		$student_class_group = $data['studentinfo']->student_class_group;
		$data['branchName']  = $this->viewattendance_model->getbranchName($student_school_branch);
		$data['className']   = $this->viewattendance_model->getclassName($student_class_group);
		
		if($sdate_search!=''){
			$startdate	= date('Y-m-d', strtotime($sdate_search));
		}
		if($edate_search!=''){
			$enddate	= date('Y-m-d', strtotime($edate_search));
		}
		  
		$search_condition['school_id']	= $this->session->userdata('user_school_id');
		$search_condition['branch_id']	= $student_school_branch;
		$search_condition['class_id']	= $student_class_group;
		$search_condition['student_id']	= $student_id;
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		$data['title']	      			= "View Attendance";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		if($PerPage!='') {
			$perpage						= $PerPage;
		} else {
			$perpage						= 10;
		}
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'viewattendance/index';

	    $config['total_rows'] 			= $this->viewattendance_model->getRows($search_condition,@$startdate,@$enddate);
	   
		$config['per_page'] 			= $perpage;
 		$config['postfix_string'] = "/?id=$student_id&sdate=$sdate_search&edate=$edate_search"; 
		$this->pagination->initialize($config);
		$odr =   "attendance_id";
		$dirc	= "asc";

		$data_rows  = $this->viewattendance_model->getPagedData($search_condition,$start,$perpage,$odr,$dirc,@$startdate,@$enddate);
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
   
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['studentid']				= $student_id;
		$data['sdate_search']			= $sdate_search;
		$data['edate_search']			= $edate_search;
		$data['PerPage']				= $PerPage;
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');

		$this->load->view('header');
		$this->load->view('attendance/view_attendance', $data);
		$this->load->view('footer');
	}
	  
    function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}



}



