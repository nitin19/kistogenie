<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lessons_model extends CI_Model {
	var $tbl	= "lessons";
	var $classes = "classes";
	var $users = "users";
	var $branch = "branch";
	var $student = "student";
	var $school = "school";
	var $subjects = "subjects";
	
    function __construct() {
        parent::__construct();
    }
	
	
	function getbranches($school_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
    }
	
	 function getRows($where=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			//$this->db->like(array($this->tbl.'.student_fname'=>$word_search));
			 $this->db->where("(lesson_name LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
          $query = $this->db->get();
		  $this->db->last_query(); 
		  return $query->num_rows();
	  }
	  
	   function getPagedData($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL,$school_id,$word_search){
	      $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('school_id', $school_id);
		  if($where != NULL){
			$this->db->where($where);
		  }
		  
		  if($word_search != NULL){
			//$this->db->like(array($this->tbl.'.student_fname'=>$word_search));
			 $this->db->where("(lesson_name LIKE '%$word_search%')", NULL, FALSE);
		  }
		  
		  if($odr){
			$this->db->order_by($odr, $dirc); 		
		   } else {
			$this->db->order_by("lesson_id", "desc"); 	
		   }
		
		     $this->db->limit($limit, $start);
             $query = $this->db->get();
		     $this->db->last_query(); 
             return  $query->result();
	  }
	
	  function get_single_lesson($lessonid) {
          $this->db->select('*');
          $this->db->from($this->tbl);
		  $this->db->where('lesson_id', $lessonid);
          $query = $this->db->get();
          return  $query->result();
       }
	
	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getclassName($class_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where('class_id', $class_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getsubjectName($subject_id) {
        $this->db->select('*');
        $this->db->from($this->subjects);
		$this->db->where('subject_id', $subject_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getclasses($schoolbranch,$school_id) {
        $this->db->select('*');
        $this->db->from($this->classes);
		$this->db->where(array("branch_id"=>$schoolbranch,"school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
      }
	  
	  function getsubjects($school_id,$schoolbranch,$classname) {
        $this->db->select('*');
        $this->db->from($this->subjects);
		$this->db->where(array("school_id="=>$school_id,"branch_id"=>$schoolbranch,"class_id="=>$classname,"is_active="=>'1',"is_deleted="=>'0'));
        $query = $this->db->get();
        return  $query->result();
      }
	
	function check_lesson_name($school_id,$branch_id,$class_id,$subject_id,$lesson_name) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"subject_id"=>$subject_id,"lesson_name"=>$lesson_name));
		  $query = $this->db->get();
		  $this->db->last_query(); 
          return $query->result();   
	}
	
	function insertLesson($lessondata){
		if($this->db->insert($this->tbl, $lessondata))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit;
	   }
	
	function check_edited_lessonname($school_id,$branch_id,$class_id,$subject_id,$lesson_name,$editedLessonid) {
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch_id,"class_id"=>$class_id,"subject_id"=>$subject_id,"lesson_name"=>$lesson_name));
		$this->db->where('lesson_id !=', $editedLessonid);
		  $query = $this->db->get();
		  $this->db->last_query();
          return $query->result();   
	}
	
	 function updateLesson($lessondata,$where) {
		$this->db->where($where);
		if($this->db->update($this->tbl, $lessondata))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	
	function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	  }
	
	 function delete($lessonid){
		$where		= array('lesson_id' => $lessonid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->tbl, $data))
			return true;
		 else
			return false;
	   }
	
	
}



 
