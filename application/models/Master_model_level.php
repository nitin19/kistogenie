<?php
 if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 class Master_model_level extends CI_Model {
	
	     var $tbl = "teacher_level";
	     var $branch = "branch";

         function __construct() {
         parent::__construct();
    }
	
	     function get_branch2($school_id){            //this function selects branch for teacher level page
	     $this->db->select('*');
         $this->db->from($this->branch);
		 $this->db->where(array("school_id"=>$school_id,"is_active"=>'1',"is_deleted"=>'0'));
		 $query = $this->db->get();
		 $last_qry = $this->db->last_query(); 
         $branch = $query->result_array();
         return $branch;
	
    }
	
	    function insert_teach_level($leveldata){          //this function inserts data into select level  for teacher level page
		if($this->db->insert($this->tbl, $leveldata))
		return $this->db->insert_id();
	    else
		return false;		
	}
	
	    function get_teach_level($school_id, $level_search, $limit=NULL, $offset=NULL){  	
		$this->db->limit($limit, $offset);  		                     
	    $this->db->select('*');                                                      //this function is used for listing or to fetch data from database for teacher level page
        $this->db->from($this->tbl);
		$this->db->join($this->branch , 'branch.branch_id = teacher_level.branch_id','INNER' );
		$this->db->where(array('teacher_level.school_id'=>$school_id,'teacher_level.is_deleted'=>'0'));
		
		if($level_search!=''){
		$this->db->where($this->tbl.'.teacher_level',$level_search);
		}
		
		$this->db->order_by($this->tbl.".id", "desc");
		
	    $query = $this->db->get();
		$last_qry = $this->db->last_query(); 
        $teachlevel = $query->result_array();
        return $teachlevel;
    }
	
		function check_teach_level($branch, $level, $school_id){         //this function checks duplicate select level for teacher level page
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->where(array("school_id"=>$school_id,"branch_id"=>$branch,"teacher_level"=>$level));
		$query = $this->db->get();
		$last_qry = $this->db->last_query(); 
        return $query->result();   
	}
    
	    function total_teach_level($level_search, $school_id){            // this function shows all teacher level records i.e used for pagination
	    $this->db->select('*');
        $this->db->from($this->tbl);
		$this->db->join($this->branch, $this->branch.'.branch_id = '.$this->tbl.'.branch_id','INNER' );
		$this->db->where(array($this->tbl.".school_id"=>$school_id, $this->tbl.".is_deleted"=>'0', ));
		
		if($level_search!=''){
		$this->db->where($this->tbl.'.teacher_level',$level_search);
		}
		
		$this->db->order_by($this->tbl.".id", "desc"); 
		$query = $this->db->get(); 
		$rr= $this->db->last_query(); 
        return $query->result_array();
	}
	
	    function edit_level_record($id) {
		$this->db->select('*');
		$this->db->from($this->tbl);
		$this->db->where('id', $id);
		$query = $this->db->get();
		$this->db->last_query(); 
		return $query->result();
	}
	
	
}