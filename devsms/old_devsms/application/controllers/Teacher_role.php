<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher_role extends CI_Controller {

	var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','teacherrole_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }
	
	public function index(){
		
		$id 				= $this->uri->segment(4);	
		
		$search_condition 	= array();
		$school_id			= $this->session->userdata('user_school_id');
		
		$PerPage			= isset($_GET['perpage'])?$_GET['perpage']:NULL;
		$role_srch			= isset($_GET['role_srch'])?$_GET['role_srch']:NULL;
		$branch_search		= isset($_GET['branch'])?$_GET['branch']:NULL;
		
		if($branch_search!=''){
			$search_condition['branch.branch_name']	= $branch_search;
		}
		
		$totrows 			= $this->teacherrole_model->totalroles($search_condition, $school_id,$role_srch);
		
		$data 				= array();
		$data["total_rows"] = count($totrows);
		$data["base_url"] 	= base_url() . "teacher_role/index";
		
		if($PerPage!='') {
			$data["per_page"] 		= $PerPage;
			$perpage				= $data["per_page"];
		} else {
			 $data["per_page"]		= 10;
			 $perpage				= $data["per_page"];
		}
		
		 $data["uri_segment"] 		= 3;
		 $data['postfix_string'] 	= "/?role=$role_srch&perpage=$perpage";
		 
		 $this->pagination->initialize($data);
		 
		 $page 						= $this->uri->segment(3,0) ;
		 $data['last_page']			= $page;
         $data["links"] 			= $this->pagination->create_links();
		 
		 $data['role_search']		= $role_srch;
		  
		//$data['branch'] 			= $this->teacherrole_model->get_branch($school_id);
		$data['detail'] 			= $this->teacherrole_model->get_roles($search_condition, $school_id, $data["per_page"], $page, $role_srch);
		
		$roledata 					= $this->teacherrole_model->get_single_record($id);
		$data['info']				= $roledata;	
	 
		$this->load->view('header');

		$this->load->view('master/teacher_role' , $data);
	
		$this->load->view('footer');
		
				}
	
	public function add_new_teachrole(){
		
		$roledata = array();
		
		$school_id		= $this->session->userdata('user_school_id');					
		$roledata['school_id'] = $school_id;
		
/*	**********************	Changes in teacher_role****************************************   */
		 /*
		 if (trim($this->input->post('branch')) != '') {
		 $roledata['branch_id'] = trim($this->input->post('branch'));
		 }*/
		 if(trim($this->input->post('role'))!= ''){
    	 $roledata['teacher_role'] = trim($this->input->post('role'));
		 }
		  if(trim($this->input->post('description'))!= ''){
		 $roledata['role_description'] = trim($this->input->post('description'));
		  }
		     $roledata['is_active'] = 1 ;
		    $roledata['is_deleted'] = 0 ;
		    $roledata['created_by'] = $this->session->userdata('user_name');
			$roledata['created_date'] = date('Y-m-d H:i:s');
			$roledata['updated_date'] = date('Y-m-d H:i:s');
						
			$role = trim($this->input->post('role'));
			
/*	**********************	Changes in teacher_role****************************************   */

			//$branch = trim($this->input->post('branch'));
						
		//$checkteacher_role = $this->teacherrole_model->check_teach_role($branch, $role, $school_id);
			
				$checkteacher_role = $this->teacherrole_model->check_teach_role($role, $school_id);
		           $countteacherrole = count($checkteacher_role);
				   
				   if($countteacherrole > 0 ) {
					   
					   $this->session->set_flashdata('error', 'Teacher role already exists in this School. Teacher role has not been added.');
				       redirect(base_url()."teacher_role/");exit;
				
				     } else {
					     if($this->teacherrole_model->insert_teach_role($roledata)) {
							$this->session->set_flashdata('success', 'Teacher Role has been added successfully.');
							redirect(base_url()."teacher_role/");exit; 
						 } else {
							$this->session->set_flashdata('error', 'Some problem exists. Teacher Role has not been added.');
							redirect(base_url()."teacher_role/");exit; 
						 }
				   }
	     }
		 
		 public function update_teachrole(){
			 $id = $this->uri->segment(4);
			  $page = $this->uri->segment(3);
			 $school_id		= $this->session->userdata('user_school_id');
			 $data							= array();
			 $id =trim($this->input->post('id'));
			 
			 $data = array(
				//'branch_id' => trim($this->input->post('branch')),
				'teacher_role' => trim($this->input->post('role')),
				'role_description' => trim($this->input->post('description'))
						  ); 
			$role = trim($this->input->post('role'));

/*	**********************	Changes in teacher_role****************************************   */

/*			$branch = trim($this->input->post('branch'));
			$checkteacher_role = $this->teacherrole_model->check_update_teach_role($branch, $role, $school_id, $id);*/
			
			$checkteacher_role = $this->teacherrole_model->check_update_teach_role($role, $school_id, $id);
		     $countteacherrole = count($checkteacher_role);
				   
				   if($countteacherrole > 0 ) {
					    $this->session->set_flashdata('error', 'Teacher role already exists in This Branch. Teacher role has not been Updated.');
				       redirect(base_url()."teacher_role/index/".$page."/".$id);exit;
				
				     } else {
					$updatedata = $this->teacherrole_model->update_teacher_role($id,$data);
					 if($updatedata = 1){
					 $this->session->set_flashdata('success', 'Teacher Role has been updated successfully.');
				redirect(base_url()."teacher_role/index/".$page."/".$id);exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Role has not been updated.');
				redirect(base_url()."teacher_role/index/".$page."/".$id);exit;
			   }
			 }
		 }
		 
		  public function role_action(){
			$id = $this->uri->segment(3);  
			$activity = $this->uri->segment(4);
			$data							= array();
			if($activity == 'active'){ 
			 $data = array(
				'is_active' => '1'
				);
		  } 
		  if($activity == 'inactive'){ 
			 $data = array(
				'is_active' => '0'
				);
		  } 
		  if($activity == 'delete'){ 
			 $data = array(
				'is_deleted' => '1'
				);
		  } 
		  $actiondata = $this->teacherrole_model->teacher_action($id, $data);
		  if($actiondata = 1){	
				$this->session->set_flashdata('success', 'Teacher Role has been updated successfully.');
				redirect(base_url()."teacher_role/");exit;
			}else{
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Role has not been updated.');
				redirect(base_url()."teacher_role/");exit;
			   }
			  
		  }
		  
		    public function checkrole_staff($roleid){
			$id = $this->uri->segment(3); 
			$school_id		= $this->session->userdata('user_school_id'); 
			$data = array(
				'is_deleted' => '1'
				);
		$roledata_rows = $this->teacherrole_model->get_delrole($school_id,$id);
		/*$teacher_role = $roledata_rows->teacher_role;*/
		$teacherrole_id = $roledata_rows->id;	
//		$branch_id = $roledata_rows->branch_id;	
		 
//		  $actiondata = $this->teacherrole_model->check_staffassignrole($teacherrole_id, $school_id, $branch_id);

		  $actiondata = $this->teacherrole_model->check_staffassignrole($teacherrole_id, $school_id);
		  if($actiondata > 0 ) {					   
					   $this->session->set_flashdata('error', 'Teacher Role has Already Assign to staff. You Cannot Delete it');
				       redirect(base_url()."teacher_role/");exit;	
				     }	   else {
						 			$updatedata = $this->teacherrole_model->teacher_action($id, $data);
									if( $updatedata = 1){
					 $this->session->set_flashdata('success', 'Data has been Deleted successfully.');
				redirect(base_url()."teacher_role/");exit;
			} else {
				$this->session->set_flashdata('error', 'Some problem exists. Teacher Role has not been Deleted.');
				redirect(base_url()."teacher_role/");exit;
				}
						 }
		  
		  }
	
}

