<?php



if (!defined('BASEPATH'))



    exit('No direct script access allowed');

class Staffenrolementdashboard_model extends CI_Model {

	var $tbl	= "staff_enrolement";

	var $users = "users";

	var $branch = "branch";
	
	var $school= "school";
	
	var $staff_education = "staff_education";
	
	var $staff_experience = "staff_experience";

    function __construct() {

        parent::__construct();


    }

	function getbranches($school_id) {

        $this->db->select('*');

        $this->db->from($this->branch);

		$this->db->where(array("school_id="=>$school_id,"is_active="=>'1',"is_deleted="=>'0'));

        $query = $this->db->get();

        return  $query->result();

    }

   	function get_single_staff($user_id) {

          $this->db->select('*');

          $this->db->from($this->tbl);

		  $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER'); 

		  $this->db->where(array($this->tbl.'.user_id'=>$user_id));

          $query = $this->db->get();
		//	echo $this->db->last_query();
          return  $query->row();

    }

	function updateUser($userdata,$whereuser){

		$this->db->where($whereuser);
	
		if($this->db->update($this->users, $userdata))

			return true;

		else

			return false;

	 }

	 function updateStaff($staffdata,$wherestaff){

		$this->db->where($wherestaff);

		if($this->db->update($this->tbl, $staffdata))

		return true;

		else

		return false;

	 }

	function get_school_info($school_id) {
		
          $this->db->select('*');
		  
          $this->db->from($this->school);
		  
		  $this->db->where(array($this->school.'.school_id'=>$school_id));
		  
          $query = $this->db->get();
		  
          return  $query->row();
    }
	
 function save_education($edudata){

  if($this->db->insert($this->staff_education, $edudata))
   
   return $this->db->insert_id();
    else
   return false;

   }
   
   	function display_education($staff_enrol_id) {
		
          $this->db->select('*');
		  
          $this->db->from($this->staff_education);
		  
		  $this->db->where(array('staff_enrol_id'=>$staff_enrol_id , 'is_deleted'=>'0'));
		  
		  $this->db->order_by($this->staff_education.".degree", "asc"); 
		  
          $query = $this->db->get();
		  //echo $this->db->last_query();exit();
		  
          return  $query->result();
    }
	
	function delete($id){
		$where		= array('id' => $id);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->staff_education, $data))
			return true;
		 else
			return false;
	  }
	  
	  
  function save_experience($expdata){

  if($this->db->insert($this->staff_experience, $expdata))
   
   return $this->db->insert_id();
    else
   return false;

   }
   
   	function display_experience($staff_enrol_id) {
		
          $this->db->select('*');
		  
          $this->db->from($this->staff_experience);
		  
		  $this->db->where(array('staff_enrol_id'=>$staff_enrol_id , 'is_deleted'=>'0'));
		  
		  $this->db->order_by($this->staff_experience.".exp_startyear", "desc"); 
		  
          $query = $this->db->get();
		  //echo $this->db->last_query();exit();
		  
          return  $query->result();
    }
	
	function delete_exp($id){
		$where		= array('id' => $id);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->staff_experience, $data))
			return true;
		 else
			return false;
	  }

}



