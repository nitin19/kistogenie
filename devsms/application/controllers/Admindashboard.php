<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admindashboard extends CI_Controller {
	
	var $logmode;
	function __construct(){
       
      parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','Admindashboard_model'));
		$this->load->database();
        $this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->library('pagination');
    }

	

	public function index(){
		
		$data = array();
		$user_id		=   $this->session->userdata('user_id');
		//$access	=   $this->session->userdata('access_student_attendance');

//echo $access; exit();
		$search_condition['to']	= $user_id;
		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		$perpage						= 10;
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'admindashboard/index';
	    $config['total_rows'] 			= $this->Admindashboard_model->totalnotification($user_id);
		$config['per_page'] 			= $perpage;
        $config['postfix_string'] = ""; 
		
		$this->pagination->initialize($config);
		
		$odr =   "notification_id";
		$dirc	= "desc";
		
		$data['show']   =	$this->Admindashboard_model->show_notification($search_condition,$start,$perpage,$odr,$dirc);
			 
		//$data['show']   =	$this->Admindashboard_model->show_notification($user_id, $data["per_page"], $page);	 
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;
		$data['total_rows']				= $config['total_rows'];
		$data['data_rows']				= $data['show'];	 
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['page_name']				= $this->uri->segment(1);
		
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		//$data["total_rows"] 			=	$this->Admindashboard_model->totalnotification($user_id);
		//$data["base_url"] = base_url() . "admindashboard/index";
		//$data["uri_segment"] = 3;
	//	$data["per_page"]	= 2;
		//$this->pagination->initialize($data);
		//$page =  $this->uri->segment(3,0) ;
		//$data['last_page']	= $page;
		//$data["links"] = $this->pagination->create_links();
		//$data['show']   =	$this->Admindashboard_model->show_notification($user_id, $data["per_page"], $page);
		
		$branch_id = $this->uri->segment(4);
		$where = 'student_leaving_date BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)';
		$week = 'created_date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)';
		$month = 'created_date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)';
		$school_id		= $this->session->userdata('user_school_id');
		$firstbranch = $this->Admindashboard_model->firstbranch($school_id);
		if($firstbranch=="")
		{
			$firstbranchid=1;
		}
		else{
		$firstbranchid = $firstbranch->branch_id;
		}
		$data['firstbranchstudent'] = $this->Admindashboard_model->firstbranchstudent($school_id, $firstbranchid);
		$data['firstbranchstaff'] = $this->Admindashboard_model->firstbranchstaff($school_id, $firstbranchid);
		$data['firstbranchclasses'] = $this->Admindashboard_model->firstbranchclasses($school_id, $firstbranchid);
		$data['totalstudents'] = $this->Admindashboard_model->totalsudents($school_id);
		$data['totalstaff'] = $this->Admindashboard_model->totalstaff($school_id);
		$data['totalbranch'] = $this->Admindashboard_model->totalbranch($school_id);
		$data['recentstudent'] = $this->Admindashboard_model->recentaddstudent($school_id);
		$data['leavingstudent'] = $this->Admindashboard_model->leavingstudent($school_id, $where);
		$data['recentstaff'] = $this->Admindashboard_model->recentaddstaff($school_id);
		$data['schoolbranches'] = $this->Admindashboard_model->totalbranch_name($school_id);
		$data['branchstudents'] = $this->Admindashboard_model->branchsudents($school_id, $branch_id);
		$data['branchclasses'] = $this->Admindashboard_model->branchclasses($school_id, $branch_id);
		$data['branchstaff'] = $this->Admindashboard_model->branchstaff($school_id, $branch_id);
		$data['recentweekstudent'] = $this->Admindashboard_model->weekstudentrecent($school_id,$week);
		$data['recentmonthkstudent'] = $this->Admindashboard_model->monthstudentrecent($school_id,$month);
		
		
		$this->load->view('header');
		$this->load->view('dashboard/admindashboard', $data);
		$this->load->view('footer');
	}
	
	public function getbranchdata(){
		$branch_id = $_REQUEST['postdata'];
		$school_id		= $this->session->userdata('user_school_id');
		$week = 'users.created_date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)';
		$month = 'users.created_date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)';
		$branchclasses = $this->Admindashboard_model->branchclasses($school_id, $branch_id);
		$recentweekstudent = $this->Admindashboard_model->branchweekstudentrecent($school_id,$week,$branch_id);
		$recentmonthstudent = $this->Admindashboard_model->branchmonthstudentrecent($school_id,$month,$branch_id);
		
		
		$classHTML = '';
		$studentHTML = '<span class="greybar bar imgstud" id="top_arrow"><img src="'.base_url().'assets/images/TOP_arrow1600.png"></span>';
		$bottomarrow ='<img src="'.base_url().'assets/images/right_arrow.png" style="height:50px;">';
		$sr = 0;
		foreach($branchclasses as $classbranch){
			$sr = $sr + 1;
			$string = $classbranch->class_name;
			$string1 = (strlen($string) > 1) ? substr($string,0,3): $string;
			$classHTML.= '<li>'.$string1.'</li>';
			$class_id = $classbranch->class_id;
			$branchstudent = $this->Admindashboard_model->classsudents($school_id, $branch_id, $class_id);
			$title="Students";
			$studentHTML.= '<span class="greybar bar'.$sr.'"><li class="bar nr_'.$sr.'" style="height:'.($branchstudent*2).'%;"><span>'.$title.':'.$branchstudent.'</span></li></span>';
		}
		echo json_encode(array('Class'=>$classHTML,'student'=>$studentHTML, 'month'=>$recentmonthstudent, 'week'=>$recentweekstudent, 'bottom'=>$bottomarrow ));
	}
	
	
	
	public function searchemail(){
		    $term = '';
			$user_id		=   $this->session->userdata('user_id');
			$school			=	$this->Admindashboard_model->get_School_ID($user_id);
			$school_id		=   $school->school_id;
		  if (isset($_REQUEST['term'])){
		     $term = $_REQUEST['term']; 
		    }
		$data = array();
		$emaildata = $this->Admindashboard_model->autocomplete($user_id,$school_id,$term);
		foreach($emaildata as $dataemail){
			       $data[] = $dataemail->email;
					}
		echo json_encode($data);
     }


  	public function insertmessage(){
		$message = array();
		$sendmessege = array();
		$user_id		=   $this->session->userdata('user_id');
		$userdata			=	$this->Admindashboard_model->get_School_ID($user_id);
		$school_id		=   $userdata->school_id;
		//$branch_id		=   $userdata->branch_id;
		$message['from'] = $user_id;
		$message['school_id'] = $school_id;
		//$message['branch_id'] = $branch_id;
		$message['is_active'] = '1';
		$message['is_deleted'] = '0';
		$message['created_date']		=	date('Y-m-d H:i:s');
		
		
		
		if($this->input->post('subject')!=''){
			$message['subject'] = $this->input->post('subject');
		}
		
		if($this->input->post('message')!=''){
			$message['message'] = $this->input->post('message');
		}
		$files 			= 	$_FILES;
        $count			= 	count($_FILES['attachments']['name']);
            for($i=0; $i<$count; $i++)
                {
                $_FILES['attachments']['name']		=	$files['attachments']['name'][$i];
                $_FILES['attachments']['type']		= 	$files['attachments']['type'][$i];
                $_FILES['attachments']['tmp_name']	= 	$files['attachments']['tmp_name'][$i];
                $_FILES['attachments']['error']		= 	$files['attachments']['error'][$i];
                $_FILES['attachments']['size']		= 	$files['attachments']['size'][$i];
                $config['upload_path'] 			= 	'./uploads/notification';
                $config['allowed_types'] 		= 	'gif|jpg|png|docx|jpeg|xls|pdf|txt';
	  	$config['max_size']             	= 	500;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload("attachments");
                $fileName = $_FILES['attachments']['name'];
                $images[] = $fileName;
                }
         $fileName = implode(',',$images);
		 $message['attachments'] = $fileName;
		 
		 if($this->input->post('toemail')!=''){
			$toemail = $this->input->post('toemail');
		
		 if (in_array("All", $toemail)){
			 $school_users		=	$this->Admindashboard_model->getall_id($user_id,$school_id);
		 
		 foreach($school_users as $users){
				 $to = $users->id;
				 $message['to'] = $to;
					$alluser = $this->Admindashboard_model->insert_notification($message);
				 }
		 
		if($alluser){
			   $this->session->set_flashdata('success', 'Message has been successfully send.');
			   redirect(base_url()."admindashboard/"); exit;
				}
		 }
		
			
			if (in_array("Staff", $toemail)){
			 $school_staff		=	$this->Admindashboard_model->getall_staff($user_id,$school_id);
		 
		 foreach($school_staff as $staff){
				 $to = $staff->id;
				 $message['to'] = $to;
				$sendmessege1 = 	$this->Admindashboard_model->insert_notification($message);
				 }
				 $sendmessege = $sendmessege1;
		}	 
		
		if (in_array("Student", $toemail)){
			 $school_student		=	$this->Admindashboard_model->getall_student($school_id);
		 
		 foreach($school_student as $student){
				 $to = $student->id;
				 $message['to'] = $to;
				$sendmessege1 = 	$this->Admindashboard_model->insert_notification($message);
				 }
				 $sendmessege = $sendmessege1;
		}	
		 }
		 
		if($this->input->post('touser')!=''){
			$touser = $this->input->post('touser');
		
		$emails =  substr(trim($touser), 0, -1); 
        $emailuser	= $this->Admindashboard_model->getuser_id($emails);
	
		if($emailuser){
			foreach($emailuser as $user){
			  $to = $user->id;
			 $message['to'] =  $to;
			 $sendmessege1 = 	$this->Admindashboard_model->insert_notification($message);
			}
			$sendmessege = $sendmessege1;
		}}
		
		if($sendmessege){
			   $this->session->set_flashdata('success', 'Message has been successfully send.');
			   redirect(base_url()."admindashboard/"); exit;
				}
				else{
			   $this->session->set_flashdata('error', 'Some problem exists. Messsage can not be send.');
				redirect(base_url()."admindashboard/");exit;
		      }
		
	 }
	 
	 
	 public function download_attachments($fileName = NULL) { 
	
       if ($fileName) {
        $file		=	realpath ( FCPATH.'uploads/notification/'.$fileName );
        if (file_exists ( $file )) {
         $data		=	file_get_contents ( $file );
         $this->load->helper('download');
         force_download ( $fileName, $data );
		 echo"doen";
        } else {
		 $this->session->set_flashdata('error', 'Requested file does not exists.');
         redirect(base_url()."admindashboard/");exit;
          }
        }
      }
	  
	  public function deletemsg($notificationid){
				if($this->Admindashboard_model->delete($notificationid))
					{
					$this->session->set_flashdata('success', 'Message has been deleted successfully.');
					redirect(base_url()."admindashboard/");exit;
					}else{
					$this->session->set_flashdata('error', 'Some problem exists. Try again.');
					redirect(base_url()."admindashboard/");exit;
					}
				}
	 
	 function createPostURL($get){
		$start	= $this->uri->segment(4,0);
		$odrby	= $this->uri->segment(5,0);
		$dirc	= $this->uri->segment(6,"asc");
		$postURL	= "$start/$odrby/$dirc/?";
		foreach($get as $key=>$val){
			$postURL	.="$key=$val&";
		}
		return  $postURL;exit;
	}


}

