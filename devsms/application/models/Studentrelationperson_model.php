<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Studentrelationperson_model extends CI_Model {
	var $tbl	= "fee_band";
	var $classes = "classes";
	var $users = "users";
	var $branch = "branch";
	var $student = "student";
	var $school = "school";
	var $student_relation = "student_relation";
	
    function __construct() {
        parent::__construct();
    }
	
	function  student_info($student_id){
        $this->db->select('*');
        $this->db->from($this->student);
		$this->db->where('user_id', $student_id);
        $query = $this->db->get();
		//echo   $this->db->last_query();exit();
        return  $query->row();
	   }

	  function insert_student_relation_info($relationdata){   
		$result=$this->db->insert($this->student_relation,$relationdata);	
	 	 return $result;
		}
	function show_relation($student_id)
	{
		$this->db->select('*');
		$this->db->from($this->student_relation);
		$this->db->where(array("student_id"=>$student_id,"is_deleted"=>0));
		$query=$this->db->get();
		return $query->result();	
	}
	
	function get_single_relation($relationid) {
          $this->db->select('*');
          $this->db->from($this->student_relation);
		  $this->db->where('student_relation_id', $relationid);
          $query = $this->db->get();
		  //echo $this->db->last_query();exit();
          return  $query->result();
    }
	 function updateRelation($relationdata,$where) {
		$this->db->where($where);
		if($this->db->update($this->student_relation, $relationdata))
			return true;
		else
			return false;
		echo $this->db->last_query();exit;
	 }
	 
	 	   
	  function delete($relationid){
		$where		= array('student_relation_id' => $relationid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->student_relation, $data))
			return true;
		 else
			return false;
	   }
	   
	   	 
	 function update($data,$where){
		$this->db->where($where);
		if($this->db->update($this->student_relation, $data))
			return true;
		else
			return false;
		//echo $this->db->last_query();exit;
	 }
	 
	 	function check_studentrelation($relationname, $contact, $student_id){
	 $this->db->select('*');
        $this->db->from($this->student_relation);
		$this->db->where(array("student_id"=>$student_id,"student_relation_fullname"=>$relationname,"student_relation_mobile_number"=>$contact));
		  $query = $this->db->get();
		 //$this->db->last_query(); 
        return $query->num_rows();   
	}
	   
}



 
