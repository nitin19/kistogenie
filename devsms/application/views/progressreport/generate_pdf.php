 <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Olive Tree</title>
  </head>
  <body>
    <div style="padding:20px 0px;">
     <?php
        $this->load->model(array('login_model','authorization_model','schooltermreport_model'));
        if(count(array_filter($results)) > 0 ) {

            foreach ($results as $result) {
                    $student_id = $result->student_id;
                    $studentInfo = $this->schooltermreport_model->getstudentinfo($student_id);
                    $branchInfo = $this->schooltermreport_model->getBranchinfo($result->branch_id);
                    $classInfo = $this->schooltermreport_model->getClassinfo($result->class_id);
                    
      ?>
      <div class="page-wrappers">
   <div style="float: left;width: 100%;">
    <div style="float:left;width:30%;">
      <a href="#"><img src="<?php echo base_url();?>assets/images/printlogo.png"></a>
    </div>
    <div style="float:left;width:65%;">
      <h1 style=" color: #252525;font-family: Proxima Nova;font-weight: bold;font-size: 22px;text-transform: uppercase;font-weight: bold; ">Annual Student Report <?php echo date(Y);?></h1>
    </div>
  </div>
  <div style="float: left;width:100%;border:1px solid #8d8d8d;">
    <div style="float: left;width:33%;">
      <h1 style="font-family: Proxima Nova;font-size: 13px;text-transform: uppercase;font-weight:normal; color:#5eb751; padding: 0px 5px;">Student Name : <span style="color: #000;font-family: open sans;font-size: 12px;"><?php echo $studentInfo->student_fname.' '.$studentInfo->student_lname;?></span></h1>
    </div>
        <div style="float: left;width:33%;border-left: 1px solid #8d8d8d;">
      <h1 style="font-family: Proxima Nova;font-size: 13px;text-transform: uppercase;font-weight:normal; color:#5eb751; padding: 0px 5px;">Class: <span style="color: #000;font-family: open sans;font-size: 12px; "><?php echo $classInfo->class_name;?></span></h1>
    </div>
        <div style="float: left;width:33%;border-left: 1px solid #8d8d8d;">
      <h1 style="font-family: Proxima Nova;font-size: 13px;text-transform: uppercase;font-weight:normal; color:#5eb751; padding: 0px 5px;">Branch: <span style="color: #000;font-family: open sans;font-size: 12px;"><?php echo $branchInfo->branch_name;?></span></h1>
    </div>
  </div>        
                    <?php    
                    $termReport = $this->schooltermreport_model->get_term_reports($student_id,$where);

                if( count($termReport) > 0 ) { 

                     foreach($termReport as $treport) { 
                    /* echo"<pre>";
                      print_r($treport);
                      echo"</pre>";*/
                      $progress_report_id       =     $treport->progress_report_id;  
                      $student_id               =     $treport->student_id;
                      $school_id                =     $treport->school_id;
                      $branch_id                =     $treport->branch_id;
                      $class_id                 =     $treport->class_id;
                      $term                     =     $treport->term;
                      $subject_id               =     $treport->subject_id;
                      $staff_id                 =     $treport->staff_id;
                      if($staff_id !='0'){
                      $staff_name               =     $this->schooltermreport_model->get_staff_name($staff_id);
                    }
                      $subject_name             =     $this->schooltermreport_model->get_subject_name($subject_id);
                      $subjectname              =     $subject_name->subject_name;
                      $effort                   =     $treport->effort;
                      $behaviour                =     $treport->behaviour;
                      $home_work                =     $treport->home_work;
                      $proof_reading            =     $treport->proof_reading;
                      $start_level              =     $treport->start_level;
                      $current_level            =     $treport->current_level;
                      if($current_level !=''){
                      $current_level_name       =     $this->schooltermreport_model->get_current_level($current_level);
                    }
                      $target_level             =     $treport->target_level;
                      $comment                  =     $treport->comment;
                      $account_of_achievement   =     $treport->account_of_achievement;
                      $area_development         =     $treport->area_development;
                      $topic_covered            =     $treport->topic_covered;
                      $termInfo                 =     $this->schooltermreport_model->getTerminfo($treport->term);
                     
                     ?>

    <div style="float: left;width: 100%;border-right:1px solid #8d8d8d;border-left:1px solid #8d8d8d;border-bottom:1px solid #8d8d8d;margin-top: 10px;">
      <?php if($subjectname=='Memorisation'){
              $color="#F63293";

      }elseif($subjectname=='Reading'){

            $color="#4FC9EE";

      }elseif($subjectname=='Islamic Studies' || $subjectname=='Islamic studies' ){

            $color="#52B647";

      }else{
            $color="#FFA42D";
      } 

      ?>
    <div style="float:left;width:100%;background: <?php echo $color; ?>;border-top:1px solid #8d8d8d;border-bottom: 1px solid #8d8d8d;">
    <div style="float:left;width:33%;border-right: 1px solid #8d8d8d;">
      <h2 style="margin: 0px;  color: #252525;font-family: Proxima Nova;font-size: 14px;margin: 0;padding: 15px 0 15px;text-align: center;text-transform: uppercase;"><?php echo $subjectname;?> </h2>
    </div>
     <div style="float:left;width:33%;border-right: 1px solid #8d8d8d;">
      <h2 style="margin: 0px; color: #161616;font-family: Proxima Nova;font-size: 14px;margin: 0;padding: 15px 0 15px;text-align: center;text-transform: uppercase;">Account of Achievement</h2>
    </div>
     <div style="float:left;width:33%;">
      <h2 style="margin: 0px;  color: #161616;font-family: Proxima Nova;font-size: 14px;margin: 0;padding: 15px 0 15px;text-align: center;text-transform: uppercase;">Area of Development</h2>
    </div>
  </div>

    <div style="float:left;width:100%;border-bottom: 1px solid #8d8d8d;" >
    <div style="float:left;width:33%;">
      <h2 style=" align-items: center;color: #5eb751;display: flex;display: -webkit-flex;font-family: Proxima Nova;font-size: 13px; justify-content: center;margin: 0;padding-top:5px;text-align: center;text-transform: uppercase;height: 185px;">Teacher Comments</h2>
    </div>
     <div style="float:left;width:33%;border-left: 1px solid #8d8d8d;">
      <p style="font-family: open sans;font-size: 12px;line-height: 14px;padding: 0 15px;height: 185px;text-align: justify;word-wrap: break-word;"><?php echo $account_of_achievement;?></p>
    </div>
     <div style="float:left;width:33%;border-left: 1px solid #8d8d8d;">
      <p style="font-family: open sans;font-size: 12px;line-height: 12px;padding: 0 15px;height: 185px;text-align: justify;word-wrap: break-word;"><?php echo nl2br($area_development);?></p>
    </div>
  </div>

  <?php if($subjectname == 'Memorisation' || $subjectname == 'Reading' ){ ?>

    <div style="float:left;width:100%;border-bottom: 1px solid #8d8d8d;">

    <div style="float:left;width:33%;height:55px;align-items: center;justify-content:center;display: flex;align-items: center;">
      <h2 style="color: #5eb751;font-family: Proxima Nova;font-size: 11px; margin: 0px;padding: 0 20px;text-align: center;text-transform: uppercase;">Start Level:- <span style="color: #000;font-family: open sans;"><?php echo $start_level;?></span></h2>
    </div>
     <div style="float:left;width:33%;border-left: 1px solid #8d8d8d;height:55px;align-items: center;justify-content:center;display: flex;">
      <h2 style="font-family: Proxima Nova;font-size: 11px;line-height: 15px;padding: 0 20px;color: #5eb751;text-transform: uppercase;margin:0px;">Current Level:- <span style="color: #000;font-family: open sans;"><?php $c_level= str_replace("LEVEL","","$current_level_name->level_name"); echo $c_level?></span></h2>
    </div>
     <div style="float:left;width:33%;border-left: 1px solid #8d8d8d;height:55px;align-items: center;justify-content:center;display: flex;">
      <h2 style="font-family: Proxima Nova;font-size: 11px;line-height: 15px;padding: 0 20px;text-transform: uppercase;margin:0px;color:#5eb751;">Target Level:-<span style="color: #000;font-family: open sans;"> <?php echo $target_level;?></span></h2>
    </div>
  </div>
<?php } ?>

  <?php if($subjectname == 'Islamic Studies' || $subjectname == 'Islamic studies'){ ?>
      <div style="float:left;width:100%;border-bottom: 1px solid #8d8d8d;">
    <div style="float:left;width:33%;">
      <h2 style=" align-items: center;color: #5eb751;display: flex;display: -webkit-flex;font-family: Proxima Nova;font-weight: bold;font-size: 14px; justify-content: center;margin: 0;padding-top:5px;text-align: center;text-transform: uppercase;height: 90px;">Topic Covered</h2>
    </div>
    <div style="float:left;width:66%;border-left: 1px solid #8d8d8d;">
      <p style="font-family: open sans;font-size: 12px;line-height: 15px;padding: 0 15px;height: 90px;"><?php echo $topic_covered;?></p>
    </div>
     
  </div>

<?php } ?>

      <div style="float:left;width:100%;">
    <div style="float:left;width:33%;height:60px;align-items: center;justify-content:center;display: flex;">
      <h2 style="color: #5eb751;font-family: Proxima Nova;font-size: 12px;margin: 0;padding-top:5px;text-align: center;text-transform: uppercase;font-weight: bold;">Behaviour: <span style="color: #000 ;font-family: open sans;"> <?php echo $behaviour;?></span></h2>
    </div>
     <div style="float:left;width:33%;border-left: 1px solid #8d8d8d;height:60px;align-items: center;justify-content:center;display: flex;">
      <h2 style="font-family: Proxima Nova;font-size: 12px;line-height: 15px;padding: 0 20px;color: #5eb751;text-transform: uppercase;margin: 0px;">Home Work: <span style="color: #000;font-family: open sans;"><?php echo $home_work;?></span>
</h2>
    </div>
     <div style="float:left;width:33%;border-left: 1px solid #8d8d8d;height:60px;align-items: center;justify-content:center;display: flex;">
      <h2 style="font-family: Proxima Nova;font-size: 12px;line-height: 15px;padding: 0 20px;color: #5eb751;text-transform: uppercase;margin: 0px;">Progress: <span style="color: #000;font-family: open sans;"><?php echo $effort;?></span>
</h2>
    </div>
  </div>

  </div>

   <div style="float: left;width:100%; margin-top: 3px;">
    <div style="float:left;width:50%;">
      <h5 style="color: #5eb751;font-family: Proxima Nova;text-transform: uppercase; padding: 0px 20px;">Subject Teacher: <span style="color: #333333;font-family: open sans;"><?php if ($staff_name->staff_fname !='' ){echo $staff_name->staff_fname.' '.$staff_name->staff_lname; }else{echo "___________";} ?></span></h3>
    </div>
    <!--<div style="float:left;width:50%;text-align: right;">
      <h5 style="color: #333333;font-family: Proxima Nova;text-transform: uppercase; padding: 0px 20px;">Parents Sign</h3>
    </div>-->
  </div>
</div>
    <?php

                     }
                
                } 
echo '<div class="test"></div>';
            }    
      }
        ?>

 </div>

    <style type="text/css">

    @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i');

      @font-face {
    font-family: Proxima Nova Bold;
    src: url("./assets/fonts/Proxima Nova Regular.ttf");
}
/*@font-face {
font-family:"OpenSans Regular";
src:url("../fonts/OpenSans-Regular.ttf");
}
*/.test{
  float: left;
  height:220px;
}
    </style>
  </body>
</html>
