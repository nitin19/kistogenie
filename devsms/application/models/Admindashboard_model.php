<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admindashboard_model extends CI_Model {
	var $tbl	= "staff";
	var $users = "users";
	var $branch = "branch";
	var $classes = "classes";
	var $student = "student";
	var $subjects = "subjects";
	var $teacher_role = "teacher_role";
	var $teacher_level = "teacher_level";
	var $teacher_type = "teacher_type";
	var $notification	= "notification";
	
    function __construct() {
        parent::__construct();
    }
	
	function recentaddstudent($school_id) {
	 $this->db->select('*');
        $this->db->from($this->student);
        $this->db->join('users', 'users.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id,"users.is_deleted="=>'0'));
		$this->db->order_by($this->student.".student_id", "desc"); 
		$this->db->limit('5');
		 $query = $this->db->get();
		 $this->db->last_query();
        return $query->result();
}

		function recentaddstaff($school_id) {
	 $this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
		$this->db->where(array($this->tbl.'.school_id'=>$school_id,"users.is_deleted="=>'0'));
		$this->db->order_by($this->tbl.".staff_id", "desc"); 
		$this->db->limit('5');
		 $query = $this->db->get();
	 $this->db->last_query();
        return $query->result();
}

	function leavingstudent($school_id, $where) {
	 $this->db->select('*');
        $this->db->from($this->student);
        $this->db->join('users', 'users.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id,"users.is_deleted="=>'0'));
		if($where!=''){
		$this->db->where($where, null, false);
		}
		$this->db->order_by($this->student.".student_id", "desc"); 
		$this->db->limit('5');
		 $query = $this->db->get();
		 $this->db->last_query(); 
        return $query->result();
}

	function getbranchName($branch_id) {
        $this->db->select('*');
        $this->db->from($this->branch);
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return  $query->row();
    }
	
	function getclassName($class) {
        $this->db->select('*');
        $this->db->from($this->classes);
        $this->db->where_in('class_id', $class);
        $query = $this->db->get();
       return  $query->row();
    }
	
	function totalsudents($school_id) {
        $this->db->from($this->student);
        $this->db->join('users', 'users.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id, "users.is_deleted="=>'0'));
        $query = $this->db->get();
		 $this->db->last_query(); 
       return $query->num_rows();
    }
	
	function totalstaff($school_id) {
        $this->db->from($this->tbl);
        $this->db->join('users', 'users.id = '.$this->tbl.'.user_id','INNER' );
		$this->db->where(array($this->tbl.'.school_id'=>$school_id, "users.is_deleted="=>'0'));
        $query = $this->db->get();
		$this->db->last_query(); 
       return $query->num_rows();
    }
	
	function totalbranch($school_id) {
        $this->db->from($this->branch);
		$this->db->where(array('school_id'=>$school_id, "is_deleted="=>'0',"is_active="=>'1'));
        $query = $this->db->get();
		 $this->db->last_query(); 
       return $query->result();
    }
	
	function totalbranch_name($school_id) {
        $this->db->from($this->branch);
		$this->db->where(array('school_id'=>$school_id, "is_deleted="=>'0',"is_active="=>'1'));
        $query = $this->db->get();
       return $query->result();
    }
	
	function branchsudents($school_id, $branch_id) {
        $this->db->from($this->student);
        $this->db->join($this->users, $this->users.'.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id, "users.is_deleted="=>'0', $this->student.'.student_school_branch'=>$branch_id));
        $query = $this->db->get();
		/*echo $this->db->last_query(); exit();*/
       return $query->result();
    }
	
	function branchclasses($school_id, $branch_id) {
        $this->db->from($this->classes);
		$this->db->where(array('school_id'=>$school_id, "is_deleted="=>'0', 'branch_id'=>$branch_id));
        $query = $this->db->get();
		/*echo $this->db->last_query(); exit();*/
       return $query->result();
    }
	
		function branchstaff($school_id, $branch_id) {
        $this->db->from($this->tbl);
        $this->db->join($this->users, $this->users.'.id = '.$this->tbl.'.user_id','INNER' );
		$this->db->where(array($this->tbl.'.school_id'=>$school_id, $this->users.".is_deleted="=>'0', $this->tbl.'.branch_id'=>$branch_id));
        $query = $this->db->get();
		/*echo $this->db->last_query(); exit();*/
       return $query->result();
    }
	
	function classsudents($school_id, $branch_id, $class_id) {
        $this->db->from($this->student);
        $this->db->join($this->users, $this->users.'.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id, $this->users.".is_deleted="=>'0', $this->student.'.student_school_branch'=>$branch_id, $this->student.'.student_class_group'=>$class_id));
        $query = $this->db->get();
		/*echo $this->db->last_query(); exit();*/
       return $query->num_rows();
    }
	
	function weekstudentrecent($school_id,$week) {
        $this->db->from($this->users);
		$this->db->where(array('school_id'=>$school_id, "is_deleted="=>'0', "user_type="=>'student'));
		 $this->db->where($week);
        $query = $this->db->get();
	 $this->db->last_query();  
       return $query->num_rows();
    }
	
	function monthstudentrecent($school_id,$month) {
        $this->db->from($this->users);
		$this->db->where(array('school_id'=>$school_id, "is_deleted="=>'0', "user_type="=>'student'));
		 $this->db->where($month);
        $query = $this->db->get();
	 $this->db->last_query();  
       return $query->num_rows();
    }
	
	function branchweekstudentrecent($school_id,$week,$branch_id) {
        $this->db->from($this->student);
        $this->db->join($this->users, $this->users.'.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id, $this->users.".is_deleted="=>'0', $this->student.'.student_school_branch'=>$branch_id));
		$this->db->where($week);
        $query = $this->db->get();
		 $this->db->last_query(); 
       return $query->num_rows();
    }
	
	function branchmonthstudentrecent($school_id,$month,$branch_id) {
        $this->db->from($this->student);
        $this->db->join($this->users, $this->users.'.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id, $this->users.".is_deleted="=>'0', $this->student.'.student_school_branch'=>$branch_id));
		$this->db->where($month);
        $query = $this->db->get();
		 $this->db->last_query(); 
       return $query->num_rows();
    }
	
	function firstbranch($school_id) {
		 $this->db->select('*');
        $this->db->from($this->branch);
		$this->db->where(array('school_id'=>$school_id, "is_deleted"=>'0', "is_active"=>'1'));
		$this->db->limit(1);
        $query = $this->db->get();
	    $this->db->last_query();  
       return $query->row();
    }
	
	function firstbranchstaff($school_id, $firstbranchid) {
        $this->db->from($this->tbl);
        $this->db->join($this->users, $this->users.'.id = '.$this->tbl.'.user_id','INNER' );
		$this->db->where(array($this->tbl.'.school_id'=>$school_id, $this->users.".is_deleted="=>'0', $this->tbl.'.branch_id'=>$firstbranchid));
        $query = $this->db->get();
		/*echo $this->db->last_query(); exit();*/
       return $query->num_rows();
    }
	
	function firstbranchstudent($school_id, $firstbranchid) {
        $this->db->from($this->student);
        $this->db->join($this->users, $this->users.'.id = '.$this->student.'.user_id','INNER' );
		$this->db->where(array($this->student.'.school_id'=>$school_id, "users.is_deleted="=>'0', $this->student.'.student_school_branch'=>$firstbranchid));
        $query = $this->db->get();
		/*echo $this->db->last_query(); exit();*/
       return $query->num_rows();
    }
	
	function firstbranchclasses($school_id, $firstbranchid) {
        $this->db->from($this->classes);
		$this->db->where(array('school_id'=>$school_id, "is_deleted="=>'0', 'branch_id'=>$firstbranchid));
        $query = $this->db->get();
		/*echo $this->db->last_query(); exit();*/
       return $query->num_rows();
    }

  
  function get_School_ID($user_id){
		$this->db->select('*');
		$this->db->from($this->users);
		$this->db->where('id', $user_id);
		$query = $this->db->get();
		return   $query->row();
		}
		
		
  public function autocomplete($user_id,$school_id,$term){
		 $this->db->select('*');
         $this->db->from($this->users);
		 $this->db->where(array('id !='=>$user_id,'school_id'=>$school_id,'is_deleted'=>'0','is_active'=>'1'));
		 $this->db->like('email', $term);
		$this->db->last_query();
         $query = $this->db->get();
		 return $query->result();
	 }
	 
	 function getall_id($user_id,$school_id){
		$this->db->select('*');
        $this->db->from($this->users);
        $this->db->where(array('id !='=>$user_id,'school_id'=>$school_id,'is_deleted='=>'0','is_active'=>'1'));
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
	    return  $query->result();
	} 
	
	function insert_notification($notification){
		if($this->db->insert($this->notification, $notification))
			return $this->db->insert_id();
		  else
			return false;
		//echo $this->db->last_query();exit();
	 }
	 
	 function getall_staff($user_id,$school_id){
		$this->db->select('*');
        $this->db->from($this->users);
        $this->db->where(array('id !='=>$user_id,'school_id'=>$school_id,'user_type='=>'teacher','is_deleted='=>'0','is_active'=>'1'));
        $query = $this->db->get();
		//echo $this->db->last_query();exit();
	   return  $query->result();
	}
	
	function getall_student($school_id){
		$this->db->select('*');
        $this->db->from($this->users);
        $this->db->where(array('school_id'=>$school_id,'user_type='=>'student','is_deleted='=>'0','is_active'=>'1'));
        $query = $this->db->get();
		//echo $this->db->last_query();exit();
	   return  $query->result();
	}
	
	function getuser_id($emails) {
		$emailsarray = explode(',',$emails);
		$array = array_filter(array_map('trim', $emailsarray));
        $this->db->select('*');
        $this->db->from($this->users);
        $this->db->where_in('email', $array);
        $query = $this->db->get();
	    $this->db->last_query(); 
	    return  $query->result();
    }
	
	function totalnotification($user_id){
		$this->db->select('*');
		$this->db->from($this->notification);
		$this->db->where(array('to'=>$user_id,'is_active'=>'1','is_deleted='=>'0'));
		$query=$this->db->get();
		return  $query->num_rows();
		}

   	function show_notification($where=NULL,$start,$limit,$odr=NULL,$dirc=NULL){
		$this->db->select('*');
		$this->db->from($this->notification);
		$this->db->where($where);
		$this->db->order_by("notification_id", "desc"); 
		$this->db->limit($limit, $start);	
		$query=$this->db->get();
		return  $query->result();
		}
		
		
   function getsenderName($sender_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('id'=>$sender_id,'is_deleted='=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	function getsenderimage($sender_id) {
        $this->db->select('*');
        $this->db->from($this->users);
		$this->db->where(array('id'=>$sender_id,'is_deleted='=>'0'));
        $query = $this->db->get();
        return  $query->row();
    }
	
	
	function delete($notificationid){
		$where		= array('notification_id' => $notificationid);
		$data		= array('is_deleted'=>'1');
		$this->db->where($where);
		if($this->db->update($this->notification, $data))
			return true;
		 else
			return false;
	   }
	
}
