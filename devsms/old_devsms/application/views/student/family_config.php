<div class="editprofile-content">

    <div class="col-sm-12 profilemenus nopadding">

    <div class="col-sm-9 col-xs-8 nopadding menubaritems">

         <ul>

		<li><a href="#">Home</a></li>

        <li><a href="#">Students</a></li>

        <li class="edit"><a href="#">Family Config</a></li>        

        </ul>

        </div>

        <div class="col-sm-3 col-xs-4 actionbtn nopadding">

        <input type="button" class="activequickaction" value="Quick actions">

        </div>

        </div>



        

        <div class="studentspage">

        	<div class="col-sm-12 profile-bg">

        	<div class="family-config">

            <p>Families should be created for all students. Each time a student is added to the system, you should first check if he or she is a member of an existing family. If they are part of a family, you can select the relevant family when adding them to the system on the 'Add Student' screen. If the student is not part of an existing family, create a new family for the student by selecting the 'Add Family' button.

It is extremely important that the information in this section is correct and kept up to date in order to achieve the best possible results in the fees section. This list will undoubtedly grow in size and become extremely long, but this should not pose a problem. Please note that family names DO NOT have to be unique, eg. there can be many 'Ahmed' families.</p>

<div class="addbtn">

<a href="#">Add Family</a>

</div>

            </div>

        </div>

        <div class="col-sm-12 profile-bg filterbox">

        <div class="filterdiv">

                <form>

                <div class="col-sm-3 col-xs-5 selectfilter halfwidsec">

                <span>Show:</span>

        <select class="form-control">

        <option>Select Name A-Z</option>

  <option>1</option>

  <option>2</option>

  <option>3</option>

  <option>4</option>

  <option>5</option>

</select>



        </div>
         <div class="col-sm-3 col-xs-5 searching termsearch halfwidsec"></div>
        <div class="col-sm-3 col-xs-5 searching termsearch halfwidsec">

        <div class="form-group ">

     <input type="text" placeholder="Search" class="form-control searchbox">

     <input type="submit" class="searchbtn" value="">

     </div>

        </div>

        <div class="col-sm-3 rightspace selectoption viewreport resviewbtn">

<div class="form-group">

    <input type="button" class="btn btn-danger bttn" value="Find Family">

    <a href="#" class="bttn">Clean the filter</a>

  </div>

</div>

        </form>

        </div>

    </div>

    <div class="col-sm-12 tablediv nopadding">

    <h1>Family Config<span>855</span></h1>

    <div class="tablewrapper">

<table class="table-bordered table-striped">

			  <thead>

				  <tr class="headings">

					  <th class="column1">S.No.</th>

					  <th class="column8">Family Name</th>

					  <th class="column5">Children in the family</th>

					  <th class="column5">Family Composition</th>

					  <th class="column1"></th>

				  </tr>

			  </thead>

				<tbody>

					<tr class="familydata familyconfig">

						<td class="column1 numeric">01</td>

						<td class="column8">Oak-Aleesha Yazmeen</td>

						<td class="column5">2</td>

                        <td class="column5"><i class="fa fa-eye"></i><span>View Family</span></td>

                        <td class="column1"><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    <tr class="familydata familyconfig">

						<td class="column1 numeric">01</td>

						<td class="column8">Oak-Aleesha Yazmeen</td>

						<td class="column5">2</td>

                        <td class="column5"><i class="fa fa-eye"></i><span>View Family</span></td>

                        <td class="column1"><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    <tr class="familydata familyconfig">

						<td class="column1 numeric">01</td>

						<td class="column8">Oak-Aleesha Yazmeen</td>

						<td class="column5">2</td>

                        <td class="column5"><i class="fa fa-eye"></i><span>View Family</span></td>

                        <td class="column1"><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>  

                    <tr class="familydata familyconfig">

						<td class="column1 numeric">01</td>

						<td class="column8">Oak-Aleesha Yazmeen</td>

						<td class="column5">2</td>

                        <td class="column5"><i class="fa fa-eye"></i><span>View Family</span></td>

                        <td class="column1"><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    <tr class="familydata familyconfig">

						<td class="column1 numeric">01</td>

						<td class="column8">Oak-Aleesha Yazmeen</td>

						<td class="column5">2</td>

                        <td class="column5"><i class="fa fa-eye"></i><span>View Family</span></td>

                        <td class="column1"><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    <tr class="familydata familyconfig">

						<td class="column1 numeric">01</td>

						<td class="column8">Oak-Aleesha Yazmeen</td>

						<td class="column5">2</td>

                        <td class="column5"><i class="fa fa-eye"></i><span>View Family</span></td>

                        <td class="column1"><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    <tr class="familydata familyconfig">

						<td class="column1 numeric">01</td>

						<td class="column8">Oak-Aleesha Yazmeen</td>

						<td class="column5">2</td>

                        <td class="column5"><i class="fa fa-eye"></i><span>View Family</span></td>

                        <td class="column1"><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

                    <tr class="familydata familyconfig">

						<td class="column1 numeric">01</td>

						<td class="column8">Oak-Aleesha Yazmeen</td>

						<td class="column5">2</td>

                        <td class="column5"><i class="fa fa-eye"></i><span>View Family</span></td>

                        <td class="column1"><i class="fa fa-trash-o"></i><i class="fa fa-cog"></i></td>

                        </tr>

				</tbody>

		  </table>

          

          <div class="profile-bg">

	<div class="col-sm-12 paginationdiv nopadding">

    <div class="col-sm-8 halfwidsec">

	<ul class="pagination">

	<li><a href="#"><img src="<?php echo base_url();?>assets/images/lefticon.png"></a></li>

	<li><a href="#">1</a></li>

	<li class="active"><a href="#">2</a></li>

	<li><a href="#">...</a></li>

	<li><a href="#">34</a></li>

	<li><a href="#"><img src="<?php echo base_url();?>assets/images/righticon.png"></a></li>

	</ul>

    </div>

    <div class="col-sm-4 totaldiv nopadding halfwidsec">

    <div class="col-sm-6 col-xs-6 totalstu halfwidsec">

    <h3>Total Students:855  </h3>

    </div>

    

    <div class="col-sm-6 col-xs-6 selectfilter selectfilterbox nopadding halfwidsec">

                <span>Showing:</span>

        <select class="form-control">

        <option>Select</option>

  <option>25</option>

  <option>30</option>

  <option>35</option>

  <option>45</option>

  <option>55</option>

</select>



        </div>

    </div>

	</div>

    </div>

</div>

	</div>

    </div>

    </div>
    <style>
	.form-group a {
    background: rgba(0, 0, 0, 0) linear-gradient(#39b54a, #33aa44) repeat scroll 0 0;
    border: 1px solid #249533 !important;
    border-radius: 3px;
    color: #fff !important;
    padding: 9px;
	font-size:14px !important;
    text-decoration: none !important;
}
.col-sm-3.rightspace.selectoption.viewreport.resviewbtn {
    float: right;
}
.form-group a{
	  padding: 7px 7px 10px 6px !important;
}
.form-control{
	padding:6px !important;
}

@media screen and (min-width:320px) and (max-width:480px){
.termsearch {
    padding: 10px 0;
    width: 100%;
}
.form-group{
	margin:0;
}
.selectfilter > span {
    float: left;
}
.selectfilter .form-control {
    width: 100%;
}
.form-group a{
	  padding: 6px !important;
	  margin-left:5px;
}
.form-group .bttn{
	float:left;
}
}

</style>