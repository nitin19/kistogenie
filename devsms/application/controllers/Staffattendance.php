<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Staffattendance extends CI_Controller {

	 var $logmode;
	function __construct(){
       
        parent::__construct();
        if( $this->authorize->is_user_logged_in() == false ){
			$this->session->set_flashdata('error', 'Please login first.');
			redirect(base_url());
		   }
		$this->logmode	= $this->session->userdata('log_mode');
        $this->load->model(array('login_model','authorization_model','staffattendance_model'));
		$this->load->database();
        $this->load->library('session');
    }

	public function index()

	{	
		
		$data							= array();
		$school_id						= $this->session->userdata('user_school_id');
			
		$branch_search					= isset($_GET['branch'])?$_GET['branch']:NULL;
		$attendancedate_search			= isset($_GET['date'])?$_GET['date']:NULL;
		
		$search_condition['school_id']	= $this->session->userdata('user_school_id');
		
		$data['all_branch']				= $this->staffattendance_model->getbranches($school_id);
		 
		if($branch_search!=''){
			$search_condition['branch_id']	= $branch_search;
		}
		
		/*if($attendancedate_search!=''){
			$search_condition['Date_Time'] = date('Y-m-d', strtotime($attendancedate_search));
		}*/
		
		$attendancedate	= date('Y-m-d', strtotime($attendancedate_search));

		$search_condition['is_active']	= '1';
		$search_condition['is_deleted']	= '0';
		
		
		$data['title']	      			= "Staff Attendance";
		$data['name']      				= $this->session->userdata('user_name');
		$data['school_id']				= $this->session->userdata('user_school_id');
		$start						    = $this->uri->segment(3,0);
		$ordrBY							= $this->uri->segment(4,0); 
		$dirc							= $this->uri->segment(5,"asc");
		
		$perpage						= 50;
		
		$config['uri_segment']			= 3;
		$config['base_url'] 			= base_url().'staffattendance/index';
	 
		$config['per_page'] 			= $perpage;
 		$config['postfix_string'] 		= "/?branch=$branch_search&attendancedate=$attendancedate_search"; 
		$this->pagination->initialize($config);
		$odr 							= "attendance_id";
		$dirc							= "asc";
		
	if($branch_search!=='' && $attendancedate_search!='') {
		 $config['total_rows'] 			= $this->staffattendance_model->getRows($search_condition , $attendancedate);

		if( $config['total_rows'] > 0 ) {
		
		    //$data_rows  				= $this->staffattendance_model->getPagedData($search_condition, $attendancedate, $start,$perpage,$odr,$dirc);
		    $data_rows  				= $this->staffattendance_model->getstaffs($school_id,$branch_search);
			$data['Attendencerecords'] 	= "Attendencerecords";
		} else {

			 $data_rows  				= $this->staffattendance_model->getstaffs($school_id,$branch_search);
			 $config['total_rows'] 		= count($data_rows);
		    }
	   }
		
		$data['dirc']					= $dirc;
		$data['ordrBY']					= $ordrBY;

		$data['data_rows']				= $data_rows;
		$data['total_rows']				= $config['total_rows'];
 
		$data['page_name']				= $this->uri->segment(1);
		$data['last_page']				= $start;
		$data['pagination']				= $this->pagination->create_links();
		$data['branch_search']			= $branch_search;
		$data['attendancedate_search']	= $attendancedate_search;
	
		$data['post_url']				= $config['postfix_string'];
		$data['logmode']				= $this->logmode;
		$data['error']					= $this->session->flashdata('error');
		$data['success']				= $this->session->flashdata('success');
		
		$this->load->view('header');

		$this->load->view('attendanceregister/staff_attendance', $data);

		$this->load->view('footer');

	}

	function check_staff_Attentance_status() {
	
		 $staffCount =  count(array_filter($this->input->post('staffid')));
		 $attendanceCount = count(array_filter($this->input->post('attendance_status')));
		 if($staffCount == $attendanceCount){
		           echo json_encode(array('Status'=>"true"));
			      } else {
			        echo json_encode(array('Status'=>"false"));
				  }
	  }
	
	function update_attendance() {
		
		   $school_id = $this->session->userdata('user_school_id');
		   
			if ($this->input->post('branchid') != '') {
                 $branch_id = trim($this->input->post('branchid'));
            }

			if ($this->input->post('attendancedate') != '') {
                  $attendancedate = trim($this->input->post('attendancedate'));
				  $attendance_date = date('Y-m-d', strtotime($attendancedate));
            }


			$chkAttendance = $this->staffattendance_model->check_attendance($school_id,$branch_id,$attendance_date);
			$countAttendance = count($chkAttendance);
			
			if( $countAttendance > 0 ) {
	
			           $input	=  $this->input->post();

		            if($this->staffattendance_model->updateAttendance($input)) {
		                  $this->session->set_flashdata('success', 'Attendance has been updated successfully.');
	   redirect(base_url()."staffattendance/index?branch=$branch_id&date=$attendancedate");exit;
				       } else {
					      $this->session->set_flashdata('error', 'Some problem exists. Attendance has not been updated.');
       redirect(base_url()."staffattendance/index?branch=$branch_id&date=$attendancedate");exit;
				        }
						
			       }  else  {
					   
					    $input		=  $this->input->post();
		            if($this->staffattendance_model->insertAttendance($input)) {
		                  $this->session->set_flashdata('success', 'Attendance has been added successfully.');
	  redirect(base_url()."staffattendance/index?branch=$branch_id&date=$attendancedate");exit;
				       } else {
					      $this->session->set_flashdata('error', 'Some problem exists. Attendance has not been added.');
	  redirect(base_url()."staffattendance/index?branch=$branch_id&date=$attendancedate");exit;
				           }
				       
					   }

	      }

}

